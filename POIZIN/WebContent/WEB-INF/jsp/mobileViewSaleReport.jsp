<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<%--    <spring:url value="/resources/css/3.3.7.bootstrap.min.css" var="bootstrap1mincss"/>
   <link rel="stylesheet" href="${bootstrap1mincss}"> --%>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-1.12.4.js" var="jquery1124js"/>
  <script src="${jquery1124js}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/angular.min.js" var="angularminjs"/>
  <script src="${angularminjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/angular-filter.js" var="angularfilterjs"/>
  <script src="${angularfilterjs}"></script>
<!--   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
  <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script>
 <spring:url value="/resources/css/common.css" var="commoncss"/>
   <link rel="stylesheet" href="${commoncss}">
   <spring:url value="/resources/css/bootstrap.min.css" var="bootstrapmincss"/>
   <link rel="stylesheet" href="${bootstrapmincss}">
 <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <spring:url value="/resources/javascript/mobileView/mobileViewSaleReport.js" var="mobileViewSaleReportjs"/>
  <script src="${mobileViewSaleReportjs}"></script> 
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
   <spring:url value="/resources/css/mobileViewSaleReport.css" var="mobileViewSaleReportcss"/>
   <link rel="stylesheet" href="${mobileViewSaleReportcss}">
   <script src="//code.angularjs.org/1.3.15/angular.js"></script>
<style>
 .color-green {
  color: green;
}

.color-orange {
  color: orange;
}

.color-red {
  color: red;
}
.glyphicon {
    position: relative;
    top: 0px;
    display: inline-block;
    font-family: 'Glyphicons Halflings';
    font-style: normal;
    font-weight: 400;
    line-height: inherit;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 12px;
}
.datalist {
    max-height: calc(100vh - 310px);
    overflow-y: auto;
    width: 100%;
}
summary{
display : block;
}
details summary::-webkit-details-marker { display:none; }
.container {
    padding-right: 2px;
    padding-left: 2px;
    padding-top: 15px;
    margin-right: auto;
    margin-left: auto;
}
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<a href="mobileViewHome" style="text-decoration: none;">
<header style="height: 55px;">
  <h2 style="float:left;padding: 0px 0px 0px 2px;">POIZIN</h2>
  <h6 style="float: right;padding: 8px 2px 0px 0px;">SALE REPORT</h6>
</header></a>
<div class="tab-content " style="font-family: unset;font-size: 13px;">
<div class="container">
<div class="row allignmentcss">
    <div class="col-md-6" style="margin-bottom: 10px;">
    Start: <input type="text" class="dateinput" id="startDate" readonly='true'>
    End: <input type="text" class="dateinput" id="endDate" readonly='true'>
    <input class="submit" type="submit" name="" ng-click="getResults('startDate','endDate')" value="Get" />
    </div>
	<div class="col-md-4" style="float: right;">
	<label class="switch">
     <input type="checkbox" id="includeZeroSaleOrNot" checked>
     <span class="slider round" style="background-color: red;" ng-click="includeZeroSaleOrNot()"></span>
    </label>
	
	<label class="switch">
     <input type="checkbox" id="catOrComp">
     <span class="slider round" style="background-color: #062351;" ng-click="FilterCatOrComp()"></span>
    </label>
	</div>
	<!-- <div class="col-md-2" style="float: left;font-weight: 600;color: #062351;">
	 Amount: {{alldetails | totalAmount | INR}}
	</div> -->
</div>
<div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
</div>
<div class="datalist">
<div class="container">
 <div id="showAllDetails">
		<div class="saleReportWithCompanyWise">
		<details ng-repeat="list in saledata | orderBy:'totalSaleAmount':true | filter:filterData">
				<summary>
					<div class="stockLiftList">
					    <span class="col-xs-4" style="color: #000;margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};">({{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%) {{list.company}}</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-down" style="color: red;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: green;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: yellow;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 " style="color: yellow;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo != 0">&nbsp;
						</span>
						<span class="col-xs-2" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2" style="margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;border-top-right-radius: 5px;border-bottom-right-radius: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalSaleAmount | INR}}</span>
					</div>
				</summary>
				<details ng-repeat="data in list.inputjson | orderBy:'saleValue':true">
				   <summary>
				   <span class="col-xs-4 secondSpan" style="color: #000;padding-left: 5px;text-align: left;background-color: #e8e8e8;">({{data.saleValue / list.totalSaleAmount * 100 | roundup}}%) {{data.brandname}}</span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-down" style="color: red;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: green;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo != 0">&nbsp;
					 </span>
					 <!-- <span class="col-xs-5 secondSpan" style="color: #000;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo != 0">{{data.brandname}} ({{data.saleValue / list.totalSaleAmount * 100 | roundup}}%)</span> -->
					 <span class="col-xs-2 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan" style="text-align: right;background-color: #e8e8e8;" title="" ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouseCase | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.saleValue | INR}}</span>
					 </summary>
					 <table class=" table table-fixed">
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true">
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
							<td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0">{{brand.quantity}}</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						</tr></table>
					 </details>
			</details>
			<details ng-repeat="list in saledata | orderBy:'totalSaleAmount':true | filter:filterDataBeer">
				<summary>
					<div class="stockLiftList">
						 <span class="col-xs-4" style="color: #000;margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};">({{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%) {{list.company}}</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-down" style="color: red;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: green;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: yellow;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 " style="color: yellow;margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo != 0">&nbsp;
						</span>
						<span class="col-xs-2" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2" style="margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;border-top-right-radius: 5px;border-bottom-right-radius: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalSaleAmount | INR}}</span>
					</div>
				</summary>
				<details ng-repeat="data in list.inputjson | orderBy:'saleValue':true">
				   <summary>
					 <span class="col-xs-4 secondSpan" style="color: #000;padding-left: 5px;text-align: left;background-color: #e8e8e8;">({{data.saleValue / list.totalSaleAmount * 100 | roundup}}%) {{data.brandname}}</span>
					  <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-down" style="color: red;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale"></span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: green;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale"></span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale"> </span>
					 <span class="col-xs-1 secondSpan" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo != 0">&nbsp; </span>
					 <span class="col-xs-2 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan" style="text-align: right;background-color: #e8e8e8;" title="" ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouseCase | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.saleValue | INR}}</span>
					 </summary>
					 <table class=" table table-fixed">
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true">
							 <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0">{{brand.quantity}}</td>
						   <td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						</tr></table>
					 </details>
			</details>  
		</div>
		<div class="saleReportWithCategoryWise">
		<details ng-repeat="list in saleCategoryData | orderBy:'categoryOrder' | filter:filterCategoryData">
				<summary>
					<div class="stockLiftList">
						<span class="col-xs-4" style="color: #000;margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};">({{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%) {{list.category}}</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-down" style="color: red;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: green;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: yellow;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1" style="color: yellow;margin-top: 5px;background-color: {{list.bgcolor}};" ng-if="list.compareNo != 0">&nbsp;
						</span>
						<span class="col-xs-2" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2" style="margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;border-top-right-radius: 5px;border-bottom-right-radius: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalSaleAmount | INR}}</span>
					</div>
				</summary>
				<details ng-repeat="data in list.inputcategoryjson | orderBy:'saleValue':true">
				   <summary>
				     <span class="col-xs-4 secondSpan" style="color: #000;padding-left: 5px;text-align: left;background-color: #e8e8e8;">({{data.saleValue / list.totalSaleAmount * 100 | roundup}}%) {{data.brandname}}</span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-down" style="color: red;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: green;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo != 0">&nbsp;
					 </span>
					 <span class="col-xs-2 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan" style="text-align: right;background-color: #e8e8e8;" title="" ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouseCase | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.saleValue | INR}}</span>
					 </summary>
					 <table class=" table table-fixed">
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true">
						 <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0">{{brand.quantity}}</td>
						   <td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						    <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						</tr></table>
					 </details>
			</details>
			<details ng-repeat="list in saleCategoryData | orderBy:'categoryOrder' | filter:filterCategoryDataBeer">
				<summary>
					<div class="stockLiftList">
						<span class="col-xs-4" style="color: #000;margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};">({{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%) {{list.category}}</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-down" style="color: red;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale"></span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: green;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale"></span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: yellow;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale"></span>
						<span class="col-xs-1" style="color: yellow;margin-top: 5px;background-color: {{list.bgcolor}};" ng-if="list.compareNo != 0">&nbsp;
						</span>
						<span class="col-xs-2" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2" style="margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;border-top-right-radius: 5px;border-bottom-right-radius: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalSaleAmount | INR}}</span>
					</div>
				</summary>
				<details ng-repeat="data in list.inputcategoryjson | orderBy:'saleValue':true">
				   <summary>
					 <span class="col-xs-4 secondSpan" style="color: #000;padding-left: 5px;text-align: left;background-color: #e8e8e8;">({{data.saleValue / list.totalSaleAmount * 100 | roundup}}%) {{data.brandname}}</span>
					<span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-down" style="color: red;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: green;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo != 0">&nbsp;
					 </span>
					 <span class="col-xs-2 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan" style="text-align: right;background-color: #e8e8e8;" title="" ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouseCase | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.saleValue | INR}}</span>
					 </summary>
					 <table class=" table table-fixed">
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true">
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0">{{brand.quantity}}</td>
						    <td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						</tr></table>
					 </details>
			</details>
		</div>
   </div>
   
    <div id="showAllOnlySaleDetails">
		<div class="saleReportWithCompanyWise">
		<h4 ng-if="(alldetails | totalAmount) <=0" style="text-align: center;padding: 8px;">No Data Available</h4>
		<details ng-repeat="list in saledataOnlySale | orderBy:'totalSaleAmount':true | filter:filterDataOnlySale">
				<summary>
					<div class="stockLiftList">
						<span class="col-xs-4" style="color: #000;margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};">({{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%) {{list.company}}</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-down" style="color: red;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: green;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: yellow;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1" style="color: yellow;margin-top: 5px;background-color: {{list.bgcolor}};" ng-if="list.compareNo != 0">&nbsp;
						</span>
						<span class="col-xs-2" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2" style="margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="" ng-class="{'color-red': list.firstFontColorWithFilter <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;border-top-right-radius: 5px;border-bottom-right-radius: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalSaleAmount | INR}}</span>
					</div>
				</summary>
				<details ng-repeat="data in list.inputjson | orderBy:'saleValue':true | filter: greaterThan('totalbtlsale', 0)">
				   <summary>
					 <span class="col-xs-4 secondSpan" style="color: #000;padding-left: 5px;text-align: left;background-color: #e8e8e8;">({{data.saleValue / list.totalSaleAmount * 100 | roundup}}%) {{data.brandname}}</span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-down" style="color: red;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: green;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo != 0">&nbsp;
					 </span>
					 <span class="col-xs-2 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan" style="text-align: right;background-color: #e8e8e8;" title="" ng-class="{'color-red': data.secondFontColorWithFilter <= 1}">{{data.inHouseCase | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.saleValue | INR}}</span>
					 </summary>
					 <table class=" table table-fixed">
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true | filter: greaterThanStock('stock', 0)">
							<td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0">{{brand.quantity}}</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						</tr></table>
					 </details>
			</details>
			<details ng-repeat="list in saledataOnlySale | orderBy:'totalSaleAmount':true | filter:filterDataBeerOnlySale">
				<summary>
					<div class="stockLiftList">
						<span class="col-xs-4" style="color: #000;margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};">({{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%) {{list.company}}</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-down" style="color: red;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: green;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: yellow;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1" style="color: yellow;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo != 0">&nbsp;
						</span>
						<span class="col-xs-2" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2" style="margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="" ng-class="{'color-red': list.firstFontColorWithFilter <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;border-top-right-radius: 5px;border-bottom-right-radius: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalSaleAmount | INR}}</span>
					</div>
				</summary>
				<details ng-repeat="data in list.inputjson | orderBy:'saleValue':true | filter: greaterThan('totalbtlsale', 0)">
				   <summary>
					 <span class="col-xs-4 secondSpan" style="color: #000;padding-left: 5px;text-align: left;background-color: #e8e8e8;">({{data.saleValue / list.totalSaleAmount * 100 | roundup}}%) {{data.brandname}}</span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-down" style="color: red;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: green;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo != 0">&nbsp;
					 </span>
					 <span class="col-xs-2 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan" style="text-align: right;background-color: #e8e8e8;" title="" ng-class="{'color-red': data.secondFontColorWithFilter <= 1}">{{data.inHouseCase | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.saleValue | INR}}</span>
					 </summary>
					 <table class=" table table-fixed">
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true | filter: greaterThanStock('stock', 0)">
							<td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0">{{brand.quantity}}</td>
						    <td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						</tr></table>
					 </details>
			</details>  
		</div>
		<div class="saleReportWithCategoryWise">
		<h4 ng-if="(alldetails | totalAmount) <=0" style="text-align: center;padding: 8px;">No Data Available</h4>
		<details ng-repeat="list in saleCategoryDataOnlySale | orderBy:'categoryOrder' | filter:filterCategoryDataOnlySale">
				<summary>
					<div class="stockLiftList">
						<span class="col-xs-4" style="color: #000;margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};">({{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%) {{list.category}}</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-down" style="color: red;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: green;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: yellow;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1" style="color: yellow;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo != 0">&nbsp;
						</span>
						<span class="col-xs-2" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2" style="margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;border-top-right-radius: 5px;border-bottom-right-radius: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalSaleAmount | INR}}</span>
					</div>
				</summary>
				<details ng-repeat="data in list.inputcategoryjson | orderBy:'saleValue':true | filter: greaterThan('totalbtlsale', 0)">
				   <summary>
					<span class="col-xs-4 secondSpan" style="color: #000;padding-left: 5px;text-align: left;background-color: #e8e8e8;">({{data.saleValue / list.totalSaleAmount * 100 | roundup}}%) {{data.brandname}}</span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-down" style="color: red;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: green;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo != 0">&nbsp;
					 </span>
					 <span class="col-xs-2 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan" style="text-align: right;background-color: #e8e8e8;" title="" ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouseCase | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.saleValue | INR}}</span>
					 </summary>
					 <table class=" table table-fixed">
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true | filter: greaterThanStock('stock', 0)">
							<td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0">{{brand.quantity}}</td>
						     <td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						</tr></table>
					 </details>
			</details>
			<details ng-repeat="list in saleCategoryDataOnlySale | orderBy:'categoryOrder' | filter:filterCategoryDataBeerOnlySale">
				<summary>
					<div class="stockLiftList">
					    <span class="col-xs-4" style="color: #000;margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};">({{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%) {{list.category}}</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-down" style="color: red;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: green;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1 glyphicon glyphicon-arrow-up" style="color: yellow;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale">
						</span>
						<span class="col-xs-1" style="color: yellow;margin-top: 5px;text-align: left;background-color: {{list.bgcolor}};" ng-if="list.compareNo != 0">&nbsp;
						</span>
						<span class="col-xs-2" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2" style="margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;border-top-right-radius: 5px;border-bottom-right-radius: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalSaleAmount | INR}}</span>
					</div>
				</summary>
				<details ng-repeat="data in list.inputcategoryjson | orderBy:'saleValue':true | filter: greaterThan('totalbtlsale', 0)">
				   <summary>
					 <span class="col-xs-4 secondSpan" style="color: #000;padding-left: 5px;text-align: left;background-color: #e8e8e8;">({{data.saleValue / list.totalSaleAmount * 100 | roundup}}%) {{data.brandname}}</span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-down" style="color: red;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: green;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan glyphicon glyphicon-arrow-up" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale">
					 </span>
					 <span class="col-xs-1 secondSpan" style="color: yellow;padding-left: 5px;text-align: left;background-color: #e8e8e8;" ng-if="data.compareNo != 0">&nbsp;
					 </span>
					 <span class="col-xs-2 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan" style="text-align: right;background-color: #e8e8e8;" title="" ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouseCase | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.saleValue | INR}}</span>
					 </summary>
					 <table class=" table table-fixed">
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true | filter: greaterThanStock('stock', 0)">
							<td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0">{{brand.quantity}}</td>
						      <td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale">{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						</tr></table>
					 </details>
			</details>
		</div>
   </div>
</div>
<!-- footer start -->
<div class="fixed">
			<div class="row">
				<div class="col-xs-4" style="text-align: left;padding: 1px;">
					 <table>
						<tr>
							<th style="font-size: 12px; font-style: italic;padding: 0px 2px 0px 0px;">Sale : </th>
							<td style="float: right;">{{alldetails | totalAmount | INR}}</td>
						</tr>
						<tr>
							<th style="font-size: 12px; font-style: italic;padding: 0px 2px 0px 0px;">Expenses : </th>
							<td style="float: right;">{{allExpenseAmt | INR}}</td>
						</tr>
					</table>
				</div>
				<div class="col-xs-4" style="text-align: center;padding: 1px;">
					 <table>
						<tr>
							<th style="font-size: 12px; font-style: italic;padding: 0px 2px 0px 0px;">BEER Cases : </th>
							<td style="float: right;">{{saleCategoryDataOnlySale | soldBeerCase | INR}}</td>
						</tr>
						<!-- <tr>
							<th style="font-size: 12px; font-style: italic;padding-right: 5px;">LIQUOR Cases : </th>
							<td style="float: right;">{{saleCategoryDataOnlySale | soldLiquorCase | INR}}</td>
						</tr> -->
					</table>
				</div>
				<div class="col-xs-4" style="    padding: 0px 10px 0px 0px;">
			     <table style="float: right;">
				        <tr>
							<th style="font-size: 12px; font-style: italic;padding: 0px 2px 0px 0px;">Card & Cash : </th>
							<td style="float: right;">{{carAndCash | INR}}</td>
						</tr>
						<tr>
							<th style="font-size: 12px; font-style: italic;padding: 0px 2px 0px 0px;">Diff : </th>
							<td style="float: right;">{{difference | INR}}</td>
						</tr>
						
					</table>
				</div>
			</div>
		</div>
<!-- footer end -->
</div>
</div>
</body>
</html>