<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
<jsp:include page="/WEB-INF/jsp/header.jsp" /> 
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
   <!-- <script src="https://rawgit.com/zorab47/jquery.ui.monthpicker/master/jquery.ui.monthpicker.js"></script> -->
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.ui.monthpicker.js" var="jqueryuimonthpickerjs"/>
  <script src="${jqueryuimonthpickerjs}"></script>
  <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">	 -->
  <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
   
 <spring:url value="/resources/javascript/invoice.js" var="invoicejs"/>
  <spring:url value="/resources/javascript/generatePdfForDiscount.js" var="generatePdfForDiscountjs"/>
  <script src="${generatePdfForDiscountjs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script>  
  <spring:url value="/resources/javascript/app.js" var="appjs"/>
  <script src="${appjs}"></script>
  <spring:url value="/resources/javascript/test.js" var="testjs"/>
  <script src="${testjs}"></script>
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
  <link rel="stylesheet" href="${newCommoncss}">
   <spring:url value="/resources/css/balanceSheet.css" var="balanceSheetcss"/>
   <link rel="stylesheet" href="${balanceSheetcss}">
   
  <style type="text/css">
  
  table{
        width: 100%;
        margin-bottom: 20px;
		border-collapse: collapse;
    }
     table td{
      padding: 2px;
      text-align: right;
      font-size: 12px;
    }
    table th{
    padding: 2px;
    text-align: center;
    font-size: 12px;
    background: antiquewhite;
    }
    table,tbody,thead,td,th {
    background-color: #fff;
    border: solid 1px #000;
    font-size: 15px;
    text-align: right;
    line-height: 20px;
}
b, strong {
    font-weight: 600;
}
.fontfamily {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
}
  </style>
</head>
<body ng-app="myApp" ng-controller="Ctrl1">
<div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
 <div class="row" style="padding-top: 100px;">
  <div class="col-sm-3 bodyFontCss" style="padding: 0px !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Discount</li>
  <li>Discount PDF</li>
</ul>
  </div>
  <div class="col-sm-1 bodyFontCss">
   </div>
   <div class="col-sm-2 bodyFontCss">
     <label style="font-weight: 700;">Start Month: </label><input type="text" class="form-control monthpicker dateinput" id="startMonth" ng-model="startMonth"  readonly='true'>
    </div>
    <div class="col-sm-2 bodyFontCss">
     <label style="font-weight: 700;">End Month: </label><input type="text" class="form-control monthpicker dateinput" id="endMonth" ng-model="endMonth" readonly='true'>
    </div>
     <div class="col-sm-2 bodyFontCss">
   <label>Select Company</label>
  <select name="debitCredit" id="listdataSec" ng-model="companyId" required="required" class="form-control"></select>
   </div>
    <div class="col-sm-2 bodyFontCss" style="padding-top: 12px;">
    <label>&nbsp;</label><input class="btn btn-default btn-rounded" type="submit" name="" ng-click="buildDiscountWithPdf()" value="GET DATA" />
    </div>
</div>
 <div class="container" style="margin-top: 20px;">
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
			<b><span id="companyVal" style="padding: 0px 0px 0px 15px;font-size: 12px;">{{companyName}} {{enterprice}} and selected month  ( From {{customStartMonthdisplay}} To {{customEndMonthdisplay}} )</span></b>
			<table id="">
			    <tbody>
			        <tr>
			        <td style="text-align: center;"><b>Total Discount Amount</b></td>
			        <td style="text-align: right;"><b>{{discountPlusRental | INR}}</b></td>
			        </tr>
			        <tr ng-if="totalVendorCredit > 0">
			        <td style="text-align: center;"><b>Total Vendor Credit</b></td>
			        <td style="text-align: right;"><b>{{totalVendorCredit | INR}}</b></td>
			        </tr>
			        <tr>
			        <td style="text-align: center;"><b>Total Received Amount</b></td>
			        <td style="text-align: right;"><b>{{totalChequeAmount | INR}}</b></td>
			        </tr>
			         <tr>
			        <td style="text-align: center;"><b>Cheque Adjustments</b></td>
			        <td style="text-align: right;"><b>{{adjCheque | INR}}</b></td>
			      </tr>
			      <tr ng-if="totalVendorDiscount != 0">
			        <td style="text-align: center;"><b>Total Vendor Discount</b></td>
			        <td style="text-align: right;"><b>{{totalVendorDiscount| INR}}</b></td>
			      </tr>
			      <%-- <tr ng-if="(((receivedData | totalChequeAmount) + (receivedData | adjCheque)  + (arrearsdetails | totalVendorDiscount)) - ((transactionData | totalDiscountAmt) + (rentalAmt)+ (previousArrears.value))) <=0"> --%>
			      <tr ng-if="((totalChequeAmount + adjCheque + totalVendorDiscount + totalVendorCreditReceived) - (discountPlusRental + previousArrears.value + totalVendorCredit)) <=0">
			        <td style="text-align: center;font-weight: bold;"><b>Amount due</b></td>
			        <td style="text-align: right;font-weight: bold;color: red;"><b>{{((discountPlusRental + previousArrears.value + totalVendorCredit) - (totalChequeAmount + adjCheque + totalVendorDiscount + totalVendorCreditReceived)) | INR}}</b></td>
			      </tr>
			      <tr ng-if="((totalChequeAmount + adjCheque + totalVendorDiscount + totalVendorCreditReceived) - (discountPlusRental + previousArrears.value + totalVendorCredit)) > 0">
			        <td style="text-align: center;font-weight: bold;"><b>Amount exceeds</b></td>
			        <td style="text-align: right;font-weight: bold;color: green;"><b>{{((totalChequeAmount + adjCheque + totalVendorDiscount) - (discountPlusRental + previousArrears.value +totalVendorCredit + totalVendorCreditReceived)) | INR}}</b></td>
			      </tr>
			    </tbody>
			  </table> 
			  <table ng-if="totalVendorDiscount != 0">
					<thead>
						<tr>
							<th>Month</th>
							<th>Vendor Discount Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr	ng-repeat="arrList in arrearsdetails">
							<td style="text-align: center;"><b>{{arrList.date}}</b></td>
							<td style="text-align: right;"><b>{{arrList.vendorAmt | INR}}</b></td>
						</tr>
					 <tr>
			             <td style="background: antiquewhite;"><b>Total Vendor Discount</b></td>
			             <td style="background: antiquewhite;"><b>{{totalVendorDiscount | INR}}</b></td>
			      </tr>
					</tbody>
				</table>
				<table ng-if="totalVendorCredit > 0">
					<thead>
						<tr>
							<th>Vendor Name</th>
							<th>Date</th>
							<th>Product name</th>
							<th>Case/BTL</th>
							<th>Total Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr	ng-repeat="creditList in creditChildBeanList">
							<td style="text-align: center;"><b>{{creditList.name}}</b></td>
							<td style="text-align: center;"><b>{{creditList.date}}</b></td>
							<td style="text-align: center;"><b>{{creditList.productName}}</b></td>
							<td style="text-align: center;"><b>{{creditList.cases}} / {{creditList.bottles}}</b></td>
							<td style="text-align: right;"><b>{{creditList.afterIncludingDiscount | INR}}</b></td>
						</tr>
					    <tr>
				             <td style="background: antiquewhite;" colspan="4"><b>Total</b></td>
				             <td style="background: antiquewhite;"><b>{{totalVendorCredit | INR}}</b></td>
			           </tr>
					</tbody>
				</table>
			
				<table id="receivedDiscount">
					<thead>
						<tr>
							<th>Tr. Date</th>
							<th>Bank</th>
							<th>Tr. Type</th>
							<th>Cheque No</th>
							<th>Cheque Amt</th>
						</tr>
					</thead>
					<tbody>
						<tr
							ng-repeat="cval in receivedData | filter: greaterThan('chequeAmount', 0)">
							<td style="text-align: left;"><b>{{cval.enterprise}}</b></td>
							<td style="text-align: center;"><b>{{cval.companyName}}</b></td>
							<td style="text-align: center;"><b>{{cval.comment}}</b></td>
							<td style="text-align: center;"><b>{{cval.color}}</b></td>
							<td><b>{{cval.chequeAmount | INR}}</b></td>
						</tr>
					 <tr>
			             <td style="background: antiquewhite;" colspan="4"><b>Total Cheques Received</b></td>
			             <td style="background: antiquewhite;"><b>{{totalChequeAmount | INR}}</b></td>
			      </tr>
					</tbody>
				</table>
				
				
				<table id="companyDiscount">
			    <thead>
			      <tr>
			        <th>Month</th>
			         <th>Brand</th>
			        <th>Case</th>
			        <th>Disc/Case</th>
			        <th>Total</th>
			      </tr>
			    </thead>
			    <tbody ng-repeat="list in transactionData">
			      <tr>
			        <td style="text-align: left;"><b>{{list.month}}</b></td>
			        <td style="text-align: left;"><b>{{list.discountMonthWiseBean[0].brand}}</b></td>
			        <td><b>{{list.discountMonthWiseBean[0].case | INR}}</b></td>
			        <td><b>{{list.discountMonthWiseBean[0].discount | INR}}</b></td>
			        <td><b>{{list.discountMonthWiseBean[0].total | INR}}</b></td>
			      </tr>
			      <tr ng-repeat="brand in list.discountMonthWiseBean" ng-if="$index > 0">
			       <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="text-align: left;"><b>{{brand.brand}}</b></td>
			        <td><b>{{brand.case | INR}}</b></td>
			        <td><b>{{brand.discount | INR}}</b></td>
			        <td><b>{{brand.total | INR}}</b></td>
			      </tr>
			       <tr>
			       <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="background: antiquewhite;"><b>{{list.totalDiscount | INR}}</b></td>
			      </tr>
			    </tbody>
			    <tbody>
			     <tr>
			       <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="background: antiquewhite;"><b>Grand Total Discount	</b></td>
			        <td style="background: antiquewhite;"><b>{{totalDiscountAmt | INR}}</b></td>
			      </tr>
			      <tr ng-if="rentalAmt != 0">
			       <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="background: antiquewhite;"><b>Rentals</b></td>
			        <td style="background: antiquewhite;"><b>{{ rentalAmt | INR}}</b></td>
			      </tr>
			    </tbody>
			  </table> 
			</div>
			<div class="col-md-1">
			<a style="float: right;" id="downloadpdfId" href="mobileView/downloadDiscountForSelectedMonths?company={{companyId}}&startMonth={{firstDate}}&endMonth={{secondDate}}&companyName={{companyName}}&customSMonth={{customStartMonthdisplay}}&customEMonth={{customEndMonthdisplay}}">
	           <img alt="pdfImg" src="resources/images/pdf.png" style="float: right; margin-right: 30px;cursor: pointer;"></a>
			</div>
		</div>
	</div>		
</body>
</html>