<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 06-->
<spring:url value="/resources/css/3.3.7.bootstrap.min.css" var="bootstrap1mincss"/>
   <link rel="stylesheet" href="${bootstrap1mincss}">
   
<jsp:include page="/WEB-INF/jsp/headerWithNav.jsp" />  
<spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script>
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">06 -->
  <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
  
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
    <spring:url value="/resources/css/bootstrap-select.min.css" var="bootstrapselectmincss"/>
   <link rel="stylesheet" href="${bootstrapselectmincss}">
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap-select.min.js" var="bootstrapselectminjs"/>
  <script src="${bootstrapselectminjs}"></script>
  <spring:url value="/resources/css/salepage.css" var="salepagecss"/>
   <link rel="stylesheet" href="${salepagecss}">
   <script>
   $( function() {
    $( "#bankdate" ).datepicker({ dateFormat: 'dd-mm-yy',
    	onSelect: function(selected,evnt) {
            updateAb(selected);
       }
    });
  });
  </script> 
  <script>
    $(function() {
    $('#toggle').click(function() {
    	   $("#saledetails,#saleSecDetails").find('th:nth-child(9),td:nth-child(9)').toggle();
    	   $("#saledetails,#saleSecDetails").find('th:nth-child(1),td:nth-child(1)').css('width', '335px');
       });
    });
    </script>
</head>
<body>
<div class="container" style="padding-top: 100px;">
 <div class="row" style="margin-bottom: 20px;">
 <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
	 <div class="col-sm-12 bodyFontCss">
	  <ul class="nav nav-tabs">
	    <li class="active"><a data-toggle="tab" href="#home">SALE</a></li>
	    <li><a data-toggle="tab" href="#menu1">CARD/CASH</a></li>
	    <li><a data-toggle="tab" href="#menu2">EXPENSES</a></li>
	    <li><a data-toggle="tab" href="#menu3">BANK CREDIT</a></li>
	    <li><a data-toggle="tab" href="#menu4">DOWNLOAD SALE SHEET</a></li>
	  </ul>
	</div>
</div>
<div class="tab-content">
	<div id="home" class="tab-pane customefade fade in active">
	<div class="row">
	 <div class="col-sm-2 bodyFontCss">
	 <input type="hidden" id="commentdate" name="">
	 <div id="saleNextDate"></div>
	 </div>
	 <div class="col-sm-2 bodyFontCss"></div>
		 <div class="col-sm-2 bodyFontCss onlySaleItmes">
		 <input type="text" class="myInputSearch form-control" id="searchonlysale" placeholder="Search Brand Name or Number..." title="Type in a name" style="height: 75%;width: 100%;">
		 </div>
		 <div class="col-sm-2 bodyFontCss totalSaleItmes">
		 <input type="text" class="myInputSearch form-control" id="searchtotalsale" placeholder="Search Brand Name or Number..." title="Type in a name" style="height: 75%;width: 100%;">
		 </div>
	 <div class="col-sm-2 bodyFontCss"></div>
	 <div class="col-sm-2 bodyFontCss">
	 <input id="toggle" type="checkbox" style="margin: 0px; width: 25px;" value="Toggle Return Column"/><label>RETURN</label>
	 </div>
	  <div class="col-sm-2 bodyFontCss">
	  <span><i class="fa fa-filter filter-icon" style="color: #243a51!important;" data-toggle="modal" data-target="#saleFilterModal" aria-hidden="true"></i></span>
	  </div>
	</div>
	<div class="row">
	<div class="col-sm-12 bodyFontCss onlySaleItmes">
	   <form action="" id="saleForm" method="post" onsubmit="return saveOnlySaleItems(this);">
	    <table id="saledetails" class="saledetailreport fixed_headers" >
	    </table>
	 <p style="text-align: center;">
	 <span style="    float: left; padding: 5px 5px 5px 5px;">
	 <textarea id="saleComment" name="saleComment" maxlength="700" required>No Data</textarea>
	 </span>
	 <input type="submit" class="saveDetail btn btn-default btn-rounded" style="    width: 8%;margin-top: 12px;"  name="" value="SAVE DETAILS" /></p>
	  <div class="ttlmrp"></div>
	    </form>
	   <div style=" float: right;padding: 0px 8px 0px 0px;font-size: 14px;font-weight: 600;">
		 <span style="color: #de7676;" class="sumOfTotalAmount"></span>
		 <span style="color: green;" class="sumOfTotalSale"></span>
	  </div>
	  
	</div>
	<div class="col-sm-12 bodyFontCss totalSaleItmes">
	   <form action="" id="saleFormSecond" method="post" onsubmit="return saveTotalSaleItems(this);">
	    <table id="saleSecDetails" class="saledetailreport fixed_headers" >
	    </table>
	 <p style="text-align: center;">
	 <span style="    float: left; padding: 5px 5px 5px 5px;">
	 <textarea id="saleCommentTotal" name="saleCommentTotal" maxlength="700" required>No Data</textarea>
	 </span>
	 <input type="submit" class="saveDetail btn btn-default btn-rounded" style="    width: 8%;margin-top: 12px;"  name="" value="SAVE DETAILS" /></p>
	  <div class="ttlmrp"></div>
	  <div style=" float: right;padding: 0px 8px 0px 0px;font-size: 14px;font-weight: 600;">
		 <span style="color: #de7676;" class="sumOfTotalAmount"></span>
		 <span style="color: green;" class="sumOfTotalSale"></span>
	  </div>
	    </form>
	   
	</div>
	</div>
	</div>
	<!-- Card cash sale -->
	<div id="menu1" class="tab-pane fade">
	 <div class="row">
	<div class="col-sm-4 bodyFontCss"></div>
	<div class="col-sm-4 bodyFontCss">
	<form style="margin-top: 20px;" id="cardsaleForm" method="post" >
	<div id="cardSaleFormId"></div>
	</form>
	</div>
	<div class="col-sm-4 bodyFontCss"></div>
	</div>
    </div>
    <!-- Expenses -->
    <div id="menu2" class="tab-pane fade">
        <!-- Expenses open -->
    <div class="row">
    <div class="col-sm-4 bodyFontCss"></div>
    <div class="col-sm-4 bodyFontCss">
    <select id='mySelect' class="form-control" onchange="jsFunction(this.value);"></select>
    </div>
    <div class="col-sm-4 bodyFontCss"></div>
    </div>
    <div class="row" style="margin-bottom: 20px;">
    <div class="col-sm-3 bodyFontCss">
    <label >Expense Date</label>
    <input type="text" id="expensedatepicker" name="expenseDate" class="form-control" readonly="readonly" required>
    </div>
    <div class="col-sm-3 bodyFontCss">
    <label style="display: none;">Expense Name</label>
    <input type="hidden" id="ename"  name="expenseName" readonly="readonly">
    <label>Expense Amount</label>
    <input type="text" id="eamount" class="form-control" onkeyup="validationDecimalFun(this)" name="expenseAmount" required>
    </div>
    <div class="col-sm-3 bodyFontCss">
    <label>Description</label>
    <textarea id="description" name="expenseComment" class="form-control" placeholder="Your Comment..."></textarea>
    <input type="hidden" id="catid" name="categoryId" value="">
    </div>
    <div class="col-sm-1 bodyFontCss customizedButton">
    <input type="button" class="add-row btn btn-default btn-rounded" value="ADD" />
    </div>
    <div class="col-sm-2 bodyFontCss"></div>
    </div>
  
     <div id="idate" style="display: none;"></div> 
     <table id="ItemsTable">
        <thead>
            <tr style="color: #fff;background-color: #062351;">
                <th>Select</th>
                <th>S No.</th>
                <th>Expense Name</th>
                <th>Ammount</th>
                <th>Comment</th>
            </tr>
        </thead>
        <tbody id="invoiceTrList">
        </tbody>
    </table>
     <div style="text-align: right;"><button type="button" class="delete-row btn btn-default btn-rounded">DELETE</button>
     <button type="button" class="btn btn-default btn-rounded" onclick="postExpensesData()" >Submit</button>
   <div id="ttlmrp"></div></div> 
    <!-- expenses close -->
    
    </div>
    <div id="menu3" class="tab-pane fade">
       <div class="row">
           <div class="col-sm-4 bodyFontCss"></div>
           <div class="col-sm-4 bodyFontCss">
           <form id="bankCreditForm" method="post">
			  <label>Selected Date:</label>
			  <input type="text" class="dateinput form-control"  id="bankdate" required readonly="readonly">
			  <label> Credit Amount</label>
			  <input type="number" class="form-control"  id="bankCredit" onkeyup="calcRetention();" onclick="calcRetention();" name="bankCredit" value="" required>
			  <label> Cash Retention</label></span>
			  <input type="number" class="form-control" id="retention" name="retention" value="" readonly="readonly">
			  <input type="hidden" id="hiddenVal" name="hiddenVal" value="">
			  <p style="text-align: center;">
			  <input type="button" class=" btn btn-default btn-rounded" onclick="submitBankCreditAmount()" value="Submit">
			  </p>
			</form>
           </div>
           <div class="col-sm-4 bodyFontCss"></div>       
		</div>
    </div> 
    <div id="menu4" class="tab-pane fade">
       <div class="row">
       <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
           <div class="col-sm-4 bodyFontCss"></div>
           <div class="col-sm-2 bodyFontCss">
          <input class="btn btn-default btn-rounded" style="width: 100%;" onclick="generateSaleSheet()" type="submit" name="" value="GENERATE SALE SHEET" />
          </div>
          <div class="col-sm-2 bodyFontCss">
          <input class="btn btn-default btn-rounded" style="width: 100%;" onclick="generateEmptySaleSheet()" type="submit" name="" value="GENERATE EMPTY SALE SHEET" />
           </div>
           <div class="col-sm-4 bodyFontCss"></div>       
		</div>
    </div> 
</div>
</div>

  <!-- Modal -->
<div class="modal fade right" id="saleFilterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-side modal-top-right" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title w-100" id="exampleModalLabel"><i class="fa fa-filter" style="color: #243a51!important;font-size: 22px;"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true" style="font-size: 25px;">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
	   <div style="height: 40px;">
	    <div style="float: right;padding: 12px;">
	    <label class="filterLabelFontSize">Only Sale Items</label>
	    </div>
	    <div style="float: right;">
	    <label class="switch ">
         <input type="checkbox" id="includeZeroSaleOrNot" checked>
         <span class="slider round" style="background-color: red;" onclick="includeZeroSaleOrNot();"></span>
        </label>
        </div>
        <div style="float: right;padding: 12px;">
        <label class="filterLabelFontSize">Total Sale  Items</label>
	   </div>
		</div>
	 </div>
      <div class="modal-footer">
       <!--  <button type="button" style="background: #1a6398!important;" class="btn btn-primary btn-rounded" data-dismiss="modal">Apply Filters</button> -->
      </div>
    </div>
  </div>
</div>
<spring:url value="/resources/javascript/sale.js" var="salejs"/>
  <script src="${salejs}"></script>
  <spring:url value="/resources/javascript/expenses.js" var="expensesjs"/>
  <script src="${expensesjs}"></script>
  <script>
  $("#searchonlysale").on("keyup", function() {
	    var value = $(this).val().toUpperCase();
	    $("#saledetails tr").each(function(index) {
	        if (index !== 0) {
	            var $row = $(this);
	            //Filter down tds that match indexOf check
	            var matches = $row.find('td').filter(function(ix,item){
	            	return $(item).text().indexOf(value) > -1;
	            });

							//if matches exist then show else hide
	            if (matches.length != 0) {
	                $row.show();    
	            }
	            else {
	                $row.hide();    
	            }
	        }
	    });
	})
$("#searchtotalsale").on("keyup", function() {
	    var value = $(this).val().toUpperCase();
	    $("#saleSecDetails tr").each(function(index) {
	        if (index !== 0) {
	            var $row = $(this);
	            //Filter down tds that match indexOf check
	            var matches = $row.find('td').filter(function(ix,item){
	            	return $(item).text().indexOf(value) > -1;
	            });

							//if matches exist then show else hide
	            if (matches.length != 0) {
	                $row.show();    
	            }
	            else {
	                $row.hide();    
	            }
	        }
	    });
	})
</script>
</body>
</html>