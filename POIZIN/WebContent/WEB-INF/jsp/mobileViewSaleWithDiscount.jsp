<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-1.12.4.js" var="jquery1124js"/>
  <script src="${jquery1124js}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/angular.min.js" var="angularminjs"/>
  <script src="${angularminjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/angular-filter.js" var="angularfilterjs"/>
  <script src="${angularfilterjs}"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
  <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script>
 <spring:url value="/resources/css/common.css" var="commoncss"/>
   <link rel="stylesheet" href="${commoncss}">
   <spring:url value="/resources/css/bootstrap.min.css" var="bootstrapmincss"/>
   <link rel="stylesheet" href="${bootstrapmincss}">
 <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <spring:url value="/resources/javascript/mobileView/mobileViewSaleWithDiscount.js" var="mobileViewSaleWithDiscountjs"/>
  <script src="${mobileViewSaleWithDiscountjs}"></script> 
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
   <spring:url value="/resources/css/mobileViewSaleReport.css" var="mobileViewSaleReportcss"/>
   <link rel="stylesheet" href="${mobileViewSaleReportcss}">
   <script src="//code.angularjs.org/1.3.15/angular.js"></script>
<style>
 .color-green {
  color: green;
}

.color-orange {
  color: orange;
}

.color-red {
  color: red;
}
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<a href="mobileViewHome" style="text-decoration: none;">
<header style="height: 55px;">
  <h2 style="float:left;padding: 0px 0px 0px 2px;">POIZIN</h2>
  <h6 style="float: right;padding: 8px 2px 0px 0px;">SALE WITH DISCOUNT</h6>
</header></a>
<div class="tab-content " style="font-family: unset;font-size: 13px;">
<div class="container">
<div class="row allignmentcss">
    <div class="col-md-6" style="margin-bottom: 10px;">
    Start: <input type="text" class="dateinput" id="startDate" readonly='true'>
    End: <input type="text" class="dateinput" id="endDate" readonly='true'>
    <input class="submit" type="submit" name="" ng-click="getResults('startDate','endDate')" value="Get" />
    </div >
	<div class="col-md-2" style="float: right;font-weight: 600;color: #062351;">
	 <label class="switch">
     <input type="checkbox" id="catOrComp">
     <span class="slider round" style="background-color: #062351;" ng-click="FilterCatOrComp()"></span>
    </label>
	</div>
	<div class="col-md-4" style="float: left;font-weight: 600;color: #062351;">
	 Sale Amt: {{alldetails | totalSaleAmount | INR}} , Disc: {{alldetails | totalDiscountAmount | INR}}
	</div>
</div>
<div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
          
      <div class="saleReportWithCompanyWise">    
         <div class="row" style="border-radius: 5px;color: #062351;font-size: 11px;">
				<span class="col-xs-4" style="margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;padding: 0px 0px 0px 20px;" title="">Company</span>
				<span class="col-xs-2" style="margin-top: 5px;text-align: right;padding: 0px;" title="">Sale</span>
				<span class="col-xs-3" style="margin-top: 5px;text-align: right;padding: 0px;">Sale Amt.</span>
				<span class="col-xs-3" style="border-bottom-right-radius: 5px;border-top-right-radius: 5px;margin-top: 5px;text-align: right;padding: 0px 10px 0px 0px;" title="">Discount</span>
			</div>
		<details ng-repeat="list in saleWithDiscountdata | orderBy:'totalDiscountAmount':true | filter:filterData | filter: greaterThan('totalSaleamount', 0)">
				<summary>
					<div class="stockLiftList">
						<span class="col-xs-4" style="color: #000;margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};" title="">{{list.company}}</span>
						<span class="col-xs-2" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalsaleQty | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="" >{{list.totalSaleamount | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;border-top-right-radius: 5px;border-bottom-right-radius: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalDiscountAmount | INR}}</span>
					</div>
				</summary>
				<details ng-repeat="data in list.inputjson | orderBy:'discountAmount':true | filter: greaterThan('salAmount', 0)">
				   <summary>
					 <span class="col-xs-4 secondSpan" style="color: #000;padding-left: 5px;text-align: left;background-color: #e8e8e8;" title="">{{data.brandname}}</span>
					<span class="col-xs-2 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.saleQty | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.salAmount | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.discountAmount | INR}}</span>
					 </summary>
					 <table class=" table table-fixed">
					     <tr ng-repeat="brand in data.Brands | orderBy:'totalDiscount':true | filter: greaterThan('totalPrice', 0)">
							<td class="col-xs-2" style="padding: 8px 0px 8px 0px;color: #000;border-top: 0px solid #ddd;" title="{{brand.discountMonth}}">{{brand.discountMonth}}</td>
						    <td class="col-xs-2" style="padding: 8px 0px 8px 0px;color: #000;border-top: 0px solid #ddd;" title="{{brand.quantity}}">{{brand.quantity}}</td>
						    <td class="col-xs-2" style="padding: 8px 0px 8px 0px;text-align: right;color: #000;border-top: 0px solid #ddd;" title="{{brand.saleInCase}}/{{brand.btlVal}}">{{brand.saleInCase}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="padding: 8px 0px 8px 0px;text-align: right;color: #000;border-top: 0px solid #ddd;" title="{{brand.totalPrice}}">{{brand.totalPrice | INR}}</td>
						    <td class="col-xs-3" style="padding: 8px 0px 8px 0px;text-align: right;color: #000;border-top: 0px solid #ddd;" title="{{brand.totalDiscount}}" >{{brand.totalDiscount | INR}}</td>
						</tr></table>
					 </details>
			</details>
			<details ng-repeat="list in saleWithDiscountdata | orderBy:'totalDiscountAmount':true | filter:filterDataBeer | filter: greaterThan('totalSaleamount', 0)">
				<summary>
					<div class="stockLiftList">
						<span class="col-xs-4" style="color: #000;margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};" title="">{{list.company}}</span>
						<span class="col-xs-2" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalsaleQty | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="" >{{list.totalSaleamount | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;border-top-right-radius: 5px;border-bottom-right-radius: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalDiscountAmount | INR}}</span>
					</div>
				</summary>
				<details ng-repeat="data in list.inputjson | orderBy:'discountAmount':true | filter: greaterThan('salAmount', 0)">
				   <summary>
					 <span class="col-xs-4 secondSpan" style="color: #000;padding-left: 5px;text-align: left;background-color: #e8e8e8;" title="">{{data.brandname}}</span>
					 <span class="col-xs-2 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.saleQty | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.salAmount | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.discountAmount | INR}}</span>
					 </summary>
					 <table class=" table table-fixed">
					     <tr ng-repeat="brand in data.Brands | orderBy:'totalDiscount':true | filter: greaterThan('totalPrice', 0)">
							<td class="col-xs-2" style="padding: 8px 0px 8px 0px;color: #000;border-top: 0px solid #ddd;" title="{{brand.discountMonth}}">{{brand.discountMonth}}</td>
						     <td class="col-xs-2" style="padding: 8px 0px 8px 0px;color: #000;border-top: 0px solid #ddd;" title="{{brand.quantity}}">{{brand.quantity}}</td>
						     <td class="col-xs-2" style="padding: 8px 0px 8px 0px;text-align: right;color: #000;border-top: 0px solid #ddd;" title="{{brand.saleInCase}}/{{brand.btlVal}}">{{brand.saleInCase}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="padding: 8px 0px 8px 0px;text-align: right;color: #000;border-top: 0px solid #ddd;" title="{{brand.totalPrice}}">{{brand.totalPrice | INR}}</td>
						    <td class="col-xs-3" style="padding: 8px 0px 8px 0px;text-align: right;color: #000;border-top: 0px solid #ddd;" title="{{brand.totalDiscount}}" >{{brand.totalDiscount | INR}}</td>
						</tr></table>
					 </details>
			</details>
	</div>
	
	<div class="saleReportWithCategoryWise">    
         <div class="row" style="border-radius: 5px;color: #062351;font-size: 11px;">
				<span class="col-xs-4" style="margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;padding: 0px 0px 0px 20px;" title="">Category</span>
				<span class="col-xs-2" style="margin-top: 5px;text-align: right;padding: 0px;" title="">Sale</span>
				<span class="col-xs-3" style="margin-top: 5px;text-align: right;padding: 0px;">Sale Amt.</span>
				<span class="col-xs-3" style="border-bottom-right-radius: 5px;border-top-right-radius: 5px;margin-top: 5px;text-align: right;padding: 0px 10px 0px 0px;" title="">Discount</span>
			</div>
		<details ng-repeat="list in saleWithDiscountCategorydata | orderBy:'totalDiscountAmount':true | filter:filterCategoryData | filter: greaterThan('totalSaleamount', 0)">
				<summary>
					<div class="stockLiftList">
						<span class="col-xs-4" style="color: #000;margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};" title="">{{list.category}}</span>
						<span class="col-xs-2" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalsaleQty | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="" >{{list.totalSaleamount | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;border-top-right-radius: 5px;border-bottom-right-radius: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalDiscountAmount | INR}}</span>
					</div>
				</summary>
				<details ng-repeat="data in list.inputjson | orderBy:'discountAmount':true | filter: greaterThan('salAmount', 0)">
				   <summary>
					 <span class="col-xs-4 secondSpan" style="color: #000;padding-left: 5px;text-align: left;background-color: #e8e8e8;" title="">{{data.brandname}}</span>
					<span class="col-xs-2 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.saleQty | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.salAmount | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.discountAmount | INR}}</span>
					 </summary>
					 <table class=" table table-fixed">
					     <tr ng-repeat="brand in data.Brands | orderBy:'totalDiscount':true | filter: greaterThan('totalPrice', 0)">
							<td class="col-xs-2" style="padding: 8px 0px 8px 0px;color: #000;border-top: 0px solid #ddd;" title="{{brand.discountMonth}}">{{brand.discountMonth}}</td>
						    <td class="col-xs-2" style="padding: 8px 0px 8px 0px;color: #000;border-top: 0px solid #ddd;" title="{{brand.quantity}}">{{brand.quantity}}</td>
						    <td class="col-xs-2" style="padding: 8px 0px 8px 0px;text-align: right;color: #000;border-top: 0px solid #ddd;" title="{{brand.saleInCase}}/{{brand.btlVal}}">{{brand.saleInCase}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="padding: 8px 0px 8px 0px;text-align: right;color: #000;border-top: 0px solid #ddd;" title="{{brand.totalPrice}}">{{brand.totalPrice | INR}}</td>
						    <td class="col-xs-3" style="padding: 8px 0px 8px 0px;text-align: right;color: #000;border-top: 0px solid #ddd;" title="{{brand.totalDiscount}}" >{{brand.totalDiscount | INR}}</td>
						</tr></table>
					 </details>
			</details>
			<details ng-repeat="list in saleWithDiscountCategorydata | orderBy:'totalDiscountAmount':true | filter:filterCategoryDataBeer | filter: greaterThan('totalSaleamount', 0)">
				<summary>
					<div class="stockLiftList">
						<span class="col-xs-4" style="color: #000;margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};" title="">{{list.category}}</span>
						<span class="col-xs-2" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalsaleQty | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="" >{{list.totalSaleamount | INR}}</span>
						<span class="col-xs-3" style="color: #000;margin-top: 5px;border-top-right-radius: 5px;border-bottom-right-radius: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.totalDiscountAmount | INR}}</span>
					</div>
				</summary>
				<details ng-repeat="data in list.inputjson | orderBy:'discountAmount':true | filter: greaterThan('salAmount', 0)">
				   <summary>
					 <span class="col-xs-4 secondSpan" style="color: #000;padding-left: 5px;text-align: left;background-color: #e8e8e8;" title="">{{data.brandname}}</span>
					 <span class="col-xs-2 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.saleQty | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.salAmount | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="color: #000;text-align: right;background-color: #e8e8e8;" title="">{{data.discountAmount | INR}}</span>
					 </summary>
					 <table class=" table table-fixed">
					     <tr ng-repeat="brand in data.Brands | orderBy:'totalDiscount':true | filter: greaterThan('totalPrice', 0)">
							<td class="col-xs-2" style="padding: 8px 0px 8px 0px;color: #000;border-top: 0px solid #ddd;" title="{{brand.discountMonth}}">{{brand.discountMonth}}</td>
						     <td class="col-xs-2" style="padding: 8px 0px 8px 0px;color: #000;border-top: 0px solid #ddd;" title="{{brand.quantity}}">{{brand.quantity}}</td>
						     <td class="col-xs-2" style="padding: 8px 0px 8px 0px;text-align: right;color: #000;border-top: 0px solid #ddd;" title="{{brand.saleInCase}}/{{brand.btlVal}}">{{brand.saleInCase}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="padding: 8px 0px 8px 0px;text-align: right;color: #000;border-top: 0px solid #ddd;" title="{{brand.totalPrice}}">{{brand.totalPrice | INR}}</td>
						    <td class="col-xs-3" style="padding: 8px 0px 8px 0px;text-align: right;color: #000;border-top: 0px solid #ddd;" title="{{brand.totalDiscount}}" >{{brand.totalDiscount | INR}}</td>
						</tr></table>
					 </details>
			</details>
	</div>
	
   
</div>
</div>
</body>
</html>