<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-1.12.4.js" var="jquery1124js"/>
  <script src="${jquery1124js}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script>
     <spring:url value="/resources/javascript/thirdpartyjs/angular.min.js" var="angularminjs"/>
  <script src="${angularminjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/angular-filter.js" var="angularfilterjs"/>
  <script src="${angularfilterjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
  <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script>
 <spring:url value="/resources/css/common.css" var="commoncss"/>
   <link rel="stylesheet" href="${commoncss}">
   <spring:url value="/resources/css/bootstrap.min.css" var="bootstrapmincss"/>
   <link rel="stylesheet" href="${bootstrapmincss}">
 <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <spring:url value="/resources/javascript/invoiceReport.js" var="invoiceReportjs"/>
  <script src="${invoiceReportjs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
     <spring:url value="/resources/css/collapseViewCommon.css" var="collapseViewCommoncss"/>
   <link rel="stylesheet" href="${collapseViewCommoncss}">
   <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
 <style>

p
{ /* padding: 0 0 20px 0; */
  line-height: 1.7em;}
.dateinput{
       padding: 10px;
    width: 130px;
    font: 100% arial;
    border: 1px solid #E5E5DB;
    background: #FFF;
    color: #47433F;
    border-radius: 5px;
}
.submit
{ font: 100% arial; 
  border: 0; 
  width: 99px; 
  height: 33px;
  padding: 2px 0 3px 0;
  cursor: pointer; 
  color: #fff;
  background-color: #337ab7;
  border-color: #2e6da4;
  border-radius: 5px;
  }
  
  .totalcls{
    float: right;
    padding: 0px 18px 0px 0px;
    color: #000;
    font-size: 13px;
    font-weight: 600;
    width: 100%;
    text-align: right;
}
</style>
</head>
<body>
 <div id="main" ng-app="myApp" ng-controller="myCtrl">
   
    <nav class="navbar navbar-inverse" class="headernav" style="height:120px;background:#062351;border-radius:0px;">
		<label style="float: right;color: #fff;padding: 10px 10px 0px 0px;">INVOICE DAY WISE REPORT</label>
		<div class="container-fluid" style="padding-right: 200px;">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">
				<img src="resources/images/logopoizin.png" /></a>
			</div>
			<ul class="nav navbar-nav navbar-right">

				<li class="dropdown"><a class="dropbtn"
					style="padding-top: 70px;">Invoice</a>
					<div class="dropdown-content">
						<a id="feature_1" href="addNewBrand">Add New Brand</a> <a
							id="feature_2" href="saveInvoice">Add Invoice</a>

					</div></li>

				<li class="dropdown"><a class="dropbtn"
					style="padding-top: 70px;">Sale</a>
					<div class="dropdown-content">
						<a id="feature_3" href="sale">Enter latest Sale</a> <a
							id="feature_9" href="updateSale">Edit latest Sale</a>
							<a id="feature_20" href="monthlyExpenses">Monthly Debit/Credit</a>
							<a id="feature_20" href="investment">Enter Investment</a>
							<a id="feature_24" href="discountTransaction">Enter Cheque With Details</a>
							<a id="feature_20" href="mobileViewEnterSaleSheet">Generate Sale Sheet</a>

					</div></li>
				<li class="dropdown"><a class="dropbtn"
					style="padding-top: 70px;">Reports</a>
					<div class="dropdown-content">
						<a id="feature_4" href="invoiceDateWiseReport">Invoice Report</a>
						<a id="feature_8" href="saleReport">Sale Report</a>
						 <a id="feature_10" href="saleDayWiseReport">Day Wise Sale Report</a>
						<a id="feature_6" href="stockLifting">Stock Lift</a>
						<!-- <a  id="feature_11" href="noOfCasesSold">No. Of Cases Sold</a> -->
                        <a id="feature_12" href="inHouseStockValue">In House Stock</a>
                       <!--  <a id="feature_13" href="stockPredictions">Stock Prediction</a> -->
                        <a id="feature_14" href="balanceSheet">Balance Sheet</a>
                         <a id="feature_15" href="productComparision">Product Comparision</a>
                        <!--  <a id="feature_16" href="newSaleReport">New Sale Report</a> old-->
                         <!-- <a id="feature_16" href="mrpRateReport">MRP Rate Report</a> -->
                         <!-- <a id="feature_21" href="profitDiscount">Profit Discount Report</a> -->
                         <!-- <a id="feature_23" href="tillDateBalanceSheet">Till Date Balance Sheet</a> -->
                          <a id="feature_22" href="productPerformance">Product Performance</a>
                          <a id="feature_22" href="productComparisionCategoryWise">Product Performance Category Wise</a>
                           <a id="feature_22" href="saleWithDiscount">Sale With Discount</a>
					</div></li>
					<li class="dropdown"><a class="dropbtn"
					style="padding-top: 70px;">Discount</a>
					<div class="dropdown-content">
						<a id="feature_17" href="discountEstimate">Discount Estimate</a>
						 <a id="feature_24" href="productWithInvestment">Indent Estimate</a>
                          <!--  <a id="feature_18" href="retrieveIntent">Accepted Discounts</a> -->
                           <!--  <a id="feature_19" href="stockLiftingWithDiscount">Stock Lift With Discount</a> -->
                            <a id="feature_24" href="tillMonthStockLiftWithDiscount">Company Discount</a>
                           <a id="feature_24" href="stockLiftWithDiscountTransaction"> Stock Lifting With Disc</a>
                            <!--  <a id="feature_24" href="companyWiseDiscount">Company Wise Discount</a> -->
                              <a id="feature_24" href="profitAndLossReport">Profit/Loss Report</a>

					</div></li>
					<li class="dropdown">
                         <c:if test="${sessionScope['scopedTarget.userSession'].loginId > 0}">
						<a class="dropbtn" style="padding-top: 70px;" href="./logout">Logout</a>
					</c:if>
					</li>
			</ul>
		</div>
		</nav>
    <div id="content_header">
    <!-- <select id="myselect" onchange="idate(this.value);" class="selectDatecss">
</select>  -->
<div class="" style="width: 100%;text-align: center;margin-bottom: 10px; margin-top: 10px;font-size: 15px;font-weight: 600;">
 <!-- <label style="float: left;color: #062351;padding: 0px 0px 0px 5px;">INVOICE REPORT</label> -->
	 	            Select Date: <input type="text" class="dateinput" id="startDate" readonly='true'>
			  <input class="submit" type="submit" name="" ng-click="getResults('startDate')" value="Get Data" />
	  </div>
    <div id="invoiceDate">
    </div>
    <div>
    <div class="tab-content ">
               <div class="container">
				 <div class="panel panel-default">
					<div class="stockLift ">
					    <span class="col-xs-3" style="text-align: left;border-top-left-radius: 3px;" data-toggle="tooltip" data-placement="bottom" title="Brand Name">Brand Name</span>
					    <span class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Brand No">Brand No</span>
					    <span class="col-xs-2" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" title="Category">Category
					    </span>
					    <span class="col-xs-2" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" title="Company">Company
					    </span>
					    <span class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Total Case">Total Case</span>
					     <span class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Total Price">Total Price</span>
					</div>
					<div class="datalist">
					<details ng-repeat="list in alldetails | filter: greaterThan('totalPriceVal', 0) | orderBy:['companyOrder','category']">
					<summary>
						<div class="stockLiftList">
						    <span class="col-xs-3" style="text-align: left;background-color: {{list.bgcolor}};" title="{{list.brandname}}">{{list.brandname}}</span>
						    <span class="col-xs-2" style="background-color: {{list.bgcolor}};" title="{{list.brandNo}}">{{list.brandNo}}</span>
						    <span class="col-xs-2" style="background-color: {{list.bgcolor}};" title="{{list.category}}">{{list.category}}</span>
						    <span class="col-xs-2" style="background-color: {{list.bgcolor}};" title="{{list.company}}">{{list.company}}</span>
						    <span class="col-xs-1" style="background-color: {{list.bgcolor}};" title="{{list.caseqty}}">{{list.caseqty | INR}}</span>
						     <span class="col-xs-2" style="background-color: {{list.bgcolor}};" title="{{list.totalPriceVal}}">{{list.totalPriceVal | INR}}</span>
						</div>
					</summary>
					<table class=" table table-fixed">
						   <thead><tr>
						   <th class="col-xs-2" title="Product Type">Product Type</th>
						   <th class="col-xs-1" title="Pack Type">Pack Type</th>
						   <th class="col-xs-1" title="Pack QTY">Pack QTY</th>
						   <th class="col-xs-2" title="QTY">QTY</th>
						   <th class="col-xs-1" title="Unit Rate">Unit Rate</th>
						   <th class="col-xs-1" title="Btl Rate">Btl Rate</th>
						    <th class="col-xs-1" title="CaseQty">CaseQty</th>
						    <th class="col-xs-1" title="BTL QTY">BTL QTY</th>
						    <th class="col-xs-2" title="Total Price">Total Price</th>
						    <!-- <th class="col-xs-1" title="Actual MRP">Actual MRP</th>
						    <th class="col-xs-1" title="MRP Roundup">MRP Roundup</th>
						    <th class="col-xs-1" title="RoundinfOff Value">RoundinfOff Value</th> -->
						    </tr></thead>
						    <tr ng-repeat="brand in list.Brands | filter: greaterThan('TotalPrice', 0)">
							<td class="col-xs-2" title="{{brand.productType}}">{{brand.productType}}</td>
							<td class="col-xs-1" title="{{brand.packType}}">{{brand.packType}}</td>
							<td class="col-xs-1" title="{{brand.packQty}}">{{brand.packQty}}</td>
							<td class="col-xs-2" title="{{brand.QTY}}">{{brand.QTY}}</td>
							<td class="col-xs-1" title="{{brand.UnitRate}}">{{brand.UnitRate | INR}}</td>
							<td class="col-xs-1" title="{{brand.BTLRate}}">{{brand.BTLRate | INR}}</td>
							<td class="col-xs-1" title="{{brand.caseQty}}">{{brand.caseQty}}</td>
							<td class="col-xs-1" title="{{brand.btlqty}}">{{brand.btlqty}}</td>
							<td class="col-xs-2" title="{{brand.TotalPrice}}">{{brand.TotalPrice | INR}}</td>
							<!-- <td class="col-xs-1" title="{{brand.actMrp}}">{{brand.actMrp}}</td>
							<td class="col-xs-1" title="{{brand.mrpRoundOf}}">{{brand.mrpRoundOf}}</td>
							<td class="col-xs-1" title="{{brand.ttlRoundOf}}">{{brand.ttlRoundOf}}</td> -->
							</tr>
						  </table>
					</details>
					</div>
                 </div>
               </div>
			</div>
    
	     <div id="invoiceTotal" class="totalcls"> </div>
	      <div id="mrpRoundingOffTotal" class="totalcls"></div>
	      <div id="netInvoiceValue" class="totalcls"></div>
	       <div id="tcs" class="totalcls"></div>
	         <div id="turnOverTax" class="totalcls"></div>
    </div>
    </div>
    </div>
</body>
</html>