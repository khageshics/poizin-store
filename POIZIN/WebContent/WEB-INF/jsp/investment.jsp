<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
  <jsp:include page="/WEB-INF/jsp/header.jsp" />  
  <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script> 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}"> 
   
  <script>
  $( function() {
	    $( "#bankdate" ).datepicker({ dateFormat: 'yy-mm-dd',
	    	onSelect: function(selected,evnt) {
	            updateAb(selected);
	       }
	    });
	  });
  
  $(document).ready(function() {
		showAlertMsg();
	});
	function showAlertMsg(){
		var msg = $("#popupmsg").val();
		if(msg != ""){
			alert(msg);
		}
		 $('#popupmsg').val("");
	}
</script>
</head>
<body>
<input type="hidden" name="popupmsg" id="popupmsg" value="${message}"  />
<div class="row" style="padding-top: 100px;">
  <div class="col-sm-3 bodyFontCss">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Sale</li>
  <li>Investment</li>
</ul>
  </div>
  <div class="col-sm-9 bodyFontCss"></div>
</div>
<div class="container" style="margin-top: 20px;">
 <div class="row">
	<div class="col-sm-4 bodyFontCss"></div>
	<div class="col-sm-4 bodyFontCss">
	<form action="./addNewInvestment" class="form_span" id="" method="post">
	  <label>Date</label > 
	  <input type="text" id="bankdate" name="bankdate" class="dateinput number-only form-control"  required readonly="readonly">
	  <label>Select Bank</label>
	  <select name="bank" id="bank" required="required" class="inputstylesright form-control ">
		<option value="" rval="">Select Bank</option>
		<option value="AB">AB</option>
		<option value="SBI">SBI</option>
		<option value="KVB">KVB</option>
		<option value="HDFC">HDFC</option>
	  </select>
	  <label>Select Tr. Type</label>
	 <select name="transactionType" id="" required="required" class="form-control">
		<option value="" rval="">Select Tr. Type</option>
	    <option value="Checks">Checks</option>
		<option value="NEFT">NEFT</option>
		<option value="IMPS">IMPS</option>
		<option value="CASH">CASH</option>
	 </select>
	 <label>Amount</label>
	 <input type="number" id="amount" name="amount" class="number-only form-control"  required>
	 <input class="btn btn-default btn-rounded" type="submit" name="" value="SUBMIT" />
	</form>
	</div>
	<div class="col-sm-4 bodyFontCss"></div>
	</div>
</div>
</body>
</html>