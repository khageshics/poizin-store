<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-filter/0.5.4/angular-filter.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <script	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
 <spring:url value="/resources/css/common.css" var="commoncss"/>
   <link rel="stylesheet" href="${commoncss}">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <spring:url value="/resources/javascript/fusioncharts/fusioncharts.js" var="fusionjs" />
<script src="${fusionjs}"></script>
<spring:url value="/resources/javascript/fusioncharts/themes/fusioncharts.theme.fint.js" var="fusionthemejs" />
<script src="${fusionthemejs}"></script>
  <spring:url value="/resources/javascript/retrieveIntent.js" var="retrieveIntentjs"/>
  <script src="${retrieveIntentjs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
 <style>


p
{ /* padding: 0 0 20px 0; */
  line-height: 1.7em;}


#site_content
{ width: 1245px;
  overflow: scroll;
  margin: 20px auto 0 auto;
  padding: 0 0 10px 0;} 

#sidebar_container
{ float: right;
  width: 44%;}



#content
{ text-align: left;
  width: 55%;
  padding: 0 0 0 5px;
  float: left;}
  
#content ul
{ margin: 2px 0 22px 0px;}

#content ul li, .sidebar ul li
{ list-style-type: none;
  background: url("resources/images/bullet.png") no-repeat;
  margin: 0 0 0 0; 
  padding: 0 0 4px 25px;
  line-height: 1.5em;}

.Report_List_user li{
	width: 120px;
	text-align: center;
	border-bottom: 1px solid #D7DEDE;
	height: 32px;
	line-height: 32px;
	font-size: 11px;
	overflow:hidden;
	text-overflow: ellipsis;
	float: left;
	background: #fff;
	cursor: pointer;
}
.Report_Details_user li {
	list-style: none;
	/* width: 14%; */
	width: 120px;
	background-color: #062351;
	text-align: center;
	height: 50px;
	line-height: 50px;
	border-bottom: 2px solid #062351;
	font-weight: bold;
	/* background: -webkit-linear-gradient(#F0F0F0, #FFFFFF, #F0F0F0);
	background: -o-linear-gradient(#F0F0F0, #FFFFFF, #F0F0F0);
	background: -moz-linear-gradient(#F0F0F0, #FFFFFF, #F0F0F0);
	background: linear-gradient(#F0F0F0, #FFFFFF, #F0F0F0);
	 */float: left;
	font-size: 13px;
	    float: left;
    text-overflow: ellipsis;
    overflow: hidden;
    display: inline-block;
    white-space: nowrap;
    font-size: 12px;
    color:#fff;
}
.evenrow{
	background-color:#81DAF5!important;float:left;width:150px;font-family:Verdana,sans-serif!important;font-size:12px;color:#424242;
}

.oddrow{
	background-color:#E6E6E6!important;float:left;width:150px;font-family:Verdana,sans-serif!important;font-size:12px;color:#424242;
}
.buttonbackbg{
	border: 2px solid #000;
    border-radius: 5px;
    padding: 5px;
    font-size: 12px;
    cursor: pointer;
}
.dateinput{
    padding: 10px;
    width: 160px;
    font: 100% arial;
    border: 1px solid #E5E5DB;
    background: #FFF;
    color: #47433F;
    border-radius: 5px;
}
ul {
    list-style: none;
    padding: 0px;
    margin: 0px;
}
 details summary::-webkit-details-marker { display:none; }
.tab-content{
    margin-top: 10px;
}
.customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

.customers td, .customers th {
   /*  border: 1px solid #ddd; */
    padding: 6px;
    text-align: center;
    border-bottom: 1px solid #ddd;
}
.customers th{
    padding-top: 6px;
    padding-bottom: 6px;
    text-align: center;
    background-color: #062351;
    color: white;
}
 .submit
{ font: 100% arial; 
  border: 0; 
  width: 99px; 
  height: 33px;
  padding: 2px 0 3px 0;
  cursor: pointer; 
  color: #fff;
  background-color: #337ab7;
  border-color: #2e6da4;
  border-radius: 5px;
  }
  .khagesh {
    
}

.khagesh .shiva {
    visibility: hidden;
    width: 200px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;

    /* Position the tooltip */
    position: absolute;
    z-index: 1;
}

.khagesh:hover .shiva {
    visibility: visible;
}

.fixed_headers {
    width:1348px;
    table-layout: fixed;
    border-collapse: collapse;
}
.fixed_headers thead tr {
    display: block;
    position: relative;
}
.fixed_headers td:nth-child(1), .fixed_headers th:nth-child(1) {
    min-width: 65px;
}
.fixed_headers td:nth-child(2), .fixed_headers th:nth-child(2) {
    min-width: 98px;
}
.fixed_headers td:nth-child(3), .fixed_headers th:nth-child(3) {
    min-width: 98px;
}
.fixed_headers td:nth-child(4), .fixed_headers th:nth-child(4) {
    min-width: 98px;
}
.fixed_headers td:nth-child(5), .fixed_headers th:nth-child(5) {
    min-width: 98px;
}
.fixed_headers th, .fixed_headers td {
    padding: 5px;
    text-align: center;
    min-width: 80px;
        font-size: 12px;
}
.fixed_headers tbody {
    display: block;
    overflow: auto;
    width: 100%;
}
.form-control {
    display: inline;
    width: auto;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}
</style>
</head>
<body>
 <div id="main" ng-app="myApp" ng-controller="myCtrl">
     <nav class="navbar navbar-inverse" class="headernav" style="height:120px;background:#062351;border-radius:0px;">
		<label style="float: right;color: #fff;padding: 10px 10px 0px 0px;">ADD NEW BRAND</label>
		<div class="container-fluid" style="padding-right: 200px;">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">
				<img src="resources/images/logopoizin.png" /></a>
			</div>
			<ul class="nav navbar-nav navbar-right">

				<li class="dropdown"><a class="dropbtn"
					style="padding-top: 70px;">Invoice</a>
					<div class="dropdown-content">
						<a id="feature_1" href="addNewBrand">Add New Brand</a> <a
							id="feature_2" href="saveInvoice">Add Invoice</a>

					</div></li>

				<li class="dropdown"><a class="dropbtn"
					style="padding-top: 70px;">Sale</a>
					<div class="dropdown-content">
						<a id="feature_3" href="sale">Enter latest Sale</a> <a
							id="feature_9" href="updateSale">Edit latest Sale</a>
							<a id="feature_20" href="monthlyExpenses">Monthly Debit/Credit</a>
							<a id="feature_20" href="investment">Enter Investment</a>

					</div></li>
				<li class="dropdown"><a class="dropbtn"
					style="padding-top: 70px;">Reports</a>
					<div class="dropdown-content">
						<!-- <a id="feature_4" href="invoiceDateWiseReport">Invoice Report</a> -->
						<a id="feature_8" href="saleReport">Sale Report</a>
						 <a id="feature_10" href="saleDayWiseReport">Day Wise Sale Report</a>
						<a id="feature_6" href="stockLifting">Stock Lift</a>
						<!-- <a  id="feature_11" href="noOfCasesSold">No. Of Cases Sold</a> -->
                        <a id="feature_12" href="inHouseStockValue">In House Stock Value</a>
                        <a id="feature_13" href="stockPredictions">Stock Prediction</a>
                        <a id="feature_14" href="balanceSheet">Balance Sheet</a>
                         <a id="feature_15" href="productComparision">Product Comparision</a>
                        <!--  <a id="feature_16" href="newSaleReport">New Sale Report</a> old-->
                         <!-- <a id="feature_16" href="mrpRateReport">MRP Rate Report</a> -->
                         <!-- <a id="feature_21" href="profitDiscount">Profit Discount Report</a> -->
                         <a id="feature_23" href="tillDateBalanceSheet">Till Date Balance Sheet</a>
                          <a id="feature_22" href="productPerformance">Product Performance</a>
                          <a id="feature_22" href="productComparisionCategoryWise">Product Performance Category Wise</a>
                           <a id="feature_22" href="saleWithDiscount">Sale With Discount</a>
					</div></li>
					<li class="dropdown"><a class="dropbtn"
					style="padding-top: 70px;">Discount</a>
					<div class="dropdown-content">
						<a id="feature_17" href="discountEstimate">Discount Estimate</a>
						<a id="feature_24" href="productWithInvestment">Indent Estimate</a>
						 <a id="feature_24" href="newIndentEstimate">New Indent Estimate</a>
                          <!--  <a id="feature_18" href="retrieveIntent">Accepted Discounts</a> -->
                            <!-- <a id="feature_19" href="stockLiftingWithDiscount">Stock Lift With Discount</a> -->
                            <a id="feature_24" href="tillMonthStockLiftWithDiscount">Till Month Stock Lift With Discount</a>
                            <a id="feature_24" href="discountTransaction">Enter Cheque With Details</a>
                            <a id="feature_24" href="stockLiftWithDiscountTransaction"> Stock Lifting With Disc</a>
                             <a id="feature_24" href="companyWiseDiscount">Company Wise Discount</a>
                             <a id="feature_24" href="profitAndLossReport">Profit/Loss Report</a>
					</div></li>
					<li class="dropdown">
                         <c:if test="${sessionScope['scopedTarget.userSession'].loginId > 0}">
						<a class="dropbtn" style="padding-top: 70px;" href="./logout">Logout</a>
					</c:if>
					</li>
			</ul>
		</div>
		</nav>
    <div id="content_header">
  <!--   <label style="float: left;color: #062351;padding: 0px 0px 0px 5px;">ACCEPTED DISCOUNTS</label> -->
    <div class="" style="width: 100%;text-align: center;    margin-top: 10px;font-size: 15px;font-weight: 600;">
	    <select ng-model="modelfilter" class="form-control"  name="sort" style="float: left;margin-right: 5px;">
			<option value>ALL Category</option> 
		    <option ng-repeat="list in categories | unique:'category'" value="{{list.category}}">{{list.category}}</option>
	  </select>
	  <select ng-model="companyfilter" class="form-control" style="float: left;">
		  <option value>ALL Company</option>
		  <option ng-repeat="list in categories | unique:'company' | filter:{category:modelfilter}" value="{{list.company}}">{{list.company}}</option>
	  </select>
	 	           Date: <input type="text" class="dateinput" id="startDate" readonly='true'>
			  <input class="submit" type="submit" name="" ng-click="getResults('startDate','endDate')" value="Get Data" />
	  </div>
	<!--  <div style="float: left;margin: 5px 8px 10px 5px; width: 50%;" class="">
   <div class="col-lg-4">
	<select ng-model="modelfilter" class="form-control"  name="sort">
		 <option value>ALL Category</option> 
	  <option ng-repeat="list in categories | unique:'category'" value="{{list.category}}">{{list.category}}</option>
  
  </select> 
  </div>
  <div class="col-lg-4">
  <select ng-model="companyfilter" class="form-control" >
		  <option value>ALL Company</option>
	  <option ng-repeat="list in categories | unique:'company' | filter:{category:modelfilter}" value="{{list.company}}">{{list.company}}</option>
  
  </select>
  </div>
  
	 </div>   -->
             <div class="tab-content">
				      <ul class="Report_Details_user" style=" width: 1360px; overflow: auto;">
				      		<!-- <li style="width:145px!important;"><span><b>S.No</b></span></li> -->
							<li style="width:425px!important;text-align: left;"><span><b>Brand Name</b></span></li>
							<li style="width:306px!important;"><span><b>Brand Number</b></span></li>
							 <li style="width:306px!important;"><span><b>Category</b></span></li>
							<li style="width:306px!important;"><span><b>Company</b></span></li>
							<!-- <li style="width:184px!important;"><span><b>Sum Of Cases</b></span></li>
							<li style="width:184px!important;"><span><b>Total Amount</b></span></li> -->
						</ul>
						<div id="listData" style="    height: 300px;overflow: scroll;width: 1360px;">
						<ul style="-webkit-padding-start: 0px;">
						 <details ng-repeat="list in alldetails = (categories | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined,}:true) | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined}:true  | orderBy:['companyOrder','category']">
								 <summary>
								 <li  style="list-style: none;line-height: 40px; font-size: 12px;background-color: {{list.bgcolor}};">
								 <p style="width:415px!important;display: inline-block;text-align: left;">{{list.brandname}}</p>
								 <p style="width:306px!important;display: inline-block;text-align: center;">{{list.brandNo}}</p>
								 <p style="width:306px!important;display: inline-block;text-align: center;">{{list.category}}</p>
								 <p style="width:306px!important;display: inline-block;text-align: center;">{{list.company}}</p>
								 <!-- <p style="width:184px!important;display: inline-block;text-align: center;">{{list.totalcases}}</p>
								 <p style="width:184px!important;display: inline-block;text-align: center;">{{list.totalPrice}}</p> -->
								 </summary>
								 </li>
								 <table class="customers fixed_headers" style="width: 1342px;">
								 <tbody>
								 <tr>
								<th>Case Rate</th><th>No Of Cases</th><th>Cash Back On Per Case</th><th>Price To Purches</th><th>Cash Back On Order</th><th>Profit % (Discount)</th><th>L2</th><th>L1</th><th>Q</th><th>P</th><th>N</th><th>D</th><th>C</th><th>X</th><th>G</th><th>Estimate Days</th>
								 </tr>
								 <tr ng-repeat="brand in list.Brands">
								 <td>{{brand.caseRate}}</td><td>{{brand.noOfCases}}</td><td>{{brand.cashBackOnPerCase}}</td><td>{{brand.priceToPurches}}</td><td>{{brand.cashBackOnOrder}}</td><td>{{brand.profitPercentageDiscount}}</td><td>{{brand.l2}}</td><td>{{brand.l1}}</td><td>{{brand.q}}</td><td>{{brand.p}}</td><td>{{brand.n}}</td><td>{{brand.d}}</td><td>{{brand.c}}</td><td>{{brand.x}}</td><td>{{brand.g}}</td><td>{{brand.estimateDays}}</td>
								 </tr>
								 </tbody>
								 </table>
						</details>
						 
						 </ul>
						
						
						
						
				    	</div>
				    </div>
				    <hr style="border-top: 2px solid #000;">
				   

    </div>
    </div>
    
</body>
</html>