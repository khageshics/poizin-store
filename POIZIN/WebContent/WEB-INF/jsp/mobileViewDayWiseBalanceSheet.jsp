<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-1.12.4.js" var="jquery1124js"/>
  <script src="${jquery1124js}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/angular.min.js" var="angularminjs"/>
  <script src="${angularminjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/angular-filter.js" var="angularfilterjs"/>
  <script src="${angularfilterjs}"></script>
 <!--  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
 <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
 
  <link rel="stylesheet" href="/resources/demos/style.css">
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
  <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script>
 <spring:url value="/resources/css/common.css" var="commoncss"/>
   <link rel="stylesheet" href="${commoncss}">
   <spring:url value="/resources/css/bootstrap.min.css" var="bootstrapmincss"/>
   <link rel="stylesheet" href="${bootstrapmincss}">
 <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <spring:url value="/resources/javascript/mobileView/mobileViewDayWiseBalanceSheet.js" var="mobileViewDayWiseBalanceSheetjs"/>
  <script src="${mobileViewDayWiseBalanceSheetjs}"></script> 
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
   <spring:url value="/resources/css/mobileViewDayWiseBalanceSheet.css" var="mobileViewDayWiseBalanceSheetcss"/>
   <link rel="stylesheet" href="${mobileViewDayWiseBalanceSheetcss}">
<style>
.color-green {
  color: green;
}

.color-blue {
  color: blue;
}

.color-red {
  color: red;
}
hr {
    border: 0;
    border-top: 1px solid #eee;
    margin: 5px;
}
p {
    margin: 0 0 5px;
}
.Monday{
background-color: #F2D7D5!important
}
.Tuesday{
background-color: #EBDEF0!important
}
.Wednesday{
background-color: #D4E6F1!important
}
.Thursday{
background-color: #D1F2EB!important
}
.Friday{
background-color: #FCF3CF!important
}
.Saturday{
background-color: #E5E7E9!important
}
.Sunday{
background-color: #ABB2B9!important
}
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<div >
<a href="mobileViewHome" style="text-decoration: none;">
<header style="height: 55px;">
  <h2 style="float:left;padding: 0px 0px 0px 2px;">POIZIN</h2>
  <h6 style="float: right;padding: 8px 2px 0px 0px;">BALANCE SHEET</h6>
</header></a>
<div class="tab-content " style="font-family: unset;font-size: 13px;">
<div class="container">
<div class="row allignmentcss">
    <div class="col-md-2">
	</div>
    <div class="col-md-6" style="margin-bottom: 10px;">
    Start: <input type="text" class="dateinput" id="startDate" readonly='true'>
    End: <input type="text" class="dateinput" id="endDate" readonly='true'>
    <input class="submit" type="submit" name="" ng-click="getResults('startDate','endDate')" value="Get" />
    </div>
	<!-- <div class="col-md-4" style="float: right;">
	</div> -->
	<div class="col-md-4" style="float: left;font-weight: 600;color: #062351;">
	Difference(After removing breakage): {{diff | INR}}
	</div>
</div>

<div class="w3-container">
  <div ng-repeat="list in balancesheet" class="w3-card-4" style="margin-top: 10px;">
    <header class="w3-container w3-light-grey" ng-class="{'Monday': list.day === 'Monday', 'Tuesday': list.day === 'Tuesday', 'Wednesday': list.day === 'Wednesday','Thursday': list.day === 'Thursday', 'Friday': list.day === 'Friday', 'Saturday': list.day === 'Saturday', 'list.day': Sunday === 'Sunday'}">
      <h6 style="text-align: left;float: left;display: inline-block;">{{list.day}}</h6><h6 style="text-align: right;float: right;display: inline-block;">{{list.date}}</h6> 
    </header>
    <div class="w3-container">
      <p style="text-align: left;display: inline-block;">Sale: </p><p style="text-align: right;float: right;display: inline-block;"> +{{list.totalPrice | INR}}</p><br>
      <p  style="text-align: left;display: inline-block;">Cash:  </p><p style="text-align: right;float: right;display: inline-block;">-{{list.cashSale | INR}}</p><br>
      <p  style="text-align: left;display: inline-block;">Card: </p><p style="text-align: right;float: right;display: inline-block;"> -{{list.cradSale | INR}}</p><br>
      <p  style="text-align: left;display: inline-block;">Cheque: </p><p style="text-align: right;float: right;display: inline-block;"> -{{list.chequeSale | INR}}</p><br>
      <p  style="text-align: left;display: inline-block;">Expense: </p><p style="text-align: right;float: right;display: inline-block;" data-toggle="modal" data-target="#myModal" ng-click="getExpenseData(list.expenseMasterId,list.date)"> -{{list.totalAmount | INR}}</p>
      <hr>
      <p  style="text-align: left;display: inline-block;">Difference: </p><p ng-class="{'color-green': list.retention > 0,'color-red': list.retention < 0}" style="text-align: right;float: right;display: inline-block;">{{list.retention}}</p>
    </div>
  </div>
  
   
</div>
  
</div>
</div>
</div>


<div class="modal fade sbi2-popup-styles in" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">�</button>
          <!--  <h4 class="modal-title" id="edate">Selected Date: 2018-07-15</h4>  -->
        </div>
        <div class="modal-body">
        <div id="expenseList">
        <table id="customers">
        <tbody>
        <tr><th>Expense Name</th>
        <th>Amount</th>
        <th>Comment</th>
        </tr>
        <tr ng-repeat="data in expensesData">
        <td>{{data.name}}</td>
        <td>{{data.expenseChildAmount}}</td>
        <td>{{data.comment}}</td>
        </tr>
        </tbody>
        </table>
        </div>
        </div>
       
      </div>
    </div>
  </div>
</body>
</html>