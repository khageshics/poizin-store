<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<%-- <spring:url value="/resources/css/3.3.7.bootstrap.min.css" var="bootstrap1mincss"/>
   <link rel="stylesheet" href="${bootstrap1mincss}"> --%>
 <jsp:include page="/WEB-INF/jsp/header.jsp" />  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script> 
 <!--  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
 <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
 
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script>
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script>
 <spring:url value="/resources/javascript/stockLiftWithDiscountTransaction.js" var="stockLiftWithDiscountTransactionjs"/>
  <script src="${stockLiftWithDiscountTransactionjs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}"> 
  <spring:url value="/resources/css/balanceSheet.css" var="balanceSheetcss"/>
   <link rel="stylesheet" href="${balanceSheetcss}">
  
   <style>
   .container{
   min-width: 100%;
   }
.ui-datepicker-calendar {
    display: none;
    }
    .ui-datepicker select.ui-datepicker-month {
    width: 44%;
    float: left;
    margin-right: 10px;
}
.dateinput{
    padding: 8px;
    width: 160px;
    font: 100% arial;
    border: 1px solid #E5E5DB;
    background: #FFF;
    color: #47433F;
    border-radius: 5px;
}

.tab-content{
    margin-top: 10px;
}
 
  input,textarea{
   padding: 0.5px;
    width: 90px;
    font: 100% arial;
    border: solid 1px #e8e8e8;
    background: #FFF;
    color: #47433F;
    border-radius: 5px;
    vertical-align: middle;
    }
    
    
    td.tip {
    /* border-bottom: 1px dashed; */
    text-decoration: none;
    list-style-type: none;
}
td.tip:hover {
   /*  cursor: help; */
    position: relative;
}
td.tip ul {
    display: none
}
td.tip ul > li {
        line-height: 20px;
        list-style-type: none; 
         text-align: left;
}
td.tip:hover ul {
     border: #c0c0c0 1px dotted;
    padding: 5px 20px 5px 5px;
    display: block;
    z-index: 100;
    left: 0px;
    margin: 10px;
    width: 85px;
    position: absolute;
    top: 10px;
    text-decoration: none;
    background: #243a51;
    color: #fff;
    
}
.filter-icon {
    color: #4285f4!important;
    font-size: 20px;
    float: right;
    margin-right: 24px;
}
.dropdown-item:hover {
background-color: #243a51 !important;
}
 span.tip {
    /* border-bottom: 1px dashed; */
    text-decoration: none;
    list-style-type: none;
}
span.tip:hover {
   /*  cursor: help; */
    position: relative;
}
span.tip ul {
    display: none
}
span.tip ul > li {
        line-height: 20px;
        list-style-type: none; 
         text-align: left;
}
span.tip:hover ul {
     border: #c0c0c0 1px dotted;
     padding: 5px 20px 5px 5px;
     display: block;
     z-index: 100;
     left: -50px;
     margin: 10px;
     width: 170px;
     position: absolute;
     top: 10px;
     text-decoration: none;
     background: #243a51;
     color: #fff;
     font-family:  'Montserrat', sans-serif !important;
     font-size: 10px;
}


    </style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
 <div class="container" style="padding-top: 100px;">
 <div class="row" style="margin-bottom: 20px;">
 <div class="col-sm-3 bodyFontCss" style="padding: 0px !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Discount</li>
  <li>Enter Discount</li>
</ul>
  </div>
 <div class="col-sm-2 bodyFontCss customizedButton">
 <span id="setDateValueID" style=" font-weight: 600;color: #062351;"></span>
 </div>
 <div class="col-sm-2 bodyFontCss">
 <label>Select Month</label>
 <input name="startDate" id="startDate" class="date-picker form-control" />
 </div>
 <div class="col-sm-1 bodyFontCss customizedButton">
 <input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getResults('startDate','endDate')" value="GET DATA" />
 </div>
 <div class="col-sm-2 bodyFontCss customizedButton">
 </div>
 <div class="col-sm-1 bodyFontCss">
 </div>
 <div class="col-sm-1 bodyFontCss customizedButton">
 <div class="dropdown" style="float:left;width: 50%;font-size: 15px;padding: 3px;">
  <div class="" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    <span class="glyphicon glyphicon-menu-hamburger"></span>
  </div>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" data-toggle="modal" ng-click="openVendorDiscountModelPopup()" style="font-size: 12px;color: #000;">Vend Disc</a>
    <a class="dropdown-item" data-toggle="modal" ng-click="openRentalModelPopup()" style="font-size: 12px;color: #000;">Add Rental</a>
    <a class="dropdown-item" data-toggle="modal" ng-click="openBreakageModelPopup()" style="font-size: 12px;color: #000;">Add Breakage</a>
  </div>
</div>
 <div style="float:left;width: 50%;">
<i class="fa fa-filter filter-icon" style="color: #243a51!important;" data-toggle="modal" data-target="#saleFilterModal" aria-hidden="true"></i>
 </div>
 </div>
 
 </div>
  <div class="row">
 <div class="col-sm-12 bodyFontCss">
 <input type="hidden" name="cpyname" id="cpyname" value="{{companyfilter}}" />
 <table class="table table-fixed" id="discounttab">
		          <thead>
		            <tr>
		              <th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Brand Name">Brand Name</th>
		              <th class="col-xs-1"  data-toggle="tooltip" data-placement="bottom" title="Company">Company</th> 
		              <th class="col-xs-1"  data-toggle="tooltip" data-placement="bottom" title="Brand No">Brand No</th> 
		              <th class="col-xs-1"  data-toggle="tooltip" data-placement="bottom" title="Case Rate">Case Rate</th> 
		              <th class="col-xs-1 " data-toggle="tooltip" data-placement="bottom" title="Current Month Stock">In-House Stock</th>
		              <th class="col-xs-1 " data-toggle="tooltip" data-placement="bottom" title="Lifted Cases">Lifted Cases</th>
		              <th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="adjustment">Adjustment</th> 
		              <th class="col-xs-1 " data-toggle="tooltip" data-placement="bottom" title="(LiftedCases + adjustment)">After Adj Cases</th>
		              <th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Target">Target</th>
		              <th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="(Target - Lifted Cases)">Pending</th>
		              <th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Discount Amount" ng-click="sort('discount','category')">Disc/Case
		              <label class="glyphicon sort-icon" style="color:#fff !important;display: initial;" ng-show="sortKey=='discount,category'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
		              </th>
		              <th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="((Lifted Cases + Adjustment) * DiscountAmount)">Total Disc</th>
		            </tr>
		          </thead>
		          <tbody> 
		          <tr ng-repeat="list in alldetails = (categories | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined,}:true) | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined}:true | filter: greaterThan('received', 0) | orderBy:sortKey:reverse">
							<td class="col-xs-1" style="border-bottom: solid 1px #e8e8e8;background: linear-gradient(to right, {{list.color}} 2%, white 2%);" >{{list.brandName}}</td>
							<td class="col-xs-1" style="border-bottom: solid 1px #e8e8e8;">{{list.company}}</td>
							<td class="col-xs-1" style="border-bottom: solid 1px #e8e8e8;">{{list.brandNo}}</td>
							<td class="col-xs-1 tip" style="border-bottom: solid 1px #e8e8e8;overflow: visible;">{{list.caseRate}}
							<ul>
							<li ng-repeat="xval in list.caseRateList">{{xval.label}} : {{xval.value}}</li>
							</ul>
							</td>
							<td class="col-xs-1" style="border-bottom: solid 1px #e8e8e8;">{{list.inHouseStock}}</td>
							<td class="col-xs-1" style="border-bottom: solid 1px #e8e8e8;">{{list.liftedCase}}</td>
							<td class="col-xs-1" style="border-bottom: solid 1px #e8e8e8;"><input type="number" id="adjustment{{list.brandNo}}" ng-model="adjustment" ng-change="afterADJ(adjustment,{{list.brandNo}},{{list.liftedCase}})"  value="{{list.adjustment}}"></input></td>
							<td class="col-xs-1" style="border-bottom: solid 1px #e8e8e8;" id="ttlcases{{list.brandNo}}">{{list.liftedAndAdjCase}}</td>
							<td class="col-xs-1" style="border-bottom: solid 1px #e8e8e8;"><input type="number" id="target{{list.brandNo}}" ng-model="target" ng-change="afterTarget(target,{{list.brandNo}},{{list.liftedCase}})"  value="{{list.targetCase}}"></input></td>
							<td class="col-xs-1" style="border-bottom: solid 1px #e8e8e8;" id="pending{{list.brandNo}}">{{list.pending}}</td>
							<td class="col-xs-1" style="border-bottom: solid 1px #e8e8e8;overflow: visible;padding: 7px;"><input type="number" id="damount{{list.brandNo}}"  ng-model="test" ng-change="onChnage(test,{{list.brandNo}})"  value="{{list.discount}}" style="width: 60px;"></input>
							<span style="font-size: 8px;vertical-align: super;" class="glyphicon glyphicon-info-sign tip">
                            <ul>
							<li>Max : {{list.maxMinDiscountBean[0].maxDisc}} / {{list.maxMinDiscountBean[0].maxDiscCase}} ({{list.maxMinDiscountBean[0].maxDiscDate}})</li>
							<li>Min : {{list.maxMinDiscountBean[0].minDisc}} / {{list.maxMinDiscountBean[0].minDiscCase}} ({{list.maxMinDiscountBean[0].minDiscDate}})</li>
							<li>Last Month : {{list.maxMinDiscountBean[0].prevoiusDisc}} / {{list.maxMinDiscountBean[0].previousDiscCase}} ({{list.maxMinDiscountBean[0].previousDiscDate}})</li>
							</ul>
                            </span>
							</td>
							<td class="col-xs-1" style="border-bottom: solid 1px #e8e8e8;background: linear-gradient(to left, {{list.categoryColor}} 2%, white 2%);" id="ttldiscoutamount{{list.brandNo}}">{{list.totalDiscount}}</td>
							<td style="display: none;">{{list.comapnyId}}</td>
							</tr>
		          </tbody>
		        </table>
 </div>
 </div>
 <div class="row">
 <div class="col-sm-5 bodyFontCss"></div>
  <div class="col-sm-2 bodyFontCss">
  <input id="submitbtn" class="btn btn-default btn-rounded" style="width: 55%;" type="submit" name="" ng-click="savediscountDetails()" value="SAVE" />
  </div>
  <div class="col-sm-5 bodyFontCss">
 <span style=" float: right;padding: 0px 5px 5px 5px;font-size: 14px;color: #de7676;font-weight: 600;"> Total Discount: <span id="discountamountid"> {{alldetails | sumOfDiscount}} </span></span>
 </div>
 </div>
 </div>
 <!-- Modal -->
<div class="modal fade right" id="saleFilterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-side modal-top-right" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title w-100" id="exampleModalLabel"><i class="fa fa-filter" style="color: #243a51!important;font-size: 22px;"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="font-size: 25px;">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
		<select ng-model="modelfilter" class="form-control"  name="sort" style="float: left;width: 48%; margin-right: 10px;">
		  <option value>ALL Category</option> 
		  <option ng-repeat="list in categories | unique:'category'" value="{{list.category}}">{{list.category}}</option>
	  </select> 
	  <select ng-model="companyfilter" class="form-control" style="width: 48%;">
		<option value>ALL Company</option>
		<option ng-repeat="list in categories | unique:'company' | filter:{category:modelfilter}" value="{{list.company}}">{{list.company}}</option>
	  </select>
	 </div>
      <div class="modal-footer">
       <!--  <button type="button" style="background: #1a6398!important;" class="btn btn-primary btn-rounded" data-dismiss="modal">Apply Filters</button> -->
      </div>
    </div>
  </div>
</div>
<!-- update vendor discount start -->
<div class="modal" id="updateVendorDiscount" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
         <h4>Vendor Discount ({{vendorShowDate}})</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
         <form ng-submit="saveVendorDiscount()" class="form_span" method="post">
         <div class="col-sm-6">
         <label style="font-size: 13px;font-weight: normal;">Select Company</label>
	      <select id="listdataSec" ng-model="vendorcompanyId" name="vendorcompanyId" required="required" class="form-control" required ng-change="getselectedVendorCompany()">
	      <option  value>Select Company</option>
	      <option ng-repeat="list in categories | unique:'company'" value="{{list.company}}">{{list.company}}</option>
	      </select>
           </div>
          <div class="col-sm-6">
          <label style="font-size: 13px;font-weight: normal;">Discount Amount</label>
          <input type="number" class=" form-control" name="vendorAmount" ng-model="vendorAmount" id="vendorAmount" ng-change="textChanged()" required>
          </div>
          <div class="col-sm-12">
          <label style="font-size: 13px;font-weight: normal;">Comment</label>
          <textarea name="vendorcomment" ng-model="vendorcomment" class=" form-control" id="vendorcomment" maxlength="450" required></textarea>
          </div>
          <div style="text-align: center;">
           <input class="btn btn-default btn-rounded" type="submit" name="" value="SUBMIT"/>
          </div>
          <div class="col-sm-12">
		 <table class="table">
		    <thead>
		      <tr>
		        <th style="font-size: 12px;font-weight: 600;">Company</th>
		        <th style="font-size: 12px;font-weight: 600;">Current Arrears</th>
		        <th style="font-size: 12px;font-weight: 600;">Previous Discount</th>
		        <th style="font-size: 12px;font-weight: 600;">New Arrears</th>
		      </tr>
		    </thead>
		    <tbody>
		      <tr>
		        <td style="font-size: 12px;">{{arrearsdetails.companyName}}</td>
		        <td style="font-size: 12px;">{{arrearsdetails.arrears}}</td>
		        <td style="font-size: 12px;">{{arrearsdetails.vendorAmt}}</td>
		        <td style="font-size: 12px;">{{afterAdj}}</td>
		      </tr>
		      </tbody>
	      </table>
	 </div>
        </form>
        </div>
      </div>
    </div>
  </div>


<!-- update vendor discount end -->
<!-- Add or update breakage and rental start -->
<div class="modal" id="addRentalPopup" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
         <h4>Rental ({{rentalShowDate}})</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
		 <form ng-submit="saveOrUpdateRentalAmount()" class="form_span" method="post">
		 <label style="font-size: 13px;font-weight: normal;">Select Company</label>
	      <select id="rentalcompanyId" ng-model="rentalcompanyId" name="rentalcompanyId" class="form-control" ng-change="getselectedCompanyForRental()" required>
	      <option  value>Select Company</option>
	      <option ng-repeat="list in categories | unique:'company'" value="{{list.company}}">{{list.company}}</option>
	      </select><br>
	      <label style="font-size: 13px;font-weight: normal;">Rental Amount</label>
	      <input type="number" class=" form-control" name="rentalAmount" ng-model="rentalAmount" id="rentalAmount" required autocomplete="off"><br>
	      <div style="text-align: center;">
	      <input class="btn btn-default btn-rounded" type="submit" name="" value="SUBMIT"/>
	      </div>
		  </form>
        </div>
      </div>
    </div>
  </div>

<div class="modal" id="addBreakagePopup" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
         <h4>Breakage ({{breakageShowDate}})</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
		  <form ng-submit="saveOrUpdateBreakageAmount()" class="form_span" method="post">
		  <label style="font-size: 13px;font-weight: normal;">Breakage Amount</label>
	      <input type="number" class=" form-control" name="breakageAmount" ng-model="breakageAmount" id="breakageAmount" required autocomplete="off"><br>
	      <label style="font-size: 13px;font-weight: normal;">Comment</label>
	      <textarea name="breakagecomment" ng-model="breakagecomment" class=" form-control" id="breakagecomment" maxlength="440"></textarea>
	      <div style="text-align: center;">
	      <input class="btn btn-default btn-rounded" type="submit" name="" value="SUBMIT"/>
	      </div>
		  </form>
        
        </div>
      </div>
    </div>
  </div>
<!-- Add or update breakage and rental end -->
</body>
</html>