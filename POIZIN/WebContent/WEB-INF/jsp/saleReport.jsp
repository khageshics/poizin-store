<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<%-- <spring:url value="/resources/css/3.3.7.bootstrap.min.css" var="bootstrap1mincss"/>
   <link rel="stylesheet" href="${bootstrap1mincss}"> --%>
   
<jsp:include page="/WEB-INF/jsp/header.jsp" />  
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
<%--    <spring:url value="/resources/javascript/thirdpartyjs/moment.min.js" var="momentminjs"/>
  <script src="${momentminjs}"></script> --%>
  
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script> 
  <spring:url value="/resources/javascript/thirdpartyjs/canvasjs.min.js" var="canvasjsminjs"/>
   <script src="${canvasjsminjs}"></script>
  <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 06-->
  <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
   
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script>
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
<spring:url value="/resources/javascript/fusioncharts/fusioncharts.js" var="fusionjs" />
<script src="${fusionjs}"></script> 
<spring:url value="/resources/javascript/fusioncharts/themes/fusioncharts.theme.fint.js" var="fusionthemejs" />
<script src="${fusionthemejs}"></script> 
  <spring:url value="/resources/javascript/saleReport.js" var="saleReportjs"/>
  <script src="${saleReportjs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}"> 
   <spring:url value="/resources/css/saleReport.css" var="saleReportcss"/>
   <link rel="stylesheet" href="${saleReportcss}">
  <!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"> -->
  <!--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/js/bootstrap-select.js"></script> 06-->
   <spring:url value="/resources/css/bootstrap-select.css" var="bootstrapselectcss"/>
   <link rel="stylesheet" href="${bootstrapselectcss}">
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap-select.js" var="bootstrapselectjs"/>
  <script src="${bootstrapselectjs}"></script>
 <style>
.container{
max-width: 100%;
}
summary{
display : block;
}
.shiva :hover{
background-color: #ccc;
}
a{
color: #fff;
}
label{
color:#000 !important;
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 15px;
}
.mdb-skin .btn-default.dropdown-toggle {
    background-color: #0b2a41!important;
       border-radius: 5px;
}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    width: 100% !important;
}
.mdb-skin .btn-default.dropdown-toggle:focus, .mdb-skin .btn-default.dropdown-toggle:hover {
    background-color: #0b2a41!important;
}
.dropdown-toggle{
padding: 4px 5px 4px 5px;
}
span.tip {
    /* border-bottom: 1px dashed; */
    text-decoration: none;
    list-style-type: none;
}
span.tip:hover {
   /*  cursor: help; */
    position: relative
}
span.tip ul {
    display: none
}
span.tip ul > li {
        line-height: 20px;
        list-style-type: none; 
         text-align: left;
}
span.tip:hover ul {
     border: #c0c0c0 1px dotted;
    padding: 5px 20px 5px 5px;
    display: block;
    z-index: 100;
    left: 0px;
    margin: 10px;
    width: 85px;
    position: absolute;
    top: 10px;
    text-decoration: none;
    background: #243a51;
    color: #fff;
    
}

</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<div class="row" style="padding-top: 100px;">
  <div class="col-sm-3 bodyFontCss" style="padding: 0px !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Reports</li>
  <li>Sale Report</li>
</ul>
  </div>
  <div class="col-sm-1 bodyFontCss"></div>
   <div class="col-sm-2 bodyFontCss">
   <label>Start Date</label>
  <input type="text" class="form-control" id="startDate" readonly='true'>
   </div>
   <div class="col-sm-2 bodyFontCss">
   <label>End Date</label>
   <input type="text" class="form-control" id="endDate" readonly='true'>
   </div>
   <div class="col-sm-1 bodyFontCss  customizedButton">
   <input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getResults('startDate','endDate')" value="Get Data" />
   </div>
  <div class="col-sm-2 bodyFontCss"> </div>
  <div class="col-sm-1 bodyFontCss">
  <span><i class="fa fa-filter filter-icon" style="color: #243a51!important;" data-toggle="modal" data-target="#saleFilterModal" aria-hidden="true"></i></span>
  </div>
  </div>
 <div class="container" style="margin-top: 20px;">
  <div class="row" style="margin-bottom: 20px;">
  <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
  
    <div class="col-sm-12 bodyFontCss">
					<div class="stockLift ">
					    <span class="col-xs-2" style="text-align: left;border-top-left-radius: 3px;" data-toggle="tooltip" data-placement="bottom" title="Brand Name" ng-click="sort('brandname','brandname')">Brand Name
					    <label class="glyphicon sort-icon" style="color:#fff !important;" ng-show="sortKey=='brandname,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					    <span class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Brand No" ng-click="sort('brandNo','brandname')">Brand No
					    <label class="glyphicon sort-icon" style="color:#fff !important;" ng-show="sortKey=='brandNo,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					    <span class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Brand No" ng-click="sort('companyOrder','brandname')">Company
					    <label class="glyphicon sort-icon" style="color:#fff !important;" ng-show="sortKey=='companyOrder,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					    <span class="col-xs-1" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" title="Category" ng-click="sort('categoryOrder','brandname')">Category
					     <label class="glyphicon sort-icon" style="color:#fff !important;" ng-show="sortKey=='categoryOrder,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					    <span class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Total Sale (Case)" ng-click="sort('cases','brandname')">Total Sale (Case)
					    <label class="glyphicon sort-icon" style="color:#fff !important;" ng-show="sortKey=='cases,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					    <span class="col-xs-2" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" title="Total Amount" ng-click="sort('totalval','brandname')">Total Amount
					    <label class="glyphicon sort-icon" style="color:#fff !important;" ng-show="sortKey=='totalval,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					    <span class="col-xs-2" style="border-top-right-radius: 3px;" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" title="Based on end date" ng-click="sort('caseClosing','brandname')">InHouseStock(Cases)
					    <label class="glyphicon sort-icon" style="color:#fff !important;" ng-show="sortKey=='caseClosing,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					</div>
					<div class="datalist onlySaleItmes">
					<h4 ng-if="(alldetails | sumItens) <=0" style="text-align: center;padding: 8px;">No Data Available</h4>
					
					<details ng-repeat="list in allgreterthanzerodetails = (categories | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined}:true) | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined}:true | filter: greaterThan('totalbtlsale', 0) | orderBy:sortKey:reverse">
					<summary>
						<div class="stockLiftList shiva" style="height:36px;border-right: 5px solid {{list.categoryColor}};border-bottom: 1px solid #e0e0e0;border-left:  5px solid {{list.bgcolor}};">
						    <div class="col-xs-2" ng-if="list.compareNoSuper == 0">
						    <span style="color: #000;text-align: left;display: inline-flex;">
						         <div ng-show="list.compairSaleSuper >= -20 &&  list.compairSaleSuper <= 20">
							     <p class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{list.preCases}}, Current Day Sold: {{list.currCases}}"></p>
							     </div>
						         {{list.brandname}}
							     <div ng-show="list.compareCurrentSale > list.comparePreviousDaySale">
							     <p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							     </div>
							     <div ng-show="list.compareCurrentSale < list.comparePreviousDaySale">
							     <p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							     </div>
							     <div ng-show="list.compareCurrentSale == list.comparePreviousDaySale">
							     <p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							     </div>
						    </span>
						    </div>
						    <div class="col-xs-2" ng-if="list.compareNoSuper != 0">
						    <span style="color: #000;text-align: left;" title="{{list.brandname}}">{{list.brandname}}</span>
						    </div>
						    <span class="col-xs-1" style="color: #000;" title="{{list.brandNo}}">{{list.brandNo}}</span>
						    <span class="col-xs-2" style="color: #000;" title="{{list.company}}">{{list.company}}</span>
						    <span class="col-xs-1" style="color: #000;" title="{{list.category}}">{{list.category}}</span>
						    <span class="col-xs-2" style="color: #000;" title="{{list.cases | INR}}">{{list.cases | INR}}</span>
						    <span class="col-xs-2" style="color: #000;" title="{{list.totalval | INR}}">{{list.totalval | INR}}</span>
						    <span class="col-xs-2 tip" style="overflow: visible;" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.caseClosing | INR}}
						     <ul>
								<li ng-repeat="qty in list.Brands | orderBy:'order'">{{qty.packType}} : {{qty.closingCase}} / {{qty.BtlStock}}</li>
							   </ul>
						    </span>
						</div>
					</summary>
					<table class=" table table-fixed">
						   <thead><tr>
						    <th class="col-xs-3" title="Quantity">Quantity</th>
						    <th class="col-xs-3" title="Sale">Sale</th>
						    <th class="col-xs-3" title="Total Cost">Total Cost</th>
						    <th class="col-xs-2" title="In House Stock">In House Stock</th>
						    <th class="col-xs-1" title="">&nbsp;</th>
						    </tr></thead>
						    <tr ng-repeat="brand in list.Brands | filter: greaterThan('totalSale', 0) | orderBy:['category','company'] ">
							<td class="col-xs-3" style="color: #000;" ng-if="brand.compareNo == 0" title="MRP {{brand.shopRate}}">
								<div ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" style="display: inline;">
								   <p class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;    padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.saleInCase}}/{{brand.btlVal}}"></p>
							    </div>
							     {{brand.quantity}}
								<div ng-if="brand.compareNo == 0 && brand.prevoiusDayTotalSale < brand.totalSale" style="display: inline;">
								    <p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
								</div>
								<div ng-if="brand.compareNo == 0 && brand.prevoiusDayTotalSale > brand.totalSale" style="display: inline;">
								   <p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
								</div>
								<div ng-if="brand.compareNo == 0 && brand.prevoiusDayTotalSale == brand.totalSale" style="display: inline;">
								    <p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
								</div>
							</td>
							<td class="col-xs-3" style="color: #000;" ng-if="brand.compareNo != 0" title="MRP {{brand.shopRate}}">{{brand.quantity}}</td>
							<td class="col-xs-3" style="color: #000;" title="{{brand.saleInCase}}/{{brand.btlVal}}">{{brand.saleInCase}}/{{brand.btlVal}}</td>
							<td class="col-xs-3" style="color: #000;" title="{{brand.totalPrice | INR}}">{{brand.totalPrice | INR}}</td>
							<td class="col-xs-2" title="{{brand.closingCase}}/{{brand.BtlStock}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.closingCase}}/{{brand.BtlStock}}</td>
							<td class="col-xs-1" data-toggle="modal" data-target="#basicExampleModal" ng-click="openModalPopup(brand.brandNoPackQty,list.brandname,brand.quantity,brand.categoryId)"><i class="fa fa-line-chart"  style="color: #000; size: 12px; font-weight: bold;"></i></td>
							</tr>
						  </table>
					</details>
					</div>
					<div class="datalist totalSaleItmes">
					<details ng-repeat="list in alldetails = (categories | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined,}:true) | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined}:true |orderBy:sortKey:reverse">
					<summary>
						<div class="stockLiftList shiva" style="height:36px;border-right: 5px solid {{list.categoryColor}};border-bottom: 1px solid #e0e0e0;border-left:  5px solid {{list.bgcolor}};">
						<div class="col-xs-2" ng-if="list.compareNoSuper == 0">
						    <span style="color: #000;text-align: left;display: inline-flex;">
						         <div  ng-show="list.compairSaleSuper >= -20 &&  list.compairSaleSuper <= 20">
							     <p class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{list.preCases}}, Current Day Sold: {{list.currCases}}"></p>
							     </div>
						         {{list.brandname}}
							     <div  ng-show="list.compareCurrentSale > list.comparePreviousDaySale">
							     <p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							     </div>
							     <div  ng-show="list.compareCurrentSale < list.comparePreviousDaySale">
							     <p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							     </div>
							     <div  ng-show="list.compareCurrentSale == list.comparePreviousDaySale">
							     <p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							     </div>
						    </span>
						   </div>
						   <div class="col-xs-2" ng-if="list.compareNoSuper != 0">
						   <span style="color: #000;text-align: left;" title="{{list.brandname}}">{{list.brandname}}
						    </span>
						    </div>
						    <span class="col-xs-1" title="{{list.brandNo}}">{{list.brandNo}}</span>
						     <span class="col-xs-2" title="{{list.company}}">{{list.company}}</span>
						    <span class="col-xs-1" title="{{list.category}}">{{list.category}}</span>
						    <span class="col-xs-2" title="{{list.cases | INR}}">{{list.cases | INR}}</span>
						    <span class="col-xs-2" title="{{list.totalval | INR}}">{{list.totalval | INR}}</span>
						    <span class="col-xs-2 tip" style="overflow: visible;" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.caseClosing | INR}}
						    <ul>
								<li ng-repeat="qty in list.Brands | orderBy:'order'">{{qty.packType}} : {{qty.closingCase}} / {{qty.BtlStock}}</li>
							   </ul>
						    </span>
						</div>
					</summary>
					<table class=" table table-fixed">
						   <thead><tr>
						    <th class="col-xs-3" title="Quantity">Quantity</th>
						    <th class="col-xs-3" title="Sale">Sale</th>
						    <th class="col-xs-3" title="Total Cost">Total Cost</th>
						     <th class="col-xs-2" title="In House Stock">In House Stock</th>
						     <th class="col-xs-1" title="">&nbsp;</th>
						    </tr></thead>
						    <tr ng-repeat="brand in list.Brands  | orderBy:['category','company']">
								<td class="col-xs-3" style="color: #000;" ng-if="brand.compareNo == 0" title="MRP {{brand.shopRate}}">
								<div ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" style="display: inline;">
									    <p class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.saleInCase}}/{{brand.btlVal}}"></p>
							    </div>
							     {{brand.quantity}}
								<div ng-if="brand.compareNo == 0 && brand.prevoiusDayTotalSale < brand.totalSale" style="display: inline;">
								    <p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
								</div>
								<div ng-if="brand.compareNo == 0 && brand.prevoiusDayTotalSale > brand.totalSale" style="display: inline;">
								   <p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
								</div>
								<div ng-if="brand.compareNo == 0 && brand.prevoiusDayTotalSale == brand.totalSale" style="display: inline;">
								    <p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
								</div></td>
							<td class="col-xs-3" style="color: #000;" ng-if="brand.compareNo != 0" title="MRP {{brand.shopRate}}">{{brand.quantity}}</td>
							<td class="col-xs-3" title="{{brand.saleInCase}}/{{brand.btlVal}}">{{brand.saleInCase}}/{{brand.btlVal}}</td>
							<td class="col-xs-3" title="{{brand.totalPrice | INR}}">{{brand.totalPrice | INR}}</td>
							<td class="col-xs-2" title="{{brand.closingCase}}/{{brand.BtlStock}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.closingCase}}/{{brand.BtlStock}}</td>
							<td class="col-xs-1" data-toggle="modal" data-target="#basicExampleModal" ng-click="openModalPopup(brand.brandNoPackQty,list.brandname,brand.quantity,brand.categoryId)"><i class="fa fa-line-chart"  style="color: #000; size: 12px; font-weight: bold;"></i></td>
							</tr>
						  </table>
					</details>
					</div>
					
                 </div>
  </div>
		<div class="row" style="margin-bottom: 50px;">
			<div class="col-sm-12 ">
				<!-- Pie Chart -->
				<div id="doughnutChart" style="height: 400px;"></div>
			</div>
		</div>
		<div class="fixed">
			<div class="row">
				<div class="col-sm-4">
					<table>
						<tr>
							<th style="font-size: 14px; font-style: italic;padding-right: 5px;">BEER Cases : </th>
							<td style="float: right;">{{alldetails | soldBeerCase | INR}}</td>
						</tr>
						<tr>
							<th style="font-size: 14px; font-style: italic;"></th>
							<td style="float: right;"></td>
						</tr>
					</table>
				</div>
				<div class="col-sm-4" style="text-align: -webkit-center;">
					<table>
						<tr>
							<th style="font-size: 14px; font-style: italic;padding-right: 5px;">LIQUOR : </th>
							<td style="float: right;">{{alldetails | Liquortotalprice | INR}}</td>
						</tr>
						<tr>
							<th style="font-size: 14px; font-style: italic;padding-right: 5px;">BEER :</th>
							<td style="float: right;">{{alldetails | beertotalprice | INR}}</td>
						</tr>
					</table>
				</div>
				<div class="col-sm-4" style="">
				<table style="float: right;">
				        <tr>
							<th style="font-size: 14px; font-style: italic;padding-right: 5px;">Sale : </th>
							<td style="float: right;">{{alldetails | sumItens | INR}}</td>
						</tr>
						<tr>
							<th style="font-size: 14px; font-style: italic;padding-right: 5px;">Expenses : </th>
							<td style="float: right;">{{expenseAmount}}</td>
						</tr>
						
					</table>
				</div>
			</div>
		</div>
 </div>
  <!-- Modal -->
<div class="modal fade right" id="saleFilterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-side modal-top-right" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title w-100" id="exampleModalLabel"><i class="fa fa-filter" style="color: #243a51!important;font-size: 22px;"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true" style="font-size: 25px;">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
		<select ng-model="modelfilter" class="form-control filterDropdownCss"  name="sort" style="float: left;">
			 <option value>ALL Category</option> 
		  <option ng-repeat="list in categories | unique:'category'" value="{{list.category}}">{{list.category}}</option>
	    </select> 
	   <select ng-model="companyfilter" class="form-control filterDropdownCss">
			  <option value>ALL Company</option>
		  <option ng-repeat="list in categories | unique:'company' | filter:{category:modelfilter}:true | orderBy:['companyOrder']" value="{{list.company}}">{{list.company}}</option>
	  
	   </select>
	
	   <!-- <div class="col-sm-3" style="font-size: 12px;font-weight: 400;"><b>Sort By:</b></div>
	    <div class="col-sm-9">
	   <select id="ddList" ng-click="onSelect()" class="form-control">
	     <option >Select Option</option>
	      <option value="brandname">Brand Name</option>
	      <option value="brandNo">Brand No</option>
	      <option value="category">Category</option>
	     <option value="cases">Total Sale</option>
	      <option value="totalval">Total Amount</option>
	     <option value="companyOrder">Company Order</option>
	     <option value="caseClosing"> In House Stock</option>
	   </select>
	   </div> -->
	   <div style="height: 40px;">
	    <div style="float: right;padding: 12px;">
	    <label class="filterLabelFontSize">Only Sale Items</label>
	    </div>
	    <div style="float: right;">
	    <label class="switch ">
         <input type="checkbox" id="includeZeroSaleOrNot" checked>
         <span class="slider round" style="background-color: red;" ng-click="includeZeroSaleOrNot()"></span>
        </label>
        </div>
        <div style="float: right;padding: 12px;">
        <label class="filterLabelFontSize">Total Sale  Items</label>
	   </div>
		</div>
	 </div>
      <div class="modal-footer">
        <!-- <button type="button" style="background: #1a6398!important;" class="btn btn-primary btn-rounded" data-dismiss="modal">CLOSE</button> -->
      </div>
    </div>
  </div>
</div>
<!-- Central Modal Small -->
<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="true" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
         <input type="hidden" value="{{brandNoPackQty}}"/>
          <div class="col-sm-2 bodyFontCss">
			   Start Date<input type="text" class="form-control" id="popupstartDate" readonly='true'>
			   </div>
			   <div class="col-sm-2 bodyFontCss">
			   End Date<input type="text" class="form-control" id="popupendDate" readonly='true'>
			   </div>
			   <div class="col-sm-4 customizedButton">
			    <select class="selectpicker" id="listdata" title="Select Products For Comparision" data-live-search="true" multiple data-max-options="5">
			     </select>
			   </div>
			   <div class="col-sm-1 bodyFontCss customizedButton">
			   <input class="btn btn-default btn-rounded" type="submit" name="" ng-click="buildMultiLineGraph()" value="GET" />
			   </div>
			   <div class="col-sm-1 bodyFontCss customizedButton"></div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="font-size: 25px;">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="chart-container" style="width: 100%;height: 300px;"></div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default btn-rounded" data-dismiss="modal">CLOSE</button> -->
        </div>
      </div>
    </div>
  </div>
  <!-- Central Modal Small -->
   <!-- Comparision Popup -->
<div class="modal" id="comparisonPupop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="true" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <h3>{{catLabel}} &nbsp;&nbsp; Case: {{totalcasesinchart}} &nbsp;&nbsp; Amount: {{totatlAmountinchart | INR}}</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="font-size: 25px;">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="height: 500px;">
        <div id="doughnutChartForACat"></div>
        
        
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
  <!-- Comparision Popup -->
</body>
</html>