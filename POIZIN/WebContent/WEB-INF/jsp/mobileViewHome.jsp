<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <spring:url value="/resources/css/mobileViewInhouseStock.css" var="mobileViewInhouseStockcss"/>
   <link rel="stylesheet" href="${mobileViewInhouseStockcss}">
  <script src="https://www.w3schools.com/lib/w3data.js"></script>
  <script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-filter/0.5.4/angular-filter.js"></script>
  <script src="js/common.js" type="text/javascript"></script>
    <spring:url value="/resources/javascript/mobileView/mobileViewHome.js" var="mobileViewHomejs"/>
  <script src="${mobileViewHomejs}"></script> 
  <style>
* {
    box-sizing: border-box;
}

body {
    font-family: Arial, Helvetica, sans-serif;
}

/* Style the header */
header {
    background-color: #062351;
    padding: 2px;
    text-align: center;
    font-size: 35px;
    color: white;
}

@media (max-width: 600px) {
    nav, article {
        width: 100%;
        height: auto;
    }
}
@font-face {
  font-family: "GothamRoundedBook";
  src: url(../assets/GothamRoundedBook_21018.ttf) format("truetype");
}
h1, h2, h3, h4, h5, h6 {
    font-family: "Segoe UI",Arial,sans-serif;
    font-weight: 400;
    margin: 10px 0;
}
h6 {
    font-size: 16px;
}
</style>
</head>

<body  ng-app="myApp" ng-controller="myCtrl">
<a href="mobileViewHome" style="text-decoration: none;">
<header style="height: 55px;">
  <h2 style="float:left;padding: 0px 0px 0px 2px;">POIZIN</h2>
  <h6 style="float: right;padding: 8px 2px 0px 0px;">HOME</h6>
</header></a>
<div class="container">
<div style="margin-top: 50px;">

<!-- <a href="mobileViewDiscountDues"><button type="button" class="btn btn-info" style="font-family: sans-serif;background: #fff !important;border: 1px solid #062351 !important;width:100%;color:#062351 !important;">Discount Dues</button></a> -->
<a href="mobileViewInhouseStockWithFilter"><button type="button" class="btn btn-info" style="font-family: sans-serif;background: #fff !important;border: 1px solid #062351 !important;width:100%;margin-top:10px;color:#062351 !important;">In House Stock- {{inHouseStockAmount | INR}}</button></a>
<a href="mobileViewDayWiseBalanceSheet"><button type="button" class="btn btn-info" style="font-family: sans-serif;background: #fff !important;border: 1px solid #062351 !important;width:100%;margin-top:10px;color:#062351 !important;">Day Wise Balance Sheet</button></a>
<a href="mobileViewSaleReport"><button type="button" class="btn btn-info" style="font-family: sans-serif;background: #fff !important;border: 1px solid #062351 !important;width:100%;margin-top:10px;color:#062351 !important;">Sale Report</button></a>
<a href="mobileViewSaleComparision"><button type="button" class="btn btn-info" style="font-family: sans-serif;background: #fff !important;border: 1px solid #062351 !important;width:100%;margin-top:10px;color:#062351 !important;">Sale Comparison</button></a>
<a href="mobileViewStockLiftingDiscount"><button type="button" class="btn btn-info" style="font-family: sans-serif;background: #fff !important;border: 1px solid #062351 !important;width:100%;margin-top:10px;color:#062351 !important;">Stock Lifting With Discount</button></a>
<a href="mobileViewStockLifting"><button type="button" class="btn btn-info" style="font-family: sans-serif;background: #fff !important;border: 1px solid #062351 !important;width:100%;margin-top:10px;color:#062351 !important;">Stock Lifting</button></a>
<!-- <a href="mobileViewDownloadIndent"><button type="button" class="btn btn-info" style="font-family: sans-serif;background: #fff !important;border: 1px solid #062351 !important;width:100%;margin-top:10px;color:#062351 !important;">Today's Indent</button></a> -->
<!-- <a href="mobileViewProductComparisionCategoryWise"><button type="button" class="btn btn-info" style="font-family: sans-serif;background: #fff !important;border: 1px solid #062351 !important;width:100%;margin-top:10px;color:#062351 !important;">Product Performance Category Wise</button></a> -->
<!-- <a href="mobileViewSaleWithDiscount"><button type="button" class="btn btn-info" style="font-family: sans-serif;background: #fff !important;border: 1px solid #062351 !important;width:100%;margin-top:10px;color:#062351 !important;">Sale With Discount</button></a> -->
 <a href="mobileViewCompanyWiseDiscount"><button type="button" class="btn btn-info" style="font-family: sans-serif;background: #fff !important;border: 1px solid #062351 !important;width:100%;margin-top:10px;color:#062351 !important;">Company Wise Discount</button></a>
 <a href="logoutByMobile"><button type="button" class="btn btn-info" style="font-family: sans-serif;background: #fff !important;border: 1px solid #062351 !important;width:100%;margin-top:10px;color:#062351 !important;">Logout</button></a>
</div>
</div>
</body>
</html>