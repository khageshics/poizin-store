<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
<jsp:include page="/WEB-INF/jsp/header.jsp" /> 
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
 <!--  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">	 -->
 <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
 
 <spring:url value="/resources/javascript/invoice.js" var="invoicejs"/>
   <spring:url value="/resources/javascript/expensesReport.js" var="expensesReportjs"/>
  <script src="${expensesReportjs}"></script> 
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script>  
     <spring:url value="/resources/javascript/app.js" var="appjs"/>
  <script src="${appjs}"></script>
  <spring:url value="/resources/javascript/test.js" var="testjs"/>
  <script src="${testjs}"></script>
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
  <link rel="stylesheet" href="${newCommoncss}">
   <spring:url value="/resources/css/expenseReport.css" var="expenseReportcss"/>
   <link rel="stylesheet" href="${expenseReportcss}">
 <style type="text/css">
    
.datalist {
  max-height:calc(100vh - 278px);
  overflow-y: auto;
  width: 100%;
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 15px;
}
    </style>
</head>
<body ng-app="myApp" ng-controller="Ctrl1">
 <div class="row" style="padding-top: 100px;">
   <div class="col-sm-3 bodyFontCss" style="padding: 0px !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Reports</li>
  <li>Expense Report</li>
</ul>
  </div>
  <div class="col-sm-1 bodyFontCss">
   </div>
   <div class="col-sm-2 bodyFontCss">
     <label style="font-weight: 700;">Satrt Date: </label><input type="text" class="form-control" id="startDate" readonly='true'>
    </div>
    <div class="col-sm-2 bodyFontCss">
     <label style="font-weight: 700;">End Date: </label><input type="text" class="form-control" id="endDate" readonly='true'>
    </div>
    <div class="col-sm-2 bodyFontCss" style="padding-top: 12px;">
    <label>&nbsp;</label><input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getResults('startDate','endDate')" value="GET DATA" />
    </div>
   <div class="col-sm-4 bodyFontCss">
   </div>
</div>
<div>

<div class="row"  style="margin-top: 20px;">
<div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>

   <div class="col-sm-3 bodyFontCss">
   <div class="datalist" style="margin-bottom: 10px;">
		  <label ng-repeat="role in expensesData" class="container">
	     <input type="checkbox" checklist-model="user.expensesData" checklist-value="role" > {{role.edate}}
	     <span class="checkmark"></span></label>
        </div>
     
     <button class="btn btn-default btn-rounded" ng-click="checkAll()" style="margin-right: 10px">Check all</button>
          <button class="btn btn-default btn-rounded" ng-click="uncheckAll()" style="margin-right: 10px">Uncheck all</button>
        
          <button ng-click="setToNull()" ng-if="setToNull">Set to null</button>
   </div>
   <div class="col-sm-9 bodyFontCss" style="float: right;">
          <table id="receivedDiscount" style="margin: 0px !important;">
           <thead>
		      <tr>
		        <th class="col-xs-2">Category</th>
		        <th class="col-xs-3">Date</th>
		        <th class="col-xs-5">Comment</th>
		        <th class="col-xs-2">Amount</th>
		      </tr>
		    </thead>
          </table>
          <div class="datalist">
		  <table id="receivedDiscount">
		    <tbody ng-repeat="list in user.expensesData | filter: greaterThan('total', 0)">
		      <tr>
		        <td class="col-xs-2" style="border: solid 1px #000;text-align: left;border-left: 0px;" rowspan="{{list.expenseCategoryBean.length+1}}">{{list.edate}}</td>
		        <td class="col-xs-3" style="border: solid 1px #000;text-align: left;">{{list.expenseCategoryBean[0].expenseDate}}</td>
		        <td class="col-xs-5" style="border: solid 1px #000;text-align: left;">{{list.expenseCategoryBean[0].comment}}</td>
		        <td class="col-xs-2" style="border: solid 1px #000;border-right: 0px;padding-right: 5px;">{{list.expenseCategoryBean[0].expenseAmount | INR}}</td>
		      </tr>
		      <tr ng-repeat="brand in list.expenseCategoryBean" ng-if="$index > 0">
		        <td class="col-xs-3" style="border: solid 1px #000;text-align: left;">{{brand.expenseDate}}</td>
		        <td class="col-xs-5" style="border: solid 1px #000;text-align: left;">{{brand.comment}}</td>
		        <td class="col-xs-2" style="border: solid 1px #000;border-right: 0px;padding-right: 5px;">{{brand.expenseAmount | INR}}</td>
		      </tr>
		      <tr>
		      <!--  <td style="border: solid 1px #000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> -->
		        <td class="col-xs-3" style="border: solid 1px #000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td class="col-xs-5" style="border: solid 1px #000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td class="col-xs-3" style="border: solid 1px #000;border-right: 0px;padding-right: 5px;"><b>{{list.total | INR}}</b></td>
		      </tr>
		    </tbody>
		  </table> 
		</div>
	</div>
   </div>
</div>
<div class="fixed">
			<div class="row">
				<div class="col-sm-10">
				</div>
				<div class="col-sm-2" style="">
				<table style="float: right;margin-bottom: 0px;">
						<tr>
							<th style="font-size: 14px; font-style: italic;padding-right: 5px;background-color: #243a51;">Total Expenses : </th>
							<td style="float: right;background-color: #243a51;">{{user.expensesData | totalExpense | INR}}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
</body>
</html>