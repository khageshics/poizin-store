<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
 <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
  <spring:url value="/resources/css/3.3.7.bootstrap.min.css" var="bootstrapmincss"/>
   <link rel="stylesheet" href="${bootstrapmincss}"> 
<jsp:include page="/WEB-INF/jsp/header.jsp" /> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script> 
<!--   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
  <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script>
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
<!--   <script src="https://rawgit.com/zorab47/jquery.ui.monthpicker/master/jquery.ui.monthpicker.js"></script> -->
  <spring:url value="/resources/javascript/thirdpartyjs/jquery.ui.monthpicker.js" var="jqueryuimonthpickerjs"/>
  <script src="${jqueryuimonthpickerjs}"></script>
  <spring:url value="/resources/javascript/newDiscountEstimate.js" var="newDiscountEstimatejs"/>
  <script src="${newDiscountEstimatejs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}"> 
   <spring:url value="/resources/css/discountEstimate.css" var="discountEstimatecss"/>
   <link rel="stylesheet" href="${discountEstimatecss}">
   <style>
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 15px;
 color: :#fff !important;
 background: #243a51;
}
td.tip {
    /* border-bottom: 1px dashed; */
    text-decoration: none;
    list-style-type: none;
}
td.tip:hover {
   /*  cursor: help; */
    position: relative
}
td.tip ul {
    display: none
}
td.tip ul > li {
        line-height: 20px;
        list-style-type: none; 
         text-align: left;
}
td.tip:hover ul {
     border: #c0c0c0 1px dotted;
    padding: 5px 20px 5px 5px;
    display: block;
    z-index: 100;
    left: 0px;
    margin: 10px;
    width: 85px;
    position: absolute;
    top: 10px;
    text-decoration: none;
    background: #243a51;
    color: #fff;
    
}
 span.tip {
    /* border-bottom: 1px dashed; */
    text-decoration: none;
    list-style-type: none;
}
span.tip:hover {
   /*  cursor: help; */
    position: relative;
}
span.tip ul {
    display: none
}
span.tip ul > li {
        line-height: 20px;
        list-style-type: none; 
         text-align: left;
}
span.tip:hover ul {
     border: #c0c0c0 1px dotted;
    padding: 5px 20px 5px 5px;
    display: block;
    z-index: 100;
    left: -50px;
    margin: 10px;
    width: 175px;
    position: absolute;
    top: 10px;
    text-decoration: none;
    background: #243a51;
    color: #fff;
    font-family:  'Montserrat', sans-serif !important;;
    font-size: 10px;
    
    
}

</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
  <div class="container" style="margin-top: 100px;">
 <div class="row">
  <div class="col-sm-4 bodyFontCss" style="padding: 0px !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Discount</li>
  <li>New Discount Estimate</li>
</ul>
  </div>
 <!--  <div class="col-sm-1 bodyFontCss"></div> -->
  <div class="col-sm-2 bodyFontCss">
   <label>Month</label><input type="text" class="monthpicker form-control"  id="monthlyDate">
  </div>
   <div class="col-sm-1 bodyFontCss customizedButton">
   <input class="btn btn-default btn-rounded" type="submit" ng-click="getResults()" value="GET DATA" />
   </div>
     <div class="col-sm-4 bodyFontCss"></div>
	   <div class="col-sm-1 bodyFontCss">
	   <span><i class="fa fa-filter filter-icon" style="color: #243a51!important;" data-toggle="modal" data-target="#saleFilterModal" aria-hidden="true"></i></span>
	   </div>
  </div>
   <div class="row">
  <div class="tab-content reportWithoutEntry" style="padding: 0px;">
   <button type="button" class="btn btn-default btn-rounded" ng-click="deleteRow('discounttab')">DELETE</button>
   <a style="float: right;" ng-click="generateTargetPdf('companyfilter')"><img alt="pdfImg" src="resources/images/pdf.png" style="float: right; margin-right: 30px;cursor: pointer;"></a>
	           <div style="margin-top: 5px;">
				 <table class="saledetailreport fixed_headers" id="discounttab">
					<thead>
					  <tr>
					   <th>Select</th>
						<th title="Brand Name">Brand Name</th>
						<th title="Brand No">Brand No</th>
						<th title="AVG(Unit Rate)">Case Rate</th>
						<th title="Current Avaiable Stock">In-House Stock</th>
						<th title="Last 30 days Sold">Last Month Sold (Case)</th>
						<th title="Sum Of Current Month Lifted Case">Lifted Case</th>
						<th title="Current Month Target Case">Target Case</th>
						<th title="(Target Case - Lifted Case)">Pending</th>
						<th title="Discount Amt per Case">Disc/Case</th>
						<th title="(Pending * Case Rate)">Investment</th>
						<th title="(Pending * Disc Per Case)">Total Disc</th>
						<th title="((Disc per Case * 100)/Case Rate)">Disc %</th>
						<th title="((Pending + In-House Stock) / Last Month Sold)">Clearn's Period In Month (Including In-House Stock)</th>
						<th title="(Pending / Last Month Sold)">Clearn's Period In Month (Excluding In-House Stock)</th>
						<th title="(Disc percentage / Clearn's Period In Month (Including In-House Stock))">Rate Of Interest (Including In-House Stock)</th>
						<th title="(Disc percentage / Clearn's Period In Month (Excluding In-House Stock))">Rate Of Interest (Excluding In-House Stock)</th>
					  </tr>
					  </thead>
					  <tbody>
					  <tr ng-repeat="brand in alldetails = (categoryList | filter:{company:companyfilter,company:companyfilter||undefined}:true)  | orderBy:['companyOrder','saleSheetOrder']">
						<td style="background: linear-gradient(to right, {{brand.companyColor}} 15%, white 6%);"><INPUT type="checkbox" name="chk" style="margin: 0px; padding: 0px;width: 15px;"></td>
						<td  id="bname{{brand.brandNo}}" style="color: #000;">{{brand.brandName}}</td>
						<td id="{{brand.brandNo}}" style="color: #000;">{{brand.brandNo}}</td>
						<td id="caserate{{brand.brandNo}}" style="color: #000;">{{brand.caseRate}}</td>
						<td id="inHouse{{brand.brandNo}}" class="tip" style="color: #000;">{{brand.inHouseStock}}
						<ul ng-if="brand.productType == 'LIQUOR'">
						<li>2L: {{brand.splitPackType.l2Stock}}</li><li>1L: {{brand.splitPackType.l1Stock}}</li><li>Q: {{brand.splitPackType.qstock}}</li>
						<li>P: {{brand.splitPackType.pstock}}</li><li>N: {{brand.splitPackType.nstock}}</li><li>D: {{brand.splitPackType.dstock}}</li>
						</ul>
						<ul ng-if="brand.productType == 'BEER'">
						<li>LB: {{brand.splitPackType.lbstock}}</li><li>SB: {{brand.splitPackType.sbstock}}</li><li>TIN: {{brand.splitPackType.tinstock}}</li></ul>
						</td>
						<td id="lastMonthSold{{brand.brandNo}}" style="color: #000;">{{brand.lastMonthSold}}</td>
						<td id="liftedCase{{brand.brandNo}}" style="color: #000;">{{brand.liftedCase}}</td>
						<td style="color: #000;"><input type="number" style="width: 80px;border: solid 1px #e8e8e8; border-radius: 2px;" id="newcase{{brand.brandNo}}"  ng-model="target"  value="{{brand.target}}"  ng-change="onChnage(target,{{brand.inHouseStock}},{{brand.brandNo}},{{brand.caseRate}},{{brand.lastMonthSold}},{{brand.liftedCase}})" /></td>
						<td id="pending{{brand.brandNo}}" ng-if="brand.pending > 0" style="color: red;">{{brand.pending}}</td>
						<td id="pending{{brand.brandNo}}" ng-if="brand.pending <= 0" style="color: #000;">{{brand.pending}}</td>
						<td style="color: #000;overflow: visible;"><input type="number" style="width: 50px;border: solid 1px #e8e8e8; border-radius: 2px;" id="discPerCase{{brand.brandNo}}"  ng-model="discPerCase"  value="{{brand.dicsPerCase}}" ng-change="calculateTotalDicount(discPerCase,{{brand.inHouseStock}},{{brand.brandNo}},{{brand.lastMonthSold}},{{brand.caseRate}})"  />
						<span style="font-size: 9px;vertical-align: super;" class="glyphicon glyphicon-info-sign tip">
                            <ul>
							<li>Max : {{brand.maxMinLastDisc[0].maxDisc}} / {{brand.maxMinLastDisc[0].maxDiscCase}} ({{brand.maxMinLastDisc[0].maxDiscDate}})</li>
							<li>Min : {{brand.maxMinLastDisc[0].minDisc}} / {{brand.maxMinLastDisc[0].minDiscCase}} ({{brand.maxMinLastDisc[0].minDiscDate}})</li>
							<li>Last Month : {{brand.maxMinLastDisc[0].prevoiusDisc}} / {{brand.maxMinLastDisc[0].previousDiscCase}} ({{brand.maxMinLastDisc[0].previousDiscDate}})</li>
							</ul>
                            </span>
						</td>
						<td id="investment{{brand.brandNo}}" style="color: #000;">{{brand.investment}}</td>
						<td id="totalDisc{{brand.brandNo}}" style="color: #000;">{{brand.totalDiscount}}</td>
						<td id="discPercentage{{brand.brandNo}}" style="color: #000;">{{brand.discountPer}}</td>
						<td id="estimationMonth{{brand.brandNo}}" style="color: #000;">{{brand.estimationMonth}}</td>
						<td id="estimationMonthExclude{{brand.brandNo}}" style="color: #000;">{{brand.clearnsPeriodWithoutInhouseStock}}</td>
						<td id="perMonth{{brand.brandNo}}" style="color: #000;">{{brand.rateOfInterestWithInhouse}}%</td>
						<td id="perMonthExclude{{brand.brandNo}}" style="color: #000;background: linear-gradient(to left, {{brand.categoryColor}} 6%, white 0%);">{{brand.rateOfInterestWithoutInhouse}}%</td>
						<td style="display: none;">{{brand.companyId}}</td>
						<td style="display: none;">{{brand.adjustment}}</td>
						<td style="display: none;">{{brand.cateId}}</td>
						</tr>
					</tbody>
				 </table>
             </div>
              <p style="text-align: center;margin: 10px 0px 50px;"><input  class=" btn btn-default btn-rounded" type="submit" name="" ng-click="savediscountDetails()" value="Save Details" /></p>
     
			<!-- footer -->
			 <div class="fixed">
			<div class="row">
				<div class="col-sm-4">
					<table style="float: center;width: 0% !important;">
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;background-color: #243a51;padding-top: 0px;">Investment : </th>
							<td style="float: right;border-top: 1px solid #243a51;padding: 0px;!important;"><span style="color: #fff;" id="TotalInvestment">{{alldetails | TotalInvestment}}</span></td>
						</tr>
					</table>
				</div>
				<div class="col-sm-2">
				<table>
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;background-color: #243a51;padding-top: 0px;display: inline;">Pending Beer : </th>
							<td style="border-top: 1px solid #243a51;padding: 0px;!important;display: inline;position: absolute;"><span style="color: #fff;" id="TotalBeerPending">{{alldetails | pendingBeerCase}}</span></td>
						</tr>
					</table>
				</div>
				<div class="col-sm-2">
				<table>
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;background-color: #243a51;padding-top: 0px;display: inline;">Pending Liquor : </th>
							<td style="border-top: 1px solid #243a51;padding: 0px;!important;display: inline;position: absolute;"><span style="color: #fff;" id="TotalLiquorPending">{{alldetails | pendingLiquorCase}}</span></td>
						</tr>
					</table>
				</div>
				<div class="col-sm-4" style="">
				<table style="float: right;width: 0% !important;">
				        <tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;background-color: #243a51;padding-top: 0px;">Discount : </th>
							<td style="float: right;border-top: 1px solid #243a51;padding: 0px;!important;"><span style="color: #fff;" id="discountAmt">{{alldetails | TotalDiscount}}</span></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<!-- footer close -->
             
    </div>
    <div class="tab-content reportWithEntry" style="padding: 0px;">
    <button type="button" class="btn btn-default btn-rounded" ng-click="deleteRowSec('discounttabSec')">DELETE</button>
    <a style="float: right;" ng-click="generateTargetPdf('companyfilterSec')"><img alt="pdfImg" src="resources/images/pdf.png" style="float: right; margin-right: 30px;cursor: pointer;"></a>
	           <div style="margin-top: 5px;">
				 <table class="saledetailreport fixed_headers" id="discounttabSec">
					<thead>
					  <tr>
					    <th>Select</th>
						<th title="Brand Name">Brand Name</th>
						<th title="Brand No">Brand No</th><th title="AVG(Unit Rate)">Case Rate</th>
						<th title="Current Avaiable Stock">In-House Stock</th>
						<th title="Last 30 days Sold">Last Month Sold (Case)</th>
						<th title="Sum Of Current Month Lifted Case">Lifted Case</th>
						<th title="Current Month Target Case">Target Case</th>
						<th title="(Target Case - Lifted Case)">Pending</th>
						<th title="Discount Amt per Case">Disc/Case</th>
						<th title="(Pending * Case Rate)">Investment</th>
						<th title="(Pending * Disc Per Case)">Total Disc</th>
						<th title="((Disc per Case * 100)/Case Rate)">Disc %</th>
						<th title="((Pending + In-House Stock) / Last Month Sold)">Clearn's Period In Month (Including In-House Stock)</th>
						<th title="(Pending / Last Month Sold)">Clearn's Period In Month (Excluding In-House Stock)</th>
						<th title="(Disc percentage / Clearn's Period In Month (Including In-House Stock))">Rate Of Interest (Including In-House Stock)</th>
						<th title="(Disc percentage / Clearn's Period In Month (Excluding In-House Stock))">Rate Of Interest (Excluding In-House Stock)</th>
					   </tr>
					  </thead>
					  <tbody>
					  <tr ng-repeat="brand in alldetails = (categoryList | filter:{company:companyfilterSec,company:companyfilterSec||undefined}:true) | filter: greaterThan('target','dicsPerCase', 0) | orderBy:['companyOrder','saleSheetOrder']">
						<td style="background: linear-gradient(to right, {{brand.companyColor}} 15%, white 6%);"><INPUT type="checkbox" name="chk" style="margin: 0px; padding: 0px;width: 15px;"></td>
						<td  id="bnameSec{{brand.brandNo}}" style="color: #000;">{{brand.brandName}}</td>
						<td id="Sec{{brand.brandNo}}" style="color: #000;">{{brand.brandNo}}</td>
						<td id="caserateSec{{brand.brandNo}}" style="color: #000;">{{brand.caseRate}}</td>
						<td id="inHouseSec{{brand.brandNo}}" class="tip" style="color: #000;">{{brand.inHouseStock}}
						<ul ng-if="brand.productType == 'LIQUOR'">
						<li>2L: {{brand.splitPackType.l2Stock}}</li><li>1L: {{brand.splitPackType.l1Stock}}</li><li>Q: {{brand.splitPackType.qstock}}</li>
						<li>P: {{brand.splitPackType.pstock}}</li><li>N: {{brand.splitPackType.nstock}}</li><li>D: {{brand.splitPackType.dstock}}</li>
						</ul>
						<ul ng-if="brand.productType == 'BEER'">
						<li>LB: {{brand.splitPackType.lbstock}}</li><li>SB: {{brand.splitPackType.sbstock}}</li><li>TIN: {{brand.splitPackType.tinstock}}</li></ul>
						</td>
						<td id="lastMonthSoldSec{{brand.brandNo}}" style="color: #000;">{{brand.lastMonthSold}}</td>
						<td id="liftedCaseSec{{brand.brandNo}}" style="color: #000;">{{brand.liftedCase}}</td>
						<td style="color: #000;"><input type="number" style="width: 80px;border: solid 1px #e8e8e8; border-radius: 2px;" id="newcaseSec{{brand.brandNo}}"  ng-model="targetSec"  value="{{brand.target}}"  ng-change="onChnageSec(targetSec,{{brand.inHouseStock}},{{brand.brandNo}},{{brand.caseRate}},{{brand.lastMonthSold}},{{brand.liftedCase}})" /></td>
						<td id="pendingSec{{brand.brandNo}}" ng-if="brand.pending > 0" style="color: red;">{{brand.pending}}</td>
						<td id="pendingSec{{brand.brandNo}}" ng-if="brand.pending <= 0" style="color: #000;">{{brand.pending}}</td>
						<td style="color: #000;overflow: visible;"><input type="number" style="width: 50px;border: solid 1px #e8e8e8; border-radius: 2px;" id="discPerCaseSec{{brand.brandNo}}"  ng-model="discPerCaseSec"  value="{{brand.dicsPerCase}}" ng-change="calculateTotalDicountSec(discPerCaseSec,{{brand.inHouseStock}},{{brand.brandNo}},{{brand.lastMonthSold}},{{brand.caseRate}})"  />
						<span style="font-size: 9px;vertical-align: super;" class="glyphicon glyphicon-info-sign tip">
                             <ul>
							<li>Max : {{brand.maxMinLastDisc[0].maxDisc}} / {{brand.maxMinLastDisc[0].maxDiscCase}} ({{brand.maxMinLastDisc[0].maxDiscDate}})</li>
							<li>Min : {{brand.maxMinLastDisc[0].minDisc}} / {{brand.maxMinLastDisc[0].minDiscCase}} ({{brand.maxMinLastDisc[0].minDiscDate}})</li>
							<li>Last Month : {{brand.maxMinLastDisc[0].prevoiusDisc}} / {{brand.maxMinLastDisc[0].previousDiscCase}} ({{brand.maxMinLastDisc[0].previousDiscDate}})</li>
							</ul>
                            </span>
						</td>
						<td id="investmentSec{{brand.brandNo}}" style="color: #000;">{{brand.investment}}</td>
						<td id="totalDiscSec{{brand.brandNo}}" style="color: #000;">{{brand.totalDiscount}}</td>
						<td id="discPercentageSec{{brand.brandNo}}" style="color: #000;">{{brand.discountPer}}</td>
						<td id="estimationMonthSec{{brand.brandNo}}" style="color: #000;">{{brand.estimationMonth}}</td>
						<td id="estimationMonthExcludeSec{{brand.brandNo}}" style="color: #000;">{{brand.clearnsPeriodWithoutInhouseStock}}</td>
						<td id="perMonthSec{{brand.brandNo}}" style="color: #000;">{{brand.rateOfInterestWithInhouse}}%</td>
						<td id="perMonthExcludeSec{{brand.brandNo}}" style="color: #000;background: linear-gradient(to left, {{brand.categoryColor}} 6%, white 0%);">{{brand.rateOfInterestWithoutInhouse}}%</td>
						<td style="display: none;">{{brand.companyId}}</td>
						<td style="display: none;">{{brand.adjustment}}</td>
						<td style="display: none;">{{brand.cateId}}</td>
						</tr>
					</tbody>
				 </table>
             </div>
               <p style="text-align: center;margin: 10px 0px 50px;"><input  class=" btn btn-default btn-rounded" type="submit" name="" ng-click="savediscountDetailsSec()" value="Save Details" /></p>
     
            <!-- <div class="row" style="margin-bottom: 5px;">
				<div class="col-sm-8"></div>
				<div class="col-sm-4" style="float: right;">
					<table style="width: 100%; border: solid 1px #000;">
						<tr>
							<th class="totalval" style="font-weight: bold;">Investment</th>
							<td class="totalval" style="text-align: right;"><span id="TotalInvestmentSec">{{alldetails | TotalInvestment}}</span></td>
							</tr>
							<tr>
							<th class="totalval" style="font-weight: bold;">Discount</th>
							<td class="totalval" style="text-align: right;"><span id="discountAmtSec">{{alldetails | TotalDiscount}}</span></td>
							</tr>
					</table>

				</div>
			</div> -->
			<!-- footer -->
			 <div class="fixed">
			<div class="row">
				<div class="col-sm-4">
					<table>
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;background-color: #243a51;padding-top: 0px;display: inline;">Investment : </th>
							<td style="border-top: 1px solid #243a51;padding: 0px;!important;display: inline;position: absolute;"><span style="color: #fff;" id="TotalInvestmentSec">{{alldetails | TotalInvestment}}</span></td>
						</tr>
					</table>
				</div>
				<div class="col-sm-2">
				<table>
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;background-color: #243a51;padding-top: 0px;display: inline;">Pending Beer : </th>
							<td style="border-top: 1px solid #243a51;padding: 0px;!important;display: inline;position: absolute;"><span style="color: #fff;" id="TotalBeerPendingSec">{{alldetails | pendingBeerCase}}</span></td>
						</tr>
					</table>
				</div>
				<div class="col-sm-2">
				<table>
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;background-color: #243a51;padding-top: 0px;display: inline;">Pending Liquor : </th>
							<td style="border-top: 1px solid #243a51;padding: 0px;!important;display: inline;position: absolute;"><span style="color: #fff;" id="TotalLiqourPendingSec">{{alldetails | pendingLiquorCase}}</span></td>
						</tr>
					</table>
				</div>
				<div class="col-sm-3" style="    text-align: right;">
				<table>
				        <tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;background-color: #243a51;padding-top: 0px;display: inline;">Discount : </th>
							<td style="border-top: 1px solid #243a51;padding: 0px;!important;display: inline;position: absolute;"><span style="color: #fff;" id="discountAmtSec">{{alldetails | TotalDiscount}}</span></td>
						</tr>
					</table>
				</div>
				<div class="col-sm-1"></div>
			</div>
		</div>
		<!-- footer close -->
    </div>
  </div>
  </div>
    <!-- Modal -->
<div class="modal fade right" id="saleFilterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-side modal-top-right" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title w-100" id="exampleModalLabel"><i class="fa fa-filter" style="color: #243a51!important;font-size: 22px;"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="font-size: 25px;">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
		<div class="reportWithoutEntry">
		    <select ng-model="companyfilter" class="form-control" ng-change="calculateVal(companyfilter);">
		          <option value>ALL Company</option>
	              <option ng-repeat="list in categoryList | unique:'company' " value="{{list.company}}">{{list.company}}</option>
               </select>
		  </div>
		  <div class="reportWithEntry">
		    <select style="float: left;margin-left: 5px;" ng-model="companyfilterSec" class="form-control" ng-change="calculateValSec(companyfilterSec);">
		          <option value>ALL Company</option>
	              <option ng-repeat="list in categoryList | unique:'company' " value="{{list.company}}">{{list.company}}</option>
               </select>
		  </div>
		   <div style="height: 40px;">
	    <div style="float: right;padding: 12px;">
	    <label class="filterLabelFontSize">All Items</label>
	    </div>
	    <div style="float: right;">
	    <label class="switch ">
         <input type="checkbox" id="catOrComp">
         <span class="slider round" style="background-color: red;" ng-click="FilterCatOrComp()"></span>
        </label>
        </div>
        <div style="float: right;padding: 12px;">
        <label class="filterLabelFontSize">Discount Items</label>
	   </div>
		</div> 
	 </div>
      <div class="modal-footer">
        <!-- <button type="button" style="background: #1a6398!important;" class="btn btn-primary btn-rounded" data-dismiss="modal">Apply Filters</button> -->
      </div>
    </div>
  </div>
</div>
</body>
</html>