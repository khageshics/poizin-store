<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
<title>www.poizin.com</title>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!--  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
 <link href="https://fonts.googleapis.com/css?family=Michroma" rel="stylesheet"> -->
  <spring:url value="/resources/css/Montserrat.css" var="Montserratcss"/>
   <link rel="stylesheet" href="${Montserratcss}"> 
    <spring:url value="/resources/css/Michroma.css" var="Michromacss"/>
   <link rel="stylesheet" href="${Michromacss}"> 
    <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
   <script src="${jqueryminjs}"></script> 
    <spring:url value="/resources/javascript/thirdpartyjs/angular.min.js" var="angularminjs"/>
  <script src="${angularminjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/angular-filter.js" var="angularfilterjs"/>
  <script src="${angularfilterjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
   <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
   <script src="${bootstrapminjs}"></script> 
   <spring:url value="/resources/javascript/thirdpartyjs/jquery-confirm.min.js" var="jqueryconfirmminjs"/>
   <script src="${jqueryconfirmminjs}"></script> 
    <!-- Bootstrap CSS-->
   <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
   <spring:url value="/resources/css/theme.css" var="themecss"/>
   <link rel="stylesheet" href="${themecss}"> 
  <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
  <spring:url value="/resources/css/bootstrap.min.css" var="bootstrapmincss"/>
  <link rel="stylesheet" href="${bootstrapmincss}">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
<%-- <spring:url value="/resources/css/font-awesome.css" var="fontawesomecss"/>
<link rel="stylesheet" href="${fontawesomecss}"> --%>
<spring:url value="/resources/newTemp/css/bootstrap.min.css" var="bootstrapmincss"/>
<link rel="stylesheet" href="${bootstrapmincss}">
<spring:url value="/resources/newTemp/css/mdb.min.css" var="bootstrapmincss"/>
<link rel="stylesheet" href="${bootstrapmincss}">
   <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}">

</head>
<body class="hidden-sn mdb-skin">
    <!--Double navigation-->
    <header>
        <!-- Sidebar navigation -->
        <div id="slide-out" class="side-nav sn-bg-4 mdb-sidenav" style="    width: 22rem;">
            <ul class="custom-scrollbar list-unstyled" style="max-height:100vh;">
                <!-- Logo -->
                <li>
                    <div class="logo-wrapper waves-light" style="padding: 25px;">
                    <p class="nav_title_css">${sessionScope['scopedTarget.userSession'].storeName}</p>
                    </div>
                </li>
                <!--/. Logo -->
               
                <!-- Side navigation links -->
                <li>
                    <ul class="collapsible collapsible-accordion">
                        <li><a class="collapsible-header waves-effect arrow-r inlineHeadrCss"><i class="fa fa-chevron-right"></i> Invoice <i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="addNewBrand" class="waves-effect inlineAchCss">Add Product</a>
                                    </li>
                                    <li><a href="saveInvoice" class="waves-effect inlineAchCss">Add Invoice</a>
                                    </li>
                                    </li>
                                     <li><a href="ocrInvoice" class="waves-effect inlineAchCss">Add OCR Invoice</a>
                                    </li>
                                    <li><a href="productList" class="waves-effect inlineAchCss">Active/Inactive Products</a>
                                    </li>
                                    <li><a href="UploadAndViewInvoice" class="waves-effect inlineAchCss">Upload/View Invoice</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li><a class="collapsible-header waves-effect arrow-r inlineHeadrCss"><i class="fa fa-chevron-right"></i> Sale<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="sale" class="waves-effect inlineAchCss">Enter Sale</a>
                                    </li>
                                    <li><a href="updateSale" class="waves-effect inlineAchCss">Edit Sale</a>
                                    </li>
                                    <li><a href="monthlyExpenses" class="waves-effect inlineAchCss">Monthly Debit/Credit</a>
                                    </li>
                                    <li><a href="investment" class="waves-effect inlineAchCss">Enter Investment</a>
                                    </li>
                                    <li><a href="discountTransaction" class="waves-effect inlineAchCss">Add Cheque</a>
                                    </li>
                                     <li><a href="credit" class="waves-effect inlineAchCss">Add Credits</a>
                                    </li>
                                    <!-- <li><a href="rentalandbreakage" class="waves-effect inlineAchCss">Add Rental&Breakage</a>
                                    </li> -->
                                    <!--  <li><a href="mobileViewEnterSaleSheet" class="waves-effect inlineAchCss">Generate Sale Sheet</a>
                                    </li> -->
                                </ul>
                            </div>
                        </li>
                        <li><a class="collapsible-header waves-effect arrow-r inlineHeadrCss"><i class="fa fa-chevron-right"></i> Reports<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="saleReport" class="waves-effect inlineAchCss">Sale Report</a>
                                    </li>
                                    <li><a href="newSaleReport" class="waves-effect inlineAchCss">New Sale Report</a>
                                    </li>
                                    <li><a href="saleDayWiseReport" class="waves-effect inlineAchCss">Day Wise Sale Report</a>
                                    </li>
                                    <li><a href="stockLifting" class="waves-effect inlineAchCss">Stock Lift</a>
                                    </li>
                                    <li><a href="inHouseStockValue" class="waves-effect inlineAchCss">InHouse Stock</a>
                                    </li>
                                    <li><a href="balanceSheet" class="waves-effect inlineAchCss">Balance Sheet</a>
                                    </li>
                                    <li><a href="productComparisionCategoryWise" class="waves-effect inlineAchCss">Product Comparison</a>
                                    </li>
                                    <li><a href="saleWithDiscount" class="waves-effect inlineAchCss">Sale With Discount</a>
                                    </li>
                                    <li><a href="expensesReport" class="waves-effect inlineAchCss">Expense Report</a>
                                    </li>
                                    <li><a href="profitAndLossReport" class="waves-effect inlineAchCss">Profit/Loss Report</a>
                                    </li>
                                    <!-- <li><a href="admin" class="waves-effect inlineAchCss">Dash board</a>
                                    </li> -->
                                </ul>
                            </div>
                        </li>
                        <li><a class="collapsible-header waves-effect arrow-r inlineHeadrCss"><i class="fa fa-chevron-right"></i>Discount<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="discountEstimate" class="waves-effect inlineAchCss">Discount Estimate</a>
                                    </li>
                                   <!--  <li><a href="newDiscountEstimate" class="waves-effect inlineAchCss">Discount Estimate</a>
                                    </li> -->
                                    <li><a href="newIndentEstimate" class="waves-effect inlineAchCss">Indent Estimate</a>
                                    </li>
                                    <li><a href="viewDiscount" class="waves-effect inlineAchCss">Company Discount</a>
                                    </li>
                                    <li><a href="stockLiftWithDiscountTransaction" class="waves-effect inlineAchCss">Enter Discount</a>
                                    </li>
                                    <li><a href="generateDiscountPdf" class="waves-effect inlineAchCss">Discount Pdf</a>
                                    </li>
                                    <li><a href="discountcalculation" class="waves-effect inlineAchCss">Discount Calculation</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li><a class=" waves-effect arrow-r inlineHeadrCss" href="./logout"> <i class="fa fa-chevron-right"></i>Logout</a></li>
                    </ul>
                </li>
                <!--/. Side navigation links -->
            </ul>
            <div class="sidenav-bg mask-strong"></div>
        </div>
        <!--/. Sidebar navigation -->
        <!-- Navbar -->
        
        <nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav" style="border-radius: 0px;">
            <!-- SideNav slide-out button -->
            <div class="float-left">
                <a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
            </div>
            <!-- Breadcrumb-->
            <div class="breadcrumb-dn mr-auto">
                <a href="admin" class="waves-effect inlineAchCss"><p style=" font-family: 'Michroma', sans-serif; font-size: 23px;">${sessionScope['scopedTarget.userSession'].storeName}</p></a>
            </div>
            <ul class="nav navbar-nav nav-flex-icons ml-auto">
                <li class="nav-item">
                    <a class="nav-link achPadding" href="saleReport"><span class="clearfix d-none d-sm-inline-block">Sale Report</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link achPadding" href="stockLifting"><span class="clearfix d-none d-sm-inline-block">Stock Lift</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link achPadding" href="inHouseStockValue"><span class="clearfix d-none d-sm-inline-block">InHouse Stock</span></a>
                </li>
               
            </ul>
        </nav>
        
        <!-- /.Navbar -->
    </header>
    <!--/.Double navigation-->
    
    
    <spring:url value="/resources/newTemp/js/jquery-3.3.1.min.js" var="jquery331minjs"/>
  <script src="${jquery331minjs}"></script>
   <spring:url value="/resources/newTemp/js/popper.min.js" var="popperminjs"/>
  <script src="${popperminjs}"></script>
 <spring:url value="/resources/newTemp/js/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script>
   <spring:url value="/resources/newTemp/js/mdb.min.js" var="mdbminjs"/>
  <script src="${mdbminjs}"></script>
  <script>
   $(".button-collapse").sideNav();
    </script>
 
</body>
</html>