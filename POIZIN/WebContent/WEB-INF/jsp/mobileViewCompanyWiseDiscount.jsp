<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-1.12.4.js" var="jquery1124js"/>
  <script src="${jquery1124js}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/angular.min.js" var="angularminjs"/>
  <script src="${angularminjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/angular-filter.js" var="angularfilterjs"/>
  <script src="${angularfilterjs}"></script>
<!--   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">

  <link rel="stylesheet" href="/resources/demos/style.css">
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
  <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script>
<!--    <script src="https://rawgit.com/zorab47/jquery.ui.monthpicker/master/jquery.ui.monthpicker.js"></script> -->
    <spring:url value="/resources/javascript/thirdpartyjs/jquery.ui.monthpicker.js" var="jqueryuimonthpickerjs"/>
  <script src="${jqueryuimonthpickerjs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
 <spring:url value="/resources/css/common.css" var="commoncss"/>
   <link rel="stylesheet" href="${commoncss}">
     <spring:url value="/resources/css/bootstrap-select.min.css" var="bootstrapselectmincss"/>
   <link rel="stylesheet" href="${bootstrapselectmincss}">
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap-select.min.js" var="bootstrapselectminjs"/>
  <script src="${bootstrapselectminjs}"></script>
   <spring:url value="/resources/css/bootstrap.min.css" var="bootstrapmincss"/>
   <link rel="stylesheet" href="${bootstrapmincss}">
 <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <spring:url value="/resources/javascript/mobileView/mobileViewCompanyWiseDiscount.js" var="mobileViewCompanyWiseDiscountjs"/>
  <script src="${mobileViewCompanyWiseDiscountjs}"></script> 
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  
   <spring:url value="/resources/css/mobileViewCompanyWiseDiscount.css" var="mobileViewCompanyWiseDiscountcss"/>
   <link rel="stylesheet" href="${mobileViewCompanyWiseDiscountcss}">
   
  <script>
  $('.selectpicker').selectpicker();
  </script>
  <style>

.dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover {
    color: #262626;
    text-decoration: none;
    background-color: #e6e8e8;
    }
     .sbi2-popup-styles .modal-body {
  padding-top: 10px !important;
}
table,tbody,thead,td,th {
    background-color: #fff;
    border: solid 1px #000;
    font-size: 15px;
    text-align: right;
}
.btn-group>.btn:first-child {
    margin-left: 0;
    padding: 1px;
}
 .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
    width: 110px;
    /* height: 15px; */
}
table{
        width: 100%;
        margin-bottom: 20px;
		border-collapse: collapse;
    }
     table td{
      padding: 2px;
      text-align: right;
      font-size: 12px;
    }
    table th{
    padding: 2px;
    text-align: center;
    font-size: 12px;
    background: antiquewhite;
    }
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<div >
<a href="mobileViewHome" style="text-decoration: none;">
<header style="height: 55px;">
  <h2 style="float:left;padding: 0px 0px 0px 2px;">POIZIN</h2>
  <h6 style="float: right;padding: 8px 2px 0px 0px;">COMPANY WISE DISCOUNT</h6>
</header></a>
<div class="tab-content " style="font-family: unset;font-size: 13px;">
<div class="container">
<div class="row allignmentcss">
    <div class="col-md-2">
	</div>
    <div class="col-md-8" style="margin-bottom: 10px;">
    Month: <input type="text" class="monthpicker dateinput" ng-model="monthlyDate" id="monthlyDate" readonly='true'>
	 	   <select class="selectpicker" id="listdataSec" ng-model="companyId" title="Select Company"></select>
		   <input class="fetchExistingExpenses submit btn-primary" type="submit" name="" value="Get" />
    </div>
    <div class="col-md-2" style="float: right;margin-bottom: 5px;">
	 <a style="float: right;" id="downloadPdfID" href="mobileView/downloadDiscountInMobile?company={{companyId}}&date={{pdfDate}}&companyName={{companyName}}">
	 <img alt="pdfImg" src="resources/images/pdf.png" style="float: right; margin-right: 30px;cursor: pointer;"></a>
	</div>
	</div>
	
<div class="row "  id="generatemobileImg">
 <b><span id="companyVal" style="padding: 0px 0px 0px 15px;">{{companyName}} {{enterprice}}</span></b> <!-- <span style=" font-size: 15px; color: red;float: right;"><b style="padding: 0px 15px 0px 0px;">Difference: {{((transactionData | totalDiscountAmt) + (rentalAmt)) - ((receivedData | totalChequeAmount) + (receivedData | adjCheque)) | INR}}</b></span> -->
<div class="col-md-1"></div>
        <div class="col-md-10">
        <table id="">
			    <tbody>
			      <tr>
			        <td style="text-align: center;"><b>Rentals + Total Discount</b></td>
			        <td style="text-align: right;"><b>{{discountPlusRental | INR}}</b></td>
			        </tr>
			        <tr>
			        <td style="text-align: center;"><b>Total Cheques Received</b></td>
			        <td style="text-align: right;"><b>{{totalChequeAmount | INR}}</b></td>
			        </tr>
			         <tr>
			        <td style="text-align: center;"><b>Cheque Adjustments</b></td>
			        <td style="text-align: right;"><b>{{adjCheque | INR}}</b></td>
			      </tr>
			      <tr ng-if="totalVendorDiscount != 0">
			        <td style="text-align: center;"><b>Total Vendor Discount</b></td>
			        <td style="text-align: right;"><b>{{totalVendorDiscount | INR}}</b></td>
			      </tr>
			      <tr ng-if="((totalChequeAmount + adjCheque + totalVendorDiscount + totalVendorCreditReceived) - (discountPlusRental + totalVendorCredit)) <=0">
			        <td style="text-align: center;font-weight: bold;"><b>Amount due</b></td>
			        <td style="text-align: right;font-weight: bold;color: red;"><b>{{((discountPlusRental + totalVendorCredit) - (totalChequeAmount + adjCheque + totalVendorDiscount + totalVendorCreditReceived)) | INR}}</b></td>
			      </tr>
			      <tr ng-if="((totalChequeAmount + adjCheque + totalVendorDiscount + totalVendorCreditReceived) - (discountPlusRental + totalVendorCredit)) > 0">
			        <td style="text-align: center;font-weight: bold;"><b>Amount exceeds</b></td>
			        <td style="text-align: right;font-weight: bold;color: green;"><b>{{((totalChequeAmount + adjCheque + totalVendorDiscount) - (discountPlusRental + totalVendorCredit + totalVendorCreditReceived)) | INR}}</b></td>
			      </tr>
			    </tbody>
			  </table> 
			  <table id="" ng-if="totalVendorDiscount != 0">
					<thead>
						<tr>
							<th>Month</th>
							<th>Vendor Discount Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr	ng-repeat="arrList in arrearsdetails">
							<td style="text-align: center;"><b>{{arrList.date}}</b></td>
							<td style="text-align: right;"><b>{{arrList.vendorAmt | INR}}</b></td>
						</tr>
					 <tr>
			             <td style="background: antiquewhite;"><b>Total Vendor Discount</b></td>
			             <td style="background: antiquewhite;"><b>{{totalVendorDiscount | INR}}</b></td>
			      </tr>
					</tbody>
				</table>
				<table ng-if="totalVendorCredit > 0">
					<thead>
						<tr>
							<th>Vendor Name</th>
							<th>Date</th>
							<th>Product name</th>
							<th>Case/BTL</th>
							<th>Total Amount</th>
						</tr>
					</thead>
					<tbody>
						<tr	ng-repeat="creditList in creditChildBeanList">
							<td style="text-align: center;"><b>{{creditList.name}}</b></td>
							<td style="text-align: center;"><b>{{creditList.date}}</b></td>
							<td style="text-align: center;"><b>{{creditList.productName}}</b></td>
							<td style="text-align: center;"><b>{{creditList.cases}} / {{creditList.bottles}}</b></td>
							<td style="text-align: right;"><b>{{creditList.afterIncludingDiscount | INR}}</b></td>
						</tr>
					    <tr>
				             <td style="background: antiquewhite;" colspan="4"><b>Total</b></td>
				             <td style="background: antiquewhite;"><b>{{totalVendorCredit | INR}}</b></td>
			           </tr>
					</tbody>
				</table>
	 <table id="receivedDiscount">
			    <thead>
			      <tr>
			        <th>Tr. Date</th>
			         <th>Bank</th>
			          <th>Tr. Type</th>
			        <th>Cheque No</th>
			       <th>Cheque Amt</th>
			      </tr>
			    </thead>
			    <tbody>
			      <tr ng-repeat="cval in receivedData | filter: greaterThan('chequeAmount', 0)">
			        <td style="text-align: left;"><b>{{cval.enterprise}}</b></td>
			        <td style="text-align: center;"><b>{{cval.companyName}}</b></td>
			        <td style="text-align: center;"><b>{{cval.comment}}</b></td>
			         <td style="text-align: center;"><b>{{cval.color}}</b></td>
			      <!--   <td><b>{{cval.discountAmount | INR}}</b></td> -->
			        <td><b>{{cval.chequeAmount | INR}}</b></td>
			      </tr>
			       <tr>
			       <td style="background: antiquewhite;" colspan="4"><b>Total Cheques Received</b></td>
			      <!--   <td style="background: antiquewhite;"><b>{{receivedData | totalAmount | INR}}</b></td> -->
			        <td style="background: antiquewhite;"><b>{{totalChequeAmount | INR}}</b></td>
			      </tr>
			     <!--  <tr>
			       <td style="background: antiquewhite;" colspan="4"><b>Rentals + Total Discount</b></td>
			        <td style="background: antiquewhite;"><b>{{((transactionData | totalDiscountAmt)+(rentalAmt))| INR}}</b></td>
			      </tr>
			      <tr>
			        <td style="background: antiquewhite;" colspan="4"><b>Cheque Adjustments</b></td>
			        <td style="background: antiquewhite;"><b>{{ receivedData | adjCheque | INR}}</b></td>
			      </tr>
			      <tr>
			        <td style="background: antiquewhite;color: red;" colspan="4"><b>Difference</b></td>
			        <td style="background: antiquewhite;color: red;"><b>{{((receivedData | totalChequeAmount) + (receivedData | adjCheque)) - ((transactionData | totalDiscountAmt) + (rentalAmt)) | INR}}</b></td>
			      </tr> -->
			    </tbody>
			  </table> 
	          
	              
			    <table id="companyDiscount">
			    <thead>
			      <tr>
			        <th>Month</th>
			         <th>Brand</th>
			        <th>Case</th>
			        <th>Disc/Case</th>
			        <th>Total</th>
			      </tr>
			    </thead>
			    <tbody ng-repeat="list in transactionData">
			      <tr>
			        <td style="text-align: left;"><b>{{list.month}}</b></td>
			        <td style="text-align: left;"><b>{{list.discountMonthWiseBean[0].brand}}</b></td>
			        <td><b>{{list.discountMonthWiseBean[0].case | INR}}</b></td>
			        <td><b>{{list.discountMonthWiseBean[0].discount | INR}}</b></td>
			        <td><b>{{list.discountMonthWiseBean[0].total | INR}}</b></td>
			      </tr>
			      <tr ng-repeat="brand in list.discountMonthWiseBean" ng-if="$index > 0">
			       <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="text-align: left;"><b>{{brand.brand}}</b></td>
			        <td><b>{{brand.case | INR}}</b></td>
			        <td><b>{{brand.discount | INR}}</b></td>
			        <td><b>{{brand.total | INR}}</b></td>
			      </tr>
			       <tr>
			       <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="background: antiquewhite;"><b>{{list.totalDiscount | INR}}</b></td>
			      </tr>
			    </tbody>
			    <tbody>
			     <tr>
			       <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="background: antiquewhite;"><b>Grand Total Discount	</b></td>
			        <td style="background: antiquewhite;"><b>{{totalDiscountAmt | INR}}</b></td>
			      </tr>
			      <tr ng-if="rentalAmt != 0">
			       <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
			        <td style="background: antiquewhite;"><b>Rentals</b></td>
			        <td style="background: antiquewhite;"><b>{{ rentalAmt | INR}}</b></td>
			      </tr>
			    </tbody>
			  </table> 
	  </div>
	  <div class="col-md-1"></div>
</div>
</div>
</div>
</div>
</body>
</html>