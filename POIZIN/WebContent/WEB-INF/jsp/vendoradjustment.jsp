<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <jsp:include page="/WEB-INF/jsp/header.jsp" /> 
<spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
<script src="${jqueryuijs}"></script> 
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://rawgit.com/zorab47/jquery.ui.monthpicker/master/jquery.ui.monthpicker.js"></script>
<spring:url value="/resources/css/bootstrap-select.min.css" var="bootstrapselectmincss"/>
<link rel="stylesheet" href="${bootstrapselectmincss}">
<spring:url value="/resources/javascript/thirdpartyjs/bootstrap-select.min.js" var="bootstrapselectminjs"/>
<script src="${bootstrapselectminjs}"></script> 
<spring:url value="/resources/javascript/common.js" var="commonjs"/>
<script src="${commonjs}"></script> 
<spring:url value="/resources/javascript/vendoradjustment.js" var="vendoradjustmentjs"/>
  <script src="${vendoradjustmentjs}"></script> 
<spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
<link rel="stylesheet" href="${newCommoncss}">
<spring:url value="/resources/css/invoiceEntry.css" var="invoiceEntrycss"/>
 <link rel="stylesheet" href="${invoiceEntrycss}">

</head>
<body ng-app="myApp" ng-controller="myCtrl" ng-init="init()">
<input type="hidden" name="popupmsg" id="popupmsg" value="${message}"  />
<div class="container" style="padding-top: 100px;">
  <div class="row">
    <div class="col-sm-3 bodyFontCss">
    <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Sale</li>
  <li>Rental & Breakage</li>
</ul>
    </div>
 </div>
	 <div class="row">
	 <!-- Rental logic start -->
	 <div class="col-sm-1"></div>
	 <div class="col-sm-5">
	 <div style="text-align: center;">
	  <h3>Rental</h3>
	 </div>
	  <form action="./saveOrUpdateRental" class="form_span" method="post">
	  <label style="font-size: 13px;font-weight: normal;">Select Month</label>
      <input type="text" class="monthpicker form-control" name="rentalMonth" ng-model="rentalMonth" id="rentalMonth" required autocomplete="off"><br>
	  <label style="font-size: 13px;font-weight: normal;">Select Company</label>
      <select id="companylistRental" ng-model="rentalcompanyId" name="rentalcompanyId" class="form-control" ng-change="getselectedCompanyForRental()" required>
      <option  value="">Select Company</option>                                                                                                    
      </select><br>
      <label style="font-size: 13px;font-weight: normal;">Rental Amount</label>
      <input type="number" class=" form-control" name="rentalAmount" ng-model="rentalAmount" id="rentalAmount" required autocomplete="off"><br>
      <div style="text-align: center;">
      <input class="btn btn-default btn-rounded" type="submit" name="" value="SUBMIT"/>
      </div>
	  </form>
	 </div>
	 <!-- Rental logic start -->
	 <!-- Breakage logic start -->
	 <div class="col-sm-5">
	 <div style="text-align: center;">
	  <h3>Breakage</h3>
	 </div>
	<form action="./saveOrUpdateBreakage" class="form_span" method="post">
	  <label style="font-size: 13px;font-weight: normal;">Select Month</label>
      <input type="text" class="monthpicker form-control" name="breakageMonth" ng-model="breakageMonth" id="breakageMonth" required autocomplete="off"><br>
      <label style="font-size: 13px;font-weight: normal;">Rental Amount</label>
      <input type="number" class=" form-control" name="breakageAmount" ng-model="breakageAmount" id="breakageAmount" required autocomplete="off"><br>
      <label style="font-size: 13px;font-weight: normal;">Comment</label>
      <textarea name="breakagecomment" ng-model="breakagecomment" class=" form-control" id="breakagecomment" maxlength="440"></textarea>
      <div style="text-align: center;">
      <input class="btn btn-default btn-rounded" type="submit" name="" value="SUBMIT"/>
      </div>
	  </form>
	 </div>
	 <div class="col-sm-1"></div>
	 <!-- Breakage logic End -->
	 </div>
 
</div>
</body>
</html>