<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
<title>www.poizin.com</title>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
 <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
 <link href="https://fonts.googleapis.com/css?family=Michroma" rel="stylesheet">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<%--  <spring:url value="/resources/css/3.3.7.bootstrap.min.css" var="bootstrap1mincss"/>
   <link rel="stylesheet" href="${bootstrap1mincss}">  --%>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
   <script src="${jqueryminjs}"></script> 
    <spring:url value="/resources/javascript/thirdpartyjs/angular.min.js" var="angularminjs"/>
  <script src="${angularminjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/angular-filter.js" var="angularfilterjs"/>
  <script src="${angularfilterjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
   <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
   <script src="${bootstrapminjs}"></script> 
   <spring:url value="/resources/javascript/thirdpartyjs/jquery-confirm.min.js" var="jqueryconfirmminjs"/>
   <script src="${jqueryconfirmminjs}"></script> 
    <!-- Bootstrap CSS-->
   <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
   <spring:url value="/resources/css/theme.css" var="themecss"/>
   <link rel="stylesheet" href="${themecss}"> 
    <spring:url value="/resources/javascript/fusioncharts/fusioncharts.js" var="fusionjs" />
<script src="${fusionjs}"></script>
<spring:url value="/resources/javascript/fusioncharts/themes/fusioncharts.theme.fint.js" var="fusionthemejs" />
<script src="${fusionthemejs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
  <spring:url value="/resources/javascript/thirdpartyjs/canvasjs.min.js" var="canvasjsminjs"/>
   <script src="${canvasjsminjs}"></script> 
  <spring:url value="/resources/css/bootstrap.min.css" var="bootstrapmincss"/>
  <link rel="stylesheet" href="${bootstrapmincss}">
    <spring:url value="/resources/javascript/dashboard.js" var="dashboardjs"/>
   <script src="${dashboardjs}"></script> 

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
<%-- <spring:url value="/resources/css/font-awesome.css" var="fontawesomecss"/>
<link rel="stylesheet" href="${fontawesomecss}"> --%>
<spring:url value="/resources/newTemp/css/bootstrap.min.css" var="bootstrapmincss"/>
<link rel="stylesheet" href="${bootstrapmincss}">
<spring:url value="/resources/newTemp/css/mdb.min.css" var="bootstrapmincss"/>
<link rel="stylesheet" href="${bootstrapmincss}">
   <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}">
   <style type="text/css">
   .datalist {
  max-height:calc(100vh - 278px);
  overflow-y: auto;
  width: 100%;
}
/* width */
::-webkit-scrollbar {
    width: 5px;
}

/* Track */
::-webkit-scrollbar-track {
    background: #f1f1f1; 
}
 
/* Handle */
::-webkit-scrollbar-thumb {
    background: #888; 
}

/* Handle on hover */
::-webkit-scrollbar-thumb:hover {
    background: #555; 
}
summary{
display : block;
}
details summary::-webkit-details-marker { display:none; }
.stockLiftList span {
    text-align: center;
    box-sizing: border-box;
    overflow: hidden;
    text-overflow: ellipsis;
    display: inline-block;
    white-space: nowrap;
    font-size: 12px;
    text-align: center;
   /*  padding: 8px; */
    /* color: #000; */
}
:focus {
    outline: #fff auto 0px;
}
.stockLift {
    width: 100%;
    background: #f0f0f0;
}
 .stockLiftSpan {
       height: 30px;
    border-bottom: 1px solid #e0e0e0;
    font-size: 12px;
    background: #243a51;
    font-weight: 600;
    color: #fff;
}
.evenrow{
	background-color:#81DAF5!important;
	font-family: 'Montserrat', sans-serif !important;
	font-size:12px;
	color:#424242;
}
.oddrow{
	background-color:#E6E6E6!important;
	font-family: 'Montserrat', sans-serif !important;
	font-size:12px;
	color:#424242;
}
  span.tip {
    /* border-bottom: 1px dashed; */
    text-decoration: none;
    list-style-type: none;
}
span.tip:hover {
   /*  cursor: help; */
    position: relative;
}
span.tip ul {
    display: none
}
span.tip ul > li {
        line-height: 20px;
        list-style-type: none; 
         text-align: left;
}
span.tip:hover ul {
     border: #c0c0c0 1px dotted;
    padding: 5px 20px 5px 5px;
    display: block;
    z-index: 100;
    left: 0px;
    margin: 10px;
    width: 150px;
    position: absolute;
    top: 10px;
    text-decoration: none;
    background: #243a51;
    color: #fff;
    font-family:  'Montserrat', sans-serif !important;;
    font-size: 12px;
    
}
.nav_title_css{
 font-family: 'Michroma', sans-serif;
  font-size: 18px;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
   </style>
<script type="text/javascript">
  $(document).ready(function() {
		if(!($("#loginPopup").val() > 0)){
			$("#loginModelPopup").modal("show");
		}
	});
  
  </script>
</head>
<body ng-app="myApp" ng-controller="myCtrl" class="hidden-sn mdb-skin page-content--bgf7">
     <c:if test="${sessionScope['scopedTarget.userSession'].loginId == NAN}">
<div class="whitebackground"></div>
</c:if>
    <!--Double navigation-->
     <c:if test="${sessionScope['scopedTarget.userSession'].loginId > 0}">
    <header>
        <!-- Sidebar navigation -->
        <div id="slide-out" class="side-nav sn-bg-4 mdb-sidenav" style="    width: 22rem;">
            <ul class="custom-scrollbar list-unstyled" style="max-height:100vh;">
                <!-- Logo -->
                <li>
                    <div class="logo-wrapper waves-light" style="    padding: 25px;">
                    <p class="nav_title_css">${sessionScope['scopedTarget.userSession'].storeName}</p>
                    </div>
                </li>
                <!--/. Logo -->
               
                <!-- Side navigation links -->
                <li>
                    <ul class="collapsible collapsible-accordion">
                        <li><a class="collapsible-header waves-effect arrow-r inlineHeadrCss"><i class="fa fa-chevron-right"></i> Invoice <i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="addNewBrand" class="waves-effect inlineAchCss">Add Product</a>
                                    </li>
                                    <li><a href="saveInvoice" class="waves-effect inlineAchCss">Add Invoice</a>
                                    </li>
                                     <li><a href="ocrInvoice" class="waves-effect inlineAchCss">Add OCR Invoice</a>
                                    </li>
                                    <li><a href="productList" class="waves-effect inlineAchCss">Active/Inactive Products</a>
                                    </li>
                                    <li><a href="UploadAndViewInvoice" class="waves-effect inlineAchCss">Upload/View Invoice</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li><a class="collapsible-header waves-effect arrow-r inlineHeadrCss"><i class="fa fa-chevron-right"></i> Sale<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="sale" class="waves-effect inlineAchCss">Enter Sale</a>
                                    </li>
                                    <li><a href="updateSale" class="waves-effect inlineAchCss">Edit Sale</a>
                                    </li>
                                    <li><a href="monthlyExpenses" class="waves-effect inlineAchCss">Monthly Debit/Credit</a>
                                    </li>
                                    <li><a href="investment" class="waves-effect inlineAchCss">Enter Investment</a>
                                    </li>
                                    <li><a href="discountTransaction" class="waves-effect inlineAchCss">Add Cheque</a>
                                    </li>
                                    <li><a href="credit" class="waves-effect inlineAchCss">Add Credits</a>
                                    </li>
                                     <!-- <li><a href="mobileViewEnterSaleSheet" class="waves-effect inlineAchCss">Generate Sale Sheet</a>
                                    </li> -->
                                </ul>
                            </div>
                        </li>
                        <li><a class="collapsible-header waves-effect arrow-r inlineHeadrCss"><i class="fa fa-chevron-right"></i> Reports<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="saleReport" class="waves-effect inlineAchCss">Sale Report</a>
                                    </li>
                                    <li><a href="newSaleReport" class="waves-effect inlineAchCss">New Sale Report</a>
                                    </li>
                                    <li><a href="saleDayWiseReport" class="waves-effect inlineAchCss">Day Wise Sale Report</a>
                                    </li>
                                    <li><a href="stockLifting" class="waves-effect inlineAchCss">Stock Lift</a>
                                    </li>
                                    <li><a href="inHouseStockValue" class="waves-effect inlineAchCss">InHouse Stock</a>
                                    </li>
                                    <li><a href="balanceSheet" class="waves-effect inlineAchCss">Balance Sheet</a>
                                    </li>
                                    <li><a href="productComparisionCategoryWise" class="waves-effect inlineAchCss">Product Comparison</a>
                                    </li>
                                    <li><a href="saleWithDiscount" class="waves-effect inlineAchCss">Sale With Discount</a>
                                    </li>
                                    <li><a href="expensesReport" class="waves-effect inlineAchCss">Expense Report</a>
                                    </li>
                                    <li><a href="profitAndLossReport" class="waves-effect inlineAchCss">Profit/Loss Report</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li><a class="collapsible-header waves-effect arrow-r inlineHeadrCss"><i class="fa fa-chevron-right"></i>Discount<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul>
                                   <li><a href="discountEstimate" class="waves-effect inlineAchCss">Discount Estimate</a>
                                    </li>
                                    <!-- <li><a href="newDiscountEstimate" class="waves-effect inlineAchCss">Discount Estimate</a>
                                    </li> -->
                                    <li><a href="newIndentEstimate" class="waves-effect inlineAchCss">Indent Estimate</a>
                                    </li>
                                   <!--  <li><a href="tillMonthStockLiftWithDiscount" class="waves-effect inlineAchCss">Company Discount</a>
                                    </li> -->
                                    <li><a href="viewDiscount" class="waves-effect inlineAchCss">Company Discount</a>
                                    </li>
                                    <li><a href="stockLiftWithDiscountTransaction" class="waves-effect inlineAchCss">Enter Discount</a>
                                    </li>
                                    <li><a href="generateDiscountPdf" class="waves-effect inlineAchCss">Discount Pdf</a>
                                    </li>
                                    <li><a href="discountcalculation" class="waves-effect inlineAchCss">Discount Calculation</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li><a class=" waves-effect arrow-r inlineHeadrCss" href="./logout"> <i class="fa fa-chevron-right"></i>Logout</a></li>
                    </ul>
                </li>
                <!--/. Side navigation links -->
            </ul>
            <div class="sidenav-bg mask-strong"></div>
        </div>
        <!--/. Sidebar navigation -->
        <!-- Navbar -->
        
        <nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav" style="border-radius: 0px;">
            <!-- SideNav slide-out button -->
             <c:if test="${sessionScope['scopedTarget.userSession'].loginId > 0}">
            <div class="float-left">
                <a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
            </div>
            <!-- Breadcrumb-->
            <div class="breadcrumb-dn mr-auto">
                <a href="admin" class="waves-effect inlineAchCss"><p style=" font-family: 'Michroma', sans-serif; font-size: 23px;">${sessionScope['scopedTarget.userSession'].storeName}</p></a>
            </div>
            <ul class="nav navbar-nav nav-flex-icons ml-auto">
                <li class="nav-item">
                    <a class="nav-link achPadding" href="saleReport"><span class="clearfix d-none d-sm-inline-block">Sale Report</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link achPadding" href="stockLifting"><span class="clearfix d-none d-sm-inline-block">Stock Lift</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link achPadding" href="inHouseStockValue"><span class="clearfix d-none d-sm-inline-block">InHouse Stock</span></a>
                </li>
               
            </ul>
             </c:if>
        </nav>
        
        <!-- /.Navbar -->
    </header>
    </c:if>
    <!--/.Double navigation-->
    
    <!--Main Layout-->
    <main>
        <div class="">
            <div class="page-wrapper">
    
    <c:if test="${sessionScope['scopedTarget.userSession'].loginId > 0}">
        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <!-- WELCOME-->
           <!--  <section class="welcome p-t-10">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="title-4">Welcome back
                                <span>John!</span>
                            </h1>
                            <hr class="line-seprate">
                        </div>
                    </div>
                </div>
            </section> -->
            <!-- END WELCOME-->

            <!-- STATISTIC-->
            <section class="statistic statistic2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--green">
                            <a href="saleReport" style="width: 100%;">
                                <h2 class="number">{{totalSaleValue | INR}}</h2>
                                <span class="desc">Sale</span>
                                <div class="icon">
                                    <i class="zmdi zmdi-account-o"></i>
                                </div>
                                </a> 
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--orange">
                            <a href="inHouseStockValue" style="width: 100%;">
                                <h2 class="number">{{inHouseStockValue | INR}}</h2>
                                <span class="desc">In house stock</span>
                                <div class="icon">
                                    <i class="zmdi zmdi-shopping-cart"></i>
                                </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--blue">
                             <a href="stockLifting" style="width: 100%;">
                               <h2 class="number">{{comulativeValue | INR}}</h2>
                                <span class="desc">threshold</span>
                                <div class="icon">
                                    <i class="zmdi zmdi-calendar-note"></i>
                                </div>
                                </a>
                            </div>
                            <span style="float: right;top: -93px;right: 5px;color: #fff;font-size: 17px;" class="glyphicon glyphicon-info-sign tip">
                            <ul>
							<li>Date : {{retailerCreditBalance.date}}</li>
							<li>Balance : {{retailerCreditBalance.value  | INR}}</li>
							</ul>
                            </span>
                        </div>
                        <div class="col-md-6 col-lg-3">
                            <div class="statistic__item statistic__item--red">
                              <a href="viewDiscount" style="width: 100%;">
                                <h2 class="number">{{discountDues | INR}}</h2>
                                <span class="desc">Discount dues</span>
                                <div class="icon">
                                    <i class="zmdi zmdi-money"></i>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END STATISTIC-->

            <!-- STATISTIC CHART-->
            <section class="statistic-chart">
                <div class="container-fluid containerGraphFluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35"></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-lg-4">
                            <!-- CHART-->
                            <div class="statistic-chart-1">
                            <div id="total_sale_chart_container_Month_wise" style="height: 340px;"></div> 
                            </div>
                            <!-- END CHART-->
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <!-- TOP CAMPAIGN-->
                            <div class="top-campaign" style="padding-top: 38px;color: #000000 !important;">
                                <h3 class="title-3 m-b-30" style="color: #000000;text-align: center;">Top Selling Products</h3>
                                <div class="table-responsive">
                                 <div id="" style="height: 300px;">
                                    <table class="table table-top-campaign">
                                        <tbody>
                                            <tr ng-repeat="brand in maxSaleProduct  | orderBy:['-value'] ">
                                                <td>{{brand.date}}</td>
                                                <td data-toggle="modal" data-target="#basicExampleModal" ng-click="openModalPopup(brand.date,brand.linechartData)">{{brand.value | INR}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                            <!-- END TOP CAMPAIGN-->
                        </div>
                        <div class="col-md-6 col-lg-4">
                            <!-- CHART PERCENT-->
                             <div class="top-campaign" style="padding-top: 38px;color: #000000 !important;">
                                <h3 class="title-3 m-b-30" style="color: #000000;text-align: center;">Highest Discount Dues</h3>
                                 <div class="table-responsive">
                                 <div id="" style="height: 300px;">
                                    <table class="table table-top-campaign">
                                        <tbody>
                                            <tr ng-repeat="list in companyDiscountDues | orderBy:'-arrears' | limitTo:8">
                                                <td ng-click="listOutAllDiscount(list.company,list.companyName)">{{list.companyName}}
                                                </td>
                                                <td ng-click="listOutAllDiscount(list.company,list.companyName)">{{list.arrears | INR}}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>  
                            <!-- END CHART PERCENT-->
                        </div>
                    </div>
                </div>
            </section>
            <!-- END STATISTIC CHART-->
            
            <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
            
            
             <!-- STATISTIC CHART-->
            <section class="statistic-chart">
                <div class="container-fluid containerGraphFluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="title-5 m-b-35"></h3>
                        </div>
                    </div>
                    <div class="row">
                        <!-- <div class="col-md-6 col-lg-4">
                           <div class="top-campaign" style="padding-top: 38px;color: #000000 !important;">
                                <h3 class="title-3 m-b-30" style="color: #000000;text-align: center;">Low Stock Products</h3>
                                <div class="table-responsive">
                                 <div id="" style="height: 300px;">
                                    <table class="table table-top-campaign">
                                        <tbody>
                                            <tr ng-repeat="brand in lowStockProduct">
                                                <td>{{brand.brandName}} ({{brand.packType}})</td>
                                                <td>{{brand.closing}}/{{brand.qtyBottels}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div> -->
                       <!--  <div class="col-md-6 col-lg-4">
                            TOP CAMPAIGN
                             <div class="top-campaign" style="padding-top: 38px;color: #000000 !important;">
                                <h3 class="title-3 m-b-30" style="color: #000000;text-align: center;">Highest Discount Dues</h3>
                                 <div class="table-responsive">
                                 <div id="" style="height: 300px;">
                                    <table class="table table-top-campaign">
                                        <tbody>
                                            <tr ng-repeat="list in companyDiscountDues | orderBy:'-arrears' | limitTo:8">
                                                <td ng-click="listOutAllDiscount(list.company,list.companyName)">{{list.companyName}}
                                                </td>
                                                <td ng-click="listOutAllDiscount(list.company,list.companyName)">{{list.arrears | INR}}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>  
                            END TOP CAMPAIGN
                        </div> -->
                        <div class="col-md-12 col-lg-8">
                            <div id="doughnutChart" style="height: 400px;"></div>
                        </div>
                        <div class="col-md-6 col-lg-4">
                           <div class="top-campaign" style="padding-top: 38px;color: #000000 !important;">
                                <h3 class="title-3 m-b-30" style="color: #000000;text-align: center;">Low Stock Products</h3>
                                <div class="table-responsive">
                                 <div id="" style="height: 300px;">
                                    <table class="table table-top-campaign">
                                        <tbody>
                                            <tr ng-repeat="brand in lowStockProduct">
                                                <td>{{brand.brandName}} ({{brand.packType}})</td>
                                                <td>{{brand.closing}}/{{brand.qtyBottels}}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END STATISTIC CHART-->

          
        </div>
      </c:if>
    </div>
        </div>
    </main>
   
 <!-- Login Form Start -->   
<%-- <input type="hidden" name="popup" id="popup" value="${message}" /> --%>
<input type="hidden" name="loginPopup" id="loginPopup" value="${sessionScope['scopedTarget.userSession'].loginId}"/>
<div class="modal fade" id="loginModelPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold homeModelHeader">POIZIN</h4>
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> -->
      </div>
      <label for="defaultForm-pass" style="color: red;padding: 10px 0px 0px 20px;">${message}</label>
   
       <form action="./profileLogin" class="form_span" id="" method="post">
    
<!--Form with header-->

    <div class="card-block" style="padding: 25px;">


        <!--Body-->
        <div class="md-form">
            <i style= "padding: 0px 10px 0px 2px"; class="fa fa-user prefix"></i>
            <input  type="text" id="userName" name="userName" class="form-control">
            <label  for="form2">Enter UserMail</label>
        </div>

        <div class="md-form">
            <i style= "padding: 0px 10px 0px 2px"; class="fa fa-lock prefix"></i>
            <input type="password" id="password" name="password" class="form-control">
            <label  for="form4">Enter Password</label>
          <%--   <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" /> --%>
        </div>
        

      <div class="modal-footer d-flex justify-content-center">
        <div style="float: right;">
        <input class="btn btn-default btn-rounded" style=" background-color: #243a51 !important;padding: 8px 25px 8px 25px;font-size: 12px;" type="submit" name="" value="Login"/>
        </div>
      
      </div>

    </div>

    <!--Footer-->
   <!--  <div class="modal-footer">
        <div class="options">
            <p style="font-size:12px">Not a member? <a href="#">Sign Up</a></p>
            <p style="font-size:12px" >Forgot <a href="#" ng-click="forgotPassword()">Password?</a></p>
        </div>
    </div> -->


<!--/Form with header-->
      
      
      
      
       </form>
    </div>
  </div>
</div>
<!-- Login Form End -->


 <!-- Forgot Form Start -->   
<%-- <input type="hidden" name="popup" id="popup" value="${message}" /> --%>
<input type="hidden" name="forgotPopup" id="forgotPopup" value="${sessionScope['scopedTarget.userSession'].loginId}"/>
<div class="modal fade" id="forgotModelPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold homeModelHeader">POIZIN</h4>
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> -->
      </div>
      <label for="defaultForm-pass" style="color: red;padding: 10px 0px 0px 20px;">${message}</label>
   
       <form  ng-submit="processForgotPassword()" >
    
<!--Form with header-->

    <div class="card-block">


        <!--Body-->
        <div class="md-form">
            <i style= "padding: 0px 10px 0px 2px"; class="fa fa-user prefix"></i>
            <input  type="text" id="forgetName" ng-model="forgetName" name="forgetName" class="form-control" required>
            <label>Enter UserName</label>
        </div>

      
        

      <div class="modal-footer d-flex justify-content-center">
        <div style="float: right;">
        <input class="btn btn-default btn-rounded" style=" background-color: #243a51 !important;padding: 8px 25px 8px 25px;font-size: 12px;" type="submit" name="" value="Forgot Password"/>
        </div>
      
      </div>

    </div>

   


<!--/Form with header-->
      
      
      
      
       </form>
    </div>
  </div>
</div>
<!-- Forgot  Form End -->

<!-- Central Modal Small -->
<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="true" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="font-size: 25px;">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="chart-container" style="width: 100%;height: 300px;"></div>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
  <!-- Central Modal Small -->
  <!-- Comparision Popup -->
<div class="modal fade" id="comparisonPupop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="true" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <h3>{{catLabel}}  ({{currentDate}})</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="font-size: 25px;">&times;</span>
          </button>
        </div>
        <div class="modal-body" style="height: 500px;">
        <div id="doughnutChartForACat"></div>
        
        
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
  <!-- Comparision Popup -->
  <!-- Discount Popup -->
<div class="modal fade" id="discountPupop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="true" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <h3 style="font-weight: bold;">{{DiscountCompanyName}}</h3>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="font-size: 25px;">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        <table class=" table table-fixed" style="margin-bottom: 0px;">
        <thead><tr>
					<th class="col-xs-3 fontfamily" style="text-align: left;background-color: #243a51;font-size: 11px;color: #fff;font-weight: 600;" data-toggle="tooltip" data-placement="bottom" title="Company" style="text-align: left;">Month</th>
					<th class="col-xs-2 fontfamily" style="text-align: right;background-color: #243a51;font-size: 11px;color: #fff;font-weight: 600;" data-toggle="tooltip" data-placement="bottom" title="Cheque">Discount</th>
					<th class="col-xs-2 fontfamily" style="text-align: right;background-color: #243a51;font-size: 11px;color: #fff;font-weight: 600;" data-toggle="tooltip" data-placement="bottom" title="Rental">Rental</th>
					<th class="col-xs-2 fontfamily" style="text-align: right;background-color: #243a51;font-size: 11px;color: #fff;font-weight: 600;" data-toggle="tooltip" data-placement="bottom" title="Comment">Cheque</th>
					<th class="col-xs-3 fontfamily" style="text-align: right;background-color: #243a51;font-size: 11px;color: #fff;font-weight: 600;" data-toggle="tooltip" data-placement="bottom" title="Comment">Arrears</th>
					</tr></thead>
        </table>
        <div class="datalist">
         <table class=" table table-fixed">
					 <tbody class="">
	                 <tr ng-repeat="list in DiscountDetails | filter: greaterThanZeroAllCompany('discountAmount','chequeAmount','arrears', 0)" data-toggle="modal" data-target="#myModalbacardiCompany" ng-click="openListDiscontsAllCompany(list.comment,list.enterprise,list.discountAmount,list.rentals,list.chequeAmount,list.arrears,list.creditAmt)">
	                 <td class="col-xs-3 fontfamily" style="text-align: left;color: #000;font-size: 11px;">{{list.comment}}</td>
						<td class="col-xs-2 fontfamily" style="text-align: right;color: #000;font-size: 11px;">{{list.discountAmount | INR}}</td>
						<td class="col-xs-2 fontfamily" style="text-align: right;color: #000;font-size: 11px;">{{list.rentals | INR}}</td>
						<td class="col-xs-2 fontfamily" style="text-align: right;color: #000;font-size: 11px;">{{list.chequeAmount | INR}}</td>
						<td class="col-xs-3 fontfamily" style="text-align: right;color: #000;font-size: 11px;">{{list.arrears | INR}}</td>
	                </tr>
                 </tbody>
         </table>
         </div>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
  <!-- Discount Popup -->
  <div class="modal fade sbi2-popup-styles" id="myModalbacardiCompany" role="dialog">
	    <div class="modal-dialog modal-lg">
	      <div class="modal-content">
	        <div class="modal-header">
	           <h4 class="modal-title"><span>Date: {{dateDash}} | Discount: {{ttlDiscountDash | INR}} |  Rental: {{rentalsAmtDash | INR}} | Arrears: {{aarearsDash | INR}} | Vendor Credit Amount: {{creditAmount | INR}}</span></h4> 
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        </div>
	        <div class="modal-body" style="overflow: scroll;height: 500px;">
	        
		    <div class="stockLiftSpan" style="height:30px;padding: 5px;border-bottom: 1px solid #e0e0e0;">
						    <span class="col-xs-4" style="text-align: left;">Brand Name</span>
						    <span class="col-xs-4" style="text-align: center;">Lifted Case</span>
						    <span class="col-xs-4" style="text-align: center;">Discount</span>
						</div>
				<details ng-repeat="list in tempData | filter: greaterThan('totalCaseQty', 0)" ng-class="$even ? 'oddrow' : 'evenrow'">
					<summary>
						<div class="stockLiftList shiva" style="height:30px;padding: 5px;border-bottom: 1px solid #e0e0e0;">
						    <span class="col-xs-4" style="color: #000;text-align: left;">{{list.productName}}</span>
						    <span class="col-xs-4" style="color: #000;text-align: center;">{{list.totalCaseQty}}</span>
						    <span class="col-xs-4" style="color: #000;text-align: center;">{{list.totalDiscount | INR}}</span>
						</div>
					</summary>
					<table class=" table table-fixed">
						   <thead><tr>
						    <th class="col-xs-3" style="background: #1a6398 !important;color: #fff;font-size: 12px;">Brand Name</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;color: #fff;font-size: 12px;">Brand No</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;color: #fff;font-size: 12px;">Lifted Case</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;color: #fff;font-size: 12px;">Discount</th>
						    <th class="col-xs-3" style="background: #1a6398 !important;color: #fff;font-size: 12px;">Total Discount</th>
						    </tr></thead>
						    <tr ng-repeat="brand in list.Brands | filter: greaterThan('caseQty', 0)">
							<td class="col-xs-3" style="color: #000;background-color: #fff;font-size: 12px;}">{{brand.brandName}}</td>
							<td class="col-xs-2" style="color: #000;background-color: #fff;font-size: 12px;">{{brand.brandNo}}</td>
							<td class="col-xs-2" style="color: #000;background-color: #fff;font-size: 12px;">{{brand.caseQty}}</td>
							<td class="col-xs-2" style="color: #000;background-color: #fff;font-size: 12px;">{{brand.unitPrice}}</td>
							<td class="col-xs-3" style="color: #000;background-color: #fff;font-size: 12px;">{{brand.totalPrice | INR}}</td>
							</tr>
						  </table>
					</details>
	        </div>
	      </div>
	    </div>
  </div>
   <spring:url value="/resources/newTemp/js/jquery-3.3.1.min.js" var="jquery331minjs"/>
  <script src="${jquery331minjs}"></script>
   <spring:url value="/resources/newTemp/js/popper.min.js" var="popperminjs"/>
  <script src="${popperminjs}"></script>
 <spring:url value="/resources/newTemp/js/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script>
   <spring:url value="/resources/newTemp/js/mdb.min.js" var="mdbminjs"/>
  <script src="${mdbminjs}"></script>
  <script>
   $(".button-collapse").sideNav();
    </script>
</body>
</html>