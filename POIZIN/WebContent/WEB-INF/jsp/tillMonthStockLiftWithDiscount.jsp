<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<jsp:include page="/WEB-INF/jsp/headerWithNav.jsp" />  
<spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script>
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
  <script src="https://rawgit.com/zorab47/jquery.ui.monthpicker/master/jquery.ui.monthpicker.js"></script>
  <spring:url value="/resources/javascript/tillMonthStockLiftWithDiscount.js" var="productPerformancejs"/>
  <script src="${productPerformancejs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
    <spring:url value="/resources/css/bootstrap-select.min.css" var="bootstrapselectmincss"/>
   <link rel="stylesheet" href="${bootstrapselectmincss}">
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap-select.min.js" var="bootstrapselectminjs"/>
  <script src="${bootstrapselectminjs}"></script>
  <spring:url value="/resources/css/balanceSheet.css" var="balanceSheetcss"/>
  <link rel="stylesheet" href="${balanceSheetcss}">
       <spring:url value="/resources/css/stockLiftDiscount.css" var="stockLiftDiscountcss"/>
   <link rel="stylesheet" href="${stockLiftDiscountcss}">
  <script src='https://cdn.rawgit.com/simonbengtsson/jsPDF/requirejs-fix-dist/dist/jspdf.debug.js'></script>
  <script src='https://unpkg.com/jspdf-autotable@2.3.2'></script>
  
  <style type="text/css">
  details summary::-webkit-details-marker { display:none; }
  .stockLiftSpan {
       height: 30px;
    padding: 5px;
    border-bottom: 1px solid #e0e0e0;
    font-size: 12px;
    background: #243a51;
    font-weight: 600;
    color: #fff;
}

.stockLiftList span {
    text-align: center;
    box-sizing: border-box;
    overflow: hidden;
    text-overflow: ellipsis;
    display: inline-block;
    white-space: nowrap;
    font-size: 12px;
    text-align: center;
  /*   padding: 8px; */
    color: #000;
}

.fontfamily {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
}
  </style>
</head>
<body ng-app="myApp" ng-controller="myCtrl" style="background: #f7f7f7">
<div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
 <div class="row" style="padding-top: 100px;">
  <div class="col-sm-3 bodyFontCss" style="padding: 0px !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Discount</li>
  <li>Company Discount</li>
</ul>
  </div>
  <div ng-hide = "IsHidden">
  <div class="col-sm-2 bodyFontCss" >
   <label>Select Till Month</label>
   <input type="text" class="monthpicker form-control" id="tillDateDiscountDate" readonly='true'>
  </div>
  <div class="col-sm-2 bodyFontCss">
   <label>Select Company</label>
  <select name="debitCredit" id="listdataSec" ng-model="companyId" required="required" class="form-control"></select>
  </div>
  <div class="col-sm-2 bodyFontCss" style="padding-top: 15px;">
   <input class="fetchTillDateStockLift btn btn-default btn-rounded" type="submit" name="" ng-click="getResults()" value="GET DATA" />
  </div>
  </div>
  <!-- tab pan 4 div for month picker Start -->
  <div ng-hide = "tabFourHidden">
   <div class="col-sm-2 bodyFontCss" style="text-align: right;"><label id="selectedMonth" style="margin-top: 15px;"></label></div>
  <div class="col-sm-2 bodyFontCss" >
   <label>Select Month</label>
   <input type="text" class="monthpicker form-control" id="discountMonthPicker" readonly='true'>
  </div>
  <div class="col-sm-2 bodyFontCss" style="padding-top: 15px;">
   <input class=" btn btn-default btn-rounded" type="submit" name="" ng-click="getSelectedMonthDiscount()" value="GET DATA" />
  </div>
  </div>
  <!-- tab pan 4 div for month picker end -->
  <div class="col-sm-3 bodyFontCss">
  </div>
</div>
 <div class="container" style="margin-top: 20px;">
<div class="row">
		   <div class="col-sm-2">
		   <ul class="nav nav-tabs tabs-left sideways">
		    <li  class="active"><a href="#allCompanyDiscount-v" data-toggle="tab" ng-click="ShowHide(0)">ALL COMPANY DISCOUNT</a></li>
            <li><a href="#home-v" data-toggle="tab" ng-click="ShowHide(1)">TILL MONTH DISCOUNT</a></li>
            <li><a href="#profile-v" data-toggle="tab" ng-click="ShowHide(1)">COMPANY WISE DISCOUNT</a></li>
            <li><a href="#discountForSingleMonth-v" data-toggle="tab" ng-click="tabHidden()">MONTH WISE DISCOUNT</a></li>
           </ul>
		  </div>

         <div class="col-sm-10">
          <div class="tab-content" style="padding: 0px;">
            <div class="tab-pane" id="home-v">
            <b><span id="companyVal" style="padding: 0px 0px 0px 15px;font-size: 15px;">{{companyName}} {{enterprice}}</span></b>
            <table class=" table table-fixed" id="bacardiCompany">
			  <thead><tr>
				<th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Month" style="text-align: left;">Month</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Discount" style="text-align: right;">Discount</th>
				<th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Rental" style="text-align: right;">Rental</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Total" style="text-align: right;">Total</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Received" style="text-align: right;">Received</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Received" style="text-align: right;">Adjustment</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Dues" style="text-align: right;">Arrears</th>
				</tr></thead>
				<tr ng-repeat="list in alldetails | filter: greaterThanZero('discounts','rentals','received','adjustmentCheque','dues', 0)">
				<td class="col-xs-1" data-toggle="modal" data-target="#myModalbacardiCompany" ng-click="openListDisconts(list.saleBean,list.discountAsPerCompanyBeenList,list.date,list.dues,list.discounts,list.rentals)" title="{{list.date}}" ng-class="$even ? 'oddrow' : 'evenrow'" style="text-align: left;">{{list.date}}</td>
				<td class="col-xs-2" data-toggle="modal" data-target="#myModalbacardiCompany" ng-click="openListDisconts(list.saleBean,list.discountAsPerCompanyBeenList,list.date,list.dues,list.discounts,list.rentals)" title="{{list.discounts | INR}}" ng-class="$even ? 'oddrow' : 'evenrow'" style="text-align: right;cursor: pointer;">{{list.discounts | INR}}</td>
				<td class="col-xs-1" data-toggle="modal" data-target="#myModalbacardiCompany" ng-click="openListDisconts(list.saleBean,list.discountAsPerCompanyBeenList,list.date,list.dues,list.discounts,list.rentals)" title="{{list.rentals | INR}}" ng-class="$even ? 'oddrow' : 'evenrow'" style="text-align: right;">{{list.rentals | INR}}</td>
				<td class="col-xs-2" data-toggle="modal" data-target="#myModalbacardiCompany" ng-click="openListDisconts(list.saleBean,list.discountAsPerCompanyBeenList,list.date,list.dues,list.discounts,list.rentals)" title="{{list.total | INR}}" ng-class="$even ? 'oddrow' : 'evenrow'" style="text-align: right;">{{list.total | INR}}</td>
				<td class="col-xs-2" data-toggle="modal" data-target="#myModalbacardiCompany" ng-click="openListDisconts(list.saleBean,list.discountAsPerCompanyBeenList,list.date,list.dues,list.discounts,list.rentals)" title="{{list.received | INR}}" ng-class="$even ? 'oddrow' : 'evenrow'" style="text-align: right;">{{list.received | INR}}</td>
				<td class="col-xs-2" data-toggle="modal" data-target="#myModalbacardiCompany" ng-click="openListDisconts(list.saleBean,list.discountAsPerCompanyBeenList,list.date,list.dues,list.discounts,list.rentals)" title="{{list.adjustmentCheque | INR}}" ng-class="$even ? 'oddrow' : 'evenrow'" style="text-align: right;">{{list.adjustmentCheque | INR}}</td>
				<td class="col-xs-2" data-toggle="modal" data-target="#myModalbacardiCompany" ng-click="openListDisconts(list.saleBean,list.discountAsPerCompanyBeenList,list.date,list.dues,list.discounts,list.rentals)" title="{{list.dues | INR}}" ng-class="$even ? 'oddrow' : 'evenrow'" style="text-align: right;">{{list.dues | INR}}</td>
				</tr>
				<tr>
				<td class="col-xs-1" style="text-align: left;font-size: 12px; font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">Total</td>
				<td class="col-xs-2" style="text-align: right;font-size: 12px; font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">{{alldetails | sumDiscounts | INR}}</td>
				<td class="col-xs-1" style="text-align: right;font-size: 12px; font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">{{alldetails | sumRental | INR}}</td>
				<td class="col-xs-2" style="text-align: right;font-size: 12px; font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">{{alldetails | sumTotal | INR}}</td>
				<td class="col-xs-2" style="text-align: right;font-size: 12px; font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">{{alldetails | sumReceived | INR}}</td>
				<td class="col-xs-2" style="text-align: right;font-size: 12px; font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">.</td>
				<td class="col-xs-2" style="text-align: right;font-size: 12px; font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">{{latestArrears | INR}}</td>
				</tr>	  
		</table>
		 <table class="table table-fixed" id="allCompanies">
			  <thead><tr>
				<th class="col-xs-4" data-toggle="tooltip" data-placement="bottom" title="Month" style="text-align: left;">Month</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Discount" style="text-align: right;">Discount</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Received" style="text-align: right;">Received</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Received" style="text-align: right;">Adjustment</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Dues" style="text-align: right;">Arrears</th>
				</tr></thead>
				<tr ng-repeat="list in alldetails | filter: greaterThanZero('discounts','rentals','received','adjustmentCheque','dues', 0)">
				<td class="col-xs-4" data-toggle="modal" data-target="#myModal" ng-click="openListDisconts(list.saleBean,list.discountAsPerCompanyBeenList,list.date,list.dues,list.discounts,list.rentals)" title="{{list.date}}" ng-class="$even ? 'oddrow' : 'evenrow'" style="text-align: left;">{{list.date}}</td>
				<td class="col-xs-2" data-toggle="modal" data-target="#myModal" ng-click="openListDisconts(list.saleBean,list.discountAsPerCompanyBeenList,list.date,list.dues,list.discounts,list.rentals)" title="{{list.discounts | INR}}" ng-class="$even ? 'oddrow' : 'evenrow'" style="text-align: right;cursor: pointer;">{{list.discounts | INR}}</td>
				<td class="col-xs-2" data-toggle="modal" data-target="#myModal" ng-click="openListDisconts(list.saleBean,list.discountAsPerCompanyBeenList,list.date,list.dues,list.discounts,list.rentals)" title="{{list.received | INR}}" ng-class="$even ? 'oddrow' : 'evenrow'" style="text-align: right;">{{list.received | INR}}</td>
				<td class="col-xs-2" data-toggle="modal" data-target="#myModal" ng-click="openListDisconts(list.saleBean,list.discountAsPerCompanyBeenList,list.date,list.dues,list.discounts,list.rentals)" title="{{list.adjustmentCheque | INR}}" ng-class="$even ? 'oddrow' : 'evenrow'" style="text-align: right;">{{list.adjustmentCheque | INR}}</td>
				<td class="col-xs-2" data-toggle="modal" data-target="#myModal" ng-click="openListDisconts(list.saleBean,list.discountAsPerCompanyBeenList,list.date,list.dues,list.discounts,list.rentals)" title="{{list.dues | INR}}" ng-class="$even ? 'oddrow' : 'evenrow'" style="text-align: right;">{{list.dues | INR}}</td>
				</tr>
				<tr>
				<td class="col-xs-4" style="text-align: left;font-size: 12px; font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">Total</td>
				<td class="col-xs-2" style="text-align: right;font-size: 12px; font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">{{alldetails | sumDiscounts | INR}}</td>
				<td class="col-xs-2" style="text-align: right;font-size: 12px; font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">{{alldetails | sumReceived | INR}}</td>
				<td class="col-xs-2" style="text-align: right;font-size: 12px; font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">.</td>
				<td class="col-xs-2" style="text-align: right;font-size: 12px; font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">{{latestArrears | INR}}</td>
				</tr>	  
		</table>
            </div>
            <div class="tab-pane" id="profile-v">
             
           <a style="float: right;" href="downloadDiscount?company={{discountCompany}}&date={{discountdate}}&companyName={{companyName}}"> <img alt="pdfImg" src="resources/images/pdf.png" style="float: right; margin-right: 30px;cursor: pointer;"></a>
        <div class="row ">
        <div class="col-md-1"></div>
          <div class="col-md-9" id="generateImg">
           <b><span id="companyVal" style="padding: 0px 0px 0px 15px;font-size: 15px;">{{companyName}} {{enterprice}}</span></b><!--<span style=" font-size: 15px; color: red;float: right;"><b>Difference: {{((transactionData | totalDiscountAmt) + (rentalAmt)) - ((receivedData | totalChequeAmount) + (receivedData | adjCheque)) | INR}}</b></span> -->
           <table id="" style=" border: solid 1px #000;">
		    <tbody style=" border: solid 1px #000;font-family: sans-serif;">
		     <tr>
		        <td style="border: solid 1px #000;font-family: sans-serif; text-align: center;"><b>Rentals + Total Discount</b></td>
		        <td style="border: solid 1px #000;"><b>{{((transactionData | totalDiscountAmt)+(rentalAmt))| INR}}</b></td>
		      </tr>
		       <tr>
		       <td style="border: solid 1px #000;font-family: sans-serif;text-align: center;"><b>Total Cheques Received</b></td>
		        <td style="border: solid 1px #000;font-family: sans-serif;"><b>{{receivedData | totalChequeAmount| INR}}</b></td>
		      </tr>
		      <tr>
		        <td style="border: solid 1px #000;font-family: sans-serif;text-align: center;"><b>Cheque Adjustments</b></td>
		        <td style="border: solid 1px #000;font-family: sans-serif;"><b>{{ receivedData | adjCheque | INR}}</b></td>
		      </tr>
		      <tr>
		        <td style="border: solid 1px #000;font-family: sans-serif;text-align: center;font-weight: bold;"><b>Difference</b></td>
		        <td style="border: solid 1px #000;font-family: sans-serif;font-weight: bold;color: red;"><b>{{((receivedData | totalChequeAmount) + (receivedData | adjCheque)) - ((transactionData | totalDiscountAmt) + (rentalAmt)) | INR}}</b></td>
		      </tr>
		    </tbody>
		  </table>
          
          
          <table id="receivedDiscount" style=" border: solid 1px #000;">
		    <thead style=" border: solid 1px #000;font-family: sans-serif;">
		      <tr>
		        <th style=" border: solid 1px #000;"><b>Tr. Date</b></th>
		        <th style=" border: solid 1px #000;"><b>Bank</b></th>
		        <th style=" border: solid 1px #000;"><b>Tr. Type</b></th>
		        <th style=" border: solid 1px #000;"><b>Cheque No/Reference</b></th>
		        <th style=" border: solid 1px #000;"><b>Cheque Amt</b></th>
		      <!--   <th>Cheque Amt</th> -->
		      </tr>
		    </thead>
		    <tbody style=" border: solid 1px #000;font-family: sans-serif;">
		      <tr ng-repeat="cval in receivedData | filter: greaterThan('chequeAmount', 0)">
		       <td style=" border: solid 1px #000;text-align: left;"><b>{{cval.enterprise}}</b></td>
		      <td style=" border: solid 1px #000;text-align: center;"><b>{{cval.companyName}}</b></td>
		      <td style=" border: solid 1px #000;text-align: center;"><b>{{cval.comment}}</b></td>
		      <td style=" border: solid 1px #000;text-align: center;"><b>{{cval.color}}</b></td>
		      <!--   <td><b>{{cval.discountAmount | INR}}</b></td> -->
		        <td style=" border: solid 1px #000;"><b>{{cval.chequeAmount | INR}}</b></td>
		      </tr>
		       <tr>
		       <td style="background: antiquewhite;border: solid 1px #000;font-family: sans-serif;" colspan="4"><b>Total Cheques Received</b></td>
		        <td style="background: antiquewhite;border: solid 1px #000;font-family: sans-serif;"><b>{{receivedData | totalChequeAmount| INR}}</b></td>
		      </tr>
		    </tbody>
		  </table> 
          
              
		    <table id="companyDiscount" style="border: solid 1px #000;">
		    <thead>
		      <tr>
		        <th style="border: solid 1px #000;font-family: sans-serif;"><b>Month</b></th>
		         <th style="border: solid 1px #000;font-family: sans-serif;"><b>Brand</b></th>
		        <th style="border: solid 1px #000;font-family: sans-serif;"><b>Case</b></th>
		        <th style="border: solid 1px #000;font-family: sans-serif;"><b>Disc/Case</b></th>
		        <th style="border: solid 1px #000;font-family: sans-serif;"><b>Total</b></th>
		      </tr>
		    </thead>
		    <tbody ng-repeat="list in transactionData">
		      <tr>
		        <td style="border: solid 1px #000;font-family: sans-serif;text-align: left;"><b>{{list.month}}</b></td>
		        <td style="border: solid 1px #000;font-family: sans-serif;text-align: left;"><b>{{list.discountMonthWiseBean[0].brand}}</b></td>
		        <td style="border: solid 1px #000;font-family: sans-serif;"><b>{{list.discountMonthWiseBean[0].case | INR}}</b></td>
		        <td style="border: solid 1px #000;font-family: sans-serif;"><b>{{list.discountMonthWiseBean[0].discount | INR}}</b></td>
		        <td style="border: solid 1px #000;font-family: sans-serif;"><b>{{list.discountMonthWiseBean[0].total | INR}}</b></td>
		      </tr>
		      <tr ng-repeat="brand in list.discountMonthWiseBean" ng-if="$index > 0">
		       <td style="border: solid 1px #000;font-family: sans-serif;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="border: solid 1px #000;font-family: sans-serif;text-align: left;"><b>{{brand.brand}}</b></td>
		        <td style="border: solid 1px #000;font-family: sans-serif;"><b>{{brand.case | INR}}</b></td>
		        <td style="border: solid 1px #000;font-family: sans-serif;"><b>{{brand.discount | INR}}</b></td>
		        <td style="border: solid 1px #000;font-family: sans-serif;"><b>{{brand.total | INR}}</b></td>
		      </tr>
		       <tr>
		       <td style="border: solid 1px #000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="border: solid 1px #000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="border: solid 1px #000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="border: solid 1px #000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="background: antiquewhite;font-family: sans-serif;border: solid 1px #000;"><b>{{list.totalDiscount | INR}}</b></td>
		      </tr>
		    </tbody>
		    <tbody>
		     <tr>
		       <td style="background: antiquewhite;border: solid 1px #000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="background: antiquewhite;border: solid 1px #000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="background: antiquewhite;border: solid 1px #000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="background: antiquewhite;border: solid 1px #000;font-family: sans-serif;"><b>Grand Total Discount</b></td>
		        <td style="background: antiquewhite;border: solid 1px #000;font-family: sans-serif;"><b>{{ transactionData | totalDiscountAmt | INR}}</b></td>
		      </tr>
		      <tr>
		       <td style="background: antiquewhite;border: solid 1px #000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="background: antiquewhite;border: solid 1px #000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="background: antiquewhite;border: solid 1px #000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="background: antiquewhite;border: solid 1px #000;font-family: sans-serif;"><b>Rentals</b></td>
		        <td style="background: antiquewhite;border: solid 1px #000;font-family: sans-serif;"><b>{{ rentalAmt | INR}}</b></td>
		      </tr>
		    </tbody>
		  </table>   
		  </div>
		  <div class="col-md-2"></div>
    </div>
            
            </div>
            <!-- 3rd Pan start -->
          <div class="tab-pane active" id="allCompanyDiscount-v">
                    <div class="stockLiftList">
						<span class="col-xs-6" style="padding: 8px;text-align: left;background-color: #243a51;color: #fff;">Company</span>
						<span class="col-xs-6" style="padding: 8px;text-align: right;background-color: #243a51;color: #fff;" ng-click="sort('duesAmt')">Total Discount
						<p class="glyphicon sort-icon" style="color: #fff;margin-bottom: 0px;" ng-show="sortKey=='duesAmt'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></p>
						</span>
					</div>
             <details ng-repeat="list in discountData | orderBy:sortKey:reverse">
				<summary>
					<div class="stockLiftList">
						<span class="col-xs-6" style="padding: 8px;text-align: left;border-bottom: solid 1px #ccc;" title="{{list.company}}">{{list.company}}</span>
						<span class="col-xs-6" style="padding: 8px;text-align: right;border-bottom: solid 1px #ccc;" title="{{list.duesAmt}}">{{list.duesAmt | INR}}</span>
					</div>
				</summary>
				 <table class=" table table-fixed">
				 <thead><tr>
					<th class="col-xs-3 fontfamily" style="text-align: left;" data-toggle="tooltip" data-placement="bottom" title="Company" style="text-align: left;">Month</th>
					<th class="col-xs-3 fontfamily" style="text-align: right;" data-toggle="tooltip" data-placement="bottom" title="Cheque">Discount</th>
					<th class="col-xs-3 fontfamily" style="text-align: right;" data-toggle="tooltip" data-placement="bottom" title="Comment">Cheque</th>
					<th class="col-xs-3 fontfamily" style="text-align: right;" data-toggle="tooltip" data-placement="bottom" title="Comment">Arrears</th>
					</tr></thead>
				     <tr ng-repeat="brand in list.mobileViewStockDiscountInner | filter: greaterThanZeroAllCompany('discount','recieved','arrears', 0)" data-toggle="modal" data-target="#myModalbacardiCompany" ng-click="openListDiscontsAllCompany(brand.discountAsPerCompanyBeenList,brand.date,brand.companyId,brand.realDate,brand.discount,brand.rentals,brand.arrears)">
						<td class="col-xs-3 fontfamily" style="border-top: 0px solid #ddd;text-align: left;" title="{{brand.date}}">{{brand.date}}</td>
					    <td class="col-xs-3 fontfamily" style="border-top: 0px solid #ddd;text-align: right;"  title="{{brand.discount}}">{{brand.discount | INR}}</td>
					    <td class="col-xs-3 fontfamily" style="border-top: 0px solid #ddd;text-align: right;" title="{{brand.recieved}}">{{brand.recieved | INR}}</td>
					    <td class="col-xs-3 fontfamily" style="border-top: 0px solid #ddd;text-align: right;" title="{{brand.arrears}}">{{brand.arrears | INR}}</td>
					</tr></table>
	       </details>
          </div>
            <!-- 3rd Pan close  -->
             <!-- 4th Pan start -->
          <div class="tab-pane" id="discountForSingleMonth-v">
                   <table class=" table table-fixed" id="monthWiseDiscount">
					  <thead><tr>
						<th class="col-xs-2" style="text-align: left;">Company
						</th>
						<th class="col-xs-2" ng-click="sorting('discountAmount')">Discount
						<p class="glyphicon sort-icon" style="color: #fff;margin-bottom: 0px;" ng-show="sortingKey=='discountAmount'" ng-class="{'glyphicon-chevron-up':reverseKey,'glyphicon-chevron-down':!reverseKey}"></p>
						</th>
						<th class="col-xs-1" ng-click="sorting('rentals')">Rental
						<p class="glyphicon sort-icon" style="color: #fff;margin-bottom: 0px;" ng-show="sortingKey=='rentals'" ng-class="{'glyphicon-chevron-up':reverseKey,'glyphicon-chevron-down':!reverseKey}"></p>
						</th>
						<th class="col-xs-2">Total</th>
						<th class="col-xs-2" ng-click="sorting('chequeAmount')">Received
						<p class="glyphicon sort-icon" style="color: #fff;margin-bottom: 0px;" ng-show="sortingKey=='chequeAmount'" ng-class="{'glyphicon-chevron-up':reverseKey,'glyphicon-chevron-down':!reverseKey}"></p>
						</th>
						<th class="col-xs-1" ng-click="sorting('adjustmentCheque')">ADJ
						<p class="glyphicon sort-icon" style="color: #fff;margin-bottom: 0px;" ng-show="sortingKey=='adjustmentCheque'" ng-class="{'glyphicon-chevron-up':reverseKey,'glyphicon-chevron-down':!reverseKey}"></p>
						</th>
						<th class="col-xs-2" ng-click="sorting('arrears')">Arrears
						<p class="glyphicon sort-icon" style="color: #fff;margin-bottom: 0px;" ng-show="sortingKey=='arrears'" ng-class="{'glyphicon-chevron-up':reverseKey,'glyphicon-chevron-down':!reverseKey}"></p>
						</th>
						</tr>
						</thead>
						<tr ng-repeat="selecetMonthDiscList in selecetMonthDisc | filter: greaterThanZero('discountAmount','rentals','chequeAmount','arrears','adjustmentCheque', 0) | orderBy:sortingKey:reverseKey">
						<td class="col-xs-2" style="text-align: left;border-bottom: 1px solid #e8e8e8;">{{selecetMonthDiscList.companyName}}</td>
						<td class="col-xs-2" style="border-bottom: 1px solid #e8e8e8;">{{selecetMonthDiscList.discountAmount | INR}}</td>
						<td class="col-xs-1" style="border-bottom: 1px solid #e8e8e8;">{{selecetMonthDiscList.rentals | INR}}</td>
						<td class="col-xs-2" style="border-bottom: 1px solid #e8e8e8;">{{(selecetMonthDiscList.discountAmount + selecetMonthDiscList.rentals) | INR}}</td>
						<td class="col-xs-2" style="border-bottom: 1px solid #e8e8e8;">{{selecetMonthDiscList.chequeAmount | INR}}</td>
						<td class="col-xs-1" style="border-bottom: 1px solid #e8e8e8;">{{selecetMonthDiscList.adjustmentCheque | INR}}</td>
						<td class="col-xs-2" style="border-bottom: 1px solid #e8e8e8;">{{selecetMonthDiscList.arrears | INR}}</td>
						</tr>
						<tr>
						<td class="col-xs-2" style="text-align: left;border-bottom: 1px solid #e8e8e8;font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">Total</td>
						<td class="col-xs-2" style="border-bottom: 1px solid #e8e8e8;font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">{{selecetMonthDisc | selecetMonthDiscListdiscountAmount | INR}}</td>
						<td class="col-xs-1" style="border-bottom: 1px solid #e8e8e8;font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">{{selecetMonthDisc | selecetMonthDiscListrentals | INR}}</td>
						<td class="col-xs-2" style="border-bottom: 1px solid #e8e8e8;font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">{{selecetMonthDisc | selecetMonthDiscListtotal | INR}}</td>
						<td class="col-xs-2" style="border-bottom: 1px solid #e8e8e8;font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">{{selecetMonthDisc | selecetMonthDiscListchequeAmount | INR}}</td>
						<td class="col-xs-1" style="border-bottom: 1px solid #e8e8e8;font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">{{selecetMonthDisc | selecetMonthDiscListadjustmentCheque | INR}}</td>
						<td class="col-xs-2" style="border-bottom: 1px solid #e8e8e8;font-weight: 600;background-color: #c8d8e8;border-top: 1px solid #000;">{{selecetMonthDisc | selecetMonthDiscListarrears | INR}}</td>
						</tr>	
		    </table>
          </div>
            <!-- 4th Pan close  -->
            </div>
          </div>
        </div> 
         <div class="clearfix"></div>
      </div>
      <!-- model pupop open -->
		<div class="modal fade sbi2-popup-styles" id="myModal" role="dialog">
	    <div class="modal-dialog modal-lg">
	      <div class="modal-content">
	        <div class="modal-header">
	           <h4 class="modal-title"><span>Date: {{date}} | Discount: {{ttlDiscount | INR}} | Arrears: {{aarears | INR}}</span></h4> 
	           <button type="button" class="close" data-dismiss="modal">&times;</button>
	        </div>
	        <div class="modal-body">
				<div class="stockLiftSpan" style="height:30px;padding: 5px;border-bottom: 1px solid #e0e0e0;">
						    <span class="col-xs-4" style="text-align: left;">Brand Name</span>
						    <span class="col-xs-4" style="text-align: center;">Lifted Case</span>
						    <span class="col-xs-4" style="text-align: center;">Discount</span>
						</div>
				<details ng-repeat="list in tempData | filter: greaterThan('totalCaseQty', 0)" ng-class="$even ? 'oddrow' : 'evenrow'">
					<summary>
						<div class="stockLiftList shiva" style="height:30px;padding: 5px;border-bottom: 1px solid #e0e0e0;">
						    <span class="col-xs-4" style="color: #000;text-align: left;">{{list.productName}}</span>
						    <span class="col-xs-4" style="color: #000;text-align: center;">{{list.totalCaseQty}}</span>
						    <span class="col-xs-4" style="color: #000;text-align: center;">{{list.totalDiscount}}</span>
						</div>
					</summary>
					<table class=" table table-fixed">
						   <thead><tr>
						    <th class="col-xs-3" style="background: #1a6398 !important;">Brand Name</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;">Brand No</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;">Lifted Case</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;">Discount</th>
						    <th class="col-xs-3" style="background: #1a6398 !important;">Total Discount</th>
						    </tr></thead>
						    <tr ng-repeat="brand in list.Brands | filter: greaterThan('caseQty', 0)"">
							<td class="col-xs-3" style="color: #000;">{{brand.brandName}}</td>
							<td class="col-xs-2" style="color: #000;">{{brand.brandNo}}</td>
							<td class="col-xs-2" style="color: #000;">{{brand.caseQty}}</td>
							<td class="col-xs-2" style="color: #000;">{{brand.unitPrice}}</td>
							<td class="col-xs-3" style="color: #000;">{{brand.totalPrice}}</td>
							</tr>
						  </table>
					</details>
		       <br>
		    <table class=" table table-fixed" >
			  <thead><tr>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Company" style="text-align: left;">Bank</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Total Discount">Tr. Type</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Cheque">Amount</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Cheque">Tr. Date</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Comment">ADJ Amt</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Arrears">Image</th>
				</tr></thead>
				<tr ng-repeat="discountlist in discountBeen">
				<td class="col-xs-2 tdClasscss" style="text-align: left;border-bottom: 1px solid #e8e8e8;" title="{{discountlist.bank}}">{{discountlist.bank}}</td>
				<td class="col-xs-2 tdClasscss" style="border-bottom: 1px solid #e8e8e8;" title="{{discountlist.transactionType}}">{{discountlist.transactionType}}</td>
				<td class="col-xs-2 tdClasscss" style="border-bottom: 1px solid #e8e8e8;" title="{{discountlist.transactionAmt | INR}}">{{discountlist.transactionAmt | INR}}</td>
				<td class="col-xs-2 tdClasscss" style="border-bottom: 1px solid #e8e8e8;" title="{{discountlist.transactionDate}}">{{discountlist.transactionDate}}</td>
				<td class="col-xs-2 tdClasscss" style="border-bottom: 1px solid #e8e8e8;" title="{{discountlist.adjCheck}}">{{discountlist.adjCheck}}</td>
				<td class="col-xs-2 glyphicon glyphicon-picture" data-toggle="modal" data-target="#myModalForImage" ng-click="openChekImg(discountlist.ngalleryImage,discountlist.imageName)" style="color:#000;border-bottom: 1px solid #e8e8e8;">
				</tr>	  
		    </table>
	        
	        </div>
	      </div>
	    </div>
  </div>
		
		<!-- model pupop close -->
		<!-- Bacardi model popup open -->
		<div class="modal fade sbi2-popup-styles" id="myModalbacardiCompany" role="dialog">
	    <div class="modal-dialog modal-lg">
	      <div class="modal-content">
	        <div class="modal-header">
	           <h4 class="modal-title"><span>Date: {{date}} | Discount: {{ttlDiscount | INR}} |  Rental: {{rentalsAmt | INR}} | Arrears: {{aarears | INR}}</span></h4> 
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        </div>
	        <div class="modal-body">
	        
		    <div class="stockLiftSpan" style="height:30px;padding: 5px;border-bottom: 1px solid #e0e0e0;">
						    <span class="col-xs-4" style="text-align: left;">Brand Name</span>
						    <span class="col-xs-4" style="text-align: center;">Lifted Case</span>
						    <span class="col-xs-4" style="text-align: center;">Discount</span>
						</div>
				<details ng-repeat="list in tempData | filter: greaterThan('totalCaseQty', 0)" ng-class="$even ? 'oddrow' : 'evenrow'">
					<summary>
						<div class="stockLiftList shiva" style="height:30px;padding: 5px;border-bottom: 1px solid #e0e0e0;">
						    <span class="col-xs-4" style="color: #000;text-align: left;">{{list.productName}}</span>
						    <span class="col-xs-4" style="color: #000;text-align: center;">{{list.totalCaseQty}}</span>
						    <span class="col-xs-4" style="color: #000;text-align: center;">{{list.totalDiscount}}</span>
						</div>
					</summary>
					<table class=" table table-fixed">
						   <thead><tr>
						    <th class="col-xs-3" style="background: #1a6398 !important;">Brand Name</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;">Brand No</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;">Lifted Case</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;">Discount</th>
						    <th class="col-xs-3" style="background: #1a6398 !important;">Total Discount</th>
						    </tr></thead>
						    <tr ng-repeat="brand in list.Brands | filter: greaterThan('caseQty', 0)"">
							<td class="col-xs-3" style="color: #000;">{{brand.brandName}}</td>
							<td class="col-xs-2" style="color: #000;">{{brand.brandNo}}</td>
							<td class="col-xs-2" style="color: #000;">{{brand.caseQty}}</td>
							<td class="col-xs-2" style="color: #000;">{{brand.unitPrice}}</td>
							<td class="col-xs-3" style="color: #000;">{{brand.totalPrice}}</td>
							</tr>
						  </table>
					</details>
		    <br>
		    <table class=" table table-fixed" >
			  <thead><tr>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Company" style="text-align: left;">Bank</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Total Discount">Tr. Type</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Cheque">Amount</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Cheque">Tr. Date</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Comment">ADJ Amt</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Arrears">Image</th>
				</tr></thead>
				<tr ng-repeat="discountlist in discountBeen">
				<td class="col-xs-2 tdClasscss" style="text-align: left;border-bottom: 1px solid #e8e8e8;" title="{{discountlist.bank}}">{{discountlist.bank}}</td>
				<td class="col-xs-2 tdClasscss" style="border-bottom: 1px solid #e8e8e8;" title="{{discountlist.transactionType}}">{{discountlist.transactionType}}</td>
				<td class="col-xs-2 tdClasscss" style="border-bottom: 1px solid #e8e8e8;" title="{{discountlist.transactionAmt | INR}}">{{discountlist.transactionAmt | INR}}</td>
				<td class="col-xs-2 tdClasscss" style="border-bottom: 1px solid #e8e8e8;" title="{{discountlist.transactionDate}}">{{discountlist.transactionDate}}</td>
				<td class="col-xs-2 tdClasscss" style="border-bottom: 1px solid #e8e8e8;" title="{{discountlist.adjCheck}}">{{discountlist.adjCheck}}</td>
				<td class="col-xs-2 glyphicon glyphicon-picture" data-toggle="modal" data-target="#myModalForImage" ng-click="openChekImg(discountlist.ngalleryImage,discountlist.imageName)" style="color:#000;border-bottom: 1px solid #e8e8e8;">
				</tr>	
		    </table>
	        
	        </div>
	      </div>
	    </div>
  </div>
		
		<!-- model pupop close -->
		
		<!-- Bacardi model popup close -->
		
		<!-- image popup open -->
		<div class="modal fade sbi2-popup-styles" id="myModalForImage" role="dialog">
	    <div class="modal-dialog modal-lg">
	      <div class="modal-content">
	        <div class="modal-header">
	          <a style="font-size: 16px;" href="downloadChecks?imagePath={{imageName}}"><span class="glyphicon glyphicon-download-alt"></span></a>
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
     	    </div>
	        <div class="modal-body">
	        <img src= "{{image}}" style="width:100%;height:100%;">
	        </div>
	      </div>
	    </div>
  </div>
		<!-- image popup close -->
</body>
</html>