<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
  <jsp:include page="/WEB-INF/jsp/header.jsp" />  
  <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
  <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
  
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script>
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
  <spring:url value="/resources/javascript/balanceSheet.js" var="balanceSheetjs"/>
  <script src="${balanceSheetjs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}"> 
   <spring:url value="/resources/css/saleReport.css" var="saleReportcss"/>
   <link rel="stylesheet" href="${saleReportcss}">
     <spring:url value="/resources/css/balanceSheet.css" var="balanceSheetcss"/>
  <link rel="stylesheet" href="${balanceSheetcss}">
 <style>
.evenrow{
	background-color:#81DAF5!important;float:left;width:150px;font-family:Verdana,sans-serif!important;font-size:12px;color:#424242;
}

.oddrow{
	background-color:#E6E6E6!important;float:left;width:150px;font-family:Verdana,sans-serif!important;font-size:12px;color:#424242;
}
 .fontcolorred{
color: red;
}
.fontcolorgreen{
color:green;
}
.fontcolorblack{
color:black;
}
  .sbi2-popup-styles .modal-body {
  padding-top: 10px !important;
}
.sbi2-popup-styles {
  color: #212121;
  font-size: 16px;
}
.sbi2-popup-styles .modal-dialog {
  margin-top: 0 !important;
  top: 50% !important;
  transform: translateY(-50%) !important;
}
.sbi2-popup-styles .modal-content {
  padding: 20px !important;
  border-radius: 0;
  position: relative;
}
.sbi2-popup-styles .modal-title {
  font-size: 16px;
  font-family: 'Montserrat', sans-serif !important
  color: #212121;
  position: relative;
  width: 95%;
}
.sbi2-popup-styles .modal-header {
  padding: 0px 20px 15px 20px;
}
.sbi2-popup-styles .modal-header .close {
  margin-top: 4px;
  font-size: 16px;
  padding: 0;
  cursor: pointer;
  background: transparent;
  border: 0;
  -webkit-appearance: none;
  float: right;
  line-height: 1;
  color: #9E9E9E;
  opacity: 1 !important;
}
.sbi2-popup-styles .modal-header .close:focus {
  outline: none;
}
.sbi2-popup-styles .modal-header .close:hover {
  color: #11304D;
}
.sbi2-popup-styles .modal-body {
  padding: 0;
  padding-top: 20px;
}
.sbi2-popup-styles .form-group {
  margin-bottom: 20px;
}
.sbi2-popup-styles .sbi2-popup-header .sbi2-popup-header-icon {
  padding-right: 5px;
}
.sbi2-popup-styles .sbi2-popup-header .sbi2-change-to-popup-header-icon {
  padding-right: 8px;
  margin-top: -3px;
}
#customers {
    font-family: 'Montserrat', sans-serif !important;
    border-collapse: collapse;
    width: 100%;
    font-size: 14px;
}

#customers td, #customers th {
    padding: 5px;
        font-size: 12px;
}

#customers th {
    padding-top: 6px;
    padding-bottom: 6px;
    text-align: left;
    background-color: #062351;
    color: white;
        font-size: 12px;
}
.container{
max-width: 100%;
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 15px;
}
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<div class="row" style="padding-top: 100px;">
  <div class="col-sm-3 bodyFontCss" style="padding: 0px; !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Reports</li>
  <li>Balance Sheet</li>
</ul>
  </div>
   <div class="col-sm-1 bodyFontCss"></div>
   <div class="col-sm-2 bodyFontCss">
   <label>Start Date</label><input type="text" class="form-control" id="startDate" readonly='true'>
   </div>
    <div class="col-sm-2 bodyFontCss">
   <label>End Date</label><input type="text" class="form-control" id="endDate" readonly='true'>
   </div>
   <div class="col-sm-1 bodyFontCss customizedButton">
   <input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getResults('startDate','endDate')" value="GET DATA" />
   </div>
  <div class="col-sm-1 bodyFontCss"></div>
  <div class="col-sm-2 bodyFontCss"></div>
  </div>
  <div class="container" style="margin-top: 20px;">
  <div class="row" style="margin-bottom: 20px;">
    <div class="col-sm-12 bodyFontCss">
    <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
    
    <table class="table table-fixed">
		          <thead>
		            <tr>
		              <th class="col-xs-1">Date</th><th class="col-xs-1" title="SalePerSheet(A)">SalePerSheet(A)</th><th class="col-xs-1">Expenses (B)</th>
		               <th class="col-xs-1">Card (C)</th><th class="col-xs-1">Cash (D)</th><th class="col-xs-1">Cheque (E)</th><th class="col-xs-2" title="Total(F=B+C+D+E)">Total(F=B+C+D+E)</th>
		                <th class="col-xs-1">Difference (A-F)</th><th class="col-xs-1">Bank Credit (G)</th><th class="col-xs-1">Retention (H)</th> <th class="col-xs-1">Investment</th>
		            </tr>
		          </thead>
					<tbody>
						<tr ng-repeat="list in balanceSheetList">
							<td class="col-xs-1" style="color:#fff;background-color: #75b9ce!important;">{{list.date}}</td>
							<td class="col-xs-1" style="background-color: #996633!important;color:#fff;">{{list.saleAmount | INR}}</td>
							<td class="col-xs-1" style="background-color: #ff4d4d!important;color:#fff;cursor: pointer;" data-toggle="modal" data-target="#myModal"
							 ng-click="getExpenseData(list.expenseMasterId,list.date)">{{list.expenseAmount | INR}}</td>
							<td class="col-xs-1" style="background-color: #ff5050!important;color:#fff;">{{list.cardAmount | INR}}</td>
							<td class="col-xs-1" style="background-color:#ff5050!important;color:#fff;">{{list.cashAmount | INR}}</td>
							<td class="col-xs-1" style="background-color:#ff5050!important;color:#fff;">{{list.chequeAmount | INR}}</td>
							<td class="col-xs-2" style="background-color:#8c8c8c!important;color:#fff;">{{list.totalAmount | INR}}</td>
							<td ng-if="list.diffAmount <= 0" class="col-xs-1 fontcolorgreen" style="background-color:  #ddf7ff!important;"  title="Excess">{{list.showShortOrExcess | INR}}</td>
							<td ng-if="list.diffAmount > 0" class="col-xs-1 fontcolorred" style="background-color:  #ddf7ff!important;"  title="Short">{{list.showShortOrExcess | INR}}</td>
							<td class="col-xs-1" style="background-color:  #304225!important;color:#fff;">{{list.bankCreditedAmount | INR}}</td>
							<td ng-if="list.retention <=0" class="col-xs-1 fontcolorgreen" style="background-color:  #85929E!important;">{{list.retention | abs | INR}}</td>
							<td ng-if="list.retention > 0" class="col-xs-1 fontcolorred" style="background-color:  #85929E!important;">{{list.retention | abs | INR}}</td>
							<td class="col-xs-1" style="background-color:  #BB8FCE!important;color:#fff;cursor: pointer;" data-toggle="modal" data-target="#investmentModal"
							ng-click="getInvestmentData(list.date)">{{list.investment | INR}}</td>
						</tr>
					</tbody>
				</table>
    </div>
  </div>
 <div class="fixed">
			<div class="row">
				<div class="col-sm-4">
					<table>
					    <tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Card Amount :</th>
							<td style="float: right;">{{balanceSheetList | totalCardAmount | INR}}</td>
						</tr>
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Cash Amount :</th>
							<td style="float: right;">{{balanceSheetList | totalCashAmount | INR}}</td>
						</tr>
					</table>
				</div>
				<div class="col-sm-4">
					<table style="float: center;">
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Sale Amount :</th>
							<td style="float: right;">{{balanceSheetList | totalSaleAmount | INR}}</td>
						</tr>
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Expenses :</th>
							<td style="float: right;">{{balanceSheetList | totalExpenseAmount | INR}}</td>
						</tr>
					</table>
				</div>
				<div class="col-sm-4" style="">
				<table style="float: right;">
						<tr ng-if="totalDiffAmount <= 0">
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Excess(After removing breakage) :</th>
							<td style="float: right;color: #88e288;"  title="Excess">{{totalDiffAmount | abs | INR}}</td>
						</tr>
						<tr ng-if="totalDiffAmount > 0">
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Shorts(After removing breakage) :</th>
							<td style="float: right;color: #f15b5b;"  title="Shorts">{{totalDiffAmount | INR}}</td>
						</tr>
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Cash Retention :</th>
							<td ng-if="totalRetentionAmount > 0" style="float: right;color: #f15b5b;">{{totalRetentionAmount | INR}}</td>
							<td ng-if="totalRetentionAmount <= 0" style="float: right;color: #88e288;">{{totalRetentionAmount | abs | INR}}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
  </div>
  <div class="modal fade sbi2-popup-styles" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">Seleceted Date: {{expenseDate}}</h4> 
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
					<table id="customers">
						<tbody>
							<tr>
								<th>Expense Name</th>
								<th>Amount</th>
								<th>Comment</th>
							</tr>
							<tr ng-repeat="list in excepseDetails">
								<td>{{list.name}}</td>
								<td>{{list.expenseChildAmount | INR}}</td>
								<td>{{list.comment}}</td>
							</tr>
							<tr style="border-top: solid 1px #ccc;">
								<td></td>
								<td></td>
								<td style="float: right; color: red;">Total : {{totalExpenseAmount | INR}}</td>
							</tr>
						</tbody>
					</table>
				</div>
      </div>
    </div>
  </div>
  <!-- model for investment -->
  <div class="modal fade sbi2-popup-styles" id="investmentModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title" id="investdate"></h4> 
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div id="investmentList"></div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>