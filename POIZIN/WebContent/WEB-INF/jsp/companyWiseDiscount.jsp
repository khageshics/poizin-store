<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-1.12.4.js" var="jquery1124js"/>
  <script src="${jquery1124js}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/angular.min.js" var="angularminjs"/>
  <script src="${angularminjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/angular-filter.js" var="angularfilterjs"/>
  <script src="${angularfilterjs}"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
  <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script>
   <script src="https://rawgit.com/zorab47/jquery.ui.monthpicker/master/jquery.ui.monthpicker.js"></script>
   <spring:url value="/resources/css/bootstrap.min.css" var="bootstrapmincss"/>
   <link rel="stylesheet" href="${bootstrapmincss}">
   <spring:url value="/resources/javascript/companyWiseDiscount.js" var="companyWiseDiscountjs"/>
  <script src="${companyWiseDiscountjs}"></script> 
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
     <spring:url value="/resources/css/bootstrap-select.min.css" var="bootstrapselectmincss"/>
   <link rel="stylesheet" href="${bootstrapselectmincss}">
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap-select.min.js" var="bootstrapselectminjs"/>
  <script src="${bootstrapselectminjs}"></script>
  <spring:url value="/resources/css/common.css" var="commoncss"/>
   <link rel="stylesheet" href="${commoncss}">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    
     <script src='https://cdn.rawgit.com/simonbengtsson/jsPDF/requirejs-fix-dist/dist/jspdf.debug.js'></script>
<script src='https://unpkg.com/jspdf-autotable@2.3.2'></script>
   
     <spring:url value="/resources/css/companyAndDiscount.css" var="companyAndDiscountcss"/>
   <link rel="stylesheet" href="${companyAndDiscountcss}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <script>
  $('.selectpicker').selectpicker();
  </script>
  <style>

.dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover {
    color: #262626;
    text-decoration: none;
    background-color: #e6e8e8;
    }
     .sbi2-popup-styles .modal-body {
  padding-top: 10px !important;
}
table,tbody,thead,td,th {
    background-color: #fff;
    border: solid 1px #000;
    font-size: 15px;
    text-align: right;
}

 
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">

 <div id="main">

		<nav class="navbar navbar-inverse" class="headernav" style="height:120px;background:#062351;border-radius:0px;">
		<label style="float: right;color: #fff;padding: 10px 10px 0px 0px;">COMPANY WISE DISCOUNT</label>
		<div class="container-fluid" style="padding-right: 200px;">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">
				<img src="resources/images/logopoizin.png" /></a>
			</div>
			<ul class="nav navbar-nav navbar-right">

				<li class="dropdown"><a class="dropbtn"
					style="padding-top: 70px;">Invoice</a>
					<div class="dropdown-content">
						<a id="feature_1" href="addNewBrand">Add New Brand</a> <a
							id="feature_2" href="saveInvoice">Add Invoice</a>

					</div></li>

				<li class="dropdown"><a class="dropbtn"
					style="padding-top: 70px;">Sale</a>
					<div class="dropdown-content">
						<a id="feature_3" href="sale">Enter latest Sale</a> <a
							id="feature_9" href="updateSale">Edit latest Sale</a>
							<a id="feature_20" href="monthlyExpenses">Monthly Debit/Credit</a>
							<a id="feature_20" href="investment">Enter Investment</a>
							 <a id="feature_24" href="discountTransaction">Enter Cheque With Details</a>
							<a id="feature_20" href="mobileViewEnterSaleSheet">Generate Sale Sheet</a>

					</div></li>
				<li class="dropdown"><a class="dropbtn"
					style="padding-top: 70px;">Reports</a>
					<div class="dropdown-content">
						<!-- <a id="feature_4" href="invoiceDateWiseReport">Invoice Report</a> -->
						<a id="feature_8" href="saleReport">Sale Report</a>
						 <a id="feature_10" href="saleDayWiseReport">Day Wise Sale Report</a>
						<a id="feature_6" href="stockLifting">Stock Lift</a>
                        <a id="feature_12" href="inHouseStockValue">In House Stock</a>
                       <!--  <a id="feature_13" href="stockPredictions">Stock Prediction</a> -->
                        <a id="feature_14" href="balanceSheet">Balance Sheet</a>
                        <!--  <a id="feature_15" href="productComparision">Product Comparision</a> -->
                        <!--  <a id="feature_23" href="tillDateBalanceSheet">Till Date Balance Sheet</a> -->
                       <!--    <a id="feature_22" href="productPerformance">Product Performance</a> -->
                          <a id="feature_22" href="productComparisionCategoryWise">Product Comparison</a>
                           <a id="feature_22" href="saleWithDiscount">Sale With Discount</a>
					</div></li>
					<li class="dropdown"><a class="dropbtn"
					style="padding-top: 70px;">Discount</a>
					<div class="dropdown-content">
						<a id="feature_17" href="discountEstimate">Discount Estimate</a>
						<!-- <a id="feature_24" href="productWithInvestment">Indent Estimate</a> -->
						 <a id="feature_24" href="newIndentEstimate">Indent Estimate</a>
                          <!--  <a id="feature_18" href="retrieveIntent">Accepted Discounts</a> -->
                            <!-- <a id="feature_19" href="stockLiftingWithDiscount">Stock Lift With Discount</a> -->
                            <a id="feature_24" href="tillMonthStockLiftWithDiscount">Till Month Stock Lift With Discount</a>
                            <a id="feature_24" href="stockLiftWithDiscountTransaction"> Stock Lifting With Disc</a>
                             <a id="feature_24" href="companyWiseDiscount">Company Wise Discount</a>
                             <a id="feature_24" href="profitAndLossReport">Profit/Loss Report</a>

					</div></li>
					<li class="dropdown">
                         <c:if test="${sessionScope['scopedTarget.userSession'].loginId > 0}">
						<a class="dropbtn" style="padding-top: 70px;" href="./logout">Logout</a>
					</c:if>
					</li>
			</ul>
		</div>
		</nav>
		
		
		<div id="content_header">
                <div class="" style="width: 100%;text-align: center;margin-bottom: 10px; margin-top: 10px;font-size: 15px;font-weight: 600;">
	 	            Select Month: <input type="text" class="monthpicker dateinput" ng-model="monthlyDate" id="monthlyDate" readonly='true'>
	 	             <select class="selectpicker" id="listdataSec" ng-model="companyId" title="Select Company" ng-change="getCompanyName()">
			         </select>
			      <input class="fetchExistingExpenses submit btn-primary" type="submit" name="" value="Get Data" />
	          </div>
        </div>
     <div id="content">
    
        <img alt="pdfImg" src="resources/images/pdf.png" onclick="generateImage()" style="float: right; margin-right: 30px;cursor: pointer;">
        <div class="row ">
        <div class="col-md-3"></div>
          <div class="col-md-6" id="generateImg">
           <b><span id="companyVal" style="padding: 0px 0px 0px 15px;">{{companyName}}</span></b><!--<span style=" font-size: 15px; color: red;float: right;"><b>Difference: {{((transactionData | totalDiscountAmt) + (rentalAmt)) - ((receivedData | totalChequeAmount) + (receivedData | adjCheque)) | INR}}</b></span> -->
          <table id="receivedDiscount">
		    <thead>
		      <tr>
		        <th>Month</th>
		        <th>Cheque Amt</th>
		      <!--   <th>Cheque Amt</th> -->
		      </tr>
		    </thead>
		    <tbody>
		      <tr ng-repeat="cval in receivedData | filter: greaterThan('chequeAmount', 0)">
		        <td><b>{{cval.enterprise}}</b></td>
		      <!--   <td><b>{{cval.discountAmount | INR}}</b></td> -->
		        <td><b>{{cval.chequeAmount | INR}}</b></td>
		      </tr>
		       <tr>
		       <td style="background: antiquewhite;"><b>Total Cheques Received</b></td>
		      <!--   <td style="background: antiquewhite;"><b>{{receivedData | totalAmount | INR}}</b></td> -->
		        <td style="background: antiquewhite;"><b>{{receivedData | totalChequeAmount| INR}}</b></td>
		      </tr>
		      <tr>
		        <td style="background: antiquewhite;"><b>Rentals + Total Discount</b></td>
		        <td style="background: antiquewhite;"><b>{{((transactionData | totalDiscountAmt)+(rentalAmt))| INR}}</b></td>
		      </tr>
		      <tr>
		        <td style="background: antiquewhite;"><b>Cheque Adjustments</b></td>
		        <td style="background: antiquewhite;"><b>{{ receivedData | adjCheque | INR}}</b></td>
		      </tr>
		      <tr>
		        <td style="background: antiquewhite;color: red;"><b>Difference</b></td>
		        <td style="background: antiquewhite;color: red;"><b>{{((receivedData | totalChequeAmount) + (receivedData | adjCheque)) - ((transactionData | totalDiscountAmt) + (rentalAmt)) | INR}}</b></td>
		      </tr>
		    </tbody>
		  </table> 
          
              
		    <table id="companyDiscount">
		    <thead>
		      <tr>
		        <th>Month</th>
		         <th>Brand</th>
		        <th>Case</th>
		        <th>Disc/Case</th>
		        <th>Total</th>
		      </tr>
		    </thead>
		    <tbody ng-repeat="list in transactionData">
		      <tr>
		        <td><b>{{list.month}}</b></td>
		        <td><b>{{list.discountMonthWiseBean[0].brand}}</b></td>
		        <td><b>{{list.discountMonthWiseBean[0].case | INR}}</b></td>
		        <td><b>{{list.discountMonthWiseBean[0].discount | INR}}</b></td>
		        <td><b>{{list.discountMonthWiseBean[0].total | INR}}</b></td>
		      </tr>
		      <tr ng-repeat="brand in list.discountMonthWiseBean" ng-if="$index > 0">
		       <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td><b>{{brand.brand}}</b></td>
		        <td><b>{{brand.case | INR}}</b></td>
		        <td><b>{{brand.discount | INR}}</b></td>
		        <td><b>{{brand.total | INR}}</b></td>
		      </tr>
		       <tr>
		       <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="background: antiquewhite;"><b>{{list.totalDiscount | INR}}</b></td>
		      </tr>
		    </tbody>
		    <tbody>
		     <tr>
		       <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="background: antiquewhite;"><b>Grand Total Discount</b></td>
		        <td style="background: antiquewhite;"><b>{{ transactionData | totalDiscountAmt | INR}}</b></td>
		      </tr>
		      <tr>
		       <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="background: antiquewhite;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
		        <td style="background: antiquewhite;"><b>Rentals</b></td>
		        <td style="background: antiquewhite;"><b>{{ rentalAmt | INR}}</b></td>
		      </tr>
		    </tbody>
		  </table>   
		  </div>
        <div class="col-md-3"></div>
    </div>

    </div>
    
 
  </div>
</body>
</html>