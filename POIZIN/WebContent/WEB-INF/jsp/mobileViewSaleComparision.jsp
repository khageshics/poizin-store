<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
   <script src="https://cdn.jsdelivr.net/momentjs/2.10.6/moment.min.js"></script>
   <script src="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
     <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
  <spring:url value="/resources/javascript/thirdpartyjs/jquery.ui.monthpicker.js" var="jqueryuimonthpickerjs"/>
  <script src="${jqueryuimonthpickerjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/angular.min.js" var="angularminjs"/>
  <script src="${angularminjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
 <script src="${jqueryuijs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/common.css" var="commoncss"/>
  <link rel="stylesheet" href="${commoncss}">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <spring:url value="/resources/javascript/mobileView/mobileViewSaleComparision.js" var="mobileViewSaleComparisionjs"/>
  <script src="${mobileViewSaleComparisionjs}"></script> 
  <spring:url value="/resources/css/mobileViewSaleReport.css" var="mobileViewSaleReportcss"/>
  <link rel="stylesheet" href="${mobileViewSaleReportcss}">
<style>

h1, h2, h3, h4, h5, h6 {
    font-family: "Segoe UI",Arial,sans-serif;
    font-weight: 400;
    margin: 10px 0;
}
h6 {
    font-size: 16px;
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 12px;
}
.datalist {
    max-height: calc(100vh - 290px);
    overflow-y: auto;
    width: 100%;
}
summary{
display : block;
}
details summary::-webkit-details-marker { display:none; }
.bootstrap-datetimepicker-widget tr:hover {
    background-color: #808080;
}
.row {
    margin-right: auto;
    margin-left: auto;
}
.container {
    padding-right: 2px;
    padding-left: 2px;
    padding-top: 15px;
    margin-right: auto;
    margin-left: auto;
}

</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl" data-ng-init="init()">
<div >
<a href="mobileViewHome" style="text-decoration: none;">
<header style="height: 55px;">
  <h2 style="float:left;padding: 0px 0px 0px 2px;">POIZIN</h2>
  <h6 style="float: right;padding: 8px 2px 0px 0px;">SALE COMPARISION</h6>
</header></a>
<div class="tab-content " style="font-family: unset;font-size: 13px;">
<div class="container">
<div class="row allignmentcss">
    <div class="col-md-5" style="margin-bottom: 10px;display: inline-block;">
    <div style="width: 40%;float: left;">
      <label ng-click="showHideMonthAndWeekPicker('mobilemonthlyDate','weeklyDatePicker')"><input type="radio" name="weekormonth" value="1" checked> Select Month</label>
      <input type="text" class="monthpicker mobilemonthpicker" id="mobilemonthlyDate" readonly='true' style="width: 95%;">
     </div>
    <div style="width: 40%;float: left;">
	   <label ng-click="showHideMonthAndWeekPicker('weeklyDatePicker','mobilemonthlyDate')"><input type="radio" name="weekormonth" value="2"> Select Week</label>
	   <input type="text" class="dateinput" id="weeklyDatePicker" autocomplete="off" style="width: 95%;">
    </div>
     <div style="width: 20%;float: left;">
        <input class="submit" type="submit" name="" ng-click="getWeekComparisionSaleData()" value="Get" style="margin-top: 25px;"/>
     </div>
    </div>
	<div class="col-md-3" style="float: right;">
	<label class="switch">
     <input type="checkbox" id="includeZeroCompareSaleOrNot">
     <span class="slider round" style="background-color: #062351;" ng-click="includeZeroCompareSaleOrNot()"></span>
    </label>
	</div>
	<div class="col-md-4" style="float: left;font-weight: 600;color: #062351;">
	<label style="font-size: 10px;">Current: {{showCurrentSelectedDate}}</label><br><label style="font-size: 10px;">Previous: {{showPreviousSelectedDate}}</label>
	</div>
</div>
</div>
<div id="waitsec" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
<div class="datalist">
<div class="container">
  <div ng-show = "showcompareCompanyDetails">
	   <div class="row" style="border-radius: 5px;color: #062351;font-size: 11px;">
		<span class="col-xs-4" style="margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;padding: 0px 0px 0px 20px;">Company</span>
		<span class="col-xs-3" style="margin-top: 5px;text-align: center;padding: 0px;">Current Sale</span>
		<span class="col-xs-3" style="margin-top: 5px;text-align: center;padding: 0px;">Previous Sale</span>
	    <span class="col-xs-2" style="border-bottom-right-radius: 5px;border-top-right-radius: 5px;margin-top: 5px;text-align: right;padding: 0px 10px 0px 0px;">%</span>
	  </div>  
	  <details ng-repeat="list in companyWiseCompareSale | orderBy:'companyOrder'">
		<summary>
		    <div class="stockLiftList">
			 <span class="col-xs-4" style="margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};">{{list.company}}
				<span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.superParentSaleInPer < 0"></span>
				<span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.superParentSaleInPer > 0"></span>
				<span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.superParentSaleInPer == 0"></span>
			 </span>
			 <span class="col-xs-3" style="margin-top: 5px;text-align: center;background-color: {{list.bgcolor}};">{{list.totalcurrentsaleprice | INR}}</span>
			 <span class="col-xs-3" style="margin-top: 5px;text-align: center;background-color: {{list.bgcolor}};">{{list.totalprevioussaleprice | INR}}</span>
			<span class="col-xs-2" style="border-bottom-right-radius: 5px;border-top-right-radius: 5px;margin-top: 5px;text-align: right;padding-right: 2px;background-color: {{list.bgcolor}};">{{list.superParentSaleInPer}}%</span>
			</div>
		</summary>
	     <details ng-repeat="data in list.inputjson">
		   <summary>
		    <span class="col-xs-4 secondSpan" style="padding-left: 6px;text-align: left;background-color: #e8e8e8;">{{data.brandname}}
		    <span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.parentSaleInPer < 0"></span>
			<span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.parentSaleInPer > 0"></span>
			<span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.parentSaleInPer == 0"></span>
		    </span>
			<span class="col-xs-3 secondSpan" style="text-align: center;background-color: #e8e8e8;">{{data.currentSalePrice | INR}}</span>
			<span class="col-xs-3 secondSpan" style="text-align: center;background-color: #e8e8e8;">{{data.previousSalePrice | INR}}</span>
			<span class="col-xs-2 secondSpan" style="text-align: center;background-color: #e8e8e8;">{{data.parentSaleInPer}}%</span>
		  </summary>
		   <table class=" table table-fixed">
			  <tr ng-repeat="brand in data.Brands">
				<td class="col-xs-4" style="border-top: 0px solid #ddd;">{{brand.quantity}}
				<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;font-size: 10px;" ng-if="brand.saleInPercentage < 0"></p>
				<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;font-size: 10px;" ng-if="brand.saleInPercentage > 0"></p>
				<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;font-size: 10px;" ng-if="brand.saleInPercentage == 0"></p>
				</td>
				<td class="col-xs-3" style="border-top: 0px solid #ddd;">{{brand.currentSalePrice | INR}}</td>
				<td class="col-xs-3" style="border-top: 0px solid #ddd;">{{brand.previousSalePrice | INR}}</td>
				<td class="col-xs-2" style="border-top: 0px solid #ddd;">{{brand.saleInPercentage}}%</td>
				</tr>
		  </table>
		</details>
	</details>
 </div> 
 <div ng-show = "showcompareCategoryDetails">
     <div class="row" style="border-radius: 5px;color: #062351;font-size: 11px;">
	  <span class="col-xs-4" style="margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;padding: 0px 0px 0px 20px;">Category</span>
	  <span class="col-xs-3" style="margin-top: 5px;text-align: center;padding: 0px;">Current Sale</span>
	  <span class="col-xs-3" style="margin-top: 5px;text-align: center;padding: 0px;">Previous Sale</span>
	   <span class="col-xs-2" style="border-bottom-right-radius: 5px;border-top-right-radius: 5px;margin-top: 5px;text-align: right;padding: 0px 10px 0px 0px;">%</span>
	  </div>  
	  <details ng-repeat="list in categoryWiseCompareSale | orderBy:'categoryOrder'">
		<summary>
		    <div class="stockLiftList">
			 <span class="col-xs-4" style="margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};">{{list.category}}
			  <span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.superParentSaleInPer < 0"></span>
			  <span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.superParentSaleInPer > 0"></span>
			  <span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.superParentSaleInPer == 0"></span>
			 </span>
			 <span class="col-xs-3" style="margin-top: 5px;text-align: center;background-color: {{list.bgcolor}};">{{list.totalcurrentsaleprice | INR}}</span>
			 <span class="col-xs-3" style="margin-top: 5px;text-align: center;background-color: {{list.bgcolor}};">{{list.totalprevioussaleprice | INR}}</span>
			 <span class="col-xs-2" style="border-bottom-right-radius: 5px;border-top-right-radius: 5px;margin-top: 5px;text-align: right;padding-right: 2px;background-color: {{list.bgcolor}};">{{list.superParentSaleInPer}}%</span>
			</div>
		</summary>
	     <details ng-repeat="data in list.inputjson">
		   <summary>
		    <span class="col-xs-4 secondSpan" style="padding-left: 6px;text-align: left;background-color: #e8e8e8;">{{data.brandname}}
		    <span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.parentSaleInPer < 0"></span>
			<span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.parentSaleInPer > 0"></span>
			<span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.parentSaleInPer == 0"></span>
		    </span>
			<span class="col-xs-3 secondSpan" style="text-align: center;background-color: #e8e8e8;">{{data.currentSalePrice | INR}}</span>
			<span class="col-xs-3 secondSpan" style="text-align: center;background-color: #e8e8e8;">{{data.previousSalePrice | INR}}</span>
			<span class="col-xs-2 secondSpan" style="text-align: right;background-color: #e8e8e8;">{{data.parentSaleInPer}}%</span>
		  </summary>
		   <table class=" table table-fixed">
			  <tr ng-repeat="brand in data.Brands">
				<td class="col-xs-4" style="border-top: 0px solid #ddd;">{{brand.quantity}}
				<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;font-size: 10px;" ng-if="brand.saleInPercentage < 0"></p>
				<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;font-size: 10px;" ng-if="brand.saleInPercentage > 0"></p>
				<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;font-size: 10px;" ng-if="brand.saleInPercentage == 0"></p>
				</td>
				<td class="col-xs-3" style="border-top: 0px solid #ddd;">{{brand.currentSalePrice | INR}}</td>
				<td class="col-xs-3" style="border-top: 0px solid #ddd;">{{brand.previousSalePrice | INR}}</td>
				<td class="col-xs-2" style="border-top: 0px solid #ddd;">{{brand.saleInPercentage}}%</td>
				</tr>
		  </table>
		</details>
	</details> 
 
 </div>
</div>
   <!-- footer start -->
<div class="fixed">
				<div style="font-size: 12px;text-align: center;width: 40%;float: left;">
				<span>Current: </span><span> {{sumOfCurrentSaleAmt | INR}}</span>
				</div>
				<div class="" style="font-size: 12px;text-align: center;width: 40%;float: left;">
				<span>Previous: </span><span> {{sumOfPreviousSaleAmt | INR}}</span>
				</div>
			    <div class="" style="font-size: 12px;text-align: center;width: 20%;float: left;">
				<span> {{wholepercentage}}%</span>
				<span class="glyphicon glyphicon-arrow-down" style="color: red; top: 0px;line-height: inherit;" ng-if="wholepercentage < 0"></span>
			    <span class="glyphicon glyphicon-arrow-up" style="color: green; top: 0px;line-height: inherit;" ng-if="wholepercentage > 0"></span>
			    <span class="glyphicon glyphicon-arrow-up" style="color: yellow; top: 0px;line-height: inherit;" ng-if="wholepercentage == 0"></span>
			 	</div>
		</div>
<!-- footer end -->
</div>
</div>
</div>
</body>
</html>