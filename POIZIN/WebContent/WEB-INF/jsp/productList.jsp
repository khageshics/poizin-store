<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<jsp:include page="/WEB-INF/jsp/header.jsp" />  
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script> 
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script>
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
  <spring:url value="/resources/javascript/productList.js" var="productListjs"/>
  <script src="${productListjs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}"> 
   <spring:url value="/resources/css/saleReport.css" var="saleReportcss"/>
   <link rel="stylesheet" href="${saleReportcss}">
   <spring:url value="/resources/css/select2.min.css" var="select2mincss"/>
   <link rel="stylesheet" href="${select2mincss}">
	<spring:url value="/resources/javascript/thirdpartyjs/select2.min.js" var="select2minjs"/>
  <script src="${select2minjs}"></script> 
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css" rel="stylesheet" /> -->
<!-- <script>
$(function() {
  $('.selectpicker').selectpicker();
});
</script> -->
 <style>
.container{
max-width: 100%;
}
.shiva :hover{
background-color: #ccc;
}
a{
color: #fff;
}
.table-fixed thead > tr> th {
    background-color: #1a6398;
    color: #fff;
}
[type=checkbox]:checked, [type=checkbox]:not(:checked) {
    position: inherit;
    opacity: 1;
    pointer-events: inherit;
}
.color-red{
color:red;
}
.color-black{
color: black;
}
.select2-container {
    width: 100% !important;
}
.brandInput{
    width: 100%;
    border: solid 1px #ccc;
    height: 25px;
}
.table-fixed thead > tr> th {
    background-color: #243a51;
    color: #fff;
}
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<div class="row" style="padding-top: 100px;">
  <div class="col-sm-3 bodyFontCss" style="padding: 0px !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Invoice</li>
  <li>Active/Inactive Products</li>
</ul>
  </div>
  <div class="col-sm-8 bodyFontCss"></div>
  <div class="col-sm-1 bodyFontCss">
  <span><i class="fa fa-filter filter-icon" style="color: #243a51!important;" data-toggle="modal" data-target="#saleFilterModal" aria-hidden="true"></i></span>
  </div>
  </div>
 <div class="container" style="margin-top: 20px;">
  <div class="row activeOrInActive" style="margin-bottom: 20px;">
    <div class="col-sm-12 bodyFontCss">
		<table class=" table table-fixed">
		   <thead><tr>
		     <th class="col-xs-1" style="border-top-left-radius: 3px;text-align: center;">Select</th>
		     <th class="col-xs-2" style="text-align: left;">Brand Name</th>
		     <th class="col-xs-1" style="text-align: center;">Brand No</th>
		     <th class="col-xs-2" title="">Quantity</th>
		     <th class="col-xs-2" title="">Pack QTY</th>
		     <th class="col-xs-1" title="">Pack Type</th>
		     <th class="col-xs-1">Product Type</th>
		     <th class="col-xs-2" style="border-top-right-radius: 3px;">Company</th>
		 </tr></thead>
		 <tr ng-repeat="brand in productList = (categories | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined,}:true) | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined}:true | orderBy:['companyOrder','brandNo'] ">
		  <td class="col-xs-1" style="color: #000;text-align: center;border-left:  5px solid {{brand.color}}" ><input type="checkbox" ng-model="brand.activeBol" ng-click="activeOrInActive(brand.brandNoPackQty,brand.activeBol)"></td>
		  <td class="col-xs-2" style="text-align: left;" ng-class="{'color-red': brand.activeBol == false,'color-black': brand.activeBol == true}">{{brand.brandName}}</td>
		  <td class="col-xs-1" style="text-align: center;" ng-class="{'color-red': brand.activeBol == false,'color-black': brand.activeBol == true}">{{brand.brandNo}}</td>
		  <td class="col-xs-2" style="" ng-class="{'color-red': brand.activeBol == false,'color-black': brand.activeBol == true}">{{brand.quantity}}</td>
		  <td class="col-xs-2" style="" ng-class="{'color-red': brand.activeBol == false,'color-black': brand.activeBol == true}">{{brand.packQty}}</td>
		  <td class="col-xs-1" style="" ng-class="{'color-red': brand.activeBol == false,'color-black': brand.activeBol == true}">{{brand.packType}}</td>
		  <td class="col-xs-1" style="" ng-class="{'color-red': brand.activeBol == false,'color-black': brand.activeBol == true}">{{brand.productType}}</td>
		  <td class="col-xs-2" style="" ng-class="{'color-red': brand.activeBol == false,'color-black': brand.activeBol == true}">{{brand.company}}</td>
		 </tr>
	 </table>
   </div>
 </div>
  <div class="row editProduct" style="margin-bottom: 20px;">
    <div class="col-sm-3 bodyFontCss"></div>
     <div class="col-sm-4 bodyFontCss">
		<select id="brand"> </select> 
   </div>
   <div class="col-sm-2 bodyFontCss"><input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getResults()" value="Get Data" /></div>
    <div class="col-sm-3 bodyFontCss"></div>
    
     <div class="col-sm-12 bodyFontCss">
     <form action="" id="editProductForm" method="post" ng-submit="submitProductForm()">
       <div style="width: 100%;">
       <div class="col-sm-2 bodyFontCss" style="margin-bottom: 5px;">Brand No:<input type="number" id="editBrandNo" value="{{singleBrandList[0].brandNo}}" class="brandInput" required="required" readonly="readonly"></div>
       <div class="col-sm-4 bodyFontCss" style="margin-bottom: 5px;">Brand Name:<input type="text" id="editBrandName" value="{{singleBrandList[0].brandName}}" class="brandInput" required="required"></div>
       <div class="col-sm-2 bodyFontCss" style="margin-bottom: 5px;">Short Name:<input type="text" id="editshortName" value="{{singleBrandList[0].shortBrandName}}" class="brandInput" required="required"></div>
       <div class="col-sm-2 bodyFontCss" style="margin-bottom: 5px;">Match Name:<input type="text" id="editmatchName" value="{{singleBrandList[0].matchName}}" class="brandInput" required="required"></div>
       <div class="col-sm-2 bodyFontCss" style="margin-bottom: 5px;">Match Value:<input type="number" id="editMatchValue" value="{{singleBrandList[0].matchValue}}" class="brandInput" required="required"></div>
       
       </div>
     
     
		<table class=" table table-fixed" id="editProductTab">
		   <thead><tr>
		     <th class="col-xs-2">Product Type</th>
		     <th class="col-xs-2">Quantity</th>
		     <th class="col-xs-2">BTL In Case</th>
		     <th class="col-xs-1">Case Type</th>
		     <th class="col-xs-1">Category</th>
		     <th class="col-xs-2">Company</th>
		     <th class="col-xs-2">Group</th>
		     <th class="col-xs-1" style="display: none">Margin</th>
		 </tr></thead>
		<tr ng-repeat="list in singleBrandList">
		<td class="col-xs-1" style="display: none;">{{list.brandNoPackQty}}</td>
		  <td class="col-xs-2"><select name="productType" id="productType{{list.brandNoPackQty}}" required="required" class=" form-control">
		                       <option value="{{list.productType}}" selected>{{list.productType}}</option>
							   <option ng-repeat="brand in list.producttype" value="{{brand}}">{{brand}}</option>
							   </select>
		  </td>
		  <td class="col-xs-2"><select name="quantity" id="quantity{{list.brandNoPackQty}}"  required="required" class=" form-control">
		                       <option value="{{list.quantity}}" selected>{{list.quantity}}</option>
							   <option ng-repeat="brand in list.quantities" value="{{brand}}">{{brand}}</option>
							   </select>
		  </td>
		  <td class="col-xs-1"><select name="packQty" id="caseQty{{list.brandNoPackQty}}"  required="required" class=" form-control">
		                       <option value="{{list.packQty}}" selected>{{list.packQty}}</option>
							   <option ng-repeat="brand in list.packqty" value="{{brand}}">{{brand}}</option>
							   </select>
		  </td>
		  <td class="col-xs-1"><select name="packType" id="caseType{{list.brandNoPackQty}}"  required="required" class=" form-control">
		                       <option value="{{list.packType}}" selected>{{list.packType}}</option>
							   <option ng-repeat="brand in list.packtype" value="{{brand}}">{{brand}}</option>
							   </select>
		  </td>
		  <td class="col-xs-2"><select name="categoryId" id="category{{list.brandNoPackQty}}"  required="required" class=" form-control">
		                       <option value="{{list.categoryId}}" selected>{{list.categoryName}}</option>
							   <option ng-repeat="brand in list.categoryList" value="{{brand.id}}">{{brand.name}}</option>
							   </select>
		  </td>
		   <td class="col-xs-2"><select name="companyId" id="company{{list.brandNoPackQty}}"  required="required" class=" form-control">
		                       <option value="{{list.companyId}}" selected>{{list.companyName}}</option>
							   <option ng-repeat="brand in list.companyList" value="{{brand.id}}">{{brand.name}}</option>
							   </select>
		  </td>
		  <td class="col-xs-2"><select name="groupId" id="group{{list.brandNoPackQty}}"  required="required" class=" form-control">
		                       <option value="{{list.groupId}}" selected>{{list.groupName}}</option>
							   <option ng-repeat="brand in list.groupingList" value="{{brand.id}}">{{brand.name}}</option>
							   </select>
		  </td>
		  <td class="col-xs-1" style="display: none;"><input type="text" id="margin{{list.brandNoPackQty}}" value="{{list.margin}}" class="brandInput" maxlength="5" required="required"></input></td>
		 </tr> 
	 </table>
	 <p style="text-align: center;margin: 0px !important;"><input class="btn btn-default btn-rounded" type="submit" name="" value="UPDATE PRODUCT"/></p>
	 </form>
   </div>
 </div>
 </div>
   <!-- Modal -->
<div class="modal fade right" id="saleFilterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-side modal-top-right" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title w-100" id="exampleModalLabel"><i class="fa fa-filter" style="color: #243a51!important;font-size: 22px;"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="font-size: 25px;">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
      <div class="activeOrInActive">
		<select ng-model="modelfilter" class="form-control filterDropdownCss"  name="sort" style="float: left;">
			 <option value>ALL Category</option> 
		  <option ng-repeat="list in categories | unique:'category'" value="{{list.category}}">{{list.category}}</option>
	    </select> 
	   <select ng-model="companyfilter" class="form-control filterDropdownCss">
			  <option value>ALL Company</option>
		  <option ng-repeat="list in categories | unique:'company' | filter:{category:modelfilter}:true | orderBy:['companyOrder']" value="{{list.company}}">{{list.company}}</option>
	   </select>
	   </div>
	   <div style="height: 40px;">
	    <div style="float: right;padding: 12px;">
	    <label class="filterLabelFontSize">Active/In Active Products</label>
	    </div>
	    <div style="float: right;">
	    <label class="switch ">
         <input type="checkbox" id="includeZeroSaleOrNot" checked>
         <span class="slider round" style="background-color: red;" ng-click="includeZeroSaleOrNot()"></span>
        </label>
        </div>
        <div style="float: right;padding: 12px;">
        <label class="filterLabelFontSize">Edit Products</label>
	   </div>
		</div>
	 </div>
      <div class="modal-footer">
       <!--  <button type="button" style="background: #1a6398!important;" class="btn btn-primary btn-rounded" data-dismiss="modal">Apply Filters</button> -->
      </div>
    </div>
  </div>
</div>
</body>
</html>