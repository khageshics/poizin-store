<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<%--  <spring:url value="/resources/css/3.3.7.bootstrap.min.css" var="bootstrapmincss"/>
   <link rel="stylesheet" href="${bootstrapmincss}">  --%>
<jsp:include page="/WEB-INF/jsp/header.jsp" /> 
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script> 
  <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
  <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
  
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script>
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
  <spring:url value="/resources/javascript/thirdpartyjs/jquery.ui.monthpicker.js" var="jqueryuimonthpickerjs"/>
  <script src="${jqueryuimonthpickerjs}"></script>
  <!-- <script src="https://rawgit.com/zorab47/jquery.ui.monthpicker/master/jquery.ui.monthpicker.js"></script> -->
  <spring:url value="/resources/javascript/discountEstimate.js" var="discountEstimatejs"/>
  <script src="${discountEstimatejs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}"> 
  <spring:url value="/resources/css/saleReport.css" var="saleReportcss"/>
   <link rel="stylesheet" href="${saleReportcss}">
   
   <style type="text/css">
 .table-css thead > tr> th {
 background-color: #337ab7;
 color:#fff;
 font-size: 10px;
}
.table-css thead > tr> td {
 font-size: 10px;
 background: #ece9e9;
}
span > input{
  border: solid #bfacac 1px;
  border-radius: 2px;
      width: 60px;
}
td > input{
  border: solid #bfacac 1px;
  border-radius: 2px;
  width: 60px;
}
div.fixed {
    position: fixed;
    bottom: 0;
    right: 0;
    width: 100%;
    background-color: #243a51;
    color: #fff;
    font-size: 18px;
    padding: 8px;
}
.customeSpanCss{
width: 6.25%;
float: left;

}
.stockLift span {
    text-align: center;
    box-sizing: border-box;
    background-color: #243a51;
    overflow: hidden;
    text-overflow: ellipsis;
    display: inline-block;
    white-space: nowrap;
    font-size: 10px;
    text-align: center;
    padding: 8px;
    color: #fff;
    /* font-weight: 600; */
    /* font-family: 'Montserrat', sans-serif !important; */
}
.stockLiftList span {
    text-align: center;
    box-sizing: border-box;
    overflow: hidden;
    text-overflow: ellipsis;
    display: inline-block;
    white-space: nowrap;
    font-size: 10px;
    text-align: center;
    padding: 8px;
    /* color: #000; */
}
[type=checkbox]:checked, [type=checkbox]:not(:checked) {
    position: inherit;
    opacity: 1;
    pointer-events:unset;
}
.datalist {
    max-height: calc(100vh - 325px);
    overflow-y: auto;
    width: 100%;
}
.spanheader{
font-size: 10px;
}
td.tip {
    text-decoration: none;
    list-style-type: none;
}
td.tip:hover {
    position: relative
}
td.tip ul {
    display: none
}
td.tip ul > li {
        line-height: 20px;
        list-style-type: none; 
         text-align: left;
}
td.tip:hover ul {
     border: #c0c0c0 1px dotted;
    padding: 5px 20px 5px 5px;
    display: block;
    z-index: 100;
    left: 0px;
    margin: 10px;
    width: 85px;
    position: absolute;
    top: 10px;
    text-decoration: none;
    background: #243a51;
    color: #fff;
    
}
span.tip {
    text-decoration: none;
    list-style-type: none;
}
span.tip:hover {
    position: relative;
}
span.tip ul {
    display: none
}
span.tip ul > li {
        line-height: 20px;
        list-style-type: none; 
         text-align: left;
}
span.tip:hover ul {
     border: #c0c0c0 1px dotted;
    padding: 5px 20px 5px 5px;
    display: block;
    z-index: 100;
    left: -50px;
    margin: 10px;
    width: 175px;
    position: absolute;
    top: 10px;
    text-decoration: none;
    background: #243a51;
    color: #fff;
    font-family:  'Montserrat', sans-serif !important;;
    font-size: 10px;
}
details summary::-webkit-details-marker { display:none; }
summary{
display : block;
}
</style>
   
   
</head>
<body ng-app="myApp" ng-controller="myCtrl" data-ng-init="init()">
<div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
<div class="container" style="padding-top: 100px;max-width: 100%;">
 <div class="row">
  <div class="col-sm-3 bodyFontCss" style="padding: 0px !important;">
	<ul class="breadcrumb">
		<li><a href="admin">Dashboard</a></li>
		<li>Discount</li>
		<li>Discount Estimate</li>
	</ul>
  </div>
  <div class="col-sm-1 bodyFontCss"></div>
  <div class="col-sm-2 bodyFontCss">
   <label>Month</label><input type="text" class="monthpicker form-control"  id="monthlyDate" autocomplete="off">
  </div>
  <div class="col-sm-1 bodyFontCss customizedButton">
   <input class="btn btn-default btn-rounded" type="submit" ng-click="callDiscountData()" value="GET DATA" />
  </div>
  <div class="col-sm-4 bodyFontCss">
  <span class="spanheader">CP+HOU = Clearn's Period In Month (Including In-House Stock)</span><br>
  <span class="spanheader">CP-HOU = Clearn's Period In Month (Excluding In-House Stock)</span><br>
  <span class="spanheader">ROI+HOU = Rate Of Interest (Including In-House Stock)</span><br>
  <span class="spanheader">ROI-HOU = Rate Of Interest (Excluding In-House Stock)</span><br>
  </div>
  <div class="col-sm-1 bodyFontCss">
  <span><i class="fa fa-filter filter-icon" style="color: #243a51!important;" data-toggle="modal" data-target="#discountModel" aria-hidden="true"></i></span>
  </div>
  </div>
 <div class="row">
 <div class="col-xs-12">
 <input class="btn btn-default btn-rounded" type="submit" ng-click="deleteSelectedItems()" value="Delete" />
 <a style="float: right;" ng-click="generateTargetPdf()"><img alt="pdfImg" src="resources/images/pdf.png" style="float: right; margin-right: 30px;cursor: pointer;"></a>
 <div class="stockLift">
 <span class="customeSpanCss" style="text-align: center;border-top-left-radius: 3px;">Delete</span>
 <span class="customeSpanCss" style="text-align: center;">Group Name</span>
 <span class="customeSpanCss" style="text-align: center;">InHouseStock</span>
 <span class="customeSpanCss" style="text-align: center;">LastMonthSold</span>
 <span class="customeSpanCss" style="text-align: center;">LiftedCase</span>
 <span class="customeSpanCss" style="text-align: center;">Commitment</span>
 <span class="customeSpanCss" style="text-align: center;">Pending</span>
 <span class="customeSpanCss" style="text-align: center;">Mini Tar</span>
 <span class="customeSpanCss" style="text-align: center;">Disc</span>
 <span class="customeSpanCss" style="text-align: center;">Investment</span>
 <span class="customeSpanCss" style="text-align: center;">TotalDisc</span>
 <span class="customeSpanCss" style="text-align: center;">Disc%</span>
 <span class="customeSpanCss" style="text-align: center;" title="Clearn's Period In Month (Including In-House Stock)">CP+HOU</span>
 <span class="customeSpanCss" style="text-align: center;" title="Clearn's Period In Month (Excluding In-House Stock)">CP-HOU</span>
 <span class="customeSpanCss" style="text-align: center;" title="Rate Of Interest (Including In-House Stock)">ROI+HOU</span>
 <span class="customeSpanCss" style="text-align: center;border-top-lright-radius: 3px;" title="Rate Of Interest (Excluding In-House Stock)">ROI-HOU</span>
 </div>
 <div class="datalist" ng-show = "discountItem">
        <h4 ng-if="!falgvalSize" style="text-align: center;padding: 8px;">No Data Available!</h4>
 <details ng-repeat="list in discountdata | filter:{companyName:companyfilter,companyName:companyfilter||undefined}:true | filter: greaterThan('flagVal') | orderBy:'-listOrder'">
   <summary>
   <div class="stockLiftList" style="height:36px;border-right: 5px solid {{list.color}};border-bottom: 1px solid #e0e0e0;border-left:  5px solid {{list.color}};">
    <span class="customeSpanCss" style="color: #000;"><INPUT type="checkbox" name="chk" ng-model="list.flag" ng-change='deleteObjectFromList(list.grouping,list.flag)' style="margin: 0px; padding: 0px;width: 15px;"></span>
    <span class="customeSpanCss" style="color: #000;text-align: left;" title="{{list.groupName}}">
    <label ng-if="list.Brands.length > 1">+ </label> {{list.groupName}}
    </span>
    <span class="customeSpanCss" style="color: #000;">{{list.inhousestockvalue}}</span>
    <span class="customeSpanCss" style="color: #000;">{{list.lastmonthsoldold}}</span>
    <span class="customeSpanCss" style="color: #000;">{{list.liftedcase}}</span>
    <span class="customeSpanCss" style="color: #000;"><input type="number" class="" ng-model="list.sumOfTarget" ng-change="splitTargetCase(list.sumOfTarget,list);"  min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></span>
    <span class="customeSpanCss" style="color: #000;">{{list.pendingvalue}}</span>
    <span class="customeSpanCss" style="color: #000;">
    <input type="number" style="color: green;" ng-model="list.currentIndent" ng-change="splitCurrentIndent(list.currentIndent,list);"  min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" />
    </span>
    <span class="customeSpanCss" style="color: #000;"><input type="number" class="" ng-model="list.parentDisc" ng-change="splitDiscount(list.parentDisc,list);" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></span>
    <span class="customeSpanCss" style="color: #000;">{{list.investmentamount}}</span>
    <span class="customeSpanCss" style="color: #000;">{{list.totaldiscount}}</span>
    <span class="customeSpanCss" style="color: #000;">{{list.parentDiscPer}} %</span>
    <span class="customeSpanCss" style="color: #000;">{{list.parentclearnsPeriodWithStock}}</span>
    <span class="customeSpanCss" style="color: #000;">{{list.parentclearnsPeriodWithoutStock}}</span>
    <span class="customeSpanCss" style="color: #000;">{{list.parentrateOfInterestWithInhouse}} %</span>
    <span class="customeSpanCss" style="color: #000;">{{list.parentrateOfInterestWithoutInhouse}} %</span>
   </div>
   </summary>
   <table class="table table-css">
    <thead>
      <tr>
	   <th>Name</th>
	   <th>Brand no</th>
	   <th>Case Rate</th>
	   <th>Inhouse stock</th>
	   <th>Last month sold</th>
	   <th>Lifted Case</th>
	   <th style="display: none;">Target</th>
	   <th style="display: none;">Pending</th>
	   <th>Mini Tar</th>
	   <th>Disc/case</th>
	   <th>Investment</th>
	   <th>Total Disc</th>
	   <th>Disc%</th>
	   <th>A</th>
	   <th>B</th>
	   <th>C</th>
	   <th>D</th>
      </tr>
	  <tr ng-repeat="brand in list.Brands">
	    <td>{{brand.brandName}}</td>
	    <td>{{brand.brandNo}}</td>
	    <td>{{brand.caseRate}}</td>
	    <td class="tip">{{brand.inhouseStock}}
	    <ul ng-if="brand.productType == 'LIQUOR'">
			<li>2L: {{brand.splitPackType.l2Stock}}</li><li>1L: {{brand.splitPackType.l1Stock}}</li><li>Q: {{brand.splitPackType.qstock}}</li>
			<li>P: {{brand.splitPackType.pstock}}</li><li>N: {{brand.splitPackType.nstock}}</li><li>D: {{brand.splitPackType.dstock}}</li>
		</ul>
		<ul ng-if="brand.productType == 'BEER'">
			<li>LB: {{brand.splitPackType.lbstock}}</li><li>SB: {{brand.splitPackType.sbstock}}</li><li>TIN: {{brand.splitPackType.tinstock}}</li>
		</ul>
	    </td>
	    <td>{{brand.lastMonthSold}}</td>
	    <td title="{{brand.stockDates}}">{{brand.liftedCase}}</td>
	    <td style="display: none;">
	    <input type="number" ng-model="brand.targetCase" ng-change="childTargetCase(brand.targetCase,list,brand.brandNo);"  min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" />
	    </td>
	    <td style="display: none;">{{brand.pending}}</td>
	    <td style="color: green;">{{brand.currentIndent}}</td>
	    <td>{{brand.discountAmount}}
	    <span style="font-size: 9px;vertical-align: super;" class="glyphicon glyphicon-info-sign tip">
        <ul>
			<li>Max : {{brand.maxMinDiscountBean[0].maxDisc}} / {{brand.maxMinDiscountBean[0].maxDiscCase}} ({{brand.maxMinDiscountBean[0].maxDiscDate}})</li>
			<li>Min : {{brand.maxMinDiscountBean[0].minDisc}} / {{brand.maxMinDiscountBean[0].minDiscCase}} ({{brand.maxMinDiscountBean[0].minDiscDate}})</li>
			<li>Last Month : {{brand.maxMinDiscountBean[0].prevoiusDisc}} / {{brand.maxMinDiscountBean[0].previousDiscCase}} ({{brand.maxMinDiscountBean[0].previousDiscDate}})</li>
		</ul>
        </span>
	    </td>
	    <td>{{brand.investment}}</td>
	    <td>{{brand.totalDiscount}}</td>
	    <td>{{brand.discountPer}} %</td>
	    <td>{{brand.estimationMonth}}</td>
	    <td>{{brand.clearnsPeriodWithoutInhouseStock}}</td>
	    <td>{{brand.rateOfInterestWithInhouse}} %</td>
	    <td>{{brand.rateOfInterestWithoutInhouse}} %</td>
	  </tr>
  </table>
 </details>
 </div>
 <!-- all Item -->
 <div class="datalist" ng-show = "totalItem">
 <details ng-repeat="list in discountdata | filter:{companyName:companyfilter,companyName:companyfilter||undefined}:true | orderBy:'-listOrder'">
   <summary>
   <div class="stockLiftList" style="height:36px;border-right: 5px solid {{list.color}};border-bottom: 1px solid #e0e0e0;border-left:  5px solid {{list.color}};">
    <span class="customeSpanCss" style="color: #000;"><INPUT type="checkbox" name="chk" ng-model="list.flag" ng-change='deleteObjectFromList(list.grouping,list.flag)' style="margin: 0px; padding: 0px;width: 15px;"></span>
    <span class="customeSpanCss" style="color: #000;text-align: left;" title="{{list.groupName}}">
    <label ng-if="list.Brands.length > 1">+ </label> {{list.groupName}}</span>
    <span class="customeSpanCss" style="color: #000;">{{list.inhousestockvalue}}</span>
    <span class="customeSpanCss" style="color: #000;">{{list.lastmonthsoldold}}</span>
    <span class="customeSpanCss" style="color: #000;">{{list.liftedcase}}</span>
    <span class="customeSpanCss" style="color: #000;"><input type="number" class="" ng-model="list.sumOfTarget" ng-change="splitTargetCase(list.sumOfTarget,list);"  min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></span>
    <span class="customeSpanCss" style="color: #000;">{{list.pendingvalue}}</span>
    <span class="customeSpanCss" style="color: #000;">
    <input type="number" style="color: green;" ng-model="list.currentIndent" ng-change="splitCurrentIndent(list.currentIndent,list);"  min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" />
    </span>
    <span class="customeSpanCss" style="color: #000;"><input type="number" class="" ng-model="list.parentDisc" ng-change="splitDiscount(list.parentDisc,list);" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></span>
    <span class="customeSpanCss" style="color: #000;">{{list.investmentamount}}</span>
    <span class="customeSpanCss" style="color: #000;">{{list.totaldiscount}}</span>
    <span class="customeSpanCss" style="color: #000;">{{list.parentDiscPer}} %</span>
    <span class="customeSpanCss" style="color: #000;">{{list.parentclearnsPeriodWithStock}}</span>
    <span class="customeSpanCss" style="color: #000;">{{list.parentclearnsPeriodWithoutStock}}</span>
    <span class="customeSpanCss" style="color: #000;">{{list.parentrateOfInterestWithInhouse}} %</span>
    <span class="customeSpanCss" style="color: #000;">{{list.parentrateOfInterestWithoutInhouse}} %</span>
   </div>
   </summary>
   <table class="table table-css">
    <thead>
      <tr>
	   <th>Name</th>
	   <th>Brand no</th>
	   <th>Case Rate</th>
	   <th>Inhouse stock</th>
	   <th>Last month sold</th>
	   <th>Lifted Case</th>
	   <th style="display: none;">Target</th>
	   <th style="display: none;">Pending</th>
	   <th>Mini Tar</th>
	   <th>Disc/case</th>
	   <th>Investment</th>
	   <th>Total Disc</th>
	   <th>Disc%</th>
	   <th>A</th>
	   <th>B</th>
	   <th>C</th>
	   <th>D</th>
      </tr>
	  <tr ng-repeat="brand in list.Brands">
	    <td>{{brand.brandName}}</td>
	    <td>{{brand.brandNo}}</td>
	    <td>{{brand.caseRate}}</td>
	    <td class="tip">{{brand.inhouseStock}}
	    <ul ng-if="brand.productType == 'LIQUOR'">
			<li>2L: {{brand.splitPackType.l2Stock}}</li><li>1L: {{brand.splitPackType.l1Stock}}</li><li>Q: {{brand.splitPackType.qstock}}</li>
			<li>P: {{brand.splitPackType.pstock}}</li><li>N: {{brand.splitPackType.nstock}}</li><li>D: {{brand.splitPackType.dstock}}</li>
		</ul>
		<ul ng-if="brand.productType == 'BEER'">
			<li>LB: {{brand.splitPackType.lbstock}}</li><li>SB: {{brand.splitPackType.sbstock}}</li><li>TIN: {{brand.splitPackType.tinstock}}</li>
		</ul>
	    </td>
	    <td>{{brand.lastMonthSold}}</td>
	    <td title="{{brand.stockDates}}">{{brand.liftedCase}}</td>
	    <td style="display: none;">
	    <input type="number" ng-model="brand.targetCase" ng-change="childTargetCase(brand.targetCase,list,brand.brandNo);"  min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" />
	    </td>
	    <td style="display: none;">{{brand.pending}}</td>
	    <td style="color: green;">{{brand.currentIndent}}</td>
	    <td>{{brand.discountAmount}}
	    <span style="font-size: 9px;vertical-align: super;" class="glyphicon glyphicon-info-sign tip">
        <ul>
			<li>Max : {{brand.maxMinDiscountBean[0].maxDisc}} / {{brand.maxMinDiscountBean[0].maxDiscCase}} ({{brand.maxMinDiscountBean[0].maxDiscDate}})</li>
			<li>Min : {{brand.maxMinDiscountBean[0].minDisc}} / {{brand.maxMinDiscountBean[0].minDiscCase}} ({{brand.maxMinDiscountBean[0].minDiscDate}})</li>
			<li>Last Month : {{brand.maxMinDiscountBean[0].prevoiusDisc}} / {{brand.maxMinDiscountBean[0].previousDiscCase}} ({{brand.maxMinDiscountBean[0].previousDiscDate}})</li>
		</ul>
        </span>
	    </td>
	    <td>{{brand.investment}}</td>
	    <td>{{brand.totalDiscount}}</td>
	    <td>{{brand.discountPer}} %</td>
	    <td>{{brand.estimationMonth}}</td>
	    <td>{{brand.clearnsPeriodWithoutInhouseStock}}</td>
	    <td>{{brand.rateOfInterestWithInhouse}} %</td>
	    <td>{{brand.rateOfInterestWithoutInhouse}} %</td>
	  </tr>
  </table>
 </details>
 </div>
</div>
<div class="col-sm-12" style="text-align: center;margin-top: 12px;">
<input ng-if="showButton" class="btn btn-default btn-rounded" type="submit" ng-click="saveDiscountEstimate()" value="Save" />
</div>
 </div>
 <div class="fixed">
			<div class="row">
			<div class="col-sm-3" style="font-size: 12px;">
				<span>Investment: </span><span>{{investmentData}}</span>
				</div>
				<div class="col-sm-3" style="font-size: 12px;">
				<span>Mini Target Beer: </span><span>{{totalBeerData}}</span>
				</div>
				<div class="col-sm-3" style="font-size: 12px;">
				<span>Mini Target Liquor: </span><span>{{totalLiquorData}}</span>
				</div>
				<div class="col-sm-3" style="font-size: 12px;">
				<span>Discount: </span><span>{{totalDiscountData}}</span>
				</div>
			</div>
		</div>
</div>
<!-- Filter model start  -->
<div class="modal fade right" id="discountModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-side modal-top-right" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title w-100" id="exampleModalLabel"><i class="fa fa-filter" style="color: #243a51!important;font-size: 22px;"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true" style="font-size: 25px;">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
	   <select ng-model="companyfilter" class="form-control" style="display: block !important;" ng-change="footerDetails()">
		  <option value>ALL Company</option>
	  <option ng-repeat="list in discountdata | unique:'companyName' " value="{{list.companyName}}">{{list.companyName}}</option>
     </select>
	   <div style="height: 40px;">
	    <div style="float: right;padding: 12px;">
	    <label class="filterLabelFontSize">Discount Item</label>
	    </div>
	    <div style="float: right;">
	    <label class="switch ">
         <input type="checkbox" id="includeZeroSaleOrNot" checked>
         <span class="slider round" style="background-color: red;" ng-click="includeZeroSaleOrNot()"></span>
        </label>
        </div>
        <div style="float: right;padding: 12px;">
        <label class="filterLabelFontSize">Total Item</label>
	   </div>
		</div>
	 </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
<!-- Filter model End -->
</body>
</html>