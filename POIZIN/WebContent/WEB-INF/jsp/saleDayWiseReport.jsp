<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
 <jsp:include page="/WEB-INF/jsp/header.jsp" />  
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script> 
<!--   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">

  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script>
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
  <spring:url value="/resources/javascript/saleDayWiseReport.js" var="saleDayWiseReportjs"/>
  <script src="${saleDayWiseReportjs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}"> 
   <spring:url value="/resources/css/saleReport.css" var="saleReportcss"/>
   <link rel="stylesheet" href="${saleReportcss}">
     <spring:url value="/resources/css/balanceSheet.css" var="balanceSheetcss"/>
  <link rel="stylesheet" href="${balanceSheetcss}">
 <style>
.container{
max-width: 100%;
}
.evenrow{
	background-color:#81DAF5!important;font-family:Verdana,sans-serif!important;font-size:12px;color:#424242;
}

.oddrow{
	background-color:#E6E6E6!important;font-family:Verdana,sans-serif!important;font-size:12px;color:#424242;
}
textarea {
    border: solid 1px #000 !important;
    height: 35px;
    color: #000;
    font-size: 12px;
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 15px;
}
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<div class="row" style="padding-top: 100px;">
  <div class="col-sm-3 bodyFontCss" style="padding: 0px !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Reports</li>
  <li>Day Wise Sale</li>
</ul>
  </div>
  <div class="col-sm-2 bodyFontCss"></div>
   <div class="col-sm-2 bodyFontCss">
   <label>Start Date</label><input type="text" class="form-control" id="startDate" readonly='true'>
   </div>
   <div class="col-sm-1 bodyFontCss customizedButton">
   <input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getResults('startDate')" value="GET DATA" />
   </div>
  <div class="col-sm-3 bodyFontCss">
  <label>Search Products</label>
  <input type="text" class="form-control" ng-model="searchProduct">
  </div>
  <div class="col-sm-1 bodyFontCss">
  <!-- <span><i class="fa fa-filter filter-icon" style="color: #243a51!important;" data-toggle="modal" data-target="#saleFilterModal" aria-hidden="true"></i></span> -->
  </div>
  </div>
 <div class="container" style="margin-top: 20px;">
  <div class="row" style="margin-bottom: 20px;">
    <div class="col-sm-12 bodyFontCss">
      <div class="stockLift ">
        <table class=" table table-fixed" >
		  <thead><tr>
			<th class="col-xs-3" data-toggle="tooltip" data-placement="bottom" title="Brand Name">Brand Name</th>
			<th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Brand No">Brand No</th>
			<th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Product Type">Product Type</th>
			<th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="QTY">QTY</th>
			<th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Opening">Opening</th>
			<th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Received">Received</th>
			<th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Closing">Closing</th>
			<th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Sale">Sale</th>
			<th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Unit Price">Unit Price</th>
			<th class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Total Cost">Total Cost</th>
			</tr></thead>
			<tbody> 
			<tr ng-repeat="list in dayWiseSale | filter:searchProduct">
			<td class="col-xs-3" ng-class-odd="'oddrow'" ng-class-even="'evenrow'" style="text-align: left;">{{ list.brandName }}</td>
            <td class="col-xs-1" ng-class-odd="'oddrow'" ng-class-even="'evenrow'">{{ list.brandNo }}</td>
            <td class="col-xs-1" ng-class-odd="'oddrow'" ng-class-even="'evenrow'">{{ list.productType }}</td>
            <td class="col-xs-1" ng-class-odd="'oddrow'" ng-class-even="'evenrow'">{{ list.quantity }}</td>
            <td class="col-xs-1" ng-class-odd="'oddrow'" ng-class-even="'evenrow'">{{ list.opening }}</td>
            <td class="col-xs-1" ng-class-odd="'oddrow'" ng-class-even="'evenrow'">{{ list.received }}</td>
            <td class="col-xs-1" ng-class-odd="'oddrow'" ng-class-even="'evenrow'">{{ list.closing }}</td>
            <td class="col-xs-1" ng-class-odd="'oddrow'" ng-class-even="'evenrow'">{{ list.totalSale }}</td>
            <td class="col-xs-1" ng-class-odd="'oddrow'" ng-class-even="'evenrow'">{{ list.unitPrice }}</td>
            <td class="col-xs-1" ng-class-odd="'oddrow'" ng-class-even="'evenrow'">{{ list.totalPrice }}</td>
			</tr>
		    </tbody>
		</table>
      </div>
    </div>
  </div>
  
 <div class="fixed">
			<div class="row">
				<div class="col-sm-3">
				<table style="float: left;">
				<tr>
					<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Cash Amount :</th>
					<td style="float: right;">{{cashAmount}}</td>
				</tr>
				</table>
				</div>
				<div class="col-sm-3">
					<table style="float: center;">
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Card Amount :</th>
							<td style="float: right;">{{cardAmount}}</td>
						</tr>
					</table>
				</div>
				<div class="col-sm-3">
					<table style="float: center;">
					    <tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Expense Amount :</th>
							<td style="float: right;">{{expenseAmount}}</td>
						</tr>
					</table>
				</div>
				<div class="col-sm-3">
				<table style="float: right;">
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Sale Amount :</th>
							<td style="float: right;">{{dayWiseSale | totalAmount}}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
 </div>
 <!-- Modal -->
<div class="modal fade right" id="saleFilterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-side modal-top-right" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title w-100" id="exampleModalLabel"><i class="fa fa-filter" style="color: #243a51!important;font-size: 22px;"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true" style="font-size: 25px;">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
	  <!--  <div style="height: 40px;">
	    <div style="float: right;padding: 12px;">
	    <label class="filterLabelFontSize">Only Sale Items</label>
	    </div>
	    <div style="float: right;">
	    <label class="switch ">
         <input type="checkbox" id="includeZeroSaleOrNot" checked>
         <span class="slider round" style="background-color: red;" onclick="includeZeroSaleOrNot();"></span>
        </label>
        </div>
        <div style="float: right;padding: 12px;">
        <label class="filterLabelFontSize">Total Sale  Items</label>
	   </div>
		</div> -->
	 </div>
      <div class="modal-footer">
      <!--   <button type="button" style="background: #1a6398!important;" class="btn btn-primary btn-rounded" data-dismiss="modal">Apply Filters</button> -->
      </div>
    </div>
  </div>
</div>
</body>
</html>