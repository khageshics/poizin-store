<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-1.12.4.js" var="jquery1124js"/>
  <script src="${jquery1124js}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/angular.min.js" var="angularminjs"/>
  <script src="${angularminjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/angular-filter.js" var="angularfilterjs"/>
  <script src="${angularfilterjs}"></script>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
  <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script>
 <spring:url value="/resources/css/common.css" var="commoncss"/>
   <link rel="stylesheet" href="${commoncss}">
   <spring:url value="/resources/css/bootstrap.min.css" var="bootstrapmincss"/>
   <link rel="stylesheet" href="${bootstrapmincss}">
 <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <spring:url value="/resources/javascript/mobileView/mobileViewStockLiftingDiscount.js" var="mobileViewStockLiftingDiscountjs"/>
  <script src="${mobileViewStockLiftingDiscountjs}"></script> 
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
   <spring:url value="/resources/css/mobileViewStockLiftingDiscount.css" var="mobileViewStockLiftingDiscountcss"/>
   <link rel="stylesheet" href="${mobileViewStockLiftingDiscountcss}">
<style>
 .color-green {
  color: green;
}

.color-orange {
  color: orange;
}

.color-red {
  color: red;
}
.tdClasscss{
color:#000;
border-bottom: 1px solid #e8e8e8;
font-family:     font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
color: #424242;
}
.fontfamily{
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;}
    
 details summary::-webkit-details-marker { display:none; }
  .stockLiftSpan {
       height: 30px;
    border-bottom: 1px solid #e0e0e0;
    font-size: 12px;
    background: #337ab7;
    font-weight: 600;
    color: #fff;
}
.evenrow{
	background-color:#81DAF5!important;font-family:Verdana,sans-serif!important;font-size:12px;color:#424242;
}
.oddrow{
	background-color:#E6E6E6!important;font-family:Verdana,sans-serif!important;font-size:12px;color:#424242;
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 12px;
}
.datalist {
    max-height: calc(100vh - 200px);
    overflow-y: auto;
    width: 100%;
}
summary{
display : block;
}
details summary::-webkit-details-marker { display:none; }
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
<div >
<a href="mobileViewHome" style="text-decoration: none;">
<header style="height: 55px;">
  <h2 style="float:left;padding: 0px 0px 0px 2px;">POIZIN</h2>
  <h6 style="float: right;padding: 8px 2px 0px 0px;">DISCOUNT</h6>
</header></a>
<div class="tab-content " style="font-family: unset;font-size: 13px;">
<div class="container">
<div class="row allignmentcss">
   <!--  <div class="col-md-2">
	</div>
    <div class="col-md-4" style="margin-bottom: 10px;">
     <span style="float: left;font-weight: 600;color: #062351;">Arrears: {{discountData | totalArrearsAmount | INR}}</span>
    </div>
	<div class="col-md-4" style="float: right;">
	</div>
	<div class="col-md-2" style="float: left;font-weight: 600;color: #062351;">
	</div> -->
</div>
</div>
<div class="datalist">
<div class="container">
<details ng-repeat="list in discountData | orderBy:'duesAmt':true">
		<summary>
			<div class="stockLiftList">
				<span class="col-xs-6" style="margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.color}};" title="{{list.company}}">{{list.company}}</span>
				<span class="col-xs-6" style="margin-top: 5px;border-bottom-right-radius: 5px;border-top-right-radius: 5px;text-align: right;background-color: {{list.color}};" title="{{list.duesAmt}}">{{list.duesAmt | INR}}</span>
			</div>
		</summary>
			 <table class=" table table-fixed">
			 <thead><tr>
				<th class="col-xs-3 fontfamily" style="text-align: left;" data-toggle="tooltip" data-placement="bottom" title="Company" style="text-align: left;">Month</th>
				<th class="col-xs-3 fontfamily" style="text-align: right;" data-toggle="tooltip" data-placement="bottom" title="Cheque">Discount</th>
				<th class="col-xs-3 fontfamily" style="text-align: right;" data-toggle="tooltip" data-placement="bottom" title="Comment">Cheque</th>
				<th class="col-xs-3 fontfamily" style="text-align: right;" data-toggle="tooltip" data-placement="bottom" title="Comment">Arrears</th>
				</tr></thead>
			     <tr ng-repeat="brand in list.mobileViewStockDiscountInner | filter: greaterThanZero('discount','recieved','arrears', 0)" data-toggle="modal" data-target="#myModal" ng-click="openListDisconts(brand.discountAsPerCompanyBeenList,brand.date,brand.companyId,brand.realDate,brand.discount,brand.rentals)">
					<td class="col-xs-3 fontfamily" style="border-top: 0px solid #ddd;text-align: left;" title="{{brand.date}}">{{brand.date}}</td>
				    <td class="col-xs-3 fontfamily" style="border-top: 0px solid #ddd;text-align: right;"  title="{{brand.discount}}">{{brand.discount | INR}}</td>
				    <td class="col-xs-3 fontfamily" style="border-top: 0px solid #ddd;text-align: right;" title="{{brand.recieved}}">{{brand.recieved | INR}}</td>
				    <td class="col-xs-3 fontfamily" style="border-top: 0px solid #ddd;text-align: right;" title="{{brand.arrears}}">{{brand.arrears | INR}}</td>
				</tr></table>
	</details>

  </div>
  <!-- footer start -->
<div class="fixed">
			<div class="row">
				<div class="col-xs-6" style="text-align: left;">
					 <table>
						<tr>
							<th style="font-size: 12px; font-style: italic;padding-right: 5px;">Arrears: </th>
							<td style="float: right;">{{discountData | totalArrearsAmount | INR}}</td>
						</tr>
					</table>
				</div>
				<div class="col-xs-6" style="">
			    <!--  <table style="float: right;">
				        <tr>
							<th style="font-size: 12px; font-style: italic;padding-right: 5px;">Sale Price: </th>
							<td style="float: right;">{{stockdata | salePrice | INR}}</td>
						</tr>
					</table> -->
				</div>
			</div>
		</div>
<!-- footer end -->
</div>
</div>
</div>
<!-- model pupop open -->
		<div class="modal fade sbi2-popup-styles" id="myModal" role="dialog">
	    <div class="modal-dialog modal-lg">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	            <h4 class="modal-title" style="float: left;width: 20%;">{{date}}</h4> <h6 class="modal-title" style="float: right;width: 35%;">Rental Amt: {{rentalsAmt | INR}}</h6>
	             <h6 class="modal-title" style="float: right;width: 35%;">Disc. Amt: {{discAmt | INR}}</h6>
	        </div>
	        <div class="modal-body">
		    <div class="stockLiftSpan" style="height:30px;padding: 5px;border-bottom: 1px solid #e0e0e0;">
						    <span class="col-xs-4" style="text-align: left;">Product Name</span>
						    <span class="col-xs-4" style="text-align: center;">Case Qty</span>
						    <span class="col-xs-4" style="text-align: center;">Discount</span>
						</div>
				<details ng-repeat="list in inputjson | filter: greaterThan('totalCaseQty', 0) | orderBy : 'order'">
					<summary>
						<div class="stockLiftList shiva" style="height:30px;padding: 0px;" ng-class="$even ? 'oddrow' : 'evenrow'">
						    <span class="col-xs-4" style="color: #000;text-align: left;padding: 5px;">{{list.productName}}</span>
						    <span class="col-xs-4" style="color: #000;text-align: center;padding: 5px;">{{list.totalCaseQty | INR}}</span>
						    <span class="col-xs-4" style="color: #000;text-align: center;padding: 5px;">{{list.totalDiscount | INR}}</span>
						</div>
					</summary>
					<table class=" table table-fixed">
						   <thead><tr>
						    <th class="col-xs-4" style="background: #1a6398 !important;">Brand Name</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;">Lifted Case (After ADJ)</th>
						    <th class="col-xs-3" style="background: #1a6398 !important;">Disc/Case</th>
						    <th class="col-xs-3" style="background: #1a6398 !important;">Total Disc</th>
						    </tr></thead>
						    <tr ng-repeat="brand in list.Brands | filter: greaterThan('caseQty', 0)"">
							<td class="col-xs-4" style="color: #000;">{{brand.brandName}}</td>
							<td class="col-xs-2" style="color: #000;">{{brand.caseQty | INR}}</td>
							<td class="col-xs-3" style="color: #000;">{{brand.discountAmt | INR}}</td>
							<td class="col-xs-3" style="color: #000;">{{brand.totalDisc | INR}}</td>
							</tr>
						  </table>
					</details>
		    <br>
		    <table class=" table table-fixed" >
			  <thead><tr>
			    <th class="col-xs-4 fontfamily" style="text-align: left;" data-toggle="tooltip" data-placement="bottom" title="Cheque">Tr. Date</th>
				<th class="col-xs-4 fontfamily" style="text-align: right;" data-toggle="tooltip" data-placement="bottom" title="Cheque">Amount</th>
				<th class="col-xs-2 fontfamily" style="text-align: right;" data-toggle="tooltip" data-placement="bottom" title="Comment">ADJ Amt</th>
				<th class="col-xs-2 fontfamily" style="text-align: right;" data-toggle="tooltip" data-placement="bottom" title="Arrears">Image</th>
				</tr></thead>
				<tr ng-repeat="discountlist in discountasCompanyBeen">
				<td class="col-xs-4 tdClasscss fontfamily" style="border-bottom: 1px solid #e8e8e8;text-align: left;" title="{{discountlist.transactionDate}}">{{discountlist.transactionDate}}</td>
				<td class="col-xs-4 tdClasscss fontfamily" style="text-align: right;border-bottom: 1px solid #e8e8e8;" title="{{discountlist.transactionAmt | INR}}">{{discountlist.transactionAmt | INR}}</td>
				<td class="col-xs-2 tdClasscss fontfamily" style="text-align: right;border-bottom: 1px solid #e8e8e8;" title="{{discountlist.adjCheck}}">{{discountlist.adjCheck}}</td>
				<td class="col-xs-2 glyphicon glyphicon-picture" style="text-align: right;" data-toggle="modal" data-target="#myModalForImage" ng-click="openChekImg(discountlist.ngalleryImage,discountlist.imageName)" style="color:#000;border-bottom: 1px solid #e8e8e8;">
				</tr>	
		    </table> 
	        
	        </div>
	      </div>
	    </div>
  </div>
		
		<!-- model pupop close -->
		
		<!-- image popup open -->
		<div class="modal fade sbi2-popup-styles" id="myModalForImage" role="dialog">
	    <div class="modal-dialog modal-lg">
	      <div class="modal-content">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <a style="font-size: 16px;" href="mobileView/downloadChecks?imagePath={{imageName}}"><span class="glyphicon glyphicon-download-alt"></span></a>
     	        </div>
	        <div class="modal-body">
	        <img src= "{{image}}" style="width:100%;height:100%;">
	        </div>
	      </div>
	    </div>
      </div>
		<!-- image popup close -->
</body>
</html>