<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <spring:url value="/resources/css/3.3.7.bootstrap.min.css" var="bootstrap1mincss"/>
   <link rel="stylesheet" href="${bootstrap1mincss}">
   
 <jsp:include page="/WEB-INF/jsp/header.jsp" /> 
    <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script>
 <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
<script src="${jqueryuijs}"></script> 
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 06-->
<spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">

<!-- <script src="https://rawgit.com/zorab47/jquery.ui.monthpicker/master/jquery.ui.monthpicker.js"></script> 06-->
 <spring:url value="/resources/javascript/thirdpartyjs/jquery.ui.monthpicker.js" var="jqueryuimonthpickerjs"/>
  <script src="${jqueryuimonthpickerjs}"></script>
  
 <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script>
      <spring:url value="/resources/javascript/mobileView/mobileViewEnterSaleSheet.js" var="mobileViewEnterSaleSheetjs"/>
  <script src="${mobileViewEnterSaleSheetjs}"></script> 
   <spring:url value="/resources/css/saleReport.css" var="saleReportcss"/>
   <link rel="stylesheet" href="${saleReportcss}">

<style>

.uploadAndVewCss{
       color: #fff;
    margin-top: 10px;
    background: #243a51;
    padding: 10px;
    border-radius: 5px;
}
.uploadAndVewCssInner{
    color: #000;
   /*  margin-top: 10px; */
    background: #e0eef3;
    font-weight: 600;
    text-align: left;
    padding: 10px;
}
.innerth{
width:33.3%;
font-size: 12px;
color: #000;
text-align: center;
background: #fff;
font-weight: 600;

}
.innerthcontent{
font-weight: normal;
height:28px;
}
.ui-state-hover .ui-icon, .ui-state-focus .ui-icon, .ui-button:hover .ui-icon, .ui-button:focus .ui-icon {
    background-image: url(https://code.jquery.com/ui/1.12.1/themes/base/images/ui-icons_444444_256x240.png);
}
.ui-widget-header .ui-icon {
    background-image: url(https://code.jquery.com/ui/1.12.1/themes/base/images/ui-icons_444444_256x240.png);
}

</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<!-- <div class="row" style="padding-top: 100px;">
  <div class="col-sm-3 bodyFontCss" style="padding: 0px !important;">
  
  </div>
  <div class="col-sm-9 bodyFontCss"></div>
  </div> -->
<input type="hidden" name="popupmsg" id="popupmsg" value="${message}"  />
<!-- <div class="tab-content " style="font-family: unset;font-size: 13px;"> -->
<div>
			<div class="row" style="display: inherit;padding-top: 100px;">
			<div class="col-md-4" style=" padding: 0px !important;">
			<ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Invoice</li>
  <li>Upload/View Invoice</li>
</ul>
			</div>
			<form action="./addNewIndentFile" class="form_span" id="" enctype="multipart/form-data" method="post">
				<div class="col-md-2">
				  <label>Select Date</label> 
			      <input type="text" id="indentDate" name="indentDate" class=" form-control"  readonly='true' required>
			      </div>
				<div class="col-md-2" style="font-weight: 600;">
				 <label>Choose pdf File</label>
					<input type="file" id="ngalleryImage" name="ngalleryImage"	accept="application/pdf" required="required" placeholder="Select File"	class="">
				</div>
				<div class="col-md-2" style="font-weight: 600;">
					<input class="btn btn-default btn-rounded" type="submit" name="" value="SUBMIT" />
				</div>
				<div class="col-md-2"></div>
				</form>
			</div>
			<div class="container">
						<details ng-repeat="brand in indentList">
							<summary>
								<div class="uploadAndVewCss">
								    <span>{{brand.year}}</span>
								    <span style="float:right;">{{brand.totalInvoice | INR}}</span>
								</div>
							</summary>
							<details ng-repeat="data in brand.uploadAndViewInvoiceBean">
				              <summary>
				              <div class="uploadAndVewCssInner">
								    <span>{{data.date}}</span>
								    <span  style="float: right;">{{data.invoiceAmt | INR}}</span>
								</div>
				              </summary>
							<table class=" table table-fixed">
								   <thead><tr>
								    <th class="innerth">Date</th>
								    <th class="innerth">View</th>
								    <th class="innerth">Download</th>
								    </tr></thead>
								    <tr ng-repeat="list in data.addNewIndentBeanList">
									<td class="innerth innerthcontent">{{list.indentDate}}</td>
									<td class="innerth innerthcontent">
									 <a href="{{list.ngalleryImage}}" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a>
									</td>
									<td class=" innerth innerthcontent">
									<a href="downloadInvioce?filePath={{list.imageName}}"><i class="glyphicon glyphicon-download-alt"></i></a></td>
									</tr>
								  </table>
						      </details>
						 </details>
			</div>
		</div>    
<!-- </div> -->
</body>
</html>