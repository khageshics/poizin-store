<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<%--  <spring:url value="/resources/css/3.3.7.bootstrap.min.css" var="bootstrapmincss"/>
   <link rel="stylesheet" href="${bootstrapmincss}">  --%>
<jsp:include page="/WEB-INF/jsp/header.jsp" /> 
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script> 
  <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
  <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
  
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script>
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
  <spring:url value="/resources/javascript/thirdpartyjs/jquery.ui.monthpicker.js" var="jqueryuimonthpickerjs"/>
  <script src="${jqueryuimonthpickerjs}"></script>
  <!-- <script src="https://rawgit.com/zorab47/jquery.ui.monthpicker/master/jquery.ui.monthpicker.js"></script> -->
  <spring:url value="/resources/javascript/discountcalculation.js" var="discountcalculationjs"/>
  <script src="${discountcalculationjs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}"> 
  <spring:url value="/resources/css/saleReport.css" var="saleReportcss"/>
   <link rel="stylesheet" href="${saleReportcss}">
 <style>
.container{
max-width: 100%;
}
summary{
display : block;
}
.shiva :hover{
background-color: #ccc;
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 15px;
}
.inputfieldcss{
    border: solid 1px #ccc;
    width: 60px;
    height: 16px;
    font-size: 12px;
}

.superinputfieldcss {
    border: solid 1px #ccc;
    width: 100px;
    height: 18px;
    font-size: 12px;
}
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl" data-ng-init="init()">
<div class="row" style="padding-top: 100px;">
 <div class="col-sm-3 bodyFontCss" style="padding: 0px; !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Discount</li>
  <li>Discount Calculation</li>
</ul>
  </div>
  <div class="col-sm-2 bodyFontCss"></div>
   <div class="col-sm-2 bodyFontCss">
   <label>Month:</label><input type="text" class="form-control" id="monthlyDate" readonly='true'>
   </div>
   <div class="col-sm-1 bodyFontCss customizedButton">
   <input class="btn btn-default btn-rounded" type="submit" name="" ng-click="callDiscountCalculation()" value="Get Data" />
   </div>
  <div class="col-sm-3 bodyFontCss"></div>
  <div class="col-sm-1 bodyFontCss">
  </div>
  </div>
 <div class="container" style="margin-top: 20px;">
  <div class="row" style="margin-bottom: 20px;">
  <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
    <div class="col-sm-12 bodyFontCss">
      <div class="stockLift ">
      <span class="col-xs-4" style="text-align: center;border-top-left-radius: 3px;">Category</span>
	  <span class="col-xs-4" style="text-align: center;">Sale (Cases)</span>
	  <span class="col-xs-4" style="text-align: center;border-top-right-radius: 3px;">Comp. Disc</span>
    </div>
    <div class="datalist">
    <details ng-repeat="list in alldetails">
		<summary>
		<div class="stockLiftList shiva">
		<span class="col-xs-4" style="text-align: left;border-bottom: solid 1px #ccc;">{{list.categoryName}}</span>
		<span class="col-xs-4" style="text-align: center;border-bottom: solid 1px #ccc;">
		<input type="number" class="superinputfieldcss" ng-model="list.totalSale"  min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" />
		</span>
		<span class="col-xs-4" style="text-align: center;border-bottom: solid 1px #ccc;">
		<input type="number" class="superinputfieldcss" ng-model="list.totalDiscount"  min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" />
		</span>
		</div>
		</summary>
		<table class=" table table-fixed">
		   <thead><tr>
			<th class="col-xs-1" title="BrandNo">BrandNo</th>
			<th class="col-xs-3" title="BrandName">BrandName</th>
			<th class="col-xs-1" title="PackType">PackType</th>
			<th class="col-xs-1" title="PackQTY">PackQTY</th>
			<th class="col-xs-1" title="Landing Cost">Landing Cost</th>
			<th class="col-xs-1" title="Bottle MRP">Bottle MRP</th>
			<th class="col-xs-1" title="Sale">Sale(Cases)</th>
			<th class="col-xs-1" title="Disc/Case">Disc/Case</th>
			<th class="col-xs-1" title="Comp Disc">Comp Disc</th>
			<th class="col-xs-1" title="Comp Disc %">Comp Disc %</th>
			</tr>
			</thead>
			<tr ng-repeat="brand in list.Brands | orderBy:'brandNo'">
			<td class="col-xs-1"  title="{{brand.brandNo}}">{{brand.brandNo}}</td>
			<td class="col-xs-3"  title="{{brand.brandName}}">{{brand.brandName}}</td>
			<td class="col-xs-1"  title="{{brand.packType}}">{{brand.packType}}</td>
			<td class="col-xs-1"  title="{{brand.packQty}}">{{brand.packQty}}</td>
			<td class="col-xs-1"  title="{{brand.landingCost}}">{{brand.landingCost}}</td>
			<td class="col-xs-1"  title="{{brand.btlMrp}}">{{brand.btlMrp}}</td>
			<td class="col-xs-1"  title="{{brand.sale}}">
			<input type="number" class="inputfieldcss" ng-model="brand.sale" ng-change="changeSaleData(brand.sale,brand);"  min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" />
			</td>
			<td class="col-xs-1"  title="{{brand.discInPer}}">
			<input type="number" class="inputfieldcss" ng-model="brand.discInPer" ng-change="changeDiscountData(brand.discInPer,brand);"  min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" />
			</td>
			<td class="col-xs-1"  title="{{brand.totalDiscount}}">{{brand.totalDiscount}}</td>
			<td class="col-xs-1"  title="{{brand.companyDiscper}}">{{brand.companyDiscper}}%</td>
			</tr>
	  </table>
	  </details>
    </div>
    </div>
   </div>
  <div class="fixed">
			<div class="row">
				<!-- <div class="col-sm-4">
					<table>
					    <tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Available Stock : </th>
							<td style="float: right;">111111</td>
						</tr>
					</table>


				</div>
				<div class="col-sm-4">
					<table style="float: center;">
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Stock Amount : </th>
							<td style="float: right;">222222</td>
						</tr>
					</table>
				</div>
				<div class="col-sm-4" style="">
				<table style="float: right;">
				        <tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Mrp Amount : </th>
							<td style="float: right;">333333</td>
						</tr>
					</table>
				</div> -->
			</div>
		</div>
 </div>

</body>
</html>