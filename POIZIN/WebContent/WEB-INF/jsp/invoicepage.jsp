<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
  <jsp:include page="/WEB-INF/jsp/header.jsp" /> 
  
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
  <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 06-->
   <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
   
  <spring:url value="/resources/css/select2.min.css" var="select2mincss"/>
   <link rel="stylesheet" href="${select2mincss}">
	<spring:url value="/resources/javascript/thirdpartyjs/select2.min.js" var="select2minjs"/>
  <script src="${select2minjs}"></script>	
 <spring:url value="/resources/javascript/invoice.js" var="invoicejs"/>
  <script src="${invoicejs}"></script> 
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script>
  <spring:url value="/resources/javascript/buildInvoice.js" var="buildInvoicejs"/>
  <script src="${buildInvoicejs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
  <link rel="stylesheet" href="${newCommoncss}">
  <spring:url value="/resources/css/invoiceEntry.css" var="invoiceEntrycss"/>
  <link rel="stylesheet" href="${invoiceEntrycss}">
</head>
<body>
 <div class="container" style="padding-top: 100px;">
 <div class="row" style="margin-bottom: 20px;">
  <div class="col-sm-3 " style="padding: 0px !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Invoice</li>
  <li>Add Invoice</li>
</ul>
  </div>
    <input type="hidden" placeholder="" id="productTyp" name="productTyp">
    <div class="col-sm-2 bodyFontCss">
     <label>Date As Per Sheet</label>
     <input type="text" placeholder="Select Date" class="form-control" id="datepicker" name="invoiceDate" readonly="readonly" required>
    </div>
    <div class="col-sm-1 bodyFontCss customizedButton">
     <input class="ferchExistibgInvoice btn btn-default btn-rounded" type="submit" name="" value="GET DATA" />
    </div>
    <div class="col-sm-2 bodyFontCss newInvoiceForm">
     <label>Date As Per Invoice</label>
     <input type="text" class=" form-control" id="dateAsPerCopy" required readonly="readonly">
    </div>
    <div class="col-sm-3 bodyFontCss newInvoiceForm">
    <label>Select Product</label>
     <select id="brand"> </select>
    </div>
    <div class="col-sm-1 bodyFontCss newInvoiceForm customizedButton">
    <input class="btn btn-default btn-rounded" type="submit" name="" onclick="myFunction()" value="GET DATA" />
    </div>
 </div>
 <div class="row newInvoiceForm" style="margin-bottom: 20px;">
    <div class="col-sm-1 bodyFontCss "></div>
    <div class="col-sm-1 bodyFontCss">
     <label >Brand No.</label>
     <input type="text" class="form-control" id="brdNum" name="brandNumber" readonly="readonly" required>
    </div>
    <div class="col-sm-1 bodyFontCss">
     <label >Cases</label>
     <input type="text" class="form-control" id="caseQty" name="caseQty" onkeyup="validationFun(this); calculateTotalPrice();" required>
    </div>
    <div class="col-sm-1 bodyFontCss">
     <label>Bottles</label>
     <input type="text" class="form-control" id="QtyBottels" onkeyup="validationFun(this); calculateTotalPrice();" name="QtyBottels" required>
    </div>
    <div class="col-sm-1 bodyFontCss ">
     <label>Case Price</label>
     <input type="text" class="form-control" id="packQtyRate" onkeyup="validationDecimalFun(this); calculateTotalPrice();"  name="packQtyRate"  required>
    </div>
    <div class="col-sm-1 bodyFontCss ">
    <label>BTL Price</label>
    <input type="text" class="form-control" id="SingleBottelRate" onkeyup="validationDecimalFun(this); calculateTotalPrice();" name="SingleBottelRate"  required>
    </div>
    <div class="col-sm-1 bodyFontCss newInvoiceForm">
     <label>BTL MRP</label>
     <input type="text" class="form-control" id="EachBottleMrp" name="EachBottleMrp" onkeyup="validationDecimalFun(this)" required>
    </div>
    <div class="col-sm-1 bodyFontCss newInvoiceForm">
     <label>MARGIN</label>
     <input type="text" class="form-control" id="specialMarginID" name="specialMarginID" required>
    </div>
    <div class="col-sm-1 bodyFontCss">
     <label>Total Price</label>
     <span id="setTotalPrice"></span>
     <input type="hidden" id="brandNoPackQtyId" name="brandNoPackQty" value="">
	 <input type="hidden" id="packQtyId" name="packQty" value="">
     <input type="hidden" id="saleId" name="saleId" value="">
    </div>
    <div class="col-sm-1 bodyFontCss customizedButton">
     <input type="button" class="add-row btn btn-default btn-rounded" value="ADD ROW" />
    </div>
    <div class="col-sm-2 bodyFontCss newInvoiceForm"></div>
 </div>
 <div class="row newInvoiceForm">
   <div id="idate" style=""></div>
   <div class="col-sm-12 bodyFontCss">
    <table id="ItemsTable">
        <thead>
            <tr style="color: #fff;background-color: #062351;">
                <th>Select</th>
                <th>S No.</th>
                <th>Brand Number</th>
                <th>Brand Name</th>
                <th>Product Type</th>
                <th>Pack Type</th>
                <th>Pack QTY</th>
                <th>Size(ml)</th>
                <th>QTY (Cases Delivered) </th>
                <th>QTY(Bottles Delivered)</th>
                <th>Unit Rate</th>
                <th>BTL Rate </th>
                <th>BTL MRP</th>
                <th>Total Price</th>
                <th>Margin</th>
            </tr>
        </thead>
         <tbody id="invoiceTrList">
        </tbody> 
    </table>
   </div>
  </div>
 <div class="row newInvoiceForm" style="margin-bottom: 20px;">
    <div class="col-sm-1 bodyFontCss">
     <label>MRPRoundOff</label>
     <input type="number" value="0" class="form-control" id="mrpRoundOff" name="mrpRoundOff"/>
    </div>
    <div class="col-sm-1 bodyFontCss">
     <label>Turnover Tax</label>
     <input type="number" value="0" class="form-control" id="turnoverTax" name="turnoverTax"/>
    </div>
    <div class="col-sm-1 bodyFontCss">
     <label>TCS</label>
     <input type="number" value="0" class="form-control" id="tcsVal" name="tcsVal" name="turnoverTax"/>
    </div>
    <div class="col-sm-1 bodyFontCss">
     <label>Retailer CB</label>
     <input type="number" value="0" class="form-control" id="retailerCreditVal" name="retailerCreditVal" name="retailerCreditVal"/>
    </div>
     <div class="col-sm-4 bodyFontCss">
      <div id="ttlmrp"></div>
    </div>
     <div class="col-sm-2 bodyFontCss">
     <input type="button" class="delete-row btn btn-default btn-rounded" value="DELETE SELECTED ROW" />
    </div>
    <div class="col-sm-2 bodyFontCss">
     <input type="button" onclick="postInvoiceDetails()" class=" btn btn-default btn-rounded" value="SUBMIT" />
    </div>
 </div>
 
 <div id="content">
        <table style="border: none;    display: none;">
        <thead>
            <tr>
                <th style="border: none;">Brand Number</th>
                <th style="border: none;">Brand Name</th>
                <th style="border: none;">Product Type:</th>
                
            </tr>
            <tr>
            <td style="border: none;"><label id="brandNolbl"></label></td>
            <td style="border: none;"><label id="brandNamelbl"></label></td>
            <td style="border: none;"><label id="productTypelbl"></label></td>
           
            </tr>
            <tr><th style="border: none;">Quantity</th>
                <th style="border: none;">No. Of Bottles IN Carton:</th>
                <th style="border: none;">Carton Type</th>
             </tr>
             <tr> <td style="border: none;"><label id="quantitylbl"></label></td>
            <td style="border: none;"><label id="packQtylbl"></label></td>
            <td style="border: none;"><label id="packTypelbl"></label></td></tr>
        </thead>
        <tbody>
        </tbody>
    </table>
      
      </div>
</div>
</body>
</html>