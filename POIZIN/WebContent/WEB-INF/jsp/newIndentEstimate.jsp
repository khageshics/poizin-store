<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<%--  <spring:url value="/resources/css/3.3.7.bootstrap.min.css" var="bootstrap1mincss"/>
   <link rel="stylesheet" href="${bootstrap1mincss}"> --%>
   
<jsp:include page="/WEB-INF/jsp/header.jsp" /> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script> 
<!--   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">

  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script>
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
  <spring:url value="/resources/javascript/newIndentEstimate.js" var="newIndentEstimatejs"/>
  <script src="${newIndentEstimatejs}"></script>
   <spring:url value="/resources/javascript/estimation.js" var="estimationjs"/>
  <script src="${estimationjs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}"> 
   <spring:url value="/resources/css/IndentEstimate.css" var="IndentEstimatecss"/>
   <link rel="stylesheet" href="${IndentEstimatecss}">

     <spring:url value="/resources/css/select2.min.css" var="select2mincss"/>
   <link rel="stylesheet" href="${select2mincss}">
	<spring:url value="/resources/javascript/thirdpartyjs/select2.min.js" var="select2minjs"/>
  <script src="${select2minjs}"></script>
  <style>
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 15px;
 color: :#fff !important;
 background: #243a51;
}
[type=checkbox]:checked, [type=checkbox]:not(:checked) {
    position: inherit;
    opacity: 1;
    pointer-events: inherit;
}
.sticky {
  position: fixed;
  top: 60px;
  width: 100%;
  
 
}
/* .sticky tr{
width: 100%;

} */
td.tip {
    /* border-bottom: 1px dashed; */
    text-decoration: none;
    list-style-type: none;
}
td.tip:hover {
   /*  cursor: help; */
    position: relative
}
td.tip ul {
    display: none
}
td.tip ul > li {
        line-height: 20px;
        list-style-type: none; 
         text-align: left;
}
td.tip:hover ul {
     border: #c0c0c0 1px dotted;
    padding: 5px 20px 5px 5px;
    display: block;
    z-index: 100;
    left: 0px;
    margin: 10px;
    width: 130px;
    position: absolute;
    top: 10px;
    text-decoration: none;
    background: #243a51;
    color: #fff;
    
}
.container {
    padding-right: 0px;
    padding-left: 0px;
    margin-right: auto;
    margin-left: auto;
}

.stickyandtop th { 
position: sticky; 
top: 10%;
 } 
.hidetdth{
display: none;
}
/* .fade:not(.show) {
    opacity: inherit;
} */
</style>


</head>
<body ng-app="myApp" ng-controller="myCtrl" data-ng-init="init()">
<div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
<div class="container" style="padding-top: 85px;">
<ul id="tabs" class="nav nav-tabs" role="tablist">
  <li role="presentation" class="active"><a href="#indentEstimateDiv" role="tab" data-toggle="tab" style="font-size: small;">Regular</a></li>
  <li role="presentation"><a href="#schemeEstimateDiv" role="tab" data-toggle="tab" style="font-size: small;" ng-click="getSchemeEstimateData()">Scheme</a></li>
</ul>
<!-- Indent Estimate Start -->
<div class="tab-content" style="padding: 0px;">
<div id="indentEstimateDiv" role="tabpanel" class="tab-pane fade in active fadecss">
 <div class="row">
 <div class="col-sm-3 bodyFontCss" style="padding: 0px !important;"> </div>
  <div class="col-sm-2 bodyFontCss customizedButton">
  <label style="font-size: 12px; display: none;" >Date: {{month}}</label>
  </div>
  <div class="col-sm-2 bodyFontCss">
  <label>Filter</label>
    <select ng-model="companyfilter" class="form-control" style="display: block !important;" ng-change="totalInvstAmt()">
		  <option value>ALL Company</option>
	  <option ng-repeat="list in alldetails | unique:'company' " value="{{list.company}}">{{list.company}}</option>
     </select>
  </div>
  <div class="col-sm-2 bodyFontCss">
   <label>Date</label><input type="text" class="form-control" id="datepicker" readonly='true'>
  </div>
  <div class="col-sm-1 bodyFontCss">
   <label>Days</label><input type="text" class="form-control" id="noOfDays" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
  </div>
   <div class="col-sm-1 bodyFontCss customizedButton">
   <input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getResults('noOfDays')" value="GET" />
   </div>
  <div class="col-sm-1 bodyFontCss customizedButton">
  <button type="button" class="btn btn-default btn-rounded" ng-click="buildModelPopupData()" data-toggle="modal" >ADD</button>
  </div>
  </div>
  <button type="button" class="btn btn-default btn-rounded" ng-click="deleteRow('indentEstimate')">DELETE</button>
  <a style="float: right;" href="downloadendentEstimateDiscount">
   <img alt="pdfImg" src="resources/images/pdf.png" style="float: right; margin-right: 30px;cursor: pointer;"></a>
   <div class="row" style="margin-bottom: 20px;">
     <table class="saledetailreport" id="indentEstimate">
					<thead class="stickyandtop">
					  <tr>
					  <th class="hideTotalPrice"></th>
						<th title="Brand Name" style="text-align: left;">Brand Name</th>
						<th class="hideTotalPrice">Brand No</th>
						<th class="hideTotalPrice" title="AVG(Unit Rate)">Case Rate</th>
						<th class="hideTotalPrice" title="Last 30 days Sold">Last 30 days Sold</th>
						<th class="hideTotalPrice" title="Current Avaiable Stock">In-House Stock</th>
						<th class="hideTotalPrice" title="Current Month Target Case">Commitment</th>
						<th class="hideTotalPrice" title="Sum Of Current Month Lifted Case">Lifted Case</th>
						<th class="hideTotalPrice" title="(Commitment - Lifted Case)">Pending</th>
						<th class="">Indent Case</th>
						<th class="hideTotalPrice" title="(IndentCase * Case Rate)">Investment</th>
						<th>2L</th>
						<th>1L</th>
						<th>Q</th>
						<th>P</th>
						<th>N</th>
						<th>D</th>
						<th>LB</th>
						<th>SB</th>
						<th>TIN</th>
						<th style="display: none;" class="">specialMrp</th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;" class=""></th>
						<th style="display: none;"></th>
						<th class="hideTotalPrice">Edit</th>
					  </tr>
					  </thead>
					  <tbody id="pupopNewProduct">
					  <tr id="{{$index}}" ng-repeat="brand in alldetails | filter:{company:companyfilter,company:companyfilter||undefined}:true  | orderBy:['companyOrder','-pending','-productType','brandName']">
						
						<td class="hideTotalPrice" style="color: #000;background: linear-gradient(to right, {{brand.companyColor}} 15%, white 6%);"><INPUT type="checkbox" name="chk" value="" style="margin: 0px; padding: 0px;width: 15px;"></td>
						<td  id="bname{{brand.brandNo}}" style="color: #000;">{{brand.brandName}}</td>
						<td class="hideTotalPrice" id="{{brand.brandNo}}" style="color: #000;">{{brand.brandNo}}</td>
						<td class="hideTotalPrice" id="caserate{{brand.brandNo}}" style="color: #000;">{{brand.caseRate}}</td>
						<td class="hideTotalPrice tip" id="lastMonthSold{{brand.brandNo}}" style="color: #000;">{{brand.lastMonthSold}}
						<ul ng-if="brand.productType == 'LIQUOR'"><li>2L: {{brand.l2sale}}</li><li>1L: {{brand.l1sale}}</li><li>Q: {{brand.qsale}}</li>
						<li>P: {{brand.psale}}</li><li>N: {{brand.nsale}}</li><li>D: {{brand.dsale}}</li>
						</ul>
						<ul ng-if="brand.productType == 'BEER'">
						<li>LB: {{brand.lbsale}}</li><li>SB: {{brand.sbsale}}</li><li>TIN: {{brand.tinsale}}</li></ul>
						</td>
						<td class="hideTotalPrice tip" id="inHouse{{brand.brandNo}}" style="color: #000;">{{brand.inHouseStock}}
						<ul ng-if="brand.productType == 'LIQUOR'"><li>2L: {{brand.l2Stock}} - {{brand.l2perday}} d</li><li>1L: {{brand.l1Stock}} - {{brand.l1perday}} d</li>
						<li>Q: {{brand.qStock}} - {{brand.qperday}} d</li>
						<li>P: {{brand.pStock}} - {{brand.pperday}} d</li><li>N: {{brand.nStock}} - {{brand.nperday}} d</li>
						<li>D: {{brand.dStock}} - {{brand.dperday}} d</li>
						</ul>
						<ul ng-if="brand.productType == 'BEER'">
						<li>LB: {{brand.lbStock}} - {{brand.lbperday}} d</li><li>SB: {{brand.sbStock}} - {{brand.sbperday}} d</li>
						<li>TIN: {{brand.tinStock}} - {{brand.tinperday}} d</li></ul>
						</td>
						<td class="hideTotalPrice" id="commitment{{brand.brandNo}}" style="color: #000;">{{brand.commitment}}</td>
						<td class="hideTotalPrice" id="liftedCase{{brand.brandNo}}" style="color: #000;">{{brand.liftedCase}}</td>
						<td class="hideTotalPrice" id="pendingCase{{brand.brandNo}}" style="" ng-class="{'color-red': brand.colorcode == 2,'color-orange': brand.colorcode == 0,'color-green': brand.colorcode ==1}">{{brand.pending}}</td>
						<td class="" style="color: #000;"><input type="number" style="width: 50px !important;border: solid 1px #e8e8e8; border-radius: 2px;" id="newcase{{brand.brandNo}}"  ng-model="brand.needCase"  value="{{brand.needCase}}"  ng-change="calculateUpdatedNeedCase(brand.needCase,{{brand.brandNo}},{{brand.caseRate}},{{brand.l2}},{{brand.l1}},{{brand.q}},{{brand.p}},{{brand.n}},{{brand.d}},{{brand.sb}},{{brand.lb}},{{brand.tin}},{{brand.l2SpecialMargin}},{{brand.l1SpecialMargin}},{{brand.qSpecialMargin}},{{brand.pSpecialMargin}},{{brand.nSpecialMargin}},{{brand.dSpecialMargin}},{{brand.sbSpecialMargin}},{{brand.lbSpecialMargin}},{{brand.tinSpecialMargin}},{{brand.l2NeedCase}},{{brand.l1NeedCase}},{{brand.qNeedCase}},{{brand.pNeedCase}},{{brand.nNeedCase}},{{brand.dNeedCase}},{{brand.sbNeedCase}},{{brand.lbNeedCase}},{{brand.tinNeedCase}});" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
						<td class="hideTotalPrice" id="investment{{brand.brandNo}}" style="color: #000;">{{brand.totalinvestment}}</td>
						<td id="2l{{brand.brandNo}}" style="color: #000;">{{brand.l2Val}}</td>
						<td id="1l{{brand.brandNo}}" style="color: #000;">{{brand.l1Val}}</td>
						<td id="q{{brand.brandNo}}" style="color: #000;">{{brand.qVal}}</td>
						<td id="p{{brand.brandNo}}" style="color: #000;">{{brand.pVal}}</td>
						<td id="n{{brand.brandNo}}" style="color: #000;">{{brand.nVal}}</td>
						<td id="d{{brand.brandNo}}" style="color: #000;">{{brand.dVal}}</td>
						<td id="lb{{brand.brandNo}}" style="color: #000;">{{brand.lbVal}}</td>
						<td id="sb{{brand.brandNo}}" style="color: #000;">{{brand.sbVal}}</td>
						<td id="tin{{brand.brandNo}}" style="color: #000;">{{brand.tinVal}}</td>
						<td class="" style="display: none;" id="specialMrp{{brand.brandNo}}">{{brand.totalSpecialMrp}}</td>
						
						<td class="" style="display: none;">{{brand.OldneedCase}}</td>
						<td class="" style="display: none;">{{brand.target}}</td>
						<td class="" style="display: none;">{{brand.companyOrder}}</td>
						<td class="" style="display: none;">{{brand.companyColor}}</td>
						<td class="" style="display: none;">{{brand.productType}}</td>
						<td class="" style="display: none;">{{brand.l2}}</td>
						<td class="" style="display: none;">{{brand.l1}}</td>
						<td class="" style="display: none;">{{brand.q}}</td>
						<td class="" style="display: none;">{{brand.p}}</td>
						<td class="" style="display: none;">{{brand.n}}</td>
						<td class="" style="display: none;">{{brand.d}}</td>
						<td class="" style="display: none;">{{brand.sb}}</td>
						<td class="" style="display: none;">{{brand.lb}}</td>
						<td class="" style="display: none;">{{brand.tin}}</td>
						<td class="" style="display: none;">{{brand.x}}</td>
						<td class="" style="display: none;">{{brand.l2SpecialMargin}}</td>
						<td class="" style="display: none;">{{brand.l1SpecialMargin}}</td>
						<td class="" style="display: none;">{{brand.qSpecialMargin}}</td>
						<td class="" style="display: none;">{{brand.pSpecialMargin}}</td>
						<td class="" style="display: none;">{{brand.nSpecialMargin}}</td>
						<td class="" style="display: none;">{{brand.dSpecialMargin}}</td>
						<td class="" style="display: none;">{{brand.sbSpecialMargin}}</td>
						<td class="" style="display: none;">{{brand.lbSpecialMargin}}</td>
						<td class="" style="display: none;">{{brand.tinSpecialMargin}}</td>
						<td class="" style="display: none;">{{brand.xSpecialMargin}}</td>
						<td class="" style="display: none;">{{brand.l2Stock}}</td>
						<td class="" style="display: none;">{{brand.l1Stock}}</td>
						<td class="" style="display: none;">{{brand.qStock}}</td>
						<td class="" style="display: none;">{{brand.pStock}}</td>
						<td class="" style="display: none;">{{brand.nStock}}</td>
						<td class="" style="display: none;">{{brand.dStock}}</td>
						<td class="" style="display: none;">{{brand.sbStock}}</td>
						<td class="" style="display: none;">{{brand.lbStock}}</td>
						<td class="" style="display: none;">{{brand.tinStock}}</td>
						<td class="" style="display: none;">{{brand.xStock}}</td>
						<td class="" style="display: none;">{{brand.l2NeedCase}}</td>
						<td class="" style="display: none;">{{brand.l1NeedCase}}</td>
						<td class="" style="display: none;">{{brand.qNeedCase}}</td>
						<td class="" style="display: none;">{{brand.pNeedCase}}</td>
						<td class="" style="display: none;">{{brand.nNeedCase}}</td>
						<td class="" style="display: none;">{{brand.dNeedCase}}</td>
						<td class="" style="display: none;">{{brand.sbNeedCase}}</td>
						<td class="" style="display: none;">{{brand.lbNeedCase}}</td>
						<td class="" style="display: none;">{{brand.tinNeedCase}}</td>
						<td class="" style="display: none;">{{brand.xNeedCase}}</td>
						<td class="" style="display: none;">{{brand.category}}</td>
						<td class="" style="display: none;">{{brand.company}}</td>
						<td class="" style="display: none;">{{brand.inHouseStock}}</td>
						<td class="" style="color: #000;display: none;">{{brand.lastMonthSold}}</td>
						<td class="" style="display: none;">{{brand.l2sale}}</td>
						<td class="" style="display: none;">{{brand.l1sale}}</td>
						<td class="" style="display: none;">{{brand.qsale}}</td>
						<td class="" style="display: none;">{{brand.psale}}</td>
						<td class="" style="display: none;">{{brand.nsale}}</td>
						<td class="" style="display: none;">{{brand.dsale}}</td>
						<td class="" style="display: none;">{{brand.sbsale}}</td>
						<td class="" style="display: none;">{{brand.lbsale}}</td>
						<td class="" style="display: none;">{{brand.tinsale}}</td>
						<td class="" style="display: none;">{{brand.xsale}}</td>
						<td class="" style="display: none;">{{brand.l2perday}}</td>
						<td class="" style="display: none;">{{brand.l1perday}}</td>
						<td class="" style="display: none;">{{brand.qperday}}</td>
						<td class="" style="display: none;">{{brand.pperday}}</td>
						<td class="" style="display: none;">{{brand.nperday}}</td>
						<td class="" style="display: none;">{{brand.dperday}}</td>
						<td class="" style="display: none;">{{brand.lbperday}}</td>
						<td class="" style="display: none;">{{brand.sbperday}}</td>
						<td class="" style="display: none;">{{brand.xperday}}</td>
						<td class="" style="display: none;">{{brand.tinperday}}</td>
                        <td class="hideTotalPrice" style="color: #000;background: linear-gradient(to left, {{brand.companyColor}} 15%, white 6%);"><span class="glyphicon glyphicon-edit " data-toggle="modal" ng-click="editRow(brand)" style="top: 0px;color: #000;font-size: 11px;cursor: pointer;"></span></td>
						</tr>
					</tbody>
				 </table>
         </div>
            <div class="row" style="margin-bottom: 45px;"><div class="col-sm-5 bodyFontCss"></div>
			   <div class="col-sm-5 bodyFontCss">
			   <input  class=" btn btn-default btn-rounded" type="submit" name="" ng-click="savedIndentEstimate()" value="Save Details" />
			   </div>
			   <div class="col-sm-5 bodyFontCss"></div>
            </div>
          <div class="fixed">
			<div class="row">
			<div class="col-sm-1" style="font-size: 12px;">
				<span>Beer: </span><span id="beerIndent">{{beerIndent}}</span>
				</div>
				<div class="col-sm-1" style="font-size: 12px;">
				<span>Liquor: </span><span id="liquorIndent">{{liquorIndent}}</span>
				</div>
				<div class="col-sm-2" style="font-size: 12px;">
				<span>Total Case: </span><span id="totalliquorIndent">{{beerIndentliquorIndent}}</span>
				</div>
				<div class="col-sm-2" style="font-size: 12px;">
				<span>Indent Value: </span><span id="TotalestimateInvestment">{{totalInvst}}</span>
				</div>
				<div class="col-sm-2" style="font-size: 12px;">
				<span>Mrp round Off: </span><span id="TotalestimateSpecialMargin">{{totalSpecialMargin}}</span>
				</div>
			    <div class="col-sm-2" style="font-size: 12px;">
				<span>TCS Value: </span><span id="tcsValue">{{tcsValue}}</span>
				</div>
				<div class="col-sm-2" style="font-size: 12px;">
				<span>Total Investment: </span><span id="sumOfInvestmentAmt">{{netIndentAmount}}</span>
				</div>
				
			</div>
		</div>
		</div>
<!-- IndentEstimat End -->
<!-- Scheme Start -->
<div id="schemeEstimateDiv" role="tabpanel" class="tab-pane fade fadecss">
 <div class="row" style="margin-bottom: 10px;margin-top: 10px;">
   <div class="col-sm-1 bodyFontCss"><button type="button" class="btn btn-default btn-rounded" ng-click="deleteSchemeRow('schemeEstimate')">DELETE</button></div>
   <div class="col-sm-3 bodyFontCss"></div>
   <div class="col-sm-2 bodyFontCss">
  <label>Filter</label>
    <select ng-model="companySchemefilter" class="form-control" style="display: block !important;" ng-change="totalSchemeInvstAmt()">
		  <option value>ALL Company</option>
	  <option ng-repeat="list in schemeList | unique:'company' " value="{{list.company}}">{{list.company}}</option>
     </select>
  </div>
  <div  class="col-sm-2 bodyFontCss"> <label>Date</label><input type="text" class="form-control" id="schemedatepicker" readonly='true'></div>
  <div class="col-sm-4 bodyFontCss">
  <a style="float: right;" href="downloadSchemeEstimateDiscount">
   <img alt="pdfImg" src="resources/images/pdf.png" style="float: right; margin-right: 30px;cursor: pointer;"></a>
  </div>
 </div>
 <div class="row">
				<table class="saledetailreport" id="schemeEstimate">
					<tr>
						<th></th>
						<th title="Brand Name" style="text-align: left;">Brand Name</th>
						<th>Brand No</th>
						<th title="AVG(Unit Rate)">Case Rate</th>
						<th title="Last 30 days Sold">Last 30 days Sold</th>
						<th title="Current Avaiable Stock">In-House Stock</th>
						<th title="Current Month Target Case">Commitment</th>
						<th title="Sum Of Current Month Lifted Case">Lifted Case</th>
						<th title="(Commitment - Lifted Case)">Pending</th>
						<th class="">Scheme Case</th>
						<th title="(SchemeCase * Case Rate)">Investment</th>
						<th>2L</th>
						<th>1L</th>
						<th>Q</th>
						<th>P</th>
						<th>N</th>
						<th>D</th>
						<th>LB</th>
						<th>SB</th>
						<th>TIN</th>
						<th>Edit</th>
						</tr>
						<tbody id="schemeEstimatebody">
					    <tr id="{{$index}}" ng-repeat="data in schemeList | filter:{company:companySchemefilter,company:companySchemefilter||undefined}:true | orderBy:['companyOrder']">
						<td style="color: #000;background: linear-gradient(to right, {{data.companyColor}} 15%, white 6%);"><INPUT type="checkbox" name="schemechk" value="" style="margin: 0px; padding: 0px;width: 15px;"></td>
						<td>{{data.brandName}}</td>
						<td>{{data.brandNo}}</td>
						<td>{{data.caseRate}}</td>
						<td class="tip">{{data.lastMonthSold}}
						<ul ng-if="data.productType == 'LIQUOR'"><li>2L: {{data.l2}}</li><li>1L: {{data.l1}}</li><li>Q: {{data.q}}</li>
						<li>P: {{data.p}}</li><li>N: {{data.n}}</li><li>D: {{data.d}}</li>
						</ul>
						<ul ng-if="data.productType == 'BEER'">
						<li>LB: {{data.lb}}</li><li>SB: {{data.sb}}</li><li>TIN: {{data.tin}}</li></ul>
						</td>
						<td class="tip">{{data.inHouseStock}}
						<ul ng-if="data.productType == 'LIQUOR'"><li>2L: {{data.l2Stock}} - {{data.l2perday}} d</li><li>1L: {{data.l1Stock}} - {{data.l1perday}} d</li>
						<li>Q: {{data.qstock}} - {{data.qperday}} d</li>
						<li>P: {{data.pstock}} - {{data.pperday}} d</li><li>N: {{data.nstock}} - {{data.nperday}} d</li>
						<li>D: {{data.dstock}} - {{data.dperday}} d</li>
						</ul>
						<ul ng-if="brand.productType == 'BEER'">
						<li>LB: {{data.lbstock}} - {{data.lbperday}} d</li><li>SB: {{data.sbstock}} - {{data.sbperday}} d</li>
						<li>TIN: {{data.tinstock}} - {{data.tinperday}} d</li></ul>
						</td>
						<td>{{data.target}}</td>
						<td>{{data.liftedCase}}</td>
						<td>{{data.pending}}</td>
						<td><input type="number" style="width: 50px !important;border: solid 1px #e8e8e8; border-radius: 2px;"  ng-model="data.noOfCases"  value="{{data.noOfCases}}"  ng-change="splitScheme(data.noOfCases,data);" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
						<td>{{data.otherInvestment}}</td>
						<td>{{data.l2NeedCase}}</td>
						<td>{{data.l1NeedCase}}</td>
						<td>{{data.qNeedCase}}</td>
						<td>{{data.pNeedCase}}</td>
						<td>{{data.nNeedCase}}</td>
						<td>{{data.dNeedCase}}</td>
						<td>{{data.lbNeedCase}}</td>
						<td>{{data.sbNeedCase}}</td>
						<td>{{data.tinNeedCase}}</td>
						<td class="hideTotalPrice" style="color: #000;background: linear-gradient(to left, {{data.companyColor}} 15%, white 6%);"><span class="glyphicon glyphicon-edit " data-toggle="modal" ng-click="editSchemeRow(data)" style="top: 0px;color: #000;font-size: 11px;cursor: pointer;"></span></td>
						</tr>
						</tbody>
				</table>
			</div>
			<div class="row" style="margin-bottom: 45px;"><div class="col-sm-5 bodyFontCss"></div>
			   <div class="col-sm-5 bodyFontCss">
			   <input  class=" btn btn-default btn-rounded" type="submit" name="" ng-click="saveSchemeListData()" value="Save Details" />
			   </div>
			   <div class="col-sm-5 bodyFontCss"></div>
            </div>
			<div class="fixed">
			<div class="row">
			<div class="col-sm-1" style="font-size: 12px;">
				<span>Beer: </span><span>{{beerSchemeIndent}}</span>
				</div>
				<div class="col-sm-1" style="font-size: 12px;">
				<span>Liquor: </span><span>{{liquorSchemeIndent}}</span>
				</div>
				<div class="col-sm-2" style="font-size: 12px;">
				<span>Total Case: </span><span>{{beerSchemeIndentliquorSchemeIndent}}</span>
				</div>
				<div class="col-sm-2" style="font-size: 12px;">
				<span>Indent Value: </span><span>{{totalSchemeInvst}}</span>
				</div>
				<div class="col-sm-2" style="font-size: 12px;">
				<span>Mrp round Off: </span><span>{{totalSchemeSpecialMargin}}</span>
				</div>
			    <div class="col-sm-2" style="font-size: 12px;">
				<span>TCS Value: </span><span>{{tcsSchemeValue}}</span>
				</div>
				<div class="col-sm-2" style="font-size: 12px;">
				<span>Total Investment: </span><span>{{netSchemeIndentAmount}}</span>
				</div>
				
			</div>
		</div>
</div>
			<!-- edit scheme popup open -->
   <div class="modal fade sbi2-popup-styles" id="editSchemeModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title"> Edit Product</h4> 
        </div>
        <div class="modal-body">
        <table style="width:100%;padding: 2px 2px;text-align: left;font-size: 12px;">
		  <tr>
		    <th style="padding: 2px 2px;text-align: left;font-size: 12px;color: #000;">Brand Name</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Case Rate</th> 
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Last 30 days Sold</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">In-House Stock</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Lifted Case</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Scheme Case</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Investment</th>
		  </tr>
		  <tr>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">{{singleObj.brandName}}</td>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">{{singleObj.caseRate}}</td>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">{{singleObj.lastMonthSold}}</td>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">{{singleObj.inHouseStock}}</td>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">{{singleObj.liftedCase}}</td>
		     <td style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">{{singleObj.noOfCases}}</td>
		      <td style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">{{singleObj.otherInvestment}}</td>
		  </tr>
		</table>  
		<table style="width:100%;padding: 2px 2px;text-align: left;font-size: 12px;margin-top: 15px;">
		  <tr>
		    <th style="padding: 2px 2px;text-align: left;font-size: 12px;color: #000;">2L</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">1L</th> 
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Q</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">P</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">N</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">D</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">LB</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">SB</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">TIN</th>
		  </tr>
		  <tr>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss" ng-model="singleObj.l2NeedCase"  value="{{singleObj.l2NeedCase}}" ng-change="editSchemeChnage()"  min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss" ng-model="singleObj.l1NeedCase"  value="{{singleObj.l1NeedCase}}" ng-change="editSchemeChnage()" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss" ng-model="singleObj.qNeedCase"  value="{{singleObj.qNeedCase}}"  ng-change="editSchemeChnage()" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss" ng-model="singleObj.pNeedCase"  value="{{singleObj.pNeedCase}}"  ng-change="editSchemeChnage()" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss" ng-model="singleObj.nNeedCase"  value="{{singleObj.nNeedCase}}" ng-change="editSchemeChnage()" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss" ng-model="singleObj.dNeedCase"  value="{{singleObj.dNeedCase}}" ng-change="editSchemeChnage()" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss"  ng-model="singleObj.lbNeedCase"  value="{{singleObj.lbNeedCase}}" ng-change="editSchemeChnage()" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss"  ng-model="singleObj.sbNeedCase"  value="{{singleObj.sbNeedCase}}" ng-change="editSchemeChnage()" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss" ng-model="singleObj.tinNeedCase"  value="{{singleObj.tinNeedCase}}" ng-change="editSchemeChnage()" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		  </tr>
		</table> 
		<p style="text-align: center;margin-top: 10px;"><input ng-click="updateSchemeRowValue()" class="btn btn-default btn-rounded edit-row" type="button" name="" value="Update"></p>
       </div>
      </div>
    </div>
  </div>
  
  <!-- edit Scheme Popup close-->
<!-- Scheme End -->
</div>
   </div>
   
   <div class="modal fade sbi2-popup-styles" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"> Add Product</h4>
        </div>
        <div class="modal-body">
         <select id="brandNoVal"> </select> <input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getPeroductDetails()" value="GET" />
        <table style="width:100%;padding: 2px 2px;text-align: left;font-size: 12px;">
		  <tr>
		    <th style="padding: 2px 2px;text-align: left;font-size: 12px;color: #000;">Brand Name</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Case Rate</th> 
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Last 30 days Sold</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">In-House Stock</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Lifted Case</th>
		  </tr>
		  <tr>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;">{{singleBrandModelPopup.brandName}}</td>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;">{{singleBrandModelPopup.caseRate}}</td>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;">{{singleBrandModelPopup.lastMonthSold}}</td>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;">{{singleBrandModelPopup.inHouseStock}}</td>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;">{{singleBrandModelPopup.liftedCase}}</td>
		  </tr>
		</table>   
		<label>Indent Case</label><input style="margin: 5px; width: 150px;   height: 30px; border: solid 1px #ccc;" type="number" id="newProductCase"  ng-model="newProductCase"  value="0"  ng-change="newProduct(newProductCase,{{singleBrandModelPopup.l2}},{{singleBrandModelPopup.l1}},{{singleBrandModelPopup.q}},{{singleBrandModelPopup.p}},{{singleBrandModelPopup.n}},{{singleBrandModelPopup.d}},{{singleBrandModelPopup.lb}},{{singleBrandModelPopup.sb}},{{singleBrandModelPopup.tin}},{{singleBrandModelPopup.l2SpecialMargin}},{{singleBrandModelPopup.l1SpecialMargin}},{{singleBrandModelPopup.qspecialMargin}},{{singleBrandModelPopup.pspecialMargin}},{{singleBrandModelPopup.nspecialMargin}},{{singleBrandModelPopup.dspecialMargin}},{{singleBrandModelPopup.sbspecialMargin}},{{singleBrandModelPopup.lbspecialMargin}},{{singleBrandModelPopup.tinspecialMargin}},{{totalcasepupop}})" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57"/>
		<table style="width:100%;padding: 2px 2px;text-align: left;font-size: 12px;">
		  <tr>
		    <th style="padding: 2px 2px;text-align: left;font-size: 12px;color: #000;">2L</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">1L</th> 
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Q</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">P</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">N</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">D</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">LB</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">SB</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">TIN</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;display: none;">totalMrpRoundOffWithSpecialMrp</th>
		  </tr>
		  <tr>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;">{{l2DistributeAgain}}</td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;">{{l1DistributeAgain}}</td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;">{{QDistributeAgain}}</td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;">{{PDistributeAgain}}</td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;">{{NDistributeAgain}}</td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;">{{DDistributeAgain}}</td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;">{{SBDistributeAgain}}</td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;">{{LBDistributeAgain}}</td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;">{{TINDistributeAgain}}</td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;display: none;">{{totalMrpRoundOffWithSpecialMrp}}</td>
		  </tr>
		</table>      
        <p style="text-align: center;margin-top: 10px;" id="addBtnForNewProduct"><input class="btn btn-default btn-rounded add-row" type="button" name="" value="SET" ng-click="addFun()"></p>
        </div>
      </div>
    </div>
  </div>
  
  <!-- edit popup open -->
   <div class="modal fade sbi2-popup-styles" id="editModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
           <h4 class="modal-title"> Edit Product</h4> 
        </div>
        <div class="modal-body">
        <table style="width:100%;padding: 2px 2px;text-align: left;font-size: 12px;">
		  <tr>
		    <th style="padding: 2px 2px;text-align: left;font-size: 12px;color: #000;">Brand Name</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Case Rate</th> 
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Last 30 days Sold</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">In-House Stock</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Lifted Case</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Indent Case</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Investment</th>
		  </tr>
		  <tr>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">{{editRowModelPopupItem.brandName}}</td>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">{{editRowModelPopupItem.caseRate}}</td>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">{{editRowModelPopupItem.lastMonthSold}}</td>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">{{editRowModelPopupItem.inHouseStock}}</td>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">{{editRowModelPopupItem.liftedCase}}</td>
		     <td style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;" id="editPopupNeedCase"></td>
		      <td style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;" id="editPopupInvestment"></td>
		  </tr>
		</table>  
		<table style="width:100%;padding: 2px 2px;text-align: left;font-size: 12px;margin-top: 15px;">
		  <tr>
		    <th style="padding: 2px 2px;text-align: left;font-size: 12px;color: #000;">2L</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">1L</th> 
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">Q</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">P</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">N</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">D</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">LB</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">SB</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;color: #000;">TIN</th>
		    <th style="padding: 2px 2px;text-align: center;font-size: 12px;display: none;">totalMrpRoundOffWithSpecialMrp</th>
		  </tr>
		  <tr>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss"  ng-model="l2Popup" id="2lPopup"  value="" ng-change="editChnage(l2,{{editRowModelPopupItem.caseRate}},{{editRowModelPopupItem.brandNo}},{{editRowModelPopupItem.l2SpecialMargin}},{{editRowModelPopupItem.l1SpecialMargin}},{{editRowModelPopupItem.qSpecialMargin}},{{editRowModelPopupItem.pSpecialMargin}},{{editRowModelPopupItem.nSpecialMargin}},{{editRowModelPopupItem.dSpecialMargin}},{{editRowModelPopupItem.sbSpecialMargin}},{{editRowModelPopupItem.lbSpecialMargin}},{{editRowModelPopupItem.tinSpecialMargin}})" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss"  ng-model="l1Popup" id="1lPopup"  value="" ng-change="editChnage(l1,{{editRowModelPopupItem.caseRate}},{{editRowModelPopupItem.brandNo}},{{editRowModelPopupItem.l2SpecialMargin}},{{editRowModelPopupItem.l1SpecialMargin}},{{editRowModelPopupItem.qSpecialMargin}},{{editRowModelPopupItem.pSpecialMargin}},{{editRowModelPopupItem.nSpecialMargin}},{{editRowModelPopupItem.dSpecialMargin}},{{editRowModelPopupItem.sbSpecialMargin}},{{editRowModelPopupItem.lbSpecialMargin}},{{editRowModelPopupItem.tinSpecialMargin}})" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss" ng-model="qPopup" id="qPopup" value="" ng-change="editChnage(q,{{editRowModelPopupItem.caseRate}},{{editRowModelPopupItem.brandNo}},{{editRowModelPopupItem.l2SpecialMargin}},{{editRowModelPopupItem.l1SpecialMargin}},{{editRowModelPopupItem.qSpecialMargin}},{{editRowModelPopupItem.pSpecialMargin}},{{editRowModelPopupItem.nSpecialMargin}},{{editRowModelPopupItem.dSpecialMargin}},{{editRowModelPopupItem.sbSpecialMargin}},{{editRowModelPopupItem.lbSpecialMargin}},{{editRowModelPopupItem.tinSpecialMargin}})" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss" ng-model="pPopup" id="pPopup" value="" ng-change="editChnage(p,{{editRowModelPopupItem.caseRate}},{{editRowModelPopupItem.brandNo}},{{editRowModelPopupItem.l2SpecialMargin}},{{editRowModelPopupItem.l1SpecialMargin}},{{editRowModelPopupItem.qSpecialMargin}},{{editRowModelPopupItem.pSpecialMargin}},{{editRowModelPopupItem.nSpecialMargin}},{{editRowModelPopupItem.dSpecialMargin}},{{editRowModelPopupItem.sbSpecialMargin}},{{editRowModelPopupItem.lbSpecialMargin}},{{editRowModelPopupItem.tinSpecialMargin}})" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss" ng-model="nPopup" id="nPopup" value="" ng-change="editChnage(n,{{editRowModelPopupItem.caseRate}},{{editRowModelPopupItem.brandNo}},{{editRowModelPopupItem.l2SpecialMargin}},{{editRowModelPopupItem.l1SpecialMargin}},{{editRowModelPopupItem.qSpecialMargin}},{{editRowModelPopupItem.pSpecialMargin}},{{editRowModelPopupItem.nSpecialMargin}},{{editRowModelPopupItem.dSpecialMargin}},{{editRowModelPopupItem.sbSpecialMargin}},{{editRowModelPopupItem.lbSpecialMargin}},{{editRowModelPopupItem.tinSpecialMargin}})" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss" ng-model="dPopup" id="dPopup" value="" ng-change="editChnage(d,{{editRowModelPopupItem.caseRate}},{{editRowModelPopupItem.brandNo}},{{editRowModelPopupItem.l2SpecialMargin}},{{editRowModelPopupItem.l1SpecialMargin}},{{editRowModelPopupItem.qSpecialMargin}},{{editRowModelPopupItem.pSpecialMargin}},{{editRowModelPopupItem.nSpecialMargin}},{{editRowModelPopupItem.dSpecialMargin}},{{editRowModelPopupItem.sbSpecialMargin}},{{editRowModelPopupItem.lbSpecialMargin}},{{editRowModelPopupItem.tinSpecialMargin}})" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss" ng-model="lbPopup" id="lbPopup" value="" ng-change="editChnage(lb,{{editRowModelPopupItem.caseRate}},{{editRowModelPopupItem.brandNo}},{{editRowModelPopupItem.l2SpecialMargin}},{{editRowModelPopupItem.l1SpecialMargin}},{{editRowModelPopupItem.qSpecialMargin}},{{editRowModelPopupItem.pSpecialMargin}},{{editRowModelPopupItem.nSpecialMargin}},{{editRowModelPopupItem.dSpecialMargin}},{{editRowModelPopupItem.sbSpecialMargin}},{{editRowModelPopupItem.lbSpecialMargin}},{{editRowModelPopupItem.tinSpecialMargin}})" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss" ng-model="sbPopup" id="sbPopup" value="" ng-change="editChnage(sb,{{editRowModelPopupItem.caseRate}},{{editRowModelPopupItem.brandNo}},{{editRowModelPopupItem.l2SpecialMargin}},{{editRowModelPopupItem.l1SpecialMargin}},{{editRowModelPopupItem.qSpecialMargin}},{{editRowModelPopupItem.pSpecialMargin}},{{editRowModelPopupItem.nSpecialMargin}},{{editRowModelPopupItem.dSpecialMargin}},{{editRowModelPopupItem.sbSpecialMargin}},{{editRowModelPopupItem.lbSpecialMargin}},{{editRowModelPopupItem.tinSpecialMargin}})" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		   <td style="padding: 2px 2px;text-align: center;font-size: 12px;"><input type="number" class="inputFiledCss" ng-model="tinPopup" id="tinPopup" value="" ng-change="editChnage(tin,{{editRowModelPopupItem.caseRate}},{{editRowModelPopupItem.brandNo}},{{editRowModelPopupItem.l2SpecialMargin}},{{editRowModelPopupItem.l1SpecialMargin}},{{editRowModelPopupItem.qSpecialMargin}},{{editRowModelPopupItem.pSpecialMargin}},{{editRowModelPopupItem.nSpecialMargin}},{{editRowModelPopupItem.dSpecialMargin}},{{editRowModelPopupItem.sbSpecialMargin}},{{editRowModelPopupItem.lbSpecialMargin}},{{editRowModelPopupItem.tinSpecialMargin}})" min="0" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" /></td>
		    <td style="padding: 2px 2px;text-align: center;font-size: 12px;display: none;" id="popupSpecialMrp"></td>
		  </tr>
		</table> 
		<p style="text-align: center;margin-top: 10px;" id="addBtnForEditProduct"><input ng-click="updateRowValue(editRowModelPopupItem.brandNo)" class="btn btn-default btn-rounded edit-row" type="button" name="" value="Update"></p>
       </div>
      </div>
    </div>
  </div>
  
  <!-- edit Popup close-->
  
</body>
</html>