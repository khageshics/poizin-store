<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<%-- <spring:url value="/resources/css/3.3.7.bootstrap.min.css" var="bootstrap1mincss"/>
   <link rel="stylesheet" href="${bootstrap1mincss}"> --%>
   
 <jsp:include page="/WEB-INF/jsp/header.jsp" /> 
  
 <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
 <script src="${jqueryuijs}"></script> 
<!--  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 06-->
<spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
   
<spring:url value="/resources/css/select2.min.css" var="select2mincss"/>
   <link rel="stylesheet" href="${select2mincss}">
	<spring:url value="/resources/javascript/thirdpartyjs/select2.min.js" var="select2minjs"/>
  <script src="${select2minjs}"></script>
 <spring:url value="/resources/javascript/credit.js" var="creditjs"/>
 <script src="${creditjs}"></script>
 <spring:url value="/resources/javascript/common.js" var="commonjs"/>
 <script src="${commonjs}"></script>
 <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
 <link rel="stylesheet" href="${newCommoncss}">
 <style type="text/css">
 input {
 border: solid 1px #000;
 }
 
 table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
  font-size: 12px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
table th {
    font-size: 12px;
    font-weight: 600;
}
table td {
    font-size: 12px;
    font-weight: 400;
}

.fadecss:not(.show) {
    opacity: 1 !important;
}
.stockLift span {
    text-align: center;
    box-sizing: border-box;
    background-color: #243a51;
    overflow: hidden;
    text-overflow: ellipsis;
    display: inline-block;
    white-space: nowrap;
    font-size: 12px;
    text-align: center;
    padding: 8px;
    color: #fff;
}
.stockLiftList span {
    text-align: center;
    box-sizing: border-box;
    overflow: hidden;
    text-overflow: ellipsis;
    display: inline-block;
    white-space: nowrap;
    font-size: 12px;
    text-align: center;
    padding: 8px;
}
 details summary::-webkit-details-marker { display:none; }
 .customtable thead>tr>th{
    background-color: #337ab7;
    color: #fff;
    text-align: center;
 }
 .customtable tbody>tr>td{
    background-color: #efe7e7;
    text-align: center;
 }
 [type=checkbox]:checked, [type=checkbox]:not(:checked) {
    position: inherit;
    opacity: 1;
    pointer-events: inherit;
}
.row{
margin-right: 0px;
margin-left: 0px;
}
 </style>
 
</head>
<body ng-app="myApp" ng-controller="myCtrl" ng-init="init()">
<div class="" style="padding-top: 85px;">
<!-- Nav tabs -->
<ul id="tabs" class="nav nav-tabs" role="tablist">
  <li role="presentation" class="active"><a href="#creditTab" role="tab" data-toggle="tab" style="font-size: small;">Credit</a></li>
  <li role="presentation"><a href="#newCreditTab" role="tab" data-toggle="tab" style="font-size: small;">New Credit</a></li>
</ul>
<!-- List of credit start -->
<div class="tab-content">
  <div role="tabpanel" class="tab-pane fadecss fade in active" id="creditTab">
  <div class="row">
   <div class="col-sm-12 bodyFontCss stockLift">
    <span class="col-sm-3" style="text-align: center;border-top-left-radius: 3px;">Name</span>
    <span class="col-sm-2" style="text-align: center;">Company</span>
    <span class="col-sm-2" style="text-align: center;">Amount</span>
    <span class="col-sm-2" style="text-align: center;">Date</span>
    <span class="col-sm-2" style="text-align: center;">Received Date</span>
    <span class="col-sm-1" style="text-align: center;border-top-right-radius: 3px;">Update</span>
    </div>
    <div class="datalist col-sm-12">
     <h4 ng-if="creditList.length <=0" style="text-align: center;padding: 8px;">No data available!</h4>
    <details ng-repeat="list in creditList">
   <summary>
    <div class="stockLiftList"  style="height:36px;border-bottom: 1px solid #e0e0e0;">
    <span class="col-sm-3" style="text-align: center;">{{list.name}}</span>
    <span class="col-sm-2" style="text-align: center;">{{list.companyName}}</span>
    <span class="col-sm-2" style="text-align: center;">{{list.totalAmount}}</span>
    <span class="col-sm-2" style="text-align: center;">{{list.date}}</span>
    <span class="col-sm-2" style="text-align: center;">{{list.receivedDate}}</span>
    <span class="col-sm-1" style="text-align: center;">
    <input type="button" ng-click="clearOrUnclear(list.creditId,list.totalAmount)" value="Update" class="btn btn-default btn-rounded" style="padding: 2px 5px 2px 5px;margin: 0px;"></span>
    </div>
    </summary>
    <table class="table table-css customtable">
    <thead>
      <tr>
      <th>Product Name</th>
      <th>Case/Bottles</th>
      <th>Price</th>
      <th>Total Price</th>
      <th>Discount / Bottle</th>
      <th>Total Discount</th>
      <th>Total Amount</th>
      </tr>
      </thead>
      <tr ng-repeat="brand in list.creditChildBeanList">
        <td>{{brand.productName}}</td>
	    <td>{{brand.cases}} / {{brand.bottles}}</td>
	    <td>{{brand.price}}</td>
	    <td>{{brand.totalPrice}}</td>
	    <td>{{brand.discount}}</td>
	    <td>{{brand.totalDiscount}}</td>
	    <td>{{brand.afterIncludingDiscount}}</td>
	   </tr>
	   </table>
    </details>
    </div>
    
  </div>
</div>
  
  
  <!-- New credit start -->
  <div role="tabpanel" class="tab-pane fadecss fade" id="newCreditTab">
  <div class="row">
	  <div class="col-sm-3 bodyFontCss"></div>
	<div class="col-sm-2"><label>Name</label><input type="text" class=" form-control" ng-model="name" /></div>
	 <div class="col-sm-2 bodyFontCss">
	 <label>Date:</label>
	      <input type="text" class=" form-control" id="datepicker" readonly='true'>
	 </div>
	 <div class="col-sm-2 bodyFontCss">
	   <label>Select Company</label>
		<select name="company" ng-model="company" required="required" class="form-control">
			<option value="" rval="">Select Company</option>
			<option ng-repeat="list in companyList" value="{{list.id}}">{{list.name}}</option>
		</select>
	 </div>
	 <div class="col-sm-2 bodyFontCss customizedButton">
	   <input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getResults()" value="Get Data" />
	   </div>
	 <div class="col-sm-1 bodyFontCss "></div>  
	</div>
	 <div class="row" ng-show="listedCredit" style="margin-bottom: 15px;">
	 <div class="col-sm-1"></div>
	 <div class="col-sm-2"><label>Select Product</label><br><select id="brand"></select></div>
	 <div class="col-sm-2"><label>Case</label><input type="number" class=" form-control" ng-model="cases" ng-change="calculateTotalPrice(cases,bottles);"/></div>
	 <div class="col-sm-2"><label>Bottles</label><input type="number" class=" form-control" ng-model="bottles" ng-change="calculateTotalPrice(cases,bottles);"/></div>
	 <div class="col-sm-1"><label>Bottle Price</label><input type="number" class=" form-control" ng-model="price" readonly/></div>
	 <div class="col-sm-1"><label>Total Price</label><input type="number" class=" form-control" ng-model="totalPrice" readonly/></div>
	 <div class="col-sm-2"><label>Discount / Bottle</label><input type="number" class=" form-control" ng-model="discount" /></div>
	 <div class="col-sm-1 customizedButton"><input type="submit" class="btn btn-default btn-rounded" ng-click="addCredit()" value="Add" /></div>
	 </div>
	
	 <div class="container" ng-show="listedCredit">
	 <div class="row">
	 <div class="col-sm-12">
	 <table id="creditTab">
	 <thead>
	       <tr>
	           <th>Product</th>
	           <th>Name</th>
	           <th>Case</th>
	           <th>Bottles</th>
	           <th>Bottle Price</th>
	           <th>Total Price</th>
	           <th>Discount / Bottle</th>
	           <th>Total Discount</th>
	           <th>Total Amount</th>
	           <th></th>
	        </tr>
	        </thead>
	        <tbody ng-repeat="m in creditCustomers">
	        <tr>
	          <td>{{m.brandname}}</td>
	          <td>{{m.name}}</td>
	          <td>{{m.cases}}</td>
	          <td>{{m.bottles}}</td>
	          <td>{{m.price}}</td>
	          <td>{{m.totalPrice}}</td>
	          <td>{{m.discount}}</td>
	          <td>{{m.totaldiscount}}</td>
	          <td>{{m.afterIncludeDiscount}}</td>
	          <td style="display: none;">{{m.brandNoPackQty}}</td>
	          <td><input type="button" class=" form-control" ng-click="Remove($index)" value="Remove" /></td>
	        </tr>
	        </tbody>
	    </table>
	    <div style="text-align: center;margin-top: 10px;">
	    <input type="submit" class="btn btn-default btn-rounded" ng-click="PostCredit()" value="Submit" />
	    <span style="float: right;font-size: 12px;font-weight: 600;">Grand Total: {{sumOfAmount}}</span>
	    </div>
	    </div>
	 </div>
	 </div>
  </div>
  
</div>
</div>
<!-- Update credit amount popup start -->
<!-- The Modal -->
  <div class="modal fade" id="addPaymentModel">
    <div class="modal-dialog">
      <div class="modal-content">
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Pay</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
        <form  ng-submit="postPaymentData()" method="POST">
            <input type="hidden" class=" form-control" name="paymentCreditId" ng-model="paymentCreditId" required>
		    Date: <input type="text" class=" form-control" name="paymentDate" ng-model="paymentDate" id="paymentdatepicker" required autocomplete="off"><br>
		    Amount: <input type="number" class=" form-control" name="paymentAmount" ng-model="paymentAmount" required readonly><br>
		  <select class=" form-control" name="paymentmode" ng-model="paymentmode" required>
		      <option value="">Select Payment Option</option>
			  <option value="Cheque">Cheque</option>
			  <option value="Cash">Cash</option>
			</select>
			Comment:
			<textarea rows="2" class=" form-control" required name="paymentcomment" ng-model="paymentcomment"></textarea><br>
			
		  <input type="submit" class=" form-control" value="Submit">
		</form>
        </div>
      </div>
    </div>
  </div>
<!-- Update credit amount popup end -->
</body>
</html>