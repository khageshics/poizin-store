<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<%-- <spring:url value="/resources/css/3.3.7.bootstrap.min.css" var="bootstrap1mincss"/>
   <link rel="stylesheet" href="${bootstrap1mincss}">
    --%>
<jsp:include page="/WEB-INF/jsp/header.jsp" />  
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script> 
<!--   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
   
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script>
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
   <spring:url value="/resources/javascript/fusioncharts/fusioncharts.js" var="fusionjs" />
<script src="${fusionjs}"></script>
<spring:url value="/resources/javascript/fusioncharts/themes/fusioncharts.theme.fint.js" var="fusionthemejs" />
<script src="${fusionthemejs}"></script>
  <spring:url value="/resources/javascript/inHouseStockValue.js" var="inHouseStockValuejs"/>
  <script src="${inHouseStockValuejs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}"> 
   <spring:url value="/resources/css/saleReport.css" var="saleReportcss"/>
   <link rel="stylesheet" href="${saleReportcss}">
 <style>
.container{
max-width: 100%;
}
summary{
display : block;
}
.shiva :hover{
background-color: #ccc;
}
label{
color: #fff !important;
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 15px;
}
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<div class="row" style="padding-top: 100px;">
 <div class="col-sm-3 bodyFontCss" style="padding: 0px; !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Reports</li>
  <li>In-House Stock</li>
</ul>
  </div>
  <div class="col-sm-1 bodyFontCss"></div>
   <div class="col-sm-2 bodyFontCss">
   <label>Date</label><input type="text" class="form-control" id="startDate" readonly='true'>
   </div>
   <div class="col-sm-1 bodyFontCss customizedButton">
   <input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getResults('startDate')" value="Get Data" />
   </div>
  <div class="col-sm-4 bodyFontCss"></div>
  <div class="col-sm-1 bodyFontCss">
  <span><i class="fa fa-filter filter-icon" style="color: #243a51!important;" data-toggle="modal" data-target="#saleFilterModal" aria-hidden="true"></i></span>
  </div>
  </div>
 <div class="container" style="margin-top: 20px;">
  <div class="row" style="margin-bottom: 20px;">
  <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
    <div class="col-sm-12 bodyFontCss">
      <div class="stockLift ">
		<span class="col-xs-2" style="text-align: left;border-top-left-radius: 3px;" data-toggle="tooltip" data-placement="bottom" title="Brand Name" ng-click="sort('brandname','brandname')">Brand Name
		<label class="glyphicon sort-icon" ng-show="sortKey=='brandname,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
		</span>
		<span class="col-xs-1" data-toggle="tooltip" data-placement="bottom"  title="Brand No" ng-click="sort('brandNo','brandname')">Brand No
		<label class="glyphicon sort-icon" ng-show="sortKey=='brandNo,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
		</span>
		<span class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Category" ng-click="sort('categoryOrder','brandname')">Category
		<label class="glyphicon sort-icon" ng-show="sortKey=='categoryOrder,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
		</span>
		<span class="col-xs-2" data-toggle="tooltip" data-placement="bottom"  data-trigger="hover" title="Company" ng-click="sort('companyOrder','brandname')">Company
		<label class="glyphicon sort-icon" ng-show="sortKey=='companyOrder,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
		</span>
		<span class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="In-House Stock" ng-click="sort('closing','brandname')">In-House Stock
		<label class="glyphicon sort-icon" ng-show="sortKey=='closing,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
		</span>
		<!-- <span class="col-xs-1" style="" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" title="In-House Stock Price" ng-click="sort('totalcost','brandname')">Stock Price
		<label class="glyphicon sort-icon" ng-show="sortKey=='totalcost,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
		</span> -->
		<span class="col-xs-2" style="" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" title="Stock Price (14.5%)" ng-click="sort('exactInvoiceValue','brandname')">Stock Price
		<label class="glyphicon sort-icon" ng-show="sortKey=='totalcost,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
		</span>
		<span class="col-xs-2" style="border-top-right-radius: 3px;" data-toggle="tooltip" data-placement="bottom" title="Brand Name" ng-click="sort('totalSaleAmount','brandname')">MRP Price
		<label class="glyphicon sort-icon" ng-show="sortKey=='totalSaleAmount,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
		</span>
	</div>
	<div class="datalist onlySaleItmes">
		<details ng-repeat="list in alldetails = (categories | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined}:true) | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined}:true | filter: greaterThan('totalcost', 0) | orderBy:sortKey:reverse">
		<summary>
			<div class="stockLiftList shiva" style="height:36px;border-right: 5px solid {{list.categoryColor}};border-bottom: 1px solid #e0e0e0;border-left:  5px solid {{list.bgcolor}};">
			<span class="col-xs-2" ng-if="list.redAlert" style="text-align: left;" title="{{list.brandname}}">{{list.brandname}} <i class="fa fa-warning" style="color:red"></i></span>
			<span class="col-xs-2" ng-if="!list.redAlert" style="text-align: left;" title="{{list.brandname}}">{{list.brandname}}</span>
			<span class="col-xs-1" title="{{list.brandNo}}">{{list.brandNo}}</span>
			<span class="col-xs-2" title="{{list.category}}">{{list.category}}</span>
			<span class="col-xs-2" title="{{list.company}}">{{list.company}}</span>
		    <span class="col-xs-1" ng-class="{'color-red': list.firstFontColor <= 1}" title="{{list.closing | INR}}">{{list.closing | INR}}</span>
			<!-- <span class="col-xs-1" title="{{list.totalcost | INR}}">{{list.totalcost | INR}}</span> -->
			<span class="col-xs-2" title="{{list.exactInvoiceValue | INR}}">{{list.exactInvoiceValue | INR}}</span>
			<span class="col-xs-2" title="{{list.totalSaleAmount | INR}}">{{list.totalSaleAmount | INR}}</span>
			</div>
		</summary>
			<table class=" table table-fixed">
				<thead><tr>
					<th class="col-xs-3" title="Quantity">Quantity</th>
					<th class="col-xs-3" title="No. Of Cases/Bottles">No. Of Cases/Bottles</th>
					<!-- <th class="col-xs-2" title="Total Cost">Invoice Amount</th> -->
					<th class="col-xs-3" title="Total Cost">Invoice Amount</th>
					<th class="col-xs-2" title="Total Cost">Sale Amount</th>
					<th class="col-xs-1" title="No. of days stock exists">Days</th>
				</tr></thead>
					<tr ng-repeat="brand in list.Brands | filter: greaterThan('totalPrice', 0)  | orderBy:['category','company']">
					<td ng-if="brand.days <= 30" class="col-xs-3" title="MRP {{brand.shopRate}}">{{brand.quantity}}</td>
					<td ng-if="brand.days > 30" class="col-xs-3" title="MRP {{brand.shopRate}}">{{brand.quantity}} <i class="fa fa-warning" style="color:red"></i></td>
					<td class="col-xs-3" ng-class="{'color-red': brand.days <= 1,'color-orange': brand.days > 1 && brand.days <= 3,'color-green': brand.days >3}" title="{{brand.case}}/{{brand.bottle}}">{{brand.case}}/{{brand.bottle}}</td>
					<!-- <td class="col-xs-3" title="{{brand.totalPrice | INR}}">{{brand.totalPrice | INR}}</td> -->
					<td class="col-xs-3" title="{{brand.exactInvoiceValue | INR}}">{{brand.exactInvoiceValue | INR}}</td>
					<td class="col-xs-2" title="{{brand.saleAmount | INR}}">{{brand.saleAmount | INR}}</td>
					<td class="col-xs-1"  title="{{brand.days}}">{{brand.days}}</td>
				</tr>
		</table>
	 </details>
    </div>
	<div class="datalist totalSaleItmes">
	   <details ng-repeat="list in alldetails = (categories | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined,}:true) | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined}:true | filter: equalsToZero('flag') | orderBy:sortKey:reverse">
		 <summary>
		 <div class="stockLiftList shiva" style="height:36px;border-right: 5px solid {{list.categoryColor}};border-bottom: 1px solid #e0e0e0;border-left:  5px solid {{list.bgcolor}};">
		    <span class="col-xs-2" style="text-align: left;" title="{{list.brandname}}">{{list.brandname}}</span>
			<span class="col-xs-1" title="{{list.brandNo}}">{{list.brandNo}}</span>
			<span class="col-xs-2" title="{{list.category}}">{{list.category}}</span>
			<span class="col-xs-2" title="{{list.company}}">{{list.company}}</span>
			<span class="col-xs-1" title="0">0</span>
			<!-- <span class="col-xs-1" title="0">0</span> -->
			<span class="col-xs-2" title="0">0</span>
			<span class="col-xs-2" title="0">0</span>
		</div>
		</summary>
			<table class=" table table-fixed">
				<thead><tr>
				<th class="col-xs-3" title="Quantity">Quantity</th>
				<th class="col-xs-3" title="No. Of Cases/Bottles">No. Of Cases/Bottles</th>
				<!-- <th class="col-xs-2" title="Total Cost">Invoice Amount</th> -->
				<th class="col-xs-3" title="Total Cost">Invoice Amount</th>
				<th class="col-xs-3" title="Total Cost">Sale Amount</th>
				</tr></thead>
				<tr ng-repeat="brand in list.Brands | filter: equalsTo('totalPrice', 0)  | orderBy:['category','company']">
				<td class="col-xs-3" title="MRP {{brand.shopRate}}">{{brand.quantity}}</td>
				<td class="col-xs-3" title="{{brand.case}}/{{brand.bottle}}">{{brand.case}}/{{brand.bottle}}</td>
				<!-- <td class="col-xs-2" title="{{brand.totalPrice | INR}}">{{brand.totalPrice | INR}}</td> -->
				<td class="col-xs-3" title="{{brand.exactInvoiceValue | INR}}">{{brand.exactInvoiceValue | INR}}</td>
				<td class="col-xs-3" title="{{brand.saleAmount | INR}}">{{brand.saleAmount | INR}}</td>
				</tr>
		   </table>
	</details>
  </div>
    </div>
   </div>
  <div class="fixed">
			<div class="row">
				<div class="col-sm-4">
					<table>
					    <tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Available Stock : </th>
							<td style="float: right;">{{alldetails | totalAvailableStock | INR}}</td>
						</tr>
					</table>


				</div>
				<div class="col-sm-4">
					<table style="float: center;">
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Stock Amount : </th>
							<td style="float: right;">{{alldetails | totalAmount | INR}}</td>
						</tr>
					</table>
				</div>
				<div class="col-sm-4" style="">
				<table style="float: right;">
				        <tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Mrp Amount : </th>
							<td style="float: right;">{{alldetails | totalMrpAmount | INR}}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
 </div>
  <!-- Modal -->
<div class="modal fade right" id="saleFilterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-side modal-top-right" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title w-100" id="exampleModalLabel"><i class="fa fa-filter" style="color: #243a51!important;font-size: 22px;"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <span aria-hidden="true" style="font-size: 25px;">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
	   <select ng-model="modelfilter" class="form-control filterDropdownCss"  name="sort" style="float:left;">
		  <option value>ALL Category</option> 
		  <option ng-repeat="list in categories | unique:'category'" value="{{list.category}}">{{list.category}}</option>
	    </select> 
	   <select ng-model="companyfilter" class="form-control filterDropdownCss">
		  <option value>ALL Company</option>
		  <option ng-repeat="list in categories | unique:'company' | filter:{category:modelfilter}:true | orderBy:['companyOrder']" value="{{list.company}}">{{list.company}}</option>
	   </select>
	   <div style="height: 40px;">
	    <div style="float: right;padding: 12px;">
	    <label class="filterLabelFontSize">Stock Items</label>
	    </div>
	    <div style="float: right;">
	    <label class="switch ">
         <input type="checkbox" id="includeZeroSaleOrNot" checked>
         <span class="slider round" style="background-color: red;" ng-click="includeZeroSaleOrNot()"></span>
        </label>
        </div>
        <div style="float: right;padding: 12px;">
        <label class="filterLabelFontSize">No Stock Items</label>
	   </div>
		</div>
	 </div>
      <div class="modal-footer">
        <!-- <button type="button" style="background: #1a6398!important;" class="btn btn-primary btn-rounded" data-dismiss="modal">Apply Filters</button> -->
      </div>
    </div>
  </div>
</div>
</body>
</html>