<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<jsp:include page="/WEB-INF/jsp/header.jsp" /> 
  
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
 <!--  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 06-->
 <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
 
  <spring:url value="/resources/css/select2.min.css" var="select2mincss"/>
   <link rel="stylesheet" href="${select2mincss}">
	<spring:url value="/resources/javascript/thirdpartyjs/select2.min.js" var="select2minjs"/>
  <script src="${select2minjs}"></script>	
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script>
  <spring:url value="/resources/javascript/updateSale.js" var="updateSalejs"/>
  <script src="${updateSalejs}"></script>
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
  <link rel="stylesheet" href="${newCommoncss}">
  <style type="text/css">
  .select2-container {
    width: 100% !important;
}
#saledetails{
width: 100% !important;
}
#saledetails tr th, #saledetails tr td
{
  background-color: #062351;
  color: #FFF;
  padding: 7px 4px;
  text-align: center;
  font-size: 12px;
  }
  
#saledetails tr td
{ background: #E5E5DB;
  color: #47433F;
  border-top: 1px solid #FFF;
  font-size: 12px;
  }
  </style>
</head>
<body>
 <div class="row" style="padding-top: 100px;">
  <div class="col-sm-3 bodyFontCss">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Sale</li>
  <li>Edit Sale</li>
</ul>
  </div>
  <div class="col-sm-2 bodyFontCss">
     <input type="text" placeholder="Select Date" class="form-control" id="startDate" name="startDate" readonly="readonly">
  </div>
  <div class="col-sm-4 bodyFontCss">
     <select id="brand"></select>
  </div>
  <div class="col-sm-2 bodyFontCss">
     <input class="btn btn-default btn-rounded" type="submit" name="" onclick="getExistingDetails()" value="GET DATA" />
  </div>
  <div class="col-sm-1 bodyFontCss"></div>
 </div>
 <div class="container" style="margin-bottom: 20px;">
 <div class="row" style="margin-bottom: 20px;">
 <div class="col-sm-12 bodyFontCss">
   <form action="" id="saleForm" method="post">
       <table id="saledetails" class="saledetailreport"></table>
    <p style="text-align: center;"><input id="updatesale" class="btn btn-default btn-rounded" onclick="saveOnlySaleItems();"  type="button" name="" value="SAVE DETAILS" /></p>
    </form>
    </div>
 </div>
 <div class="row" style="margin-bottom: 20px;">
  <div class="col-sm-5 bodyFontCss shadowBoxCss" >
    <label style="font-size: 15px;">Card/Cash Sale</label>
    <form id="cardsaleForm" method="post" >
         <div id="cardSaleFormId"></div>
         </form>
  </div>
  <div class="col-sm-2" ></div>
  <div class="col-sm-5 bodyFontCss shadowBoxCss" >
    <label style="font-size: 15px;">Expense</label>
    <form id="expenseForm" method="post" onsubmit="return updateExpenseItems(this);">
         <div id="UpdateExpensesFormId"></div>
         <input class=" updateexpense btn btn-default btn-rounded" type="submit" name="" value="SUBMIT" />
         </form>
  </div>
 </div>
 </div>
</body>
</html>