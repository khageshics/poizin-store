<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html lang="en">
<head>
  <title>www.poizin.com</title>
   <jsp:include page="/WEB-INF/jsp/header.jsp" /> 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
     <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script> 
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
 
 <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <spring:url value="/resources/javascript/thirdpartyjs/jquery-confirm.min.js" var="jqueryconfirmminjs"/>
  <script src="${jqueryconfirmminjs}"></script>
  
     <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
   <spring:url value="/resources/javascript/newBrand.js" var="newBrandjs"/>
  <script src="${newBrandjs}"></script> 
   <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script>
 <!-- <script  src="resources/myscript.js"></script> -->
    <spring:url value="/resources/css/balanceSheet.css" var="balanceSheetcss"/>
  <link rel="stylesheet" href="${balanceSheetcss}">
    <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}">
   <spring:url value="/resources/javascript/ocr.js" var="ocrjs"/>
  <script src="${ocrjs}"></script>
  <spring:eval expression="@propertyConfigurer.getProperty('pblink')" var="pblink" />
 
 <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
 <style>
table, th , td  {
  border: 1px solid grey;
  border-collapse: collapse;
  padding: 5px;
  font-size: 12px;
  text-align: center;
  width:auto

}


  
}
table{
width: 100%;
}

#invoice-table{
  display: block;
}

#salesTable{

}

table td input {
   /* display: block; */
  width: 90%;
  /* height: 100%; */
 border: none;
border-color: transparent;
background-color: rgba(255, 255, 255, .1);
text-align:center
}
thead, th{
padding: 11px;
}

table th{
 background-color: #243a51;
 color: #fff;
font-size: .99rem;
    font-weight: 400;
}
table td{

font-size: .99rem;
    font-weight: 400;
}

table tr:nth-child(odd) {
  background-color: #E6E6E6;
}
table tr:nth-child(even) {
  background-color: #81DAF5;
}
.flagBg{
background-color: #ffc475;
}
@import "compass/css3";

.table-editable {
  position: relative;
  
  .glyphicon {
    font-size: 20px;
  }
}


  #progressbar {
    margin-top: 20px;
  }
 
  .progress-label {
    font-weight: bold;
    text-shadow: 1px 1px 0 #fff;
  }
 
  .ui-dialog-titlebar-close {
    display: none;
  }

#hStyle{
margin-left: 290px;
}

#outer
{
    width:100%;
    text-align: center;
}
.inner
{
    display: inline-block;
}
 




@media only screen and (max-width: 460px) {
    #invoice-table {
        width:90%;
    }
    input[type=text], textarea {
        width:10%;
    }
    #hStyle{
    margin-left: 10px;
    }
}

table.glyphicon-hover .glyphicon {
 visibility: hidden;
}
table.glyphicon-hover td:hover .glyphicon {
 visibility: visible;
}
[type=checkbox]:checked, [type=checkbox]:not(:checked) {
    position: inherit;
    opacity: 1;
    pointer-events: auto;
}


.loader {

  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 120px;
  height: 120px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
  display:block;
  margin: 0 auto;
  
}

</style>

</head>
<body>

</head>

<script>
  $( function() {
	  $('#datepicker2').datepicker({ 
		  dateFormat: 'dd-mm-yy' });
	
  } );
  
  $( function() {
	  $("#ocrAddNewProduct").draggable({ handle: ".modal-header" });
	
  } );
  
 
</script>

<body>

<!-- <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"> -->

<div  ng-app="myApp" ng-controller="myCtrl">


<!-- Modal Progress bar-->
<div class="modal fade" id="progreesBarModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-clock-o"></i> POIZIN</h4>
      </div>
      <div class="modal-body center-block">
         <p>Analyzing the document, Please wait...</p>
         <div class="w3-light-grey">
              <div id="progressId" class="w3-container w3-green w3-center" style="width:0%">0%</div>
         </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal Loader-->
<div class="modal fade" id="loader_show" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-clock-o"></i> POIZIN</h4>
      </div>
      <div class="modal-body center-block">
         <div ng-show="loader_show" style="float:center;" class="loader"></div>
         <div class="w3-light-grey">
              <div id="loaderId" class="w3-container w3-green w3-center" style="width:0%">0%</div>
         </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- edit row start -->
<!-- Modal suggestions bar-->
<div class="modal fade" id="editTableRow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
        
       


      </div>
      <div class="modal-body center-block">
         
    <div ng-show="tableShow">
<table  id="invoice-table-dialog">
<thead class="thead-dark">
<tr>
<td>Brand Number</td>
<td>{{data}}</td>
<td>Submit</td>
</tr>
</thead>

<tr ng-repeat="sug in suggData track by $index"  ng-click="ocrRowUpdate(brandNo,sug)">
<td>{{brandNo}}</td>
<td>{{sug}}</td>
<td><button type="button" ng-click="ocrRowUpdate(brandNo,sug)">OK</button></td>
</tr>
</table>
</div>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- edit row end -->

<!-- Modal suggestions bar for packQty-->
<div class="modal fade" id="packQtyEditTableRow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
        
       


      </div>
      <div class="modal-body center-block">
         
    <div ng-show="tableShow">
<table  id="invoice-table-dialog">
<thead class="thead-dark">
<tr>
<td>Brand Number</td>
<td>PackType</td>
<td>Quantity</td>
<td>Submit</td>
</tr>
</thead>

<tr ng-repeat="sug in suggData track by $index"  ng-click="ocrRowUpdate(brandNo,sug)">
<td>{{brandNo}}</td>
<td>{{sug.packType}}</td>
<td>{{sug.quantity}}</td>
<td><button type="button" ng-click="ocrRowUpdate(brandNo,sug)">OK</button></td>
</tr>
</table>
</div>

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- edit row end -->



<!-- Modal packSize suggestions-->
<div class="modal fade" id="editPackQty" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body center-block">
          <div ng-show="tableShow">
<table  id="invoice-table-dialog">
<thead class="thead-dark">
<tr>
<td class="col-xs-1">Brand Number</td>
<td class="col-xs-1">PackQty</td>
<td class="col-xs-1">PackSize</td>
<td class="col-xs-1">BottelRate</td>
<td class="col-xs-1">UnitRate</td>
<!-- <td class="col-xs-1">Submit</td> -->
</tr>
</thead>

<tr ng-repeat="sug in suggPackQty" ng-click="packQtySizeUpdate($index,sug)">
<td class="col-xs-1">{{sug.brandNo}}</td>
<td class="col-xs-1">{{sug.packQty}}</td>
<td class="col-xs-1">{{sug.quantity}}</td>
<td class="col-xs-1">{{sug.SingleBottelRate}}</td>
<td class="col-xs-1">{{sug.packQtyRate}}</td>
<!-- <td class="col-xs-1"><button type="button" ng-click="packQtySizeUpdate($index,sug)">OK</button></td> -->
</tr>
</table>
</div>

         

      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- Modal for Already submitted Invoice Data suggestions-->
<div class="modal fade" id="preSubData" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
 <div class="modal-dialog modal-fluid">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body center-block">
          <p>Data already submitted on this same date, do you want to submit again , click on "PROCEED" button</p>
<table  id="invoice-table-dialog">
<thead class="thead-dark">
<tr>
<th>SI.No.</th>
<th>Brand Number</th>
<th>Brand Name</th>
<th>Product Type</th>
<th>Pack Type</th>
<th>Pack Qty</th>
<th>Pack Size(ml)</th>
<th>Qty(Cases.Delivered)</th>
<th>Qty(Bottles.Delivered)</th>
<th>Unit Rate</th>
<th>Btl Rate</th>
<th>Btl MRP</th>
<th>Margin</th>
<th>Total</th>
</tr>
</thead>

<tr ng-repeat="sug in reSubData">
<td>{{$index+1}}</td>
<td >{{sug.brandNumber}}</td>
<td>{{sug.brandName}}</td>
<td>{{sug.productType}}</td>
<td>{{sug.packType}}</td>
<td>{{sug.packQty}}</td>
<td>{{sug.packSize}}</td>
<td>{{sug.caseDeliveredQty}}</td>
<td>{{sug.bottelsDeliveredQty}}</td>
<td>{{sug.unitRate}}</td>
<td>{{sug.btlRate}}</td>
<td>{{sug.btlMrp}}</td>
<td>{{sug.margin}}</td>
<td>{{sug.total}}</td>

</tr>
</table>
				  <button class="btn btn-default btn-rounded" style="float:right" type="button" id="" ng-click=ocrSubmit(true)>Proceed</button> 
</div>

       
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<!-- ocr records suggestion model -->
<div class="modal fade" id="editOCRRowSugg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
        
  

        
      </div>
      <div class="modal-body center-block">
         
<p>{{alertMsg}}</p>

      </div>
     <div style="float:right;"> <button style="float:right;" class="btn btn-default btn-rounded" type="button" ng-click="closeDialog($event)"  >OK</button></div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->




<!-- Add New Product Model dialog -->
<div class="modal fade" id="ocrAddNewProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body center-block">
      <!-- Body Start -->
     <a style="float:right;" href="${pblink}" target="_blank">TSBCL</a>
      
       <div class="shadowBoxCss">
     
<div style="text-align: center;padding-bottom: 32px;">
<label style="text-align: center;font-size: 16px;">ADD PRODUCT</label></div>
  <div class="row">
     <input type="hidden" name="popupmsg" id="popupmsg" value="${message}"/>
   
    <div class="col-sm-6 bodyFontCss">
    <label>Brand Number</label>
    <input type="text" id="ocrBrandNumber" name="brandNo" ng-model="brandNo" class="number-only form-control"  required>
    <label>Brand Name</label>
    <input type="text"  id="ocrBrandName" name="brandName"  class="form-control" required>
    <label>Select Product Type</label>
    <select name="productType" id="ocrProductType" required="required"  class=" form-control">
		<option value="" rval="">Select Product Type</option>
		<option ng-repeat="list in productDetails.productType" value="{{list}}">{{list}}</option>
	</select>
	<label>Select Quantity</label>
	<select name="quantity" id="ocrQuantity" required="required"  class="form-control">
		<option value="" rval="">Select Quantity</option>
		<option ng-repeat="list in productDetails.quantity" value="{{list}}">{{list}}</option>
	</select>
	<label>Select No. Of Bottles In A Case</label>
	<select name="packQty" id="ocrPackQty" required="required" class="form-control">
		<option value="" rval="">Select No. Of Bottles In A Case</option>
		<option ng-repeat="list in productDetails.packQty" value="{{list}}">{{list}}</option>
	</select>
	<label>Select Case Type</label>
	<select name="packType" id="ocrPackType" required="required" class="form-control">
		<option value="" rval="">Select Case Type</option>
		<option ng-repeat="list in productDetails.caseType" value="{{list}}">{{list}}</option>
	</select>
	</div>
	<div class="col-sm-6 bodyFontCss">
	<label>Select Category</label>
	<select name="category" id="ocrCategory" required="required"  class="form-control">
		<option value="" rval="">Select Category</option>
		<option ng-repeat="list in categoryList" value="{{list.id}}">{{list.name}}</option>
	</select>
	<label>Select Company</label>
	<select name="company" id="ocrCompany" required="required"  class="form-control">
		<option value="" rval="">Select Company</option>
		<option ng-repeat="list in companyList" value="{{list.id}}">{{list.name}}</option>
	</select>
	<label>Select Group</label>
	<select name="group" id="ocrGroup" required="required"  class="form-control">
		<option value="" rval="">Select Group</option>
		<option ng-repeat="list in groupList" value="{{list.id}}">{{list.name}}</option>
	</select>
	
	<label>Short Brand Name</label >
	<input type="text" id="ocrShortBrandName" name="shortBrandName" ng-model="shortBrandName" class=" form-control" required>
	<!-- <label>Special Margin</label>
	<input type="text" id="specialMargin" name="specialMargin" class=" form-control"  required> -->
	<label>Match Value</label>
	<input type="number" id="ocrMatch" name="match" class="number-only form-control" value="{{brandNo}}" required>
	<label>Match Name</label>
	<input type="text" id="ocrMatchName" name="matchName" class=" form-control"  value="{{shortBrandName}}" required>
	<label>Real Brand Number</label>
	<input type="number" id="ocrRealBrandNo" name="realBrandNo" class="number-only form-control"  value="{{brandNo}}" required>
    </div>
   </div>
   <p style="text-align: right;margin: 0px !important;"><input class="btn btn-default btn-rounded" type="submit" name="" value="ADD PRODUCT" ng-click="ocrAddNewBrandName()"/></p> 
   </div>
         
         

       <!-- Body Closed -->
      </div>
<!--            <div style="float:right;"> <button style="float:right;" class="btn btn-default btn-rounded" type="button" ng-click="closeDialog($event)"  >OK</button></div>
 -->      
     <div style="float:right;"> <button style="float:right;" class="btn btn-default btn-rounded" type="button"  data-dismiss="modal" >OK</button></div>
   
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
<!-- Add New Product Model Dialog Closed -->


 <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<!-- <nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">POIZIN</a>
    </div>
 
  </div>
</nav> -->

<div>
<div class="container-fluid" style="margin-left: 100px">

<div class="col-sm-4">

 <div style="float:left" ng-show="chooseDivShow">
			<form ng-submit="fileUpload()" method="post" class="ng-pristine ng-valid ng-submitted">
				<div id="outer">
            <div class="inner">
			<input class="btn btn-default btn-rounded" type="file" id="bannerImage" name="bannerImage" fileread="uploadme" accept="application/pdf" placeholder="Select Image">
		    </div>
           <div class="inner"> 
		   <input class="btn btn-default btn-rounded" type="submit" value="Process">
		   </div>
    </div> 
			</form>
			
		</div>
	
		</div>
			
				<!-- datepicker -->
						
				
	  <div class="col-sm-2 bodyFontCss">
     <label>Date As Per Sheet</label>
     <input style="width:120px" type="text" placeholder="Select Date" class="form-control" id="datepicker" name="invoiceDate" readonly="readonly" required>
    </div>
			
		
			
		<div class="col-sm-6" style="float:right">
		<div ng-show="tableShow">
		
	
		 <button class="btn btn-default btn-rounded" type="button" id="add-row" ng-click="addRow('0000','Enter Brand Name','-','-','0','0','0','0','0')">Add Record</button> 
		  <button class="btn btn-default btn-rounded" type="button" id="delete-row" ng-click="removeRow()">Delete Record</button> 
				  <button class="btn btn-default btn-rounded" type="button" id="delete-row" ng-click="ocrAddNewProduct()">Add New Product</button> 
				<%--   <div style="float:right">
				  <form ng-submit="ocrInvoicePdfViewer()">
				  <input type="image"  alt="submit" src="resources/images/pdf.png">
				  </form></div> --%>
				  <br>
		
		</div>
		</div>
</div>

      
            <div ng-show="tableShow">
<table class="table-editable glyphicon-hover" id="invoice-table" style="margin-bottom: 20px;">
<thead class="thead-dark">
<tr>

<th>Select</th>
<th>SI.No.</th>
<th>Brand Number</th>
<th>Brand Name</th>
<th>Product Type</th>
<th>Pack Type</th>
<th>Pack Qty</th>
<th>Pack Size(ml)</th>
<th>Qty(Cases.Delivered)</th>
<th>Qty(Bottles.Delivered)</th>
<th>Unit Rate</th>
<th>Btl Rate</th>
<th>Btl MRP</th>
<th>Margin</th>
<th>Total</th>
<!-- <th>Add/Delete</th> -->

</tr>
</thead>
<tr ng-repeat="row in invoice1">

<td ><input type="checkbox" name="record"></td>
<td  contenteditable='true'>{{$index+1}}</td>

<td ng-class="{'flagBg' : row.brand_No_Flag == false}" contenteditable='false' onmouseover="this.style.color = 'Red';" onmouseout="this.style.color = 'Black';" ><input id="brandNo" type="text"  ng-value="row.brandNumber" ng-model="row.brandNumber" ng-change="ocrEditData($index,'brandNumber')" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"></input></td>

<td style="width:200px" ng-class="{'flagBg' : row.brand_Name_Flag == false}" contenteditable='false' onmouseover="this.style.color = 'Red';" onmouseout="this.style.color = 'Black';"><input style="width:200px" id="brandName" value=text ng-model="row.brandName"  style="text-align: left;"  ng-value="row.brandName"  ng-change="ocrEditData($index,'brandName')"/><i ng-click="editTableRow(row.brandNumber,'brandName',$index)" class="glyphicon fas fa-edit" style="color: red;padding: 0px 0px 0px 6px;"></i></td>
<td ng-class="{'flagBg' : row.prod_Type_Flag == false}" contenteditable='false' onmouseover="this.style.color = 'Red';" onmouseout="this.style.color = 'Black';" ><input id="productType" type="text" ng-model="row.productType" ng-value="row.productType" ng-change="ocrEditData($index,'productType')"/><i ng-click="editTableRow(row.brandNumber,'productType',$index)" class="glyphicon fas fa-edit" style="color: red;padding: 0px 0px 0px 6px;"></i></td>
<td ng-class="{'flagBg' : row.pack_Type_Flag == false}" contenteditable='false' onmouseover="this.style.color = 'Red';" onmouseout="this.style.color = 'Black';" ><input id="packType" type="text" ng-model="row.packType" ng-value="row.packType"  ng-change="ocrEditData($index,'packType')" /><i ng-click="packTypeEditTableRow(row.brandNumber,'packType',$index)" class="glyphicon fas fa-edit" style="color: red;padding: 0px 0px 0px 6px;"></i></td>
<td ng-class="{'flagBg' : row.pack_Qty_Flag == false}" contenteditable='false' onmouseover="this.style.color = 'Red';" onmouseout="this.style.color = 'Black';" ><input id="packQty" type="text" ng-model="row.packQty" ng-change="salesValueCalc()" ng-value="row.packQty" ng-change="ocrEditData($index,'packQty')" /><i ng-click="editPackQty(row.brandNumber,'packQty',$index)" class="glyphicon fas fa-edit" style="color: red;padding: 0px 0px 0px 6px;"></i></td>
<td ng-class="{'flagBg' : row.pack_Size_Flag == false}" contenteditable='false' onmouseover="this.style.color = 'Red';" onmouseout="this.style.color = 'Black';"><input id="packSize" type="text"  ng-model="row.packSize" ng-change="salesValueCalc()" ng-value="row.packSize" ng-change="ocrEditData($index,'packSize')" /><i ng-click="editPackQty(row.brandNumber,'packSize',$index)" class="glyphicon fas fa-edit" style="color: red;padding: 0px 0px 0px 6px;"></i></td>
<td  contenteditable='false' onmouseover="this.style.color = 'Red';" onmouseout="this.style.color = 'Black';" ><input id="caseDeliveredQty" type="text"  ng-value="row.caseDeliveredQty"  ng-model="row.caseDeliveredQty" ng-change="salesValueCalc()"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"></input></td>
<td  contenteditable='false' onmouseover="this.style.color = 'Red';" onmouseout="this.style.color = 'Black';" ><input id="bottelsDeliveredQty" type="text"   ng-value="row.bottelsDeliveredQty" ng-model="row.bottelsDeliveredQty" ng-change="salesValueCalc()" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"></input></td>
<td  contenteditable='false' onmouseover="this.style.color = 'Red';" onmouseout="this.style.color = 'Black';" ><input id="unitRate" type="text"  ng-value="row.unitRate"  ng-model="row.unitRate" ng-change="salesValueCalc()"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"></input></td>
<td  contenteditable='false' onmouseover="this.style.color = 'Red';" onmouseout="this.style.color = 'Black';"><input id="btlRate" type="text"  ng-value="row.btlRate" ng-change="salesValueCalc()"  ng-model="row.btlRate"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"></input></td>
<td  ng-class="{'flagBg' : row.btlMrp_Flag == false}"  contenteditable='false' onmouseover="this.style.color = 'Red';" onmouseout="this.style.color = 'Black';" ><input id="btlMrp" type="text"  ng-value="row.btlMrp"  ng-model="row.btlMrp" ng-change=""  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"></input></td>
<td  ng-class="{'flagBg' : row.margin_Flag == false}"  contenteditable='false' onmouseover="this.style.color = 'Red';" onmouseout="this.style.color = 'Black';"><input id="margin" type="text"  ng-value="row.margin" ng-change=""  ng-model="row.margin"  oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"></input></td>
<td  contenteditable='true' onmouseover="this.style.color = 'Red';" onmouseout="this.style.color = 'Black';" ng-value="totalValue" ng-model="total"  ng-change="salesValueCalc()" pattern="^\d*(\.\d{0,2})?$" >{{row.total | number:2}}</td>
<!-- <td><i ng-click="editTableRow(row.brandNumber,'packType',$index)" class="glyphicon fas fa-plus" style="color: green;padding: 0px 0px 0px 8px;"></i><i ng-click="editTableRow(row.brandNumber,'packType',$index)" class="glyphicon fas fa-remove" style="color: red;padding: 0px 0px 0px 8px;"></i></td>
 --></tr>
</table>
</div> 


<div ng-show="tableShow" id="salesTable">
<table class="table-editable glyphicon-hover">
<thead class="thead-dark">
<tr>
<th class="col-xs-1">Date</th>
<th class="col-xs-1">Sales Value</th>
<th class="col-xs-1">MRP Rounding Off</th>
<th class="col-xs-1">Retail Shop Excise Turnover Tax</th>
<th class="col-xs-1">TCS</th>
<th class="col-xs-1">Retailer Credit Balance</th>

</tr>
</thead>
<tr>

<td  contenteditable='false' onmouseover="this.style.color = 'Red';" onmouseout="this.style.color = 'Black';" ><input id="datepicker2" name="invoiceDate" readonly="readonly" required type="text" ng-model="invoice2.invoiceDate" ng-value="invoice2.invoiceDate" /></td>


<td class="col-xs-1" contenteditable='true'>{{salesValue | number:2}}</td>

<td class="col-xs-1" contenteditable='true'>{{invoice2.mrpRoundingOff}}</td>
<td class="col-xs-1" contenteditable='true'>{{invoice2.retailShopExciseTurnoverTax}}</td>
<td class="col-xs-1" contenteditable='true'>{{invoice2.tcs}}</td>
<td class="col-xs-1" contenteditable='true'>{{invoice2.retailerCreditBalance}}</td>

</tr>

</table>
</div>

<div ng-show="tableShow">
 <button style="float: right" class="btn btn-default btn-rounded" type="button" id="add-row" ng-click=ocrSubmit(false)>Submit</button> 
</div>


  
</div>
</div>
</body>
</html>
