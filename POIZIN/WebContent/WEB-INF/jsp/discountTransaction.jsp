<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<spring:url value="/resources/css/3.3.7.bootstrap.min.css" var="bootstrap1mincss"/>
   <link rel="stylesheet" href="${bootstrap1mincss}">
   
 <jsp:include page="/WEB-INF/jsp/header.jsp" /> 
<spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
<script src="${jqueryuijs}"></script> 
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://rawgit.com/zorab47/jquery.ui.monthpicker/master/jquery.ui.monthpicker.js"></script> 06-->
<spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
<spring:url value="/resources/javascript/thirdpartyjs/jquery.ui.monthpicker.js" var="jqueryuimonthpickerjs"/>
  <script src="${jqueryuimonthpickerjs}"></script>   

<spring:url value="/resources/css/bootstrap-select.min.css" var="bootstrapselectmincss"/>
<link rel="stylesheet" href="${bootstrapselectmincss}">
<spring:url value="/resources/javascript/thirdpartyjs/bootstrap-select.min.js" var="bootstrapselectminjs"/>
<script src="${bootstrapselectminjs}"></script> 
<spring:url value="/resources/javascript/common.js" var="commonjs"/>
<script src="${commonjs}"></script> 
<spring:url value="/resources/javascript/discountTransaction.js" var="discountTransactionjs"/>
  <script src="${discountTransactionjs}"></script> 
<spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
<link rel="stylesheet" href="${newCommoncss}">
<spring:url value="/resources/css/invoiceEntry.css" var="invoiceEntrycss"/>
 <link rel="stylesheet" href="${invoiceEntrycss}">

</head>
<body ng-app="myApp" ng-controller="myCtrl" ng-init="init()">
<input type="hidden" name="popupmsg" id="popupmsg" value="${message}"  />
<div class="container" style="padding-top: 100px;">
<div>
  <div class="row">
    <div class="col-sm-3 bodyFontCss">
    <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Sale</li>
  <li>Enter Cheque Details</li>
</ul>
    </div>
    <div class="col-sm-2 bodyFontCss">
    <label>Select Month</label>
    <input type="text" class="monthpicker form-control" ng-model="monthlyDate" id="monthlyDate" readonly='true'>
    </div>
    <div class="col-sm-2 bodyFontCss">
    <label>Select Company</label>
    <select id="listdataSec" ng-model="companyId" required="required" class="form-control"></select>
    </div>
    <div class="col-sm-2 bodyFontCss" style="padding-top: 15px;">
    <input class="fetchExistingExpenses btn btn-default btn-rounded" type="submit" name="" value="GET DATA" />
    </div>
    <div class="col-sm-3 bodyFontCss"></div>
 </div>
 <div class="row newMonthlyExpensesForm">
   <%--  <form action="./addNewTransaction" class="form_span" id="" enctype="multipart/form-data" method="post"> --%>
   <form ng-submit="uploadReceivedCheckWithDetails()">
    <input type="hidden" id="company" name="company" class="number-only form-control" value="{{companyId}}"  readonly='true' required>
    <input type="hidden" id="months" name="months"  class=" form-control"  value="{{monthDate}}" readonly='true' required>
  <div class="col-sm-1 bodyFontCss">
    <label>Bank</label>
    <select name="bank" id="bank" required="required" class="inputstylesright form-control">
	<option value="" rval="">Select Bank</option>
	<option value="AB">AB</option>
	<option value="SBI">SBI</option>
	<option value="KVB">KVB</option>
	<option value="HDFC">HDFC</option>
	</select>
    </div>
    <div class="col-sm-1 bodyFontCss">
    <label>Tr. Type</label>
    <select name="TransactionType" id="TransactionType" required="required" class="inputstylesright form-control">
	<option value="" rval="">Tr. Type</option>
	<option value="Checks">Checks</option>
	<option value="NEFT">NEFT</option>
	<option value="IMPS">IMPS</option>
	<option value="CASH">CASH</option>
	</select>
    </div>
    <div class="col-sm-1 bodyFontCss">
    <label>Amount</label> 
    <input type="number" id="transactionAmt" name="transactionAmt" class=" form-control"  required>
    </div>
    <div class="col-sm-1 bodyFontCss" style="top: 25px;">
    <label style="padding: 0px 8px 0px 0px;">Received</label>
     <input type="checkbox" name="received" id="received" style="position: absolute;" ng-model="reveivedchecked" checked>
    </div>
    <div class="col-sm-1 bodyFontCss">
    <label>Credit Date</label>
     <input type="text" id="transactionDate" name="transactionDate" class="number-only form-control" ng-disabled="reveivedchecked"  readonly='true'>
    </div>
    <div class="col-sm-1 bodyFontCss">
    <label>ADJ Check</label>
     <input type="number" id="adjCheck" name="adjCheck" class=" form-control"  required>
    </div>
    <div class="col-sm-1 bodyFontCss">
    <label>Check No </label>
     <input type="text" id="checkNo" name="checkNo" class=" form-control"  required>
    </div>
    <div class="col-sm-1 bodyFontCss">
    <label>From Bank</label>
     <input type="text" id="fromBank" name="fromBank" class=" form-control"  required>
    </div>
    <div class="col-sm-1 bodyFontCss">
    <label>Comment</label>
     <textarea id="comment" name="comment" class=" form-control"  required></textarea>
    </div>
    <div class="col-sm-2 bodyFontCss">
    <label>Choose image</label ><input type="file" id="ngalleryImage" name="ngalleryImage" accept="image/*" required="required" placeholder="Select Image" class="">
    </div>
    <div class="col-sm-1 bodyFontCss" style="padding-top: 15px;">
    <input class="btn btn-default btn-rounded" type="submit" name="" value="SUBMIT"/>
    </div>
    </form>
 </div>
 <div class="row newMonthlyExpensesForm">
   <div class="col-sm-12 bodyFontCss">
    <table id="ItemsTable">
        <thead>
            <tr style="color: #fff;background-color: #062351;">
                <th class="col-xs-1">Delete</th>
                <th class="col-xs-2">Date</th>
                <th class="col-xs-1">Bank</th>
                <th class="col-xs-2">Tr. Type</th>
                <th class="col-xs-2">Amount</th>
                <th class="col-xs-1">ADJ cheque</th>
                <th class="col-xs-1">Cheque No</th>
                <th class="col-xs-1">From Bank</th>
                <th class="col-xs-1">Comment</th>
            </tr>
        </thead>
        <tr ng-repeat="list in transactionData" id="delete{{$index}}">
			<td class="col-xs-1" ng-class="$even ? 'oddrow' : 'evenrow'">
			<span class="glyphicon glyphicon-trash " ng-click="openListDisconts($index,list.id,list.companyId,list.realMonth,list.ngalleryImage)" style="color: #000;font-size: 12px;cursor: pointer;"></span>
			</td>
			<td class="col-xs-2" ng-class="$even ? 'oddrow' : 'evenrow'">{{list.transactionDate}}</td>
			<td class="col-xs-1" ng-class="$even ? 'oddrow' : 'evenrow'">{{list.bank}}</td>
			<td class="col-xs-2" ng-class="$even ? 'oddrow' : 'evenrow'">{{list.transactionType}}</td>
			<td class="col-xs-2" ng-class="$even ? 'oddrow' : 'evenrow'">{{list.transactionAmt}}</td>
			<td class="col-xs-1" ng-class="$even ? 'oddrow' : 'evenrow'">{{list.adjCheck}}</td>
			<td class="col-xs-1" ng-class="$even ? 'oddrow' : 'evenrow'">{{list.checkNo}}</td>
			<td class="col-xs-1" ng-class="$even ? 'oddrow' : 'evenrow'">{{list.fromBank}}</td>
			<td class="col-xs-1" ng-class="$even ? 'oddrow' : 'evenrow'">{{list.comment}}</td>
		</tr>
    </table>
   </div>
  </div>
  </div>
		<div style="margin-top: 50px;" ng-if="pendingCheckDetails.length > 0">
			<div class="row">
				<div class="col-sm-12 bodyFontCss">
				<h4 style="text-align: center;">Not yet received</h4>
					<table id="ItemsTable">
						<thead>
							<tr style="color: #fff; background-color: #062351;">
								<th class="col-xs-1">Month</th>
								<th class="col-xs-2">Company</th>
								<th class="col-xs-1">Amount</th>
								<th class="col-xs-2">From Bank</th>
								<th class="col-xs-1">Cheque No.</th>
								<th class="col-xs-1">Received</th>
								<th class="col-xs-2">Tr. Date</th>
								<th class="col-xs-1">Cleared Bank</th>
								<th class="col-xs-1">Submit</th>
							</tr>
						</thead>
						<tr ng-repeat="list in pendingCheckDetails" id="submit{{$index}}" on-finish-render="ngRepeatFinished">
							<td class="col-xs-1" ng-class="$even ? 'oddrow' : 'evenrow'">{{list.months}}</td>
							<td class="col-xs-2" ng-class="$even ? 'oddrow' : 'evenrow'">{{list.company}}</td>
							<td class="col-xs-1" ng-class="$even ? 'oddrow' : 'evenrow'">{{list.transactionAmt}}</td>
							<td class="col-xs-2" ng-class="$even ? 'oddrow' : 'evenrow'">{{list.fromBank}}</td>
							<td class="col-xs-1" ng-class="$even ? 'oddrow' : 'evenrow'">{{list.checkNo}}</td>
							<td class="col-xs-1" ng-class="$even ? 'oddrow' : 'evenrow'">
							<input type="checkbox" id="receivedupdate{{$index}}" ng-model="list.received" checked>
							</td>
							<td class="col-xs-2" ng-class="$even ? 'oddrow' : 'evenrow'">
							<input type="text" class=" form-control trdatetoupdate" id="trdate{{$index}}" ng-disabled="list.received" readonly='true'>
							</td>
							<td class="col-xs-1" ng-class="$even ? 'oddrow' : 'evenrow'">
							<select name="clearedBank{{$index}}" id="clearedBank{{$index}}" ng-model="list.bank" required="required" class="inputstylesright form-control">
								<option value="AB">AB</option>
								<option value="SBI">SBI</option>
								<option value="KVB">KVB</option>
								<option value="HDFC">HDFC</option>
							</select>
							</td>
							<td class="col-xs-1" ng-class="$even ? 'oddrow' : 'evenrow'">
								<span class="glyphicon glyphicon-edit "
								ng-click="updateReceived($index,list.id,list.companyId,list.months)"
								style="color: #000; font-size: 12px; cursor: pointer;"></span>
							</td>
						</tr>
					</table>



				</div>
			</div>
		</div>
	</div>
</body>
</html>