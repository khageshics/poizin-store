<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<jsp:include page="/WEB-INF/jsp/header.jsp" />  
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script> 
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-confirm.min.js" var="jqueryconfirmminjs"/>
  <script src="${jqueryconfirmminjs}"></script>
   <spring:url value="/resources/javascript/newBrand.js" var="newBrandjs"/>
  <script src="${newBrandjs}"></script> 
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}"> 
   <script>
var app = angular.module('myApp', []);
app.controller('myCtrl', function($scope, $http) {
	var myUrl = window.location.href;
	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
	var productListUrl =  url+"getDropDownListFromProduct";
	$http({
	    method : "GET",
	    url : productListUrl
	  }).then(function mySuccess(response) {
		  $scope.productDetails = response.data;
	  }, function myError(response) {
	  });
	var productListUrl =  url+"getCategoryList";
	$http({
	    method : "GET",
	    url : productListUrl
	  }).then(function mySuccess(response) {
		  $scope.categoryList = response.data;
	  }, function myError(response) {
	  });
	var productListUrl =  url+"getCompanyList";
	$http({
	    method : "GET",
	    url : productListUrl
	  }).then(function mySuccess(response) {
		  $scope.companyList = response.data;
	  }, function myError(response) {
	  });
	var groupListUrl =  url+"getGroupList";
	$http({
	    method : "GET",
	    url : groupListUrl
	  }).then(function mySuccess(response) {
		  $scope.groupList = response.data;
	  }, function myError(response) {
	  });
	
	
	
});
</script>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<h1>shiva</h1>
<div class="row" style="margin-bottom: 20px;padding-top: 100px;">
<div class="col-sm-3 bodyFontCss">
<ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Invoice</li>
  <li>Add Brand</li>
</ul>
</div>
<div class="col-sm-11 bodyFontCss"></div>
</div>
<div class="container">

<div class="shadowBoxCss">
<div style="text-align: center;padding-bottom: 5px;"><label style="text-align: center;font-size: 16px;">ADD PRODUCT</label></div>
 <form action="./addNewBrandName" class="form_span addNewBrandField" id="" method="post">
  <div class="row">
     <input type="hidden" name="popupmsg" id="popupmsg" value="${message}"/>
   
    <div class="col-sm-6 bodyFontCss">
    <label>Brand Number</label>
    <input type="text" id="brandNumber" name="brandNo" ng-model="brandNo" class="number-only form-control"  required>
    <label>Brand Name</label>
    <input type="text"  id="" name="brandName" class="form-control" required>
    <label>Select Product Type</label>
    <select name="productType" id="" required="required" class=" form-control">
		<option value="" rval="">Select Product Type</option>
		<option ng-repeat="list in productDetails.productType" value="{{list}}">{{list}}</option>
	</select>
	<label>Select Quantity</label>
	<select name="quantity" id="" required="required" class="form-control">
		<option value="" rval="">Select Quantity</option>
		<option ng-repeat="list in productDetails.quantity" value="{{list}}">{{list}}</option>
	</select>
	<label>Select No. Of Bottles In A Case</label>
	<select name="packQty" id="" required="required" class="form-control">
		<option value="" rval="">Select No. Of Bottles In A Case</option>
		<option ng-repeat="list in productDetails.packQty" value="{{list}}">{{list}}</option>
	</select>
	<label>Select Case Type</label>
	<select name="packType" id="" required="required" class="form-control">
		<option value="" rval="">Select Case Type</option>
		<option ng-repeat="list in productDetails.caseType" value="{{list}}">{{list}}</option>
	</select>
	</div>
	<div class="col-sm-6 bodyFontCss">
	<label>Select Category</label>
	<select name="category" id="" required="required" class="form-control">
		<option value="" rval="">Select Category</option>
		<option ng-repeat="list in categoryList" value="{{list.id}}">{{list.name}}</option>
	</select>
	<label>Select Company</label>
	<select name="company" id="" required="required" class="form-control">
		<option value="" rval="">Select Company</option>
		<option ng-repeat="list in companyList" value="{{list.id}}">{{list.name}}</option>
	</select>
	<label>Select Group</label>
	<select name="group" id="" required="required" class="form-control">
		<option value="" rval="">Select Group</option>
		<option ng-repeat="list in groupList" value="{{list.id}}">{{list.name}}</option>
	</select>
	<label>Short Brand Name</label >
	<input type="text" id="shortBrandName" name="shortBrandName" ng-model="shortBrandName" class=" form-control" required>
	<!-- <label>Special Margin</label>
	<input type="text" id="specialMargin" name="specialMargin" class=" form-control"  required> -->
	<label>Match Value</label>
	<input type="number" id="match" name="match" class="number-only form-control" value="{{brandNo}}" required>
	<label>Match Name</label>
	<input type="text" id="matchName" name="matchName" class=" form-control" value="{{shortBrandName}}" required>
	<label>Real Brand Number</label>
	<input type="number" id="realBrandNo" name="realBrandNo" class="number-only form-control" value="{{brandNo}}" required>
    </div>
   </div>
   <p style="text-align: right;margin: 0px !important;"><input class="btn btn-default btn-rounded" type="submit" name="" value="ADD PRODUCT"/></p>
   </form>
   </div>
   </div>         
      
</body>
</html>