<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script>
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script>
     <spring:url value="/resources/javascript/thirdpartyjs/angular.min.js" var="angularminjs"/>
  <script src="${angularminjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/angular-filter.js" var="angularfilterjs"/>
  <script src="${angularfilterjs}"></script>
 <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
 
  <spring:url value="/resources/javascript/fusioncharts/fusioncharts.js" var="fusionjs" />
<script src="${fusionjs}"></script>
<spring:url value="/resources/javascript/fusioncharts/themes/fusioncharts.theme.fint.js" var="fusionthemejs" />
<script src="${fusionthemejs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
    <spring:url value="/resources/css/bootstrap-select.min.css" var="bootstrapselectmincss"/>
   <link rel="stylesheet" href="${bootstrapselectmincss}">
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap-select.min.js" var="bootstrapselectminjs"/>
  <script src="${bootstrapselectminjs}"></script>
  
    <spring:url value="/resources/javascript/mobileView/mobileViewProductPerformanceCategoryWise.js" var="mobileViewProductPerformanceCategoryWisejs"/>
  <script src="${mobileViewProductPerformanceCategoryWisejs}"></script> 
     <spring:url value="/resources/css/mobileViewDayWiseBalanceSheet.css" var="mobileViewDayWiseBalanceSheetcss"/>
   <link rel="stylesheet" href="${mobileViewDayWiseBalanceSheetcss}">
   <!-- <script type="text/javascript" src="https://canvasjs.com/assets/script/jquery-1.11.1.min.js"></script>  --> 
<script type="text/javascript" src="https://canvasjs.com/assets/script/jquery.canvasjs.min.js"></script>
   <script>
  $('.selectpicker').selectpicker();
  </script>
  <style>
* {
    box-sizing: border-box;
}

body {
    font-family: Arial, Helvetica, sans-serif;
}

/* Style the header */
header {
    background-color: #062351;
    padding: 2px;
    text-align: center;
    font-size: 35px;
    color: white;
}

@media (max-width: 600px) {
    nav, article {
        width: 100%;
        height: auto;
    }
}
@font-face {
  font-family: "GothamRoundedBook";
  src: url(../assets/GothamRoundedBook_21018.ttf) format("truetype");
}
h1, h2, h3, h4, h5, h6 {
    font-family: "Segoe UI",Arial,sans-serif;
    font-weight: 400;
    margin: 10px 0;
}
h6 {
    font-size: 16px;
}
.bootstrap-select.btn-group .dropdown-menu li a span.text {
    display: inline-block;
    font-size: 11px;
}
 .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
    width: 50%;
}
.dropdown-menu>li>a:focus, .dropdown-menu>li>a:hover {
    color: #262626;
    text-decoration: none;
    background-color: #e6e8e8;
    }
 .bootstrap-select.btn-group .btn .filter-option {
    display: inline-block;
    overflow: hidden;
    width: 100%;
    text-align: left;
    font-size: 12px;
}
.canvasjs-chart-toolbar{
display: none;
}
</style>
</head>

<body  ng-app="myApp" ng-controller="myCtrl">
<a href="mobileViewHome" style="text-decoration: none;">
<header style="height: 55px;">
  <h2 style="float:left;padding: 0px 0px 0px 2px;">POIZIN</h2>
  <h6 style="float: right;padding: 8px 2px 0px 0px;">PRODUCT PERFORMANCE</h6>
</header></a>
<div class="container">
<div class="row allignmentcss">
    <div class="col-md-6" style="margin-bottom: 10px;">
    Start: <input type="text" class="dateinput" id="startDate" readonly='true'>
    End: <input type="text" class="dateinput" id="endDate" readonly='true'>
    </div>
	<div class="col-md-6">
	<select class="selectpicker" id="listdataSec" ng-model="dropValue" title="Select Product" data-live-search="false">
			</select>
			<input class="submit" type="submit" name="" ng-click="generateReport()" value="GET" />
	</div>
</div>

<div id="chart-container_for_beer" style="height: 600px;margin-top: 10px;"></div>

</div>
</body>
</html>