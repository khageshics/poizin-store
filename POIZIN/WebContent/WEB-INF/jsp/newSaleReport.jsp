<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <jsp:include page="/WEB-INF/jsp/header.jsp" />  
 
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
   <script src="https://cdn.jsdelivr.net/momentjs/2.10.6/moment.min.js"></script>
   <script src="https://cdn.jsdelivr.net/bootstrap.datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
  <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.ui.monthpicker.js" var="jqueryuimonthpickerjs"/>
  <script src="${jqueryuimonthpickerjs}"></script>
  
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script>
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
   <spring:url value="/resources/javascript/fusioncharts/fusioncharts.js" var="fusionjs" />
<script src="${fusionjs}"></script>
<spring:url value="/resources/javascript/fusioncharts/themes/fusioncharts.theme.fint.js" var="fusionthemejs" />
<script src="${fusionthemejs}"></script>
  <spring:url value="/resources/javascript/saleComparision.js" var="saleComparisionjs"/>
  <script src="${saleComparisionjs}"></script>
  <spring:url value="/resources/javascript/newSaleReport.js" var="newSaleReportjs"/>
  <script src="${newSaleReportjs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}"> 
 <spring:url value="/resources/css/newSaleReport.css" var="newSaleReportcss"/>
 <link rel="stylesheet" href="${newSaleReportcss}">
    <spring:url value="/resources/css/bootstrap-select.css" var="bootstrapselectcss"/>
   <link rel="stylesheet" href="${bootstrapselectcss}">
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap-select.js" var="bootstrapselectjs"/>
  <script src="${bootstrapselectjs}"></script>
   
 <style>
 .color-green {
  color: green;
}

.color-orange {
  color: orange;
}

.color-red {
  color: red;
}
.glyphicon {
    position: relative;
    top: 0px;
    display: inline-block;
    font-family: 'Glyphicons Halflings';
    font-style: normal;
    font-weight: 400;
    line-height: inherit;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 12px;
}
.datalist {
    max-height: calc(100vh - 310px);
    overflow-y: auto;
    width: 100%;
}
.row {
    margin-right: 0px;
    margin-left: 0px;
}
.datalist{
  max-height: calc(100vh - 278px);
  overflow-y: auto;
  width: 100%;
}
summary{
display : block;
}
details summary::-webkit-details-marker { display:none; }
.glyphiconcss{
	text-align: left;
	padding: 0px;
	font-size: 10px;
	overflow: initial;
	border: 0px;
}
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    width: 100% !important;
}
.mdb-skin .btn-default.dropdown-toggle {
    background-color: #0b2a41!important;
    border-radius: 5px;
}
span.tip {
    /* border-bottom: 1px dashed; */
    text-decoration: none;
    list-style-type: none;
}
span.tip:hover {
   /*  cursor: help; */
    position: relative
}
span.tip ul {
    display: none
}
span.tip ul > li {
        line-height: 20px;
        list-style-type: none; 
         text-align: left;
}
span.tip:hover ul {
     border: #c0c0c0 1px dotted;
    padding: 5px 20px 5px 5px;
    display: block;
    z-index: 100;
    left: 0px;
    margin: 10px;
    width: 85px;
    position: absolute;
    top: 10px;
    text-decoration: none;
    background: #243a51;
    color: #fff;
}
.fadecss:not(.show) {
    opacity: 1 !important;
}
.tab-content {
    padding: 0px;
}
.bootstrap-datetimepicker-widget tr:hover {
    background-color: #808080;
}
[type=radio]:checked, [type=radio]:not(:checked) {
    position: inherit;
    opacity: 1;
    pointer-events: auto;
}

</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<div class="container" style="margin-top: 85px;">
<ul id="tabs" class="nav nav-tabs" role="tablist">
  <li role="presentation" class="active"><a href="#saleReportTab" role="tab" data-toggle="tab" style="font-size: small;">Sale Report</a></li>
  <li role="presentation"><a href="#saleComparisionTab" role="tab" data-toggle="tab" style="font-size: small;">Sale Comparision</a></li>
</ul>
<div class="tab-content">
<div id="saleReportTab" role="tabpanel" class="tab-pane fade in active fadecss">
<div class="row" style="margin-bottom: 5px;">
   <div class="col-sm-4 bodyFontCss"></div>
   <div class="col-sm-2 bodyFontCss">
   <label>Start Date</label><input type="text" class="form-control" id="startDate" readonly='true'>
   </div>
   <div class="col-sm-2 bodyFontCss">
   <label>End Date</label><input type="text" class="form-control" id="endDate" readonly='true'>
   </div>
   <div class="col-sm-1 bodyFontCss customizedButton">
   <input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getResults('startDate','endDate')" value="Get Data" />
   </div>
  <div class="col-sm-2 bodyFontCss"></div>
  <div class="col-sm-1 bodyFontCss" style="text-align: right;">
  <span><i class="fa fa-filter filter-icon" style="color: #243a51!important;" data-toggle="modal" data-target="#saleFilterModal" aria-hidden="true"></i></span>
  </div>
  </div>
  <div class="row" style="margin-bottom: 20px;">
  <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
    <div class="col-sm-12 bodyFontCss">
      <div id="showAllDetails">
		<div class="saleReportWithCompanyWise">
		<span class="col-xs-1 paddingAndHeader" style="text-align: left;">&nbsp;</span>
		<span class="col-xs-2 paddingAndHeader" style="text-align: left;">Company</span>
		<span class="col-xs-2 paddingAndHeader">%</span>
		<span class="col-xs-2 paddingAndHeader">Cases</span>
		<span class="col-xs-2 paddingAndHeader">In-HouseStock</span>
		<span class="col-xs-2 paddingAndHeader" style="text-align: right;">Total Price</span>
		<span class="col-xs-1 paddingAndHeader" style="text-align: right;">&nbsp;</span>
		<div class="datalist">
		<details ng-repeat="list in saledata | orderBy:'totalSaleAmount':true | filter:filterData">
				<summary>
					<div class="stockLiftList">
					    <span class="col-xs-1 borderBottomAllignment" style="color: #000;text-align: left;border-left: 5px solid {{list.bgcolor}};">&nbsp;</span>
					    <span class="col-xs-2 borderBottomAllignment" style="color: #000;text-align: left;">{{list.company}}
						<span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale">
						</span>
						<span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale">
						</span>
						<span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale">
						</span>
						<span class="" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo != 0">&nbsp;
						</span>
						</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="">{{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" title="" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" style="text-align:right; color: #000;">{{list.totalSaleAmount | INR}}</span>
						<span class="col-xs-1 borderBottomAllignment" style="text-align:right; color: #000;border-bottom: solid 1px #ccc;border-right: 5px solid {{list.bgcolor}};" title="">&nbsp;</span>
					</div>
				</summary>
				     <span class="col-xs-1 paddingAndInnerHeader" style="text-align: left;">&nbsp;</span>
				     <span class="col-xs-2 paddingAndInnerHeader" style="text-align: left;">Brand Name</span>
				     <span class="col-xs-2 paddingAndInnerHeader">%</span>
					 <span class="col-xs-2 paddingAndInnerHeader">Cases</span>
					 <span class="col-xs-2 paddingAndInnerHeader">In-HouseStock</span>
					 <span class="col-xs-2 paddingAndInnerHeader" style="text-align: right;">Total Price</span>
					 <span class="col-xs-1 paddingAndInnerHeader" style="text-align: right;">&nbsp;</span>
				<details ng-repeat="data in list.inputjson | orderBy:'saleValue':true">
				   <summary>
				   <span class="col-xs-1 secondSpan secondSpanPadding" style="color: #000;text-align: left;">&nbsp;</span>
				   <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align: left;">
					   <span class="glyphicon glyphicon-question-sign" style="color: #000;text-align: right;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compairSaleSuper >= -20 &&  data.compairSaleSuper <= 20" title="Previous Day Sold: {{data.preCases}}, Current Day Sold: {{data.currCases}}"></span>
					   {{data.brandname}}
						<span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale">
					 </span>
					 <span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale">
					 </span>
					 <span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale">
					 </span>
					 <span class="" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="data.compareNo != 0">&nbsp;
					 </span>
					 </span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="">{{data.saleValue / list.totalSaleAmount * 100 | roundup}}%</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding tip" style="overflow: visible;" ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouseCase | INR}}
					 <ul>
						<li ng-repeat="qty in data.Brands | orderBy:'order'">{{qty.packType}} : {{qty.caseStock}} / {{qty.closing}}</li>
					 </ul>
					 </span>
					  <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align: right;">{{data.saleValue | INR}}</span>
					 <span class="col-xs-1 secondSpan secondSpanPadding" style="color: #000;text-align: right;">&nbsp;</span>
					 </summary>
					 <table class=" table table-fixed">
					   <thead>
						    <tr>
						      <th class="col-xs-3">Quantity</th>
						      <th class="col-xs-3">Sale (Case/Btl)</th>
						      <th class="col-xs-3">In-HouseStock</th>
						      <th class="col-xs-2">Total Price</th>
						      <th class="col-xs-1">&nbsp;</th>
						    </tr>
					    </thead>
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true">
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
							<td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0" title="MRP {{brand.shopRate}}">{{brand.quantity}}</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-2" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						    <td class="col-xs-1" style="color: #000;border-top: 0px solid #ddd;" data-toggle="modal" data-target="#basicExampleModal" ng-click="openModalPopup(brand.brandNoPackQty,data.brandname,brand.quantity,brand.categoryId)"><i class="fa fa-line-chart"  style="color: #000; size: 12px; font-weight: bold;"></i></td>
						</tr></table>
					 </details>
			</details>
			<details ng-repeat="list in saledata | orderBy:'totalSaleAmount':true | filter:filterDataBeer">
				<summary>
					<div class="stockLiftList">
					    <span class="col-xs-1 borderBottomAllignment" style="color: #000;text-align: left;border-left: 5px solid {{list.bgcolor}};">&nbsp;</span>
						 <span class="col-xs-2 borderBottomAllignment" style="color: #000;text-align: left;">{{list.company}}
						<span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale">
						</span>
						<span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale">
						</span>
						<span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale">
						</span>
						<span class="" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo != 0">&nbsp;
						</span>
						</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="">{{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" style="text-align:right; color: #000;">{{list.totalSaleAmount | INR}}</span>
						<span class="col-xs-1 borderBottomAllignment" style="text-align:right; color: #000;border-bottom: solid 1px #ccc;border-right: 5px solid {{list.bgcolor}};" title="">&nbsp;</span>
					</div>
				</summary>
				     <span class="col-xs-1 paddingAndInnerHeader" style="text-align: left;">&nbsp;</span>
				     <span class="col-xs-2 paddingAndInnerHeader" style="text-align: left;">Brand Name</span>
				     <span class="col-xs-2 paddingAndInnerHeader">%</span>
					 <span class="col-xs-2 paddingAndInnerHeader">Cases</span>
					 <span class="col-xs-2 paddingAndInnerHeader">In-HouseStock</span>
					 <span class="col-xs-2 paddingAndInnerHeader" style="text-align: right;">Total Price</span>
					 <span class="col-xs-1 paddingAndInnerHeader" style="text-align: right;">&nbsp;</span>
				<details ng-repeat="data in list.inputjson | orderBy:'saleValue':true">
				   <summary>
				     <span class="col-xs-1 secondSpan secondSpanPadding" style="color: #000;text-align: left;">&nbsp;</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align: left;">
					 <span class="glyphicon glyphicon-question-sign" style="color: #000;text-align: right;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compairSaleSuper >= -20 &&  data.compairSaleSuper <= 20" title="Previous Day Sold: {{data.preCases}}, Current Day Sold: {{data.currCases}}"></span>
					 {{data.brandname}}
					  <span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale"></span>
					 <span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale"></span>
					 <span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale"> </span>
					 <span class="" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="data.compareNo != 0">&nbsp; </span>
					 </span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="">{{data.saleValue / list.totalSaleAmount * 100 | roundup}}%</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding tip" style="overflow: visible;" ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouseCase | INR}}
					 <ul>
						<li ng-repeat="qty in data.Brands | orderBy:'order'">{{qty.packType}} : {{qty.caseStock}} / {{qty.closing}}</li>
					</ul>
					 </span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align:right;" title="">{{data.saleValue | INR}}</span>
					 <span class="col-xs-1 secondSpan secondSpanPadding" style="color: #000;text-align:right;" title="">&nbsp;</span>
					 </summary>
					 <table class=" table table-fixed">
					   <thead>
						    <tr>
						      <th class="col-xs-3">Quantity</th>
						      <th class="col-xs-3">Sale (Case/Btl)</th>
						      <th class="col-xs-3">In-HouseStock</th>
						      <th class="col-xs-2">Total Price</th>
						      <th class="col-xs-1">&nbsp;</th>
						    </tr>
					    </thead>
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true">
							 <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0" title="MRP {{brand.shopRate}}">{{brand.quantity}}</td>
						   <td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale" title="MRP {{brand.shopRate}}">
						   <p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
						   {{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-2" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						    <td class="col-xs-1" style="color: #000;border-top: 0px solid #ddd;" data-toggle="modal" data-target="#basicExampleModal" ng-click="openModalPopup(brand.brandNoPackQty,data.brandname,brand.quantity,brand.categoryId)"><i class="fa fa-line-chart"  style="color: #000; size: 12px; font-weight: bold;"></i></td>
						</tr></table>
					 </details>
			</details> 
			</div> 
		</div>
		<div class="saleReportWithCategoryWise">
		<span class="col-xs-1 paddingAndHeader" style="text-align: left;">&nbsp;</span>
		<span class="col-xs-2 paddingAndHeader" style="text-align: left;">Category</span>
		<span class="col-xs-2 paddingAndHeader">%</span>
		<span class="col-xs-2 paddingAndHeader">Cases</span>
		<span class="col-xs-2 paddingAndHeader">In-HouseStock</span>
		<span class="col-xs-2 paddingAndHeader" style="text-align: right;">Total Price</span>
		<span class="col-xs-1 paddingAndHeader" style="text-align: right;">&nbsp;</span>
		<div class="datalist">
		<details ng-repeat="list in saleCategoryData | orderBy:'categoryOrder' | filter:filterCategoryData">
				<summary>
					<div class="stockLiftList">
					    <span class="col-xs-1 borderBottomAllignment" style="color: #000;text-align: left;border-left: 5px solid {{list.bgcolor}};">&nbsp;</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;text-align: left;">{{list.category}}
						<span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale">
						</span>
						<span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale">
						</span>
						<span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale">
						</span>
						<span class="" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo != 0">&nbsp;
						</span>
						</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="">{{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;text-align: right;">{{list.totalSaleAmount | INR}}</span>
						<span class="col-xs-1 borderBottomAllignment" style="color: #000;text-align: right;border-right: 5px solid {{list.bgcolor}};" title="">&nbsp;</span>
					</div>
				</summary>
				     <span class="col-xs-1 paddingAndInnerHeader" style="text-align: left;">&nbsp;</span>
				     <span class="col-xs-2 paddingAndInnerHeader" style="text-align: left;">Brand Name</span>
				     <span class="col-xs-2 paddingAndInnerHeader">%</span>
					 <span class="col-xs-2 paddingAndInnerHeader">Cases</span>
					 <span class="col-xs-2 paddingAndInnerHeader">In-HouseStock</span>
					 <span class="col-xs-2 paddingAndInnerHeader" style="text-align: right;">Total Price</span>
					 <span class="col-xs-1 paddingAndInnerHeader" style="text-align: right;">&nbsp;</span>
				<details ng-repeat="data in list.inputcategoryjson | orderBy:'saleValue':true">
				   <summary>
				     <span class="col-xs-1 secondSpan secondSpanPadding" style="color: #000;text-align: left;">&nbsp;</span>
				     <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align: left;">
				     <span class="glyphicon glyphicon-question-sign" style="color: #000;text-align: right;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compairSaleSuper >= -20 &&  data.compairSaleSuper <= 20" title="Previous Day Sold: {{data.preCases}}, Current Day Sold: {{data.currCases}}"></span>
				     {{data.brandname}}
					 <span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale">
					 </span>
					 <span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale">
					 </span>
					 <span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale">
					 </span>
					 <span class="" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="data.compareNo != 0">&nbsp;
					 </span>
					 </span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" >{{data.saleValue / list.totalSaleAmount * 100 | roundup}}%</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" >{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding tip" style="overflow: visible;" ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouseCase | INR}}
					 <ul>
						<li ng-repeat="qty in data.Brands | orderBy:'order'">{{qty.packType}} : {{qty.caseStock}} / {{qty.closing}}</li>
				    	</ul>
					 </span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align: right;">{{data.saleValue | INR}}</span>
					 <span class="col-xs-1 secondSpan secondSpanPadding" style="color: #000;text-align: right;">&nbsp;</span>
					 </summary>
					 <table class=" table table-fixed">
					 <thead>
						    <tr>
						      <th class="col-xs-3">Quantity</th>
						      <th class="col-xs-3">Sale (Case/Btl)</th>
						      <th class="col-xs-3">In-HouseStock</th>
						      <th class="col-xs-2">Total Price</th>
						      <th class="col-xs-1">&nbsp;</th>
						    </tr>
					    </thead>
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true">
						 <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0" title="MRP {{brand.shopRate}}">{{brand.quantity}}</td>
						   <td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale" title="MRP {{brand.shopRate}}">
						   <p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
						   {{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						    <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-2" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						    <td class="col-xs-1" style="color: #000;border-top: 0px solid #ddd;" data-toggle="modal" data-target="#basicExampleModal" ng-click="openModalPopup(brand.brandNoPackQty,data.brandname,brand.quantity,brand.categoryId)"><i class="fa fa-line-chart"  style="color: #000; size: 12px; font-weight: bold;"></i></td>
						</tr></table>
					 </details>
			</details>
			<details ng-repeat="list in saleCategoryData | orderBy:'categoryOrder' | filter:filterCategoryDataBeer">
				<summary>
					<div class="stockLiftList">
					    <span class="col-xs-1 borderBottomAllignment" style="color: #000;text-align: left;border-left: 5px solid {{list.bgcolor}};">&nbsp;</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;text-align: left;">{{list.category}}
						<span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale"></span>
						<span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale"></span>
						<span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale"></span>
						<span class="" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo != 0">&nbsp;
						</span>
						</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="">{{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;text-align: right;">{{list.totalSaleAmount | INR}}</span>
						<span class="col-xs-1 borderBottomAllignment" style="color: #000;text-align: right;border-right: 5px solid {{list.bgcolor}};" title="">&nbsp;</span>
					</div>
				</summary>
				     <span class="col-xs-1 paddingAndInnerHeader" style="text-align: left;">&nbsp;</span>
				     <span class="col-xs-2 paddingAndInnerHeader" style="text-align: left;">Brand Name</span>
				     <span class="col-xs-2 paddingAndInnerHeader">%</span>
					 <span class="col-xs-2 paddingAndInnerHeader">Cases</span>
					 <span class="col-xs-2 paddingAndInnerHeader">In-HouseStock</span>
					 <span class="col-xs-2 paddingAndInnerHeader" style="text-align: right;">Total Price</span>
					 <span class="col-xs-1 paddingAndInnerHeader" style="text-align: right;">&nbsp;</span>
				<details ng-repeat="data in list.inputcategoryjson | orderBy:'saleValue':true">
				   <summary>
					 <span class="col-xs-1 secondSpan secondSpanPadding" style="color: #000;text-align: left;">&nbsp;</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align: left;">
					 <span class="glyphicon glyphicon-question-sign" style="color: #000;text-align: right;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compairSaleSuper >= -20 &&  data.compairSaleSuper <= 20" title="Previous Day Sold: {{data.preCases}}, Current Day Sold: {{data.currCases}}"></span>
					 {{data.brandname}}
					<span class="glyphicon glyphicon-arrow-down secondSpanPadding" style="color: red;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale">
					 </span>
					 <span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale">
					 </span>
					 <span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale">
					 </span>
					 <span class="" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="data.compareNo != 0">&nbsp;
					 </span>
					 </span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="">{{data.saleValue / list.totalSaleAmount * 100 | roundup}}%</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding tip" style="overflow: visible;" ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouseCase | INR}}
					 <ul>
						<li ng-repeat="qty in data.Brands | orderBy:'order'">{{qty.packType}} : {{qty.caseStock}} / {{qty.closing}}</li>
					</ul>
					 </span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align: right;" title="">{{data.saleValue | INR}}</span>
					 <span class="col-xs-1 secondSpan secondSpanPadding" style="color: #000;text-align: right;" title="">&nbsp;</span>
					 </summary>
					 <table class=" table table-fixed">
					    <thead>
						    <tr>
						      <th class="col-xs-3">Quantity</th>
						      <th class="col-xs-3">Sale (Case/Btl)</th>
						      <th class="col-xs-3">In-HouseStock</th>
						      <th class="col-xs-2">Total Price</th>
						      <th class="col-xs-1">&nbsp;</th>
						    </tr>
					    </thead>
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true">
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0" title="MRP {{brand.shopRate}}">{{brand.quantity}}</td>
						    <td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale" title="MRP {{brand.shopRate}}">
						    <p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
						    {{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-2" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						    <td class="col-xs-1" style="color: #000;border-top: 0px solid #ddd;" data-toggle="modal" data-target="#basicExampleModal" ng-click="openModalPopup(brand.brandNoPackQty,data.brandname,brand.quantity,brand.categoryId)"><i class="fa fa-line-chart"  style="color: #000; size: 12px; font-weight: bold;"></i></td>
						</tr></table>
					 </details>
			</details>
			</div>
		</div>
   </div>
   
   <div id="showAllOnlySaleDetails">
		<div class="saleReportWithCompanyWise">
		<span class="col-xs-1 paddingAndHeader" style="text-align: left;">&nbsp;</span>
		<span class="col-xs-2 paddingAndHeader" style="text-align: left;">Company</span>
		<span class="col-xs-2 paddingAndHeader">%</span>
		<span class="col-xs-2 paddingAndHeader">Cases</span>
		<span class="col-xs-2 paddingAndHeader">In-HouseStock</span>
		<span class="col-xs-2 paddingAndHeader" style="text-align: right;">Total Price</span>
		<span class="col-xs-1 paddingAndHeader">&nbsp;</span>
		<div class="datalist">
		<h4 ng-if="(alldetails | conditionVal) <=0" style="text-align: center;padding: 8px;">No Data Available</h4>
		
		<details ng-repeat="list in saledataOnlySale | orderBy:'totalSaleAmount':true | filter:filterDataOnlySale">
				<summary>
					<div class="stockLiftList">
						<span class="col-xs-1 borderBottomAllignment" style="color: #000;text-align: left;border-left: 5px solid {{list.bgcolor}};">&nbsp;</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;text-align: left;">{{list.company}}
						<span class=" glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale">
						</span>
						<span class=" glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale">
						</span>
						<span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale">
						</span>
						<span class="" style=";color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo != 0">&nbsp;
						</span>
						</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="">{{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" style="text-align:right; color: #000;">{{list.totalSaleAmount | INR}}</span>
						<span class="col-xs-1 borderBottomAllignment" style="text-align:right; color: #000;border-right: 5px solid {{list.bgcolor}};" title="">&nbsp;</span>
					</div>
				</summary>
				     <span class="col-xs-1 paddingAndInnerHeader" style="text-align: left;">&nbsp;</span>
				     <span class="col-xs-2 paddingAndInnerHeader" style="text-align: left;">Brand Name</span>
				     <span class="col-xs-2 paddingAndInnerHeader">%</span>
					 <span class="col-xs-2 paddingAndInnerHeader">Cases</span>
					 <span class="col-xs-2 paddingAndInnerHeader">In-HouseStock</span>
					 <span class="col-xs-2 paddingAndInnerHeader" style="text-align: right;">Total Price</span>
					 <span class="col-xs-1 paddingAndInnerHeader" style="text-align: right;">&nbsp;</span>
				<details ng-repeat="data in list.inputjson | orderBy:'saleValue':true | filter: greaterThan('totalbtlsale', 0)">
				   <summary>
				   <span class="col-xs-1 secondSpan secondSpanPadding" style="color: #000;text-align: left;">&nbsp;</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align: left;">
					  <span class="glyphicon glyphicon-question-sign" style="color: #000;text-align: right;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compairSaleSuper >= -20 &&  data.compairSaleSuper <= 20" title="Previous Day Sold: {{data.preCases}}, Current Day Sold: {{data.currCases}}">
					  </span>
					 {{data.brandname}}
					 <span class=" glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale">
					 </span>
					 <span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale">
					 </span>
					 <span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale">
					 </span>
					 <span class="" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="data.compareNo != 0">&nbsp;
					 </span>
					 </span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="">{{data.saleValue / list.totalSaleAmount * 100 | roundup}}%</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding tip" style="overflow: visible;"  ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouseCase | INR}}
					 <ul>
						<li ng-repeat="qty in data.Brands | orderBy:'order'">{{qty.packType}} : {{qty.caseStock}} / {{qty.closing}}</li>
					</ul>
					 </span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align:right;" title="">{{data.saleValue | INR}}</span>
					 <span class="col-xs-1 secondSpan secondSpanPadding" style="color: #000;text-align:right;" title="">&nbsp;</span>
					 </summary>
					 <table class=" table table-fixed">
					   <thead>
						    <tr>
						      <th class="col-xs-3">Quantity</th>
						      <th class="col-xs-3">Sale (Case/Btl)</th>
						      <th class="col-xs-3">In-HouseStock</th>
						      <th class="col-xs-2">Total Price</th>
						      <th class="col-xs-1">&nbsp;</th>
						    </tr>
					    </thead>
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true | filter: greaterThan('amount', 0)">
							<td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0" title="MRP {{brand.shopRate}}">{{brand.quantity}}</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-2" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						    <td class="col-xs-1" style="color: #000;border-top: 0px solid #ddd;" data-toggle="modal" data-target="#basicExampleModal" ng-click="openModalPopup(brand.brandNoPackQty,data.brandname,brand.quantity,brand.categoryId)"><i class="fa fa-line-chart"  style="color: #000; size: 12px; font-weight: bold;"></i></td>
						</tr></table>
					 </details>
			</details>
			<details ng-repeat="list in saledataOnlySale | orderBy:'totalSaleAmount':true | filter:filterDataBeerOnlySale">
				<summary>
					<div class="stockLiftList">
					    <span class="col-xs-1 borderBottomAllignment" style="color: #000;text-align: left;border-left: 5px solid {{list.bgcolor}};" title="">&nbsp;</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;text-align: left;">{{list.company}}
						<span class=" glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale">
						</span>
						<span class=" glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale">
						</span>
						<span class=" glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale">
						</span>
						<span class="" style=";color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo != 0">&nbsp;
						</span>
						</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="">{{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" style="text-align:right; color: #000;">{{list.totalSaleAmount | INR}}</span>
						<span class="col-xs-1 borderBottomAllignment" style="text-align:right;color: #000;border-right: 5px solid {{list.bgcolor}};" title="">&nbsp;</span>
					</div>
				</summary>
				     <span class="col-xs-1 paddingAndInnerHeader" style="text-align: left;">&nbsp;</span>
				     <span class="col-xs-2 paddingAndInnerHeader" style="text-align: left;">Brand Name</span>
				     <span class="col-xs-2 paddingAndInnerHeader">%</span>
					 <span class="col-xs-2 paddingAndInnerHeader">Cases</span>
					 <span class="col-xs-2 paddingAndInnerHeader">In-HouseStock</span>
					 <span class="col-xs-2 paddingAndInnerHeader" style="text-align: right;">Total Price</span>
					 <span class="col-xs-1 paddingAndInnerHeader" style="text-align: right;">&nbsp;</span>
				<details ng-repeat="data in list.inputjson | orderBy:'saleValue':true | filter: greaterThan('totalbtlsale', 0)">
				   <summary>
				     <span class="col-xs-1 secondSpan secondSpanPadding" style="color: #000;text-align: left;">&nbsp;</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align: left;">
					 <span class="glyphicon glyphicon-question-sign" style="color: #000;text-align: right;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compairSaleSuper >= -20 &&  data.compairSaleSuper <= 20" title="Previous Day Sold: {{data.preCases}}, Current Day Sold: {{data.currCases}}"></span>
					 {{data.brandname}}
					 <span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale">
					 </span>
					 <span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale">
					 </span>
					 <span class=" glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale">
					 </span>
					 <span class="" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 9px;" ng-if="data.compareNo != 0">&nbsp;
					 </span>
					 </span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="">{{data.saleValue / list.totalSaleAmount * 100 | roundup}}%</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding tip" style="overflow: visible;" ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouseCase | INR}}
					 <ul>
						<li ng-repeat="qty in data.Brands | orderBy:'order'">{{qty.packType}} : {{qty.caseStock}} / {{qty.closing}}</li>
					</ul>
					 </span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="text-align:right;color: #000;" title="">{{data.saleValue | INR}}</span>
					 <span class="col-xs-1 secondSpan secondSpanPadding" style="text-align:right;color: #000;" title="">&nbsp;</span>
					 </summary>
					 <table class="table table-fixed">
					 <thead>
					    <tr>
					      <th class="col-xs-3">Quantity</th>
					      <th class="col-xs-3">Sale (Case/Btl)</th>
					      <th class="col-xs-3">In-HouseStock</th>
					      <th class="col-xs-2">Total Price</th>
					      <th class="col-xs-1">&nbsp;</th>
					    </tr>
					    </thead>
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true | filter: greaterThan('amount', 0)">
							<td  class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0" title="MRP {{brand.shopRate}}">{{brand.quantity}}</td>
						    <td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale" title="MRP {{brand.shopRate}}">
						    <p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
						    {{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-2" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						    <td class="col-xs-1" style="color: #000;border-top: 0px solid #ddd;" data-toggle="modal" data-target="#basicExampleModal" ng-click="openModalPopup(brand.brandNoPackQty,data.brandname,brand.quantity,brand.categoryId)"><i class="fa fa-line-chart"  style="color: #000; size: 12px; font-weight: bold;"></i></td>
						</tr></table>
					 </details>
			</details> 
			</div> 
		</div>
		<div class="saleReportWithCategoryWise">
		<span class="col-xs-1 paddingAndHeader" style="text-align: left;">&nbsp;</span>
		<span class="col-xs-2 paddingAndHeader" style="text-align: left;">Category</span>
		<span class="col-xs-2 paddingAndHeader">%</span>
		<span class="col-xs-2 paddingAndHeader">Cases</span>
		<span class="col-xs-2 paddingAndHeader">In-HouseStock</span>
		<span class="col-xs-2 paddingAndHeader" style="text-align: right;">Total Price</span>
		<span class="col-xs-1 paddingAndHeader" style="text-align: right;">&nbsp;</span>
		<div class="datalist">
		<h4 ng-if="(alldetails | conditionVal) <=0" style="text-align: center;padding: 8px;">No Data Available</h4>
		<details ng-repeat="list in saleCategoryDataOnlySale | orderBy:'categoryOrder' | filter:filterCategoryDataOnlySale">
				<summary>
					<div class="stockLiftList">
					    <span class="col-xs-1 borderBottomAllignment" style="color: #000;text-align: left;border-left: 5px solid {{list.bgcolor}};">&nbsp;</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;text-align: left;">{{list.category}}
						<span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale">
						</span>
						<span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale">
						</span>
						<span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale">
						</span>
						<span class="" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo != 0">&nbsp;
						</span>
						</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="">{{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;text-align: right;">{{list.totalSaleAmount | INR}}</span>
						<span class="col-xs-1 borderBottomAllignment" style="color: #000;text-align: right;border-right: 5px solid {{list.bgcolor}};" title="">&nbsp;</span>
					</div>
				</summary>
				     <span class="col-xs-1 paddingAndInnerHeader" style="text-align: left;">&nbsp;</span>
				     <span class="col-xs-2 paddingAndInnerHeader" style="text-align: left;">Brand Name</span>
					 <span class="col-xs-2 paddingAndInnerHeader">%</span>
					 <span class="col-xs-2 paddingAndInnerHeader">Case</span>
					 <span class="col-xs-2 paddingAndInnerHeader">In-HouseStock</span>
					 <span class="col-xs-2 paddingAndInnerHeader" style="text-align: right;">Total Price</span>
					 <span class="col-xs-1 paddingAndInnerHeader" style="text-align: right;">&nbsp;</span>
				<details ng-repeat="data in list.inputcategoryjson | orderBy:'saleValue':true | filter: greaterThan('totalbtlsale', 0)">
				   <summary>
				    <span class="col-xs-1 secondSpan secondSpanPadding" style="color: #000;text-align: left;">&nbsp;</span>
					<span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align: left;">
					<span class="glyphicon glyphicon-question-sign" style="color: #000;text-align: right;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compairSaleSuper >= -20 &&  data.compairSaleSuper <= 20" title="Previous Day Sold: {{data.preCases}}, Current Day Sold: {{data.currCases}}"></span>
					{{data.brandname}}
					 <span class="glyphicon glyphicon-arrow-down " style="color: red;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale">
					 </span>
					 <span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale">
					 </span>
					 <span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale">
					 </span>
					 <span class="" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="data.compareNo != 0">&nbsp;
					 </span>
					 </span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="">{{data.saleValue / list.totalSaleAmount * 100 | roundup}}%</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding tip" style="overflow: visible;" ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouseCase | INR}}
						 <ul>
							<li ng-repeat="qty in data.Brands | orderBy:'order'">{{qty.packType}} : {{qty.caseStock}} / {{qty.closing}}</li>
						</ul>
					 </span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align:right;" title="">{{data.saleValue | INR}}</span>
					 <span class="col-xs-1 secondSpan secondSpanPadding" style="color: #000;text-align:right;" title="">&nbsp;</span>
					 </summary>
					 <table class=" table table-fixed">
					 <thead>
					    <tr>
					      <th class="col-xs-3">Quantity</th>
					      <th class="col-xs-3">Sale (Case/Btl)</th>
					      <th class="col-xs-3">In-HouseStock</th>
					      <th class="col-xs-2">Total Price</th>
					      <th class="col-xs-1">&nbsp;</th>
					    </tr>
					    </thead>
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true | filter: greaterThan('amount', 0)">
							<td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0" title="MRP {{brand.shopRate}}">{{brand.quantity}}</td>
						     <td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale" title="MRP {{brand.shopRate}}">
						     <p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
						     {{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-2" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						    <td class="col-xs-1" style="color: #000;border-top: 0px solid #ddd;" data-toggle="modal" data-target="#basicExampleModal" ng-click="openModalPopup(brand.brandNoPackQty,data.brandname,brand.quantity,brand.categoryId)"><i class="fa fa-line-chart"  style="color: #000; size: 12px; font-weight: bold;"></i></td>
						</tr></table>
					 </details>
			</details>
			<details ng-repeat="list in saleCategoryDataOnlySale | orderBy:'categoryOrder' | filter:filterCategoryDataBeerOnlySale">
				<summary>
					<div class="stockLiftList">
					    <span class="col-xs-1 borderBottomAllignment" style="color: #000;text-align: left;border-left: 5px solid {{list.bgcolor}};">&nbsp;</span>
					    <span class="col-xs-2 borderBottomAllignment" style="color: #000;text-align: left;">{{list.category}}
						<span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale < list.superComparePreviousDaySale">
						</span>
						<span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale > list.superComparePreviousDaySale">
						</span>
						<span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo == 0 && list.superCompareCurrentSale == list.superComparePreviousDaySale">
						</span>
						<span class="" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.compareNo != 0">&nbsp;
						</span>
						</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;">{{list.totalSaleAmount / totalPriceForPercentage * 100 | roundup}}%</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" ng-class="{'color-red': list.firstFontColor <= 1}">{{list.inhousestock | INR}}</span>
						<span class="col-xs-2 borderBottomAllignment" style="color: #000;text-align: right;">{{list.totalSaleAmount | INR}}</span>
						<span class="col-xs-1 borderBottomAllignment" style="color: #000;text-align: right;border-right: 5px solid {{list.bgcolor}};" title="">&nbsp;</span>
					</div>
				</summary>
				     <span class="col-xs-1 paddingAndInnerHeader" style="text-align: left;">&nbsp;</span>
				     <span class="col-xs-2 paddingAndInnerHeader" style="text-align: left;">Brand Name</span>
				     <span class="col-xs-2 paddingAndInnerHeader">%</span>
					 <span class="col-xs-2 paddingAndInnerHeader">Cases</span>
					 <span class="col-xs-2 paddingAndInnerHeader">In-HouseStock</span>
					 <span class="col-xs-2 paddingAndInnerHeader" style="text-align: right;">Total Price</span>
					 <span class="col-xs-1 paddingAndInnerHeader" style="text-align: right;">&nbsp;</span>
				<details ng-repeat="data in list.inputcategoryjson | orderBy:'saleValue':true | filter: greaterThan('totalbtlsale', 0)">
				   <summary>
				   <span class="col-xs-1 secondSpan secondSpanPadding" style="color: #000;text-align: left;background-color: #e8e8e8;">&nbsp;</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align: left;background-color: #e8e8e8;">
					 <span class="glyphicon glyphicon-question-sign" style="color: #000;text-align: right;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compairSaleSuper >= -20 &&  data.compairSaleSuper <= 20" title="Previous Day Sold: {{data.preCases}}, Current Day Sold: {{data.currCases}}"></span>
					 {{data.brandname}}
					 <span class=" glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale < data.comparePreviousDaySale">
					 </span>
					 <span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale > data.comparePreviousDaySale">
					 </span>
					 <span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.compareNo == 0 && data.compareCurrentSale == data.comparePreviousDaySale">
					 </span>
					 <span class="" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="data.compareNo != 0">&nbsp;
					 </span>
					  </span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="">{{data.saleValue / list.totalSaleAmount * 100 | roundup}}%</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan secondSpanPadding tip" style="overflow: visible;" ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouseCase | INR}}
					 <ul>
						<li ng-repeat="qty in data.Brands | orderBy:'order'">{{qty.packType}} : {{qty.caseStock}} / {{qty.closing}}</li>
					</ul>
					 </span>
					 <span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align:right;" title="">{{data.saleValue | INR}}</span>
					 <span class="col-xs-1 secondSpan secondSpanPadding" style="color: #000;text-align:right;" title="">&nbsp;</span>
					 </summary>
					 <table class=" table table-fixed">
					 <thead>
					    <tr>
					      <th class="col-xs-3">Quantity</th>
					      <th class="col-xs-3">Sale (Case/Btl)</th>
					      <th class="col-xs-3">In-HouseStock</th>
					      <th class="col-xs-2">Total Price</th>
					      <th class="col-xs-1">&nbsp;</th>
					    </tr>
					    </thead>
					     <tr ng-repeat="brand in data.Brands | orderBy:'amount':true | filter: greaterThan('amount', 0)">
							<td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo != 0" title="MRP {{brand.shopRate}}">{{brand.quantity}}</td>
						      <td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale < brand.compareTotalSale" title="MRP {{brand.shopRate}}">
						      <p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
						      {{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale > brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;"></p>
							</td>
							<td class="col-xs-3" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" ng-if="brand.compareNo == 0 && brand.totalSale == brand.compareTotalSale" title="MRP {{brand.shopRate}}">
							<p ng-if="brand.compairSale >= -20 && brand.compairSale <= 20" class="glyphicon glyphicon-question-sign" style="margin: 0px !important;color: #000;padding-right: 2px;" title="Previous Day Sold: {{brand.previousSaleInCase}}/{{brand.previousBtlVal}}, Current Day Sold: {{brand.caseVal}}/{{brand.btlVal}}"></p>
							{{brand.quantity}}
							<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;"></p>
							</td>
						    <td class="col-xs-3" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.caseVal}}/{{brand.btlVal}}">{{brand.caseVal}}/{{brand.btlVal}}</td>
						     <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseStock}}/{{brand.closing}}" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseStock}}/{{brand.closing}}</td>
						    <td class="col-xs-2" style="color: #000;border-top: 0px solid #ddd;" title="{{brand.amount}}" >{{brand.amount | INR}}</td>
						    <td class="col-xs-1" style="color: #000;border-top: 0px solid #ddd;" data-toggle="modal" data-target="#basicExampleModal" ng-click="openModalPopup(brand.brandNoPackQty,data.brandname,brand.quantity,brand.categoryId)"><i class="fa fa-line-chart"  style="color: #000; size: 12px; font-weight: bold;"></i></td>
						</tr></table>
					 </details>
			</details>
			</div>
		</div>
   </div>
    </div>
</div>
<div class="fixed">
			<div class="row">
			    <div class="col-sm-2" style="font-size: 12px;">
				<span>BEER Cases: </span><span> {{saleCategoryDataOnlySale | soldBeerCase}}</span>
				</div>
				<div class="col-sm-2" style="font-size: 12px;">
				<span>LIQUOR Cases: </span><span> {{saleCategoryDataOnlySale | soldLiquorCase}}</span>
				</div>
			    <div class="col-sm-2" style="font-size: 12px;">
				<span>Sale: </span><span> {{alldetails | totalAmount | INR}}</span>
				</div>
				<div class="col-sm-2" style="font-size: 12px;">
				<span>Expenses: </span><span> {{allExpenseAmt | INR}}</span>
				</div>
				<div class="col-sm-2" style="font-size: 12px;">
				<span>Card & Cash: </span><span> {{carAndCash | INR}}</span>
				</div>
				<div class="col-sm-2" style="font-size: 12px;">
				<span>Diff: </span><span> {{difference | INR}}</span>
				</div>
			</div>
		</div>
  </div>
  
  <div id="saleComparisionTab" role="tabpanel" class="tab-pane fade fadecss">
  <div class="row" style="margin-bottom: 5px;">
   <div class="col-sm-3 bodyFontCss"></div>
   <div class="col-sm-2 bodyFontCss">
   <label ng-click="showHideMonthAndWeekPicker('monthlyDate','weeklyDatePicker')"><input type="radio" name="weekormonth" value="1"> Select Month</label>
   <input type="text" class="monthpicker form-control" ng-model="monthlyDate" id="monthlyDate" readonly='true'>
   </div>
   <div class="col-sm-2 bodyFontCss" >
   <label ng-click="showHideMonthAndWeekPicker('weeklyDatePicker','monthlyDate')"><input type="radio" name="weekormonth" value="2" checked> Select Week</label>
   <input type="text" class="form-control" id="weeklyDatePicker">
   </div>
   <div class="col-sm-1 bodyFontCss customizedButton">
   <input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getWeekComparisionSaleData()" value="Get Data" />
   </div>
  <div class="col-sm-3 bodyFontCss"><!-- <label>{{showCurrentSelectedDate}}</label><br><label>{{showPreviousSelectedDate}}</label> --></div>
  <div class="col-sm-1 bodyFontCss" style="text-align: right;">
   <span><i class="fa fa-filter filter-icon" style="color: #243a51!important;" data-toggle="modal" data-target="#saleCompareFilterModal" aria-hidden="true"></i></span>
  </div>
  </div>
  <div class="row" style="margin-bottom: 20px;">
  <div id="waitsec" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
  <div class="col-sm-12 bodyFontCss">
  <div ng-show = "showcompareCompanyDetails">
		<span class="col-xs-2 paddingAndHeader" style="text-align: left;">Company</span>
		<span class="col-xs-2 paddingAndHeader">Current Cases</span>
		<span class="col-xs-2 paddingAndHeader">Previous Cases</span>
		<span class="col-xs-2 paddingAndHeader" title="{{showCurrentSelectedDate}}">Current Sale Amt.({{showCurrentSelectedDate}})</span>
		<span class="col-xs-2 paddingAndHeader" title="{{showPreviousSelectedDate}}">Previous Sale Amt.({{showPreviousSelectedDate}})</span>
		<span class="col-xs-2 paddingAndHeader">%</span>
		<div class="datalist">
		<h4 ng-if="!compareSaleData.length > 0" style="text-align: center;padding: 8px;">No Data Available</h4>
		  <details ng-repeat="list in companyWiseCompareSale | orderBy:'companyOrder'">
		  <summary>
			<div class="stockLiftList">
			<span class="col-xs-2 borderBottomAllignment" style="color: #000;text-align: left;border-left: 5px solid {{list.bgcolor}};">{{list.company}}
			<span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.superParentSaleInPer < 0"></span>
			<span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.superParentSaleInPer > 0"></span>
			<span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.superParentSaleInPer == 0"></span>
			</span>
			<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="{{list.currentStartDate}} To {{list.currentEndDate}}">{{list.totalCurrentSale}}</span>
			<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="{{list.previousSatrtDate}} To {{list.previousEndDate}}">{{list.totalPreviousSale}}</span>
			<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="{{list.currentStartDate}} To {{list.currentEndDate}}">{{list.totalcurrentsaleprice | INR}}</span>
			<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="{{list.previousSatrtDate}} To {{list.previousEndDate}}">{{list.totalprevioussaleprice | INR}}</span>
			<span class="col-xs-2 borderBottomAllignment" style="color: #000;border-right: 5px solid {{list.bgcolor}};" title="">{{list.superParentSaleInPer}}%</span>
			</div>
		</summary>
			<span class="col-xs-2 paddingAndInnerHeader" style="text-align: left;">Brand Name</span>
			<span class="col-xs-2 paddingAndInnerHeader">Current Cases</span>
			<span class="col-xs-2 paddingAndInnerHeader">Previous Case</span>
			<span class="col-xs-2 paddingAndInnerHeader">Current Sale Amt.</span>
			<span class="col-xs-2 paddingAndInnerHeader">Previous Sale Amt.</span>
			<span class="col-xs-2 paddingAndInnerHeader">%</span>
			<details ng-repeat="data in list.inputjson">
			<summary>
			<span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align: left;">{{data.brandname}}
			<span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.parentSaleInPer < 0"></span>
			<span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.parentSaleInPer > 0"></span>
			<span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.parentSaleInPer == 0"></span>
			</span>
			<span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="{{data.currentStartDate}} To {{data.currentEndDate}}">{{data.currentcases}}</span>
			<span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="{{data.previousSatrtDate}} To {{data.previousEndDate}}">{{data.previouscases}}</span>
			<span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="{{data.currentStartDate}} To {{data.currentEndDate}}">{{data.currentSalePrice | INR}}</span>
			<span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="{{data.previousSatrtDate}} To {{data.previousEndDate}}">{{data.previousSalePrice | INR}}</span>
			<span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;">{{data.parentSaleInPer}}%</span>
			</summary>
			<table class=" table table-fixed">
				<thead>
					<tr>
					<th class="col-xs-2">Quantity</th>
					<th class="col-xs-2">Current Sale (Case/Btl)</th>
					<th class="col-xs-2">Previous Sale (Case/Btl)</th>
					<th class="col-xs-2">Current Sale Amt.</th>
					<th class="col-xs-2">Previous Sale Amt.</th>
					<th class="col-xs-2">%</th>
					</tr>
				</thead>
				<tr ng-repeat="brand in data.Brands">
				<td class="col-xs-2" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;">{{brand.quantity}}
				<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;font-size: 10px;" ng-if="brand.saleInPercentage < 0"></p>
				<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;font-size: 10px;" ng-if="brand.saleInPercentage > 0"></p>
				<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;font-size: 10px;" ng-if="brand.saleInPercentage == 0"></p>
				</td>
				<td class="col-xs-2" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" title="{{brand.currentStartDate}} To {{brand.currentEndDate}}">{{brand.currentcaseVal}} / {{brand.currentbtlVal}}</td>
				<td class="col-xs-2" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" title="{{brand.previousSatrtDate}} To {{brand.previousEndDate}}">{{brand.previouscaseVal}} / {{brand.previousbtlVal}}</td>
				<td class="col-xs-2" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" title="{{brand.currentStartDate}} To {{brand.currentEndDate}}">{{brand.currentSalePrice | INR}}</td>
				<td class="col-xs-2" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" title="{{brand.previousSatrtDate}} To {{brand.previousEndDate}}">{{brand.previousSalePrice | INR}}</td>
				<td class="col-xs-2" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;">{{brand.saleInPercentage}}%</td>
				</tr>
				</table>
				</details>
			</details>
		</div>
  </div>
  
  <div ng-show = "showcompareCategoryDetails">
		<span class="col-xs-2 paddingAndHeader" style="text-align: left;">Category</span>
		<span class="col-xs-2 paddingAndHeader">Current Cases</span>
		<span class="col-xs-2 paddingAndHeader">Previous Cases</span>
		<span class="col-xs-2 paddingAndHeader" title="{{showCurrentSelectedDate}}">Current Sale Amt.({{showCurrentSelectedDate}})</span>
		<span class="col-xs-2 paddingAndHeader" title="{{showPreviousSelectedDate}}">Previous Sale Amt.({{showPreviousSelectedDate}})</span>
		<span class="col-xs-2 paddingAndHeader">%</span>
		<div class="datalist">
		  <details ng-repeat="list in categoryWiseCompareSale | orderBy:'categoryOrder'">
		  <summary>
			<div class="stockLiftList">
			<span class="col-xs-2 borderBottomAllignment" style="color: #000;text-align: left;border-left: 5px solid {{list.bgcolor}};" title="">{{list.category}}
			<span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.superParentSaleInPer < 0"></span>
			<span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.superParentSaleInPer > 0"></span>
			<span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 10px;overflow: initial;border: 0px;" ng-if="list.superParentSaleInPer == 0"></span>
			</span>
			<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="{{list.currentStartDate}} To {{list.currentEndDate}}">{{list.totalCurrentSale}}</span>
			<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="{{list.previousSatrtDate}} To {{list.previousEndDate}}">{{list.totalPreviousSale}}</span>
		    <span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="{{list.currentStartDate}} To {{list.currentEndDate}}">{{list.totalcurrentsaleprice | INR}}</span>
			<span class="col-xs-2 borderBottomAllignment" style="color: #000;" title="{{list.previousSatrtDate}} To {{list.previousEndDate}}">{{list.totalprevioussaleprice | INR}}</span>
		    <span class="col-xs-2 borderBottomAllignment" style="color: #000;border-right: 5px solid {{list.bgcolor}};" title="">{{list.superParentSaleInPer}}%</span>
			</div>
		</summary>
			<span class="col-xs-2 paddingAndInnerHeader" style="text-align: left;">Brand Name</span>
			<span class="col-xs-2 paddingAndInnerHeader">Current Cases</span>
			<span class="col-xs-2 paddingAndInnerHeader">Previous Case</span>
			<span class="col-xs-2 paddingAndInnerHeader">Current Sale Amt.</span>
			<span class="col-xs-2 paddingAndInnerHeader">Previous Sale Amt.</span>
			<span class="col-xs-2 paddingAndInnerHeader">%</span>
			<details ng-repeat="data in list.inputjson">
			<summary>
			<span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;text-align: left;">{{data.brandname}}
			<span class="glyphicon glyphicon-arrow-down" style="color: red;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.parentSaleInPer < 0"></span>
			<span class="glyphicon glyphicon-arrow-up" style="color: green;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.parentSaleInPer > 0"></span>
			<span class="glyphicon glyphicon-arrow-up" style="color: yellow;text-align: left;padding: 0px;font-size: 9px;overflow: initial;border: 0px;" ng-if="data.parentSaleInPer == 0"></span>
			</span>
			<span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="{{data.currentStartDate}} To {{data.currentEndDate}}">{{data.currentcases}}</span>
			<span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="{{data.previousSatrtDate}} To {{data.previousEndDate}}">{{data.previouscases}}</span>
			<span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="{{data.currentStartDate}} To {{data.currentEndDate}}">{{data.currentSalePrice | INR}}</span>
			<span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;" title="{{data.previousSatrtDate}} To {{data.previousEndDate}}">{{data.previousSalePrice | INR}}</span>
			<span class="col-xs-2 secondSpan secondSpanPadding" style="color: #000;">{{data.parentSaleInPer}}%</span>
			</summary>
			<table class=" table table-fixed">
				<thead>
					<tr>
					<th class="col-xs-2">Quantity</th>
					<th class="col-xs-2">Current Sale (Case/Btl)</th>
					<th class="col-xs-2">Previous Sale (Case/Btl)</th>
					<th class="col-xs-2">Current Sale Amt.</th>
					<th class="col-xs-2">Previous Sale Amt.</th>
					<th class="col-xs-2">%</th>
					</tr>
				</thead>
				<tr ng-repeat="brand in data.Brands">
				<td class="col-xs-2" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;">{{brand.quantity}}
				<p class="glyphicon glyphicon-arrow-down" style="margin: 0px !important;color: red;font-size: 10px;" ng-if="brand.saleInPercentage < 0"></p>
				<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: green;font-size: 10px;" ng-if="brand.saleInPercentage > 0"></p>
				<p class="glyphicon glyphicon-arrow-up" style="margin: 0px !important;color: yellow;font-size: 10px;" ng-if="brand.saleInPercentage == 0"></p>
				</td>
				<td class="col-xs-2" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" title="{{brand.currentStartDate}} To {{brand.currentEndDate}}">{{brand.currentcaseVal}} / {{brand.currentbtlVal}}</td>
				<td class="col-xs-2" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" title="{{brand.previousSatrtDate}} To {{brand.previousEndDate}}">{{brand.previouscaseVal}} / {{brand.previousbtlVal}}</td>
				<td class="col-xs-2" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" title="{{brand.currentStartDate}} To {{brand.currentEndDate}}">{{brand.currentSalePrice | INR}}</td>
				<td class="col-xs-2" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;" title="{{brand.previousSatrtDate}} To {{brand.previousEndDate}}">{{brand.previousSalePrice | INR}}</td>
				<td class="col-xs-2" style="line-height: 1 !important;color: #000;border-top: 0px solid #ddd;">{{brand.saleInPercentage}}%</td>
				</tr>
				</table>
				</details>
			</details>
		</div>
  </div>
  
  </div>
  </div>
  <div class="fixed">
			<div class="row">
			    <div class="col-sm-4" style="font-size: 12px;text-align: center;">
				<span>Current Sale Amt: </span><span> {{sumOfCurrentSaleAmt | INR}}</span>
				</div>
				<div class="col-sm-4" style="font-size: 12px;text-align: center;">
				<span>Previous Sale Amt: </span><span> {{sumOfPreviousSaleAmt | INR}}</span>
				</div>
			    <div class="col-sm-4" style="font-size: 12px;text-align: center;">
				<span> {{wholepercentage}}%</span>
				<span class="glyphicon glyphicon-arrow-down" style="color: red;" ng-if="wholepercentage < 0"></span>
			    <span class="glyphicon glyphicon-arrow-up" style="color: green;" ng-if="wholepercentage > 0"></span>
			    <span class="glyphicon glyphicon-arrow-up" style="color: yellow;" ng-if="wholepercentage == 0"></span>
				</div>
			</div>
		</div>
  </div>  
  
  
</div>  
</div>
     <!-- Modal -->
<div class="modal fade right" id="saleFilterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-side modal-top-right" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title w-100" id="exampleModalLabel"><i class="fa fa-filter" style="color: #243a51!important;font-size: 22px;"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="font-size: 25px;">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
       <div style="width: 100%;">
        <span style="float: right;padding: 12px;">
	    <label class="filterLabelFontSize">Only Sale Items</label>
	    </span>
	    <span style="float: right;display: inline-block;">
		<label class="switch">
	     <input type="checkbox" id="includeZeroSaleOrNot" checked>
	     <span class="slider round" style="background-color: red;" ng-click="includeZeroSaleOrNot()"></span>
	    </label>
	    <span style="float: left;padding: 12px;">
        <label class="filterLabelFontSize">Total Sale  Items</label>
	   </span>
		</span>
		</div>
		
		<div style="width: 100%;display: inline-block;">
		<span style="float: right;padding: 12px;">
	    <label class="filterLabelFontSize">Category</label>
	    </span>
	    <span style="float: right;">
		<label class="switch">
	     <input type="checkbox" id="catOrComp">
	     <span class="slider round" style="background-color: #062351;" ng-click="FilterCatOrComp()"></span>
	    </label>
	    <span style="float: left;padding: 12px;">
        <label class="filterLabelFontSize">Company</label>
	   </span>
		</span>
		<div>
	 </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
  </div>
	</div>
	<!-- Central Modal Small -->
<div class="modal fade" id="basicExampleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="true" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
         <input type="hidden" value="{{brandNoPackQty}}"/>
          <div class="col-sm-2 bodyFontCss">
			   Start Date<input type="text" class="form-control" id="popupstartDate" readonly='true' style="width: inherit;">
			   </div>
			   <div class="col-sm-2 bodyFontCss">
			   End Date<input type="text" class="form-control" id="popupendDate" readonly='true' style="width: inherit;">
			   </div>
			   <div class="col-sm-4 customizedButton">
			    <select class="selectpicker" id="listdata" title="Select Products For Comparision" data-live-search="true" multiple data-max-options="5">
			     </select>
			   </div>
			   <div class="col-sm-1 bodyFontCss customizedButton">
			   <input class="btn btn-default btn-rounded" type="submit" name="" ng-click="buildMultiLineGraph()" value="GET" />
			   </div>
			   <div class="col-sm-1 bodyFontCss customizedButton"></div>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true" style="font-size: 25px;">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div id="chart-container" style="width: 100%;height: 300px;"></div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default btn-rounded" data-dismiss="modal">CLOSE</button> -->
        </div>
      </div>
    </div>
  </div>
  <!-- Central Modal Small -->
      <!-- Modal -->
<div class="modal fade right" id="saleCompareFilterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-side modal-top-right" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title w-100" id="exampleModalLabel"><i class="fa fa-filter" style="color: #243a51!important;font-size: 22px;"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="font-size: 25px;">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
       <div style="width: 100%;">
        <span style="float: right;padding: 12px;">
	    <label class="filterLabelFontSize">Company</label>
	    </span>
	    <span style="float: right;display: inline-block;">
		<label class="switch">
	     <input type="checkbox" id="includeZeroCompareSaleOrNot" checked>
	     <span class="slider round" style="background-color: red;" ng-click="includeZeroCompareSaleOrNot()"></span>
	    </label>
	    <span style="float: left;padding: 12px;">
        <label class="filterLabelFontSize">Category</label>
	   </span>
		</span>
		</div>
	 </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
  </div>
	</div>
</body>
</html>