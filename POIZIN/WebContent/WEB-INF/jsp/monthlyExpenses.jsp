<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 <jsp:include page="/WEB-INF/jsp/header.jsp" /> 
  
 <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
 <script src="${jqueryuijs}"></script> 
 <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 <script src="https://rawgit.com/zorab47/jquery.ui.monthpicker/master/jquery.ui.monthpicker.js"></script>
 <spring:url value="/resources/css/select2.min.css" var="select2mincss"/>
 <link rel="stylesheet" href="${select2mincss}">
 <spring:url value="/resources/javascript/thirdpartyjs/select2.min.js" var="select2minjs"/>
 <script src="${select2minjs}"></script>	
 <spring:url value="/resources/javascript/monthlyExpenses.js" var="monthlyExpensesjs"/>
 <script src="${monthlyExpensesjs}"></script>
 <spring:url value="/resources/javascript/common.js" var="commonjs"/>
 <script src="${commonjs}"></script>
 <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
 <link rel="stylesheet" href="${newCommoncss}">
 <spring:url value="/resources/css/invoiceEntry.css" var="invoiceEntrycss"/>
 <link rel="stylesheet" href="${invoiceEntrycss}">
</head>
<body>
 <div class="row" style="padding-top: 100px;">
  <div class="col-sm-3 bodyFontCss">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Sale</li>
  <li>Monthly Expenses</li>
</ul>
  </div>
  <div class="col-sm-1 bodyFontCss"></div>
  <div class="col-sm-2 bodyFontCss">
   <label>Select Month</label>
   <input type="text" class="monthpicker form-control" id="monthlyExpensesDate" readonly='true'>
  </div>
  <div class="col-sm-2 bodyFontCss" style="padding-top: 15px;">
   <input class="fetchExistingExpenses btn btn-default btn-rounded" type="submit" name="" value="GET DATA" />
  </div>
  <div class="col-sm-4 bodyFontCss newInvoiceForm"></div>
</div>
 <div class="container">
<div class="row newMonthlyExpensesForm" style="margin-top: 20px;">
  <div class="col-sm-1 bodyFontCss">
  </div>
  <div class="col-sm-2 bodyFontCss">
   <label>Select Dedit/Credit</label>
    <select name="debitCredit" id="debitCredit" required="required" class="form-control">
	<option value="0">Debit</option>
	<option value="1">Credit</option>
  </select>
  </div>
  <div class="col-sm-2 bodyFontCss">
   <label>Amount</label >
   <input type="number" id="amount" name="amount" class=" form-control"  required>
  </div>
  <div class="col-sm-2 bodyFontCss">
   <label>Subject</label>
   <select id='subject' class=" form-control"></select>
  </div>
  <div class="col-sm-2 bodyFontCss">
   <label>Comment</label> 
   <textarea rows="1" class=" form-control" id="comment" name="comment" maxlength="150"></textarea>
  </div>
  <div class="col-sm-2 bodyFontCss" style="padding-top: 15px;">
  <input class="btn btn-default btn-rounded add-row" type="button" name="" value="ADD">
  </div>
  <div class="col-sm-1 bodyFontCss"></div>
</div>
 <div class="row newMonthlyExpensesForm">
   <div class="col-sm-12 bodyFontCss">
    <table id="ItemsTable">
        <thead>
            <tr style="color: #fff;background-color: #062351;">
                <th>Delete</th>
                <th>S No.</th>
                <th>Debit / Credit</th>
                <th>Amount</th>
                <th>Subject</th>
                <th>Comment</th>
            </tr>
        </thead>
         <tbody id="monthlyExpensesList">
        </tbody> 
    </table>
   </div>
  </div>
 <div class="row newMonthlyExpensesForm" style="float: right;padding-right: 20px;">
 <button type="button" class="btn btn-default btn-rounded" onclick="buildJsonForExpenses()" >SUBMIT</button>
 </div>
 </div>
</body>
</html>