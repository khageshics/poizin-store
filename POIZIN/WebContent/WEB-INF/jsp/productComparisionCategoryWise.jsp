<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<jsp:include page="/WEB-INF/jsp/header.jsp" /> 
  
<spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
<script src="${jqueryuijs}"></script> 
<!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">

<spring:url value="/resources/css/bootstrap-select.min.css" var="bootstrapselectmincss"/>
<link rel="stylesheet" href="${bootstrapselectmincss}">
<spring:url value="/resources/javascript/thirdpartyjs/bootstrap-select.min.js" var="bootstrapselectminjs"/>
<script src="${bootstrapselectminjs}"></script> 
  <spring:url value="/resources/javascript/fusioncharts/fusioncharts.js" var="fusionjs" />
<script src="${fusionjs}"></script>
<spring:url value="/resources/javascript/fusioncharts/themes/fusioncharts.theme.fint.js" var="fusionthemejs" />
<script src="${fusionthemejs}"></script>
<spring:url value="/resources/javascript/common.js" var="commonjs"/>
<script src="${commonjs}"></script> 
<spring:url value="/resources/javascript/productComparisionCategoryWise.js" var="productComparisionCategoryWisejs"/>
<script src="${productComparisionCategoryWisejs}"></script>
<spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
<link rel="stylesheet" href="${newCommoncss}">
<style type="text/css">
.rowcellheader li{
    list-style: none;
    line-height: 28px;
    border-bottom: 1px solid #e8e2e2;
    font-size: 12px;
    padding: 10px;
    text-align: center;
    /* color: #000; */
}
.rowcellheader:hover{
background: #243a51;
color: #fff;
}
select {
    display: block!important;
}
.container {
    max-width: 100%;
}
b, strong {
    font-weight: bold;
}
.striped {
    background: #ccc;
}
  </style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<div class="row" style="padding-top: 100px;">
  <div class="col-sm-3 bodyFontCss" style="padding: 0px; !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Reports</li>
  <li>Product Comparison</li>
</ul>
  </div>
  <!-- <div class="col-sm-1 bodyFontCss"></div> -->
   <div class="col-sm-2 bodyFontCss">
     <label>Start Date: </label><input type="text" class="form-control" id="startDate" readonly='true'>
    </div>
    <div class="col-sm-2 bodyFontCss">
     <label>End Date: </label><input type="text" class="form-control" id="endDate" readonly='true'>
    </div>
    <div class="col-sm-2 bodyFontCss">
     <label>Select Category</label>
     <select name="debitCredit" id="listdataSec" ng-model="dropValue" required="required" class="form-control"></select>
    </div>
    <div class="col-sm-1 bodyFontCss customizedButton">
    <label>&nbsp;</label><input class="btn btn-default btn-rounded" type="submit" name="" ng-click="generateReport()" value="GET DATA" />
    </div>
   <div class="col-sm-2 bodyFontCss"></div>
</div>
 <div class="container" style="margin-top: 20px;">
 <div class="row">
 
 <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
 
   <div class="col-sm-12 bodyFontCss">
        <div style="width: 100%;" class="container">
           <ul class="rowcellheader " style="text-align: left;float: left;width :20%;padding: 0px; ">
              <li style="background: #243a51;color: #fff;"><b>Brand Name</b></li>
               <li><b>Cases Sold</b></li>
               <li><b>Case Rate</b></li>
               <li><b>Investment</b></li>
               <li><b>Govt. Margin</b></li>
               <li><b>Comp. Discount</b></li>
               <li><b>Total Discount</b></li>
             </ul>
          <div style="float: left;width :80%;display: flex;flex-wrap: nowrap;overflow-x: auto;">
            <ul class="rowcellheader " ng-repeat="brand in performanceWithComparion | filter: greaterThan('casesSold', 0)" style="float: left;padding: 0px; flex: 0 0 auto;">
               <li style="background: #243a51;color: #fff;"><b>{{brand.brandName}}</b></li>
               <li>{{brand.casesSold}}</li>
               <li>{{brand.caseRate | INR}}</li>
               <li>{{brand.costPrice | INR}}</li>
               <li>{{brand.govtMargin | INR}}</li>
               <li>{{brand.companyDisc | INR}}</li>
               <li>{{brand.totalDisc | INR}}</li>
             </ul>
            </div>
          </div>
   </div>
   <div id="chart-container-casesold"  style="height: 600px;"></div>
   <div id="chart-container-totaldiscount"  style="height: 600px;"></div>
 </div>
 </div>   
</body>
</html>