<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
   <spring:url value="/resources/css/bootstrap.min.css" var="bootstrapmincss"/>
   <link rel="stylesheet" href="${bootstrapmincss}">
   <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  
  <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script> 
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
       <spring:url value="/resources/javascript/thirdpartyjs/angular.min.js" var="angularminjs"/>
  <script src="${angularminjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/angular-filter.js" var="angularfilterjs"/>
  <script src="${angularfilterjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-confirm.min.js" var="jqueryconfirmminjs"/>
  <script src="${jqueryconfirmminjs}"></script> 
<%--    <spring:url value="/resources/javascript/newBrand.js" var="newBrandjs"/>
  <script src="${newBrandjs}"></script>  --%>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/common.css" var="commoncss"/>
   <link rel="stylesheet" href="${commoncss}">
   <spring:url value="/resources/css/balanceSheet.css" var="balanceSheetcss"/>
  <link rel="stylesheet" href="${balanceSheetcss}">
   <script type="text/javascript">
   var app = angular.module('myApp', []);
   app.controller('myCtrl', function($scope, $http) {
		var myUrl = window.location.href;
		var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
		var productListUrl = url+"getBrandDetailsForProductOrder.json";
		$http({
		    method : "GET",
		    url : productListUrl
		  }).then(function mySuccess(response) {
			  $scope.alldetails = response.data;
		  }, function myError(response) {
		    //alert(response);
		  });
   });
 </script>
 <style>
 .evenrow{
	background-color:#81DAF5!important;
}

.oddrow{
	background-color:#E6E6E6!important;
}
 </style>
</head>
<body>

 <div id="main" ng-app="myApp" ng-controller="myCtrl">

		<nav class="navbar navbar-inverse" class="headernav" style="height:120px;background:#062351;border-radius:0px;">
		<label style="float: right;color: #fff;padding: 10px 10px 0px 0px;">PRODUCT ORDER BY</label>
		<div class="container-fluid" style="padding-right: 200px;">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">
				<img src="resources/images/logopoizin.png" /></a>
			</div>
			<ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
                         <c:if test="${sessionScope['scopedTarget.userSession'].loginId > 0}">
						<a class="dropbtn" style="padding-top: 70px;" href="./logout">Logout</a>
					</c:if>
					</li>
					</ul>
		</div>
		</nav>
		<div id="content_header"></div>
    <div id="site_content">
     <div id="content">
        <!-- insert the page content here -->
        <div class="row ">
  <!-- <div class="col-lg-2">
  </div> -->
  <div class="col-lg-4">
        <div class="form_settings">
		  <input type="hidden" name="popupmsg" id="popupmsg" value="${message}"  /> 
		 <form action="./addOrderForAProduct" class="form_span" id="" method="post">
              <label>Brand Number</label > <input type="number" placeholder="Ex. 1234" id="brandNo" name="brandNo" class="form-control"  required>
              <p><label>Select No. Of Bottles In A Case</label><select name="packQty" id="packQty" required="required" class="form-control">
			   <option value="" rval="">Select No. Of Bottles In A Case</option>
			   <option value="6">6</option>
			    <option value="4">4</option>
			    <option value="9">9</option>
			   <option value="12">12</option>
			   <option value="24">24</option>
			   <option value="48">48</option>
			   <option value="96">96</option>
			   </select></p>
              <p><label>Order Value</label><input type="number" placeholder="Ex. Name" id="orderValue" name="orderValue" class="form-control" required></p>
              
			   <input class="btn btn-primary btn-md" type="submit" name="" value="Submit" style="float: right;"/>
			   </form>
          </div>
          </div>
          <div class="col-lg-8">
               <div class="container">
			 <div class="panel panel-default">
							        
						    <table id="" class=" fixed_headers table table-fixed" >
						    <thead><tr>
						    <th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Brand No">Product ID</th>
						    <th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Brand Name">Order No.</th>
						    <th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Company">Brand No </th>
						    <th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Category">Pack Qty</th>
						    <th class="col-xs-4" data-toggle="tooltip" data-placement="bottom" title="Pack Type">Brand Name</th>
						    </tr></thead>
						    <tr ng-repeat="list in alldetails">
							<td class="col-xs-2"  ng-class-odd="'oddrow'" title="{{list.productId}}">{{list.productId}}</td>
							<td class="col-xs-2"  ng-class-odd="'oddrow'"  title="{{list.match}}">{{list.match}}</td>
							<td class="col-xs-2"  ng-class-odd="'oddrow'"  title="{{list.brandNo}}">{{list.brandNo}}</td>
							<td class="col-xs-2"  ng-class-odd="'oddrow'" title="{{list.packQty}}">{{list.packQty}}</td>
							<td class="col-xs-4"  ng-class-odd="'oddrow'" title="{{list.brandName}}">{{list.brandName}}</td>
							</tr>
						  </table>
						   </div>
						  <!-- </div> -->
						</div>
          
          
          </div>
</div>
            
      </div>
    </div>
  </div>
</body>
</html>