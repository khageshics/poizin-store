<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">  -->  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <!--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css"> -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <spring:url value="/resources/javascript/editMrp.js" var="editMrpjs"/>
  <script src="${editMrpjs}"></script> 
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/common.css" var="commoncss"/>
   <link rel="stylesheet" href="${commoncss}">
  <style>
 table
{ /* margin: 10px 0 30px 0; */
        width: 100%;
        /* margin-bottom: 20px; */
		border-collapse: collapse;
}

table tr th, table tr td
{ background: #062351;
  color: #FFF;
  padding: 7px 4px;
  text-align: left;}
  
table tr td
{ background: #E5E5DB;
  color: #47433F;
  border-top: 1px solid #FFF;
  }
.saledetailreport {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    /* border-collapse: collapse;
    width: 99%; */
}

.saledetailreport td, .saledetailreport th {
   /*  border: 1px solid #ddd; */
    padding: 8px;
        text-align: center;
}

.saledetailreport tr:nth-child(even){background-color: #f2f2f2;}

.saledetailreport tr:hover {background-color: #ddd;}

.saledetailreport th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: center;
    background-color: #062351;
    color: white;
}
input, textarea,select
{ padding: 10px;
    width: 90px;
    font: 100% arial;
    border: 1px solid #E5E5DB;
    background: #FFF;
    color: #47433F;
    border-radius: 5px;
    vertical-align: middle;
    }
  
 .submit
{ font: 100% arial; 
  border: 0; 
  width: 99px; 
  height: 28px;
  padding: 2px 0 3px 0;
  cursor: pointer; 
  color: #fff;
  background-color: #337ab7;
  border-color: #2e6da4;
      margin: 10px;}
</style>
</head>
<body>

 <div id="main">

		<nav class="navbar navbar-inverse" class="headernav" style="height:120px;background:#062351;border-radius:0px;">
		<div class="container-fluid" style="padding-right: 200px;">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">
				<img src="resources/images/logopoizin.png" /></a>
			</div>
			<ul class="nav navbar-nav navbar-right">
			<li class="dropdown">
                         <c:if test="${sessionScope['scopedTarget.userSession'].loginId > 0}">
						<a class="dropbtn" style="padding-top: 70px;" href="./logout">Logout</a>
					</c:if>
					</li>
			</ul>
			
		</div>
		</nav> 
		<div id="content_header"></div>
    <div id="site_content">
     <div id="content">
        <!-- insert the page content here -->
        <div class="row ">
  <div class="col-lg-12">
  		<div class="form_settings">
     <input type="text" style="width: 20%;margin-right: 5px;" placeholder="Select Date" id="datepicker" name="invoiceDate" readonly="readonly" required><input class=" submit btn-primary" onclick="getDetailsforEditMrp();" type="submit" name="" value="Get Data" />
     </div>
     <div class="totalSaleItmes">
    <form action="" id="saleFormSecond" method="post" onsubmit="return saveTotalSaleItems(this);">
    <table id="saleSecDetails" class="saledetailreport">
   
  </table>
 <p style="text-align: center;"><input class="saveDetail submit" type="submit" name="" value="Save Details" /></p>
  <div class="ttlmrp"></div>
    </form>
   </div> 
     
     
  
  </div>
  
</div>
            
      </div>
    </div>
  </div>
</body>
</html>