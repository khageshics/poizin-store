<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<jsp:include page="/WEB-INF/jsp/header.jsp" /> 
  
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
  <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">	 -->
  <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
  
 <spring:url value="/resources/javascript/invoice.js" var="invoicejs"/>
   <spring:url value="/resources/javascript/profitAndLossReport.js" var="profitAndLossReportjs"/>
  <script src="${profitAndLossReportjs}"></script> 
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script>  
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
  <link rel="stylesheet" href="${newCommoncss}">
  <spring:url value="/resources/css/profitOrLoss.css" var="profitOrLosscss"/>
  <link rel="stylesheet" href="${profitOrLosscss}">
</head>
<body ng-app="myApp" ng-controller="myCtrl">
 <div class="row" style="padding-top: 100px;">
   <div class="col-sm-3 bodyFontCss" style="padding: 0px !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Discount</li>
  <li>Profit/Loss Report</li>
</ul>
  </div>
   <div class="col-sm-2 bodyFontCss">
     <label>Satrt Date: </label><input type="text" class="form-control" id="startDate" readonly='true'>
    </div>
    <div class="col-sm-2 bodyFontCss">
     <label>End Date: </label><input type="text" class="form-control" id="endDate" readonly='true'>
    </div>
    <div class="col-sm-2 bodyFontCss" style="padding-top: 12px;">
    <label>&nbsp;</label><input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getResults('startDate','endDate')" value="GET DATA" />
    </div>
   <div class="col-sm-3 bodyFontCss"></div>
</div>
<div class="container" style="margin-top: 20px;">
<div class="row" style="margin-top: 20px;">
   <div class="col-sm-2 bodyFontCss"></div>
   <div class="col-sm-8 bodyFontCss">
   <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
     <table id="receivedDiscount">
		    <thead>
		      <tr>
		        <th class="cssForth">Account</th>
		        <th class="cssForth">Debits</th>
		        <th class="cssForth">Credits</th>
		      </tr>
		    </thead>
		    <tbody>
		    <tr>
		     <th class="thcss" style="text-align: left;">OP. STOCK - VALUE</th>
		     <th class="thcss" style="text-align: right;">{{profitAndLoss.openingStock | INR}}</th>
		     <th class="thcss"></th>
		     </tr>
		     <tr>
		     <th class="thcss" style="text-align: left;">CL. STOCK - VALUE</th>
		     <th class="thcss"></th>
		     <th class="thcss" style="text-align: right;">{{profitAndLoss.closingStock | INR}}</th>
		    </tr>
		    <tr>
		     <th class="thcss" style="text-align: left;">SALES</th>
		     <th class="thcss"></th>
		     <th class="thcss" style="text-align: right;">{{profitAndLoss.sales | INR}}</th>
		    </tr>
		    <tr>
		     <th class="thcss" style="text-align: left;"> PURCHASE A/C</th>
		     <th class="thcss" style="text-align: right;">{{(profitAndLoss.mrpRoundOff + profitAndLoss.purchase + profitAndLoss.tcsAmt + profitAndLoss.turnOverTax) | INR}}</th>
		     <th class="thcss"></th>
		    </tr>
		    <tr>
		     <td class="thcss" style="text-align: left;padding-left: 12px">MRP Roundinf Up </td>
		     <td class="thcss" style="text-align: right;">{{profitAndLoss.mrpRoundOff | INR}}</td>
		     <td class="thcss"></td>
		    </tr>
		    <tr>
		     <td class="thcss" style="text-align: left;padding-left: 12px">TCS </td>
		     <td class="thcss" style="text-align: right;">{{profitAndLoss.tcsAmt | INR}}</td>
		     <td class="thcss"></td>
		    </tr>
		    <tr ng-show="profitAndLoss.turnOverTax > 0">
		     <td class="thcss" style="text-align: left;padding-left: 12px">EXCISE TUROVER TAX </td>
		     <td class="thcss" style="text-align: right;">{{profitAndLoss.turnOverTax | INR}}</td>
		     <td class="thcss"></td>
		    </tr>
		    <tr>
		     <td class="thcss" style="text-align: left;padding-left: 12px">PURCHASE </td>
		     <td class="thcss" style="text-align: right;">{{profitAndLoss.purchase | INR}}</td>
		     <td class="thcss"></td>
		    </tr>
		    <tr>
		     <th class="thcss" style="text-align: left;">ADMINISTRATION EXPENCES</th>
		     <th class="thcss" style="text-align: right;">{{totalAdministration | INR}}</th>
		     <th class="thcss"></th>
		    </tr>
		    <!-- <tr ng-show="profitAndLoss.receivedAmt > 0">
		     <td class="thcss" style="text-align: left;padding-left: 12px">INCENTIVES</td>
		     <td class="thcss" style="text-align: right;"></td>
		     <td class="thcss">{{profitAndLoss.receivedAmt | INR}}</td>
		    </tr> -->
		    <!-- <tr ng-show="profitAndLoss.sittingAmt > 0">
		     <td class="thcss" style="text-align: left;padding-left: 12px">SITTING RENT </td>
		     <td class="thcss" style="text-align: right;"></td>
		     <td class="thcss" style="text-align: right;">{{profitAndLoss.sittingAmt | INR}}</td>
		    </tr> -->
		    <tr ng-show="profitAndLoss.breackage > 0">
		     <td class="thcss" style="text-align: left;padding-left: 12px">BREKAGE</td>
		     <td class="thcss" style="text-align: right;">{{profitAndLoss.breackage | INR}}</td>
		     <td class="thcss"></td>
		    </tr>
		    <tr ng-repeat="list in profitAndLoss.administrationExpensesBean">
		     <td class="thcss" style="text-align: left;padding-left: 12px">{{list.name}} </td>
		     <td class="thcss" style="text-align: right;">{{list.value | INR}}</td>
		     <td class="thcss"></td>
		    </tr>
		    <tr ng-show="profitAndLoss.monthlyExpenses > 0">
		     <th class="thcss" style="text-align: left;padding-left: 12px">MONTHLY EXPENSES</th>
		     <th class="thcss" style="text-align: right;">{{profitAndLoss.monthlyExpenses | INR}}</th>
		     <th class="thcss"></th>
		    </tr>
		    <tr ng-show="profitAndLoss.licenseFee > 0">
		     <th class="thcss" style="text-align: left;padding-left: 12px">LICENCE FEE</th>
		     <th class="thcss" style="text-align: right;">{{profitAndLoss.licenseFee | INR}}</th>
		     <th class="thcss"></th>
		    </tr>
		    <tr>
		     <th class="thcss" style="text-align: left;" ng-show="profitForthePeriod > 0">PROFIT</th>
		      <th class="thcss" style="text-align: left;" ng-show="lossForthePeriod > 0">LOSS</th>
		     <th class="thcss" style="text-align: right;">{{profitForthePeriod | INR}}</th>
		     <th class="thcss" style="text-align: right;">{{lossForthePeriod | INR}}</th>
		    </tr>
		    <tr>
		     <td class="thcss" style="text-align: left;">TOTAL (RUPEES)</td>
		     <td class="thcss" style="text-align: right;">{{totalDebitRupees | INR}}</td>
		     <td class="thcss">{{totalCreditRupees | INR}}</td>
		    </tr>
		    </tbody>
		  </table> 
   </div>
   <div class="col-sm-2 bodyFontCss"></div>
</div>
</div>
</body>
</html>