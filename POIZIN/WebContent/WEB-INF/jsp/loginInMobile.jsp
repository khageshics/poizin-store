<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   <spring:url value="/resources/css/mobileViewInhouseStock.css" var="mobileViewInhouseStockcss"/>
   <link rel="stylesheet" href="${mobileViewInhouseStockcss}">
  <script src="https://www.w3schools.com/lib/w3data.js"></script>
  <script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-filter/0.5.4/angular-filter.js"></script>
  <script src="js/common.js" type="text/javascript"></script>
  <style>
* {
    box-sizing: border-box;
}

body {
    font-family: Arial, Helvetica, sans-serif;
}

/* Style the header */
header {
    background-color: #062351;
    padding: 2px;
    text-align: center;
    font-size: 35px;
    color: white;
}

@media (max-width: 600px) {
    nav, article {
        width: 100%;
        height: auto;
    }
}
@font-face {
  font-family: "GothamRoundedBook";
  src: url(../assets/GothamRoundedBook_21018.ttf) format("truetype");
}
h1, h2, h3, h4, h5, h6 {
    font-family: "Segoe UI",Arial,sans-serif;
    font-weight: 400;
    margin: 10px 0;
}
h6 {
    font-size: 16px;
}
</style>
</head>

<body  ng-app="myApp" ng-controller="myCtrl">
<a href="mobileViewHome" style="text-decoration: none;">
<header style="height: 55px;">
  <h2 style="float:left;padding: 0px 0px 0px 2px;">POIZIN</h2>
  <h6 style="float: right;padding: 8px 2px 0px 0px;">LOGIN</h6>
</header></a>
<div class="container">
  <div style="margin-top: 50px;">
   <div class="row">
  <div class="col-sm-4"></div>
  <div class="col-sm-4">
  <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold homeModelHeader">POIZIN</h4>
      </div>
     <label for="defaultForm-pass" style="color: red;padding: 10px 0px 0px 20px;font-size: 10px;">${message}</label>
      <form action="./appUserLogin" class="form_span" id="" method="post">
      <div class="modal-body mx-3">
       
        <div class="md-form mb-4">
        <label for="defaultForm-pass" style="font-size: 10px;color: #243a51 !important;">Enter Your Admin ID</label>
         <input type="password" id="userid" name="userId" class="form-control">
         
        </div>

      </div>
      <div class="modal-footer d-flex justify-content-center">
        <div style="text-align: center;">
        <input class="btn btn-default btn-rounded" style=" background-color: #243a51 !important;padding: 8px 25px 8px 25px;font-size: 12px;color: #fff;" type="submit" name="" value="Login"/>
        </div>
      
      </div>
       </form>
    </div>
  
  </div>
  <div class="col-sm-4"></div>
  </div>

</div>
</div>
</body>
</html>