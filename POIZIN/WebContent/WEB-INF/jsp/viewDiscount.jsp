<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
<jsp:include page="/WEB-INF/jsp/header.jsp" /> 
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
 <!--   <script src="https://rawgit.com/zorab47/jquery.ui.monthpicker/master/jquery.ui.monthpicker.js"></script> -->
 <spring:url value="/resources/javascript/thirdpartyjs/jquery.ui.monthpicker.js" var="jqueryuimonthpickerjs"/>
  <script src="${jqueryuimonthpickerjs}"></script>
 <!--  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">	 -->
 <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
   
 <spring:url value="/resources/javascript/invoice.js" var="invoicejs"/>
   <spring:url value="/resources/javascript/viewDiscount.js" var="viewDiscountjs"/>
  <script src="${viewDiscountjs}"></script> 
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script>  
  <spring:url value="/resources/javascript/app.js" var="appjs"/>
  <script src="${appjs}"></script>
  <spring:url value="/resources/javascript/test.js" var="testjs"/>
  <script src="${testjs}"></script>
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
  <link rel="stylesheet" href="${newCommoncss}">
   <spring:url value="/resources/css/balanceSheet.css" var="balanceSheetcss"/>
   <link rel="stylesheet" href="${balanceSheetcss}">
     <spring:url value="/resources/css/viewDiscount.css" var="viewDiscountcss"/>
   <link rel="stylesheet" href="${viewDiscountcss}">
 <style type="text/css">
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 15px;
}
.row {
    margin-right: 0px;
    margin-left: 0px;
}
 summary {
    display: block;
} 
    </style>
</head>
<body ng-app="myApp" ng-controller="Ctrl1">
 <div class="row" style="padding-top: 100px;">
   <div class="col-sm-3 bodyFontCss" style="padding: 0px !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Reports</li>
  <li>Company Discount</li>
</ul>
  </div>
  <div class="col-sm-1 bodyFontCss">
   </div>
   <div class="col-sm-2 bodyFontCss">
     <label style="font-weight: 700;">Start Month: </label><input type="text" class="form-control" id="startMonth" readonly='true'>
    </div>
    <div class="col-sm-2 bodyFontCss">
     <label style="font-weight: 700;">End Month: </label><input type="text" class="form-control" id="endMonth" readonly='true'>
    </div>
    <div class="col-sm-2 bodyFontCss" style="padding-top: 12px;">
    <label>&nbsp;</label><input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getResults('startDate','endDate')" value="GET DATA" />
    </div>
   <div class="col-sm-2 bodyFontCss">
   </div>
</div>
<div class="">

<div class="row" style="margin-top: 20px;">
<div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>

   <div class="col-sm-3 bodyFontCss">
    <div class="datalist" style="margin-bottom: 10px;">
		  <label ng-repeat="role in discountData" class="container">
	     <input type="checkbox" checklist-model="user.discountData" checklist-value="role" > {{role.company}}
	     <span class="checkmark"></span></label>
        </div>
     
     <button class="btn btn-default btn-rounded" ng-click="checkAll()" style="margin-right: 10px">Check all</button>
          <button class="btn btn-default btn-rounded" ng-click="uncheckAll()" style="margin-right: 10px">Uncheck all</button>
        
          <button ng-click="setToNull()" ng-if="setToNull">Set to null</button>
   </div>
   <div class="col-sm-9 bodyFontCss">
                    <div class="stockLiftList">
						<span class="col-xs-3" style="padding: 8px;text-align: left;background-color: #243a51;color: #fff;">Company</span>
						<span class="col-xs-3" style="padding: 8px;text-align: right;background-color: #243a51;color: #fff;" ng-click="sort('totalDiscount')">Discount + Rental
						<p class="glyphicon sort-icon" style="color: #fff;margin-bottom: 0px;" ng-show="sortKey=='totalDiscount'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></p>
						</span>
						<span class="col-xs-3" style="padding: 8px;text-align: right;background-color: #243a51;color: #fff;">Vendor Disc.</span>
						<span class="col-xs-3" style="padding: 8px;text-align: right;background-color: #243a51;color: #fff;" ng-click="sort('duesAmt')">Arrears
						<p class="glyphicon sort-icon" style="color: #fff;margin-bottom: 0px;" ng-show="sortKey=='duesAmt'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></p>
						</span>
					</div>
			<div class="datalist">
              <details ng-repeat="list in user.discountData | filter: greaterThanTopCheck('totalDiscount','duesAmt', 0) | orderBy:sortKey:reverse">
				<summary>
					<div class="stockLiftList">
						<span class="col-xs-3" style="padding: 8px;text-align: left;border-bottom: solid 1px #ccc;" title="{{list.company}}"><p ng-if="list.notReceived > 0" style="display: initial;color: red;">*</p>{{list.company}}</span>
						<span class="col-xs-3" style="padding: 8px;text-align: right;border-bottom: solid 1px #ccc;" title="{{list.duesAmt}}">{{list.totalDiscount | INR}}</span>
						<span class="col-xs-3" style="padding: 8px;text-align: right;border-bottom: solid 1px #ccc;" title="{{list.totalVendorAmount}}">{{list.totalVendorAmount | INR}}</span>
						<span class="col-xs-3" style="padding: 8px;text-align: right;border-bottom: solid 1px #ccc;" title="{{list.duesAmt}}">{{list.duesAmt | INR}}</span>
					</div>
				</summary>
					 <table class=" table table-fixed" ng-if="list.notClearCheckBean.length > 0">
						<thead>
							<tr><th class="col-xs-3 fontfamily" style="background-color: #ec8989;">Month</th><th class="col-xs-3 fontfamily" style="background-color: #ec8989;">Check No</th><th class="col-xs-3 fontfamily" style="background-color: #ec8989;">Amount</th> <th class="col-xs-3 fontfamily" style="background-color: #ec8989;">Not Clear</th></tr>
						</thead>
						<tr ng-repeat="notClear in list.notClearCheckBean">
						<td class="col-xs-3 fontfamily">{{notClear.month}}</td><td class="col-xs-3 fontfamily">{{notClear.checkNo}}</td>
						<td class="col-xs-3 fontfamily">{{notClear.amount | INR}}</td>
						<td class="col-xs-3 fontfamily"><input type="checkbox" id="{{$index}}{{$parent.$index}}addOrRemoveCheck" ng-click="addOrRemoveNotClearCheck($index,$parent.$index,notClear.amount,list.duesAmt,list.company,notClear)" style="position: inherit;opacity: 1;pointer-events: auto;"></td>
						</tr>
					</table>
					<table class=" table table-fixed">
				 <thead><tr>
					<th class="col-xs-2 fontfamily" style="text-align: left;" data-toggle="tooltip" data-placement="bottom" title="Company" style="text-align: left;">Month</th>
					<th class="col-xs-2 fontfamily" style="text-align: right;" data-toggle="tooltip" data-placement="bottom" title="Cheque">Discount</th>
					<th class="col-xs-2 fontfamily" style="text-align: right;" data-toggle="tooltip" data-placement="bottom" title="Rental">Rental</th>
					<th class="col-xs-2 fontfamily" style="text-align: right;" data-toggle="tooltip" data-placement="bottom" title="Comment">Cheque</th>
					<th class="col-xs-2 fontfamily" style="text-align: right;" data-toggle="tooltip" data-placement="bottom" title="Comment">Vendor Disc</th>
					<th class="col-xs-2 fontfamily" style="text-align: right;" data-toggle="tooltip" data-placement="bottom" title="Comment">Arrears</th>
					</tr></thead>
				     <tr ng-repeat="brand in list.mobileViewStockDiscountInner | filter: greaterThanZeroAllCompany('discount','recieved','arrears', 0)" data-toggle="modal" data-target="#myModalbacardiCompany" ng-click="openListDiscontsAllCompany(brand.discountAsPerCompanyBeenList,brand.date,brand.companyId,brand.realDate,brand.discount,brand.rentals,brand.arrears,brand.creditAmt)">
						<td class="col-xs-2 fontfamily" style="border-top: 0px solid #ddd;text-align: left;" title="{{brand.date}}">{{brand.date}}</td>
					    <td class="col-xs-2 fontfamily" style="border-top: 0px solid #ddd;text-align: right;"  title="{{brand.discount}}">{{brand.discount | INR}}</td>
					    <td class="col-xs-2 fontfamily" style="border-top: 0px solid #ddd;text-align: right;"  title="{{brand.rentals}}">{{brand.rentals | INR}}</td>
					    <td class="col-xs-2 fontfamily" style="border-top: 0px solid #ddd;text-align: right;" title="{{brand.recieved}}">{{brand.recieved | INR}}</td>
					    <td class="col-xs-2 fontfamily" style="border-top: 0px solid #ddd;text-align: right;" title="{{brand.comment}}">{{brand.vendorAmount | INR}}</td>
					    <td class="col-xs-2 fontfamily" style="border-top: 0px solid #ddd;text-align: right;" title="{{brand.arrears}}">{{brand.arrears | INR}}</td>
					</tr></table>
	       </details>
          </div>
   </div>
  <!--  <div class="col-sm-2 bodyFontCss"></div> -->
</div>
</div>

<div class="modal fade sbi2-popup-styles" id="myModalbacardiCompany" role="dialog">
	    <div class="modal-dialog modal-lg">
	      <div class="modal-content">
	        <div class="modal-header">
	           <h4 class="modal-title"><span>Date: {{date}} | Discount: {{ttlDiscount | INR}} |  Rental: {{rentalsAmt | INR}} | Arrears: {{aarears | INR}}</span>
	           <span ng-click=getvendorcreditdetails()> | Vendor credit: {{creditAmt | INR}}</span>
	           </h4> 
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        </div>
	        <div class="modal-body" style="overflow: auto;height: 475px;">
	        
		    <div class="stockLiftSpan" style="height:30px;padding: 5px;border-bottom: 1px solid #e0e0e0;">
						    <span class="col-xs-4" style="text-align: left;">Brand Name</span>
						    <span class="col-xs-4" style="text-align: center;">Lifted Case</span>
						    <span class="col-xs-4" style="text-align: center;">Discount</span>
						</div>
				<details ng-repeat="list in tempData | filter: greaterThan('totalCaseQty', 0)" ng-class="$even ? 'oddrow' : 'evenrow'">
					<summary>
						<div class="stockLiftList shiva" style="height:30px;padding: 5px;border-bottom: 1px solid #e0e0e0;">
						    <span class="col-xs-4" style="color: #000;text-align: left;">{{list.productName}}</span>
						    <span class="col-xs-4" style="color: #000;text-align: center;">{{list.totalCaseQty}}</span>
						    <span class="col-xs-4" style="color: #000;text-align: center;">{{list.totalDiscount | INR}}</span>
						</div>
					</summary>
					<table class=" table table-fixed">
						   <thead><tr>
						    <th class="col-xs-3" style="background: #1a6398 !important;">Brand Name</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;">Brand No</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;">Lifted Case</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;">Discount</th>
						    <th class="col-xs-3" style="background: #1a6398 !important;">Total Discount</th>
						    </tr></thead>
						    <tr ng-repeat="brand in list.Brands | filter: greaterThan('caseQty', 0)"">
							<td class="col-xs-3" style="color: #000;">{{brand.brandName}}</td>
							<td class="col-xs-2" style="color: #000;">{{brand.brandNo}}</td>
							<td class="col-xs-2" style="color: #000;">{{brand.caseQty}}</td>
							<td class="col-xs-2" style="color: #000;">{{brand.unitPrice}}</td>
							<td class="col-xs-3" style="color: #000;">{{brand.totalPrice | INR}}</td>
							</tr>
						  </table>
					</details>
		    <br>
		    <table class=" table table-fixed" >
			  <thead><tr>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Company" style="text-align: left;">Bank</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Total Discount">Tr. Type</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Cheque">Amount</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Cheque">Tr. Date</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Comment">ADJ Amt</th>
				<th class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Arrears">Image</th>
				</tr></thead>
				<tr ng-repeat="discountlist in discountBeen">
				<td class="col-xs-2 tdClasscss" style="text-align: left;border-bottom: 1px solid #e8e8e8;" title="{{discountlist.bank}}">{{discountlist.bank}}</td>
				<td class="col-xs-2 tdClasscss" style="border-bottom: 1px solid #e8e8e8;" title="{{discountlist.transactionType}}">{{discountlist.transactionType}}</td>
				<td class="col-xs-2 tdClasscss" style="border-bottom: 1px solid #e8e8e8;" title="{{discountlist.transactionAmt | INR}}">{{discountlist.transactionAmt | INR}}</td>
				<td class="col-xs-2 tdClasscss" style="border-bottom: 1px solid #e8e8e8;" title="{{discountlist.transactionDate}}">{{discountlist.transactionDate}}</td>
				<td class="col-xs-2 tdClasscss" style="border-bottom: 1px solid #e8e8e8;" title="{{discountlist.adjCheck}}">{{discountlist.adjCheck}}</td>
				<td class="col-xs-2 glyphicon glyphicon-picture" data-toggle="modal" data-target="#myModalForImage" ng-click="openChekImg(discountlist.ngalleryImage,discountlist.imageName)" style="color:#000;border-bottom: 1px solid #e8e8e8;">
				</tr>	
		    </table>
	        
	        </div>
	      </div>
	    </div>
  </div>
<!-- image popup open -->
		<div class="modal fade sbi2-popup-styles" id="myModalForImage" role="dialog">
	    <div class="modal-dialog modal-lg">
	      <div class="modal-content">
	        <div class="modal-header">
	          <a style="font-size: 16px;" href="downloadChecks?imagePath={{imageName}}"><span class="glyphicon glyphicon-download-alt"></span></a>
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
     	    </div>
	        <div class="modal-body">
	        <img src= "{{image}}" style="width:100%;height:100%;">
	        </div>
	      </div>
	    </div>
  </div>
		<!-- image popup close -->
<!-- vendor credit model popup start -->		
<div class="modal fade sbi2-popup-styles" id="vendorCreditModelPopup" role="dialog">
	    <div class="modal-dialog modal-lg">
	      <div class="modal-content" style="background: #e4e4fb;">
	        <div class="modal-header">
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
     	    </div>
	        <div class="modal-body">
	          <div class="stockLiftSpan" style="height:30px;padding: 5px;border-bottom: 1px solid #e0e0e0;">
					<span class="col-xs-3" style="text-align: left;">Vendor Name</span>
				    <span class="col-xs-3" style="text-align: center;">Date</span>
				    <span class="col-xs-3" style="text-align: center;">Amount</span>
				    <span class="col-xs-3" style="text-align: center;">Received Date</span>
			  </div>
              <details ng-repeat="list in vendorCreditData" ng-class="$even ? 'oddrow' : 'evenrow'">
					<summary>
						<div class="stockLiftList shiva" style="height:30px;padding: 5px;border-bottom: 1px solid #e0e0e0;">
						    <span class="col-xs-3" style="color: #000;text-align: left;">{{list.name}}</span>
						    <span class="col-xs-3" style="color: #000;text-align: center;">{{list.date}}</span>
						    <span class="col-xs-3" style="color: #000;text-align: center;">{{list.totalAmount | INR}}</span>
						    <span class="col-xs-3" style="color: #000;text-align: center;">{{list.receivedDate}}</span>
						</div>
					</summary>
					<table class=" table table-fixed">
						   <thead><tr>
						    <th class="col-xs-2" style="background: #1a6398 !important;font-size: 10px;">Brand Name</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;font-size: 10px;">Case / Bottles</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;font-size: 10px;">Price</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;font-size: 10px;">Total Price</th>
						    <th class="col-xs-1" style="background: #1a6398 !important;font-size: 10px;">Disc/Btl</th>
						    <th class="col-xs-1" style="background: #1a6398 !important;font-size: 10px;">Total Discount</th>
						    <th class="col-xs-2" style="background: #1a6398 !important;font-size: 10px;">Total Amount</th>
						    
						    </tr></thead>
						    <tr ng-repeat="brand in list.creditChildBeanList">
							<td class="col-xs-2" style="color: #000;background: #f1ede8;padding: 5px;font-size: 10px;">{{brand.productName}}</td>
							<td class="col-xs-2" style="color: #000;background: #f1ede8;padding: 5px;font-size: 10px;">{{brand.cases}} / {{brand.bottles}}</td>
							<td class="col-xs-2" style="color: #000;background: #f1ede8;padding: 5px;font-size: 10px;">{{brand.price}}</td>
							<td class="col-xs-2" style="color: #000;background: #f1ede8;padding: 5px;font-size: 10px;">{{brand.totalPrice | INR}}</td>
							<td class="col-xs-1" style="color: #000;background: #f1ede8;padding: 5px;font-size: 10px;">{{brand.discount}}</td>
							<td class="col-xs-1" style="color: #000;background: #f1ede8;padding: 5px;font-size: 10px;">{{brand.totalDiscount}}</td>
							<td class="col-xs-2" style="color: #000;background: #f1ede8;padding: 5px;font-size: 10px;">{{brand.afterIncludingDiscount | INR}}</td>
							</tr>
						  </table>
					</details>
	        </div>
	      </div>
	    </div>
  </div>		
		
		
<!-- Vendor credit model popup end -->
<div class="fixed">
			<div class="row">
				<div class="col-sm-4">
				</div>
				<div class="col-sm-6" style="">
				<table style="float: right;margin-bottom: 0px;">
				     <tbody style="background-color: #243a51;">
						<tr>
							<th style="font-size: 14px; font-style: italic;padding-right: 5px;background-color: #243a51;text-align: right;">Discount :</th>
							 <td style="background-color: #243a51;text-align: left;">{{user.discountData | Discount | INR}}</td>
						</tr>
						</tbody>
					</table>
				</div>
				<div class="col-sm-2" style="">
				<table style="float: right;margin-bottom: 0px;">
				     <tbody style="background-color: #243a51;">
						<tr>
							<th style="font-size: 14px; font-style: italic;padding-right: 5px;background-color: #243a51;text-align: right;">Arrears :</th>
							 <td style="background-color: #243a51;text-align: left;">{{user.discountData | Arrears | INR}}</td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
</body>
</html>