<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   <%-- <spring:url value="/resources/css/3.3.7.bootstrap.min.css" var="bootstrap1mincss"/>
   <link rel="stylesheet" href="${bootstrap1mincss}"> --%>

 <jsp:include page="/WEB-INF/jsp/header.jsp" />  
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script> 
 <!--  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
 <spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
 
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script>
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
   <spring:url value="/resources/javascript/fusioncharts/fusioncharts.js" var="fusionjs" />
<script src="${fusionjs}"></script>
<spring:url value="/resources/javascript/fusioncharts/themes/fusioncharts.theme.fint.js" var="fusionthemejs" />
<script src="${fusionthemejs}"></script>
  <spring:url value="/resources/javascript/stockLifting.js" var="stockLiftingjs"/>
  <script src="${stockLiftingjs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}"> 
   <spring:url value="/resources/css/saleReport.css" var="saleReportcss"/>
   <link rel="stylesheet" href="${saleReportcss}">
 <style>
.container{
max-width: 100%;
}
summary{
display : block;
}
.shiva :hover{
background-color: #ccc;
}
label {
    color: #000 !important;
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 15px;
}
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<div class="row" style="padding-top: 100px;">
  <div class="col-sm-3 bodyFontCss" style="padding: 0px; !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Reports</li>
  <li>Stock Lift</li>
</ul>
  </div>
   <div class="col-sm-1 bodyFontCss"></div>
   <div class="col-sm-2 bodyFontCss">
   <label>Start Date</label><input type="text" class="form-control" id="startDate" readonly='true'>
   </div>
   <div class="col-sm-2 bodyFontCss">
   <label>End Date</label><input type="text" class="form-control" id="endDate" readonly='true'>
   </div>
   <div class="col-sm-1 bodyFontCss customizedButton">
   <input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getResults('startDate','endDate')" value="Get Data" />
   </div>
  <div class="col-sm-2 bodyFontCss"></div>
  <div class="col-sm-1 bodyFontCss">
  <span><i class="fa fa-filter filter-icon" style="color: #243a51!important;" data-toggle="modal" data-target="#saleFilterModal" aria-hidden="true"></i></span>
  </div>
  </div>
<div class="container" style="margin-top: 20px;">
  <div class="row" style="margin-bottom: 20px;">
  <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
    <div class="col-sm-12 bodyFontCss">
      <div class="stockLift ">
			<span class="col-xs-2" style="text-align: left;border-top-left-radius: 3px;" data-toggle="tooltip" data-placement="bottom" title="Brand Name" ng-click="sort('brandname','brandname')">Brand Name
			<label class="glyphicon sort-icon" style="color: #fff !important;" ng-show="sortKey=='brandname,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
			</span>
			<span class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Brand No" ng-click="sort('brandNo','brandname')">Brand No
			<label class="glyphicon sort-icon" style="color: #fff !important;" ng-show="sortKey=='brandNo,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
			</span>
			<span class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Category" ng-click="sort('companyOrder','brandname')">Company
			<label class="glyphicon sort-icon" style="color: #fff !important;" ng-show="sortKey=='companyOrder,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
			</span>
			<span class="col-xs-2" data-toggle="tooltip" data-placement="bottom" title="Category" ng-click="sort('categoryOrder','brandname')">Category
			<label class="glyphicon sort-icon" style="color: #fff !important;" ng-show="sortKey=='categoryOrder,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
			</span>
			<span class="col-xs-2" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" title="Sum Of Cases" ng-click="sort('totalcases','brandname')">Lifted Case
			<label class="glyphicon sort-icon" style="color: #fff !important;" ng-show="sortKey=='totalcases,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
			</span>
			<span class="col-xs-3" style="border-top-right-radius: 3px;" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" title="Total Amount" ng-click="sort('totalPrice','brandname')">Total Amount
			<label class="glyphicon sort-icon" style="color: #fff !important;" ng-show="sortKey=='totalPrice,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
			</span>
	 </div>
	<div class="datalist">
	<details ng-repeat="list in alldetails = (categories | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined,}:true) | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined}:true | filter: greaterThan('totalcases', 0)  | orderBy:sortKey:reverse">
      <summary>
		<div class="stockLiftList shiva" style="height:36px;border-right: 5px solid {{list.categoryColor}};border-bottom: 1px solid #e0e0e0;border-left:  5px solid {{list.bgcolor}};">
		<span class="col-xs-2" style="text-align: left;" title="{{list.brandname}}">{{list.brandname}}</span>
		<span class="col-xs-1" title="{{list.brandNo}}">{{list.brandNo}}</span>
		<span class="col-xs-2" title="{{list.company}}">{{list.company}}</span>
		<span class="col-xs-2" title="{{list.category}}">{{list.category}}</span>
		<span class="col-xs-2" title="{{list.totalcases | INR}}">{{list.totalcases | INR}}</span>
		<span class="col-xs-3" title="{{list.totalPrice | INR}}">{{list.totalPrice | INR}}</span>
		</div>
	</summary>
		<table class=" table table-fixed">
			<thead><tr>
			  <th class="col-xs-2" title="Quantity">Quantity</th>
				<th class="col-xs-2" title="Cases">Lifted Case</th>
				<th class="col-xs-2" title="Unit Price (Case)">Unit Price (Case)</th>
				<th class="col-xs-2" title="BTL Rate">BTL Rate</th>
				<th class="col-xs-2" title="Date">Date</th>
				<th class="col-xs-2" title="Total Price">Total Price</th>
			</tr></thead>
			<tr ng-repeat="brand in list.Brands">
			    <td class="col-xs-2" title="{{brand.qty}}">{{brand.qty}}</td>
				<td class="col-xs-2" title="{{brand.cases}}">{{brand.cases}}/{{brand.qtyBottels}}</td>
				<td class="col-xs-2" title="{{brand.unitPrice}}">{{brand.unitPrice | INR}}</td>
				<td class="col-xs-2" title="{{brand.BtlMrp}}">{{brand.BtlMrp | INR}}</td>
				<td class="col-xs-2" title="{{brand.date}}">{{brand.date}}</td>
				<td class="col-xs-2" title="{{brand.totalAmount}}">{{brand.totalAmount | INR}}</td>
			</tr>
		</table>
	</details>
	</div>
    </div>
  </div>

		<div class="fixed">
			<div class="row">
				<div class="col-sm-4">
					<table>
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Invoice
								Value :</th>
							<td style="float: right;">{{alldetails | sumItens | INR}}</td>
						</tr>
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">MRP Round
								Off :</th>
							<td style="float: right;"><span id="mrpRoundingOffTotal">
							</span></td>
						</tr>
					</table>


				</div>
				<div class="col-sm-4" style="text-align: -webkit-center;">
					<table>
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">TCS :</th>
							<td style="float: right;"><span id="tcs"></span></td>
						</tr>
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">TurnOver
								Tax :</th>
							<td style="float: right;"><span id="turnOverTax"> </span></td>
						</tr>
					</table>
				</div>
				<div class="col-sm-4" style="">
				<table style="float: right;">
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Net Invoice Value :</th>
							<td style="float: right;"><span id="netInvoiceValue"></span></td>
						</tr>
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Cummulative Value(From 01-Nov-2019) :</th>
							<td style="float: right;"><span id="invoiceTotal"> </span></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>
  <!-- Modal -->
<div class="modal fade right" id="saleFilterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-side modal-top-right" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title w-100" id="exampleModalLabel"><i class="fa fa-filter" style="color: #243a51!important;font-size: 22px;"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="font-size: 25px;">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
	    <select ng-model="modelfilter" class="form-control filterDropdownCss"  name="sort" style="float: left;">
			<option value>ALL Category</option> 
			<option ng-repeat="list in categories | unique:'category'" value="{{list.category}}">{{list.category}}</option>
        </select> 
		<select ng-model="companyfilter" class="form-control filterDropdownCss">
			<option value>ALL Company</option>
		    <option ng-repeat="list in categories | unique:'company' | filter:{category:modelfilter}" value="{{list.company}}">{{list.company}}</option>
		</select>
		<!-- <div class="col-sm-3" style="font-size: 12px;font-weight: 400;"><b>Sort By:</b></div>
	    <div class="col-sm-9">
	   <select id="sortById" ng-click="sortBy()" class="form-control">
	     <option >Select Option</option>
	      <option value="brandname">Brand Name</option>
	      <option value="brandNo">Brand No</option>
	      <option value="category">Category</option>
	      <option value="company">Company</option>
	     <option value="totalcases">Lifted Case</option>
	      <option value="totalPrice">Total Amount</option>
	     <option value="companyOrder">Company Order</option>
	   </select>
	   </div> -->
	 </div>
      <div class="modal-footer">
        <!-- <button type="button" style="background: #1a6398!important;" class="btn btn-primary btn-rounded" data-dismiss="modal">Apply Filters</button> -->
      </div>
    </div>
  </div>
</div>
</body>
</html>