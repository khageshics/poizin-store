<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-1.12.4.js" var="jquery1124js"/>
  <script src="${jquery1124js}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/angular.min.js" var="angularminjs"/>
  <script src="${angularminjs}"></script>
  <spring:url value="/resources/javascript/thirdpartyjs/angular-filter.js" var="angularfilterjs"/>
  <script src="${angularfilterjs}"></script>
<!--   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
   
  <link rel="stylesheet" href="/resources/demos/style.css">
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script> 
  <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script>
<!--    <script src="https://rawgit.com/zorab47/jquery.ui.monthpicker/master/jquery.ui.monthpicker.js"></script> -->
    <spring:url value="/resources/javascript/thirdpartyjs/jquery.ui.monthpicker.js" var="jqueryuimonthpickerjs"/>
  <script src="${jqueryuimonthpickerjs}"></script>

 <spring:url value="/resources/css/common.css" var="commoncss"/>
   <link rel="stylesheet" href="${commoncss}">
   <spring:url value="/resources/css/bootstrap.min.css" var="bootstrapmincss"/>
   <link rel="stylesheet" href="${bootstrapmincss}">
 <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <spring:url value="/resources/javascript/mobileView/mobileViewStockLifting.js" var="mobileViewStockLiftingjs"/>
  <script src="${mobileViewStockLiftingjs}"></script> 
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
   <spring:url value="/resources/css/mobileViewSaleReport.css" var="mobileViewSaleReportcss"/>
   <link rel="stylesheet" href="${mobileViewSaleReportcss}">
<style>
 .color-green {
  color: green;
}

.color-orange {
  color: orange;
}

.color-red {
  color: red;
}

h1, h2, h3, h4, h5, h6 {
    font-family: "Segoe UI",Arial,sans-serif;
    font-weight: 400;
    margin: 10px 0;
}
h6 {
    font-size: 16px;
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 12px;
}
.datalist {
    max-height: calc(100vh - 260px);
    overflow-y: auto;
    width: 100%;
}
summary{
display : block;
}
details summary::-webkit-details-marker { display:none; }
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
<div >
<a href="mobileViewHome" style="text-decoration: none;">
<header style="height: 55px;">
  <h2 style="float:left;padding: 0px 0px 0px 2px;">POIZIN</h2>
  <h6 style="float: right;padding: 8px 2px 0px 0px;">STOCK LIFTING</h6>
</header></a>
<div class="tab-content " style="font-family: unset;font-size: 13px;">
<div class="container">
<div class="row allignmentcss">
    <!-- <div class="col-md-2">
	</div> -->
    <div class="col-md-5" style="margin-bottom: 10px;">
    Month: <input id="startDate" class="monthpicker" type="text" readonly='true'>
    <input id="setDateValueID" class="" type="text" readonly='true' style="border: none; text-align: center;width: 95px;font-weight: 600;color: #062351;"">
    <input class="submit" type="submit" name="" ng-click="getResults('startDate','endDate')" value="Get" />
    </div>
	<div class="col-md-3" style="float: right;">
	<label class="switch">
     <input type="checkbox" id="catOrComp">
     <span class="slider round" style="background-color: #062351;" ng-click="FilterCatOrComp()"></span>
    </label>
	</div>
	<div class="col-md-4" style="float: left;font-weight: 600;color: #062351;">
	<!--  CASE: {{alldetails | totalCase | INR}}, CUMULATIVE: {{commulative | INR}} -->
	</div>
</div>
</div>
<div class="datalist">
<div class="container">
        <div class="saleReportWithCompanyWise">
          <div class="row" style="border-radius: 5px;color: #062351;font-size: 11px;margin-right: 0px;margin-left: 0px;">
				<span class="col-xs-5" style="margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;padding: 0px 0px 0px 20px;" title="">Company</span>
				<span class="col-xs-2" style="margin-top: 5px;text-align: right;padding: 0px;" title="">Lifted Case</span>
				<span class="col-xs-2" style="margin-top: 5px;text-align: right;padding: 0px;">InHouse</span>
				<span class="col-xs-3" style="border-bottom-right-radius: 5px;border-top-right-radius: 5px;margin-top: 5px;text-align: right;padding: 0px 10px 0px 0px;" title="">Pend. Case</span>
			</div>
		<details ng-repeat="list in stockdata | orderBy:['companyOrder'] | filter: greaterThan('sumOfCase', 0)">
				<summary>
					<div class="stockLiftList">
						<span class="col-xs-5" style="margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};" title="">{{list.company}} ({{list.sumOfCase / totalCaseForPercentage * 100 | roundup}}%)</span>
						<span class="col-xs-2" style="margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2" style="margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.sumOfStock}}</span>
						<span class="col-xs-3" style="border-bottom-right-radius: 5px;border-top-right-radius: 5px;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.sumOvPending}}</span>
					</div>
				</summary>
				<details ng-repeat="data in list.inputjson | filter: greaterThan('cases', 0)">
				   <summary>
					 <span class="col-xs-5 secondSpan" style="padding-left: 6px;text-align: left;background-color: #e8e8e8;" title="">{{data.brandname}} ({{data.cases / list.sumOfCase * 100 | roundup}}%)</span>
					 <span class="col-xs-2 secondSpan" style="text-align: right;background-color: #e8e8e8;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan" style="text-align: right;background-color: #e8e8e8;" title="" ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouse | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="padding-right: 35px;text-align: right;background-color: #e8e8e8;" title="">{{data.pending}}</span>
					 </summary>
					 <table class=" table table-fixed">
					     <tr ng-repeat="brand in data.Brands">
							<td class="col-xs-6" style="border-top: 0px solid #ddd;" title="{{brand.quantity}}">{{brand.quantity}}</td>
						    <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseVal}}">{{brand.caseVal | INR}}</td>
						    <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseQty}}/{{brand.btls}}</td>
						</tr></table>
					 </details>
			</details>
</div>
  
  <div class="saleReportWithCategoryWise">
          <div class="row" style="border-radius: 5px;color: #062351;font-size: 11px;">
				<span class="col-xs-5" style="margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;padding: 0px 0px 0px 20px;" title="">Category</span>
				<span class="col-xs-2" style="margin-top: 5px;text-align: right;padding: 0px;" title="">Lifted Case</span>
				<span class="col-xs-2" style="margin-top: 5px;text-align: right;padding: 0px;">InHouse</span>
				<span class="col-xs-3" style="border-bottom-right-radius: 5px;border-top-right-radius: 5px;margin-top: 5px;text-align: right;padding: 0px 10px 0px 0px;" title="">Pend. Case</span>
			</div>
		<details ng-repeat="list in stockCastegorydata | orderBy:['categoryOrder'] | filter: greaterThan('sumOfCase', 0)">
				<summary>
					<div class="stockLiftList">
						<span class="col-xs-5" style="margin-top: 5px;border-bottom-left-radius: 5px;border-top-left-radius: 5px;text-align: left;background-color: {{list.bgcolor}};" title="">{{list.category}} ({{list.sumOfCase / totalCaseForPercentage * 100 | roundup}}%)</span>
						<span class="col-xs-2" style="margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.sumOfCase | INR}}</span>
						<span class="col-xs-2" style="margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.sumOfStock}}</span>
						<span class="col-xs-3" style="border-bottom-right-radius: 5px;border-top-right-radius: 5px;margin-top: 5px;text-align: right;background-color: {{list.bgcolor}};" title="">{{list.sumOvPending}}</span>
					</div>
				</summary>
				<details ng-repeat="data in list.inputjson | filter: greaterThan('cases', 0)">
				   <summary>
					 <span class="col-xs-5 secondSpan" style="padding-left: 6px;text-align: left;background-color: #e8e8e8;" title="">{{data.brandname}} ({{data.cases / list.sumOfCase * 100 | roundup}}%)</span>
					 <span class="col-xs-2 secondSpan" style="text-align: right;background-color: #e8e8e8;" title="">{{data.cases | INR}}</span>
					 <span class="col-xs-2 secondSpan" style="text-align: right;background-color: #e8e8e8;" title="" ng-class="{'color-red': data.secondFontColor <= 1}">{{data.inHouse | INR}}</span>
					 <span class="col-xs-3 secondSpan" style="padding-right: 35px;text-align: right;background-color: #e8e8e8;" title="">{{data.pending}}</span>
					 </summary>
					 <table class=" table table-fixed">
					     <tr ng-repeat="brand in data.Brands">
							<td class="col-xs-6" style="border-top: 0px solid #ddd;" title="{{brand.quantity}}">{{brand.quantity}}</td>
						    <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="{{brand.caseVal}}">{{brand.caseVal | INR}}</td>
						    <td class="col-xs-3" style="border-top: 0px solid #ddd;" title="" ng-class="{'color-red': brand.noOfDays <= 1,'color-orange': brand.noOfDays >= 2 && brand.noOfDays <= 3,'color-green': brand.noOfDays >=4}">{{brand.caseQty}}/{{brand.btls}}</td>
						</tr></table>
					 </details>
			</details>
</div>
</div>
   <!-- footer start -->
<div class="fixed">
			<div class="row">
				<div class="col-xs-6" style="text-align: left;">
					 <table>
						<tr>
							<th style="font-size: 12px; font-style: italic;padding-right: 5px;">CASE: </th>
							<td style="float: right;">{{alldetails | totalCase | INR}}</td>
						</tr>
					</table>
				</div>
				<div class="col-xs-6" style="">
			    <!--  <table style="float: right;">
				        <tr>
							<th style="font-size: 12px; font-style: italic;padding-right: 5px;">CUMULATIVE: </th>
							<td style="float: right;">{{commulative | INR}}</td>
						</tr>
					</table> -->
				</div>
			</div>
		</div>
<!-- footer end -->
</div>
</div>
</div>
</body>
</html>