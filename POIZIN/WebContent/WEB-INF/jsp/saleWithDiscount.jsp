<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>www.poizin.com</title>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<%-- <spring:url value="/resources/css/3.3.7.bootstrap.min.css" var="bootstrap1mincss"/>
   <link rel="stylesheet" href="${bootstrap1mincss}"> --%>
<jsp:include page="/WEB-INF/jsp/header.jsp" />  
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
   <spring:url value="/resources/javascript/thirdpartyjs/jquery.min.js" var="jqueryminjs"/>
  <script src="${jqueryminjs}"></script> 
<!--   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> -->
<spring:url value="/resources/css/themejquery-ui.css" var="themejqueryuicss"/>
   <link rel="stylesheet" href="${themejqueryuicss}">
   
  <spring:url value="/resources/javascript/thirdpartyjs/jquery-ui.js" var="jqueryuijs"/>
  <script src="${jqueryuijs}"></script>
    <spring:url value="/resources/javascript/thirdpartyjs/bootstrap.min.js" var="bootstrapminjs"/>
  <script src="${bootstrapminjs}"></script> 
  <spring:url value="/resources/javascript/saleWithDiscount.js" var="saleWithDiscountjs"/>
  <script src="${saleWithDiscountjs}"></script>
  <spring:url value="/resources/javascript/common.js" var="commonjs"/>
  <script src="${commonjs}"></script> 
  <spring:url value="/resources/css/newCommon.css" var="newCommoncss"/>
   <link rel="stylesheet" href="${newCommoncss}"> 
   <spring:url value="/resources/css/saleReport.css" var="saleReportcss"/>
   <link rel="stylesheet" href="${saleReportcss}">
 <style>
.container{
max-width: 100%;
}
summary{
display : block;
}
label {
    color: #fff !important;
}
div.fixed {
  position: fixed;
  bottom: 0;
  right: 0;
  width: 100%;
  background-color: #243a51;
  color: #fff;
  font-size: 18px;
  padding: 8px;
}
.fixed td{
 font-size: 15px;
}
.shiva :hover{
background-color: #ccc;
}
</style>
</head>
<body ng-app="myApp" ng-controller="myCtrl">
 <div class="row" style="padding-top: 100px;">
  <div class="col-sm-3 bodyFontCss" style="padding: 0px; !important;">
  <ul class="breadcrumb">
  <li><a href="admin">Dashboard</a></li>
   <li>Reports</li>
  <li>Sale With Discount</li>
</ul>
  </div>
  <div class="col-sm-1 bodyFontCss"></div>
   <div class="col-sm-2 bodyFontCss">
   <label>Start Date</label><input type="text" class="form-control" id="startDate" readonly='true'>
   </div>
   <div class="col-sm-2 bodyFontCss">
   <label>End Date</label><input type="text" class="form-control" id="endDate" readonly='true'>
   </div>
   <div class="col-sm-1 bodyFontCss customizedButton">
   <input class="btn btn-default btn-rounded" type="submit" name="" ng-click="getResults('startDate','endDate')" value="Get Data" />
   </div>
  <div class="col-sm-2 bodyFontCss"></div>
  <div class="col-sm-1 bodyFontCss">
  <span><i class="fa fa-filter filter-icon" style="color: #243a51!important;" data-toggle="modal" data-target="#saleFilterModal" aria-hidden="true"></i></span>
  </div>
  </div>
 <div class="container" style="margin-top: 20px;">
  <div class="row" style="margin-bottom: 20px;">
  <div id="wait" style="display:none;width:69px;height:89px;position:absolute;top:50%;left:50%;padding:2px;"><img src='https://www.w3schools.com/jquery/demo_wait.gif' width="64" height="64" /><br>Loading..</div>
    <div class="col-sm-12 bodyFontCss">
    <div class="stockLift ">
					    <span class="col-xs-1" style="text-align: left;border-top-left-radius: 3px;" data-toggle="tooltip" data-placement="bottom" title="Brand Name" ng-click="sort('brandname','brandname')">Brand Name
					    <label class="glyphicon sort-icon" ng-show="sortKey=='brandname,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					    <span class="col-xs-1" data-toggle="tooltip" data-placement="bottom" title="Brand No" ng-click="sort('brandNo','brandname')">Brand No
					    <label class="glyphicon sort-icon" ng-show="sortKey=='brandNo,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					    <span class="col-xs-1" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" title="Category" ng-click="sort('categoryOrder','brandname')">Category
					    <label class="glyphicon sort-icon" ng-show="sortKey=='categoryOrder,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					    <span style="display: none;" class="col-xs-2" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" title="Company">Company
					    </span>
					    <span class="col-xs-1" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" title="Total Sale (Case)" ng-click="sort('cases','brandname')">Sale
					    <label class="glyphicon sort-icon" ng-show="sortKey=='cases,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					    <span class="col-xs-1" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" title="Total Amount" ng-click="sort('totalval','brandname')">Sale Amt.
					    <label class="glyphicon sort-icon" ng-show="sortKey=='totalval,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					    <span class="col-xs-1" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" title="Total Amount" ng-click="sort('costPrice','brandname')">Inv Amt
					    <label class="glyphicon sort-icon" ng-show="sortKey=='costPrice,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					    <span class="col-xs-2" style="" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" ng-click="sort('discountAmt','brandname')"> Comp.Disc (%)
					    <label class="glyphicon sort-icon" ng-show="sortKey=='discountAmt,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					     <span class="col-xs-2" style="" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" ng-click="sort('govtDiscount','brandname')"> Gov Mar (%)
					     <label class="glyphicon sort-icon" ng-show="sortKey=='govtDiscount,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					    <span class="col-xs-2" style="border-top-right-radius: 3px;" data-toggle="tooltip" data-placement="bottom" data-trigger="hover" ng-click="sort('totalDiscountVal','brandname')"> Total Discount (%)
					    <label class="glyphicon sort-icon" ng-show="sortKey=='totalDiscountVal,brandname'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></label>
					    </span>
					</div>
					<div class="datalist totalSaleItmes">
					<h4 ng-if="(alldetails | ttlSaleAmt) <=0" style="text-align: center;padding: 8px;">No Data Available</h4>
					
					<details ng-repeat="list in alldetails = (categories | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined}:true) | filter:{company:companyfilter,category:modelfilter,company:companyfilter||undefined,category:modelfilter||undefined}:true | filter: greaterThan('totalbtlsale', 0) | orderBy:sortKey:reverse">
					<summary>
						<div class="stockLiftList shiva" style="height:36px;border-right: 5px solid {{list.categoryColor}};border-bottom: 1px solid #e0e0e0;border-left:  5px solid {{list.bgcolor}};">
						    <span class="col-xs-1" style="color: #000;text-align: left;" title="{{list.brandname}}">{{list.brandname}}</span>
						    <span class="col-xs-1" style="color: #000;" title="{{list.brandNo}}">{{list.brandNo}}</span>
						    <span class="col-xs-1" style="color: #000;" title="{{list.category}}">{{list.category}}</span>
						    <span class="col-xs-2" style="display: none;color: #000;" title="{{list.company}}">{{list.company}}</span>
						    <span class="col-xs-1" style="color: #000;" title="{{list.cases | INR}}">{{list.cases | INR}}</span>
						    <span class="col-xs-1" style="color: #000;" title="{{list.totalval | INR}}">{{list.totalval | INR}}</span>
						    <span class="col-xs-1" style="color: #000;" title="{{list.costPrice | INR}}">{{list.costPrice | INR}}</span>
						    <span class="col-xs-2" style="color: #000;" title="{{list.discountAmt | INR}} ({{list.companyDiscInPercentage}})">{{list.discountAmt | INR}} ({{list.companyDiscInPercentage}})</span>
						    <span class="col-xs-2" style="color: #000;" title="{{list.govtDiscount | INR}} ({{list.govtMarginInPercentage}})">{{list.govtDiscount | INR}} ({{list.govtMarginInPercentage}})</span>
						    <span class="col-xs-2" style="color: #000;" title="{{list.totalDiscountVal | INR}}">{{list.totalDiscountVal | INR}} ({{list.totalDiscountValInPer}})</span>
						</div>
					</summary>
					<table class=" table table-fixed">
						   <thead><tr>
						    <th class="col-xs-1" title="Disc. Month">Disc. Month</th>
						    <th class="col-xs-1" title="Quantity">Quantity</th>
						    <th class="col-xs-2" title="Sale">Sale</th>
						    <th class="col-xs-1" title="Total Cost">Sale Amount</th>
						     <th class="col-xs-1" title="Cost Cost">Inv Amt</th>
						     <th class="col-xs-2" title="Comp. Discount">Comp. Disc (%)</th>
						    <th class="col-xs-2" title="Govt. Discount">Govt. Mar (%)</th>
						    <th class="col-xs-2" title="Govt. Discount">Total Discount (%)</th>
						    </tr></thead>
						    <tr ng-repeat="brand in list.Brands | filter: greaterThan('totalSale', 0)  | orderBy:['category','company'] ">
						    <td class="col-xs-1" style="color: #000;" title="{{brand.discountMonth}}">{{brand.discountMonth}}</td>
							<td class="col-xs-1" style="color: #000;" title="{{brand.quantity}}">{{brand.quantity}}</td>
							<td class="col-xs-2" style="color: #000;" title="{{brand.saleInCase}}/{{brand.btlVal}}">{{brand.saleInCase}}/{{brand.btlVal}}</td>
							<td class="col-xs-1" style="color: #000;" title="{{brand.totalPrice | INR}}">{{brand.totalPrice | INR}}</td>
							<td class="col-xs-1" style="color: #000;" title="{{brand.costPrice | INR}}">{{brand.costPrice | INR}}</td>
							<td class="col-xs-2" style="color: #000;" title="{{brand.discount | INR}}">{{brand.discount | INR}} ({{brand.companyDiscInPercentage}})</td>
							<td class="col-xs-2" style="color: #000;" title="{{brand.govtDiscount | INR}}">{{brand.govtDiscount | INR}} ({{brand.govtMarginInPercentage}})</td>
							<td class="col-xs-2" style="color: #000;" title="{{brand.totalDiscount | INR}}">{{brand.totalDiscount | INR}} ({{brand.totalDiscountValInPer}})</td>
							</tr>
						  </table>
					</details>
					</div>
    </div>
  </div> 
 <div class="fixed">
			<div class="row">
				<div class="col-sm-3">
					<table>
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Sale Amount :</th>
							<td style="float: right;">{{alldetails | ttlSaleAmt | INR}}</td>
						</tr>
					</table>
				</div>
				<div class="col-sm-3">
					<table style="float: center;">
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Comp. Discount :</th>
							<td style="float: right;">{{alldetails | ttlDiscountAmt | INR}}</td>
						</tr>
					</table>
				</div>
				<div class="col-sm-3" style="">
				<table style="float: center;">
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Govt. Margin :</th>
							<td style="float: right;">{{alldetails | ttlGovtDiscountAmt | INR}}</td>
						</tr>
					</table>
				</div>
				<div class="col-sm-3" style="">
				<table style="float: right;">
						<tr>
							<th style="font-size: 14px; font-style: italic; padding-right: 5px;">Discount :</th>
							<td style="float: right;">{{alldetails | ttlGovtCompDiscountAmt | INR}}</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
 
 
 </div>
 <!-- Modal -->
<div class="modal fade right" id="saleFilterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-side modal-top-right" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title w-100" id="exampleModalLabel"><i class="fa fa-filter" style="color: #243a51!important;font-size: 22px;"></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" style="font-size: 25px;">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="text-align: center;">
	   <select ng-model="modelfilter" class="form-control filterDropdownCss"  name="sort" style="float:left;">
			 <option value>ALL Category</option> 
		  <option ng-repeat="list in categories | unique:'category'" value="{{list.category}}">{{list.category}}</option>
	    </select> 
	   <select ng-model="companyfilter" class="form-control filterDropdownCss">
			  <option value>ALL Company</option>
		  <option ng-repeat="list in categories | unique:'company' | filter:{category:modelfilter}:true | orderBy:['companyOrder']" value="{{list.company}}">{{list.company}}</option>
	  
	   </select>
	 </div>
      <div class="modal-footer">
        <!-- <button type="button" style="background: #1a6398!important;" class="btn btn-primary btn-rounded" data-dismiss="modal">Apply Filters</button> -->
      </div>
    </div>
  </div>
</div>
</body>
</html>