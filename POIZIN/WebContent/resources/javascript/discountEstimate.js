var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope, $http) {
	$scope.totalItem = false;
	$scope.discountItem = true;
	var myUrl = window.location.href;
	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
	 $scope.init = function(){
		 $('#monthlyDate').monthpicker({changeYear:true,dateFormat: "MM-yy" });
		 $("#monthlyDate").val($.datepicker.formatDate('MM-yy', new Date()));
		 $scope.callDiscountData();
		 
	 } 
	 
	 $scope.callDiscountData = function(){
		 $("#wait").css("display", "block");
		var date = changeDateFormateForMonth($("#monthlyDate").val());
		$scope.finalDate = date;
		var tempDate = new Date();
			tempDate.setDate(0);
			tempDate.setDate(1);
			tempDate.setHours(0,0,0,0);
			if(new Date(date) >= tempDate)
				$scope.showButton = true;
			else
				$scope.showButton = false;
		
		var callingUrl = url+"callForDiscountData?month="+date;
		$http({
		    method : "GET",
		    url : callingUrl
		  }).then(function mySuccess(response) {
			  //console.log(JSON.stringify(response.data))
			  var groupingNum = [];
			  $.each(response.data, function(index, value){
	    	   	if(groupingNum.indexOf(value.productGrouping) === -1){
	    	   		groupingNum.push(value.productGrouping);
	    	   	}
	    	   });
			  var inputjson = [];
			  var falgvalSize = false;
	    	   for (var i = 0; i < groupingNum.length; i++) {
	    		   var Brands= [];var color = "",companyName="",groupName="";var sumOfTarget=0,totalSoldCase=0,inhousestock=0,lastmonthsoldold=0;
	    		       var pendingvalue=0,liftedcase=0,investmentamount=0,totaldiscount=0,idxval=0,companyid=0;
	    		   $.each(response.data, function(index, value){
	    			   if(groupingNum[i] == value.productGrouping){
	    				   var pending=0,clearnsPeriodWithStock=0,clearnsPeriodWithoutStock=0,rateOfInterestWithInhouse=0,rateOfInterestWithoutInhouse=0,discountPer=0;
	    				   
		    				  if((value.targetCase - value.liftedCase) > 0)
		    					  pending = value.targetCase - value.liftedCase;
	    				 
		    				  var input = {
	    						   "brandName":value.name,
	    			        	   "brandNo":value.brandNo,
	    			        	   "caseRate":value.caseRate,
	    			        	   "inhouseStock":value.inhouseStock,
	    			        	   "lastMonthSold":value.lastMonthSold,
	    			        	   "liftedCase":value.liftedCase,
	    			        	   "targetCase":value.targetCase,
	    			        	   "investment":0,
	    			        	   "productGrouping":value.productGrouping,
	    			        	   "companyOrder":value.companyOrder,
	    			        	   "companyColor":value.companyColor,
	    			        	   "pending":pending,
	    			        	   "discountPer":0,
	    			        	   "estimationMonth":0,
	    			        	   "clearnsPeriodWithoutInhouseStock":0,
	    			        	   "rateOfInterestWithInhouse":0,
	   		  	        		   "rateOfInterestWithoutInhouse":0,
	   		  	        		   "totalDiscount":0,
	   		  	        		   "adjustment":value.adjustment,
	   		  	        		   "companyId":value.companyId,
	   		  	        		   "companyName":value.companyName,
	   		  	        		   "discountAmount":value.discountAmount,
	   		  	        		   "productType":value.productType,
	   		  	        		   "splitPackType":value.splitPackType,
	   		  	        		   "maxMinDiscountBean":value.maxMinDiscountBean,
	   		  	        		   "currentIndent":pending,
	   		  	        		   "stockDates":value.stockDates
	    				   };
	    				   Brands.push(input);
	    				   color = value.companyColor;
	    				   sumOfTarget += value.targetCase;
	    				   totalSoldCase += value.lastMonthSold;
	    				   inhousestock += value.inhouseStock;
	    				   lastmonthsoldold += value.lastMonthSold;
	    				   pendingvalue += pending;
	    				   liftedcase += value.liftedCase;
	    				   parentDisc = value.discountAmount;
	    				   companyName = value.companyName;
	    				   groupName = value.groupName;
	    				   
	    			   }
	    		   });
	    		   var falgval = false;
	    		   if(parentDisc > 0 || sumOfTarget > 0){
	    			   falgval = true;
	    			   falgvalSize = true;
	    		   }
	    			   
	    		   var  inputparent = {
	    		           "grouping": groupingNum[i],
	    		           "color": color,
	    		           "sumOfTarget": sumOfTarget,
	    		           "totalSoldCase":totalSoldCase,
	    		           "inhousestockvalue":inhousestock,
	    		           "lastmonthsoldold":lastmonthsoldold,
	    		           "pendingvalue":pendingvalue,
	    		           "liftedcase":liftedcase,
	    		           "investmentamount":0,
	    		           "totaldiscount":0,
	    		           "parentDisc":parentDisc,
	    		           "parentclearnsPeriodWithStock":0,
	    		           "parentclearnsPeriodWithoutStock":0,
	    		           "parentrateOfInterestWithInhouse":0,
	    		           "parentrateOfInterestWithoutInhouse":0,
	    		           "parentDiscPer":0,
	    		           "companyName":companyName,
	    		           "groupName":groupName,
	    		           "flag":false,
	    		           "currentIndent":0,
	    		           "flagVal":falgval,
	    		           "Brands":Brands,
	    		           "listOrder":Brands.length
	    		   };
	    		   inputjson.push(inputparent);
	    	   }
	    	   $scope.discountdata = inputjson;
	    	   $scope.falgvalSize = falgvalSize;
	    	   //console.log(JSON.stringify(inputjson));
	    	   $scope.divideToCurrentEstimate(inputjson);
	    	   //console.log(JSON.stringify($scope.discountdata));
	    	   $scope.footerDetails();
	    	   $("#wait").css("display", "none");
		  }, function myError(response) {
			  
          });
	 }
$scope.divideToCurrentEstimate = function(inputjson){
	for (var j = 0; j < $scope.discountdata.length; j++) {
		var parentDiscPer = 0, parentclearnsPeriodWithStock=0, parentclearnsPeriodWithoutStock=0,investmentamount=0,totaldiscount=0,idxval=0;
		var parentrateOfInterestWithInhouse=0,parentrateOfInterestWithoutInhouse=0,currentIndent=0;
		
		for (var k = 0; k < $scope.discountdata[j].Brands.length; k++) {
			var clearnsPeriodWithStock=0,clearnsPeriodWithoutStock=0,rateOfInterestWithInhouse=0,rateOfInterestWithoutInhouse=0,discountPer=0;
			
			$scope.discountdata[j].Brands[k].currentIndent = $scope.divideCaseForChild($scope.discountdata[j].Brands[k].lastMonthSold,$scope.discountdata[j].totalSoldCase,$scope.discountdata[j].pendingvalue);
			
			if($scope.discountdata[j].Brands[k].caseRate > 0)
				  discountPer = (($scope.discountdata[j].Brands[k].discountAmount * 100)/$scope.discountdata[j].Brands[k].caseRate).toFixed(2);
			  else
				  discountPer = 0;
			  
			if($scope.discountdata[j].Brands[k].lastMonthSold > 0){
			    clearnsPeriodWithStock=(($scope.discountdata[j].Brands[k].currentIndent + $scope.discountdata[j].Brands[k].inhouseStock) / $scope.discountdata[j].Brands[k].lastMonthSold);
				clearnsPeriodWithoutStock=($scope.discountdata[j].Brands[k].currentIndent / $scope.discountdata[j].Brands[k].lastMonthSold);
			}
			var profitForTheMonth = ($scope.discountdata[j].Brands[k].discountAmount*100)/($scope.discountdata[j].Brands[k].caseRate);
			
			  if(profitForTheMonth > 0 && clearnsPeriodWithStock > 0){
				  rateOfInterestWithInhouse = (profitForTheMonth)/(($scope.discountdata[j].Brands[k].currentIndent + $scope.discountdata[j].Brands[k].inhouseStock) / $scope.discountdata[j].Brands[k].lastMonthSold);
			  }
			  if(profitForTheMonth > 0 && clearnsPeriodWithoutStock > 0){
				  rateOfInterestWithoutInhouse = (profitForTheMonth)/($scope.discountdata[j].Brands[k].currentIndent / $scope.discountdata[j].Brands[k].lastMonthSold);
			  }
			  
			  $scope.discountdata[j].Brands[k].discountPer = discountPer;
			  $scope.discountdata[j].Brands[k].investment = ($scope.discountdata[j].Brands[k].caseRate * $scope.discountdata[j].Brands[k].currentIndent);
			  $scope.discountdata[j].Brands[k].estimationMonth = (clearnsPeriodWithStock).toFixed(2);
			  $scope.discountdata[j].Brands[k].clearnsPeriodWithoutInhouseStock = (clearnsPeriodWithoutStock).toFixed(2);
			  $scope.discountdata[j].Brands[k].rateOfInterestWithInhouse = (rateOfInterestWithInhouse).toFixed(2);
			  $scope.discountdata[j].Brands[k].rateOfInterestWithoutInhouse = (rateOfInterestWithoutInhouse).toFixed(2);
			  $scope.discountdata[j].Brands[k].totalDiscount = ($scope.discountdata[j].Brands[k].discountAmount * $scope.discountdata[j].Brands[k].currentIndent);
			  
			  investmentamount += $scope.discountdata[j].Brands[k].investment;
			  totaldiscount += $scope.discountdata[j].Brands[k].totalDiscount;
			  parentclearnsPeriodWithStock += clearnsPeriodWithStock;
			  parentclearnsPeriodWithoutStock += clearnsPeriodWithoutStock;
			  parentrateOfInterestWithInhouse += rateOfInterestWithInhouse;
			  parentrateOfInterestWithoutInhouse += rateOfInterestWithoutInhouse;
			  parentDiscPer += parseFloat(discountPer);
			  currentIndent += $scope.discountdata[j].Brands[k].currentIndent
			  
			  idxval++;
		}
		$scope.discountdata[j].investmentamount = investmentamount;
		$scope.discountdata[j].totaldiscount = totaldiscount;
		$scope.discountdata[j].parentclearnsPeriodWithStock = (parentclearnsPeriodWithStock / idxval).toFixed(2);
		$scope.discountdata[j].parentclearnsPeriodWithoutStock = (parentclearnsPeriodWithoutStock / idxval).toFixed(2);
		$scope.discountdata[j].parentrateOfInterestWithInhouse = (parentrateOfInterestWithInhouse / idxval).toFixed(2);
		$scope.discountdata[j].parentrateOfInterestWithoutInhouse = (parentrateOfInterestWithoutInhouse / idxval).toFixed(2);
		$scope.discountdata[j].parentDiscPer = (parentDiscPer / idxval).toFixed(2);
		$scope.discountdata[j].currentIndent = currentIndent;
		//$scope.discountdata[j].pendingvalue = 0;
		
	}
	
}

$scope.divideCaseForChild = function(saleVal, soldCases, target){
	var inPer=(saleVal*100)/soldCases;
	 var realVal = (inPer * target)/100;
	 if (! isNaN(realVal)) {
	 return Math.round(realVal);
	 }else{
		 return Math.round(0);
	 }
}	 
$scope.splitTargetCase = function(targetCase, data){
	for (var j = 0; j < $scope.discountdata.length; j++) {
		if(data.grouping == $scope.discountdata[j].grouping){
			for (var k = 0; k < data.Brands.length; k++) {
				var pending = 0;
				var tempCase = $scope.divideCaseForChild(data.Brands[k].lastMonthSold,data.totalSoldCase,targetCase);
				
				if((tempCase - $scope.discountdata[j].Brands[k].liftedCase) > 0)
				 pending = tempCase - $scope.discountdata[j].Brands[k].liftedCase;
				
				$scope.discountdata[j].Brands[k].targetCase = tempCase;
				$scope.discountdata[j].Brands[k].pending = pending;
				
			}
			$scope.discountdata[j].pendingvalue = (targetCase - $scope.discountdata[j].liftedcase);
			//$scope.discountdata[j].currentIndent = (targetCase - $scope.discountdata[j].liftedcase);
			//$scope.splitCurrentIndent($scope.discountdata[j].currentIndent, data);
			$scope.footerDetails();
		}
	}
} 
/*$scope.childTargetCase = function(targetcase, data, brandno){
	for (var j = 0; j < $scope.discountdata.length; j++) {
		if(data.grouping == $scope.discountdata[j].grouping){
			var parentDisc = 0,parentInvestment=0,parenttotalDisc=0,idxval=0,parentDiscPer = 0, parentclearnsPeriodWithStock=0, parentclearnsPeriodWithoutStock=0,parentrateOfInterestWithInhouse=0,parentrateOfInterestWithoutInhouse=0;
			var sumOfTargetCase = 0;
			for (var k = 0; k < data.Brands.length; k++) {
				sumOfTargetCase += data.Brands[k].targetCase;
				if(brandno == data.Brands[k].brandNo){
					var pending = 0;
					if((targetcase - $scope.discountdata[j].Brands[k].liftedCase) > 0)
					 pending = targetcase - $scope.discountdata[j].Brands[k].liftedCase;
					
					$scope.discountdata[j].Brands[k].targetCase = targetcase;
					$scope.discountdata[j].Brands[k].pending = pending;
				}
				
			}
			$scope.discountdata[j].sumOfTarget = sumOfTargetCase;
			$scope.discountdata[j].pendingvalue = ($scope.discountdata[j].sumOfTarget - $scope.discountdata[j].liftedcase);
			$scope.discountdata[j].currentIndent = ($scope.discountdata[j].sumOfTarget - $scope.discountdata[j].liftedcase);
			
			$scope.splitCurrentIndent($scope.discountdata[j].currentIndent, data);
		}
	}
}*/
$scope.splitDiscount = function(discountAmt, data){
	for (var j = 0; j < $scope.discountdata.length; j++) {
		if(data.grouping == $scope.discountdata[j].grouping){
			var  parentrateOfInterestWithInhouse=0,parentrateOfInterestWithoutInhouse=0,idxval=0; 
			for (var k = 0; k < data.Brands.length; k++) {
				$scope.discountdata[j].Brands[k].discountAmount = discountAmt;
				$scope.discountdata[j].Brands[k].totalDiscount = ($scope.discountdata[j].Brands[k].currentIndent * discountAmt);
				
				if($scope.discountdata[j].Brands[k].caseRate > 0)
					$scope.discountdata[j].Brands[k].discountPer = ((discountAmt * 100) / $scope.discountdata[j].Brands[k].caseRate).toFixed(2);
				  else
					  $scope.discountdata[j].Brands[k].discountPer = 0;
				
				var profitForTheMonth = (discountAmt * 100)/($scope.discountdata[j].Brands[k].caseRate);
				
				  if(profitForTheMonth > 0 && $scope.discountdata[j].Brands[k].estimationMonth > 0){
					  $scope.discountdata[j].Brands[k].rateOfInterestWithInhouse = ((profitForTheMonth)/(($scope.discountdata[j].Brands[k].currentIndent + $scope.discountdata[j].Brands[k].inhouseStock) / $scope.discountdata[j].Brands[k].lastMonthSold)).toFixed(2);
				  }else{
					  $scope.discountdata[j].Brands[k].rateOfInterestWithInhouse = 0;
				  }
				  if(profitForTheMonth > 0 && $scope.discountdata[j].Brands[k].clearnsPeriodWithoutInhouseStock > 0){
					  $scope.discountdata[j].Brands[k].rateOfInterestWithoutInhouse = ((profitForTheMonth)/($scope.discountdata[j].Brands[k].currentIndent / $scope.discountdata[j].Brands[k].lastMonthSold)).toFixed(2);
				  }else{
					  $scope.discountdata[j].Brands[k].rateOfInterestWithoutInhouse = 0;
				  }
				  parentrateOfInterestWithInhouse += parseFloat($scope.discountdata[j].Brands[k].rateOfInterestWithInhouse);
				  parentrateOfInterestWithoutInhouse += parseFloat($scope.discountdata[j].Brands[k].rateOfInterestWithoutInhouse);
				  idxval ++;
			}
			$scope.discountdata[j].totaldiscount = ($scope.discountdata[j].currentIndent * discountAmt);
			if($scope.discountdata[j].totaldiscount > 0)
			$scope.discountdata[j].parentDiscPer = (($scope.discountdata[j].totaldiscount * 100 ) / $scope.discountdata[j].investmentamount).toFixed(2);
			else
				$scope.discountdata[j].parentDiscPer = 0;
			$scope.discountdata[j].parentrateOfInterestWithInhouse = (parentrateOfInterestWithInhouse / idxval).toFixed(2);
			$scope.discountdata[j].parentrateOfInterestWithoutInhouse = (parentrateOfInterestWithoutInhouse / idxval).toFixed(2);
			//console.log(JSON.stringify($scope.discountdata[j]));
			//$scope.footerDetails();
		}
	}
	$scope.footerDetails();
}
$scope.greaterThan = function(prop1){
    return function(item){
      return item[prop1] == true;
    }
}
$scope.includeZeroSaleOrNot = function(){
	var catOrComp = $('#includeZeroSaleOrNot').prop('checked');
   if(catOrComp === true){
	    $scope.totalItem = true;
		$scope.discountItem = false;
   }else{
	   $scope.totalItem = false;
		$scope.discountItem = true;
   }
	   
}
$scope.footerDetails = function(){
	   var investmentData = 0,totalBeerData=0,totalLiquorData=0,totalDiscountData=0;
    angular.forEach($scope.discountdata, function(data) {
    	 angular.forEach(data.Brands, function(value) {
    		 if($scope.companyfilter == value.companyName && $scope.companyfilter != undefined){
    			 investmentData += value.investment;
        		 totalDiscountData += value.totalDiscount;
        		 if(value.productType == "LIQUOR")
        			 totalLiquorData +=value.currentIndent;
        		 else
        			 totalBeerData +=value.currentIndent;
    		 }else if($scope.companyfilter == undefined || $scope.companyfilter == null || $scope.companyfilter == ""){
    			 investmentData += value.investment;
        		 totalDiscountData += value.totalDiscount;
        		 if(value.productType == "LIQUOR")
        			 totalLiquorData +=value.currentIndent;
        		 else
        			 totalBeerData +=value.currentIndent;
    		 }
    	 });
    });
 $scope.investmentData = investmentData;
 $scope.totalBeerData = totalBeerData;
 $scope.totalLiquorData = totalLiquorData;
 $scope.totalDiscountData = totalDiscountData;
}
$scope.saveDiscountEstimate = function(){
	var jsonobject = {"discountDetails": [],}
	 angular.forEach($scope.discountdata, function(data) {
    	 angular.forEach(data.Brands, function(value) {
    		 var input = {
		        		"brandNo":value.brandNo+"",
		        		"discountAmount":value.discountAmount+"",
		        		"adjustment":value.adjustment,
		        		"discountTotalAmount":Math.round((value.discountAmount * (value.liftedCase+value.adjustment)))+"",
		        		"companyId":value.companyId+"",
		        		"target":value.targetCase,
		        		"totalDiscount":(value.discountAmount * (value.liftedCase+value.adjustment))+"",
		        		"currentIndent":value.currentIndent
		        };
    		 jsonobject.discountDetails.push(input);
    	 });
    });
	$scope.saveDiscountEstimationDetails(jsonobject);
}
$scope.saveDiscountEstimationDetails = function(jsonobject){
	//var date = changeDateFormateForMonth($("#monthlyDate").val());
	console.log(JSON.stringify(jsonobject));
	 var myUrl = window.location.href;
	  var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
	  var discountListUrl = url+"saveGroupingDiscountEstimateData?date="+$scope.finalDate;
			$http({
			    method : "POST",
			    contentType : 'application/json; charset=utf-8',
			    data: JSON.stringify(jsonobject),
			    url : discountListUrl,
			    headers: { 'x-my-custom-header': ip }
			  }).then(function mySuccess(response) {
				  alert(response.data.message);
				  location.reload(true);
			  }, function myError(response) {
				  alert("error"+response.data.message);
				  location.reload(true);
			  });
}
// Delete object from list
var deleteObj = [];
$scope.deleteObjectFromList = function(groupId,index){
    
	if(index == true)
		deleteObj.push(groupId);
	else
	 deleteObj.splice(deleteObj.indexOf(groupId),1);
	
}
$scope.deleteSelectedItems = function(){
	for(var j = 0; j < deleteObj.length; j++){
		for(var k = 0; k < $scope.discountdata.length; k++){
			if(deleteObj[j] == $scope.discountdata[k].grouping){
				$scope.discountdata.splice(k, 1);
			}
		}
		
	}
	deleteObj = [];
	$scope.footerDetails();
}

$scope.splitCurrentIndent = function(indentCase, data){
	for (var j = 0; j < $scope.discountdata.length; j++) {
		if(data.grouping == $scope.discountdata[j].grouping){
		var parentDiscPer = 0, parentclearnsPeriodWithStock=0, parentclearnsPeriodWithoutStock=0,investmentamount=0,totaldiscount=0,idxval=0;
		var parentrateOfInterestWithInhouse=0,parentrateOfInterestWithoutInhouse=0,currentIndent=0;
		
		for (var k = 0; k < $scope.discountdata[j].Brands.length; k++) {
			var clearnsPeriodWithStock=0,clearnsPeriodWithoutStock=0,rateOfInterestWithInhouse=0,rateOfInterestWithoutInhouse=0,discountPer=0;
			
			$scope.discountdata[j].Brands[k].currentIndent = $scope.divideCaseForChild($scope.discountdata[j].Brands[k].lastMonthSold,$scope.discountdata[j].totalSoldCase,indentCase);
			
			if($scope.discountdata[j].Brands[k].caseRate > 0)
				  discountPer = (($scope.discountdata[j].Brands[k].discountAmount * 100)/$scope.discountdata[j].Brands[k].caseRate).toFixed(2);
			  else
				  discountPer = 0;
			  
			if($scope.discountdata[j].Brands[k].lastMonthSold > 0){
			    clearnsPeriodWithStock=(($scope.discountdata[j].Brands[k].currentIndent + $scope.discountdata[j].Brands[k].inhouseStock) / $scope.discountdata[j].Brands[k].lastMonthSold);
				clearnsPeriodWithoutStock=($scope.discountdata[j].Brands[k].currentIndent / $scope.discountdata[j].Brands[k].lastMonthSold);
			}
			var profitForTheMonth = ($scope.discountdata[j].Brands[k].discountAmount*100)/($scope.discountdata[j].Brands[k].caseRate);
			
			  if(profitForTheMonth > 0 && clearnsPeriodWithStock > 0){
				  rateOfInterestWithInhouse = (profitForTheMonth)/(($scope.discountdata[j].Brands[k].currentIndent + $scope.discountdata[j].Brands[k].inhouseStock) / $scope.discountdata[j].Brands[k].lastMonthSold);
			  }
			  if(profitForTheMonth > 0 && clearnsPeriodWithoutStock > 0){
				  rateOfInterestWithoutInhouse = (profitForTheMonth)/($scope.discountdata[j].Brands[k].currentIndent / $scope.discountdata[j].Brands[k].lastMonthSold);
			  }
			  
			  $scope.discountdata[j].Brands[k].discountPer = discountPer;
			  $scope.discountdata[j].Brands[k].investment = ($scope.discountdata[j].Brands[k].caseRate * $scope.discountdata[j].Brands[k].currentIndent);
			  $scope.discountdata[j].Brands[k].estimationMonth = (clearnsPeriodWithStock).toFixed(2);
			  $scope.discountdata[j].Brands[k].clearnsPeriodWithoutInhouseStock = (clearnsPeriodWithoutStock).toFixed(2);
			  $scope.discountdata[j].Brands[k].rateOfInterestWithInhouse = (rateOfInterestWithInhouse).toFixed(2);
			  $scope.discountdata[j].Brands[k].rateOfInterestWithoutInhouse = (rateOfInterestWithoutInhouse).toFixed(2);
			  $scope.discountdata[j].Brands[k].totalDiscount = ($scope.discountdata[j].Brands[k].discountAmount * $scope.discountdata[j].Brands[k].currentIndent);
			  
			  investmentamount += $scope.discountdata[j].Brands[k].investment;
			  totaldiscount += $scope.discountdata[j].Brands[k].totalDiscount;
			  parentclearnsPeriodWithStock += clearnsPeriodWithStock;
			  parentclearnsPeriodWithoutStock += clearnsPeriodWithoutStock;
			  parentrateOfInterestWithInhouse += rateOfInterestWithInhouse;
			  parentrateOfInterestWithoutInhouse += rateOfInterestWithoutInhouse;
			  parentDiscPer += parseFloat(discountPer);
			  currentIndent += $scope.discountdata[j].Brands[k].currentIndent
			  
			  idxval++;
		}
		$scope.discountdata[j].investmentamount = investmentamount;
		$scope.discountdata[j].totaldiscount = totaldiscount;
		$scope.discountdata[j].parentclearnsPeriodWithStock = (parentclearnsPeriodWithStock / idxval).toFixed(2);
		$scope.discountdata[j].parentclearnsPeriodWithoutStock = (parentclearnsPeriodWithoutStock / idxval).toFixed(2);
		$scope.discountdata[j].parentrateOfInterestWithInhouse = (parentrateOfInterestWithInhouse / idxval).toFixed(2);
		$scope.discountdata[j].parentrateOfInterestWithoutInhouse = (parentrateOfInterestWithoutInhouse / idxval).toFixed(2);
		$scope.discountdata[j].parentDiscPer = (parentDiscPer / idxval).toFixed(2);
		$scope.discountdata[j].currentIndent = indentCase;
		$scope.discountdata[j].pendingvalue = ($scope.discountdata[j].sumOfTarget - ($scope.discountdata[j].currentIndent + $scope.discountdata[j].liftedcase));
		
	  }
	}
	$scope.footerDetails();
}
/**
 * This method is used to download discount pdf
 * */
  $scope.generateTargetPdf = function(company){
	  var date = $("#monthlyDate").val();
	  var company = $scope.$eval('companyfilter');
	  if(company != undefined && company != "" && company != null){
		  var myUrl = window.location.href;
		    var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
		  $.ajax({
		        url: url+'downloadtargetanddiscpercase?date='+$scope.finalDate+'&company='+company,
		        method: 'GET',
		        xhrFields: {
		            responseType: 'blob'
		        },
		        success: function (data) {
		            var a = document.createElement('a');
		            var url = window.URL.createObjectURL(data);
		            a.href = url;
		            a.download = company+'.pdf';
		            a.click();
		            window.URL.revokeObjectURL(url);
		        }
		    });
	  }else{
		  alert("Please select company.")
	  }
  }

});
app.filter('unique', function () {

    return function (items, filterOn) {

        if (filterOn === false) {
            return items;
        }

        if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
            var hashCheck = {}, newItems = [];

            var extractValueToCompare = function (item) {
                if (angular.isObject(item) && angular.isString(filterOn)) {
                    return item[filterOn];
                } else {
                    return item;
                }
            };

            angular.forEach(items, function (item) {
                var valueToCheck, isDuplicate = false;

                for (var i = 0; i < newItems.length; i++) {
                    if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                        isDuplicate = true;
                        break;
                    }
                }
                if (!isDuplicate) {
                    newItems.push(item);
                }

            });
            items = newItems;
        }
        return items;
    };
});