var app = angular.module('myApp', []);
var myObj;

app.controller('myCtrl', function($scope, $http) {
	var myUrl = window.location.href;
	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
	
	$scope.init = function () {
		$scope.getDateAndDaysFun();
		
	};
$scope.getDateAndDaysFun = function (){
	var callingUrl = url+"getDateAndDays";
	$http({
	    method : "GET",
	    url : callingUrl
	  }).then(function mySuccess(response) {
		  var custDate = formatDate(new Date());
		  $("#datepicker").val(custDate);
		  $("#schemedatepicker").val(custDate);
		  var enableDays = [custDate];
		  if(response.data.message != "" && response.data.message != null){
			  enableDays.push(response.data.message);
		  }
		  getTimeStamp(enableDays);
		  if(response.data.message != "" && response.data.message != null && response.data.customerId > 0){
			  $("#noOfDays").val(response.data.customerId);
			  if($("#datepicker").val() == response.data.message){
				  $("#noOfDays").val(response.data.customerId);
				  calculateIndentEstimate(response.data.customerId,response.data.message);
			  }
			  
		  }
			  
	  }, function myError(response) {
  
           });
}
	
	function formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;
	    return [day, month, year].join('-');
	   // return [year, month, day].join('-');
	}
	function getTimeStamp(enableDays){
		$("#datepicker").datepicker({
			  dateFormat: 'dd-mm-yy',  beforeShowDay: enableAllTheseDays,
			  numberOfMonths: 1,
			  onSelect: function(selected) {
			  $("#datepicker").datepicker("option","minDate", selected);
			  }
			  });
		    function enableAllTheseDays(date) {
		        var sdate = $.datepicker.formatDate( 'dd-mm-yy', date)
		        if($.inArray(sdate, enableDays) != -1) {
		            return [true];
		        }
		        return [false];
		    }
		    $('#datepicker').datepicker({dateFormat: 'dd-mm-yy', beforeShowDay: enableAllTheseDays});
	}
	$scope.getResults = function (){
		var noOfDays = $("#noOfDays").val();
		 var date = $("#datepicker").val();
		 if(validateCalendarField()==false){return false;}
		 calculateIndentEstimate(noOfDays,date);
		 
	}
	function validateCalendarField(){
		var noOfDays = $("#noOfDays").val();
		 var date = $("#datepicker").val();
		if(noOfDays == ""){
			alert("Please fill Days.");
			return false;
		}
		if(date == ""){
			alert("Please fill Date.");
			return false;
		}
		return true;
	}
	
function  calculateIndentEstimate(noOfDays,date){	
	$("#wait").css("display", "block");
var myUrl = window.location.href;
var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
var productListUrl = url+"getDiscountEsitmateDataWithDays?noOfDays="+noOfDays+"&date="+date;
$http({
    method : "GET",
    url : productListUrl
  }).then(function mySuccess(response) {
	 //console.log(JSON.stringify(response.data.discountEstimationBean));
	  $scope.month = response.data.date;
	  var inputjson = [];
	  var totalinvstval=0;
	  if(response.data.flag){
		  $.each(response.data.discountEstimationBean, function (index, value) {
			  var pendingVal = 0;
			  var pendingColor = "";
	    	  var pendingCase = value.commitment-value.liftedCase;
	    	  var needCase = 0;
	    	  if(pendingCase > 0){
	    		  needCase = value.needCase;
	    		  pendingVal = pendingCase;
	    		  pendingColor = "#DAFAC4";
	    	  }
	    	  else{
	    		  needCase = value.needCase;
	    		  pendingVal = 0;
	    		  pendingColor = "#D6EAF8";
	    	  }
	    	  
	    	  var colorcode =0;
	    	     if((needCase) > 0 || (value.commitment-value.liftedCase) > 0){
	    	    	 if((needCase) > 0 && (value.commitment-value.liftedCase) > 0)
	    	    		 colorcode=0;
	    	    	 else if((needCase) > 0 && (value.commitment-value.liftedCase) <= 0)
	    	    		 colorcode=1;
	    	    	 else 
	    	    		 colorcode=2;
	    	  var input = {
	    			    "colorcode":colorcode,
		        		"brandNo":value.brandNo,
		        		"brandName":value.brandName,
		        		"caseRate":value.caseRate,
		        		"category":value.category,
		        		"company":value.company,
		        		"noOfCases":value.noOfCases,
		        		"target":value.target,
		        		"lastMonthSold":value.lastMonthSold,
		        		"companyOrder":value.companyOrder,
		        		"companyColor":value.companyColor,
		        		"estimationMonth":0,
		        		"inHouseStock":value.inHouseStock,
		        		"p":value.p,
		        		"q":value.q,
		        		"x":value.x,
		        		"l2":value.l2,
		        		"l1":value.l1,
		        		"n":value.n,
		        		"sb":value.sb,
		        		"d":value.d,
		        		"lb":value.lb,
		        		"tin":value.tin,
		        		"psale":value.psale,
		        		"qsale":value.qsale,
		        		"xsale":value.xsale,
		        		"l2sale":value.l2Sale,
		        		"l1sale":value.l1Sale,
		        		"nsale":value.nsale,
		        		"sbsale":value.sbsale,
		        		"dsale":value.dsale,
		        		"lbsale":value.lbsale,
		        		"tinsale":value.tinsale,
		        		"pNeedCase":value.pNeedCase,
		        		"qNeedCase":value.qNeedCase,
		        		"xNeedCase":value.xNeedCase,
		        		"l2NeedCase":value.l2NeedCase,
		        		"l1NeedCase":value.l1NeedCase,
		        		"nNeedCase":value.nNeedCase,
		        		"sbNeedCase":value.sbNeedCase,
		        		"dNeedCase":value.dNeedCase,
		        		"lbNeedCase":value.lbNeedCase,
		        		"tinNeedCase":value.tinNeedCase,
		        		"pSpecialMargin":value.pspecialMargin,
		        		"qSpecialMargin":value.qspecialMargin,
		        		"xSpecialMargin":value.xspecialMargin,
		        		"l2SpecialMargin":value.l2SpecialMargin,
		        		"l1SpecialMargin":value.l1SpecialMargin,
		        		"nSpecialMargin":value.nspecialMargin,
		        		"sbSpecialMargin":value.sbspecialMargin,
		        		"dSpecialMargin":value.dspecialMargin,
		        		"lbSpecialMargin":value.lbspecialMargin,
		        		"tinSpecialMargin":value.tinspecialMargin,
		        		"pStock":value.pstock,
		        		"qStock":value.qstock,
		        		"xStock":value.xstock,
		        		"l2Stock":value.l2Stock,
		        		"l1Stock":value.l1Stock,
		        		"nStock":value.nstock,
		        		"sbStock":value.sbstock,
		        		"dStock":value.dstock,
		        		"lbStock":value.lbstock,
		        		"tinStock":value.tinstock,
		        		"pending":pendingVal,
		        		"liftedCase":value.liftedCase,
		        		"commitment":value.commitment,
		        		"totalinvestment":(needCase * value.caseRate),
		        		"pVal":Math.round(value.pNeedCase),
		        		"qVal":Math.round(value.qNeedCase),
		        		"xVal":Math.round(value.xNeedCase),
		        		"l2Val":Math.round(value.l2NeedCase),
		        		"l1Val":Math.round(value.l1NeedCase),
		        		"nVal":Math.round(value.nNeedCase),
		        		"sbVal":Math.round(value.sbNeedCase),
		        		"dVal":Math.round(value.dNeedCase),
		        		"lbVal":Math.round(value.lbNeedCase),
		        		"tinVal":Math.round(value.tinNeedCase),
		        		"l2perday": (value.l2perday).toFixed(1),
		        	    "l1perday": (value.l1perday).toFixed(1),
		        	    "qperday": (value.qperday).toFixed(1),
		        	    "pperday": (value.pperday).toFixed(1),
		        	    "nperday": (value.nperday).toFixed(1),
		        	    "dperday": (value.dperday).toFixed(1),
		        	    "lbperday": (value.lbperday).toFixed(1),
		        	    "sbperday": (value.sbperday).toFixed(1),
		        	    "xperday": (value.xperday).toFixed(1),
		        	    "tinperday": (value.tinperday).toFixed(1),
		        		"needCase":needCase,
		        		"OldneedCase":value.needCase,
		        		"productType":value.productType,
		        		"liftedCase":value.liftedCase,
		        		"totalSpecialMrp":((Math.round(value.pNeedCase)*value.pspecialMargin)+(Math.round(value.qNeedCase)*value.qspecialMargin)+(Math.round(value.xNeedCase)*value.xspecialMargin)+
		        				(Math.round(value.l2NeedCase)*value.l2SpecialMargin)+(Math.round(value.l1NeedCase)*value.l1SpecialMargin)+(Math.round(value.nNeedCase)*value.nspecialMargin)
		        				+(Math.round(value.sbNeedCase)*value.sbspecialMargin)+(Math.round(value.dNeedCase)*value.dspecialMargin)+(Math.round(value.lbNeedCase)*value.lbspecialMargin)
		        				+(Math.round(value.tinNeedCase)*value.tinspecialMargin))

		        };
	    	  inputjson.push(input);
	    	 }
	      });
	  }else{
		  $.each(response.data.discountEstimationBean, function (index, value) {
	    	  var colorcode =0;
	    	     if((value.needCase) > 0 || (value.otherPending) > 0){
	    	    	 if((value.needCase) > 0 && (value.otherPending) > 0)
	    	    		 colorcode=0;
	    	    	 else if((value.needCase) > 0 && (value.otherPending) <= 0)
	    	    		 colorcode=1;
	    	    	 else 
	    	    		 colorcode=2;
	    	  var input = {
	    			    "colorcode":colorcode,
		        		"brandNo":value.brandNo,
		        		"brandName":value.brandName,
		        		"caseRate":value.caseRate,
		        		"category":value.category,
		        		"company":value.company,
		        		"noOfCases":value.noOfCases,
		        		"target":value.target,
		        		"lastMonthSold":value.lastMonthSold,
		        		"companyOrder":value.companyOrder,
		        		"companyColor":value.companyColor,
		        		"estimationMonth":0,
		        		"inHouseStock":value.inHouseStock,
		        		"p":value.p,
		        		"q":value.q,
		        		"x":value.x,
		        		"l2":value.l2,
		        		"l1":value.l1,
		        		"n":value.n,
		        		"sb":value.sb,
		        		"d":value.d,
		        		"lb":value.lb,
		        		"tin":value.tin,
		        		"psale":value.psale,
		        		"qsale":value.qsale,
		        		"xsale":value.xsale,
		        		"l2sale":value.l2Sale,
		        		"l1sale":value.l1Sale,
		        		"nsale":value.nsale,
		        		"sbsale":value.sbsale,
		        		"dsale":value.dsale,
		        		"lbsale":value.lbsale,
		        		"tinsale":value.tinsale,
		        		"pNeedCase":value.pNeedCase,
		        		"qNeedCase":value.qNeedCase,
		        		"xNeedCase":value.xNeedCase,
		        		"l2NeedCase":value.l2NeedCase,
		        		"l1NeedCase":value.l1NeedCase,
		        		"nNeedCase":value.nNeedCase,
		        		"sbNeedCase":value.sbNeedCase,
		        		"dNeedCase":value.dNeedCase,
		        		"lbNeedCase":value.lbNeedCase,
		        		"tinNeedCase":value.tinNeedCase,
		        		"pSpecialMargin":value.pspecialMargin,
		        		"qSpecialMargin":value.qspecialMargin,
		        		"xSpecialMargin":value.xspecialMargin,
		        		"l2SpecialMargin":value.l2SpecialMargin,
		        		"l1SpecialMargin":value.l1SpecialMargin,
		        		"nSpecialMargin":value.nspecialMargin,
		        		"sbSpecialMargin":value.sbspecialMargin,
		        		"dSpecialMargin":value.dspecialMargin,
		        		"lbSpecialMargin":value.lbspecialMargin,
		        		"tinSpecialMargin":value.tinspecialMargin,
		        		"pStock":value.pstock,
		        		"qStock":value.qstock,
		        		"xStock":value.xstock,
		        		"l2Stock":value.l2Stock,
		        		"l1Stock":value.l1Stock,
		        		"nStock":value.nstock,
		        		"sbStock":value.sbstock,
		        		"dStock":value.dstock,
		        		"lbStock":value.lbstock,
		        		"tinStock":value.tinstock,
		        		"pending":value.otherPending,
		        		"liftedCase":value.liftedCase,
		        		"commitment":value.commitment,
		        		"totalinvestment":value.otherInvestment,
		        		"pVal":value.pVal,
		        		"qVal":value.qVal,
		        		"xVal":value.xVal,
		        		"l2Val":value.l2Val,
		        		"l1Val":value.l1Val,
		        		"nVal":value.nVal,
		        		"sbVal":value.sbVal,
		        		"dVal":value.dVal,
		        		"lbVal":value.lbVal,
		        		"tinVal":value.tinVal,
		        		"l2perday": value.l2perday,
		        	    "l1perday": value.l1perday,
		        	    "qperday": value.qperday,
		        	    "pperday": value.pperday,
		        	    "nperday": value.nperday,
		        	    "dperday": value.dperday,
		        	    "lbperday": value.lbperday,
		        	    "sbperday": value.sbperday,
		        	    "xperday": value.xperday,
		        	    "tinperday": value.tinperday,
		        		"needCase":value.needCase,
		        		"OldneedCase":value.needCase,
		        		"productType":value.productType,
		        		"liftedCase":value.liftedCase,
		        		"totalSpecialMrp":value.totalSpecialMrp
		        };
	    	  inputjson.push(input);
	    	  
	    	 }
	      });
	  }
	  $("#wait").css("display", "none");
      $scope.alldetails = inputjson;
      $scope.totalInvstAmt();
     //console.log(JSON.stringify(inputjson));
    
  }, function myError(response) {
    //alert(response);
  });

}
$scope.calculateUpdatedNeedCase = function(targetcase,brandNo,caserate,L2,L1,Q,P,N,D,sb,lb,tin,SpecialMarginL2,SpecialMarginL1,SpecialMarginQ,SpecialMarginP,SpecialMarginN,SpecialMarginD,SpecialMarginsb,SpecialMarginlb,SpecialMargintin,L2NeedCase,L1NeedCase,QNeedCase,PNeedCase,NNeedCase,DNeedCase,sbNeedCase,lbNeedCase,tinNeedCase){
	var totalMrpRoundOffWithSpecialMrp = 0;
	
	//$('#investment'+brandNo).html(Math.round(targetcase*caserate));
	var tempNeedCase = (L2NeedCase + L1NeedCase + QNeedCase+PNeedCase + NNeedCase + DNeedCase + sbNeedCase + lbNeedCase + tinNeedCase);
	var noofcases= targetcase - tempNeedCase;
	var p=PNeedCase,q=QNeedCase,l2=L2NeedCase,l1=L1NeedCase,n=NNeedCase,sb=sbNeedCase,d=DNeedCase,lb=lbNeedCase,tin=tinNeedCase;
	var totalcases=(L2+L1+Q+P+N+D+sb+lb+tin);
	if(targetcase >= tempNeedCase){
		 p += stockDivive(P,noofcases,totalcases);
		 q += stockDivive(Q,noofcases,totalcases);
		 l2 += stockDivive(L2,noofcases,totalcases);
		 l1 += stockDivive(L1,noofcases,totalcases);
		 n  += stockDivive(N,noofcases,totalcases);
		 sb += stockDivive(sb,noofcases,totalcases);
		 d  += stockDivive(D,noofcases,totalcases);
		 lb += stockDivive(lb,noofcases,totalcases);
		 tin += stockDivive(tin,noofcases,totalcases);
		 
	}else{
		 p=0,q=0,l2=0,l1=0,n=0,sb=0,d=0,lb=0,tin=0;
	}
	
	totalMrpRoundOffWithSpecialMrp = ((Math.round(l2)*SpecialMarginL2)+(Math.round(l1)*SpecialMarginL1)+(Math.round(q)*SpecialMarginQ)
			 +(Math.round(p)*SpecialMarginP)+(Math.round(n)*SpecialMarginN)+(Math.round(d)*SpecialMarginD)+(Math.round(sb)*SpecialMarginsb)
			 +(Math.round(lb)*SpecialMarginlb)+(Math.round(tin)*SpecialMargintin));
	
	for (j = 0; j < $scope.alldetails.length; j++) {
		if(brandNo == $scope.alldetails[j].brandNo){
			$scope.alldetails[j].totalinvestment = targetcase*caserate;
			$scope.alldetails[j].needCase = parseInt(targetcase);
			$scope.alldetails[j].l2Val = Math.round(l2);
			$scope.alldetails[j].l1Val = Math.round(l1);
			$scope.alldetails[j].qVal = Math.round(q);
			$scope.alldetails[j].pVal = Math.round(p);
			$scope.alldetails[j].nVal = Math.round(n);
			$scope.alldetails[j].dVal = Math.round(d);
			$scope.alldetails[j].sbVal = Math.round(sb);
			$scope.alldetails[j].lbVal = Math.round(lb);
			$scope.alldetails[j].tinVal = Math.round(tin);
			$scope.alldetails[j].totalSpecialMrp = Math.round(totalMrpRoundOffWithSpecialMrp);
			 $scope.totalInvstAmt();
		}
			
	}
  };
  
  
$scope.buildModelPopupData = function(){
	//$('.selectpicker').selectpicker();
    $('#addBtnForNewProduct').css('display','none');
	var array1 =[];
	var table = $("#indentEstimate tbody");
	table.find('tr').each(function (i) {
		 var $tds = $(this).find('td'),
		 name = $tds.eq(1).text(),
		 brandNo = parseFloat($tds.eq(2).text())
		 var input = {
     		"brandName":name,
     		"brandNo":brandNo
     };
		 array1.push(input);
	 });
	
	var myUrl = window.location.href;
	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
	var productListUrl = url+"getDistinctBrandNameAndNo.json";
	$http({
	    method : "GET",
	    url : productListUrl
	  }).then(function mySuccess(response) {
		  $scope.productList = response.data;
		  var array2 = $scope.productList;
		  remove_duplicates(array1,array2);
		  var brand =new Array();
		  $.each(array2, function (index, value) {
			  brand.push(value.brandName+" "+value.brandNo);
	      });
		  $("#brandNoVal").select2({
			  data: brand
			});
		  $("#myModal").modal("show");
		  
	  }, function myError(response) {
	    //alert(response);
	  });
	
	
}

$scope.greaterThan = function(prop, val){
    return function(item){
      return item[prop] > val;
    }
}
$scope.deleteRow = function(tableID){
	try {
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;

		for(var i=0; i<rowCount; i++) {
			var row = table.rows[i];
			var chkbox = row.cells[0].childNodes[0];
			if(null != chkbox && true == chkbox.checked) {
				table.deleteRow(i);
				rowCount--;
				i--;
				var brandNumber = Number(row.cells[2].childNodes[0].nodeValue);
				for (j = 0; j < $scope.alldetails.length; j++) {
					if(brandNumber == $scope.alldetails[j].brandNo)
						$scope.alldetails.splice(j, 1);
				}
			}
		}
		}catch(e) {
			alert(e);
		}
		$scope.totalInvstAmt();
		
};
  $scope.getPeroductDetails = function() {
	  var inputdate = $("#datepicker").val();
	  var noOfDays = $("#noOfDays").val();
	  var brandname = $("#brandNoVal option:selected").text();
		var n = brandname.split(" ");
	    var brandNoVal = n[n.length - 1];
	  $('#addBtnForNewProduct').css('display','block');
	 // var brandNoVal = $scope.brandNoVal;
	  var myUrl = window.location.href;
		var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
		var productListUrl = url+"getDiscountEsitmateDataWithBrandNo?brandNoVal="+brandNoVal+"&inputdate="+inputdate+"&noOfDays="+noOfDays;
		$http({
		    method : "GET",
		    url : productListUrl
		  }).then(function mySuccess(response) {
			  //console.log(JSON.stringify(response.data));
			  $scope.singleBrandModelPopup = response.data;
			  var pendingCase = response.data.commitment - response.data.liftedCase;
			  var pendingValscope = 0;
			  var pendingColorScope ="";
			  var colorCodeVal=0;
			  if(pendingCase > 0){
	    		  pendingValscope = pendingCase;
	    		  pendingColorScope = "#DAFAC4";
	    		  colorCodeVal=0;
	    	  }
	    	  else{
	    		  pendingValscope = 0;
	    		  pendingColorScope = "#D6EAF8";
	    		  colorCodeVal=1;
	    	  }
			  $scope.pendingValscope = pendingValscope;
			  $scope.pendingColorScope = pendingColorScope;
			  $scope.colorCodeVal = colorCodeVal;
			  var totalcasepupop = response.data.l2+response.data.l1+response.data.q+response.data.p+response.data.n+response.data.d+response.data.lb+response.data.sb+response.data.tin;
			  $scope.totalcasepupop = totalcasepupop;
			  $scope.l2DistributeAgain = 0;
				$scope.l1DistributeAgain = 0;
				$scope.QDistributeAgain = 0;
				$scope.PDistributeAgain = 0;
				$scope.NDistributeAgain = 0;
				$scope.DDistributeAgain = 0;
				$scope.SBDistributeAgain = 0;
				$scope.LBDistributeAgain = 0;
				$scope.TINDistributeAgain = 0;
				$scope.totalMrpRoundOffWithSpecialMrp = 0;
				
				$("#newProductCase").val(0);
		  }, function myError(response) {
		    //alert(response);
		  });
	
	};
	$scope.newProduct = function(noofcases,L2,L1,Q,P,N,D,lb,sb,tin,SpecialMarginL2,SpecialMarginL1,SpecialMarginQ,SpecialMarginP,SpecialMarginN,SpecialMarginD,SpecialMarginsb,SpecialMarginlb,SpecialMargintin,totalcases) {
		 $scope.noofcasespupop = noofcases;
		 var totalMrpRoundOffWithSpecialMrp = 0;
		 if(totalcases > 0){
		 $scope.l2DistributeAgain = Math.round(stockDivive(L2,noofcases,totalcases));
		 $scope.l1DistributeAgain = Math.round(stockDivive(L1,noofcases,totalcases));
		 $scope.QDistributeAgain = Math.round(stockDivive(Q,noofcases,totalcases));
		 $scope.PDistributeAgain = Math.round(stockDivive(P,noofcases,totalcases));
		 $scope.NDistributeAgain = Math.round(stockDivive(N,noofcases,totalcases));
		 $scope.DDistributeAgain = Math.round(stockDivive(D,noofcases,totalcases));
		 $scope.SBDistributeAgain = Math.round(stockDivive(sb,noofcases,totalcases));
		 $scope.LBDistributeAgain = Math.round(stockDivive(lb,noofcases,totalcases));
		 $scope.TINDistributeAgain = Math.round(stockDivive(tin,noofcases,totalcases));
		 $scope.totalMrpRoundOffWithSpecialMrp = ((Math.round($scope.l2DistributeAgain)*SpecialMarginL2)+(Math.round($scope.l1DistributeAgain)*SpecialMarginL1)+(Math.round($scope.QDistributeAgain)*SpecialMarginQ)
				 +(Math.round($scope.PDistributeAgain)*SpecialMarginP)+(Math.round($scope.NDistributeAgain)*SpecialMarginN)+(Math.round($scope.DDistributeAgain)*SpecialMarginD)+(Math.round($scope.SBDistributeAgain)*SpecialMarginsb)
				 +(Math.round($scope.LBDistributeAgain)*SpecialMarginlb)+(Math.round($scope.TINDistributeAgain)*SpecialMargintin));
		 }else{
			 $scope.l2DistributeAgain = 0;
				$scope.l1DistributeAgain = 0;
				$scope.QDistributeAgain = 0;
				$scope.PDistributeAgain = 0;
				$scope.NDistributeAgain = 0;
				$scope.DDistributeAgain = 0;
				$scope.SBDistributeAgain = 0;
				$scope.LBDistributeAgain = 0;
				$scope.TINDistributeAgain = 0;
				$scope.totalMrpRoundOffWithSpecialMrp = 0;
		 }
			
	 }
	 $(".add-row").click(function(){
		 var newProductCase = $("#newProductCase").val();
		 if(!(newProductCase == null || newProductCase == "" || newProductCase <= 0)){
			 var addnewObj =  $scope.alldetails;
			 var input = {
	 			        "colorcode":$scope.colorCodeVal,
		        		"brandNo":$scope.singleBrandModelPopup.brandNo,
		        		"brandName":$scope.singleBrandModelPopup.brandName,
		        		"caseRate":$scope.singleBrandModelPopup.caseRate,
		        		"category":$scope.singleBrandModelPopup.category,
		        		"company":$scope.singleBrandModelPopup.company,
		        		"noOfCases":newProductCase,
		        		"target":$scope.singleBrandModelPopup.target,
		        		"lastMonthSold":$scope.singleBrandModelPopup.lastMonthSold,
		        		"companyOrder":$scope.singleBrandModelPopup.companyOrder,
		        		"companyColor":$scope.singleBrandModelPopup.companyColor,
		        		"estimationMonth":0,
		        		"inHouseStock":$scope.singleBrandModelPopup.inHouseStock,
		        		"p":$scope.singleBrandModelPopup.p,
		        		"q":$scope.singleBrandModelPopup.q,
		        		"x":$scope.singleBrandModelPopup.x,
		        		"l2":$scope.singleBrandModelPopup.l2,
		        		"l1":$scope.singleBrandModelPopup.l1,
		        		"n":$scope.singleBrandModelPopup.n,
		        		"sb":$scope.singleBrandModelPopup.sb,
		        		"d":$scope.singleBrandModelPopup.d,
		        		"lb":$scope.singleBrandModelPopup.lb,
		        		"tin":$scope.singleBrandModelPopup.tin,
		        		"psale":$scope.singleBrandModelPopup.psale,
		        		"qsale":$scope.singleBrandModelPopup.qsale,
		        		"xsale":$scope.singleBrandModelPopup.xsale,
		        		"l2sale":$scope.singleBrandModelPopup.l2Sale,
		        		"l1sale":$scope.singleBrandModelPopup.l1Sale,
		        		"nsale":$scope.singleBrandModelPopup.nsale,
		        		"sbsale":$scope.singleBrandModelPopup.sbsale,
		        		"dsale":$scope.singleBrandModelPopup.dsale,
		        		"lbsale":$scope.singleBrandModelPopup.lbsale,
		        		"tinsale":$scope.singleBrandModelPopup.tinsale,
		        		"pNeedCase":0,
		        		"qNeedCase":0,
		        		"xNeedCase":0,
		        		"l2NeedCase":0,
		        		"l1NeedCase":0,
		        		"nNeedCase":0,
		        		"sbNeedCase":0,
		        		"dNeedCase":0,
		        		"lbNeedCase":0,
		        		"tinNeedCase":0,
		        		"pSpecialMargin":$scope.singleBrandModelPopup.pspecialMargin,
		        		"qSpecialMargin":$scope.singleBrandModelPopup.qspecialMargin,
		        		"xSpecialMargin":$scope.singleBrandModelPopup.xspecialMargin,
		        		"l2SpecialMargin":$scope.singleBrandModelPopup.l2SpecialMargin,
		        		"l1SpecialMargin":$scope.singleBrandModelPopup.l1SpecialMargin,
		        		"nSpecialMargin":$scope.singleBrandModelPopup.nspecialMargin,
		        		"sbSpecialMargin":$scope.singleBrandModelPopup.sbspecialMargin,
		        		"dSpecialMargin":$scope.singleBrandModelPopup.dspecialMargin,
		        		"lbSpecialMargin":$scope.singleBrandModelPopup.lbspecialMargin,
		        		"tinSpecialMargin":$scope.singleBrandModelPopup.tinspecialMargin,
		        		"pStock":$scope.singleBrandModelPopup.pstock,
		        		"qStock":$scope.singleBrandModelPopup.qstock,
		        		"xStock":$scope.singleBrandModelPopup.xstock,
		        		"l2Stock":$scope.singleBrandModelPopup.l2Stock,
		        		"l1Stock":$scope.singleBrandModelPopup.l1Stock,
		        		"nStock":$scope.singleBrandModelPopup.nstock,
		        		"sbStock":$scope.singleBrandModelPopup.sbstock,
		        		"dStock":$scope.singleBrandModelPopup.dstock,
		        		"lbStock":$scope.singleBrandModelPopup.lbstock,
		        		"tinStock":$scope.singleBrandModelPopup.tinstock,
		        		"pending": $scope.pendingValscope,
		        		"liftedCase":$scope.singleBrandModelPopup.liftedCase,
		        		"commitment":$scope.singleBrandModelPopup.commitment,
		        		"totalinvestment":($scope.noofcasespupop * $scope.singleBrandModelPopup.caseRate),
		        		"pVal":$scope.PDistributeAgain,
		        		"qVal":$scope.QDistributeAgain,
		        		"xVal":$scope.l2DistributeAgain,
		        		"l2Val":$scope.l2DistributeAgain,
		        		"l1Val":$scope.l1DistributeAgain,
		        		"nVal":$scope.NDistributeAgain,
		        		"sbVal":$scope.SBDistributeAgain,
		        		"dVal":$scope.DDistributeAgain,
		        		"lbVal":$scope.LBDistributeAgain,
		        		"tinVal":$scope.TINDistributeAgain,
		        		"l2perday": ($scope.singleBrandModelPopup.l2perday).toFixed(1),
		        	    "l1perday": ($scope.singleBrandModelPopup.l1perday).toFixed(1),
		        	    "qperday": ($scope.singleBrandModelPopup.qperday).toFixed(1),
		        	    "pperday": ($scope.singleBrandModelPopup.pperday).toFixed(1),
		        	    "nperday": ($scope.singleBrandModelPopup.nperday).toFixed(1),
		        	    "dperday": ($scope.singleBrandModelPopup.dperday).toFixed(1),
		        	    "lbperday": ($scope.singleBrandModelPopup.lbperday).toFixed(1),
		        	    "sbperday": ($scope.singleBrandModelPopup.sbperday).toFixed(1),
		        	    "xperday": ($scope.singleBrandModelPopup.xperday).toFixed(1),
		        	    "tinperday": ($scope.singleBrandModelPopup.tinperday).toFixed(1),
		        		"needCase":$scope.noofcasespupop,
		        		"OldneedCase":0,
		        		"productType":$scope.singleBrandModelPopup.productType,
		        		"liftedCase":$scope.singleBrandModelPopup.liftedCase,
		        		"totalSpecialMrp":$scope.totalMrpRoundOffWithSpecialMrp
		        };
				 addnewObj.push(input);
				 $scope.totalInvstAmt();
					
			 	$('#myModal').modal('hide');
		       }else{
		    	   alert("Field should not be empty!");
		       }
		 
	 });
  
	 
	 $scope.editRow = function(item){
		var brandNo = item.brandNo
		$('#editPopupInvestment').html(parseInt($("#investment"+brandNo).text()));
		$('#editPopupNeedCase').html(parseInt($("#newcase"+brandNo).val()));
		 $('#popupSpecialMrp').html(parseInt($("#specialMrp"+brandNo).text()));
		 
		 $("#2lPopup").val(parseInt($("#2l"+brandNo).text()));
	      $("#1lPopup").val(parseInt($("#1l"+brandNo).text()));
	      $("#qPopup").val(parseInt($("#q"+brandNo).text()));
	      $("#pPopup").val(parseInt($("#p"+brandNo).text()));
	      $("#nPopup").val(parseInt($("#n"+brandNo).text()));
	      $("#dPopup").val(parseInt($("#d"+brandNo).text()));
	      $("#lbPopup").val(parseInt($("#lb"+brandNo).text()));
	      $("#sbPopup").val(parseInt($("#sb"+brandNo).text()));
	      $("#tinPopup").val(parseInt($("#tin"+brandNo).text()))
		  
		   $scope.editRowModelPopupItem = item;
		 $("#editModal").modal("show");
		 
			
		}
	 $scope.editChnage = function(currentVal,caseRate,brandNo,SpecialMarginL2,SpecialMarginL1,SpecialMarginQ,SpecialMarginP,SpecialMarginN,SpecialMarginD,SpecialMarginsb,SpecialMarginlb,SpecialMargintin){
      // var l2 = parseInt($("2lPopup"+brandNo).text());
       var l2 = parseInt($("#2lPopup").val());
       var l1 = parseInt($("#1lPopup").val());
       var q = parseInt($("#qPopup").val());
       var p = parseInt($("#pPopup").val());
       var n = parseInt($("#nPopup").val());
       var d = parseInt($("#dPopup").val());
       var lb = parseInt($("#lbPopup").val());
       var sb = parseInt($("#sbPopup").val());
       var tin = parseInt($("#tinPopup").val());
      var totalCase = (l2+l1+q+p+n+d+lb+sb+tin);
     var popupSpecialMrp = ((Math.round(l2)*SpecialMarginL2)+(Math.round(l1)*SpecialMarginL1)+(Math.round(q)*SpecialMarginQ)
				 +(Math.round(p)*SpecialMarginP)+(Math.round(n)*SpecialMarginN)+(Math.round(d)*SpecialMarginD)+(Math.round(sb)*SpecialMarginsb)
				 +(Math.round(lb)*SpecialMarginlb)+(Math.round(tin)*SpecialMargintin));
      $('#editPopupInvestment').html(totalCase * caseRate);
      $('#editPopupNeedCase').html(totalCase);
      $('#popupSpecialMrp').html(popupSpecialMrp);
		 
	 }
	 $scope.updateRowValue = function(brandNo){
		 var editPopupNeedCase = parseInt($("#editPopupNeedCase").text());
		     if(editPopupNeedCase >= 0){
		    	 for (j = 0; j < $scope.alldetails.length; j++) {
		    			if(brandNo == $scope.alldetails[j].brandNo){
		    				$scope.alldetails[j].totalinvestment = parseInt($("#editPopupInvestment").text());
		    				$scope.alldetails[j].needCase = parseInt($("#editPopupNeedCase").text());
		    				$scope.alldetails[j].l2Val = Math.round(parseInt($("#2lPopup").val()));
		    				$scope.alldetails[j].l1Val = Math.round(parseInt($("#1lPopup").val()));
		    				$scope.alldetails[j].qVal = Math.round(parseInt($("#qPopup").val()));
		    				$scope.alldetails[j].pVal = Math.round(parseInt($("#pPopup").val()));
		    				$scope.alldetails[j].nVal = Math.round(parseInt($("#nPopup").val()));
		    				$scope.alldetails[j].dVal = Math.round(parseInt($("#dPopup").val()));
		    				$scope.alldetails[j].sbVal = Math.round(parseInt($("#sbPopup").val()));
		    				$scope.alldetails[j].lbVal = Math.round(parseInt($("#lbPopup").val()));
		    				$scope.alldetails[j].tinVal = Math.round(parseInt($("#tinPopup").val()));
		    				$scope.alldetails[j].totalSpecialMrp = Math.round(parseInt($("#popupSpecialMrp").text()));
		    				 $scope.totalInvstAmt();
		    			}
		    				
		    		}
		  
			
		 	$('#editModal').modal('hide');
		       }else{
		    	   alert("Field should not be empty!");
		       }
		 }
	 
	 //--------------- POST request -------------------// 
	 
	 $scope.savedIndentEstimate = function(){
		 var noOfDays = $("#noOfDays").val();
		 var date = changeDateFormat($("#datepicker").val());
		 
		 if(noOfDays !="" && noOfDays > 0){
		 var json = {
				    "indentDetails": []
		        };
		 
		 var table = $("#indentEstimate tbody");
		 	table.find('tr').each(function (i) {
		 		 var $tds = $(this).find('td'),
		 		 brandName = $tds.eq(1).text(),
		 		 brandNo = parseInt($tds.eq(2).text()),
		 		 caseRate = parseFloat($tds.eq(3).text()),
		 		 lastMonthSold = parseInt($tds.eq(69).text()),
		 		 //inhousestock = parseInt($tds.eq(5).text()),
		 		 commitment = parseInt($tds.eq(6).text()),
		 	     liftedCase = parseInt($tds.eq(7).text()),
		 	     pending = parseInt($tds.eq(8).text()),
		 	     needCase = parseInt($tds.eq(9).text()),
		 	     investMent = parseInt($tds.eq(10).text()),
		 	     l2Val = parseInt($tds.eq(11).text()),
		 	     l1Val = parseInt($tds.eq(12).text()),
		 	     qVal = parseInt($tds.eq(13).text()),
		 	     pVal = parseInt($tds.eq(14).text()),
		 	     nVal = parseInt($tds.eq(15).text()),
		 	     dVal = parseInt($tds.eq(16).text()),
		 	     lbVal = parseInt($tds.eq(17).text()),
		 	     sbVal = parseInt($tds.eq(18).text()),
		 	     tinVal = parseInt($tds.eq(19).text()),
		 	    totalSpecialMrp = parseFloat($tds.eq(20).text()),
		 	   //  needCase = parseInt($tds.eq(21).text()),
		 	     target = parseInt($tds.eq(22).text()),
		 	     companyOrder = parseInt($tds.eq(23).text()),
		 	     companyColor =$tds.eq(24).text(),
		 	     productType = $tds.eq(25).text(),
		 	     l2 = parseFloat($tds.eq(26).text()),
		 	     l1 = parseFloat($tds.eq(27).text()),
		 	     q = parseFloat($tds.eq(28).text()),
		 	     p = parseFloat($tds.eq(29).text()),
		 	     n = parseFloat($tds.eq(30).text()),
		 	     d = parseFloat($tds.eq(31).text()),
		 	     sb = parseFloat($tds.eq(32).text()),
		 	     lb = parseFloat($tds.eq(33).text()),
		 	     tin = parseFloat($tds.eq(34).text()),
		 	     x = parseFloat($tds.eq(35).text()),
		 	    l2SpecialMargin = parseFloat($tds.eq(36).text()),
		 	    l1SpecialMargin = parseFloat($tds.eq(37).text()),
		 	    qSpecialMargin = parseFloat($tds.eq(38).text()),
		 	    pSpecialMargin = parseFloat($tds.eq(39).text()),
		 	    nSpecialMargin = parseFloat($tds.eq(40).text()),
		    	dSpecialMargin = parseFloat($tds.eq(41).text()),
		 	    sbSpecialMargin = parseFloat($tds.eq(42).text()),
		 	    lbSpecialMargin = parseFloat($tds.eq(43).text()),
		 	    tinSpecialMargin = parseFloat($tds.eq(44).text()),
		 	    xSpecialMargin = parseFloat($tds.eq(45).text()),
		 	   l2Stock = parseFloat($tds.eq(46).text()),
		 	   l1Stock = parseFloat($tds.eq(47).text()),
		 	   qStock = parseFloat($tds.eq(48).text()),
		 	   pStock = parseFloat($tds.eq(49).text()),
		 	   nStock = parseFloat($tds.eq(50).text()),
		 	   dStock = parseFloat($tds.eq(51).text()),
		 	   sbStock = parseFloat($tds.eq(52).text()),
		 	   lbStock = parseFloat($tds.eq(53).text()),
		 	   tinStock = parseFloat($tds.eq(54).text()),
		 	   xStock = parseFloat($tds.eq(55).text()),
		 	   l2NeedCase = parseFloat($tds.eq(56).text()),
		 	   l1NeedCase = parseFloat($tds.eq(57).text()),
		 	   qNeedCase = parseFloat($tds.eq(58).text()),
		 	   pNeedCase = parseFloat($tds.eq(59).text()),
		 	   nNeedCase = parseFloat($tds.eq(60).text()),
		 	   dNeedCase = parseFloat($tds.eq(61).text()),
		 	   sbNeedCase = parseFloat($tds.eq(62).text()),
		 	   lbNeedCase = parseFloat($tds.eq(63).text()),
		 	   tinNeedCase = parseFloat($tds.eq(64).text()),
		 	   xNeedCase = parseFloat($tds.eq(65).text()),
		 	   category = $tds.eq(66).text(),
		 	   company = $tds.eq(67).text(),
		 	   inhousestock = $tds.eq(68).text()
		 	    l2sale = parseFloat($tds.eq(70).text()),
		 	     l1sale = parseFloat($tds.eq(71).text()),
		 	     qsale = parseFloat($tds.eq(72).text()),
		 	     psale = parseFloat($tds.eq(73).text()),
		 	     nsale = parseFloat($tds.eq(74).text()),
		 	     dsale = parseFloat($tds.eq(75).text()),
		 	     sbsale = parseFloat($tds.eq(76).text()),
		 	     lbsale = parseFloat($tds.eq(77).text()),
		 	     tinsale = parseFloat($tds.eq(78).text()),
		 	     xsale = parseFloat($tds.eq(79).text()),
		 	     l2perday = parseFloat($tds.eq(80).text()),
		 	     l1perday = parseFloat($tds.eq(81).text()),
		 	     qperday = parseFloat($tds.eq(82).text()),
		 	     pperday = parseFloat($tds.eq(83).text()),
		 	     nperday = parseFloat($tds.eq(84).text()),
		 	     dperday = parseFloat($tds.eq(85).text()),
		 	     lbperday = parseFloat($tds.eq(86).text()),
		 	     sbperday = parseFloat($tds.eq(87).text()),
		 	     xperday = parseFloat($tds.eq(88).text()),
		 	     tinperday = parseFloat($tds.eq(89).text())
		 	   
		 	   if(isNaN(needCase) || needCase == ""){
		 	   var needCase = $("#newcase"+brandNo).val();
		 		  if(isNaN(needCase) || needCase == ""){
		 			 needCase=0;
			             }
		 	   }
		 	  var input = {
		        		"brandName":brandName,
		        		"brandNo":brandNo,
		        		"caseRate":caseRate,
		        		"lastMonthSold":lastMonthSold,
		        		"inhousestock":inhousestock,
		        		"commitment":commitment,
		        		"liftedCase":liftedCase,
		        		"needCase":needCase,
		        		"target":target,
		        		"companyOrder":companyOrder,
		        		"companyColor":companyColor,
		        		"productType":productType,
		        		"l2":l2,
		        		"l1":l1,
		        		"q":q,
		        		"p":p,
		        		"n":n,
		        		"d":d,
		        		"sb":sb,
		        		"lb":lb,
		        		"tin":tin,
		        		"x":x,
		        		"l2SpecialMargin":l2SpecialMargin,
		        		"l1SpecialMargin":l1SpecialMargin,
		        		"qSpecialMargin":qSpecialMargin,
		        		"pSpecialMargin":pSpecialMargin,
		        		"nSpecialMargin":nSpecialMargin,
		        		"dSpecialMargin":dSpecialMargin,
		        		"sbSpecialMargin":sbSpecialMargin,
		        		"lbSpecialMargin":lbSpecialMargin,
		        		"tinSpecialMargin":tinSpecialMargin,
		        		"xSpecialMargin":xSpecialMargin,
		        		"l2Stock":l2Stock,
		        		"l1Stock":l1Stock,
		        		"qStock":qStock,
		        		"pStock":pStock,
		        		"nStock":nStock,
		        		"dStock":dStock,
		        		"sbStock":sbStock,
		        		"lbStock":lbStock,
		        		"tinStock":tinStock,
		        		"xStock":xStock,
		        		"noOfDays":noOfDays,
		        		"date":date,
		        		"category":category,
		        		"company":company,
		        		"pending":pending,
		        		"investMent":investMent,
		        		 "l2Val":l2Val,
		        		 "l1Val":l1Val,
		        		 "qVal":qVal,
		        		 "pVal":pVal,
		        		 "nVal":nVal,
		        		 "dVal":dVal,
		        		 "lbVal":lbVal,
		        		 "sbVal":sbVal,
		        		 "tinVal":tinVal,
		        		 "totalSpecialMrp":totalSpecialMrp,
		        		 "l2NeedCase":l2NeedCase,
		        		 "l1NeedCase":l1NeedCase,
		        		 "qNeedCase":qNeedCase,
		        		 "pNeedCase":pNeedCase,
		        		 "nNeedCase":nNeedCase,
		        		 "dNeedCase":dNeedCase,
		        		 "sbNeedCase":sbNeedCase,
		        		 "lbNeedCase":lbNeedCase,
		        		 "tinNeedCase":tinNeedCase,
		        		 "xNeedCase":xNeedCase,
		        		 "l2sale":l2sale,
			        	 "l1sale":l1sale,
			        	 "qsale":qsale,
			        	 "psale":psale,
			        	 "nsale":nsale,
			        	 "dsale":dsale,
			        	 "sbsale":sbsale,
			        	 "lbsale":lbsale,
			        	 "tinsale":tinsale,
			        	 "xsale":xsale,
			        	 "l2perday":l2perday,
			        	 "l1perday":l1perday,
			        	 "qperday":qperday,
			        	 "pperday":pperday,
			        	 "nperday":nperday,
			        	 "dperday":dperday,
			        	 "lbperday":lbperday,
			        	 "sbperday":sbperday,
			        	 "xperday":xperday,
			        	 "tinperday":tinperday,
		        };
		        json.indentDetails.push(input);
		 	 });
		 	saveTempIndent(json);
		 }
	 }
	 function saveTempIndent(json) {
			var myUrl = window.location.href;
			var url1 = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
			   $.ajax({
				  async:false,
			      type: "POST",
			      contentType : 'application/json; charset=utf-8',
			      //dataType : 'json',
			      url: url1+"/saveTempIndent",
			      data: JSON.stringify(json), 
			      success: function (data) {
			    	 alert(data.message); 
			    	 // $("#mymessage").text("");
			    		 location.reload();
			      },
			      error: function (data) {
			    	  alert(data+"error");
			    	  location.reload();
			      }
			  });
			}

$scope.totalInvstAmt = function() {
	   var totalInvst = 0,beerIndent=0,liquorIndent=0,beerIndentliquorIndent=0,totalSpecialMargin=0,tcsValue=0,netIndentAmount=0;
       angular.forEach($scope.alldetails, function(data) {
    	  // console.log("Shiva:: "+$scope.companyfilter);
    	   if($scope.companyfilter == data.company && $scope.companyfilter != undefined){
    		   totalInvst += data.totalinvestment;
    		   if(data.productType == "BEER")
    			   beerIndent += data.needCase;
    		   else
    			   liquorIndent += data.needCase;
    		   
    		   totalSpecialMargin += data.totalSpecialMrp;
    		   tcsValue += data.totalinvestment;
    	   }
	      
    	   else if($scope.companyfilter == undefined || $scope.companyfilter == null || $scope.companyfilter == ""){
    		   totalInvst += data.totalinvestment;
    		   if(data.productType == "BEER")
    			   beerIndent += data.needCase;
    		   else
    			   liquorIndent += data.needCase;
    		   
    		   totalSpecialMargin += data.totalSpecialMrp;
    		   tcsValue += data.totalinvestment;
    	   }
	  });
    $scope.totalInvst = totalInvst;
    $scope.beerIndent = beerIndent;
    $scope.liquorIndent = liquorIndent;
    $scope.beerIndentliquorIndent = beerIndent + liquorIndent;
    $scope.totalSpecialMargin = Math.ceil(totalSpecialMargin);
    $scope.tcsValue = Math.ceil((tcsValue*1)/100);
    $scope.netIndentAmount = Math.ceil(totalInvst + totalSpecialMargin + ((tcsValue*1)/100));
    
    
};

// Scheme estimate start

$scope.getSchemeEstimateData = function(){
	 $("#wait").css("display", "block");
	var schemeurl = url+"getSchemeData?date="+$("#schemedatepicker").val();
	$http({
	    method : "GET",
	    url : schemeurl
	  }).then(function mySuccess(response) {
		  $scope.schemeList = response.data;
		  $scope.totalSchemeInvstAmt();
		  $("#wait").css("display", "none");
		//console.log(JSON.stringify(response.data));
   }, function myError(response) {
	   $("#wait").css("display", "none");
  });

}

$scope.deleteSchemeRow = function(tableID){
	try {
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;

		for(var i=0; i<rowCount; i++) {
			var row = table.rows[i];
			var chkbox = row.cells[0].childNodes[0];
			if(null != chkbox && true == chkbox.checked) {
				table.deleteRow(i);
				rowCount--;
				i--;
				var brandNumber = Number(row.cells[2].childNodes[0].nodeValue);
				for (j = 0; j < $scope.schemeList.length; j++) {
					if(brandNumber == $scope.schemeList[j].brandNo)
						$scope.schemeList.splice(j, 1);
				}
			}
		}
		}catch(e) {
			alert(e);
		}
		$scope.totalSchemeInvstAmt();
		
}
$scope.splitScheme = function(schemecase,data){
	for (j = 0; j < $scope.schemeList.length; j++) {
		if(data.brandNo == $scope.schemeList[j].brandNo){
			$scope.schemeList[j].otherInvestment = schemecase * $scope.schemeList[j].caseRate;
			$scope.schemeList[j].l2NeedCase = stockDivive($scope.schemeList[j].l2,schemecase,$scope.schemeList[j].totalSoldCases);
			$scope.schemeList[j].l1NeedCase = stockDivive($scope.schemeList[j].l1,schemecase,$scope.schemeList[j].totalSoldCases);
			$scope.schemeList[j].qNeedCase = stockDivive($scope.schemeList[j].q,schemecase,$scope.schemeList[j].totalSoldCases);
			$scope.schemeList[j].pNeedCase = stockDivive($scope.schemeList[j].p,schemecase,$scope.schemeList[j].totalSoldCases);
			$scope.schemeList[j].nNeedCase = stockDivive($scope.schemeList[j].n,schemecase,$scope.schemeList[j].totalSoldCases);
			$scope.schemeList[j].dNeedCase = stockDivive($scope.schemeList[j].d,schemecase,$scope.schemeList[j].totalSoldCases);
			$scope.schemeList[j].lbNeedCase = stockDivive($scope.schemeList[j].lb,schemecase,$scope.schemeList[j].totalSoldCases);
			$scope.schemeList[j].sbNeedCase = stockDivive($scope.schemeList[j].sb,schemecase,$scope.schemeList[j].totalSoldCases);
			$scope.schemeList[j].tinNeedCase = stockDivive($scope.schemeList[j].tin,schemecase,$scope.schemeList[j].totalSoldCases);
			$scope.schemeList[j].totalSpecialMrp = (($scope.schemeList[j].l2NeedCase * $scope.schemeList[j].l2SpecialMargin) + 
					($scope.schemeList[j].l1NeedCase * $scope.schemeList[j].l1SpecialMargin) +
					($scope.schemeList[j].qNeedCase * $scope.schemeList[j].qspecialMargin) +
					($scope.schemeList[j].pNeedCase * $scope.schemeList[j].pspecialMargin) +
					($scope.schemeList[j].nNeedCase * $scope.schemeList[j].nspecialMargin) +
					($scope.schemeList[j].dNeedCase * $scope.schemeList[j].dspecialMargin) +
					($scope.schemeList[j].lbNeedCase * $scope.schemeList[j].lbspecialMargin) +
					($scope.schemeList[j].sbNeedCase * $scope.schemeList[j].sbspecialMargin) +
					($scope.schemeList[j].tinNeedCase * $scope.schemeList[j].tinspecialMargin));
		}
	}
	$scope.totalSchemeInvstAmt();
}
$scope.editSchemeRow = function(singleObj){
	$scope.singleObj = angular.copy(singleObj);
	$("#editSchemeModal").modal("show");
}
$scope.editSchemeChnage = function(){
	$scope.singleObj.noOfCases = ($scope.singleObj.l2NeedCase + $scope.singleObj.l1NeedCase + $scope.singleObj.qNeedCase + $scope.singleObj.pNeedCase + $scope.singleObj.nNeedCase + $scope.singleObj.dNeedCase + $scope.singleObj.lbNeedCase + $scope.singleObj.sbNeedCase + $scope.singleObj.tinNeedCase);
	$scope.singleObj.otherInvestment = ($scope.singleObj.noOfCases * $scope.singleObj.caseRate);
	//console.log(JSON.stringify($scope.singleObj));
}
$scope.updateSchemeRowValue = function(){
	for (j = 0; j < $scope.schemeList.length; j++) {
		if($scope.singleObj.brandNo == $scope.schemeList[j].brandNo){
			$scope.schemeList[j].otherInvestment = $scope.singleObj.otherInvestment;
			$scope.schemeList[j].noOfCases = $scope.singleObj.noOfCases;
			$scope.schemeList[j].l2NeedCase = $scope.singleObj.l2NeedCase;
			$scope.schemeList[j].l1NeedCase = $scope.singleObj.l1NeedCase;
			$scope.schemeList[j].qNeedCase = $scope.singleObj.qNeedCase;
			$scope.schemeList[j].pNeedCase = $scope.singleObj.pNeedCase;
			$scope.schemeList[j].nNeedCase = $scope.singleObj.nNeedCase;
			$scope.schemeList[j].dNeedCase = $scope.singleObj.dNeedCase;
			$scope.schemeList[j].lbNeedCase = $scope.singleObj.lbNeedCase;
			$scope.schemeList[j].sbNeedCase = $scope.singleObj.sbNeedCase;
			$scope.schemeList[j].tinNeedCase = $scope.singleObj.tinNeedCase;
			$scope.schemeList[j].totalSpecialMrp = (($scope.schemeList[j].l2NeedCase * $scope.schemeList[j].l2SpecialMargin) + 
					($scope.schemeList[j].l1NeedCase * $scope.schemeList[j].l1SpecialMargin) +
					($scope.schemeList[j].qNeedCase * $scope.schemeList[j].qspecialMargin) +
					($scope.schemeList[j].pNeedCase * $scope.schemeList[j].pspecialMargin) +
					($scope.schemeList[j].nNeedCase * $scope.schemeList[j].nspecialMargin) +
					($scope.schemeList[j].dNeedCase * $scope.schemeList[j].dspecialMargin) +
					($scope.schemeList[j].lbNeedCase * $scope.schemeList[j].lbspecialMargin) +
					($scope.schemeList[j].sbNeedCase * $scope.schemeList[j].sbspecialMargin) +
					($scope.schemeList[j].tinNeedCase * $scope.schemeList[j].tinspecialMargin));
		}
	}
	$scope.totalSchemeInvstAmt();
	$("#editSchemeModal").modal("hide");
}
$scope.totalSchemeInvstAmt = function(){
	   var totalInvst = 0,beerIndent=0,liquorIndent=0,beerIndentliquorIndent=0,totalSpecialMargin=0,tcsValue=0,netIndentAmount=0;
       angular.forEach($scope.schemeList, function(data) {
    	   if($scope.companySchemefilter == data.company && $scope.companySchemefilter != undefined){
    		   totalInvst += data.otherInvestment;
    		   if(data.productType == "BEER")
    			   beerIndent += data.noOfCases;
    		   else
    			   liquorIndent += data.noOfCases;
    		   
    		   totalSpecialMargin += data.totalSpecialMrp;
    		   tcsValue += data.otherInvestment;
    	   }
	      
    	   else if($scope.companySchemefilter == undefined || $scope.companySchemefilter == null || $scope.companySchemefilter == ""){
    		   totalInvst += data.otherInvestment;
    		   if(data.productType == "BEER")
    			   beerIndent += data.noOfCases;
    		   else
    			   liquorIndent += data.noOfCases;
    		   
    		   totalSpecialMargin += data.totalSpecialMrp;
    		   
    		   tcsValue += data.otherInvestment;
    	   }
	  });
    $scope.totalSchemeInvst = totalInvst;
    $scope.beerSchemeIndent = beerIndent;
    $scope.liquorSchemeIndent = liquorIndent;
    $scope.beerSchemeIndentliquorSchemeIndent = beerIndent + liquorIndent;
    $scope.totalSchemeSpecialMargin = Math.ceil(totalSpecialMargin);
    $scope.tcsSchemeValue = Math.ceil((tcsValue*1)/100);
    $scope.netSchemeIndentAmount = Math.ceil(totalInvst + totalSpecialMargin + ((tcsValue*1)/100));
    
    
}
$scope.saveSchemeListData = function(){
	var date = $("#schemedatepicker").val();
	if($scope.schemeList.length > 0){
		var postListUrl =  url+"saveSchemeListData?date="+date;
		$http({
		    method : "POST",
		    url : postListUrl,
		    data: $scope.schemeList,
		  }).then(function mySuccess(response) {
			  alert(response.data);
			  location.reload();
		  }, function myError(response) {
			  alert("Error"+response.data);
		  });	
	}else{
		alert("Please add product");
	}
}
// Scheme estimate end
});

app.filter('unique', function () {

    return function (items, filterOn) {

        if (filterOn === false) {
            return items;
        }

        if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
            var hashCheck = {}, newItems = [];

            var extractValueToCompare = function (item) {
                if (angular.isObject(item) && angular.isString(filterOn)) {
                    return item[filterOn];
                } else {
                    return item;
                }
            };

            angular.forEach(items, function (item) {
                var valueToCheck, isDuplicate = false;

                for (var i = 0; i < newItems.length; i++) {
                    if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                        isDuplicate = true;
                        break;
                    }
                }
                if (!isDuplicate) {
                    newItems.push(item);
                }

            });
            items = newItems;
        }
        return items;
    };
});