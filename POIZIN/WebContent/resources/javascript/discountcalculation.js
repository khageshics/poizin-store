var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope, $http) {
	var myUrl = window.location.href;
	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
	 $scope.init = function(){
		 $('#monthlyDate').monthpicker({changeYear:true,dateFormat: "MM-yy" });
		 $("#monthlyDate").val($.datepicker.formatDate('MM-yy', new Date()));
		 $scope.callDiscountCalculation();
	 } 
$scope.callDiscountCalculation = function(){
	$("#wait").css("display", "block");
	var date = changeDateFormateForMonth($("#monthlyDate").val());
	var callingUrl = url+"getDiscountCalculationList?month="+date;
	$http({
	    method : "GET",
	    url : callingUrl
	  }).then(function mySuccess(response) {
		  //console.log(JSON.stringify(response.data));
		  var categoryList = [];
		  $.each(response.data, function(index, value){
    	   	if(categoryList.indexOf(value.categoryId) === -1){
    	   		categoryList.push(value.categoryId);
    	   	}
    	   });
		  var inputjson = [];
		  for (var i = 0; i < categoryList.length; i++) {
			  var totalSale = 0, totalDiscount = 0,categoryid=0;
			  var Brands= [];
			  var categoryName = "";
			  $.each(response.data, function(index, value){
				  if(categoryList[i] == value.categoryId){
				  var totalDisc = (value.sale * value.discInPer);
				  var landingCost = (value.caseRate * (value.specialMargin * value.packQty));
				  var companyDiscper = (value.discInPer / landingCost) * 100;
				  var input = {
						   "brandName":value.productName,
			        	   "brandNo":value.brandNo,
			        	   "caseRate":value.caseRate,
			        	   "specialMargin":value.specialMargin,
			        	   "discInPer":value.discInPer,
			        	   "packType":value.packType,
			        	   "packQty":value.packQty,
			        	   "sale":value.sale,
			        	   "totalDiscount":totalDisc,
			        	   "landingCost":parseFloat(landingCost.toFixed(2)),
			        	   "companyDiscper":parseFloat(companyDiscper.toFixed(2)),
			        	   "btlMrp":value.btlMrp,
			        	   "brandNoPackQty":value.brandNoPackQty,
			        	   "categoryId":value.categoryId
				  };
				  Brands.push(input);
				  totalSale += value.sale;
				  totalDiscount += totalDisc;
				  categoryName = value.categoryName;
				 }
			  });
			  var  inputparent = {
					  "Brands":Brands,
					  "categoryName":categoryName,
					  "totalSale":totalSale,
					  "totalDiscount":totalDiscount,
					  "categoryid":categoryList[i]
          		  };
			  inputjson.push(inputparent);
		  }
		  $scope.alldetails = inputjson;
		  $("#wait").css("display", "none");
		//  console.log(JSON.stringify(inputjson));
	  }, function myError(response) {
		  $("#wait").css("display", "none");
      });
	
}

$scope.changeSaleData = function(sale,brand){
	for (var j = 0; j < $scope.alldetails.length; j++){
		if(brand.categoryId == $scope.alldetails[j].categoryid){
			var ttlsale = 0, ttldiscamt = 0;
			for (var k = 0; k < $scope.alldetails[j].Brands.length; k++) {
				  var totalDisc = ($scope.alldetails[j].Brands[k].sale * $scope.alldetails[j].Brands[k].discInPer);
				$scope.alldetails[j].Brands[k].totalDiscount = totalDisc;
				ttlsale += $scope.alldetails[j].Brands[k].sale;
				ttldiscamt += totalDisc;
			}
			$scope.alldetails[j].totalSale = ttlsale;
			$scope.alldetails[j].totalDiscount = ttldiscamt;
		}
	}
}

$scope.changeDiscountData = function(discount,brand){
	for (var j = 0; j < $scope.alldetails.length; j++){
		if(brand.categoryId == $scope.alldetails[j].categoryid){
			var ttldiscamt = 0;
			for (var k = 0; k < $scope.alldetails[j].Brands.length; k++) {
				  var totalDisc = ($scope.alldetails[j].Brands[k].sale * $scope.alldetails[j].Brands[k].discInPer);
				  var landingCost = ($scope.alldetails[j].Brands[k].caseRate * ($scope.alldetails[j].Brands[k].specialMargin * $scope.alldetails[j].Brands[k].packQty));
				  var companyDiscper = ($scope.alldetails[j].Brands[k].discInPer / landingCost) * 100;
				
				  $scope.alldetails[j].Brands[k].totalDiscount = totalDisc;
				  $scope.alldetails[j].Brands[k].companyDiscper = parseFloat(companyDiscper.toFixed(2));
				  ttldiscamt += totalDisc;
			}
			$scope.alldetails[j].totalDiscount = ttldiscamt;
		}
	}
}


});
app.filter('INR', function () {        
    return function (input) {
        if (! isNaN(input)) {
        	 var currencySymbol = '';
            var result = input.toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
            
            if (result.length > 1) {
                output += "." + result[1];
            }            

            return currencySymbol + output;
        }
    }
});
