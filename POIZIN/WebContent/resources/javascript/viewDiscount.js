var myApp = angular.module('myApp', ["checklist-model"]);
myApp.controller('Ctrl1', function($scope, $http) {
	var tempDate = new Date()
	tempDate.setDate(0);
	tempDate.setDate(1);
	$('#startMonth').monthpicker({changeYear:true,dateFormat: "MM-yy" });
	$('#startMonth').val($.datepicker.formatDate('MM-yy', tempDate));
	$('#endMonth').monthpicker({changeYear:true,dateFormat: "MM-yy" });
	$('#endMonth').val($.datepicker.formatDate('MM-yy', new Date()));
	
	buildTillMonthStockLiftWithDiscount();
	 $scope.getResults = function() {
		if (validateCalendarField() == false) {
				return false;
			}
		buildTillMonthStockLiftWithDiscount();

	}
 function validateCalendarField(){
		var startMonth = $("#startMonth").val();
		var endMonth = $("#endMonth").val();
		var date1, date2;
        date1 = new Date(startMonth);
        date2 = new Date(endMonth);
        if (date1 > date2){
        	alert("Start month should not be greater than End month!");
        	return false;
        }
		if(startMonth == ""){
			alert("Please fill Start Month.");
			return false;
		}
		if(endMonth == ""){
			alert("Please fill End Month.");
			return false;
		}
		return true;
	}
 $scope.user = {
	        discountData: []
	      };
	      $scope.checkAll = function() {
	        $scope.user.discountData = angular.copy($scope.discountData);
	      };
	      $scope.uncheckAll = function() {
	        $scope.user.discountData = [];
	      };
function buildTillMonthStockLiftWithDiscount(){
	$("#wait").css("display", "block");
	  //$scope.user.discountData = "";
	var startMonth = changeDateFormateForMonth($("#startMonth").val());
	var endMonth = changeDateFormateForMonth($("#endMonth").val());
	//console.log("startMonth"+startMonth+"endMonth:"+endMonth);
	var myUrl = window.location.href;
	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
	var URL = url+"discountReportForSelectedMonth?startMonth="+startMonth+"&endMonth="+endMonth;
	$http({
	    method : "GET",
	    url : URL
	  }).then(function mySuccess(response) {
		  //console.log(JSON.stringify(response.data));
		  $scope.discountData = response.data;
		  $scope.user.discountData = angular.copy($scope.discountData);
		  $("#wait").css("display", "none");
	  }, function myError(response) {
		  $("#wait").css("display", "none");
	  });
}

	$scope.openChekImg = function(image,imageName) {
		 $scope.image = image;
		 $scope.imageName = imageName;
		
		};
		$scope.greaterThanTopCheck = function(prop1,prop2, val){
		    return function(item){
		      return (item[prop1]+item[prop2]) > val;
		    }
		}
	$scope.greaterThan = function(prop, val){
	    return function(item){
	      return item[prop] > val;
	    }
	}
	$scope.greaterThanZero = function(prop1,prop2,prop3,prop4,prop5, val){
	    return function(item){
	      return (item[prop1]+item[prop2]+item[prop3]+item[prop4]+item[prop5]) > val;
	    }
	}
	$scope.greaterThanZeroAllCompany = function(prop1,prop2,prop3, val){
	    return function(item){
	      return (item[prop1]+item[prop2]+item[prop3]) > val;
	    }
	}
	$scope.openListDiscontsAllCompany = function(discountasCompanyBeen,date,companyId,realDate,discAmt,rentals,arrears,creditAmt) {
		 $scope.date = date;
		 $scope.ttlDiscount = discAmt;
		 $scope.rentalsAmt = rentals;
		 $scope.aarears = arrears;
		 $scope.discountBeen = discountasCompanyBeen;
		 $scope.creditAmt = creditAmt;
		 $scope.vendorCompany = companyId;
		 $scope.realDate = realDate;
		 var myUrl = window.location.href;
			var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
			var Url = url+"mobileView/stockLiftingWithDiscountReportWithBrandDetails?date="+realDate+"&companyId="+companyId;
			$http({
			    method : "GET",
			    url : Url
			  }).then(function mySuccess(response) {
				  var brandNoarr = [];
				   $.each(response.data, function(index, value){
				   	if(brandNoarr.indexOf(value.invoiceId) === -1){
				   		brandNoarr.push(value.invoiceId);
				   	}
				   });
				   var inputjson = [];
				   for (var i = 0; i < brandNoarr.length; i++) {
					   var Brands= [];
					   var totalCaseQty=0,totalDiscount=0;
					   var productName="";
					   $.each(response.data, function(index, value){
						   if(brandNoarr[i] == value.invoiceId){
							   var input = {
						        		"brandName":value.brandName,
						        		"brandNo":value.brandNo,
						        		"caseQty":value.caseQty,
						        		"unitPrice":value.unitPrice,
						        		"totalPrice":value.totalPrice
						        };
							   Brands.push(input);
							   totalCaseQty += value.caseQty;
							   totalDiscount += value.totalPrice;
							   productName = value.productType;
						   }
						   
			    	    });
					   var  inputparent = {
				   	        		"productName":productName,
				   	        		"totalCaseQty":totalCaseQty,
				   	        		"totalDiscount":totalDiscount,
				   	        		"Brands":Brands
					       };
			              inputjson.push(inputparent);
					   
				   }
				   $scope.tempData = inputjson;
			  }, function myError(response) {
			  });
		};
	// All company Discount end
		$scope.IsHidden = true;
		$scope.ShowHide = function(val) {
			$scope.tabFourHidden = true;
			if(val == 1)
			 $scope.IsHidden =  false;
			else
			 $scope.IsHidden = true;
		}
		$scope.tabFourHidden = true;
		$scope.tabHidden = function() {
			 $scope.tabFourHidden =  false;
			 $scope.IsHidden = true;
		}
	$scope.sortKey = 'duesAmt';
	$scope.reverse = true;
	$scope.sort = function(propName){
		$scope.sortKey = propName;
	    $scope.reverse = !$scope.reverse;
	}
$scope.addOrRemoveNotClearCheck = function(index, parentIndex, amount, duesAmt,company,notClear){
	var boolVal = $('#'+index+parentIndex+'addOrRemoveCheck').prop('checked');
	$.each($scope.user.discountData, function(indexnum, value){
	   	if(company == value.company){
	   		if(boolVal){
	   			$scope.user.discountData[indexnum].duesAmt = duesAmt - amount;
	   			$.each(value.mobileViewStockDiscountInner, function(innerindexnum, innervalue){
		   			if(new Date(innervalue.realDate) >= new Date(notClear.realDate)){
		   				$scope.user.discountData[indexnum].mobileViewStockDiscountInner[innerindexnum].arrears -= notClear.amount;
		   			}
		   		});
	   		}else{
	   			$scope.user.discountData[indexnum].duesAmt = duesAmt + amount;
	   			$.each(value.mobileViewStockDiscountInner, function(innerindexnum, innervalue){
		   			if(new Date(innervalue.realDate) >= new Date(notClear.realDate)){
		   				$scope.user.discountData[indexnum].mobileViewStockDiscountInner[innerindexnum].arrears += notClear.amount;
		   			}
		   		});
	   		}
	   	 }
	  });
	
}	
// Vendor credit service call start
$scope.getvendorcreditdetails = function(){
	//alert("Company::"+$scope.vendorCompany+" Date:: "+$scope.realDate);
	$("#wait").css("display", "block");
	var myUrl = window.location.href;
	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
	var URL = url+"getcompanyWiseExistingCredit?date="+$scope.realDate+"&company="+$scope.vendorCompany;
	$http({
	    method : "GET",
	    url : URL
	  }).then(function mySuccess(response) {
		  console.log(JSON.stringify(response.data));
		  $scope.vendorCreditData = response.data;
		  $("#wait").css("display", "none");
	  }, function myError(response) {
		  $("#wait").css("display", "none");
	  });
	
	$('#vendorCreditModelPopup').modal('show');
}



// Vendor credit service call end
});
myApp.filter('INR', function () {        
    return function (input) {
        if (! isNaN(input)) {
           // var currencySymbol = '₹';
        	 var currencySymbol = '';
            //var output = Number(input).toLocaleString('en-IN');   <-- This method is not working fine in all browsers!           
            var result = Math.round(input).toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
            
            if (result.length > 1) {
                output += "." + result[1];
            }            

            return currencySymbol + output;
        }
    }
});
myApp.filter("Arrears", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(shiva, i) {
             s += shiva.duesAmt;
        });
        return Math.ceil(s);
    };
})
myApp.filter("Discount", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(shiva, i) {
             s += shiva.totalDiscount;
        });
        return Math.ceil(s);
    };
})