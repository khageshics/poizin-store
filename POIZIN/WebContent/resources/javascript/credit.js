var myApp = angular.module('myApp', []);
myApp.controller('myCtrl', function($scope, $http, $window) {
	var myUrl = window.location.href;
	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
$scope.init = function(){
	$scope.listedCredit = false;
	$scope.cases = 0;
	$scope.bottles = 0;
	
	$('#datepicker').datepicker({dateFormat: "dd-mm-yy"}).datepicker('setDate',new Date());
	$('#paymentdatepicker').datepicker({dateFormat: "dd-mm-yy"}).datepicker('setDate',new Date());
	$scope.getCompanyList();
	$scope.buildBrandDropDown();
	$scope.getExistingCreditDetails();
	
}
$scope.getExistingCreditDetails = function(){
	var ListUrl =  url+"getExistingCredit";
	$http({
	    method : "GET",
	    url : ListUrl
	  }).then(function mySuccess(response) {
		  $scope.creditList  = response.data;
		// console.log(JSON.stringify(response.data));
	  }, function myError(response) {
	  });
}
$scope.getResults = function(){
	if($scope.company != null && $scope.company != undefined && $scope.company != "" && $scope.name != undefined && $scope.name != ""){
		$scope.listedCredit = true;
		$scope.sumOfAmount=0;
	}else{
		alert("All fields are manadatory!");
	}
	
}
$scope.buildBrandDropDown = function(){
	var productListUrl =  url+"getBrandDetails.json";
	$http({
	    method : "GET",
	    url : productListUrl
	  }).then(function mySuccess(response) {
		  var brand =new Array();
      $.each(response.data, function (index, value) {
	    	  brand.push(value.brandName+" "+value.brandNo+" "+value.packQty);
	      });
	      $("#brand").select2({
			  data: brand
			});
	      }, function myError(response) {
	  });
}
$scope.getCompanyList = function(){
	var productListUrl =  url+"getCompanyList";
	$http({
	    method : "GET",
	    url : productListUrl
	  }).then(function mySuccess(response) {
		  $scope.companyList = response.data;
	  }, function myError(response) {
	  });
}	
$scope.getSingleProductDetails = function(){
	var brandname = $("#brand option:selected").text();
	var n = brandname.split(" ");
	var val = n[n.length - 2].concat(n[n.length - 1]);
	var productListUrl =  url+"getSingleProductDetails?brandNoPackQty="+val+"&date="+$('#datepicker').val();
	$http({
	    method : "GET",
	    url : productListUrl
	  }).then(function mySuccess(response) {
		  $scope.price = response.data;
		  $scope.brandname = brandname;
		  $scope.discount = 0;
	  }, function myError(response) {
	  });
	
}
$scope.calculateTotalPrice = function(cases,bottles){
	var brandname = $("#brand option:selected").text();
	var n = brandname.split(" ");
	var val = n[n.length - 2].concat(n[n.length - 1]);
	var qty = n[n.length - 1];
	var productListUrl =  url+"getSingleProductDetails?brandNoPackQty="+val+"&date="+$('#datepicker').val();
	$http({
	    method : "GET",
	    url : productListUrl
	  }).then(function mySuccess(response) {
		  $scope.price = response.data;
		  $scope.brandname = brandname;
		  $scope.discount = 0;
		  $scope.totalPrice = ((cases * qty) + bottles) * response.data;
	  }, function myError(response) {
	  });
}

$scope.creditCustomers = [];
$scope.addCredit = function(){
	var bottles = $scope.bottles;
	var price = $scope.price;
	var totalPrice = $scope.totalPrice;
	if(bottles > 0 || $scope.cases > 0){
		var brandname = $("#brand option:selected").text();
		var n = brandname.split(" ");
		var val = n[n.length - 2].concat(n[n.length - 1]);
		var qty = n[n.length - 1];
		var customer = {};
		   customer.name = $scope.name;
		   customer.cases = $scope.cases;
		   customer.bottles = $scope.bottles;
		   customer.brandname = brandname;
		   customer.price = $scope.price;
		   customer.totaldiscount = $scope.discount * (($scope.cases * qty) + $scope.bottles);
		   customer.totalPrice = $scope.totalPrice;
		   customer.discount = $scope.discount;
		   customer.brandNoPackQty = val;
		   customer.date = changeDateFormat($('#datepicker').val());
		   customer.company = $scope.company;
		   customer.afterIncludeDiscount = $scope.totalPrice - ($scope.discount * (($scope.cases * qty) + $scope.bottles));
		$scope.creditCustomers.push(customer);

		/*$scope.name = "";*/
		$scope.cases = 0;
		$scope.bottles = 0;
		$scope.price = 0;
		$scope.totalPrice = 0;
		$scope.discount = 0;
		$scope.brandNoPackQty = 0;
		$scope.afterIncludeDiscount=0;
		$scope.totaldiscount = 0;
		$scope.calculateTotalAmount();
	}else{
		alert("Fields should not be empty!");
	}
	
	
}

$scope.Remove = function (index) {
var name = $scope.creditCustomers[index].brandname;
if ($window.confirm("Do you want to delete: " + name)) {
      $scope.creditCustomers.splice(index, 1);
      $scope.calculateTotalAmount();
   }
}
$scope.PostCredit = function(){
var jsondata = {
		      "name":$scope.name,
		      "totalAmt":$scope.sumOfAmount,
		      "company":$scope.company,
		      "date":changeDateFormat($('#datepicker').val()),
		      "creditData": $scope.creditCustomers
       };
if($scope.creditCustomers.length > 0){
	var postListUrl =  url+"postCreditData";
	$http({
	    method : "POST",
	    url : postListUrl,
	    data: jsondata,
	  }).then(function mySuccess(response) {
		  alert(response.data);
		  location.reload();
	  }, function myError(response) {
		  alert("Error"+response.data);
	  });	
}else{
	alert("Please add product");
}
	
}	
$scope.calculateTotalAmount = function(){
	$scope.sumOfAmount = 0;
	for(var i =0; i< $scope.creditCustomers.length; i++){
		$scope.sumOfAmount += $scope.creditCustomers[i].afterIncludeDiscount;
	}
}
$scope.clearOrUnclear = function(id,amount){
	$scope.paymentCreditId = id;
	$scope.paymentAmount = amount;
	 $('#addPaymentModel').modal('show');
	

}
$scope.postPaymentData = function(){
	var jsondata = {
			  "paymentCreditId":$scope.paymentCreditId,
		      "paymentDate":changeDateFormat($("#paymentdatepicker").val()),
		      "paymentAmount":$scope.paymentAmount,
		      "paymentmode":$scope.paymentmode,
		      "paymentcomment": $scope.paymentcomment
     };
	var postListUrl =  url+"clearOrUnclear";
	$http({
	    method : "POST",
	    url : postListUrl,
	    data: jsondata,
	  }).then(function mySuccess(response) {
		  alert(response.data);
		  location.reload();
	  }, function myError(response) {
		  alert("Error"+response.data);
	  });	

}
});