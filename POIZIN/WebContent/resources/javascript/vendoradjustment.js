var myApp = angular.module('myApp', []);
myApp.controller('myCtrl', function($scope, $http) {
	 var msg = $("#popupmsg").val();
		if(msg != ""){
			alert(msg);
		}
		 $('#popupmsg').val("");
		 var myUrl = window.location.href;
	     var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
		 $scope.init = function() {
		        $scope.getCompanyList();
		   };
$scope.getCompanyList = function(){
	var productListUrl = url+"getCompanyList.json";
	$http({
	    method : "GET",
	    url : productListUrl
	  }).then(function mySuccess(response) {
		  $.each(response.data, function (index, value) {
			  $('#companylistRental').append( '<option  value="'+value.id+'">'+value.name+'</option>' );
		     });
	  }, function myError(response) {
	    //alert(response);
	  });
}	
// Logic Start for Rental
$('#rentalMonth').monthpicker({
    changeYear:true,
    dateFormat: "yy-mm-01", 
        onSelect: function() {
        	var date = $("#rentalMonth").val();
        	var company = $scope.rentalcompanyId;
           if(date != "" && date != null && date != undefined && company != "" && company != null && company != undefined){
        	   var existingRentalUrl = url+"getExistingRentalForAMonth?rentalMonth="+date+"&company="+company;
        		$http({
        		    method : "GET",
        		    url : existingRentalUrl
        		  }).then(function mySuccess(response) {
        			  $scope.rentalAmount = response.data.amount;
        		  }, function myError(response) {
        		    //alert(response);
        		  });
             }
        }	
});

$scope.getselectedCompanyForRental = function(){
	var date = $("#rentalMonth").val();
	var company = $scope.rentalcompanyId;
   if(date != "" && date != null && date != undefined && company != "" && company != null && company != undefined){
	   var existingRentalUrl = url+"getExistingRentalForAMonth?rentalMonth="+date+"&company="+company;
		$http({
		    method : "GET",
		    url : existingRentalUrl
		  }).then(function mySuccess(response) {
			  $scope.rentalAmount = response.data.amount;
		  }, function myError(response) {
		    //alert(response);
		  });
   }
}




// Logic End for Rental

//Logic Start for Breakage
$('#breakageMonth').monthpicker({
    changeYear:true,
    dateFormat: "yy-mm-01", 
        onSelect: function() {
        	var date = $("#breakageMonth").val();
        	if(date != "" && date != null && date != undefined){
        		   var existingBreakageUrl = url+"getExistingBreakageForAMonth?breakageMonth="+date;
        			$http({
        			    method : "GET",
        			    url : existingBreakageUrl
        			  }).then(function mySuccess(response) {
        				// console.log(JSON.stringify(response.data));
        				  $scope.breakageAmount = response.data.amount;
        				  $scope.breakagecomment = response.data.comment;
        			  }, function myError(response) {
        			    //alert(response);
        			  });
        	   }
        }	
});

// Logic End for Breakage
});
