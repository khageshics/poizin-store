var myApp = angular.module('myApp', []);
myApp.controller('myCtrl', function($scope, $http) {
	$scope.showcompareCompanyDetails = true;
	 $scope.showcompareCategoryDetails = false;
	var myUrl = window.location.href;
	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
	 $scope.init = function(){
		 $('input.mobilemonthpicker').monthpicker({changeYear:true,dateFormat: "MM-yy" });
		 $("#mobilemonthlyDate").val($.datepicker.formatDate('MM-yy', new Date()));
		 
		 $("#weeklyDatePicker").datetimepicker({
		      format: 'DD-MM-YYYY'
		  });
		   //Get the value of Start and End of Week
		  $('#weeklyDatePicker').on('dp.change', function (e) {
		      var value = $("#weeklyDatePicker").val();
		      var firstDate = moment(value, "DD-MM-YYYY").day(0).format("DD-MM-YYYY");
		      var lastDate =  moment(value, "DD-MM-YYYY").day(6).format("DD-MM-YYYY");
		      $("#weeklyDatePicker").val(firstDate + " - " + lastDate);
		  });
		  $scope.getWeekComparisionSaleData();
	 } 
	 
	 $('#weeklyDatePicker').hide();
	 $scope.showHideMonthAndWeekPicker = function(id1, id2){
	 	$('#'+id1).show();
	 	$('#'+id2).hide();
	 }
  
	$scope.getWeekComparisionSaleData = function(){
		var radioValue = $("input[name='weekormonth']:checked").val();
		if(radioValue == 2 || radioValue == '2'){
			var weekDate = $('#weeklyDatePicker').val();
			if(weekDate != "" && weekDate != undefined){
				var n = weekDate.split(" - ");
				var val1 = changeDateFormat(n[n.length - 2]);
				var val2 = changeDateFormat(n[n.length - 1]);
				$scope.getComparisionSaleData(val1, val2, radioValue);
			}else{
				alert("Please select date");
			}
		}else{
			var month = $("#mobilemonthlyDate").val();
			if(month != "" && month != undefined){
			var val1 =	changeDateFormateForMonth(month);
			var val2 = getLastDayOfMonth(val1);
			$scope.getComparisionSaleData(val1, val2, radioValue);
			}else{
				alert("Please select month");
			}
		}
	}
	$scope.getComparisionSaleData = function(val1,val2, radioValue){
		$("#waitsec").css("display", "block");
		var weeklysaleUrl = url+"getWeeklySaleComparisionData?startDate="+val1+"&endDate="+val2+"&weekormonth="+radioValue;
		$http({
		    method : "GET",
		    url : weeklysaleUrl
		  }).then(function mySuccess(response) {
			// console.log(JSON.stringify(response.data));
			  $scope.showCurrentSelectedDate="";
			  $scope.showPreviousSelectedDate="";
			  if(response.data.length > 0){
				  $scope.showCurrentSelectedDate = response.data[0].showCurrentSelectedDate;
				  $scope.showPreviousSelectedDate = response.data[0].showPreviousSelectedDate;
			  }
			  var arr = response.data;
			  $scope.compareSaleData = response.data;
			  var sumOfCurrentSaleAmt = 0,sumOfPreviousSaleAmt =0,wholepercentage=0;
			  var companyid = [...new Set(response.data.map(s => s.companyId))];
	/*For Companywise Start*/
			  var inputsuperjson = [];
	   	   for (var i = 0; i < companyid.length; i++) {
	   		var bgcolor="",company = "",previousEndDate="",previousSatrtDate="",currentStartDate="",currentEndDate="";
	   		var totalCurrentSale=0, totalPreviousSale=0,companyOrder=0,totalcurrentsaleprice=0,totalprevioussaleprice=0;
	   		 var brandNoarr = [];
	   		  
	   		 arr.filter(function(item, pos){
	   			if(brandNoarr.indexOf(item.brandNo) === -1){
	    	   		if(item.companyId == companyid[i])
	    	   		brandNoarr.push(item.brandNo);
	    	   	}
	   		   });
	   		 
	   		 var inputjson = [];
		   		for (var j = 0; j < brandNoarr.length; j++) {
		   			var currentcases=0,previouscases=0,currentSalePrice=0,previousSalePrice=0;
		   			var Brands= [];
		 		    var name="";
		 		   $.each(response.data, function(index, value){
		 			  if(brandNoarr[j] == value.brandNo){
		 				 var currentcaseVal = Math.trunc(value.currentSale/value.packQty);
		 				 var previouscaseVal = Math.trunc(value.previousSale/value.packQty);
		 				var saleInPer = (((value.currentSalePrice - value.previousSalePrice) / value.currentSalePrice) * 100);
		 				if(!isFinite(saleInPer)){
		 					saleInPer = 0;
		 				}
		 				var input = {
				        		"quantity":value.quantity,
				        		"brandName":value.brandName,
				        		"currentSale":value.currentSale,
				        		"previousSale":value.previousSale,
				        		"currentcaseVal":currentcaseVal,
				        		"previouscaseVal":previouscaseVal,
				        		"currentbtlVal":value.currentSale-(currentcaseVal*value.packQty),
				        		"previousbtlVal":value.previousSale-(previouscaseVal*value.packQty),
				        		"companyId":value.companyId,
				        		"brandNoPackQty":value.brandNoPackQty,
				        		"packType":value.packType,
				        		"packQty":value.packQty,
				        		"saleInPercentage": parseFloat(saleInPer.toFixed(1)),
				        		"currentStartDate":value.currentStartDate,
				        		"currentEndDate":value.currentEndDate,
				        		"previousSatrtDate":value.previousStartDate,
				        		"previousEndDate":value.previousEndDate,
				        		"currentSalePrice":value.currentSalePrice,
				        		"previousSalePrice":value.previousSalePrice
				        };
					   Brands.push(input);
					   currentcases += value.currentSale/value.packQty;
					   previouscases += value.previousSale/value.packQty;
					   bgcolor = value.companyColor;
					   company = value.company;
					   name = value.brandName;
					   companyOrder = value.companyOrder;
					   currentStartDate = value.currentStartDate;
					   currentEndDate = value.currentEndDate;
					   previousSatrtDate = value.previousStartDate;
					   previousEndDate = value.previousEndDate;
					   previousSalePrice += value.previousSalePrice;
					   currentSalePrice += value.currentSalePrice;
		 			  }
		 		  });
		 		  var parentSaleInPer = (((currentSalePrice - previousSalePrice) / currentSalePrice) * 100);
					if(!isFinite(parentSaleInPer)){
						parentSaleInPer = 0;
					}
		 		  var  inputparent = {
			   	        		"brandNo":brandNoarr[j],
			   	        		"brandname":name,
			   	        		"currentcases":Math.round(currentcases),
			   	        		"previouscases":Math.round(previouscases),
			   	        		"Brands":Brands,
			   	        		"parentSaleInPer":parseFloat(parentSaleInPer.toFixed(1)),
			   	        		"currentStartDate":currentStartDate,
			   	        		"currentEndDate":currentEndDate,
			   	        		"previousSatrtDate":previousSatrtDate,
			   	        		"previousEndDate":previousEndDate,
			   	        		"previousSalePrice":previousSalePrice,
			   	        		"currentSalePrice":currentSalePrice
				       };
				   inputjson.push(inputparent);
				   totalCurrentSale += currentcases;
				   totalPreviousSale += previouscases;
				   totalcurrentsaleprice += currentSalePrice;
				   totalprevioussaleprice += previousSalePrice;
		   		}
		   		var superParentSaleInPer = (((totalcurrentsaleprice - totalprevioussaleprice) / totalcurrentsaleprice) * 100);
				if(!isFinite(superParentSaleInPer)){
					superParentSaleInPer = 0;
				}
	   		var  inputsuperparent = {
	 	        		"company":company,
	 	        	    "bgcolor":bgcolor,
	 	        	    "totalCurrentSale":Math.round(totalCurrentSale),
	 	        	    "totalPreviousSale":Math.round(totalPreviousSale),
	 	        		"inputjson":inputjson,
	 	        		"superParentSaleInPer":parseFloat(superParentSaleInPer.toFixed(1)),
	 	        		"companyOrder":companyOrder,
	 	        		"currentStartDate":currentStartDate,
	   	        		"currentEndDate":currentEndDate,
	   	        		"previousSatrtDate":previousSatrtDate,
	   	        		"previousEndDate":previousEndDate,
	   	        		"totalcurrentsaleprice":totalcurrentsaleprice,
	   	        		"totalprevioussaleprice":totalprevioussaleprice
		       };
	   		sumOfCurrentSaleAmt += totalcurrentsaleprice;
	   		sumOfPreviousSaleAmt += totalprevioussaleprice;
	   		
		   inputsuperjson.push(inputsuperparent);
	   	   }
	   	$scope.companyWiseCompareSale = inputsuperjson;
	   	$scope.sumOfCurrentSaleAmt = sumOfCurrentSaleAmt;
		$scope.sumOfPreviousSaleAmt = sumOfPreviousSaleAmt;
		$scope.wholepercentage = parseFloat((((sumOfCurrentSaleAmt - sumOfPreviousSaleAmt) / sumOfCurrentSaleAmt) * 100).toFixed(1));
	   	//console.log(JSON.stringify($scope.companyWiseCompareSale));
	/*For Companywise End*/	
	    var categoryid = [...new Set(response.data.map(s => s.categoryId))];
	    var inputsupercategoryjson = [];
		   for (var i = 0; i < categoryid.length; i++) {
			var bgcolor="",category = "",previousEndDate="",previousSatrtDate="",currentStartDate="",currentEndDate="";;
			var totalCurrentSale=0, totalPreviousSale=0,categoryOrder=0,totalcurrentsaleprice=0,totalprevioussaleprice=0;
			 var brandNoarr = [];
			  
			 arr.filter(function(item, pos){
		   			if(brandNoarr.indexOf(item.brandNo) === -1){
		    	   		if(item.categoryId == categoryid[i])
		    	   		brandNoarr.push(item.brandNo);
		    	   	}
		   		   });
			 
			 var inputjson = [];
		   		for (var j = 0; j < brandNoarr.length; j++) {
		   			var currentcases=0,previouscases=0,currentSalePrice=0,previousSalePrice=0;
		   			var Brands= [];
		 		    var name="";
		 		   $.each(response.data, function(index, value){
		 			  if(brandNoarr[j] == value.brandNo){
		 				 var currentcaseVal = Math.trunc(value.currentSale/value.packQty);
		 				 var previouscaseVal = Math.trunc(value.previousSale/value.packQty);
		 				var saleInPer = (((value.currentSalePrice - value.previousSalePrice) / value.currentSalePrice) * 100);
		 				if(!isFinite(saleInPer)){
		 					saleInPer = 0;
		 				}
		 				var input = {
				        		"quantity":value.quantity,
				        		"brandName":value.brandName,
				        		"currentSale":value.currentSale,
				        		"previousSale":value.previousSale,
				        		"currentcaseVal":currentcaseVal,
				        		"previouscaseVal":previouscaseVal,
				        		"currentbtlVal":value.currentSale-(currentcaseVal*value.packQty),
				        		"previousbtlVal":value.previousSale-(previouscaseVal*value.packQty),
				        		"categoryId":value.categoryId,
				        		"brandNoPackQty":value.brandNoPackQty,
				        		"packType":value.packType,
				        		"packQty":value.packQty,
				        		"saleInPercentage":parseFloat(saleInPer.toFixed(1)),
				        		"currentStartDate":value.currentStartDate,
				        		"currentEndDate":value.currentEndDate,
				        		"previousSatrtDate":value.previousStartDate,
				        		"previousEndDate":value.previousEndDate,
				        		"currentSalePrice":value.currentSalePrice,
				        		"previousSalePrice":value.previousSalePrice
				        };
					   Brands.push(input);
					   currentcases += value.currentSale/value.packQty;
					   previouscases += value.previousSale/value.packQty;
					   bgcolor = value.categoryColor;
					   category = value.category;
					   name = value.brandName;
					   categoryOrder = value.categoryOrder;
					   currentStartDate = value.currentStartDate;
					   currentEndDate = value.currentEndDate;
					   previousSatrtDate = value.previousStartDate;
					   previousEndDate = value.previousEndDate;
					   previousSalePrice += value.previousSalePrice;
					   currentSalePrice += value.currentSalePrice;
					   
		 			  }
		 		  });
		 		  var parentSaleInPer = (((currentSalePrice - previousSalePrice) / currentSalePrice) * 100);
					if(!isFinite(parentSaleInPer)){
						parentSaleInPer = 0;
					}
		 		  var  inputparent = {
			   	        		"brandNo":brandNoarr[j],
			   	        		"brandname":name,
			   	        		"currentcases":Math.round(currentcases),
			   	        		"previouscases":Math.round(previouscases),
			   	        		"Brands":Brands,
			   	        		"parentSaleInPer":parseFloat(parentSaleInPer.toFixed(1)),
			   	        		"currentStartDate":currentStartDate,
			   	        		"currentEndDate":currentEndDate,
			   	        		"previousSatrtDate":previousSatrtDate,
			   	        		"previousEndDate":previousEndDate,
			   	        		"previousSalePrice":previousSalePrice,
			   	        		"currentSalePrice":currentSalePrice
				       };
				   inputjson.push(inputparent);
				   totalCurrentSale += currentcases;
				   totalPreviousSale += previouscases;
				   totalcurrentsaleprice += currentSalePrice;
				   totalprevioussaleprice += previousSalePrice;
		   		}
		   		var superParentSaleInPer = (((totalcurrentsaleprice - totalprevioussaleprice) / totalcurrentsaleprice) * 100);
				if(!isFinite(superParentSaleInPer)){
					superParentSaleInPer = 0;
				}
			var  inputsuperparent = {
		        		"category":category,
		        	    "bgcolor":bgcolor,
		        	    "totalCurrentSale":Math.round(totalCurrentSale),
		        	    "totalPreviousSale":Math.round(totalPreviousSale),
		        		"inputjson":inputjson,
		        		"superParentSaleInPer":parseFloat(superParentSaleInPer.toFixed(1)),
		        		"categoryOrder":categoryOrder,
		        		"currentStartDate":currentStartDate,
	   	        		"currentEndDate":currentEndDate,
	   	        		"previousSatrtDate":previousSatrtDate,
	   	        		"previousEndDate":previousEndDate,
	   	        		"totalcurrentsaleprice":totalcurrentsaleprice,
	   	        		"totalprevioussaleprice":totalprevioussaleprice
		       };
			inputsupercategoryjson.push(inputsuperparent);
		   }
		$scope.categoryWiseCompareSale = inputsupercategoryjson;
	//console.log(JSON.stringify(inputsupercategoryjson));
		$("#waitsec").css("display", "none");
		  }, function myError(response) {
			  $("#waitsec").css("display", "none");
		  });
		
	}	  
	/* Weekly and monthly sale comparision Start*/
	$scope.includeZeroCompareSaleOrNot = function(){
		var catOrComp = $('#includeZeroCompareSaleOrNot').prop('checked');
	   if(catOrComp === true){
		   $scope.showcompareCompanyDetails = true;
		   $scope.showcompareCategoryDetails = false;
	   }else{
		   $scope.showcompareCompanyDetails = false;
		   $scope.showcompareCategoryDetails = true;
	   }
		   
	}
	
	
	
});

myApp.filter('INR', function () {        
    return function (input) {
        if (! isNaN(input)) {
        	 var currencySymbol = '';
            var result = Math.ceil(input).toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
            
            if (result.length > 1) {
                output += "." + result[1];
            }            

            return currencySymbol + output;
        }
    }
});