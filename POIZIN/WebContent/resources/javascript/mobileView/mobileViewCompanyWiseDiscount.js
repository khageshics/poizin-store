var myApp = angular.module('myApp', []);
myApp.controller('myCtrl', function($scope, $http) {
	      
	    $('#downloadPdfID').css("display", "none");
	 
	    $( "#transactionDate" ).datepicker({dateFormat: 'MM-yy'});
        $('input.monthpicker').monthpicker({changeYear:true,dateFormat: "MM-yy" });
	  
        var myUrl = window.location.href;
    	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
    	var productListUrl = url+"mobileView/getCompanyList.json";
    	$http({
    	    method : "GET",
    	    url : productListUrl
    	  }).then(function mySuccess(response) {
    		  $scope.companyData = response.data;
       	        var options="<option data-hidden=\"true\">Company</option>";
    		     $.each(response.data, function (index, value) {
    		   	  options=options+"<option value=\""+value.id+"\">"+value.name+"</option>";
    		     });
    		     $("#listdataSec").html(options);
    		     $(".selectpicker").selectpicker('refresh');
    	  }, function myError(response) {
    	    //alert(response);
    	  });
    	
    	$(".fetchExistingExpenses").click(function(){
    		 var date =	changeDateFormateForMonth($("#monthlyDate").val());
    		$scope.pdfDate = date;
    		 var company = $scope.companyId;
    	   if(date!="" && company!="" && company != undefined){
    		   $('#downloadPdfID').css("display", "block");
    		    var myUrl = window.location.href;
    	    	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
    	    	
    	    	var mobileviewDiscountpdf = url + "mobileView/buildmobiledicountpdf?date="+date+"&company="+company; 
    	    	$http({
    	    	    method : "GET",
    	    	    url : mobileviewDiscountpdf
    	    	  }).then(function mySuccess(response) {
    	    		 // console.log(JSON.stringify(response.data));
    	    		  $scope.transactionData = response.data.transactiontransactionData;
    	    		  $scope.receivedData = response.data.receivedData;
    	    		  $scope.rentalAmt = response.data.rental;
    	    		  $scope.arrearsdetails = response.data.arrearsdetails;
    	    		  
    	    		$scope.totalChequeAmount = response.data.totalChequeAmount;
  	 	    		$scope.adjCheque = response.data.adjCheque;
  	 	    		$scope.totalVendorDiscount = response.data.totalVendorDiscount;
  	 	    		$scope.totalDiscountAmt = response.data.totalDiscountAmt;
  	 	    		$scope.discountPlusRental = response.data.totalDiscountAmt + response.data.rental;
  	 	    		$scope.totalVendorCredit = response.data.creditMasterBean.totalAmount;
	 	    		$scope.creditChildBeanList = response.data.creditChildBeanList;
	 	    		$scope.totalVendorCreditReceived = response.data.creditMasterBean.received;
    	    	  }, function myError(response) {
    	    	  });
    	    	
    	    	var companyUrl = url+"mobileView/getCompanyWithEnterPrice?company="+company;
    	    	$http({
    	    	    method : "GET",
    	    	    url : companyUrl
    	    	  }).then(function mySuccess(response) {
    	    		  //console.log(JSON.stringify(response.data));
    	    		  $scope.companyName = response.data.label;
    	    		  $scope.enterprice = "  ("+response.data.color+")";
    	    		  
    	    	  }, function myError(response) {
    	    	  });
    	    	
    	   }else{
    		   alert("Please Select Month and Company");
    	   }
    	 });	
    	
    	$scope.greaterThan = function(prop, val){
    	    return function(item){
    	      return item[prop] > val;
    	    }
    	}
    
});
myApp.filter('INR', function () {        
    return function (input) {
        if (! isNaN(input)) {
           // var currencySymbol = '₹';
        	 var currencySymbol = '';
            //var output = Number(input).toLocaleString('en-IN');   <-- This method is not working fine in all browsers!           
            var result = input.toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
            
            if (result.length > 1) {
                output += "." + result[1];
            }            

            return currencySymbol + output;
        }
    }
});
