var myApp = angular.module('myApp', []);
myApp.controller('myCtrl', function($scope, $http) {
	 $("#showAllDetails").hide();
	 $(".saleReportWithCategoryWise").hide();
	 $scope.showcompareCompanyDetails = true;
	 $scope.showcompareCategoryDetails = false;
	var myUrl = window.location.href;
	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
	var saleDatesUrl = url+"mobileView/getTotalSaleDates";
	$http({
	    method : "GET",
	    url : saleDatesUrl
	  }).then(function mySuccess(response) {
		  var startDate="";
	      var enableDays =[];
	      $.each(response.data, function (index, value) {
	    	  enableDays.push(response.data[index].date);
	    	  startDate=response.data[index].date;
	      });
	      $("#startDate").val(startDate);
	  	  $("#endDate").val(startDate);
	  	  if((response.data.length-7) > 0){
	  		$("#popupstartDate").val(response.data[response.data.length-7].date);
	  		$scope.popupDate1 = response.data[response.data.length-7].date;
	  	  }else{
	  		$("#popupstartDate").val(startDate);
	  		$scope.popupDate1 = startDate;
	  	  }
	  	  $("#popupendDate").val(startDate);
			
			$scope.popupDate2 = startDate;
	  	   getTimeStamp(enableDays);
	  	 getSaleReport(startDate,startDate);
	  }, function myError(response) {
	  });
	function getTimeStamp(enableDays){
		$("#startDate,#popupstartDate").datepicker({
			  dateFormat: 'dd-mm-yy',  beforeShowDay: enableAllTheseDays,
			  numberOfMonths: 1,
			  onSelect: function(selected) {
			  $("#endDate,#popupendDate").datepicker("option","minDate", selected);
			  }
			  });
		    function enableAllTheseDays(date) {
		        var sdate = $.datepicker.formatDate( 'dd-mm-yy', date)
		        if($.inArray(sdate, enableDays) != -1) {
		            return [true];
		        }
		        return [false];
		    }
		    $('#endDate,#popupendDate').datepicker({dateFormat: 'dd-mm-yy', beforeShowDay: enableAllTheseDays});
	}
	$scope.getResults = function (startDate,endDate){
		var startDate = $("#startDate").val();
		var endDate = $("#endDate").val();
		 if(validateCalendarField()==false){return false;}
		 getSaleReport(startDate,endDate);
		 
	}
	function validateCalendarField(){
		var startDate = $("#startDate").val();
		var endDate = $("#endDate").val();
		if(startDate == ""){
			alert("Please fill Start Date.");
			return false;
		}
		if(endDate == ""){
			alert("Please fill End Date.");
			return false;
		}
		return true;
	}
	function  getSaleReport(startDate,endDate){
		var compareNo = startDate.localeCompare(endDate);
		$("#wait").css("display", "block");
		var myUrl = window.location.href;
		var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
		var callingUrl = url+"mobileView/getSaleReports.json?startDate="+startDate+"&endDate="+endDate;
		$http({
		    method : "GET",
		    url : callingUrl
		  }).then(function mySuccess(response) {
			// console.log(JSON.stringify(response.data.saleBean));
			  $scope.alldetails = response.data.saleBean;
			  $scope.allExpenseAmt = response.data.amount;
			  $scope.carAndCash = response.data.total;
			  $scope.difference = response.data.diff;
			  var companyName = [];
			  var categoryName = [];
			  var totalPriceForPercentage=0;
	    	   $.each(response.data.saleBean, function(index, value){
	    		   totalPriceForPercentage+=value.totalPrice;
	    	   	if(companyName.indexOf(value.invoiceId) === -1){          // companyName=companyID
	    	   		companyName.push(value.invoiceId);
	    	   	}
	    	   	if(categoryName.indexOf(value.saleId) === -1){          // saleId for categoryId
	    	   		categoryName.push(value.saleId);
	    	   	}
	    	   });
	    	   $scope.totalPriceForPercentage = totalPriceForPercentage;
	    	   // for company including zero
	    	   var inputsuperjson = [];
	    	   for (var i = 0; i < companyName.length; i++) {
	    		   var brandNoarr = [];
	    		   var bgcolor="",company = "";
	    		   var sumOfCase = 0,bottles = 0,totalSaleAmount = 0;
	    		   var companyOrder = 0,inhousestock = 0,firstFontColor=5,firstFontColorWithFilter=5,superCompareCurrentSale=0,superComparePreviousDaySale=0;
		    	   $.each(response.data.saleBean, function(index, value){
		    		   
		    	   	if(brandNoarr.indexOf(value.brandNo) === -1){
		    	   		if(companyName[i] == value.invoiceId)
		    	   		brandNoarr.push(value.brandNo);
		    	   	}
		    	   });
		    	   var inputjson = [];
		    	   for (var j = 0; j < brandNoarr.length; j++) {
		    		   var Brands= [];
		    		   var name="";
		    		   var cases=0,totalbtlsale=0,saleValue = 0,inHouseCase = 0,secondFontColor=5,secondFontColorWithFilter=5,compareCurrentSale=0,comparePreviousDaySale=0;
		    		    var preCases=0,currCases=0;
		    		   $.each(response.data.saleBean, function(index, value){
		    		   if(brandNoarr[j] == value.brandNo){
		    			   var caseVal = Math.trunc(value.totalSale/value.packQty);
		    			   var previouscaseVal = Math.trunc(value.received/value.packQty);
	    				   var input = {
	    			        		"quantity":value.quantity,
	    			        		"totalSale":value.totalSale,
	    			        		"caseVal":caseVal,
	    			        		"amount":value.totalPrice,
	    			        		"btlVal":value.totalSale-(caseVal*value.packQty),
	    			        		"closing":value.closing,
	    			        		"caseStock":Math.trunc(value.bottleSaleMrp),
	    			        		"noOfDays":value.noOfReturnsBtl,
	    			        		"stock":value.qtyBottels+value.totalSale,
	    			        		"compareTotalSale":value.received,
	    			        		"compareNo":compareNo,
	    			        		"shopRate":value.opening,
	    			        		"categoryId":value.saleId,
	    			        		"brandNoPackQty":value.brandNoPackQty,
	    			        		"compairSale":((value.totalSale - value.received) / value.totalSale) * 100,
	    			        		"previousSaleInCase":previouscaseVal,
	    			        		"previousBtlVal":value.received-(previouscaseVal*value.packQty),
	    			        		"packType":value.packType
	    			        };
	    				   Brands.push(input);
	    				   cases=cases+value.cummulativePrice;
	    				   brandname=value.brandName;
	    				   bgcolor=value.color;
	    				   saleValue=saleValue+value.totalPrice;
	    				   totalbtlsale=totalbtlsale+value.totalSale;
	    				   compareCurrentSale += value.totalSale;
	    				   comparePreviousDaySale += value.received;
	    				   preCases+=value.received/value.packQty;
	    				   currCases+=value.totalSale/value.packQty;
	    				   company=value.company;
	    				   companyOrder=value.companyOrder;
	    				   inHouseCase=inHouseCase+value.bottleSaleMrp; // for in house stock as case
	    					 if(((value.noOfReturnsBtl) <= 1))
	    					   secondFontColor = 0;
		    		     }
		    		   });
		    		   var  inputparent = {
	       	   	        		"brandNo":brandNoarr[j],
	       	   	        		"brandname":brandname,
	       	   	        		"cases":Math.round(cases),
	       	   	        		"saleValue":saleValue,
	       	   	        		"totalbtlsale":totalbtlsale,
	       	   	        		"inHouseCase":Math.round(inHouseCase),
	       	   	        		"secondFontColor":secondFontColor,
	       	   	        	    "compareCurrentSale": compareCurrentSale,
	       	   	        	    "comparePreviousDaySale": comparePreviousDaySale,
	       	   	        	    "compareNo":compareNo,
	       	   	        		"Brands":Brands,
	       	   	        	    "compairSaleSuper":((compareCurrentSale - comparePreviousDaySale) / compareCurrentSale) * 100,
	       	   	        	    "preCases":preCases.toFixed(1),
	       	   	        	    "currCases":currCases.toFixed(1)
		    		       };
		    		   inputjson.push(inputparent);
		    		   sumOfCase=sumOfCase+cases;
		    		   superCompareCurrentSale += compareCurrentSale;
		    		   superComparePreviousDaySale += comparePreviousDaySale;
		    		   totalSaleAmount=totalSaleAmount+saleValue;
		    		   bottles=bottles+totalbtlsale;
		    		   inhousestock=inhousestock+inHouseCase;
		    		   if(secondFontColor <=1)
		    			   firstFontColor = 0;
		    	   }
		    	   var  inputsuperparent = {
      	   	        		"company":company,
      	   	        	    "bgcolor":bgcolor,
      	   	        	    "sumOfCase":Math.round(sumOfCase),
      	   	        	    "superComparePreviousDaySale" :superComparePreviousDaySale,
      	   	        	    "superCompareCurrentSale" :superCompareCurrentSale,
      	   	        	    "bottles":bottles,
      	   	        	    "totalSaleAmount":totalSaleAmount,
      	   	        	    "companyOrder":companyOrder,
      	   	        	    "inhousestock":Math.round(inhousestock),
      	   	        	    "firstFontColor":firstFontColor,
      	   	        	    "compareNo":compareNo,
      	   	        		"inputjson":inputjson
	    		       };
		    	   inputsuperjson.push(inputsuperparent);
	    	   }
	    	   $scope.saledata = inputsuperjson;
	    	   
	    	   // For company including only sale
	    	   $scope.saledataOnlySale = inputsuperjson;
	    	   
	    	   // For category wise including zero
	    	   var inputsuperCategoryjson = [];
	    	   for (var i = 0; i < categoryName.length; i++) {
	    		   var brandNoarr = [];
	    		   var bgcolor="",category = "";
	    		   var sumOfCase = 0,bottles = 0,totalSaleAmount = 0;
	    		   var categoryOrder = 0,inhousestock = 0,firstFontColor=5,firstFontColorWithFilter=5,superCompareCurrentSale=0,superComparePreviousDaySale=0;
		    	   $.each(response.data.saleBean, function(index, value){
		    		   
		    	   	if(brandNoarr.indexOf(value.brandNo) === -1){
		    	   		if(categoryName[i] == value.saleId)
		    	   		brandNoarr.push(value.brandNo);
		    	   	}
		    	   });
		    	   var inputcategoryjson = [];
		    	   for (var j = 0; j < brandNoarr.length; j++) {
		    		   var Brands= [];
		    		   var cases=0;
		    		   var name="";
		    		   var totalbtlsale=0;
		    		   var saleValue = 0,inHouseCase = 0,secondFontColor=5,secondFontColorWithFilter=5,compareCurrentSale=0,comparePreviousDaySale=0;
		    		   var preCases=0,currCases=0;
		    		   $.each(response.data.saleBean, function(index, value){
		    		   if(brandNoarr[j] == value.brandNo){
		    			   var caseVal = Math.trunc(value.totalSale/value.packQty);
		    			   var previouscaseVal = Math.trunc(value.received/value.packQty);
	    				   var input = {
	    			        		"quantity":value.quantity,
	    			        		"totalSale":value.totalSale,
	    			        		"caseVal":caseVal,
	    			        		"amount":value.totalPrice,
	    			        		"btlVal":value.totalSale-(caseVal*value.packQty),
	    			        		"closing":value.closing,
	    			        		"caseStock":Math.trunc(value.bottleSaleMrp),
	    			        		"noOfDays":value.noOfReturnsBtl,
	    			        		"stock":value.qtyBottels+value.totalSale,
	    			        		"compareTotalSale":value.received,
	    			        		"compareNo":compareNo,
	    			        		"shopRate":value.opening,
	    			        		"categoryId":value.saleId,
	    			        		"brandNoPackQty":value.brandNoPackQty,
	    			        		"compairSale":((value.totalSale - value.received) / value.totalSale) * 100,
	    			        		"previousSaleInCase":previouscaseVal,
	    			        		"previousBtlVal":value.received-(previouscaseVal*value.packQty),
	    			        		"packType":value.packType
	    			        };
	    				   Brands.push(input);
	    				   cases=cases+value.cummulativePrice;
	    				   brandname=value.brandName;
	    				   bgcolor=value.invoiceDate;// for color
	    				   saleValue=saleValue+value.totalPrice;
	    				   totalbtlsale=totalbtlsale+value.totalSale;
	    				   compareCurrentSale += value.totalSale;
	    				   comparePreviousDaySale += value.received;
	    				   preCases+=value.received/value.packQty;
	    				   currCases+=value.totalSale/value.packQty;
	    				   category=value.category;
	    				   categoryOrder=value.categoryOrder;
	    				   inHouseCase=inHouseCase+value.bottleSaleMrp; // for in house stock as case
	    				   if(((value.noOfReturnsBtl) <= 1) && ((value.qtyBottels+value.totalSale) > 0))
	    					   firstFontColorWithFilter = 0;
	    					   if(((value.noOfReturnsBtl) <= 1))
	    					   secondFontColor = 0;
		    		     }
		    		   });
		    		   var  inputparent = {
	       	   	        		"brandNo":brandNoarr[j],
	       	   	        		"brandname":brandname,
	       	   	        		"cases":Math.round(cases),
	       	   	        	    "compareCurrentSale": compareCurrentSale,
   	   	        	            "comparePreviousDaySale": comparePreviousDaySale,
	       	   	        		"saleValue":saleValue,
	       	   	        		"totalbtlsale":totalbtlsale,
	       	   	        		"inHouseCase":Math.round(inHouseCase),
	       	   	        		"secondFontColor":secondFontColor,
	       	   	        		"secondFontColorWithFilter":secondFontColorWithFilter,
	       	   	        	    "compareNo":compareNo,
	       	   	        		"Brands":Brands,
	       	   	            	"compairSaleSuper":((compareCurrentSale - comparePreviousDaySale) / compareCurrentSale) * 100,
	       	   	                "preCases":preCases.toFixed(1),
	       	   	                "currCases":currCases.toFixed(1)
		    		       };
		    		   inputcategoryjson.push(inputparent);
		    		   sumOfCase=sumOfCase+cases;
		    		   superCompareCurrentSale += compareCurrentSale;
		    		   superComparePreviousDaySale += comparePreviousDaySale;
		    		   totalSaleAmount=totalSaleAmount+saleValue;
		    		   bottles=bottles+totalbtlsale;
		    		   inhousestock=inhousestock+inHouseCase;
		    		   if(secondFontColor <=1)
		    			   firstFontColor = 0;
		    		   if(secondFontColorWithFilter <= 1)
		    			   firstFontColorWithFilter = 0;
		    	   }
		    	   var  inputsuperparent = {
      	   	        		"category":category,
      	   	        	    "bgcolor":bgcolor,
      	   	        	    "sumOfCase":Math.round(sumOfCase),
      	   	        	    "superComparePreviousDaySale" :superComparePreviousDaySale,
	   	        	        "superCompareCurrentSale" :superCompareCurrentSale,
      	   	        	    "bottles":bottles,
      	   	        	    "totalSaleAmount":totalSaleAmount,
      	   	        	    "categoryOrder":categoryOrder,
      	   	        	    "inhousestock":Math.round(inhousestock),
      	   	        	    "firstFontColor":firstFontColor,
      	   	        	    "firstFontColorWithFilter":firstFontColorWithFilter,
      	   	        	    "compareNo":compareNo,
      	   	        		"inputcategoryjson":inputcategoryjson
	    		       };
		    	   inputsuperCategoryjson.push(inputsuperparent);
	    	   }
	    	   $scope.saleCategoryData = inputsuperCategoryjson;
	    	   
	    	   // For category wise without including zero
	    	   $scope.saleCategoryDataOnlySale = inputsuperCategoryjson;
	    	   
	    	   
	    	   $("#wait").css("display", "none");
		  }, function myError(response) {
			  $("#wait").css("display", "none");
		  });
		
	 }
	$scope.includeZeroSaleOrNot = function(){
		var catOrComp = $('#includeZeroSaleOrNot').prop('checked');
       if(catOrComp === true){
    	   $("#showAllOnlySaleDetails").hide();
    	   $("#showAllDetails").show();
       }else{
    	  
    	   $("#showAllDetails").hide();
    	   $("#showAllOnlySaleDetails").show();
       }
    	   
	}
	$scope.greaterThan = function(prop, val){
	    return function(item){
	      return item[prop] > val;
	    }
	}
	$scope.greaterThanStock = function(prop, val){
	    return function(item){
	    	 return item[prop] > val;
	    }
	}
	$scope.filterDataOnlySale = function (item) {
	    return (!(item.company === "Carlsberg" || item.company === "Kingfisher" || item.company === "SAB Miller")&&(item.bottles > 0 || item.totalSaleAmount > 0));
	  }
	$scope.filterDataBeerOnlySale = function (item) {
	    return ((item.company === "Carlsberg" || item.company === "Kingfisher" || item.company === "SAB Miller")&&(item.bottles > 0 || item.totalSaleAmount > 0));
	  }
	$scope.filterCategoryDataOnlySale = function (item) {
	   // return (!(item.category === "BEER" || item.category === "FOREIGN BEER" || item.category === "BEER -FL")&&(item.bottles > 0 || item.totalSaleAmount > 0));
		return (!(item.category === "FOREIGN-BEER" || item.category === "PREMIUM-BIRA" || item.category === "PREMIUM-STRONG-BEER" || item.category === "PREMIUM-LAGER-BEER" || item.category === "CARLSBERG-PREMIUM" || item.category === "REGULAR-STRONG" || item.category === "REGULAR-LAGER") && (item.bottles > 0 || item.totalSaleAmount > 0));
	  }
	$scope.filterCategoryDataBeerOnlySale = function (item) {
	    return ((item.category === "FOREIGN-BEER" || item.category === "PREMIUM-BIRA" || item.category === "PREMIUM-STRONG-BEER" || item.category === "PREMIUM-LAGER-BEER" || item.category === "CARLSBERG-PREMIUM" || item.category === "REGULAR-STRONG" || item.category === "REGULAR-LAGER") && (item.bottles > 0 || item.totalSaleAmount > 0));
	  }
	
	
	$scope.filterData = function (item) {
	    return !(item.company === "Carlsberg" || item.company === "Kingfisher" || item.company === "SAB Miller");
	  }
	$scope.filterDataBeer = function (item) {
	    return (item.company === "Carlsberg" || item.company === "Kingfisher" || item.company === "SAB Miller");
	  }
	
	$scope.filterCategoryData = function (item) {
	    return !(item.category === "FOREIGN-BEER" || item.category === "PREMIUM-BIRA" || item.category === "PREMIUM-STRONG-BEER" || item.category === "PREMIUM-LAGER-BEER" || item.category === "CARLSBERG-PREMIUM" || item.category === "REGULAR-STRONG" || item.category === "REGULAR-LAGER");
	  }
	$scope.filterCategoryDataBeer = function (item) {
	    return (item.category === "FOREIGN-BEER" || item.category === "PREMIUM-BIRA" || item.category === "PREMIUM-STRONG-BEER" || item.category === "PREMIUM-LAGER-BEER" || item.category === "CARLSBERG-PREMIUM" || item.category === "REGULAR-STRONG" || item.category === "REGULAR-LAGER");
	  }
	$scope.FilterCatOrComp = function(){
		var catOrComp = $('#catOrComp').prop('checked');
       if(catOrComp === true){
    	   $(".saleReportWithCategoryWise").hide();
    	   $(".saleReportWithCompanyWise").show();
       }else{
    	   $(".saleReportWithCompanyWise").hide();
    	   $(".saleReportWithCategoryWise").show();
       }
    	   
	}
// This is the logic for popup line graph
	$scope.openModalPopup = function(brandNoPackQty,name,qty,categoryId){
		  $scope.brandNoPackQty = brandNoPackQty;
		  $scope.popupBrandName = name;
		  $scope.popupQuantity = qty;
		  if(($("#startDate").val().localeCompare($("#endDate").val())) == 0){
			  var today = new Date(changeDateFormat($("#endDate").val()));
			  today.setDate(today.getDate() - 6);
			  var newDate = (("0" + today.getDate()).slice(-2)) + "-" + (("0" + (today.getMonth() + 1)).slice(-2)) + "-" + (today.getFullYear());
			  $("#popupstartDate").val(newDate);
		  	  $("#popupendDate").val($("#endDate").val());
		  }
		  else{
			  $("#popupstartDate").val($("#startDate").val());
		  	  $("#popupendDate").val($("#endDate").val());
		  }
		  var myUrl = window.location.href;
			var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
			var productListUrl = url+"getProductListWithShortName?brandNoPackQty="+brandNoPackQty+"&categoryId="+categoryId;
			$http({
			    method : "GET",
			    url : productListUrl
			  }).then(function mySuccess(response) {
				  var options="<option value=\""+brandNoPackQty+"\" selected>"+name+"_"+qty+"</option>";
		 	      $.each(response.data, function (index, value) {
		            options=options+"<option value=\""+value.brandNoPackQty+"\">"+value.shortBrandName+"_"+value.quantity+"</option>";
		 	     });
		 	      $("#listdata").html(options);
		 	     $(".selectpicker").selectpicker('refresh');
			  }, function myError(response) {
		  
	               });
		  
   		$scope.buildLineGraph(brandNoPackQty);
		 
	  }
	  $scope.buildLineGraph = function(brandNoPackQty){
		var sdate = $("#popupstartDate").val();
		var edate = $("#popupendDate").val();
	    var myUrl = window.location.href;
		var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
		var lineChartUrl = url+"getSaleByBrandNoPackQty?brandNoPackQty="+brandNoPackQty+"&sdate="+sdate+"&edate="+edate;
		$http({
		    method : "GET",
		    url : lineChartUrl
		  }).then(function mySuccess(response) {
			  //console.log(JSON.stringify(response.data));
			  var salesChart = new FusionCharts({
			        type: 'line',
			        renderAt: 'chart-container',
			        width: '100%',
			        height: '100%',
			        dataFormat: 'json',
			        dataSource: {
			            "chart": {
			                "caption": $scope.popupBrandName+", "+$scope.popupQuantity,
			                "subCaption": "",
			                "xAxisName": "Date",
			                "yAxisName": "Sale In Bottles",
			                "numberPrefix": "",
			                "formatNumberScale": "0",
			                "thousandSeparatorPosition": "2,3",
			                "showValues": "1",
			                "showAlternateHGridColor": "0",
			                "captionFontSize": "12",
			                "subcaptionFontSize": "12",
			                "subcaptionFontBold": "0",
			                "toolTipColor": "#ffffff",
			                "toolTipBorderThickness": "0",
			                "toolTipBgColor": "#000000",
			                "toolTipBgAlpha": "80",
			                "toolTipBorderRadius": "2",
			                "toolTipPadding": "5",
			                "rotatevalues": "1",
			                "lineThickness": "1",
			                "theme": "fint",
			                "outCnvBaseFont": "'Montserrat', sans-serif !important",
			                "outCnvBaseFontColor": "#00000",
			                "outCnvBaseFontSize":"11"
			            },
			            "data": JSON.parse(JSON.stringify(response.data))
			        }
			    });
				salesChart.render();
		  }, function myError(response) {
	  		    //alert(response);
	  
             });
		} 
	  $scope.buildMultiLineGraph = function(){
			var sdate = $("#popupstartDate").val();
			var edate = $("#popupendDate").val();
			var targets = [];
			$.each($("#listdata option:selected"), function(){
			targets.push($(this).val());
			});
			if(targets.length > 0){
				var myUrl = window.location.href;
				var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
				var callingUrl = url+"getMultiChartBeanSaleData.json?sdate="+sdate+"&edate="+edate+"&targets="+targets;
				$http({
	 	    	    method : "GET",
	 	    	    url : callingUrl
	 	    	  }).then(function mySuccess(response) {
	 	    	   // console.log(JSON.stringify(response.data));
	 	    	   var salesChart = new FusionCharts({
				        type: 'msline',
				        renderAt: 'chart-container',
				        width: '100%',
				        height: '100%',
				        dataFormat: 'json',
				        dataSource: {
				        	"chart": {
				                "theme": "fint",
				                "caption": "",
				                "subCaption": "",
				                "xAxisName": "Date",
				        		"yAxisName": "Sale In Bottles",
				                "numberPrefix": "",
				        		"formatNumberScale": "0",
				        		"thousandSeparatorPosition": "2,3",
				        		"showValues": "1",
				                "toolTipColor": "#ffffff",
				        		"toolTipBorderThickness": "0",
				        		"toolTipBgColor": "#000000",
				        		"toolTipBgAlpha": "80",
				        		"toolTipBorderRadius": "2",
				        		"toolTipPadding": "5"
				              },
				              "categories": [{
				                  "category": JSON.parse(JSON.stringify(response.data.categoryLineChart))
				                }],
				                "dataset": JSON.parse(JSON.stringify(response.data.dataset))
				        }
				    });
					salesChart.render();
	 	    	  }, function myError(response) {
	 	    	  });
			}
		} 
	  
/*Weekly and monthly sale comparision Start*/
	  $('input.monthpicker').monthpicker({changeYear:true,dateFormat: "MM-yy" });
	  $('#monthlyDate').hide();
	  $scope.showHideMonthAndWeekPicker = function(id1, id2){
	  	$('#'+id1).show();
	  	$('#'+id2).hide();
	  }


	  $scope.getWeekComparisionSaleData = function(){
	  	var radioValue = $("input[name='weekormonth']:checked").val();
	  	if(radioValue == 2 || radioValue == '2'){
	  		var weekDate = $('#weeklyDatePicker').val();
	  		if(weekDate != "" && weekDate != undefined){
	  			var n = weekDate.split(" - ");
	  			var val1 = changeDateFormat(n[n.length - 2]);
	  			var val2 = changeDateFormat(n[n.length - 1]);
	  			$scope.getComparisionSaleData(val1, val2, radioValue);
	  		}else{
	  			alert("Please select date");
	  		}
	  	}else{
	  		var month = $("#monthlyDate").val();
	  		if(month != "" && month != undefined){
	  		var val1 =	changeDateFormateForMonth(month);
	  		var val2 = getLastDayOfMonth(val1);
	  		$scope.getComparisionSaleData(val1, val2, radioValue);
	  		}else{
	  			alert("Please select month");
	  		}
	  	}
	  }
	  	  
	  	  
	  $scope.getComparisionSaleData = function(val1,val2, radioValue){
	  	$("#waitsec").css("display", "block");
	  	var weeklysaleUrl = url+"getWeeklySaleComparisionData?startDate="+val1+"&endDate="+val2+"&weekormonth="+radioValue;
	  	$http({
	  	    method : "GET",
	  	    url : weeklysaleUrl
	  	  }).then(function mySuccess(response) {
	  		  //console.log(JSON.stringify(response.data));
	  		  var arr = response.data;
	  		  $scope.showCurrentSelectedDate="";
	  		  $scope.showPreviousSelectedDate="";
	  		  if(response.data.length > 0){
	  			  $scope.showCurrentSelectedDate = response.data[0].showCurrentSelectedDate;
	  			  $scope.showPreviousSelectedDate = response.data[0].showPreviousSelectedDate;
	  		  }
	  		  $scope.compareSaleData = response.data;
	  		  var sumOfCurrentSaleAmt = 0,sumOfPreviousSaleAmt =0,wholepercentage=0;
	  		  var companyid = [...new Set(response.data.map(s => s.companyId))];
	  /*For Companywise Start*/
	  		  var inputsuperjson = [];
	     	   for (var i = 0; i < companyid.length; i++) {
	     		var bgcolor="",company = "",previousEndDate="",previousSatrtDate="",currentStartDate="",currentEndDate="";
	     		var totalCurrentSale=0, totalPreviousSale=0,companyOrder=0,totalcurrentsaleprice=0,totalprevioussaleprice=0;
	     		 var brandNoarr = [];
	     		  
	     		 arr.filter(function(item, pos){
	     			if(brandNoarr.indexOf(item.brandNo) === -1){
	      	   		if(item.companyId == companyid[i])
	      	   		brandNoarr.push(item.brandNo);
	      	   	}
	     		   });
	     		 
	     		 var inputjson = [];
	  	   		for (var j = 0; j < brandNoarr.length; j++) {
	  	   			var currentcases=0,previouscases=0,currentSalePrice=0,previousSalePrice=0;
	  	   			var Brands= [];
	  	 		    var name="";
	  	 		   $.each(response.data, function(index, value){
	  	 			  if(brandNoarr[j] == value.brandNo){
	  	 				 var currentcaseVal = Math.trunc(value.currentSale/value.packQty);
	  	 				 var previouscaseVal = Math.trunc(value.previousSale/value.packQty);
	  	 				var saleInPer = (((value.currentSalePrice - value.previousSalePrice) / value.currentSalePrice) * 100);
	  	 				if(!isFinite(saleInPer)){
	  	 					saleInPer = 0;
	  	 				}
	  	 				var input = {
	  			        		"quantity":value.quantity,
	  			        		"brandName":value.brandName,
	  			        		"currentSale":value.currentSale,
	  			        		"previousSale":value.previousSale,
	  			        		"currentcaseVal":currentcaseVal,
	  			        		"previouscaseVal":previouscaseVal,
	  			        		"currentbtlVal":value.currentSale-(currentcaseVal*value.packQty),
	  			        		"previousbtlVal":value.previousSale-(previouscaseVal*value.packQty),
	  			        		"companyId":value.companyId,
	  			        		"brandNoPackQty":value.brandNoPackQty,
	  			        		"packType":value.packType,
	  			        		"packQty":value.packQty,
	  			        		"saleInPercentage": parseFloat(saleInPer.toFixed(1)),
	  			        		"currentStartDate":value.currentStartDate,
	  			        		"currentEndDate":value.currentEndDate,
	  			        		"previousSatrtDate":value.previousStartDate,
	  			        		"previousEndDate":value.previousEndDate,
	  			        		"currentSalePrice":value.currentSalePrice,
	  			        		"previousSalePrice":value.previousSalePrice
	  			        };
	  				   Brands.push(input);
	  				   currentcases += value.currentSale/value.packQty;
	  				   previouscases += value.previousSale/value.packQty;
	  				   bgcolor = value.companyColor;
	  				   company = value.company;
	  				   name = value.brandName;
	  				   companyOrder = value.companyOrder;
	  				   currentStartDate = value.currentStartDate;
	  				   currentEndDate = value.currentEndDate;
	  				   previousSatrtDate = value.previousStartDate;
	  				   previousEndDate = value.previousEndDate;
	  				   previousSalePrice += value.previousSalePrice;
	  				   currentSalePrice += value.currentSalePrice;
	  				   
	  	 			  }
	  	 		  });
	  	 		  var parentSaleInPer = (((currentSalePrice - previousSalePrice) / currentSalePrice) * 100);
	  				if(!isFinite(parentSaleInPer)){
	  					parentSaleInPer = 0;
	  				}
	  	 		  var  inputparent = {
	  		   	        		"brandNo":brandNoarr[j],
	  		   	        		"brandname":name,
	  		   	        		"currentcases":Math.round(currentcases),
	  		   	        		"previouscases":Math.round(previouscases),
	  		   	        		"Brands":Brands,
	  		   	        		"parentSaleInPer":parseFloat(parentSaleInPer.toFixed(1)),
	  		   	        		"currentStartDate":currentStartDate,
	  		   	        		"currentEndDate":currentEndDate,
	  		   	        		"previousSatrtDate":previousSatrtDate,
	  		   	        		"previousEndDate":previousEndDate,
	  		   	        		"previousSalePrice":previousSalePrice,
	  		   	        		"currentSalePrice":currentSalePrice
	  			       };
	  			   inputjson.push(inputparent);
	  			   totalCurrentSale += currentcases;
	  			   totalPreviousSale += previouscases;
	  			   totalcurrentsaleprice += currentSalePrice;
	  			   totalprevioussaleprice += previousSalePrice;
	  	   		}
	  	   		var superParentSaleInPer = (((totalcurrentsaleprice - totalprevioussaleprice) / totalcurrentsaleprice) * 100);
	  			if(!isFinite(superParentSaleInPer)){
	  				superParentSaleInPer = 0;
	  			}
	     		var  inputsuperparent = {
	   	        		"company":company,
	   	        	    "bgcolor":bgcolor,
	   	        	    "totalCurrentSale":Math.round(totalCurrentSale),
	   	        	    "totalPreviousSale":Math.round(totalPreviousSale),
	   	        		"inputjson":inputjson,
	   	        		"superParentSaleInPer":parseFloat(superParentSaleInPer.toFixed(1)),
	   	        		"companyOrder":companyOrder,
	   	        		"currentStartDate":currentStartDate,
	     	        		"currentEndDate":currentEndDate,
	     	        		"previousSatrtDate":previousSatrtDate,
	     	        		"previousEndDate":previousEndDate,
	     	        		"totalcurrentsaleprice":totalcurrentsaleprice,
	     	        		"totalprevioussaleprice":totalprevioussaleprice
	  	       };
	     		sumOfCurrentSaleAmt += totalcurrentsaleprice;
	     		sumOfPreviousSaleAmt += totalprevioussaleprice;
	     		
	  	   inputsuperjson.push(inputsuperparent);
	     	   }
	     	$scope.companyWiseCompareSale = inputsuperjson;
	     	$scope.sumOfCurrentSaleAmt = sumOfCurrentSaleAmt;
	  	$scope.sumOfPreviousSaleAmt = sumOfPreviousSaleAmt;
	  	$scope.wholepercentage = parseFloat((((sumOfCurrentSaleAmt - sumOfPreviousSaleAmt) / sumOfCurrentSaleAmt) * 100).toFixed(1));
	     	//console.log(JSON.stringify($scope.companyWiseCompareSale));
	  /*For Companywise End*/	
	      var categoryid = [...new Set(response.data.map(s => s.categoryId))];
	      var inputsupercategoryjson = [];
	  	   for (var i = 0; i < categoryid.length; i++) {
	  		var bgcolor="",category = "",previousEndDate="",previousSatrtDate="",currentStartDate="",currentEndDate="";
	  		var totalCurrentSale=0, totalPreviousSale=0,categoryOrder=0,totalcurrentsaleprice=0,totalprevioussaleprice=0;
	  		 var brandNoarr = [];
	  		  
	  		 arr.filter(function(item, pos){
	  	   			if(brandNoarr.indexOf(item.brandNo) === -1){
	  	    	   		if(item.categoryId == categoryid[i])
	  	    	   		brandNoarr.push(item.brandNo);
	  	    	   	}
	  	   		   });
	  		 
	  		 var inputjson = [];
	  	   		for (var j = 0; j < brandNoarr.length; j++) {
	  	   			var currentcases=0,previouscases=0,currentSalePrice=0,previousSalePrice=0;
	  	   			var Brands= [];
	  	 		    var name="";
	  	 		   $.each(response.data, function(index, value){
	  	 			  if(brandNoarr[j] == value.brandNo){
	  	 				 var currentcaseVal = Math.trunc(value.currentSale/value.packQty);
	  	 				 var previouscaseVal = Math.trunc(value.previousSale/value.packQty);
	  	 				var saleInPer = (((value.currentSalePrice - value.previousSalePrice) / value.currentSalePrice) * 100);
	  	 				if(!isFinite(saleInPer)){
	  	 					saleInPer = 0;
	  	 				}
	  	 				var input = {
	  			        		"quantity":value.quantity,
	  			        		"brandName":value.brandName,
	  			        		"currentSale":value.currentSale,
	  			        		"previousSale":value.previousSale,
	  			        		"currentcaseVal":currentcaseVal,
	  			        		"previouscaseVal":previouscaseVal,
	  			        		"currentbtlVal":value.currentSale-(currentcaseVal*value.packQty),
	  			        		"previousbtlVal":value.previousSale-(previouscaseVal*value.packQty),
	  			        		"categoryId":value.categoryId,
	  			        		"brandNoPackQty":value.brandNoPackQty,
	  			        		"packType":value.packType,
	  			        		"packQty":value.packQty,
	  			        		"saleInPercentage":parseFloat(saleInPer.toFixed(1)),
	  			        		"currentStartDate":value.currentStartDate,
	  			        		"currentEndDate":value.currentEndDate,
	  			        		"previousSatrtDate":value.previousStartDate,
	  			        		"previousEndDate":value.previousEndDate,
	  			        		"currentSalePrice":value.currentSalePrice,
	  			        		"previousSalePrice":value.previousSalePrice
	  			        };
	  				   Brands.push(input);
	  				   currentcases += value.currentSale/value.packQty;
	  				   previouscases += value.previousSale/value.packQty;
	  				   bgcolor = value.categoryColor;
	  				   category = value.category;
	  				   name = value.brandName;
	  				   categoryOrder = value.categoryOrder;
	  				   currentStartDate = value.currentStartDate;
	  				   currentEndDate = value.currentEndDate;
	  				   previousSatrtDate = value.previousStartDate;
	  				   previousEndDate = value.previousEndDate;
	  				   previousSalePrice += value.previousSalePrice;
	  				   currentSalePrice += value.currentSalePrice;
	  				   
	  	 			  }
	  	 		  });
	  	 		  var parentSaleInPer = (((currentSalePrice - previousSalePrice) / currentSalePrice) * 100);
	  				if(!isFinite(parentSaleInPer)){
	  					parentSaleInPer = 0;
	  				}
	  	 		  var  inputparent = {
	  		   	        		"brandNo":brandNoarr[j],
	  		   	        		"brandname":name,
	  		   	        		"currentcases":Math.round(currentcases),
	  		   	        		"previouscases":Math.round(previouscases),
	  		   	        		"Brands":Brands,
	  		   	        		"parentSaleInPer":parseFloat(parentSaleInPer.toFixed(1)),
	  		   	        		"currentStartDate":currentStartDate,
	  		   	        		"currentEndDate":currentEndDate,
	  		   	        		"previousSatrtDate":previousSatrtDate,
	  		   	        		"previousEndDate":previousEndDate,
	  		   	        		"previousSalePrice":previousSalePrice,
	  		   	        		"currentSalePrice":currentSalePrice
	  			       };
	  			   inputjson.push(inputparent);
	  			   totalCurrentSale += currentcases;
	  			   totalPreviousSale += previouscases;
	  			   totalcurrentsaleprice += currentSalePrice;
	  			   totalprevioussaleprice += previousSalePrice;
	  	   		}
	  	   		var superParentSaleInPer = (((totalcurrentsaleprice - totalprevioussaleprice) / totalcurrentsaleprice) * 100);
	  			if(!isFinite(superParentSaleInPer)){
	  				superParentSaleInPer = 0;
	  			}
	  		var  inputsuperparent = {
	  	        		"category":category,
	  	        	    "bgcolor":bgcolor,
	  	        	    "totalCurrentSale":Math.round(totalCurrentSale),
	  	        	    "totalPreviousSale":Math.round(totalPreviousSale),
	  	        		"inputjson":inputjson,
	  	        		"superParentSaleInPer":parseFloat(superParentSaleInPer.toFixed(1)),
	  	        		"categoryOrder":categoryOrder,
	  	        		"currentStartDate":currentStartDate,
	     	        		"currentEndDate":currentEndDate,
	     	        		"previousSatrtDate":previousSatrtDate,
	     	        		"previousEndDate":previousEndDate,
	     	        		"totalcurrentsaleprice":totalcurrentsaleprice,
	     	        		"totalprevioussaleprice":totalprevioussaleprice
	  	       };
	  		inputsupercategoryjson.push(inputsuperparent);
	  	   }
	  	$scope.categoryWiseCompareSale = inputsupercategoryjson;
	  //console.log(JSON.stringify(inputsupercategoryjson));
	  	$("#waitsec").css("display", "none");
	  	  }, function myError(response) {
	  		  $("#waitsec").css("display", "none");
	  	  });
	  	
	  }	  
/* Weekly and monthly sale comparision Start*/
$scope.includeZeroCompareSaleOrNot = function(){
	var catOrComp = $('#includeZeroCompareSaleOrNot').prop('checked');
   if(catOrComp === true){
	   $scope.showcompareCompanyDetails = false;
	   $scope.showcompareCategoryDetails = true;
   }else{
	   $scope.showcompareCompanyDetails = true;
	   $scope.showcompareCategoryDetails = false;
   }
	   
}
});
myApp.filter('roundup', function () {
    return function (value) {
        //return Math.ceil(NaN);
    	if(!isNaN(value))
    		return value.toFixed(1);
    	else
    		return 0.0;
    		
    };
})
myApp.filter("totalAmount", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(alldetails, i) {
             s += alldetails.totalPrice;
        });
        return Math.ceil(s);
    };
});
myApp.filter("soldBeerCase", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(saleCategoryDataOnlySale, i) {
        	//if(saleCategoryDataOnlySale.category === "BEER" || saleCategoryDataOnlySale.category === "BEER -FL")
        	if(saleCategoryDataOnlySale.category === "FOREIGN-BEER" || saleCategoryDataOnlySale.category === "PREMIUM-BIRA" || saleCategoryDataOnlySale.category === "PREMIUM-STRONG-BEER" || saleCategoryDataOnlySale.category === "PREMIUM-LAGER-BEER" || saleCategoryDataOnlySale.category === "CARLSBERG-PREMIUM" || saleCategoryDataOnlySale.category === "REGULAR-STRONG" || saleCategoryDataOnlySale.category === "REGULAR-LAGER")
             s += saleCategoryDataOnlySale.sumOfCase;
        });
        return Math.round(s);
    };
});
myApp.filter("soldLiquorCase", function() {
	return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(saleCategoryDataOnlySale, i) {
        	//if(!(saleCategoryDataOnlySale.category === "BEER" || saleCategoryDataOnlySale.category === "BEER -FL"))
        	if(!(saleCategoryDataOnlySale.category === "FOREIGN-BEER" || saleCategoryDataOnlySale.category === "PREMIUM-BIRA" || saleCategoryDataOnlySale.category === "PREMIUM-STRONG-BEER" || saleCategoryDataOnlySale.category === "PREMIUM-LAGER-BEER" || saleCategoryDataOnlySale.category === "CARLSBERG-PREMIUM" || saleCategoryDataOnlySale.category === "REGULAR-STRONG" || saleCategoryDataOnlySale.category === "REGULAR-LAGER"))
             s += saleCategoryDataOnlySale.sumOfCase;
        });
        return Math.round(s);
    };
});
myApp.filter('INR', function () {        
    return function (input) {
        if (! isNaN(input)) {
           // var currencySymbol = '₹';
        	 var currencySymbol = '';
            //var output = Number(input).toLocaleString('en-IN');   <-- This method is not working fine in all browsers!           
            var result = input.toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
            
            if (result.length > 1) {
                output += "." + result[1];
            }            

            return currencySymbol + output;
        }
    }
});
myApp.filter("conditionVal", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(alldetails, i) {
             s += alldetails.totalPrice;
        });
        return Math.ceil(s);
    };
});