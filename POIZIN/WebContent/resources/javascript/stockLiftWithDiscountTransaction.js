var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope, $http) {
	 $('.date-picker').datepicker( {
	        changeMonth: true,
	        changeYear: true,
	        showButtonPanel: true,
	        dateFormat: 'MM yy',
	        onClose: function(dateText, inst) { 
	            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
	        }
	    });
	 $("#startDate").val($.datepicker.formatDate('MM yy', new Date()));
	 buildstockLifting($.datepicker.formatDate('MM yy', new Date()));

var myUrl = window.location.href;
var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
		
$scope.getResults = function (startDate){
	var startDate = $("#startDate").val();
	 if(validateCalendarField()==false){return false;}
	 buildstockLifting(startDate);
	 
}
function validateCalendarField(){
	var startDate = $("#startDate").val();
	if(startDate == ""){
		alert("Please fill Start Date.");
		return false;
	}
	
	return true;
}
$scope.greaterThan = function(prop, val){
    return function(item){
      return item[prop] > val;
    }
}
$scope.sortKey = ['companyOrder','category'];
$scope.sort = function(propName1,propName2){
      $scope.sortKey = [propName1,propName2];
      $scope.reverse = !$scope.reverse;
  }
function  buildstockLifting(startDate){
	$("#wait").css("display", "block");
	$("#setDateValueID").text(startDate);
	var n = startDate.split(" ");
    var val1 = n[n.length - 2];
    var val2 = n[n.length - 1];
    var date="01/"+val1+"/"+val2;
	var myUrl = window.location.href;
	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
	var callingUrl = url+"getStockLiftingWithDiscount.json?date="+date;
	$http({
	    method : "GET",
	    url : callingUrl
	  }).then(function mySuccess(response) {
		//  console.log(JSON.stringify(response.data));
		  $scope.alldetails = response.data;
   	      $scope.categories = response.data;
      	$("#wait").css("display", "none");
	  }, function myError(response) {
		$("#wait").css("display", "none");
});
	
}
$scope.onChnage = function(discountamount,brandno){
	var totalcases = parseInt($("#ttlcases"+brandno).text());
	var totalDiscountAmount=totalcases*discountamount;
	if(totalDiscountAmount > 0){
		$('#ttldiscoutamount'+brandno).html(Math.round(totalDiscountAmount));
	}
	else
		$('#ttldiscoutamount'+brandno).html(0);
	
	var ttlDicAmt=0;
	var table = $("#discounttab tbody");
	 table.find('tr').each(function (i) {
	        var $tds = $(this).find('td'),
	            discountTotalAmount = parseInt($tds.eq(11).text())
	            ttlDicAmt=ttlDicAmt+discountTotalAmount;
	    });
	 
	 $('#discountamountid').html(ttlDicAmt);
	
};


$scope.afterADJ = function(adj,brandNo,ttlCase){
    var data = ttlCase+(adj);
    $('#ttlcases'+brandNo).html(data);
    var discountAmt = $('#damount'+brandNo).val();
    $scope.onChnage(discountAmt,brandNo);
};
$scope.afterTarget = function(target,brandNo,liftedCase){
    var data = target - liftedCase;
    if(data > 0)
    $('#pending'+brandNo).html(data);
    else
    	 $('#pending'+brandNo).html(0);
};

function validateNum(number){
	var regex = /^[0-9]*(?:\d{1,2})?$/;
	return regex.test(number);
}
$scope.savediscountDetails = function(){
	  var json = {
		    	"discountDetails": [],
		}
	  
		var table = $("#discounttab tbody");
		 table.find('tr').each(function (i) {
		        var $tds = $(this).find('td'),
		            brandName = $tds.eq(0).text(),
		            brandNo = $tds.eq(2).text(),
		            caseRate = $tds.eq(3).text(),
		            inHousestock = $tds.eq(4).text(),
		            totalcases = $tds.eq(5).text(),
		            adj = $tds.eq(6).text(),
		            ttlCases = $tds.eq(7).text(),
		            target = $tds.eq(8).text(),
		            pending = parseInt($tds.eq(9).text()),
		            discAmt = $tds.eq(10).text(),
		            discountTotalAmount = $tds.eq(11).text(),
		            companyId = $tds.eq(12).text()
		            
		            var discountAmount = $('#damount'+brandNo).val();
		            var Adjustment = parseInt($('#adjustment'+brandNo).val());
		            var targetVal =  parseInt($('#target'+brandNo).val());
		            if(isNaN(Adjustment)){
		            	Adjustment=0;
			             }
		            if(isNaN(discountAmount)){
		            	discountAmount=0;
			             }
		            if(isNaN(targetVal)){
		            	targetVal=0;
			             }
		        var input = {
		        		"brandNo":brandNo,
		        		"discountAmount":discountAmount,
		        		"adjustment":Adjustment,
		        		"discountTotalAmount":discountTotalAmount,
		        		"companyId":companyId,
		        		"target":targetVal,
		        		"totalDiscount":(discountAmount * target)+""
		        };
		        json.discountDetails.push(input);
		    });
		 if((Object.keys(json.discountDetails).length) > 0)
		saveDiscountData(json);
	};
	function saveDiscountData(json){
		var startDate = $("#setDateValueID").text();
		var n = startDate.split(" ");
		var val1 = n[n.length - 2];
	    var val2 = n[n.length - 1];
	    var date="01/"+val1+"/"+val2;
		 var myUrl = window.location.href;
		  var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
		  var discountListUrl = url+"saveStockLiftWithDiscountData?date="+date;
				$http({
				    method : "POST",
				    contentType : 'application/json; charset=utf-8',
				    data: JSON.stringify(json),
				    url : discountListUrl,
				    headers: { 'x-my-custom-header': ip }
				  }).then(function mySuccess(response) {
					  alert(response.data.message);
					  location.reload(true);
				  }, function myError(response) {
					  alert("error"+response.data.message);
					  location.reload(true);
				  });
				
	  }
	function saveDiscountDataSecond(json){
		var startDate = $("#setDateValueID").text();
		var n = startDate.split(" ");
		var val1 = n[n.length - 2];
	    var val2 = n[n.length - 1];
	    var date="01/"+val1+"/"+val2;
	    //console.log(JSON.stringify(json)+" date>> "+date);
		 var myUrl = window.location.href;
		  var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
		  var discountListUrl = url+"saveStockLiftWithDiscountData?date="+date;
				$http({
				    method : "POST",
				    contentType : 'application/json; charset=utf-8',
				    data: JSON.stringify(json),
				    url : discountListUrl,
				    headers: { 'x-my-custom-header': ip }
				  }).then(function mySuccess(response) {
				  }, function myError(response) {
				  });
				
	  }
	// Start logic for vendor discount
$scope.openVendorDiscountModelPopup = function(){
	var startDate = $("#setDateValueID").text();
	$scope.arrearsdetails = null;
	$scope.afterAdj = null;
	$scope.vendorShowDate = startDate;
	
	var n = startDate.split(" ");
	var val1 = n[n.length - 2];
    var val2 = n[n.length - 1];
    var date="01/"+val1+"/"+val2;	
    var company = $scope.companyfilter;
    $scope.vendorcompanyId = company;
    $scope.getselectedCompany(company,date);
    $scope.getExistingBreakage(date);
$('#updateVendorDiscount').modal('show');


}	
$scope.getselectedCompany = function(company,date){
   if(date != "" && date != null && date != undefined && company != "" && company != null && company != undefined){
	   var existingArrearsUrl = url+"getExistingSingleCompanyArrears?discountMonth="+date+"&company="+company;
		$http({
		    method : "GET",
		    url : existingArrearsUrl
		  }).then(function mySuccess(response) {
			 //console.log(JSON.stringify(response.data));
			  $scope.vendorAmount = null;
			  $scope.arrearsdetails = response.data;
			  $scope.afterAdj = $scope.arrearsdetails.arrears;
		  }, function myError(response) {
		    //alert(response);
		  });
	   
    }
   }
$scope.getselectedVendorCompany = function(){
	var startDate = $("#setDateValueID").text();
	var n = startDate.split(" ");
	var val1 = n[n.length - 2];
    var val2 = n[n.length - 1];
    var date="01/"+val1+"/"+val2;	
    var company = $scope.vendorcompanyId;
    $scope.getselectedCompany(company,date);
}

$scope.textChanged = function(){
	//console.log($scope.vendorAmount);
	$scope.afterAdj = $scope.arrearsdetails.arrears -  $scope.vendorAmount;
}
$scope.saveVendorDiscount = function(){
	var company = $scope.vendorcompanyId;
	var startDate = $("#setDateValueID").text();
	var n = startDate.split(" ");
	var val1 = n[n.length - 2];
    var val2 = n[n.length - 1];
    var vendorMonth="01/"+val1+"/"+val2;
    var vendorAmount = $scope.vendorAmount;
    var vendorcomment = $scope.vendorcomment;
    
    if(company == "" || company == undefined){
		alert("Please select company.");
		return false;
	}
    if(vendorMonth == ""){
		alert("Please select Month.");
		return false;
	}
    if((vendorAmount == "") || isNaN(vendorAmount)){
		alert("Please Enter Amount");
		return false;
	}
    if(vendorAmount <= 0){
    	alert("Amount Should be greater than zero");
		return false;
    }
    if(vendorcomment == ""){
		alert("Please Enter Comment.");
		return false;
	}
    var formData = new FormData();
	formData.append('companyId',company); 
	formData.append('vendorMonth',vendorMonth); 
	formData.append('vendorAmount',vendorAmount); 
	formData.append('vendorcomment',vendorcomment); 
	
	$.ajax({
		async : false,
        url: url+"/saveVendorAdjustment",
        data: formData,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(data){
         $('#updateVendorDiscount').modal('hide');
        	alert(data);
        },
        error: function(data){
        	//$('#updateVendorDiscount').modal('hide');
        	alert(data);
        }
	    
    });
	
}
	// End logic for vendor discount

// Start Logic for Rental And Breackage
$scope.openRentalModelPopup = function(){
	var startDate = $("#setDateValueID").text();
	$scope.rentalAmount = 0;
	$scope.rentalShowDate = startDate;
	
	var n = startDate.split(" ");
	var val1 = n[n.length - 2];
    var val2 = n[n.length - 1];
    var date="01/"+val1+"/"+val2;	
    var company = $scope.companyfilter;
    $scope.rentalcompanyId = company;
    $scope.getExistingRental(company,date);
$('#addRentalPopup').modal('show');
}
$scope.openBreakageModelPopup = function(){
	var startDate = $("#setDateValueID").text();
	var breakageAmount = 0;
    var breakagecomment = null;
	$scope.breakageShowDate = startDate;
	
	var n = startDate.split(" ");
	var val1 = n[n.length - 2];
    var val2 = n[n.length - 1];
    var date="01/"+val1+"/"+val2;	
    $scope.getExistingBreakage(date);
$('#addBreakagePopup').modal('show');
}
$scope.getExistingRental = function(company,date){
	if(date != "" && date != null && date != undefined && company != "" && company != null && company != undefined){
		var existingRentalUrl = url+"getExistingRentalForAMonth?rentalMonth="+date+"&company="+company;
		$http({
		    method : "GET",
		    url : existingRentalUrl
		  }).then(function mySuccess(response) {
			  $scope.rentalAmount = response.data.amount;
		  }, function myError(response) {
		    //alert(response);
		  });
		   
	    }
}

$scope.getselectedCompanyForRental = function(){
	var startDate = $("#setDateValueID").text();
	var n = startDate.split(" ");
	var val1 = n[n.length - 2];
    var val2 = n[n.length - 1];
    var date="01/"+val1+"/"+val2;	
    var company = $scope.rentalcompanyId;
    $scope.getExistingRental(company,date);
	
}
$scope.saveOrUpdateRentalAmount = function(){
	var company = $scope.rentalcompanyId;
	var startDate = $("#setDateValueID").text();
	var n = startDate.split(" ");
	var val1 = n[n.length - 2];
    var val2 = n[n.length - 1];
    var rentalMonth="01/"+val1+"/"+val2;
    var rentalAmount = $scope.rentalAmount;
    if(company == "" || company == undefined){
		alert("Please select company.");
		return false;
	}
    if(rentalMonth == ""){
		alert("Please select Month.");
		return false;
	}
    if((rentalAmount == "") || isNaN(rentalAmount)){
		alert("Please Enter Amount");
		return false;
	}
    if(rentalAmount <= 0){
    	alert("Amount Should be greater than zero");
		return false;
    }
    var formData = new FormData();
	formData.append('rentalcompanyId',company); 
	formData.append('rentalMonth',rentalMonth); 
	formData.append('rentalAmount',rentalAmount); 
	
	$.ajax({
		async : false,
        url: url+"/saveOrUpdateRental",
        data: formData,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(data){
         $('#addRentalPopup').modal('hide');
        	alert(data);
        },
        error: function(data){
        	alert(data);
        }
	    
    });
	
}
$scope.getExistingBreakage = function(date){
	if(date != "" && date != null && date != undefined){
		   var existingBreakageUrl = url+"getExistingBreakageForAMonth?breakageMonth="+date;
			$http({
			    method : "GET",
			    url : existingBreakageUrl
			  }).then(function mySuccess(response) {
				// console.log(JSON.stringify(response.data));
				  $scope.breakageAmount = response.data.amount;
				  $scope.breakagecomment = response.data.comment;
			  }, function myError(response) {
			    //alert(response);
			  });
	   }
}
$scope.saveOrUpdateBreakageAmount = function(){
	var startDate = $("#setDateValueID").text();
	var n = startDate.split(" ");
	var val1 = n[n.length - 2];
    var val2 = n[n.length - 1];
    var breakageMonth="01/"+val1+"/"+val2;
    var breakageAmount = $scope.breakageAmount;
    var breakagecomment = $scope.breakagecomment;
    if(breakageMonth == ""){
		alert("Please select Month.");
		return false;
	}
    if((breakageAmount == "") || isNaN(breakageAmount)){
		alert("Please Enter Amount");
		return false;
	}
    if(breakageAmount <= 0){
    	alert("Amount Should be greater than zero");
		return false;
    }
    if(breakagecomment == ""){
		alert("Please Enter Comments.");
		return false;
	}
    var formData = new FormData();
	formData.append('breakageAmount',breakageAmount); 
	formData.append('breakagecomment',breakagecomment); 
	formData.append('breakageMonth',breakageMonth); 
	
	$.ajax({
		async : false,
        url: url+"/saveOrUpdateBreakage",
        data: formData,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(data){
        	$('#addBreakagePopup').modal('hide');
        	alert(data);
        },
        error: function(data){
        	alert(data);
        }
	    
    });
	
}
//Start Logic for Rental And Breackage
});

app.filter('unique', function () {

    return function (items, filterOn) {

        if (filterOn === false) {
            return items;
        }

        if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
            var hashCheck = {}, newItems = [];

            var extractValueToCompare = function (item) {
                if (angular.isObject(item) && angular.isString(filterOn)) {
                    return item[filterOn];
                } else {
                    return item;
                }
            };

            angular.forEach(items, function (item) {
                var valueToCheck, isDuplicate = false;

                for (var i = 0; i < newItems.length; i++) {
                    if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                        isDuplicate = true;
                        break;
                    }
                }
                if (!isDuplicate) {
                    newItems.push(item);
                }

            });
            items = newItems;
        }
        return items;
    };
});
app.filter("sumOfDiscount", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(alldetails, i) {
            s += alldetails.totalDiscount;
        });
        return Math.ceil(s);
    };
})
app.filter('INR', function () {        
    return function (input) {
        if (! isNaN(input)) {
           // var currencySymbol = '₹';
        	 var currencySymbol = '';
            //var output = Number(input).toLocaleString('en-IN');   <-- This method is not working fine in all browsers!           
            var result = input.toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
            
            if (result.length > 1) {
                output += "." + result[1];
            }            

            return currencySymbol + output;
        }
    }
});

