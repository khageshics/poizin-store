var app = angular.module("myApp", []);

app.controller("myCtrl", function($scope,$http) {
	
	
	/*Onload date picker db call for dates*/
	var myUrl = window.location.href;
    var url = myUrl.substring(0, myUrl.lastIndexOf('/'));
    var productListUrl = url+"/getTotalInvoiceDate.json";
	$.ajax ({
		url: productListUrl,
	       type: "POST",
	       success: function(data){
	    	  
	    		   var startDate="";
	    		   try{
	    		 startDate=  data[data.length-1].date;
	    		   }catch(e){
	    			   
	    		   }
	      $( "#datepicker" ).datepicker({dateFormat: 'dd-mm-yy',minDate:startDate}).datepicker("setDate",startDate);
	    	   
	       }
	});
    
   /* $.ajax ({
		url: productListUrl,
	       type: "POST",
	       success: function(data){
	     // console.log(JSON.stringify(data));
	      var startDate="";
	      var enableDays =[];
	      $.each(data, function (index, value) {
	    	  startDate=data[index].date;
	      });
	      $( "#datepicker" ).datepicker({
	    	  dateFormat: 'dd-mm-yy',
	          minDate: startDate
	      });
	    }
	});*/
	
	var productListUrl =  url+"/getDropDownListFromProduct";
	$http({
	    method : "GET",
	    url : productListUrl
	  }).then(function mySuccess(response) {
		  $scope.productDetails = response.data;
	  }, function myError(response) {
	  });
	var productListUrl =  url+"/getCategoryList";
	$http({
	    method : "GET",
	    url : productListUrl
	  }).then(function mySuccess(response) {
		  $scope.categoryList = response.data;
	  }, function myError(response) {
	  });
	var productListUrl =  url+"/getCompanyList";
	$http({
	    method : "GET",
	    url : productListUrl
	  }).then(function mySuccess(response) {
		  $scope.companyList = response.data;
	  }, function myError(response) {
	  });
	var groupListUrl =  url+"/getGroupList";
	$http({
	    method : "GET",
	    url : groupListUrl
	  }).then(function mySuccess(response) {
		  $scope.groupList = response.data;
		  //console.log(JSON.stringify(response.data));
	  }, function myError(response) {
	  });
	
	
	
  //console.log(myUrl+" :"+url);
//	var myUrl = "http://localhost:8081/OCR/";
  var myUrl=url;
	$scope.chooseDivShow=true;
	$scope.tableShow=false;
	$scope.progressBar=false;
	 $scope.alertMsg="Suggesions are not available..";
	 $scope.loader_show=false;
	 

	 
	  
	  
	  $scope.progressBar = function(){
		  $('#progreesBarModal').modal('show');
		  
		  var elem = document.getElementById("progressId");   
		  var width = 0;
		  var id = setInterval(frame, 400);
		  function frame() {
		    if (width >= 100) {
		      clearInterval(id);
		    } else {
		      width++; 
		      elem.style.width = width + '%'; 
		      elem.innerHTML = width * 1  + '%';
		    }
		  }
	   }
	  
	$scope.fileUpload = function(){
		$scope.tableShow=false;
		if($('#bannerImage')[0].files[0]==null){
	alert("Null request triggered...");
	
      }else{
    		//$scope.progressBar=true;
    	// alert("Request is Processing...");
    	   $scope.progressBar();
    
      }
    var formData = new FormData();
    formData.append('bulkUploadFile', $('#bannerImage')[0].files[0]);
    var bulkUrl = myUrl+"/ocrInvoiceUpload";
    
    
    var request = {
            method: 'POST',
            url: bulkUrl,
            data: formData,
            headers: {
                'Content-Type': undefined
            }
        };
    
    $http(request)
    .success(function (data) {
    	  $scope.tableShow=true;
   	   $scope.invoice=data;
   	   $scope.invoice1=$scope.invoice[0];
   	   $scope.invoice2=$scope.invoice[1];
  	   
   	  /*console.log(JSON.stringify(  $scope.invoice1));*/
   
   	   
   	   /*Update SalesValue*/
	   var salesValue=0;
   	   try{
  
   
   	   
   	   for(i=0;i<$scope.invoice1.length;i++){
   		/* total=$scope.invoice1[i].total;*/
   		    
   		    try{
   		    	var caseDeliveredQty=parseFloat($scope.invoice1[i].caseDeliveredQty);
   		    	var unitRate=parseFloat($scope.invoice1[i].unitRate);
   		    	var bottelsDeliveredQty=parseFloat($scope.invoice1[i].bottelsDeliveredQty);
   		    	var btlRate=parseFloat($scope.invoice1[i].btlRate);
   		    	var rowTotal=((caseDeliveredQty*unitRate) + (bottelsDeliveredQty*btlRate)).toFixed(2);
   	            $scope.invoice1[i].total=rowTotal;
   	            if(rowTotal>0){
   	            salesValue=salesValue+parseFloat(rowTotal);
   	            }
   	            	           
   	            
   	
   		    }catch(e){
   		    	
   		    }	   	
   			   		   
   	   }
   	   
   	  try{
		   var invoiceDate=$scope.invoice2.invoiceDate;
	           var dateSpliter=invoiceDate.split("-");
	           if(dateSpliter.length==3){
	        	$scope.yearLocal=dateSpliter[0];
  			$scope.monthLocal="00";
  			$scope.dateLocal=dateSpliter[0];
  			
  			 switch(dateSpliter[1]) {
  			 case "Jan":
  				 $scope.monthLocal="01";
  			    break;
  			 case "Feb":
  				 $scope.monthLocal="02";
  			    break;
  			 case "Mar":
  				 $scope.monthLocal="03";
  			    break;
  			 case "Apr":
  				 $scope.monthLocal="04";
  			    break;
  			 case "May":
  				 $scope.monthLocal="05";
  			    break;
  			 case "Jun":
  				 $scope.monthLocal="06";
  			    break;
  			 case "Jul":
  				 $scope.monthLocal="07";
  			    break;
  			  case "Aug":
  				 $scope.monthLocal="08";
  			    break;
  			 case "Sep":
  				 $scope.monthLocal="09";
  			    break;
  			case "Sept":
  				 $scope.monthLocal="09";
  			    break;
  			 case "Oct":
  				 $scope.monthLocal="10";
  			    break;
  			 case "Nov":
  				 $scope.monthLocal="11";
  			    break;
  			 case "Dec":
  				 $scope.monthLocal="12";
  			    break;
  			 
  			  default:
  				 $scope.monthLocal="00";
  			} 
  			//$scope.invoice2.invoiceDate = $scope.yearLocal+"-"+$scope.monthLocal+"-"+$scope.dateLocal;//upload date
  			$scope.invoice2.invoiceDate = $scope.dateLocal+"-"+$scope.monthLocal+"-"+$scope.yearLocal;//upload date
  			
	           }else{
	        	$scope.invoice2.invoiceDate="00-00-0000";
	           }
  			   
  			
  		
	   }catch(e){
		   $scope.tableShow=false;
	   		 alert("Unable to process this invoice,please upload another invoice..");
	   }
   	   
   	   
   	   
    	
   	   }catch(e){
   		 $scope.tableShow=false;
   		 alert("Unable to process this invoice,please upload another invoice..");
   	   }
   	$scope.salesValue=salesValue;
   	   
   	//$scope.salesValue = $scope.invoice1.reduce(function (a,b){ return a + Number(b.total)},0);
   	   
   //	console.log(JSON.stringify($scope.invoice1));
   	
   	   $('#progreesBarModal').modal('hide');
   	   
   	/*  if(data.status==200){
   		  
   	  }else{
   		alert(data.message);
   	  }*/
   	   
    })
    .error(function (errer) {
    	$('#progreesBarModal').modal('hide');
    	alert(errer.message);
    });
    

	/*$scope.progressBar=false;*/
    
}
	
	
	$scope.salesValueCalc=function(){
		  /*Update SalesValue*/
		   try{

			   var total=0.0;
			   var table = $("#invoice-table tbody");
	  		  table.find('tr').each(function(i){
	  			
	  			  var $tds=$(this).find('td');
	  			 
	  			var caseDeliveredQty= parseFloat($(this).find("td:eq(8) input[type='text']").val());
	  			var bottelsDeliveredQty= parseFloat($(this).find("td:eq(9) input[type='text']").val());
	  			var unitRate= parseFloat($(this).find("td:eq(10) input[type='text']").val());
	  			var btlRate=parseFloat($(this).find("td:eq(11) input[type='text']").val());
	  			
	           
	  		      $scope.rowTotal=((caseDeliveredQty*unitRate) + (bottelsDeliveredQty*btlRate)).toFixed(2); 
	  		      $scope.invoice1[i].total=$scope.rowTotal;
			          
			           if($scope.rowTotal>0){
			           	
			        	   
			        	   total=total+parseFloat($scope.rowTotal); 
			           }
					
					
				 });
	  		  
	 	$scope.salesValue=total;
	 	

	 	
		   }catch(e){
			   
		   }
		   
		   /*console.log(JSON.stringify($scope.invoice1));*/
		 
		   
	}
	
$scope.editTableRow = function(brandNumber,data,index){
	/*console.log(JSON.stringify(index));
	$scope.invoice1[index].data = "Khagesh";
	*/
	console.log(JSON.stringify($scope.invoice1));
	$scope.indexnum = index;
	$scope.brandNo=brandNumber;
	$scope.data=data;
	/*alert(brandNumber+"   "+data);*/
	
	   $http({
	        method : "GET",
	        url :  myUrl+"/ocrSuggestion?brand_No="+brandNumber+"&FlagData="+data
	      }).then(function mySuccess(response) {
	         //console.log(JSON.stringify(response.data));
	     
	     	   $scope.suggData=response.data;
	     	   if(response.data.length==0){
	            $scope.alertMsg="Suggesions are not available..";
	     			$('#editOCRRowSugg').modal('show');
	     	   }else{
	     	 
	     	$('#editTableRow').modal('show');
	     	   }
	         
	      }, function myError(response) {
	          
	    	  //console.log("Error:: "+JSON.stringify(response.data));
	    	  /*alert("Suggestions are not available")*/
	    	  $scope.alertMsg="Suggestions are not available";
	    	  $('#editOCRRowSugg').modal('show');
	    	  
	      });
	
	 //Update total value
	   $scope.salesValueCalc();
	
	
	
}

$scope.packTypeEditTableRow=function(brandNumber,data,index){
	console.log(JSON.stringify($scope.invoice1));
	$scope.indexnum = index;
	$scope.brandNo=brandNumber;
	$scope.data=data;
	/*alert(brandNumber+"   "+data);*/
	
	   $http({
	        method : "GET",
	        url :  myUrl+"/ocrSuggestionPackType?brand_No="+brandNumber+"&FlagData="+data
	      }).then(function mySuccess(response) {
	         //console.log(JSON.stringify(response.data));
	     
	     	   $scope.suggData=response.data;
	     	   if(response.data.length==0){
	            $scope.alertMsg="Suggesions are not available..";
	     			$('#editOCRRowSugg').modal('show');
	     	   }else{
	     	 
	     	$('#packQtyEditTableRow').modal('show');
	     	   }
	         
	      }, function myError(response) {
	          
	    	  //console.log("Error:: "+JSON.stringify(response.data));
	    	  /*alert("Suggestions are not available")*/
	    	  $scope.alertMsg="Suggestions are not available";
	    	  $('#editOCRRowSugg').modal('show');
	    	  
	      });
	
	 //Update total value
	   $scope.salesValueCalc();
}

$scope.editPackQty=function(brandNumber,data,index){
	$scope.indexnum = index;
	$scope.brandNo=brandNumber;
	$scope.data=data;
	
	$http({
		  method : "GET",
	        url :  myUrl+"/ocrSuggestionPackQty?brand_No="+brandNumber+"&FlagData="+data
	}).then(function mysuccess(response){
		

  	   $scope.suggPackQty=response.data;
  	   // console.log(JSON.stringify($scope.suggPackQty));
  	   if(response.data.length==0){
  
  			$('#editPackQty').modal('show');
  	   }else{
  	 
  	$('#editPackQty').modal('show');
  	   }
  	   
	},function fail(response){
		alert("Suggestions are not available")
	})
	
	 $scope.salesValueCalc();
}




$scope.ocrRowUpdate = function(brandNo,resultdata){
	
	if($scope.data == 'brandName'){
		$scope.invoice1[$scope.indexnum].brandName = resultdata;
	    $scope.invoice1[$scope.indexnum].brand_Name_Flag=true;
	}else if($scope.data == 'productType'){
		$scope.invoice1[$scope.indexnum].productType = resultdata;
		$scope.invoice1[$scope.indexnum].prod_Type_Flag=true;
	}else if($scope.data == 'packType'){
		$scope.invoice1[$scope.indexnum].packType = resultdata.packType;
		var quantity=resultdata.quantity.replace('ml','').trim();
		$scope.invoice1[$scope.indexnum].packSize = quantity;
		$scope.invoice1[$scope.indexnum].pack_Type_Flag=true;
		$('#packQtyEditTableRow').modal('hide');
	}else if($scope.data == 'packQty'){
		$scope.invoice1[$scope.indexnum].packQty = resultdata;
	    $scope.invoice1[$scope.indexnum].pack_Qty_Flag=true;
	}else if($scope.data == 'packSize'){
		$scope.invoice1[$scope.indexnum].packSize = resultdata.replace('ml','');
		$scope.invoice1[$scope.indexnum].pack_Size_Flag=true;
	}
	
	/*console.log(JSON.stringify($scope.invoice1));*/
	console.log(JSON.stringify($scope.invoice1));
	/*
	
	$scope.invoice1[$scope.indexnum].brandName = data;
	$scope.brandNo=brandNo;
	$scope.data=data;*/
	
	//console.log("Data :: "+brandNo+"  "+data);
	$('#editTableRow').modal('hide');
	
	//Update total value
	 $scope.salesValueCalc();

}

$scope.packQtySizeUpdate = function(index,resultdata){
	
	
		$scope.invoice1[$scope.indexnum].packQty = resultdata.packQty;
	    $scope.invoice1[$scope.indexnum].pack_Qty_Flag=true;

		$scope.invoice1[$scope.indexnum].packSize = resultdata.quantity.replace('ml','');
		$scope.invoice1[$scope.indexnum].pack_Size_Flag=true;
		
		$scope.invoice1[$scope.indexnum].btlRate=resultdata.SingleBottelRate;
		$scope.invoice1[$scope.indexnum].unitRate=resultdata.packQtyRate;
		

	$('#editPackQty').modal('hide');
	
	//Update total value
	 $scope.salesValueCalc();
	 
	//Get btlMRP and Margin
	 $http({
		  method : "GET",
	        url :  myUrl+"/suggBtlMrpAndSplMargin?brand_No="+$scope.invoice1[$scope.indexnum].brandNumber+"&quantity="+$scope.invoice1[$scope.indexnum].packSize
	}).then(function mysuccess(response){
		

 	   $scope.respData=response.data;
 	  $scope.invoice1[$scope.indexnum].btlMrp=$scope.respData[0];
 	 $scope.invoice1[$scope.indexnum].margin=$scope.respData[1];
 	 $scope.invoice1[$scope.indexnum].btlMrp_Flag=$scope.respData[2];
 	 $scope.invoice1[$scope.indexnum].margin_Flag=$scope.respData[3];
 	    /*console.log(JSON.stringify($scope.respData));*/
 	 
 	   
	},function fail(response){
		alert("Error")
	});

}


	$scope.ocrCalculation=function(brandNumber,column,index){
		
		alert();
		$scope.packQty=$scope.invoice1[index].packQty;
	    $scope.packSize=$scope.invoice1[index].packSize;
	    $scope.caseDelivered=$scope.invoice1[index].caseDelivered;
	    $scope.bottelsDelivered=$scope.invoice1[index].bottelsDelivered;
	    $scope.btlRate=$scope.invoice1[index].btlRate;
	    
	    $scope.unitRate=$scope.packQty * $scope.btlRate;
		
		if(column=='caseDelivered'){
			
		 
			$scope.invoice1[index].unitRate=$scope.unitRate;
			
		}
		
		//Update total value
		 $scope.salesValueCalc();
		
	}
	
	$scope.ocrEditData=function(rowIndex,columnType){
		
		if(columnType=='brandNumber'){
			$scope.invoice1[rowIndex].brand_No_Flag=true;
	    }else if(columnType == 'brandName'){
		    $scope.invoice1[rowIndex].brand_Name_Flag=true;
		}else if(columnType == 'productType'){
			$scope.invoice1[rowIndex].prod_Type_Flag=true;
		}else if(columnType == 'packType'){
			$scope.invoice1[rowIndex].pack_Type_Flag=true;
		}else if(columnType == 'packQty'){
		    $scope.invoice1[rowIndex].pack_Qty_Flag=true;
		}else if(columnType == 'packSize'){
			$scope.invoice1[rowIndex].pack_Size_Flag=true;
		}
		console.log(JSON.stringify($scope.invoice1));
		
		//Update total value
		 $scope.salesValueCalc();
	}
	
	
	
	
	$scope.ocrSubmit=function(flag){
		/*..............Invoice1 data validation validation............*/
		$scope.submitFlag=false;
		for(var i=0;i<$scope.invoice1.length;i++){
			$scope.submitFlag=false;
		if(($scope.invoice1[i].brandNumber).toString().length>0 && (($scope.invoice1[i].brandNumber)==null)==false ){
			if(($scope.invoice1[i].brandName)!=null &&($scope.invoice1[i].brandName).toString().length>0){
				if ((($scope.invoice1[i].productType)==null)==false && ($scope.invoice1[i].productType).toString().length>0 ){
					if((($scope.invoice1[i].packType)==null)==false && ($scope.invoice1[i].packType).toString().length>0){
						if(($scope.invoice1[i].packQty).toString().length>0 && (($scope.invoice1[i].packQty)==null)==false){
							if(($scope.invoice1[i].packSize).toString().length>0 && (($scope.invoice1[i].packSize)==null)==false){
								if((($scope.invoice1[i].caseDeliveredQty)==null)==false){
									if((($scope.invoice1[i].bottelsDeliveredQty)==null)==false){
										if((($scope.invoice1[i].unitRate)==null)==false){
											if((($scope.invoice1[i].btlRate)==null)==false){
												if((($scope.invoice1[i].btlMrp)==null || ($scope.invoice1[i].btlMrp)==0)==false){
													if((($scope.invoice1[i].margin)==null)==false){
												if((($scope.invoice1[i].total)==null)==false){
													
											        $scope.submitFlag=true;

												}else{
													alert("Total should not be empty");
													break;	
												}
													}else{
														alert("Margin should not be empty or Zero");
														break;	
													}
												}else{
													alert("Bottel MRP should not be empty or Zero");
													break;	
												}
											}else{
												alert("BottelRate should not be empty");
												break;	
											}
		
										}else{
											alert("UnitRate should not be empty");
											break;	
										}
									}else{
										alert("BottelsDeliveredQty should not be empty");
										break;
									}
		
								}else{
									alert("CaseDeliveredQty should not be empty");
									break;
								}
							}else{
								alert("PackSize should not be empty or Zero");
								break;
							}
						}else{
							alert("PackQty should not be empty or Zero");
							break;
						}
					}else{
						alert("PackType should not be empty");
						break;
					}
		
				}else{
					alert("ProductType should not be empty");
					break;
				}
			}else{
				alert("BrandName should not be empty");
				break;
			}
		
		}else{
			alert("BrandNumber should not be empty or Zero");
			break;
		}
		}

	

		
		/*Invoice2 data validation*/
		if($scope.submitFlag==true){
		for(var i=0;i<1;i++){
			  var dateSpliter=(changeDateFormat($('#datepicker2').val())).split("-");
	         
	        	$scope.yearLocal=dateSpliter[0];
		if($('#datepicker2').val()==null || ($('#datepicker2').val())==="" || ($('#datepicker2').val())==="00-00-0000" || $scope.yearLocal.length<4){
			alert("Please select InvoiceDate");
			break;
		}else{
			
			
			
		
		
		try{
		
		 $('#loader_show').modal('show');
		 
		 $scope.invoiceDate=0;
		 var salestable = $("#salesTable tbody");
		 
		 $scope.mrpRoundOff=0;
		 $scope.turnoverTax=0;
		 $scope.tcsValue=0;
		 $scope.retailerCreditVal=0;
		 
		 salestable.find('tr').each(function(val){
			 
			  var $tds=$(this).find('td');
			 
			     var  dateAsPerInvoice = changeDateFormat($tds.find("#datepicker2").val());
			     $scope.invoiceDate=dateAsPerInvoice;
     
			     $scope.mrpRoundOff =  parseFloat($tds.eq(2).text().replace(',', ''));
			     $scope.turnoverTax =  parseFloat($tds.eq(3).text().replace(',', ''));
			     $scope.tcsValue =  parseFloat($tds.eq(4).text().replace(',', ''));
			     $scope.retailerCreditVal = parseFloat($tds.eq(5).text().replace(',', ''));
				 
		  })
	
		  
		  if(isNaN($scope.retailerCreditVal)){
				$('#loader_show').modal('hide');
				alert("Please check RetailerCreditvalue");
			 }else{
				 if(isNaN($scope.tcsValue)){
						$('#loader_show').modal('hide');
					 alert("Please check TCS Value");
				 }else{
					 
					 if(isNaN($scope.mrpRoundOff)){
							$('#loader_show').modal('hide');
						 alert("Please check MRPRoundOff Value");
					 }else{
					
		  
		  
		

		$http({
			  method: 'POST',
			  url :  myUrl+"/btlMrpAndSplMargin",
	            data: $scope.invoice1,
	            headers: {
	                'Content-Type': 'application/json; charset=utf-8'
	            }
	       
	        
	      }).then(function mySuccess(response) {
	         //console.log(JSON.stringify(response.data));
	     
	     	   $scope.mrpAndMarginData=response.data;
	     	   if(response.status==204){
	     
	     			alert("New products found,Please save new products before submit....");
	     	   }else{
	     	 
	     		//  console.log(JSON.stringify( $scope.mrpAndMarginData));
	     		 var json = {"invoiceDetails": []};
	     		 var table = $("#invoice-table tbody");
	     		  table.find('tr').each(function(i){
	     			 console.log("i value is :"+i);
	     			  var $tds=$(this).find('td'),
	     			 
			            sno = $tds.eq(1).text(),
			            brandNumber = $(this).find("td:eq(2) input[type='text']").val(),
			             brandName = $(this).find("#brandName").val(),
			             productType = $(this).find("td:eq(4) input[type='text']").val(),
			             packTypelbl = $(this).find("td:eq(5) input[type='text']").val(),
			             packQtyId = $(this).find("td:eq(6) input[type='text']").val(),
			             quantity = $(this).find("td:eq(7) input[type='text']").val(),
			             caseQty = $(this).find("td:eq(8) input[type='text']").val(),
			             QtyBottels = $(this).find("td:eq(9) input[type='text']").val(),
			             packQtyRate = $(this).find("td:eq(10) input[type='text']").val(),
			             SingleBottelRate = $(this).find("td:eq(11) input[type='text']").val(),
			             EachBottleMrp = $(this).find("td:eq(12) input[type='text']").val(),
			             marginVal = $(this).find("td:eq(13) input[type='text']").val(),
			             totalValue = $tds.eq(14).text();
	     			  
	     			     var regex = new RegExp(',', 'g');
	     			     //replace via regex
	     			     totalValue=totalValue.replace(regex, '');
			           
	     			  //date
	     		    var copyDate =changeDateFormat($('#datepicker').val());
	     		   var brandNoPackQtyList=$scope.mrpAndMarginData[i];
		             brandNoPackQtyId =brandNoPackQtyList[0];
			             
			             saleId = "0";
			             
			             var input = {
					        		"brandName":brandName,
					        		"brandNumber":brandNumber.trim(),
					        		"packQtyId":packQtyId.trim(),
					        		"date":$scope.invoiceDate.trim(),
					        		"dateAsCopy":copyDate.trim(),
					        		"caseQty":caseQty.trim(),
					        		"packQtyRate":packQtyRate.trim(),
					        		"QtyBottels":QtyBottels.trim(),
					        		"SingleBottelRate":SingleBottelRate.trim(),
					        		"EachBottleMrp":EachBottleMrp.trim(),
					        		"totalValue":totalValue.trim(),
					        		"brandNoPackQtyId":brandNoPackQtyId,
					        		"saleId":saleId.trim(),
					        		"marginVal":marginVal.trim()
					        };
			             json.invoiceDetails.push(input);
			             
			          
			            console.log(JSON.stringify(json.invoiceDetails));
			           
			         
	     		  })
	     		  
	     		// console.log(JSON.stringify(json));
	     		 searchText(json,flag);
	     	   }
	         
	      }, function myError(response) {
	          
	    	  console.log("Error:: "+JSON.stringify(response));
	    	  alert("Special Margin and MRP Not available")
	    	  
	      });
		
		$('#loader_show').modal('hide');
		
 }
					 
				 }
				
			 }
		
		}catch(e){
			alert("Something went wrong, please check the input data...");
		}
	
		
		
		
		}
		}
	}
	}
	
	
	
	function searchText(json,flag) {
		var myUrl = window.location.href;
		var url1 = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
		   $.ajax({
			  async: false,
		      type: "POST",
		      contentType : 'application/json; charset=utf-8',
		      //dataType : 'json',
		      url: url1+"/ocrSaveInvoiceJsonData/"+flag,
		      headers: { 'x-my-custom-header': ip },
		      data: JSON.stringify(json), // Note it is important
		      success: function (data,status,xhr) {
		    	  $scope.loader_show=false;
		    	 /* alert(xhr.status);*/
		    	  if(xhr.status===200){
		    		  
		    		  /*popup table with content*/
		    		  if(data[0]!=201){
		    		  $scope.reSubData=data;
		    		  $('#preSubData').modal('show');
		    		  
		    		  
		    	  }else{
		    		  
						 saveMrpRoundOff(changeDateFormat($('#datepicker').val()),$scope.invoiceDate,$scope.mrpRoundOff,$scope.turnoverTax,$scope.tcsValue, $scope.retailerCreditVal);
						 alert("Invoice Added Successfully");
		    		  
		    	  location.reload();
		    	  }
		      }else{
		    	  alert("something went wrong");
		      }
		    	  
		      },
		      error: function (data) {
		    	  $scope.loader_show=false;
		    	  alert("Something went wrong , please check input data");
		    	  //location.reload();
		    	 
		      }
		  });
		   
		   
		}
	
	function saveMrpRoundOff(date,dateAsPerCopy,mrproundoff,turnoverTax,tcsVal,retailerCreditVal){
		//alert("date>> "+date+" mrproundoff>> "+mrproundoff);
		var myUrl = window.location.href;
		var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
		var productListUrl = url+"ocrSaveMrpRoundOff?date="+date+"&dateAsPerCopy="+dateAsPerCopy+"&mrproundoff="+mrproundoff+"&turnoverTax="+turnoverTax+"&tcsVal="+tcsVal+"&retailerCreditVal="+retailerCreditVal;
		$.ajax ({
			url: productListUrl,
		       type: "POST",
		       success: function(data){
		     
		    }
		});
	}
	
	
	
	$scope.addRow=function(brandNo,brandName,productType,packType,packQty,packSize,caseDeliveredQty,bottelsDeliveredQty,unitRate,btlRate,total){
		  /* var markup="<tr><td ><input type='checkbox' name='record'></td><td  contenteditable='true'>'{{$invoice1.length+1}}'</td>"+

           " "+"<td ng-class='{'flagBg' : row.brand_No_Flag == false}' contenteditable='false' onmouseover='this.style.color' = 'Red';' onmouseout='this.style.color = 'Black';' ><input id='brandNo' type='text'  ng-value='row.brandNumber' ng-model='row.brandNumber' oninput='this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');></input></td>"+

           " "+"<td style='width:200px' ng-class='{'flagBg' : row.brand_Name_Flag == false}' contenteditable='false' onmouseover=this.style.color = 'Red'; onmouseout=this.style.color = 'Black';'><input style='width:200px' id='brandName' value='text' ng-model='row.brandName'  style='text-align: left;'  ng-value='row.brandName'  ng-change='ocrEditData($index,'brandName')'/><i ng-click='editTableRow(row.brandNumber,'brandName',$index)' class='glyphicon fas fa-edit' style='color: red;padding: 0px 0px 0px 6px;'></i></td>"+
           " "+"<td ng-class='{'flagBg' : row.prod_Type_Flag == false}' contenteditable='false' onmouseover='this.style.color = 'Red'; onmouseout=this.style.color = 'Black';' ><input id='productType' type='text' ng-model='row.productType' ng-value='row.productType' ng-change='ocrEditData($index,'productType')'/><i ng-click='editTableRow(row.brandNumber,'productType',$index)' class='glyphicon fas fa-edit' style='color: red;padding: 0px 0px 0px 6px;'></i></td>"+
           " "+"<td ng-class='{'flagBg' : row.pack_Type_Flag == false}' contenteditable='false' onmouseover='this.style.color = 'Red';' onmouseout='this.style.color = 'Black';' ><input id='packType' type='text' ng-model='row.packType' ng-value='row.packType'  ng-change='ocrEditData($index,'packType')' /><i ng-click='editTableRow(row.brandNumber,'packType',$index)' class='glyphicon fas fa-edit' style='color: red;padding: 0px 0px 0px 6px;'></i></td>"+
           " "+"<td ng-class='{'flagBg' : row.pack_Qty_Flag == false}' contenteditable='false' onmouseover='this.style.color = 'Red';' onmouseout='this.style.color = 'Black';' ><input id='packQty' type='text' ng-model='row.packQty' ng-value='row.packQty' ng-change='ocrEditData($index,'packQty')'' /><i ng-click='editPackQty(row.brandNumber,'packQty',$index)' class='glyphicon fas fa-edit' style='color: red;padding: 0px 0px 0px 6px;''></i></td>"+
           " "+"<td ng-class='{'flagBg' : row.pack_Size_Flag == false}' contenteditable='false' onmouseover='this.style.color = 'Red';' onmouseout='this.style.color = 'Black';'><input id='packSize' type='text'  ng-model='row.packSize' ng-value='row.packSize' ng-change='ocrEditData($index,'packSize')' /><i ng-click='editPackQty(row.brandNumber,'packSize',$index)' class='glyphicon fas fa-edit' style='color: red;padding: 0px 0px 0px 6px;'></i></td>"+
           " "+"<td  contenteditable='false' onmouseover='this.style.color = 'Red';' onmouseout='this.style.color = 'Black';' ><input id='caseDeliveredQty' type='text'  ng-value='row.caseDeliveredQty' ng-model='row.caseDeliveredQty' oninput='this.value' = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');'></input></td>"+
           " "+"<td  contenteditable='false' onmouseover='this.style.color = 'Red';' onmouseout='this.style.color = 'Black';' ><input id='bottelsDeliveredQty' type='text'  ng-value='row.bottelsDeliveredQty' ng-model='row.bottelsDeliveredQty' oninput='this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');'></input></td>"+
           " "+"<td  contenteditable='false' onmouseover='this.style.color = 'Red';' onmouseout='this.style.color = 'Black';' ><input id='unitRate' type='text'  ng-value='row.unitRate' ng-model='row.unitRate' oninput='this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');'></input></td>"+
           " "+"<td  contenteditable='false' onmouseover='this.style.color = 'Red';' onmouseout='this.style.color = 'Black';'><input id='btlRate' type='text'  ng-value='row.btlRate' ng-model='row.btlRate' oninput='this.value '= this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');'></input></td>"+
           " "+"<td  contenteditable='false' onmouseover='this.style.color = 'Red';' onmouseout='this.style.color = 'Black'; ng-model='total' pattern='^\d*(\.\d{0,2})?$' >{{((row.caseDeliveredQty*row.unitRate) + (row.bottelsDeliveredQty*row.btlRate)) | number:2}}</td></tr>";

           $("#invoice-table").append(markup);*/
		packSize=packSize.replace('ml','').trim();
           //create JSON Empty object
           var addRowObj={"brandNumber":brandNo,"brandName":brandName,"productType":productType,"packType":packType,"packQty":packQty,"packSize":packSize,"caseDeliveredQty":caseDeliveredQty,"bottelsDeliveredQty":bottelsDeliveredQty,"unitRate":unitRate,"btlRate":btlRate,"total":total,"brand_Name_Flag":true,"caseDelv_Flag":true,"btlDelv_Flag":true,"unitRate_Flag":true,"btlRate_Flag":true,"total_Flag":true,"prod_Type_Flag":true,"pack_Size_Flag":true,"pack_Type_Flag":true,"brand_No_Flag":true,"pack_Qty_Flag":true,"$$hashKey":""};
           $scope.invoice1.push(addRowObj);
           $scope.alertMsg="Row Added Successfully...";
	    	  $('#editOCRRowSugg').modal('show');
	}
	
	$scope.removeRow=function(){
		
	
		$scope.alertMsg="Row deleted Successfully...";
		try {
			var table = document.getElementById("invoice-table");
			var rowCount = table.rows.length;
			var delCnt=0;
			
			for(var i=0; i<rowCount; i++) {
				var row = table.rows[i+1];
				var chkbox = row.cells[0].childNodes[0];
				if(null != chkbox && true == chkbox.checked) {
					table.deleteRow(i+1);
					 $scope.invoice1.splice(i,1);
					rowCount--;
					i--;
					
					if(delCnt>0){
						 $scope.alertMsg="Rows deleted Successfully...";
					}
					delCnt=delCnt+1;
				}
			}
			
			}catch(e) {
				
		    	  $('#editOCRRowSugg').modal('show');
		    	  
			}
			
			
			//Update total value
			 $scope.salesValueCalc();
		
		
	}
	

  $scope.closeDialog=function(event){
	  $('#editOCRRowSugg').modal('hide');
  }
  


$scope.totalCalc=function(index){
	
/*	{{((row.caseDeliveredQty*row.unitRate) + (row.bottelsDeliveredQty*row.btlRate)) | number:2}}*/
	var unitRate=$scope.invoice1[index].unitRate;
	var caseDeliveredQty=$scope.invoice1[index].caseDeliveredQty;
	var bottelsDeliveredQty=$scope.invoice1[index].bottelsDeliveredQty;
	var btlRate=$scope.invoice1[index].btlRate;
      $scope.rowTotal=($scope.invoice1[index].caseDeliveredQty*$scope.invoice1[index].unitRate) + ($scope.invoice1[index].bottelsDeliveredQty*$scope.invoice1[index].btlRate);
      $scope.invoice1[index].total=$scope.rowTotal;
    
    $scope.salesValueCalc();
}

$scope.ocrAddNewProduct=function(){
	 $('#ocrAddNewProduct').modal('show');
}
$scope.ocrAddNewBrandName=function(){
	var brandNo=$('#ocrBrandNumber').val();
	var brandName=$('#ocrBrandName').val();
	var productType=$('#ocrProductType').val();
	var quantity=$('#ocrQuantity').val();
	var packQty=$('#ocrPackQty').val();
	var packType=$('#ocrPackType').val();
	var category=$('#ocrCategory').val();
	var company=$('#ocrCompany').val();
	var group=$('#ocrGroup').val();
	var shortBrandName=$('#ocrShortBrandName').val();
	var match=$('#ocrMatch').val();
	var matchName=$('#ocrMatchName').val();
	var realBrandNo=$('#ocrRealBrandNo').val();
	var addNewBrandField={"brandNo":brandNo,"brandName":brandName,"productType":productType,"quantity":quantity,"packQty":packQty,"packType":packType,"category":category,"company":company,"group":group,"shortBrandName":shortBrandName,"match":match,"matchName":matchName,"realBrandNo":realBrandNo};
	console.log(JSON.stringify(addNewBrandField));
	
	console.log(JSON.stringify(addNewBrandField)); 
		var myUrl = window.location.href;
		var url1 = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
		   $.ajax({
			  async:false,
		      type: "POST",
		      contentType : 'application/json; charset=utf-8',
		      url: url1+"/ocrAddNewBrandName",
		      data: JSON.stringify(addNewBrandField), 
		      success: function (data) {
		    	 
		    	 alert(data); 
		    	
		    	
		    	
		    	 
		    	 //clear the GUI data
		    	 $('#ocrBrandNumber').attr("value", ""); 
		    	 $('#ocrBrandName').val('');
		    	 $('#ocrProductType').val('');
		    	 $('#ocrQuantity').val('');
		    	 $('#ocrPackQty').val('');
		    	 $('#ocrPackType').val('');
		    	 $('#ocrCategory').val('');
		    	 $('#ocrCompany').val('');
		    	 $('#ocrGroup').val('');
		    	 $('#ocrShortBrandName').val('');
		    	
		    	 
		    	 $scope.brandNo='';
		    	 $scope.shortBrandName='';
		    	 
		    	 /*Logic to append row to table*/
              if((data).includes("Already Existed")){
		    		  
		    	  }else{
		    		  $scope.addRow(brandNo,brandName,productType,packType,packQty,quantity,0,0,0,0,0);
		    	  }
		    	  
		    	 
		    	 
		    	 
		    	 $('#editOCRRowSugg').modal('hide');
		    	 
		    	
		    	 
		      },
		      error: function (data) {
		    	  alert("Error: "+data);
		    	 
		      }
		  });
	 
	 
	 
}

$scope.ocrInvoicePdfViewer=function(){
	
	var path= $('#bannerImage')[0].files[0].name;
	alert(path);
	window.open(path);
}



	});

