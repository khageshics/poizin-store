var app=angular.module('myApp',[]);

app.controller('myCtrl',function($scope,$http){
	
	
	var myUrl = window.location.href;
    var url = myUrl.substring(0, myUrl.lastIndexOf('/'));
	console.log("&&&&&&&&&&&&&&&&&&&&7"+url);
	var productListUrl =  url+"/getProfileType";
	$http({
	    method : "GET",
	    url : productListUrl
	  }).then(function mySuccess(response) {
		  $scope.profileType = response.data;
		  console.log(JSON.stringify(response.data));
	  }, function myError(response) {
		  console.log(JSON.stringify(response.data));
	  });
	
	
	
	
	
	$scope.createNewStore=function(){
		
		var storeName=$scope.storeName;
		var profileType=$scope.profile;
		var license=$scope.licenseNo;
		var entrolDate=$scope.edate;
		var primaryPhn=$scope.primaryMob;
		var alterPhn=$scope.altrMob;
		var primaryEmail=$scope.primaryEmail;
		var alterEmail= $scope.altrEmail;
		
		var state=$scope.state;
		var district=$scope.district;
		var city= $scope.city;
		var portalAndSteet=$scope.portalStreet;
		
		
		var profileData={
				storeName : storeName,
				profileType : profileType,
				license : license,
				entrolmentDate : entrolDate,
				phonePrimary : primaryPhn,
				phoneSecondary : alterPhn,
				emailPrimary : primaryEmail,
				emailSecondary : alterEmail
			
		}
		
		var address={
				streetAddress : portalAndSteet,
				cityName : city,
				district : district,
				state :state
				
		}
		
		
		   $.ajax({
			  async: false,
		      type: "POST",
		      contentType : 'application/json; charset=utf-8',
		      //dataType : 'json',
		      url: url+"/createStore",
		      headers: {
	                'Content-Type': 'application/json; charset=utf-8'
	            },
		      data: JSON.stringify({storeProfile:profileData, storeAddress:address}), // Note it is important
		      success: function (data,status,xhr) {
		    	
		    	  alert(data);
		    	  
		      },
		      error: function (data) {
		    	
		    	  alert("Something went wrong , please check input data");
		    	  //location.reload();
		    	 
		      }
		  });
	}
	
});
