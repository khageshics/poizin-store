var app = angular.module('myApp', []);

app.controller('myCtrl', function($scope, $http) {
	var myUrl = window.location.href;
	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
	var saleDatesUrl = url+"getTotalSaleDate.json";
	$.ajax ({
		url: saleDatesUrl,
	       type: "GET",
	       success: function(data){
	      var startDate="";
	      var enableDays =[];
	      $.each(data, function (index, value) {
	    	  enableDays.push(data[index].date);
	    	  startDate=data[index].date;
	      });
	      getTimeStamp(enableDays);
	      buildBalanceSheet(startDate,startDate);
	      $("#startDate").val(startDate);
	  	  $("#endDate").val(startDate);
	    }
	});
	function getTimeStamp(enableDays){
		$("#startDate").datepicker({
			  dateFormat: 'dd-mm-yy',  beforeShowDay: enableAllTheseDays,
			  numberOfMonths: 1,
			  onSelect: function(selected) {
			  $("#endDate").datepicker("option","minDate", selected);
			  }
			  });
		    function enableAllTheseDays(date) {
		        var sdate = $.datepicker.formatDate( 'dd-mm-yy', date)
		        if($.inArray(sdate, enableDays) != -1) {
		            return [true];
		        }
		        return [false];
		    }
		    $('#endDate').datepicker({dateFormat: 'dd-mm-yy', beforeShowDay: enableAllTheseDays});
	}
	function buildBalanceSheet(startDate, endDate) {
		$("#wait").css("display", "block");
		var callingUrl = url + "getBalanceSheet.json?startDate=" + startDate + "&endDate=" + endDate;
		$http({
			method : "GET",
			url : callingUrl
		}).then(function mySuccess(response) {
			// console.log(JSON.stringify(response.data));
			var inputjson = [];
			var totalDiffAmount=0,totalRetentionAmount = 0;
			$.each(response.data.balanceSheetBean, function(index, value){
				var showShortOrExcess=0;
				var totalAmt = value.totalAmount + value.cradSale + value.cashSale + value.chequeSale;
				var diff = value.totalPrice - totalAmt;
				if(diff <= 0){
					showShortOrExcess = totalAmt- value.totalPrice;// plus
				}else{
					showShortOrExcess = diff;
				}
				var input = {
						"date" : changeDateFormatSec(value.date),
						"saleAmount" : value.totalPrice,
						"expenseAmount" : value.totalAmount,
						"cardAmount" : value.cradSale,
						"cashAmount" : value.cashSale,
						"chequeAmount" : value.chequeSale,
						"totalAmount" : totalAmt,
						"diffAmount" : diff,
						"showShortOrExcess" : showShortOrExcess,
						"expenseMasterId" : value.expenseMasterId,
						"bankCreditedAmount" : value.creditedAmount,
						"retention" : value.retention,
						"investment" : value.investment
				};
				totalDiffAmount += diff;
				totalRetentionAmount += value.retention;
				inputjson.push(input);
			});
			//console.log(JSON.stringify(inputjson));
			$scope.balanceSheetList = inputjson;
			$scope.totalDiffAmount = totalDiffAmount-(response.data.data.value);
			$scope.totalRetentionAmount = totalRetentionAmount;
			$("#wait").css("display", "none");
		}, function myError(response) {
			// alert(response);
			$("#wait").css("display", "none");
		});

	}
	$scope.getResults = function (startDate,endDate){
		var startDate = $("#startDate").val();
		var endDate = $("#endDate").val();
		 if(validateCalendarField()==false){return false;}
		 buildBalanceSheet(startDate,endDate);
	}
	function validateCalendarField(){
		var startDate = $("#startDate").val();
		var endDate = $("#endDate").val();
		if(startDate == ""){
			alert("Please fill Start Date.");
			return false;
		}
		if(endDate == ""){
			alert("Please fill End Date.");
			return false;
		}
		return true;
	}
$scope.getExpenseData = function (expenseMasterID,idate){
		var expenseListUrl = url+"getExpenseDetails.json?expenseMasterID="+expenseMasterID;
		$http({
			method : "GET",
			url : expenseListUrl
		}).then(function mySuccess(response) {
		    	   $scope.excepseDetails = response.data;
		    	   $scope.expenseDate = idate;
		    	   var totalAmount=0;
		    	   $.each(response.data,function(index, value){
		    		   totalAmount=totalAmount+value.expenseChildAmount;
		    	   });
		    	   $scope.totalExpenseAmount = totalAmount;
		    	  // console.log(JSON.stringify($scope.excepseDetails));
		}, function myError(response) {
			// alert(response);
		});
		
	}
$scope.getInvestmentData = function(date){
	var investmentListUrl = url+"getInvestmentData.json?date="+date;
	$.ajax ({
		url: investmentListUrl,
	       type: "GET",
	       success: function(data){
	    	   //console.log(JSON.stringify(data));
	    	   var totalAmount=0;
	    	   var investmentList="<table id=\"customers\"><tr><th>Amount</th><th>Tr. Type</th><th>Bank</th></tr>";
	    	   $.each(data,function(index, value){
	    		   totalAmount+=value.amount;
	    		   investmentList=investmentList+"<tr><td>"+(value.amount).toLocaleString('en-IN',{ maximumFractionDigits: 2})+"</td><td>"+value.transactionType+"</td><td>"+value.bank+"</td></tr>";
	    		   
	    	   });
	    	   investmentList=investmentList+"<tr style=\"border-top: solid 1px #ccc;\"><td></td><td></td><td style=\"float:right;color:red;\">Total : "+totalAmount.toLocaleString('en-IN',{ maximumFractionDigits: 2})+"</td></tr></table>";
	    	   $("#investmentList").html(investmentList);
	    	   $("#investdate").text("Selected Date: "+date);
	    }
	});
	
}
});
app.filter("totalCardAmount", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(balanceSheetList, i) {
            s += balanceSheetList.cardAmount;
        });
        return Math.ceil(s);
    };
});
app.filter("totalCashAmount", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(balanceSheetList, i) {
            s += balanceSheetList.cashAmount;
        });
        return Math.ceil(s);
    };
});
app.filter("totalSaleAmount", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(balanceSheetList, i) {
            s += balanceSheetList.saleAmount;
        });
        return Math.ceil(s);
    };
});
app.filter("totalSaleAmount", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(balanceSheetList, i) {
            s += balanceSheetList.saleAmount;
        });
        return Math.ceil(s);
    };
});
app.filter("totalExpenseAmount", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(balanceSheetList, i) {
            s += balanceSheetList.expenseAmount;
        });
        return Math.ceil(s);
    };
});
/*app.filter("totalRetentionAmount", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(balanceSheetList, i) {
            s += balanceSheetList.retention;
        });
        return Math.abs(Math.ceil(s));
    };
});*/
app.filter("totalSortOrPlusAmount", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(balanceSheetList, i) {
            s += balanceSheetList.diffAmount;
        });
        return Math.ceil(s);
    };
});
app.filter('abs', function () {
	  return function(val) {
	    return Math.abs(val);
	  }
	});
app.filter('INR', function () {        
    return function (input) {
        if (! isNaN(input)) {
           // var currencySymbol = '₹';
        	 var currencySymbol = '';
            //var output = Number(input).toLocaleString('en-IN');   <-- This method is not working fine in all browsers!           
            var result = input.toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
            
            if (result.length > 1) {
                output += "." + result[1];
            }            

            return currencySymbol + output;
        }
    }
});
