var myApp = angular.module('myApp', []);
myApp.controller('myCtrl', function($scope, $http) {
	 $('.newMonthlyExpensesForm').hide();
	 var msg = $("#popupmsg").val();
		if(msg != ""){
			alert(msg);
		}
		 $('#popupmsg').val("");
	    $( "#transactionDate" ).datepicker({dateFormat: 'dd-mm-yy'});
        $('input.monthpicker').monthpicker({changeYear:true,dateFormat: "MM-yy" });
	  
        var myUrl = window.location.href;
    	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
       $scope.init = function() {
    	   $scope.getCompanies();
    	   $scope.getPendingCheck();
        }; 	
	 $scope.getCompanies = function(){
		 var productListUrl = url+"getCompanyList.json";
	 	$http({
	 	    method : "GET",
	 	    url : productListUrl
	 	  }).then(function mySuccess(response) {
	 		  $.each(response.data, function (index, value) {
	 			  $('#listdataSec').append( '<option  value="'+value.id+'">'+value.name+'</option>' );
	 		     });
	 	  }, function myError(response) {
	 	    //alert(response);
	 	  });
	 }
	// $scope.receivedupdate = true;
	 $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
		   
		 $( ".trdatetoupdate" ).datepicker({dateFormat: 'dd-mm-yy'});
		 
		});	
   $scope.getPendingCheck = function(){
	   var checkUrl = url+"getAllPendingChecks";
   	$http({
   	    method : "GET",
   	    url : checkUrl
   	  }).then(function mySuccess(response) {
   		  $scope.pendingCheckDetails = response.data;
   		  //console.log(JSON.stringify(response.data));
   	  }, function myError(response) {
   	    //alert(response);
   	  });
   } 	
    	
    	$(".fetchExistingExpenses").click(function(){
    		 var date =	changeDateFormateForMonth($("#monthlyDate").val());
    		 var company = $scope.companyId;
    	   if(date!="" && company!="" && company != undefined){
    		   $('.newMonthlyExpensesForm').show();
    		   
    		    var myUrl = window.location.href;
    	    	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
    	    	var transactionUrl = url+"getDiscountTransactionDetails?date="+date+"&company="+company;
    	    	$http({
    	    	    method : "GET",
    	    	    url : transactionUrl
    	    	  }).then(function mySuccess(response) {
    	    		  //console.log(JSON.stringify(response.data));
    	    		  $scope.transactionData = response.data;
    	    		  $scope.monthDate = date;
    	    	  }, function myError(response) {
    	    	  });
    			   
    	   }else{
    		   alert("Please Select Month and Company");
    	   }
    	 });	
    	
    	$scope.openListDisconts = function(indexNo,id,companyId,month,image) {
    		if (confirm("Are you sure you want to delete ?"))
    		  {
	    		var myUrl = window.location.href;
		    	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
		    	var transactionUrl = url+"deleteDiscountTransactionDetails?id="+id+"&companyId="+companyId+"&month="+month+"&image="+image;
		    	$http({
		    	    method : "GET",
		    	    url : transactionUrl
		    	  }).then(function mySuccess(response) {
		    		  
		    	  }, function myError(response) {
		    	  });
		    	$('#delete'+indexNo).remove();
    		  }
      };
      
$scope.reveivedchecked = true;
$scope.uploadReceivedCheckWithDetails = function(){
	
	var company = $("#company").val();
	var months = $("#months").val();
	var bank = $("#bank option:selected").val();
	var TransactionType = $("#TransactionType option:selected").val();
	var transactionAmt = parseFloat($("#transactionAmt").val());
	var received = $('#received').prop('checked');
	var transactionDate = changeDateFormat($("#transactionDate").val());
	var adjCheck = parseFloat($("#adjCheck").val());
	var checkNo = $("#checkNo").val();
	var fromBank = $("#fromBank").val();
	var comment = $.trim($("#comment").val());
	
	if(!received && (transactionDate == "" || transactionDate == undefined)){
		alert("Please select transaction date.");
		return false;
	}
	
	var formData = new FormData();
	formData.append('company', company);
	formData.append('months', months);
	formData.append('bank', bank);
	formData.append('TransactionType', TransactionType);
	formData.append('transactionAmt', transactionAmt);
	formData.append('received', received);
	formData.append('transactionDate', transactionDate);
	formData.append('adjCheck', adjCheck);
	formData.append('checkNo', checkNo);
	formData.append('fromBank', fromBank);
	formData.append('comment', comment);
	formData.append('ngalleryImage', $( '#ngalleryImage' )[0].files[0]);
	
	$scope.postCheckWithDetails(formData);

}
$scope.postCheckWithDetails = function (formData) {
	var addNewTransaction = url+"addNewTransaction";
	$.ajax({
		async : false,
        url: addNewTransaction,
        data: formData,
        contentType: false,
        processData: false,
        type: 'POST',
        success: function(data){
           alert(data);
           location.reload();
        }
    });    
}
$scope.updateReceived = function(indexnum, trid, companyid,month){
	var received = $("#receivedupdate"+indexnum).prop('checked');
	var transactionDate = changeDateFormat($("#trdate"+indexnum).val());
	if(!received && transactionDate !="" && transactionDate != undefined){
		var formData = new FormData();
		formData.append('trid', trid);
		formData.append('companyid', companyid);
		formData.append('month', month);
		formData.append('transactionDate', transactionDate);
		formData.append('clearedBank', $("#clearedBank"+indexnum).val());
		var updateReceivedCheckStatus = url+"updateReceivedCheckStatus";
		$.ajax({
			async : false,
	        url: updateReceivedCheckStatus,
	        data: formData,
	        contentType: false,
	        processData: false,
	        type: 'POST',
	        success: function(data){
	        $scope.getPendingCheck();
	           alert(data);
	        }
	    });
	}else{
		alert("Please select date and unchecked check box.");
	}
	
}
});
myApp.directive('onFinishRender', function ($timeout) {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			if (scope.$last === true) {
				$timeout(function () {
					scope.$emit('ngRepeatFinished');
				});
			}
		}
	}
});