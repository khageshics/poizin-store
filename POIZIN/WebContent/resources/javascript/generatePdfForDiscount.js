var myApp = angular.module('myApp', ["checklist-model"]);
myApp.controller('Ctrl1', function($scope, $http) {
	$('#downloadpdfId').css("display", "none");
	$('input.monthpicker').monthpicker({changeYear:true,dateFormat: "MM-yy" });
	var myUrl = window.location.href;
	var url = myUrl.substring(0, myUrl.lastIndexOf('/') + 1);
	var productListUrl = url+"getCompanyList.json";
	$http({
	    method : "GET",
	    url : productListUrl
	  }).then(function mySuccess(response) {
		 // console.log(JSON.stringify(response.data));
		 $scope.companyData = response.data;
		  $.each(response.data, function (index, value) {
    	      $('#listdataSec').append( '<option  value="'+value.id+'">'+value.name+'</option>' );
	  });
	  }, function myError(response) {
	    //alert(response);
	  });
	function formatDate(date) {
		  var monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

		  var day = date.getDate();
		  var monthIndex = date.getMonth();
		  var year = date.getFullYear();

		  return monthNames[monthIndex] + '-' + year;
		}
	$scope.buildDiscountWithPdf = function (){
		$scope.customStartMonthdisplay = $("#startMonth").val();
		 $scope.customEndMonthdisplay =  $("#endMonth").val();
		 
		var startMonth = changeDateFormateForMonth($("#startMonth").val());
		var endMonth =	changeDateFormateForMonth($("#endMonth").val());
		 var company = $scope.companyId;
		 if(startMonth !="" && endMonth !="" && company !="" && company != undefined){
			 /*$scope.customStartMonth = formatDate(new Date(startMonth));
			 $scope.customEndMonth = formatDate(new Date(endMonth));*/
			 $scope.customStartMonth = startMonth;
			 $scope.customEndMonth =  endMonth;
			 
			 var discountdetailurl = url+"mobileView/buildDiscount?startMonth="+startMonth+"&endMonth="+endMonth+"&company="+company
			 $http({
	 	    	    method : "GET",
	 	    	    url : discountdetailurl
	 	    	  }).then(function mySuccess(response) {
	 	    		//console.log(JSON.stringify(response.data));
	 	    		 $scope.transactionData = response.data.transactiontransactionData;
	 	    		$scope.receivedData = response.data.receivedData;
	 	    		$scope.rentalAmt = response.data.rental;
	 	    		$scope.previousArrears = response.data.previousArrears;
	 	    		$scope.arrearsdetails = response.data.arrearsdetails;
	 	    		$scope.totalChequeAmount = response.data.totalChequeAmount;
	 	    		$scope.adjCheque = response.data.adjCheque;
	 	    		$scope.totalVendorDiscount = response.data.totalVendorDiscount;
	 	    		$scope.totalDiscountAmt = response.data.totalDiscountAmt;
	 	    		$scope.discountPlusRental = response.data.totalDiscountAmt + response.data.rental;
	 	    		$scope.totalVendorCredit = response.data.creditMasterBean.totalAmount;
	 	    		$scope.creditChildBeanList = response.data.creditChildBeanList;
	 	    		$scope.totalVendorCreditReceived = response.data.creditMasterBean.received;
	 	    	  }, function myError(response) {
	 	    	  });
			 
			 var companyUrl = url+"mobileView/getCompanyWithEnterPrice?company="+company;
		    	$http({
		    	    method : "GET",
		    	    url : companyUrl
		    	  }).then(function mySuccess(response) {
		    		  $scope.companyName = response.data.label;
		    		  $scope.firstDate = startMonth;
		    		  $scope.secondDate = endMonth;
		    		  $scope.enterprice = "  ("+response.data.color+")";
		    		  
		    	  }, function myError(response) {
		    	  });
		    	 $('#downloadpdfId').css("display", "block");
			 
		 }else{
			 alert("Please Select Company.")
		 }
	
	}
	
	$scope.greaterThan = function(prop, val){
	    return function(item){
	      return item[prop] > val;
	    }
	}
	});
myApp.filter('INR', function () {        
    return function (input) {
        if (! isNaN(input)) {
           // var currencySymbol = '₹';
        	 var currencySymbol = '';
            //var output = Number(input).toLocaleString('en-IN');   <-- This method is not working fine in all browsers!           
            var result = input.toString().split('.');

            var lastThree = result[0].substring(result[0].length - 3);
            var otherNumbers = result[0].substring(0, result[0].length - 3);
            if (otherNumbers != '')
                lastThree = ',' + lastThree;
            var output = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
            
            if (result.length > 1) {
                output += "." + result[1];
            }            

            return currencySymbol + output;
        }
    }
});
/*myApp.filter("totalVendorDiscount", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(arrearsdetails, i) {
             s += arrearsdetails.vendorAmt;
        });
        return Math.ceil(s);
    };
})
myApp.filter("totalChequeAmount", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(receivedData, i) {
             s += receivedData.chequeAmount;
        });
        return Math.ceil(s);
    };
})
myApp.filter("adjCheque", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(receivedData, i) {
             s += receivedData.adjustmentCheque;
        });
        return Math.ceil(s);
    };
})
myApp.filter("totalDiscountAmt", function() {
    return function(data, index) {
        var s = 0;
       // if(!data || !data[0]) return soma;
        
        angular.forEach(data, function(transactionData, i) {
             s += transactionData.totalDiscount;
        });
        return Math.ceil(s);
    };
})*/
