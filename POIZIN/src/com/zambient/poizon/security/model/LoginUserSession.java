/*
 * package com.zambient.poizon.security.model;
 * 
 * import org.springframework.context.annotation.Primary; import
 * org.springframework.context.annotation.ScopedProxyMode; import
 * org.springframework.context.annotation.Scope; import
 * org.springframework.stereotype.Component; import java.io.Serializable;
 * 
 * @Component()
 * 
 * @Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
 * 
 * @Primary public class LoginUserSession implements Serializable { private
 * String UserId; private int loginProfileId; private int storeId; private Long
 * loginId;
 * 
 * public String getUserId() { return this.UserId; }
 * 
 * public void setUserId(final String userId) { this.UserId = userId; }
 * 
 * public int getLoginProfileId() { return this.loginProfileId; }
 * 
 * public void setLoginProfileId(final int loginProfileId) { this.loginProfileId
 * = loginProfileId; }
 * 
 * public int getStoreId() { return this.storeId; }
 * 
 * public void setStoreId(final int storeId) { this.storeId = storeId; }
 * 
 * public Long getLoginId() { return this.loginId; }
 * 
 * public void setLoginId(final Long loginId) { this.loginId = loginId; } }
 */