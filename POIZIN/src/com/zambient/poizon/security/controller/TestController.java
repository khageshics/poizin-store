package com.zambient.poizon.security.controller;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.zambient.poizon.security.entity.LoginCredentials;
import com.zambient.poizon.security.dao.UserAuthDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Controller;

@Controller
public class TestController
{
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    UserAuthDaoImpl userAuthDao;
    
    @RequestMapping({ "/store" })
    @ResponseBody
    public LoginCredentials healthCheck() {
        final LoginCredentials user = this.userAuthDao.findByUserName("poizin1@gmail.com");
        System.out.println(user.getLoginProfile().getRole());
        return user;
    }
}