package com.zambient.poizon.security.controller;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;

@Controller
public class AuthController
{
    @RequestMapping({ "/profileLogin" })
    @ResponseBody
    public String profileLoginProcess(final HttpServletRequest request, final RedirectAttributes redirectAttributes, @RequestParam("userName") final String userName, @RequestParam("password") final String password) {
        try {
            if (!(userName != null & password != null)) {
                return "redirect:/admin";
            }
            System.out.println(String.valueOf(userName) + " " + password);
        }
        catch (Exception exc) {
            exc.printStackTrace();
        }
        return "redirect:/admin";
    }
}