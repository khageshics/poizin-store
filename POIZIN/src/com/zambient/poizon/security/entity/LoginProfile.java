package com.zambient.poizon.security.entity;

import java.util.Date;
import javax.persistence.JoinColumn;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import java.io.Serializable;

@Component
@Entity
@Table(name = "login_profile")
public class LoginProfile implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "login_profile_id")
    private int loginProfileId;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_id", nullable = false)
    private StoreProfile storeProfile;
    @Column(name = "username")
    private String userName;
    @Column(name = "password_change_ind")
    private char passChangeInd;
    @Column(name = "portal_type")
    private String role;
    @Column(name = "security_role_ids")
    private String securityRoleIds;
    @Column(name = "is_locked")
    private char isLocked;
    @Column(name = "failed_login_attemts")
    private int failedLoginAttemps;
    
    @Column(name = "last_login_ts")
    private Date lastLoginTs;
    
    @Column(name = "last_failed_attempt_ts")
    private Date lastFailedAttempt;
    
    @Column(name = "created_ts")
    private Date createdTs;
    
    @Column(name = "updated_ts")
    private Date updatedTs;
    
    @Column(name = "is_active")
    private char isActive;
    
    public int getLoginProfileId() {
        return this.loginProfileId;
    }
    
    public void setLoginProfileId(final int loginProfileId) {
        this.loginProfileId = loginProfileId;
    }
    
    public StoreProfile getStoreProfile() {
        return this.storeProfile;
    }
    
    public void setStoreProfile(final StoreProfile storeProfile) {
        this.storeProfile = storeProfile;
    }
    
    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(final String userName) {
        this.userName = userName;
    }
    
    public char getPassChangeInd() {
        return this.passChangeInd;
    }
    
    public void setPassChangeInd(final char passChangeInd) {
        this.passChangeInd = passChangeInd;
    }
    
    public String getRole() {
        return this.role;
    }
    
    public void setRole(final String role) {
        this.role = role;
    }
    
    public String getSecurityRoleIds() {
        return this.securityRoleIds;
    }
    
    public void setSecurityRoleIds(final String securityRoleIds) {
        this.securityRoleIds = securityRoleIds;
    }
    
    public char getIsLocked() {
        return this.isLocked;
    }
    
    public void setIsLocked(final char isLocked) {
        this.isLocked = isLocked;
    }
    
    public int getFailedLoginAttemps() {
        return this.failedLoginAttemps;
    }
    
    public void setFailedLoginAttemps(final int failedLoginAttemps) {
        this.failedLoginAttemps = failedLoginAttemps;
    }
    
    public Date getLastLoginTs() {
        return this.lastLoginTs;
    }
    
    public void setLastLoginTs(final Date lastLoginTs) {
        this.lastLoginTs = lastLoginTs;
    }
    
    public Date getLastFailedAttempt() {
        return this.lastFailedAttempt;
    }
    
    public void setLastFailedAttempt(final Date lastFailedAttempt) {
        this.lastFailedAttempt = lastFailedAttempt;
    }
    
    public Date getCreatedTs() {
        return this.createdTs;
    }
    
    public void setCreatedTs(final Date createdTs) {
        this.createdTs = createdTs;
    }
    
    public Date getUpdatedTs() {
        return this.updatedTs;
    }
    
    public void setUpdatedTs(final Date updatedTs) {
        this.updatedTs = updatedTs;
    }
    
    public char getIsActive() {
        return this.isActive;
    }
    
    public void setIsActive(final char isActive) {
        this.isActive = isActive;
    }
}