package com.zambient.poizon.security.entity;

import java.util.Date;
import javax.persistence.JoinColumn;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import java.io.Serializable;

@Component
@Entity
@Table(name = "login_credentials")
public class LoginCredentials implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_profile_id")
    private int userProfileId;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "login_profile_id")
    private LoginProfile loginProfile;
    
    @Column(name = "user_name")
    private String userName;
    @Column(name = "password")
    private String password;
    @Column(name = "enabled")
    private boolean enabled;
    @Column(name = "created_ts")
    private Date createdTs;
    @Column(name = "updated_ts")
    private Date updatedTs;
    
    public int getUserProfileId() {
        return this.userProfileId;
    }
    
    public void setUserProfileId(final int userProfileId) {
        this.userProfileId = userProfileId;
    }
    
    public LoginProfile getLoginProfile() {
        return this.loginProfile;
    }
    
    public void setLoginProfile(final LoginProfile loginProfile) {
        this.loginProfile = loginProfile;
    }
    
    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(final String userName) {
        this.userName = userName;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(final String password) {
        this.password = password;
    }
    
    public boolean isEnabled() {
        return this.enabled;
    }
    
    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }
    
    public Date getCreatedTs() {
        return this.createdTs;
    }
    
    public void setCreatedTs(final Date createdTs) {
        this.createdTs = createdTs;
    }
    
    public Date getUpdatedTs() {
        return this.updatedTs;
    }
    
    public void setUpdatedTs(final Date updatedTs) {
        this.updatedTs = updatedTs;
    }
}