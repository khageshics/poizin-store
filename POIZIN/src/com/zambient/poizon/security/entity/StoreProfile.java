package com.zambient.poizon.security.entity;

import javax.persistence.TemporalType;
import javax.persistence.Temporal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Entity;
import java.io.Serializable;





@Entity
@Table(name = "store_profile")
public class StoreProfile implements Serializable
{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "store_id", nullable = false)
    private int storeId;
    @Column(name = "store_name")
    private String storeName;
    @Column(name = "profile_type")
    private String profileType;
    @Column(name = "license")
    private String license;
    @Column(name = "entrolment_date")
    private String entrolmentDate;
    @Column(name = "current_academic_year")
    private Date currentAcademicYear;
    @Column(name = "phone_number1")
    private String phonePrimary;
    @Column(name = "phone_number2")
    private String phoneSecondary;
    
    @Column(name = "email_addr1")
    private String emailPrimary;
    
    @Column(name = "email_addr2")
    private String emailSecondary;
    @Column(name = "is_active")
    private String isActive;
    
    @Column(name = "created_ts")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creadtedTs;
    @Column(name = "updated_ts")
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedTs;
    @Column(name = "created_by")
    private String createdBy;
    
    public int getStoreId() {
        return this.storeId;
    }
    
    public void setStoreId(final int storeId) {
        this.storeId = storeId;
    }
    
    public String getStoreName() {
        return this.storeName;
    }
    
    public void setStoreName(final String storeName) {
        this.storeName = storeName;
    }
    
    public String getProfileType() {
        return this.profileType;
    }
    
    public void setProfileType(final String profileType) {
        this.profileType = profileType;
    }
    
    public String getLicense() {
        return this.license;
    }
    
    public void setLicense(final String license) {
        this.license = license;
    }
    
    public String getEntrolmentDate() {
        return this.entrolmentDate;
    }
    
    public void setEntrolmentDate(final String entrolmentDate) {
        this.entrolmentDate = entrolmentDate;
    }
    
  
    
    public Date getCurrentAcademicYear() {
		return currentAcademicYear;
	}

	public void setCurrentAcademicYear(Date currentAcademicYear) {
		this.currentAcademicYear = currentAcademicYear;
	}

	public String getPhonePrimary() {
        return this.phonePrimary;
    }
    
    public void setPhonePrimary(final String phonePrimary) {
        this.phonePrimary = phonePrimary;
    }
    
    public String getPhoneSecondary() {
        return this.phoneSecondary;
    }
    
    public void setPhoneSecondary(final String phoneSecondary) {
        this.phoneSecondary = phoneSecondary;
    }
    
    public String getEmailPrimary() {
        return this.emailPrimary;
    }
    
    public void setEmailPrimary(final String emailPrimary) {
        this.emailPrimary = emailPrimary;
    }
    
    public String getEmailSecondary() {
        return this.emailSecondary;
    }
    
    public void setEmailSecondary(final String emailSecondary) {
        this.emailSecondary = emailSecondary;
    }
    
    public String getIsActive() {
        return this.isActive;
    }
    
    public void setIsActive(final String isActive) {
        this.isActive = isActive;
    }
    
    public Date getCreadtedTs() {
        return this.creadtedTs;
    }
    
    public void setCreadtedTs(final Date creadtedTs) {
        this.creadtedTs = creadtedTs;
    }
    
    public Date getUpdatedTs() {
        return this.updatedTs;
    }
    
    public void setUpdatedTs(final Date updatedTs) {
        this.updatedTs = updatedTs;
    }
    
    public String getCreatedBy() {
        return this.createdBy;
    }
    
    public void setCreatedBy(final String createdBy) {
        this.createdBy = createdBy;
    }
}