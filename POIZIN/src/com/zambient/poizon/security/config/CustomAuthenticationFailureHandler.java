package com.zambient.poizon.security.config;

import javax.servlet.ServletException;
import java.io.IOException;
import org.springframework.security.core.AuthenticationException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler
{
    public void onAuthenticationFailure(final HttpServletRequest request, final HttpServletResponse response, final AuthenticationException e) throws IOException, ServletException {
        response.sendRedirect("/loginFailed");
    }
}