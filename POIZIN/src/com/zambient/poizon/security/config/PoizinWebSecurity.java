package com.zambient.poizon.security.config;

import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.annotation.web.configurers.FormLoginConfigurer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class PoizinWebSecurity extends WebSecurityConfigurerAdapter
{
    @Autowired
    @Qualifier("userDetailsService")
    UserDetailsService userDetailsService;
    @Autowired
    CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;
    
    @Autowired
    MyLogoutSuccessHandler myLogoutSuccessHandler;
    
    @Autowired
    public void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(this.userDetailsService);
    }
    
    public void configure(HttpSecurity http) throws Exception {
    	
       	http
        		.authorizeRequests()
        		.anyRequest()
        		.authenticated()
        	.and()
        		.formLogin()
        		.loginPage("/admin")
        		.loginProcessingUrl("/profileLogin")
        		.usernameParameter("userName")
        		.passwordParameter("password")
        		.successHandler(customAuthenticationSuccessHandler)
        		.failureUrl("/admin")
        		.permitAll()
        		.and()
        		.logout()
        		.logoutUrl("/logout")
        		//.logoutSuccessHandler(myLogoutSuccessHandler)
        		.permitAll()
        	.and()
        		.csrf()
        		.disable()
        		.exceptionHandling()
        		.accessDeniedPage("/admin");
    }
    
    public void configure(final WebSecurity web) throws Exception {
        web.ignoring().antMatchers(new String[] { "/resources/**","/store/**","/store/forgotPassword/**","/store/forgotPProcess/**","/logout" });
    }
}