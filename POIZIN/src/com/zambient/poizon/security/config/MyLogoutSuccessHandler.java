package com.zambient.poizon.security.config;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.zambient.poizon.utill.UserSession;
@Component
public class MyLogoutSuccessHandler implements LogoutSuccessHandler {
	@Autowired
	private UserSession userSession;
	
	@Override
	public void onLogoutSuccess(HttpServletRequest request,HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		
		if(authentication != null) {
			System.out.println(authentication.getName());
		}
		//perform other required operation
		try{
			userSession.setLoginId(Long.parseLong("0"));
			HttpSession userSession = request.getSession(false);
			userSession.invalidate();
			FlashMap redirectAttribute=new FlashMap();
			redirectAttribute.	addTargetRequestParam("message", "Logout Sucessfully");
			
		}catch(Exception e){
			e.printStackTrace();
			
		}
		
	
	}
} 