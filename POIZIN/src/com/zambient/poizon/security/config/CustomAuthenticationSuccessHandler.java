package com.zambient.poizon.security.config;

import javax.servlet.ServletException;
import java.io.IOException;
import com.zambient.poizon.security.entity.LoginProfile;
import com.zambient.poizon.security.entity.LoginCredentials;
import java.util.Iterator;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.Authentication;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import com.zambient.poizon.utill.UserSession;
import org.springframework.stereotype.Component;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler
{
    @Autowired
    private UserSession userSession;
    
    public void onAuthenticationSuccess(final HttpServletRequest request, final HttpServletResponse response, final Authentication authentication) throws IOException, ServletException {
        final User principal = (User)authentication.getPrincipal();
        System.out.println("principal" + principal.getUsername());
        boolean isAdmin = false;
        final Iterator<? extends GrantedAuthority> grantedAuthorityIterator = principal.getAuthorities().iterator();
        while (grantedAuthorityIterator.hasNext()) {
            if (((GrantedAuthority)grantedAuthorityIterator.next()).getAuthority().equalsIgnoreCase("ADMIN")) {
                isAdmin = true;
            }
        }
        if (isAdmin) {
            System.out.println("Success");
            final User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            final LoginCredentials loginUser = user.getLoginUser();
            final LoginProfile loginProfile = loginUser.getLoginProfile();
            final int storeId = loginProfile.getStoreProfile().getStoreId();
            String storeName=loginProfile.getStoreProfile().getStoreName();
            this.userSession.setUserId("POIZIN");
            this.userSession.setLoginId(Long.valueOf((long)loginProfile.getLoginProfileId()));
            this.userSession.setStoreId(storeId);
            this.userSession.setStoreName(storeName);
            response.sendRedirect("admin");
        }
        else {
            System.out.println("&&&&&&&&& Failed &&&&&&&&&&");
            response.sendRedirect("admin");
        }
    }
}