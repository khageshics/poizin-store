package com.zambient.poizon.security.config;

import com.zambient.poizon.security.entity.LoginCredentials;
import org.springframework.security.core.GrantedAuthority;
import java.util.Collection;
import org.springframework.security.core.userdetails.UserDetails;

public class User implements UserDetails
{
    private String userName;
    private String password;
    private boolean isEnabled;
    private Boolean accountNonExpired;
    private Boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private Collection<? extends GrantedAuthority> authorities;
    private LoginCredentials loginUser;
    
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }
    
    public String getPassword() {
        return this.password;
    }
    
    public String getUsername() {
        return this.userName;
    }
    
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }
    
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }
    
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }
    
    public boolean isEnabled() {
        return this.isEnabled;
    }
    
    public String getUserName() {
        return this.userName;
    }
    
    public void setUserName(final String userName) {
        this.userName = userName;
    }
    
    public Boolean getAccountNonExpired() {
        return this.accountNonExpired;
    }
    
    public void setAccountNonExpired(final Boolean accountNonExpired) {
        this.accountNonExpired = accountNonExpired;
    }
    
    public Boolean getAccountNonLocked() {
        return this.accountNonLocked;
    }
    
    public void setAccountNonLocked(final Boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }
    
    public LoginCredentials getLoginUser() {
        return this.loginUser;
    }
    
    public void setLoginUser(final LoginCredentials loginUser) {
        this.loginUser = loginUser;
    }
    
    public void setPassword(final String password) {
        this.password = password;
    }
    
    public void setEnabled(final boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
    
    public void setCredentialsNonExpired(final boolean credentialsNonExpired) {
        this.credentialsNonExpired = credentialsNonExpired;
    }
    
    public void setAuthorities(final Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }
    
    public User(final String userName, final String password, final boolean isEnabled, final Boolean accountNonExpired, final Boolean accountNonLocked, final boolean credentialsNonExpired, final Collection<? extends GrantedAuthority> authorities, final LoginCredentials loginUser) {
        this.userName = userName;
        this.password = password;
        this.isEnabled = isEnabled;
        this.accountNonExpired = accountNonExpired;
        this.accountNonLocked = accountNonLocked;
        this.credentialsNonExpired = credentialsNonExpired;
        this.authorities = authorities;
        this.loginUser = loginUser;
    }
}