package com.zambient.poizon.security.dao;

import com.zambient.poizon.security.entity.LoginCredentials;

public interface UserAuthDao
{
    LoginCredentials findByUserName(final String p0);
}