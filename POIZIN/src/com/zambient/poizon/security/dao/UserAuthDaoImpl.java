package com.zambient.poizon.security.dao;

import java.util.List;
import java.util.ArrayList;
import com.zambient.poizon.security.entity.LoginCredentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class UserAuthDaoImpl implements UserAuthDao
{
    @Autowired
    private SessionFactory sessionFactory;
    
    public LoginCredentials findByUserName(String userName) {
        List<LoginCredentials> users = new ArrayList<LoginCredentials>();
        
        		Session session=sessionFactory.openSession();
        		Query query=session.createQuery("FROM com.zambient.poizon.security.entity.LoginCredentials credentials WHERE credentials.userName = :uName");
        				query.setParameter("uName",userName);
        				users = (List<LoginCredentials>)query.list();
        if (users.size() > 0) {
            System.out.println("user details :" + users.get(0).getPassword());
            return users.get(0);
        }
        return null;
    }
}