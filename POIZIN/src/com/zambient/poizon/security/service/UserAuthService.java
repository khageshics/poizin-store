package com.zambient.poizon.security.service;

import com.zambient.poizon.security.config.User;
import java.util.Set;
import java.util.Collection;
import java.util.ArrayList;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import java.util.HashSet;
import com.zambient.poizon.security.entity.LoginProfile;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.GrantedAuthority;
import java.util.List;
import com.zambient.poizon.security.entity.LoginCredentials;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import com.zambient.poizon.security.dao.UserAuthDaoImpl;
import org.springframework.stereotype.Service;
import org.springframework.security.core.userdetails.UserDetailsService;

@Service("userDetailsService")
public class UserAuthService implements UserDetailsService
{
    @Autowired
    UserAuthDaoImpl userDao;
    
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(final String userName) throws UsernameNotFoundException {
        System.out.println("##########Entered in to userDetailsService##########");
        final LoginCredentials user = this.userDao.findByUserName(userName);
        System.out.println(user.getLoginProfile().getRole());
        final List<GrantedAuthority> authorities = this.buildUserAuthority(user.getLoginProfile());
        return (UserDetails)this.buildUserForAuthentication(user, authorities);
    }
    
    private List<GrantedAuthority> buildUserAuthority(final LoginProfile userRoles) {
        final Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();
        System.out.println("*********** Role :" + userRoles.getRole() + "*****************");
        setAuths.add((GrantedAuthority)new SimpleGrantedAuthority(userRoles.getRole().trim()));
        final List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);
        return Result;
    }
    
    private User buildUserForAuthentication(final LoginCredentials user, final List<GrantedAuthority> authorities) {
        return new User(user.getUserName(), user.getPassword(), user.isEnabled(), true, true, true, authorities, user);
    }
}