package com.zambient.poizon.dao;

import java.util.List;

import com.zambient.poizon.bean.CardCashSaleBeen;
import com.zambient.poizon.bean.DateAndIndentBean;
import com.zambient.poizon.bean.DiscountAndMonthBean;
import com.zambient.poizon.bean.DiscountAsPerCompanyBeen;
import com.zambient.poizon.bean.DiscountTransactionBean;
import com.zambient.poizon.bean.ExpenseCategoryAndDate;
import com.zambient.poizon.bean.HomeBean;
import com.zambient.poizon.bean.MobileViewStockLiftingDiscount;
import com.zambient.poizon.bean.RentalAndBreakageBean;
import com.zambient.poizon.bean.TargetWithBudget;
import com.zambient.poizon.bean.TotalPriceDateWiseBean;
import com.zambient.poizon.bean.VendorAdjustmentGetArrearsBean;

public interface PoizonThirdDao {

	 public HomeBean getHomePageDetails();
	 public List<DiscountAsPerCompanyBeen> downLoadCompanyDiscount(int company, String date);
	 public List<DiscountAndMonthBean> downloadDiscountCompanyWiseForAllMonth(String date,int company);
	 public List<MobileViewStockLiftingDiscount> discountReportForSelectedMonth(String startMonth,String endMonth);
	 public List<ExpenseCategoryAndDate>  getExpenseDataRangeWise(String startDate,String endDate);
	 public List<DiscountAsPerCompanyBeen> getHomePageDiscountDetails(int comapnyId);
	 public List<DiscountAsPerCompanyBeen> generateCompanyDiscount(int company,String startMonth,String endMonth);
	 public CardCashSaleBeen getTotalDiscountAndRental(int company,String startMonth,String endMonth);
	 public List<TotalPriceDateWiseBean> getSaleCanvasPieChart(String startDate,String endDate);
	 public VendorAdjustmentGetArrearsBean getExistingSingleCompanyArrears(String discountMonth,String company);
	 public RentalAndBreakageBean getExistingRentalForAMonth(String company,String month);
	 public String saveOrUpdateRentalForAMonth(RentalAndBreakageBean bean, String companyId);
	 public RentalAndBreakageBean getExistingBreakageForAMonth(String month);
	 public String saveOrUpdateBreakageForAMonth(RentalAndBreakageBean bean);
	 public List<DiscountTransactionBean> getAllPendingChecks();
	 public String updateReceivedCheckStatus(int trid,int companyid,String month,String transactionDate, String clearedBank);
	 public DateAndIndentBean downloadendentEstimateDiscountAsPdf(boolean flag);
	 public TargetWithBudget downloadtargetanddiscpercase(String company,String date);
	 
}
