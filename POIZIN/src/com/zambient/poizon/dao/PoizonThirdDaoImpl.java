package com.zambient.poizon.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.zambient.poizon.bean.CardCashSaleBeen;
import com.zambient.poizon.bean.DashboardQuantitySaleBean;
import com.zambient.poizon.bean.DashboardSaleBean;
import com.zambient.poizon.bean.DateAndIndentBean;
import com.zambient.poizon.bean.DateAndValueBean;
import com.zambient.poizon.bean.DayAndSaleBean;
import com.zambient.poizon.bean.DiscountAndMonthBean;
import com.zambient.poizon.bean.DiscountAsPerCompanyBeen;
import com.zambient.poizon.bean.DiscountMonthWiseBean;
import com.zambient.poizon.bean.DiscountTransactionBean;
import com.zambient.poizon.bean.DownloadTargetCase;
import com.zambient.poizon.bean.ExpenseCategoryAndDate;
import com.zambient.poizon.bean.ExpenseCategoryBean;
import com.zambient.poizon.bean.HomeBean;
import com.zambient.poizon.bean.IndentEstimatePDF;
import com.zambient.poizon.bean.MobileViewStockDiscountInner;
import com.zambient.poizon.bean.MobileViewStockLiftingDiscount;
import com.zambient.poizon.bean.NotClearCheckBean;
import com.zambient.poizon.bean.PieChartBean;
import com.zambient.poizon.bean.RentalAndBreakageBean;
import com.zambient.poizon.bean.SaleBean;
import com.zambient.poizon.bean.TargetWithBudget;
import com.zambient.poizon.bean.TotalPriceDateWiseBean;
import com.zambient.poizon.bean.TurnovertaxBean;
import com.zambient.poizon.bean.VendorAdjustmentGetArrearsBean;
import com.zambient.poizon.utill.UserSession;

@PropertySource("classpath:/com/zambient/poizon/resources/poizon.properties")
@Repository("poizonThirdDao")
public class PoizonThirdDaoImpl implements PoizonThirdDao{
	final static Logger log = Logger.getLogger(PoizonThirdDaoImpl.class);
    @Autowired
	private JdbcTemplate jdbcTemplate;
    
    @Autowired 
	private UserSession userSession;
    
    @Value("${discountDate.1}")
   	private String discountDate1;
       
    @Value("${discountDate.2}")
   	private String discountDate2;
       
    @Value("${discountDate.4}")
   	private String discountDate4;
       
    @Value("${discountDate.5}")
    private String discountDate5;
    
    @Value("${discountDate.6}")
    private String discountDate6;

	@Override
	public HomeBean getHomePageDetails() {
		
		int storeId=userSession.getStoreId();
	HomeBean bean = new HomeBean();
	try{
		// bean.setTotalPrice(jdbcTemplate.queryForObject(QUERY, Double.class));
		/*double tempVal = jdbcTemplate.queryForObject("SELECT ROUND(SUM(((SELECT i.SingleBottelRate FROM invoice i WHERE i.brandNoPackQty=s2.brandNoPackQty AND "
				+ "`invoiceDate`<=(SELECT MAX(`invoiceDateAsPerSheet`) FROM invoice WHERE `invoiceDateAsPerSheet`<=( SELECT `saleDate` FROM sale ORDER BY `saleDate`"
				+ " DESC LIMIT 1)) ORDER BY `invoiceId` DESC LIMIT 1)*s2.`closing`))+ (SELECT COALESCE(SUM(`totalPrice`),0) FROM `invoice`"
				+ " WHERE `invoiceDate` > (SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1)),0) AS inhouseAmount FROM sale s2 "
				+ " WHERE s2.`saleDate`= (SELECT MAX(`saleDate`) FROM sale WHERE `saleDate`<=(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1))", Double.class);
		*/
		
		bean.setInHouseStockValue((double) Math.round(InHouseStockAmount()));
		
		
			/*
			 * bean.setSaleValue(jdbcTemplate.
			 * queryForObject("SELECT SUM(`totalPrice`) AS totalPrice FROM sale WHERE saleDate=(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1)"
			 * , Double.class));
			 */	   
		
		//StoreId appended logic
		try {
        bean.setSaleValue((Double)this.jdbcTemplate.queryForObject("SELECT SUM(`totalPrice`) AS totalPrice FROM sale WHERE saleDate=(SELECT `saleDate` FROM sale WHERE store_id=" + storeId +" ORDER BY `saleDate` DESC LIMIT 1) AND store_id=" + storeId + "",Double.class));
		}catch(Exception exc) {
			bean.setSaleValue(0.0);
		}
		
		bean.setPendingDiscount(getAllCompaniesArrears());
	   // bean.setMonthlySale(getSalePriceMonthWise());
	    bean.setDailySale(getSalePriceDailyWise());
	    
			/*
			 * bean.setComulativeValue(jdbcTemplate.
			 * queryForObject("SELECT ROUND(SUM(`totalPrice`), 0) AS comulative FROM invoice WHERE `invoiceDateAsPerSheet`>='"
			 * +discountDate6+"'", Double.class));
			 */
	    //StoreId Appended query
        bean.setComulativeValue((Double)this.jdbcTemplate.queryForObject("SELECT ROUND(SUM(`totalPrice`), 0) AS comulative FROM invoice WHERE store_id=" + storeId + " AND `invoiceDateAsPerSheet`>='" + this.discountDate6 + "'", (Class)Double.class));

	    
			/*
			 * bean.setTcsVal(jdbcTemplate.
			 * queryForObject("SELECT ROUND(SUM(`tcsVal`), 0)  FROM invoice_mrp_round_off WHERE `mrpRoundOffDateAsCopy`>='"
			 * +discountDate6+"'", Double.class));
			 */
	    
        //StoreId appended query
        bean.setTcsVal((Double)this.jdbcTemplate.queryForObject("SELECT ROUND(SUM(`tcsVal`), 0)  FROM invoice_mrp_round_off WHERE store_id=" + storeId + " AND `mrpRoundOffDateAsCopy`>='" + this.discountDate6 + "'", (Class)Double.class));

	    
	    //  bean.setProfitOrLoss(getCompleteProfitOrLossAmount(tempVal));
	    bean.setMaxSaleProduct(getMaxSaleProduct());
	    
	    bean.setDoughnutChart(getDoughnutChart());
	    
	    bean.setLowStockProduct(getTopTenLowStockProduct());
	    
	    bean.setCompanyDiscountDues(getCompanyDiscountDuesAmt());
	    
			/*
			 * bean.setCurrentDate(jdbcTemplate.
			 * queryForObject("SELECT DATE_FORMAT(saleDate, '%d-%b-%Y') FROM sale ORDER BY `saleDate` DESC LIMIT 1"
			 * , String.class));
			 */
	    
	    //StoreId appended Query
	    
	    try {
        bean.setCurrentDate((String)this.jdbcTemplate.queryForObject("SELECT DATE_FORMAT(saleDate, '%d-%b-%Y') FROM sale where store_id=" + storeId + " ORDER BY `saleDate` DESC LIMIT 1", (Class)String.class));
	    }catch(Exception exc) {
	        bean.setCurrentDate((String)this.jdbcTemplate.queryForObject("SELECT DATE_FORMAT(invoiceDate, '%d-%b-%Y') FROM invoice where store_id=" + storeId + " ORDER BY `invoiceDate` DESC LIMIT 1",String.class));

	    }
	    bean.setRetailerCreditBalance(getRetailerCrdit(storeId));
	}catch(Exception e){
			e.printStackTrace();
			return bean;
		}
	return bean;
	}
	private DateAndValueBean getRetailerCrdit(int storeId) {
		DateAndValueBean bean = new DateAndValueBean();
		try{
			
			/*
			 * List<Map<String, Object>> rows = jdbcTemplate.
			 * queryForList("SELECT DATE_FORMAT(mrpRoundOffDateAsCopy, '%d-%b-%Y') AS date, retailer_credit_balance FROM invoice_mrp_round_off ORDER BY mrpRoundOffDateAsCopy DESC LIMIT 1"
			 * );
			 */			
             List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT DATE_FORMAT(mrpRoundOffDateAsCopy, '%d-%b-%Y') AS date, retailer_credit_balance FROM invoice_mrp_round_off where store_id=" + storeId + " ORDER BY mrpRoundOffDateAsCopy DESC LIMIT 1");

			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					bean.setDate(row.get("date")!=null?row.get("date").toString():"");
					bean.setValue(row.get("retailer_credit_balance")!=null?Double.parseDouble(row.get("retailer_credit_balance").toString()):0);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return bean;
		}
		return bean;
	}
	public double InHouseStockAmount() {
		
		int storeId=userSession.getStoreId();
		double result=0;
		String date=null;
		try {
		date = jdbcTemplate.queryForObject("SELECT `saleDate` FROM sale where store_id="+storeId+" ORDER BY `saleDate` DESC LIMIT 1", String.class);
		}catch(Exception exc) {
			date = jdbcTemplate.queryForObject("SELECT DATE_ADD((SELECT `invoiceDate` FROM `invoice` WHERE store_id = "+userSession.getStoreId()+" ORDER BY `invoiceDate` DESC LIMIT 1), INTERVAL -1 DAY)", String.class);
		}
		
		//SELECT DATE_ADD((SELECT `invoiceDate` FROM `invoice` WHERE store_id = "+userSession.getStoreId()+" ORDER BY `invoiceDate` DESC LIMIT 1), INTERVAL 1 DAY)
		
		
		/*
		 * String
		 * Query="SELECT p.`shortBrandName`,p.`brandNo`,p.`brandNoPackQty`FROM product p WHERE "
		 * +
		 * "(SELECT COALESCE(SUM(s.closing),0) + (SELECT COALESCE(SUM(`receivedBottles`),0) FROM invoice r WHERE r.invoiceDate > '"
		 * +date+"'" +
		 * " AND r.brandNoPackQty = p.`brandNoPackQty`) FROM sale s WHERE s.`saleDate`='"
		 * +date+"' " +
		 * "AND s.brandNoPackQty = p.`brandNoPackQty`) > 0 GROUP BY p.`brandNoPackQty` ORDER BY p.`brandNo`"
		 * ;
		 */
		
		
		//storeId implemented query
         String Query = "SELECT p.`shortBrandName`,p.`brandNo`,p.`brandNoPackQty`FROM product p WHERE "
         		+ "(SELECT COALESCE(SUM(s.closing),0) + (SELECT COALESCE(SUM(`receivedBottles`),0) FROM"
         		+ " invoice r WHERE r.invoiceDate > '" + date + "'" + " AND r.brandNoPackQty = p.`brandNoPackQty` AND r.store_id = " + storeId + ") "
         				+ "FROM sale s WHERE s.`saleDate`='" + date + "' " + "AND s.brandNoPackQty = p.`brandNoPackQty` AND s.store_id = " + storeId + ")"
         						+ " > 0 GROUP BY p.`brandNoPackQty` ORDER BY p.`brandNo`";

		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(Query);	
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					result += getexactInvoiceValue(2,date,Double.parseDouble(row.get("brandNoPackQty").toString()),storeId);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}
	private double getexactInvoiceValue(int num,String startDate, double brandNoPackQty,int storeId) {
		double exactInvoice=0;
		
		/*
		 * long totalSale = jdbcTemplate.
		 * queryForObject("SELECT COALESCE(SUM(s.sale),0) AS sale FROM sale s WHERE s.`brandNoPackQty`="
		 * +brandNoPackQty+" AND s.`saleDate` <='"+startDate+"'",Long.class);
		 */		
		
		//StoreId appended query
         long totalSale = jdbcTemplate.queryForObject("SELECT COALESCE(SUM(s.sale),0) AS sale FROM sale s WHERE s.`brandNoPackQty`=" + brandNoPackQty + " AND s.store_id=" + storeId + " AND s.`saleDate` <='" + startDate + "'", Long.class);

			long stockVal =0,cnt=0;
		List<TurnovertaxBean> beanList = getInvoiceList(num,startDate,brandNoPackQty,storeId);
		for(TurnovertaxBean bean : beanList){
			stockVal = stockVal + bean.getStock();
     		if(stockVal >= totalSale){
     			if(cnt == 0){
					long val = stockVal - totalSale;
					exactInvoice += val * bean.getInvoiceRate();
					cnt++;
				}else{
				exactInvoice += bean.getStock() * bean.getInvoiceRate();
				cnt++;
				}	
     		}	
			
		}
		return exactInvoice;
	}
	private List<TurnovertaxBean> getInvoiceList(int num, String startDate, double brandNoPackQty,int storeId) {
		List<TurnovertaxBean> beanList = new ArrayList<TurnovertaxBean>();
		String QUERY = null;
		if(num == 1){
			
			
			/*
			 * QUERY =
			 * "SELECT p.`brandNo`,p.`brandNoPackQty`, i.`receivedBottles`, i.`invoiceDateAsPerSheet`,i.`SingleBottelRate` "
			 * +
			 * "FROM product p INNER JOIN invoice i  ON p.`brandNoPackQty`=i.`brandNoPackQty`  WHERE i.`receivedBottles` > 0 AND i.`invoiceDateAsPerSheet` <= '"
			 * +startDate+"' AND p.`brandNoPackQty`="
			 * +brandNoPackQty+"  GROUP BY i.`invoiceDateAsPerSheet`  ";
			 */
			//StoreId appended query
            QUERY = "SELECT p.`brandNo`,p.`brandNoPackQty`, i.`receivedBottles`, i.`invoiceDateAsPerSheet`,i.`SingleBottelRate` FROM product p INNER JOIN invoice i  ON p.`brandNoPackQty`=i.`brandNoPackQty`  WHERE i.`receivedBottles` > 0 AND i.`invoiceDateAsPerSheet` <= '" + startDate + "' AND i.store_id=" + storeId + " AND p.`brandNoPackQty`=" + brandNoPackQty + "  GROUP BY i.`invoiceDateAsPerSheet`  ";

			
		}else{
			
			
			/*
			 * QUERY =
			 * "SELECT p.`brandNo`,p.`brandNoPackQty`, i.`receivedBottles`, i.`invoiceDateAsPerSheet`,i.`SingleBottelRate` "
			 * +
			 * "FROM product p INNER JOIN invoice i  ON p.`brandNoPackQty`=i.`brandNoPackQty`  WHERE i.`receivedBottles` > 0  AND p.`brandNoPackQty`="
			 * +brandNoPackQty+"  GROUP BY i.`invoiceDateAsPerSheet`  ";
			 * 
			 */
			
			//StoredId appended query
            QUERY = "SELECT p.`brandNo`,p.`brandNoPackQty`, i.`receivedBottles`, i.`invoiceDateAsPerSheet`,i.`SingleBottelRate` FROM product p INNER JOIN invoice i  ON p.`brandNoPackQty`=i.`brandNoPackQty`  WHERE i.`receivedBottles` > 0  AND p.`brandNoPackQty`=" + brandNoPackQty + " AND i.store_id=" + storeId + " GROUP BY i.`invoiceDateAsPerSheet`  ";

			
		}
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					TurnovertaxBean bean = new TurnovertaxBean();
					String invoicedate = row.get("invoiceDateAsPerSheet")!=null?row.get("invoiceDateAsPerSheet").toString():"";
					long received = row.get("receivedBottles")!=null?Long.parseLong(row.get("receivedBottles").toString()):0;
					double price = row.get("SingleBottelRate")!=null?Double.parseDouble(row.get("SingleBottelRate").toString()):0;
					//if((sdf.parse(invoicedate).after(sdf.parse(discountDate1)) && sdf.parse(invoicedate).before(sdf.parse(discountDate4))) || sdf.parse(invoicedate).after(sdf.parse(discountDate5)))
					if((sdf.parse(invoicedate).after(sdf.parse(discountDate1)) && sdf.parse(invoicedate).before(sdf.parse(discountDate4))) || (sdf.parse(invoicedate).after(sdf.parse(discountDate5)) && sdf.parse(invoicedate).before(sdf.parse(discountDate6)))) 
					{
						bean.setDate(invoicedate);
						bean.setInvoiceRate(price + (price * 14.5)/100);
						bean.setStock(received);
						beanList.add(bean);
       			      } 
       			 else{
       				bean.setDate(invoicedate);
					bean.setInvoiceRate(price);
					bean.setStock(received);
					beanList.add(bean);
       			  }
				}
			}
		   }catch(Exception e){
			e.printStackTrace();
			return beanList;
			}
		return beanList;
	}
	private List<DiscountAsPerCompanyBeen> getCompanyDiscountDuesAmt() {
		int storeId=userSession.getStoreId();
		
		/*
		 * String QUERY = "SELECT ct.`companyId`,ct.`companyName`," +
		 * "(SELECT COALESCE(SUM(sd.totalDiscountAmount),0) FROM stock_lift_with_discount sd "
		 * +
		 * "WHERE sd.`companyName` =ct.`companyId` )AS totalDiscountAmount,(SELECT COALESCE(SUM(dt.transactionAmt + dt.adjCheck),0) "
		 * +
		 * "FROM discount_transaction dt WHERE dt.companyId =ct.`companyId` AND dt.received=1 AND dt.transactionDate IS NOT NULL)AS transactionAmt,"
		 * +
		 * "(SELECT COALESCE(SUM(t.rentalAmount),0) FROM `rental_tab` t WHERE t.company =ct.`companyId` )AS rentalAmount,"
		 * +
		 * "(SELECT COALESCE(SUM(ds.`adj_amount`),0) FROM `vendor_adjustment` ds WHERE ds.company =ct.`companyId`) AS vendorAmt,"
		 * +
		 * "(SELECT COALESCE(SUM(cm.totalAmount - cm.received ),0) FROM credit_master cm WHERE cm.company = ct.`companyId`) as creditAmt FROM `company_tab` ct"
		 * ;
		 */
	
		//StoreId appended query
        final String QUERY = "SELECT ct.`companyId`,ct.`companyName`,(SELECT COALESCE(SUM(sd.totalDiscountAmount),0) FROM stock_lift_with_discount sd "
        		+ "WHERE sd.`companyName` =ct.`companyId` AND sd.store_id=" + storeId + " )AS totalDiscountAmount,(SELECT COALESCE(SUM(dt.transactionAmt + dt.adjCheck),0) " + 
        		"FROM discount_transaction dt WHERE dt.companyId =ct.`companyId` AND dt.received=1 AND dt.store_id=" + storeId + " AND dt.transactionDate IS NOT NULL)AS transactionAmt," + 
        		"(SELECT COALESCE(SUM(t.rentalAmount),0) FROM `rental_tab` t WHERE t.company =ct.`companyId` and t.store_id=" + storeId + ")AS rentalAmount," + 
        		"(SELECT COALESCE(SUM(ds.`adj_amount`),0) FROM `vendor_adjustment` ds WHERE ds.company =ct.`companyId` and ds.store_id=" + storeId + ") AS vendorAmt FROM `company_tab` ct";

		
	//System.out.println(QUERY);
		List<DiscountAsPerCompanyBeen> beanList = new ArrayList<DiscountAsPerCompanyBeen>();
	    try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						DiscountAsPerCompanyBeen bean = new DiscountAsPerCompanyBeen();
						bean.setCompanyName(row.get("companyName")!=null?row.get("companyName").toString():"");
						bean.setArrears(((row.get("totalDiscountAmount")!=null?Double.parseDouble(row.get("totalDiscountAmount").toString()):0) + (row.get("creditAmt")!=null?Double.parseDouble(row.get("creditAmt").toString()):0)
								+ (row.get("rentalAmount")!=null?Double.parseDouble(row.get("rentalAmount").toString()):0)) - ((row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0) + (row.get("vendorAmt")!=null?Double.parseDouble(row.get("vendorAmt").toString()):0)));
						bean.setCompany(row.get("companyId")!=null?Long.parseLong(row.get("companyId").toString()):-1);
						beanList.add(bean);
					}
				}
		 }catch(Exception e){
			 e.printStackTrace();
			 return beanList;
		 }
		return beanList;
	}
	private List<SaleBean> getTopTenLowStockProduct() {
		
		int storeId=userSession.getStoreId();
		/*
		 * String QUERY =
		 * "SELECT DISTINCT p.brandNoPackQty,p.brandNo,p.shortBrandName,p.packQty,p.category,p.`packType`,"
		 * +
		 * "TRUNCATE(s.closing/p.packQty,0) AS closingCase,(s.closing - (p.`packQty`*(TRUNCATE(s.closing/p.`packQty`,0)))) AS bottle,"
		 * +
		 * "(SELECT TRUNCATE((SELECT COALESCE(s1.closing,0) FROM sale s1 WHERE s1.`brandNoPackQty`=p.brandNoPackQty "
		 * +
		 * " AND s1.saleDate =(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1))/(SELECT TRUNCATE(COALESCE(SUM(s2.`sale`)/14,0),2) FROM sale s2 "
		 * +
		 * "WHERE s2.`brandNoPackQty`= p.brandNoPackQty AND s2.saleDate >=(SELECT (SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1) -INTERVAL 14 DAY) "
		 * +
		 * "AND s2.saleDate <=(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1)),0)) AS noOfDays FROM product p INNER JOIN sale s"
		 * +
		 * "  ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`=(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1) AND p.`active`=1 GROUP BY brandNoPackQty ORDER BY noOfDays ASC LIMIT 8"
		 * ;
		 */
		
		//StoreId appended query
        final String QUERY = "SELECT DISTINCT p.brandNoPackQty,p.brandNo,p.shortBrandName,p.packQty,p.category,p.`packType`,TRUNCATE(s.closing/p.packQty,0) AS closingCase,(s.closing - (p.`packQty`*(TRUNCATE(s.closing/p.`packQty`,0)))) AS bottle,(SELECT TRUNCATE((SELECT COALESCE(s1.closing,0) FROM sale s1 WHERE s1.store_id=" + storeId + " AND s1.`brandNoPackQty`=p.brandNoPackQty AND " + "s1.saleDate =(SELECT `saleDate` FROM sale WHERE store_id=" + storeId + "  ORDER BY `saleDate` DESC LIMIT 1))/(SELECT TRUNCATE(COALESCE(SUM(s2.`sale`)/14,0),2) FROM sale s2" + " WHERE s2.store_id=" + storeId + " AND s2.`brandNoPackQty`= p.brandNoPackQty AND s2.saleDate >=(SELECT (SELECT `saleDate` FROM sale  WHERE store_id=" + storeId + "  ORDER BY `saleDate` DESC LIMIT 1) -INTERVAL 14 DAY)" + " AND s2.saleDate <=(SELECT `saleDate` FROM sale WHERE store_id=" + storeId + " ORDER BY `saleDate` DESC LIMIT 1)),0)) AS noOfDays FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` " + "AND `saleDate`=(SELECT `saleDate` FROM sale WHERE store_id=" + storeId + " ORDER BY `saleDate` DESC LIMIT 1) AND s.`store_id`=" + storeId + " AND p.`active`=1 GROUP BY brandNoPackQty ORDER BY noOfDays ASC LIMIT 8";

		List<SaleBean> beanList = new ArrayList<SaleBean>();
        try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						SaleBean bean = new SaleBean();
						bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
						bean.setPackType(row.get("packType")!=null?row.get("packType").toString():"");
						bean.setClosing(row.get("closingCase")!=null?Long.parseLong(row.get("closingCase").toString()):0);
						bean.setQtyBottels(row.get("bottle")!=null?Long.parseLong(row.get("bottle").toString()):0);
						bean.setNoOfReturnsBtl(row.get("noOfDays")!=null?Long.parseLong(row.get("noOfDays").toString()):0);
						beanList.add(bean);
					}
				}
		 }catch(Exception e){
			 e.printStackTrace();
			 return beanList;
		 }
		return beanList;
	}
	private List<TotalPriceDateWiseBean> getDoughnutChart() {
		
		int storeId=userSession.getStoreId();
		String previousDate=null,currentDate=null;
		/*String QUERY = "SELECT SUM(s.`sale`) AS sale,(SELECT t.categoryName FROM `category_tab` t WHERE t.categoryId = p.`category`) AS categoryName FROM product p INNER JOIN sale s"
				+ "   ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`=(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1) "
				+ " GROUP BY p.`category`";*/
		List<TotalPriceDateWiseBean> beanList = new ArrayList<TotalPriceDateWiseBean>();
        try{
			/*
			 * currentDate = jdbcTemplate.
			 * queryForObject("SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1"
			 * , String.class);
			 * 
			 * 
			 * double totalSale = jdbcTemplate.
			 * queryForObject("SELECT ROUND(SUM(s.`sale`/p.packQty),1 )AS saleInCase FROM product p INNER JOIN sale s"
			 * +
			 * "  ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`='"+currentDate+
			 * "'", Double.class);
			 * 
			 * 
			 * String
			 * QUERY="SELECT SUM(s.`sale`) AS sale,ROUND(SUM(s.`sale`/p.packQty),1 ) AS saleInCase, p.category,"
			 * +
			 * "(SELECT t.categoryName FROM `category_tab` t WHERE t.categoryId = p.`category`) AS categoryName,"
			 * +
			 * "SUM(s.totalPrice) AS amount,ROUND(((ROUND(SUM(s.`sale`/p.packQty),1 ))*100)/"
			 * +totalSale+",1) AS caseInPercentage" +
			 * " FROM product p INNER JOIN sale s  ON p.`brandNoPackQty` = s.`brandNoPackQty`"
			 * + " AND `saleDate`='"
			 * +currentDate+"' GROUP BY p.`category` ORDER BY p.`category`";
			 */
		        
        	//StoreId appended queries
        	try {
        	 currentDate = jdbcTemplate.queryForObject("SELECT `saleDate` FROM sale where store_id=" + storeId + " ORDER BY `saleDate` DESC LIMIT 1", String.class);
        	}catch(Exception exc) {
           	 currentDate = jdbcTemplate.queryForObject("SELECT `invoiceDate` FROM invoice where store_id=" + storeId + " ORDER BY `invoiceDate` DESC LIMIT 1", String.class);
        	}
        	double totalSale=0.0;
        	try {
        	  totalSale =jdbcTemplate.queryForObject("SELECT ROUND(SUM(s.`sale`/p.packQty),1 )AS saleInCase FROM product p INNER JOIN sale s  ON p.`brandNoPackQty` = s.`brandNoPackQty` AND s.store_id=" + storeId + " AND `saleDate`='" + currentDate + "'", Double.class);
        	}catch(Exception exc) {
        	}
        	 String QUERY = "SELECT SUM(s.`sale`) AS sale,ROUND(SUM(s.`sale`/p.packQty),1 ) AS saleInCase, p.category,(SELECT t.categoryName FROM `category_tab` t WHERE t.categoryId = p.`category`) AS categoryName,SUM(s.totalPrice) AS amount,ROUND(((ROUND(SUM(s.`sale`/p.packQty),1 ))*100)/" + totalSale + ",1) AS caseInPercentage" + " FROM product p INNER JOIN sale s  ON p.`brandNoPackQty` = s.`brandNoPackQty`" + " AND s.store_id=" + storeId + " AND `saleDate`='" + currentDate + "' GROUP BY p.`category` ORDER BY p.`category`";
		        
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						TotalPriceDateWiseBean bean = new TotalPriceDateWiseBean();
						//String str = (row.get("categoryName").toString() + "-"+(row.get("saleInCase").toString())+"C");
						//System.out.println("str>>"+str);
						bean.setValue(row.get("amount")!=null?Double.parseDouble(row.get("amount").toString()):0);
						bean.setLabel(row.get("categoryName")!=null?row.get("categoryName").toString():"");
						//bean.setLabel(str);
						bean.setSaleInCase(row.get("saleInCase")!=null?Double.parseDouble(row.get("saleInCase").toString()):0);
						bean.setCaseInPercentage(row.get("caseInPercentage")!=null?Double.parseDouble(row.get("caseInPercentage").toString()):0);
						bean.setDashboardSaleBeanList(getDashboardSale(row.get("category")!=null?Long.parseLong(row.get("category").toString()):0,currentDate,row.get("saleInCase")!=null?Double.parseDouble(row.get("saleInCase").toString()):0,storeId));
						beanList.add(bean);
					}
				}
		 }catch(Exception e){
			// e.printStackTrace();
			 return beanList;
		 }
		return beanList;
	}
	private List<DashboardSaleBean> getDashboardSale(long categoryId, String currentDate,double saleInCase,int storeId) {
		List<DashboardSaleBean> beanList = new ArrayList<DashboardSaleBean>();
		String QUERY = "SELECT p.`shortBrandName`,p.`brandNo`,ROUND(SUM(s.`sale`/p.packQty),1 ) AS sale,"
				+ "ROUND(((ROUND(SUM(s.`sale`/p.packQty),1 ))*100)/"+saleInCase+",1) AS caseInPercentage,SUM(s.`totalPrice`) AS amount"
				+ " FROM product p INNER JOIN sale s ON p.`brandNoPackQty`=s.`brandNoPackQty` "
				+ "WHERE p.`category`="+categoryId+" AND saleDate='"+currentDate+"' AND p.`active` AND s.store_id='"+storeId+"' GROUP BY p.`brandNo` ORDER BY p.`brandNo`";
		//System.out.println(QUERY);
		try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						DashboardSaleBean bean = new DashboardSaleBean();
						bean.setBrandNo(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0);
						bean.setShortName(row.get("shortBrandName").toString());
						bean.setAmount(row.get("amount")!=null?Double.parseDouble(row.get("amount").toString()):0);
						bean.setSaleInCase(row.get("sale")!=null?Double.parseDouble(row.get("sale").toString()):0);
						bean.setCaseInPercentage(row.get("caseInPercentage")!=null?Double.parseDouble(row.get("caseInPercentage").toString()):0);
						//bean.setDashboardQuantitySaleBean(getDashboardInnerSale(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0,previousDate,currentDate));
						if((row.get("amount")!=null?Double.parseDouble(row.get("amount").toString()):0) > 0)
						beanList.add(bean);
					}
				}
		 }catch(Exception e){
			 e.printStackTrace();
			 return beanList;
		 }
		return beanList;
	}
	private List<DashboardQuantitySaleBean> getDashboardInnerSale(long brandNo, String previousDate, String currentDate) {
        String QUERY="SELECT p.`shortBrandName`,p.`brandNo`,p.`quantity`,SUM(s.`sale`) AS sale,(SELECT SUM(s1.sale) FROM sale s1, product p1 WHERE "
        		+ " s1.saleDate='"+previousDate+"' AND s1.brandNoPackQty = p1.brandNoPackQty AND p.`brandNo`=p1.brandNo) AS previousSale FROM"
        		+ " product p INNER JOIN sale s ON p.`brandNoPackQty`=s.`brandNoPackQty` WHERE saleDate='"+currentDate+"' AND p.`active` "
        		+ "AND p.`brandNo` = "+brandNo+" GROUP BY p.`brandNoPackQty` ";
        List<DashboardQuantitySaleBean>  beanList = new ArrayList<DashboardQuantitySaleBean>();
        try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						DashboardQuantitySaleBean bean = new DashboardQuantitySaleBean();
						bean.setCurrentSale(row.get("sale")!=null?Long.parseLong(row.get("sale").toString()):0);
						bean.setPreviousSale(row.get("previousSale")!=null?Long.parseLong(row.get("previousSale").toString()):0);
						bean.setQuantity(row.get("quantity")!=null?row.get("quantity").toString():"");
						beanList.add(bean);
					}
				}
		 }catch(Exception e){
			 e.printStackTrace();
			 return beanList;
		 }
		
		return beanList;
	}
	private List<DayAndSaleBean> getMaxSaleProduct() {
		int storeId=userSession.getStoreId();
		List<DayAndSaleBean> beanList = new ArrayList<DayAndSaleBean>();
		
		/*
		 * String QUERY =
		 * "SELECT p.`brandNo`,p.`shortBrandName`,p.`packQty`,SUM(s.`totalPrice`) AS amount FROM product p INNER JOIN sale s ON p.`brandNoPackQty`= s.`brandNoPackQty` WHERE saleDate =(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1) GROUP BY p.`brandNo` ORDER BY amount DESC LIMIT 8"
		 * ;
		 */		
		
		//multi store , storeId appended query
        final String QUERY = "SELECT p.`brandNo`,p.`shortBrandName`,p.`packQty`,SUM(s.`totalPrice`) AS amount FROM product p INNER JOIN sale s ON p.`brandNoPackQty`= s.`brandNoPackQty` WHERE s.store_id=" + storeId + " AND saleDate =(SELECT `saleDate` FROM sale where store_id=" + storeId + " ORDER BY `saleDate` DESC LIMIT 1) GROUP BY p.`brandNo` ORDER BY amount DESC LIMIT 8";

		try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						DayAndSaleBean bean = new DayAndSaleBean();
						bean.setValue(row.get("amount")!=null?Double.parseDouble(row.get("amount").toString()):0);
						bean.setDate(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
						bean.setLinechartData(getUpToLastWeekSoldValue(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0,storeId));
						beanList.add(bean);
					}
				}
		 }catch(Exception e){
			 e.printStackTrace();
			 return beanList;
		 }
		return beanList;
	}
	private List<PieChartBean> getUpToLastWeekSoldValue(long brandNo,int storeId) {
		List<PieChartBean> beanList = new ArrayList<PieChartBean>();
		try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT p.`brandNo`,p.`shortBrandName`,DATE_FORMAT(s.`saleDate`, '%a') AS saleDate,"
	    	 		+ "SUM(s.`totalPrice`) AS amount FROM product p INNER JOIN sale s ON p.`brandNoPackQty`= s.`brandNoPackQty` "
	    	 		+ "WHERE s.store_id='"+storeId+"' AND s.saleDate >= (SELECT DATE_SUB((SELECT `saleDate` FROM sale WHERE store_id='"+storeId+"' ORDER BY `saleDate` DESC LIMIT 1), INTERVAL 7 DAY)) AND p.`brandNo`="+brandNo+" GROUP BY s.`saleDate` ORDER BY s.`saleDate`");
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						PieChartBean bean = new PieChartBean();
						bean.setValue(row.get("amount")!=null?Double.parseDouble(row.get("amount").toString()):0);
						bean.setLabel(row.get("saleDate")!=null?row.get("saleDate").toString():"");
						beanList.add(bean);
					}
				}
		 }catch(Exception e){
			 e.printStackTrace();
			 return beanList;
		 }
		return beanList;
	}
	private Double getCompleteProfitOrLossAmount(double closingStockVal) {
		double result = 0,totalAdministration=0;
    	 String QUERY ="SELECT SUM(totalPrice) AS sale,(SELECT ROUND(SUM(`mrpRoundOff`), 0) FROM invoice_mrp_round_off) AS mrpRoundOff,"
	 		+ "(SELECT ROUND(SUM(`totalPrice`), 0) FROM invoice) AS purchase,(SELECT SUM(transactionAmt)  FROM discount_transaction )AS transactionAmt, "
	 		+ "(SELECT SUM(turnoverTax) FROM invoice_mrp_round_off) AS turnoverTax,(SELECT SUM(breckageAmount) FROM breckage_tab WHERE breckageDate <=(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1)) AS breckageAmount,"
	 		+ "(SELECT COALESCE(SUM(amount),0) FROM monthlyexpenses WHERE debitCredit = 0 AND SUBJECT = 11 "
	 		+ "AND monthlyExpensesDate <=(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1)) AS licenseFee,"
	 		+ "(SELECT COALESCE(SUM(amount),0) FROM monthlyexpenses WHERE debitCredit = 1 AND SUBJECT = 10 AND monthlyExpensesDate <=(SELECT `saleDate` "
	 		+ "FROM sale ORDER BY `saleDate` DESC LIMIT 1)) AS sittingAmt,(SELECT ROUND(SUM(totalAmount), 0) FROM `expense_master`) AS expenses  FROM sale";
		 try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						 totalAdministration = ((row.get("expenses")!=null?Double.parseDouble(row.get("expenses").toString()):0)
								+ (row.get("mrpRoundOff")!=null?Double.parseDouble(row.get("mrpRoundOff").toString()):0) 
								+ (row.get("purchase")!=null?Double.parseDouble(row.get("purchase").toString()):0) 
								+ ((row.get("turnoverTax")!=null?Double.parseDouble(row.get("turnoverTax").toString()):0) 
								+ (row.get("breckageAmount")!=null?Double.parseDouble(row.get("breckageAmount").toString()):0)
								+ (row.get("licenseFee")!=null?Double.parseDouble(row.get("licenseFee").toString()):0))
								- ((row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0) 
										+(row.get("sittingAmt")!=null?Double.parseDouble(row.get("sittingAmt").toString()):0)));
						 
						 result = ((row.get("sale")!=null?Double.parseDouble(row.get("sale").toString()):0)+closingStockVal) - totalAdministration;
					}
				}
		 }catch(Exception e){
			 e.printStackTrace();
			 return result;
		 }
	 return result;
	}
	private List<TotalPriceDateWiseBean> getSalePriceDailyWise() {
		
		int storeId=userSession.getStoreId();
		
		List<TotalPriceDateWiseBean> beanList = new ArrayList<TotalPriceDateWiseBean>();
		
		
		/*
		 * String QUERY =
		 * "SELECT SUM(totalPrice) AS totalPrice,(SELECT DATE_FORMAT(saleDate, '%a')) AS date"
		 * +
		 * " FROM sale WHERE saleDate >= (SELECT (SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1) - INTERVAL 9 DAY)"
		 * +
		 * " AND saleDate <= (SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1)  GROUP BY saleDate ORDER BY saleDate"
		 * ;
		 */
		
		//StoreId appended query
        final String QUERY = "SELECT SUM(totalPrice) AS totalPrice,(SELECT DATE_FORMAT(saleDate, '%a')) AS date FROM sale WHERE saleDate >= (SELECT (SELECT `saleDate` FROM sale where store_id=" + storeId + " ORDER BY `saleDate` DESC LIMIT 1 ) - INTERVAL 9 DAY)" + " AND saleDate <= (SELECT `saleDate` FROM sale where store_id=" + storeId + " ORDER BY `saleDate` DESC LIMIT 1 ) AND store_id=" + storeId + " GROUP BY saleDate ORDER BY saleDate";

        
        try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						TotalPriceDateWiseBean bean = new TotalPriceDateWiseBean();
						bean.setValue(row.get("totalPrice")!=null?Double.parseDouble(row.get("totalPrice").toString()):0);
						bean.setLabel(row.get("date")!=null?row.get("date").toString():"");
						beanList.add(bean);
					}
				}
		 }catch(Exception e){
			 e.printStackTrace();
			 return beanList;
		 }
		return beanList;
	}
	private List<DayAndSaleBean> getSalePriceMonthWise() {
		List<DayAndSaleBean> beanList = new ArrayList<DayAndSaleBean>();
		String QUERY = "SELECT SUM(totalPrice) AS totalPrice,(SELECT DATE_FORMAT(saleDate, '%b' ' %Y')) AS date FROM sale GROUP BY (SELECT CONCAT(DATE_FORMAT(LAST_DAY(saleDate - INTERVAL 0 MONTH),'%Y-%m-'),'01')) ORDER BY saleDate";
		 try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						DayAndSaleBean bean = new DayAndSaleBean();
						bean.setValue(row.get("totalPrice")!=null?Double.parseDouble(row.get("totalPrice").toString()):0);
						bean.setDate(row.get("date")!=null?row.get("date").toString():"");
						beanList.add(bean);
					}
				}
		 }catch(Exception e){
			 e.printStackTrace();
			 return beanList;
		 }
		return beanList;
	}

	private Double getAllCompaniesArrears() {
		int storeId=userSession.getStoreId();
		double result = 0;
		try {
			/*
			 * List<Map<String, Object>> rows = jdbcTemplate.
			 * queryForList("SELECT COALESCE(SUM(sd.totalDiscountAmount),0) AS totalDiscountAmount, "
			 * +
			 * "(SELECT COALESCE(SUM(t.rentalAmount),0) FROM `rental_tab` t )AS rentalAmount,"
			 * +
			 * "(SELECT COALESCE(SUM(ds.`adj_amount`),0) FROM `vendor_adjustment` ds ) AS vendorAmt,"
			 * +
			 * "(SELECT COALESCE(SUM(cm.totalAmount - cm.received ),0) FROM credit_master cm) AS crediAmt,"
			 * +
			 * "(SELECT COALESCE(SUM(dt.transactionAmt + dt.adjCheck),0) FROM discount_transaction dt WHERE dt.received=1 AND dt.transactionDate IS NOT NULL) AS transactionAmt FROM stock_lift_with_discount sd"
			 * );
			 */
			
			//StoreId appended query
            final List<Map<String, Object>> rows = (List<Map<String, Object>>)this.jdbcTemplate.queryForList("SELECT COALESCE(SUM(sd.totalDiscountAmount),0) AS totalDiscountAmount, (SELECT COALESCE(SUM(t.rentalAmount),0)"
            		+ " FROM `rental_tab` t where t.store_id=" + storeId + ") AS rentalAmount," + "(SELECT COALESCE(SUM(ds.`adj_amount`),0) "
            				+ "FROM `vendor_adjustment` ds where ds.store_id=" + storeId + ") AS vendorAmt," + "(SELECT COALESCE(SUM(dt.transactionAmt + dt.adjCheck),0)"
            						+ " FROM discount_transaction dt WHERE dt.received=1 AND dt.store_id ='"+storeId+"' AND dt.transactionDate IS NOT NULL) AS transactionAmt "
            						+ "FROM stock_lift_with_discount sd where sd.store_id=" + storeId + "");

			
			if (rows != null && rows.size() > 0) {
				for (Map row : rows) {
					result = (((row.get("totalDiscountAmount")!=null?Double.parseDouble(row.get("totalDiscountAmount").toString()):0)+(row.get("crediAmt")!=null?Double.parseDouble(row.get("crediAmt").toString()):0) +
							(row.get("rentalAmount")!=null?Double.parseDouble(row.get("rentalAmount").toString()):0)) - ((row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0) + (row.get("vendorAmt")!=null?Double.parseDouble(row.get("vendorAmt").toString()):0)));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return result;
		}
		return result;
	}
	@Override
	public List<DiscountAsPerCompanyBeen> downLoadCompanyDiscount(int company,String date) {
		List<DiscountAsPerCompanyBeen> discountBeanList = new ArrayList<DiscountAsPerCompanyBeen>();
		String QUERY="SELECT month,transactionAmt,adjCheck,DATE_FORMAT(transactionDate, '%d %b %Y') AS transactionDate,bank,transactionType,checkNo,fromBank,"
				+ "(SELECT SUM(rentalAmount) FROM rental_tab WHERE company = "+company+" AND rentalDate <='"+date+"' AND store_id = "+userSession.getStoreId()+") AS rental,"
				+ "(SELECT SUM(totalDiscountAmount) FROM stock_lift_with_discount WHERE companyName = "+company+" AND stockDiscountDate <='"+date+"' AND store_id = "+userSession.getStoreId()+") AS totalDiscountAmount FROM discount_transaction WHERE companyId="+company+" AND store_id = "+userSession.getStoreId()+" AND received=1 AND transactionDate IS NOT NULL "
				+ " AND (transactionAmt > 0 OR adjCheck != 0 ) ORDER BY MONTH DESC";
		//System.out.println(QUERY);     
		try{
		    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
					if(rows != null && rows.size()>0) {
						for(Map row : rows){
							DiscountAsPerCompanyBeen bean = new DiscountAsPerCompanyBeen();
							bean.setEnterprise(row.get("transactionDate")!=null?row.get("transactionDate").toString():"");
							bean.setChequeAmount(row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0);
							bean.setAdjustmentCheque(row.get("adjCheck")!=null?Double.parseDouble(row.get("adjCheck").toString()):0);
							bean.setCompanyName(row.get("fromBank")!=null?row.get("fromBank").toString():"");
							bean.setComment(row.get("transactionType")!=null?row.get("transactionType").toString():"");
							bean.setRentals(row.get("rental")!=null?Double.parseDouble(row.get("rental").toString()):0);
							bean.setDiscountAmount(row.get("totalDiscountAmount")!=null?Double.parseDouble(row.get("totalDiscountAmount").toString()):0);
							bean.setColor(row.get("checkNo")!=null?row.get("checkNo").toString():"");
							discountBeanList.add(bean);
						}
					}
		     }catch(Exception e){
		    	 e.printStackTrace();
		    	 return discountBeanList;
		     }
		
		return discountBeanList;
	}
	@Override
	public List<DiscountAndMonthBean> downloadDiscountCompanyWiseForAllMonth(String date, int company) {
		int storeId = userSession.getStoreId();
		 List<DiscountAndMonthBean>  discountAndMonthBeanList = new ArrayList<DiscountAndMonthBean>();
		ArrayList<String> arrlist = new ArrayList<String>();
		// String QUERY="SELECT DISTINCT saleDate FROM sale WHERE `saleDate` LIKE '%-01' AND saleDate <='"+date+"' ORDER BY saleDate DESC;";
		String QUERY = "SELECT DISTINCT DATE_FORMAT(saleDate ,'%Y-%m-01') AS saleDate FROM sale WHERE  saleDate <='"+date+"' AND store_id="+storeId+" ORDER BY saleDate DESC";
	     try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						arrlist.add(row.get("saleDate")!=null?row.get("saleDate").toString():"");
					}
				}
				if(arrlist.size() > 0){
					for (String saledate : arrlist) {
						double totalDisc=0;String dateVal=null;double discountVal=0;double totalSum=0;
						DiscountAndMonthBean discountAndMonthBean = new DiscountAndMonthBean();
						List<DiscountMonthWiseBean>   discountMonthWiseBeanList = new ArrayList<DiscountMonthWiseBean>();
					 List<Map<String, Object>> secrows = jdbcTemplate.queryForList("SELECT DISTINCT p.realBrandNo,p.matchName,p.packQty,p.matchs,p.packType,UPPER(DATE_FORMAT('"+saledate+"', '%b' ' %y')) AS date,"
					 		+ "(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,i.packQtyRate,"
					 		+ "(SELECT d.discountAmount FROM stock_lift_with_discount d WHERE d.brandNo=p.realBrandNo AND d.stockDiscountDate='"+saledate+"' AND d.store_id = "+storeId+") AS discountAmount,"
					 		+ "(SELECT d.adjustment FROM stock_lift_with_discount d WHERE d.brandNo=p.realBrandNo AND d.stockDiscountDate='"+saledate+"' AND d.store_id = "+storeId+") AS adjustment,"
					 		+ "(SELECT dt.totalDiscountAmount FROM stock_lift_with_discount dt WHERE dt.brandNo=p.realBrandNo AND dt.stockDiscountDate='"+saledate+"' AND dt.store_id = "+storeId+")  "
					 		+ " AS totalDiscountAmount, SUM(i.caseQty) AS caseQty,CEIL(SUM(i.receivedBottles/i.packQty)) AS totalcases,"
					 		+ " (SELECT SUM(dt.totalDiscountAmount) FROM stock_lift_with_discount dt WHERE dt.stockDiscountDate='"+saledate+"' AND dt.companyName="+company+" AND dt.store_id = "+storeId+")  "
					 		+ " AS discountSum  FROM product p LEFT JOIN invoice i  ON p.`brandNoPackQty`  = i.`brandNoPackQty`  WHERE p.`company`="+company+" AND"
					 		+ " i.`invoiceDateAsPerSheet`>='"+saledate+"' "
					 		+ " AND i.`invoiceDateAsPerSheet`<=(SELECT LAST_DAY('"+saledate+"' - INTERVAL 0 MONTH)) AND i.store_id = "+storeId+" GROUP BY p.realBrandNo");
					 for(Map secrow : secrows){
						 DiscountMonthWiseBean bean = new DiscountMonthWiseBean();
						  discountVal += secrow.get("discountAmount")!=null?Double.parseDouble(secrow.get("discountAmount").toString()):0;
						  totalSum = secrow.get("discountAmount")!=null?Double.parseDouble(secrow.get("discountAmount").toString()):0;
						  if(totalSum>0){
							  bean.setMatch(secrow.get("matchs")!=null?Long.parseLong(secrow.get("matchs").toString()):0);
						 bean.setBrand(secrow.get("matchName")!=null?secrow.get("matchName").toString():"");
						 bean.setCase((secrow.get("totalcases")!=null?Long.parseLong(secrow.get("totalcases").toString()):0)+(secrow.get("adjustment")!=null?Long.parseLong(secrow.get("adjustment").toString()):0));
						 bean.setDiscount(secrow.get("discountAmount")!=null?Double.parseDouble(secrow.get("discountAmount").toString()):0);
						 bean.setTotal(secrow.get("totalDiscountAmount")!=null?Double.parseDouble(secrow.get("totalDiscountAmount").toString()):0);
						 bean.setDate(saledate);
						 discountMonthWiseBeanList.add(bean);
						 totalDisc=secrow.get("discountSum")!=null?Double.parseDouble(secrow.get("discountSum").toString()):0;
						 dateVal=secrow.get("date")!=null?secrow.get("date").toString():"";
						 }
					 }
					 if(discountVal >0){
					 discountAndMonthBean.setDiscountMonthWiseBean(discountMonthWiseBeanList);
					 discountAndMonthBean.setMonth(dateVal);
					 discountAndMonthBean.setTotalDiscount(totalDisc);
					 discountAndMonthBeanList.add(discountAndMonthBean);
					 }
					
					}
				}
	     }catch(Exception e){
	    	 log.log(Level.WARN, "getDiscountCompanyWiseForAllMonth in PoizonSecondDaoImpl exception", e);
				e.printStackTrace();
				return discountAndMonthBeanList;
	     }
		return discountAndMonthBeanList;
	}
	@Override
	public List<MobileViewStockLiftingDiscount> discountReportForSelectedMonth(String startMonth, String endMonth) {
		
		int storeId=userSession.getStoreId();
		
		List<MobileViewStockLiftingDiscount> mobileViewStockLiftingDiscount = new ArrayList<MobileViewStockLiftingDiscount>();
		ArrayList<Integer> arrlist = new ArrayList<Integer>();
		 String QUERY="SELECT `companyId` FROM `company_tab`";
	     try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						arrlist.add(row.get("companyId")!=null?Integer.parseInt(row.get("companyId").toString()):-1);
					}
				}
				if(arrlist.size() > 0){
					for (int companyid : arrlist) {
						List<MobileViewStockDiscountInner> mobileViewStockDiscountInner = new ArrayList<MobileViewStockDiscountInner>();
						MobileViewStockLiftingDiscount mobileViewStockLiftingDiscountBean = new MobileViewStockLiftingDiscount();
						String cName=null,colorval=null;
						long companyOrder=0;
						double totalDiscountAmt=0,totalReceived=0,totalAdjustment=0,inc=0,arrears=0,vendorAmt=0,notreceived=0;
						
						List<Map<String, Object>> rowdata = jdbcTemplate.queryForList("SELECT DISTINCT c.stockDiscountDate FROM `stock_lift_with_discount` c"
								+ " WHERE c.stockDiscountDate >='"+startMonth+"' AND c.stockDiscountDate <='"+endMonth+"' AND c.store_id="+storeId+" ORDER BY c.stockDiscountDate DESC");
						
						if(rowdata != null && rowdata.size()>0) {
							for(Map row : rowdata){
								 MobileViewStockDiscountInner bean = new MobileViewStockDiscountInner();
								 List<Map<String, Object>> outerrows = jdbcTemplate.queryForList("SELECT COALESCE(SUM(d.`totalDiscountAmount`),0) AS discountamount,(SELECT t.`companyName` FROM `company_tab` t  WHERE t.`companyId`="+companyid+") AS company,"
											+ "(SELECT t.`companyOrder` FROM `company_tab` t  WHERE t.`companyId`="+companyid+") AS companyOrder,"
											+ "(SELECT t.`color` FROM `company_tab` t  WHERE t.`companyId`="+companyid+") AS color,"
											+ "UPPER(DATE_FORMAT('"+row.get("stockDiscountDate")+"', '%b' ' %y')) AS DATE,"
											+ "(SELECT COALESCE(SUM(ds.`rentalAmount`),0) FROM `rental_tab` ds WHERE ds.store_id="+storeId+" AND ds.rentalDate = '"+row.get("stockDiscountDate")+"' AND ds.`company`="+companyid+") AS rental,"
											+ "(SELECT COALESCE(SUM(ds.`adj_amount`),0) FROM `vendor_adjustment` ds WHERE ds.store_id="+storeId+" AND ds.vendor_month = '"+row.get("stockDiscountDate")+"' AND ds.`company`="+companyid+") AS vendorAmt,"
										    + "(SELECT vendor_comment FROM `vendor_adjustment` ds WHERE ds.store_id="+storeId+" AND ds.vendor_month = '"+row.get("stockDiscountDate")+"' AND ds.`company`="+companyid+") AS vendorComment,"
											+ "((SELECT COALESCE(SUM(ds.`transactionAmt`),0) FROM `discount_transaction` ds WHERE ds.store_id="+storeId+" AND ds.month = '"+row.get("stockDiscountDate")+"' AND ds.`companyId`="+companyid+" AND ds.received=1 AND ds.transactionDate IS NOT NULL)"
											+ " +(SELECT COALESCE(SUM(ds.`adjCheck`),0) FROM `discount_transaction` ds WHERE ds.store_id="+storeId+" AND ds.month = '"+row.get("stockDiscountDate")+"' AND ds.`companyId`="+companyid+" AND ds.received=1 AND ds.transactionDate IS NOT NULL)) "
											+ "AS chequeAmount, (SELECT COALESCE(SUM(ds.`adjCheck`),0) FROM `discount_transaction` ds WHERE ds.store_id="+storeId+" AND ds.month = '"+row.get("stockDiscountDate")+"' AND ds.`companyId`="+companyid+" AND ds.received=1 AND ds.transactionDate IS NOT NULL)"
											+ " AS adjustmentCheque, (((SELECT COALESCE(SUM(totalDiscountAmount),0) FROM stock_lift_with_discount WHERE store_id="+storeId+" AND stockDiscountDate"
											+ " >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount` WHERE store_id="+storeId+"  ORDER BY stockDiscountDate ASC LIMIT 1) AND stockDiscountDate<='"+row.get("stockDiscountDate")+"' "
											+ "AND companyName="+companyid+")+(SELECT COALESCE(SUM(rentalAmount),0) FROM rental_tab  WHERE store_id="+storeId+" AND rentalDate >=(SELECT `stockDiscountDate`"
											+ " FROM `stock_lift_with_discount` WHERE store_id="+storeId+" ORDER BY stockDiscountDate ASC LIMIT 1) AND rentalDate<='"+row.get("stockDiscountDate")+"' AND company="+companyid+")+"
											+ "(SELECT COALESCE(SUM(cm.totalAmount - cm.received ),0) FROM credit_master cm WHERE cm.store_id="+storeId+" AND cm.masterDate >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount` WHERE store_id="+storeId+"  ORDER BY stockDiscountDate ASC LIMIT 1) AND cm.masterDate <= LAST_DAY('"+row.get("stockDiscountDate")+"') AND cm.company = "+companyid+"))-"
											+ " ((SELECT COALESCE(SUM(transactionAmt+adjCheck),0) FROM discount_transaction WHERE store_id="+storeId+" AND MONTH >=(SELECT `stockDiscountDate` FROM"
											+ " `stock_lift_with_discount` WHERE store_id="+storeId+"  ORDER BY stockDiscountDate ASC LIMIT 1) AND MONTH<='"+row.get("stockDiscountDate")+"' AND companyId="+companyid+" AND received=1 AND transactionDate IS NOT NULL)+"
											+ "(SELECT COALESCE(SUM(adj_amount),0) FROM vendor_adjustment  WHERE store_id="+storeId+" AND vendor_month >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount` WHERE store_id="+storeId+" "
											+ "ORDER BY stockDiscountDate ASC LIMIT 1) AND vendor_month<='"+row.get("stockDiscountDate")+"' AND company="+companyid+"))) AS arrears,"
										   + "(SELECT COALESCE(SUM(ds.`transactionAmt`),0) FROM `discount_transaction` ds WHERE ds.store_id="+storeId+" AND ds.`companyId`="+companyid+" AND ds.received=0 "
										   + "AND ds.transactionDate IS NULL) AS notreceived, "
										   + "(SELECT SUM(cm.totalAmount) FROM credit_master cm WHERE cm.store_id="+storeId+" AND cm.masterDate >='"+row.get("stockDiscountDate")+"' AND cm.masterDate <= LAST_DAY('"+row.get("stockDiscountDate")+"') AND cm.company = "+companyid+") AS credit "
											+ "FROM `stock_lift_with_discount` d WHERE d.store_id="+storeId+" AND d.`companyName`="+companyid+"   AND d.`stockDiscountDate` = '"+row.get("stockDiscountDate")+"'");
								 if(outerrows != null && outerrows.size()>0) {
										for(Map outerrow : outerrows){
									    bean.setDate(outerrow.get("date")!=null?outerrow.get("date").toString():"");
									    bean.setDiscount(outerrow.get("discountamount")!=null?Double.parseDouble(outerrow.get("discountamount").toString()):0);
									    bean.setRentals(outerrow.get("rental")!=null?Double.parseDouble(outerrow.get("rental").toString()):0);
									    bean.setVendorAmount(outerrow.get("vendorAmt")!=null?Double.parseDouble(outerrow.get("vendorAmt").toString()):0);
									    bean.setComment(outerrow.get("vendorComment")!=null?outerrow.get("vendorComment").toString():"");
									    bean.setRecieved(outerrow.get("chequeAmount")!=null?Double.parseDouble(outerrow.get("chequeAmount").toString()):0);
									    bean.setCompanyId(companyid);
									    bean.setRealDate(row.get("stockDiscountDate")!=null?row.get("stockDiscountDate").toString():"");
									    bean.setDiscountAsPerCompanyBeenList(getDiscountAsPerCompanyWise(row.get("stockDiscountDate")!=null?row.get("stockDiscountDate").toString():"",companyid));
									    bean.setArrears(outerrow.get("arrears")!=null?Double.parseDouble(outerrow.get("arrears").toString()):0);
									    bean.setCreditAmt(outerrow.get("credit")!=null?Double.parseDouble(outerrow.get("credit").toString()):0);
                                        mobileViewStockDiscountInner.add(bean);
                                        if(inc == 0)
                                        	arrears = outerrow.get("arrears")!=null?Double.parseDouble(outerrow.get("arrears").toString()):0;
                                        totalDiscountAmt=totalDiscountAmt+(outerrow.get("discountamount")!=null?Double.parseDouble(outerrow.get("discountamount").toString()):0)+(outerrow.get("rental")!=null?Double.parseDouble(outerrow.get("rental").toString()):0);
                                        totalReceived=totalReceived+(outerrow.get("chequeAmount")!=null?Double.parseDouble(outerrow.get("chequeAmount").toString()):0);
                                        cName=outerrow.get("company")!=null?outerrow.get("company").toString():""; 
                                        companyOrder=outerrow.get("companyOrder")!=null?Long.parseLong(outerrow.get("companyOrder").toString()):0;
                                        colorval=outerrow.get("color")!=null?outerrow.get("color").toString():"";
                                        vendorAmt += outerrow.get("vendorAmt")!=null?Double.parseDouble(outerrow.get("vendorAmt").toString()):0;
                                        notreceived += outerrow.get("notreceived")!=null?Double.parseDouble(outerrow.get("notreceived").toString()):0;
										inc++;
										}
									}
							}
						}
						mobileViewStockLiftingDiscountBean.setCompany(cName);
						mobileViewStockLiftingDiscountBean.setCompanyOrder(companyOrder);
						mobileViewStockLiftingDiscountBean.setColor(colorval);
						mobileViewStockLiftingDiscountBean.setDuesAmt(arrears);
						mobileViewStockLiftingDiscountBean.setTotalDiscount(totalDiscountAmt);
						mobileViewStockLiftingDiscountBean.setMobileViewStockDiscountInner(mobileViewStockDiscountInner);
						mobileViewStockLiftingDiscount.add(mobileViewStockLiftingDiscountBean);
						mobileViewStockLiftingDiscountBean.setTotalVendorAmount(vendorAmt);
						mobileViewStockLiftingDiscountBean.setNotReceived(notreceived);
						mobileViewStockLiftingDiscountBean.setNotClearCheckBean(getNotReceivedChecks(companyid));
					}
					
				}
	     }catch(Exception e){
	    	 e.printStackTrace();
	    	 return mobileViewStockLiftingDiscount;
	    	 }
		return mobileViewStockLiftingDiscount;
	}
	private List<NotClearCheckBean> getNotReceivedChecks(int companyid) {
		
		int storeId=userSession.getStoreId();
		
     String QUERY = "SELECT ds.`transactionAmt`,ds.`checkNo`,UPPER(DATE_FORMAT(ds.`month`, '%b' ' %y')) AS date,ds.`month` FROM `discount_transaction` ds"
     		+ " WHERE  ds.store_id="+storeId+" AND ds.`companyId`=? AND ds.received=0 AND ds.transactionDate IS NULL";
     List<NotClearCheckBean> listBean= new ArrayList<NotClearCheckBean>();
       try{
    	   List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY,new Object[]{companyid});
    	   if(rows != null && rows.size()>0) {
				for(Map row : rows){
					NotClearCheckBean bean = new NotClearCheckBean();
					bean.setAmount(row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0);
					bean.setCheckNo(row.get("checkNo")!=null?row.get("checkNo").toString():"");
					bean.setMonth(row.get("date")!=null?row.get("date").toString():"");
					bean.setRealDate(row.get("month")!=null?row.get("month").toString():"");
					listBean.add(bean);
				}
    	   }
       }catch(Exception e){
    	   e.printStackTrace();
    	   return listBean;
       }
		return listBean;
	}
	private List<DiscountTransactionBean> getDiscountAsPerCompanyWise(String stockDate, int company) {
		
		int storeId=userSession.getStoreId();
		List<DiscountTransactionBean> discountAsPerCompanyBeenList = new ArrayList<DiscountTransactionBean>();
		String QUERY="SELECT dt.companyId,dt.month,dt.transactionType,dt.transactionAmt,UPPER(DATE_FORMAT(dt.transactionDate, '%d ' '%b ' '%Y')) AS transactionDate,dt.adjCheck,dt.imageName,dt.imagePath,dt.bank,"
				+ "(SELECT r.rentalAmount FROM rental_tab r WHERE dt.companyId = r.company AND dt.month=r.rentalDate AND r.store_id="+storeId+") AS rentalAmount"
				+ " FROM discount_transaction dt"
				+ " WHERE dt.month='"+stockDate+"' AND dt.companyId="+company+" AND dt.received=1 AND dt.store_id="+storeId+" AND dt.transactionDate IS NOT NULL";
         
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					DiscountTransactionBean bean = new DiscountTransactionBean();
					bean.setCompanyId(row.get("companyId")!=null?Long.parseLong(row.get("companyId").toString()):0);
					bean.setTransactionType(row.get("transactionType")!=null?row.get("transactionType").toString():"");
					bean.setTransactionAmt(row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0);
					bean.setAdjCheck(row.get("adjCheck")!=null?Double.parseDouble(row.get("adjCheck").toString()):0);
					bean.setImageName(row.get("imageName")!=null?row.get("imageName").toString():"");
					bean.setNgalleryImage(row.get("imagePath")!=null?row.get("imagePath").toString():"");
					bean.setRental(row.get("rentalAmount")!=null?Double.parseDouble(row.get("rentalAmount").toString()):0);
					bean.setTransactionDate(row.get("transactionDate")!=null?row.get("transactionDate").toString():"");
					bean.setBank(row.get("bank")!=null?row.get("bank").toString():"");
					discountAsPerCompanyBeenList.add(bean);
				}
			}
		}catch(Exception e){
			log.log(Level.WARN, "getDiscountAsPerCompanyWise private method in PoizonMobileUiDaoImpl exception", e);
			e.printStackTrace();
			return discountAsPerCompanyBeenList;
		}
		return discountAsPerCompanyBeenList;
	}
	@Override
	public List<ExpenseCategoryAndDate> getExpenseDataRangeWise(String startDate, String endDate) {
		List<ExpenseCategoryAndDate> beanList = new ArrayList<ExpenseCategoryAndDate>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT e.`categoryId`,e.`expenseName` FROM `expense_category` e ORDER BY e.`categoryId`");
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					double totalAmount=0;
					ExpenseCategoryAndDate bean = new ExpenseCategoryAndDate();
					List<ExpenseCategoryBean> secBeanList = new ArrayList<ExpenseCategoryBean>();
					bean.setEdate(row.get("expenseName")!=null?row.get("expenseName").toString():"");
					long categoryid = row.get("categoryId")!=null?Long.parseLong(row.get("categoryId").toString()):0;
					List<Map<String, Object>> innerRows = jdbcTemplate.queryForList("SELECT DATE_FORMAT(m.`expenseMasterDate`, '%d-%b-%Y') AS expenseMasterDate, c.`comment`,c.`expenseChildAmount`,t.`expenseName`"
							+ " FROM `expense_master` m INNER JOIN `expense_child` c ON m.`expenseMasterId`=c.`expenseMasterId` "
							+ "INNER JOIN `expense_category` t ON c.`categoryId`= t.`categoryId` "
							+ "WHERE m.`expenseMasterDate` >='"+startDate+"' AND m.`expenseMasterDate` <='"+endDate+"' AND c.`categoryId`="+categoryid+" AND m.`store_id`="+userSession.getStoreId()+""
									+ " GROUP BY m.`expenseMasterDate` ORDER BY m.`expenseMasterDate` DESC");
					if(innerRows != null && innerRows.size()>0) {
						for(Map innerRow : innerRows){
							ExpenseCategoryBean secBean = new ExpenseCategoryBean();
							secBean.setComment(innerRow.get("comment")!=null?innerRow.get("comment").toString():"");
							secBean.setExpenseDate(innerRow.get("expenseMasterDate")!=null?innerRow.get("expenseMasterDate").toString():"");
							secBean.setExpenseAmount(innerRow.get("expenseChildAmount")!=null?Double.parseDouble(innerRow.get("expenseChildAmount").toString()):0);
							secBeanList.add(secBean);
							totalAmount += innerRow.get("expenseChildAmount")!=null?Double.parseDouble(innerRow.get("expenseChildAmount").toString()):0;
						}
					}
				 bean.setExpenseCategoryBean(secBeanList);
                 bean.setTotal(totalAmount);
                 beanList.add(bean);
				}
			}
		}catch(Exception e){
			log.log(Level.WARN, "getExpenseDataRangeWise in PoizinThirdDaoImpl exception", e);
			e.printStackTrace();
			return beanList;
		}
		return beanList;
	}
	@Override
	public List<DiscountAsPerCompanyBeen> getHomePageDiscountDetails(int comapnyId) {
		String QUERY = "SELECT c.stockDiscountDate FROM `stock_lift_with_discount` c UNION SELECT MONTH FROM discount_transaction r ORDER BY stockDiscountDate DESC";
		List<DiscountAsPerCompanyBeen> beanList = new ArrayList<DiscountAsPerCompanyBeen>();
		try {
			List<Map<String, Object>> daterows = jdbcTemplate.queryForList(QUERY);
			if (daterows != null && daterows.size() > 0) {
				for (Map daterow : daterows) {
					List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT COALESCE(SUM(d.`totalDiscountAmount`),0) AS discountamount,"
							+ "(SELECT t.`companyName` FROM `company_tab` t  WHERE t.`companyId`="+comapnyId+") AS company,"
							+ "UPPER(DATE_FORMAT('"+daterow.get("stockDiscountDate")+"', '%b' ' %y')) AS date,(SELECT COALESCE(SUM(ds.`rentalAmount`),0) FROM `rental_tab` ds WHERE"
							+ " ds.rentalDate = '"+daterow.get("stockDiscountDate")+"' AND ds.`company`="+comapnyId+") AS rental,((SELECT COALESCE(SUM(ds.`transactionAmt`),0) FROM `discount_transaction` "
							+ "ds WHERE ds.month = '"+daterow.get("stockDiscountDate")+"' AND ds.`companyId`="+comapnyId+" AND ds.received=1 AND ds.transactionDate IS NOT NULL) +(SELECT COALESCE(SUM(ds.`adjCheck`),0) FROM `discount_transaction` ds WHERE "
							+ "ds.month = '"+daterow.get("stockDiscountDate")+"' AND ds.`companyId`="+comapnyId+" AND ds.received=1 AND ds.transactionDate IS NOT NULL)) AS chequeAmount, (SELECT COALESCE(SUM(ds.`adjCheck`),0) FROM `discount_transaction` ds "
							+ "WHERE ds.month = '"+daterow.get("stockDiscountDate")+"' AND ds.`companyId`="+comapnyId+") AS adjustmentCheque,"
							+ "(SELECT COALESCE(SUM(cm.totalAmount),0) "
							+ "FROM credit_master cm WHERE cm.masterDate >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount`  ORDER BY stockDiscountDate ASC LIMIT 1)"
							+ " AND cm.masterDate <= LAST_DAY('"+daterow.get("stockDiscountDate")+"') AND cm.company = "+comapnyId+") as creditAmt, (((SELECT COALESCE(SUM(totalDiscountAmount),0) "
							+ "FROM stock_lift_with_discount WHERE stockDiscountDate >=(SELECT `stockDiscountDate`  FROM `stock_lift_with_discount`  ORDER BY "
							+ "stockDiscountDate ASC LIMIT 1) AND stockDiscountDate<='"+daterow.get("stockDiscountDate")+"' AND companyName="+comapnyId+")+ (SELECT COALESCE(SUM(rentalAmount),0) "
							+ "FROM rental_tab  WHERE rentalDate >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount`  ORDER BY stockDiscountDate ASC LIMIT 1) "
							+ "AND rentalDate<='"+daterow.get("stockDiscountDate")+"' AND company="+comapnyId+") + (SELECT COALESCE(SUM(cm.totalAmount - cm.received ),0) "
							+ "FROM credit_master cm WHERE cm.masterDate >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount`  ORDER BY stockDiscountDate ASC LIMIT 1)"
							+ " AND cm.masterDate <= LAST_DAY('"+daterow.get("stockDiscountDate")+"') AND cm.company = "+comapnyId+")) - ((SELECT COALESCE(SUM(transactionAmt+adjCheck),0) FROM discount_transaction WHERE"
							+ " MONTH >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount`  ORDER BY stockDiscountDate ASC LIMIT 1) "
							+ "AND MONTH<='"+daterow.get("stockDiscountDate")+"' AND companyId="+comapnyId+" AND received=1 AND transactionDate IS NOT NULL) + (SELECT COALESCE(SUM(adj_amount),0) "
									+ " FROM vendor_adjustment  WHERE vendor_month >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount`  ORDER BY stockDiscountDate ASC LIMIT 1) "
									+ "AND vendor_month<='"+daterow.get("stockDiscountDate")+"' AND company="+comapnyId+"))) AS arrears FROM `stock_lift_with_discount` d WHERE d.`companyName`="+comapnyId+" "
							+ "AND d.`stockDiscountDate` = '"+daterow.get("stockDiscountDate")+"'");
					
					if (rows != null && rows.size() > 0) {
						for (Map row : rows) {
							DiscountAsPerCompanyBeen bean = new DiscountAsPerCompanyBeen();
							bean.setComment(row.get("date")!=null?row.get("date").toString():"");
							bean.setDiscountAmount(row.get("discountamount")!=null?Double.parseDouble(row.get("discountamount").toString()):0);
							bean.setChequeAmount(row.get("chequeAmount")!=null?Double.parseDouble(row.get("chequeAmount").toString()):0);
							bean.setRentals(row.get("rental")!=null?Double.parseDouble(row.get("rental").toString()):0);
							bean.setArrears(row.get("arrears")!=null?Double.parseDouble(row.get("arrears").toString()):0);
							bean.setEnterprise(daterow.get("stockDiscountDate")!=null?daterow.get("stockDiscountDate").toString():"");
							bean.setCreditAmt(row.get("creditAmt")!=null?Double.parseDouble(row.get("creditAmt").toString()):0);
							beanList.add(bean);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return beanList;
		}
		return beanList;
	}
	@Override
	public List<DiscountAsPerCompanyBeen> generateCompanyDiscount(int company, String startMonth, String endMonth) {
		List<DiscountAsPerCompanyBeen> discountBeanList = new ArrayList<DiscountAsPerCompanyBeen>();
		String QUERY="SELECT MONTH,transactionAmt,adjCheck,DATE_FORMAT(transactionDate, '%d %b %Y') AS transactionDate,bank,transactionType,checkNo,fromBank,"
				+ "(SELECT SUM(rentalAmount) FROM rental_tab WHERE company = "+company+" AND rentalDate >='"+startMonth+"' AND rentalDate <='"+endMonth+"') AS rental,"
				+ "(SELECT SUM(totalDiscountAmount) FROM stock_lift_with_discount WHERE companyName = "+company+" AND stockDiscountDate >='"+startMonth+"'"
				+ " AND stockDiscountDate <='"+endMonth+"') AS totalDiscountAmount FROM discount_transaction WHERE companyId="+company+" AND (transactionAmt > 0 OR adjCheck != 0 ) "
				+ "AND MONTH >='"+startMonth+"' AND MONTH <='"+endMonth+"' AND received=1 AND transactionDate IS NOT NULL ORDER BY MONTH DESC";
		//System.out.println(QUERY);
		     try{
		    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
					if(rows != null && rows.size()>0) {
						for(Map row : rows){
							DiscountAsPerCompanyBeen bean = new DiscountAsPerCompanyBeen();
							bean.setEnterprise(row.get("transactionDate")!=null?row.get("transactionDate").toString():"");
							bean.setChequeAmount(row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0);
							bean.setAdjustmentCheque(row.get("adjCheck")!=null?Double.parseDouble(row.get("adjCheck").toString()):0);
							bean.setCompanyName(row.get("fromBank")!=null?row.get("fromBank").toString():"");
							bean.setComment(row.get("transactionType")!=null?row.get("transactionType").toString():"");
							bean.setRentals(row.get("rental")!=null?Double.parseDouble(row.get("rental").toString()):0);
							bean.setDiscountAmount(row.get("totalDiscountAmount")!=null?Double.parseDouble(row.get("totalDiscountAmount").toString()):0);
							bean.setColor(row.get("checkNo")!=null?row.get("checkNo").toString():"");
							discountBeanList.add(bean);
						}
					}
		     }catch(Exception e){
		    	 e.printStackTrace();
		    	 return discountBeanList;
		     }
		
		return discountBeanList;
	}
	@Override
	public CardCashSaleBeen getTotalDiscountAndRental(int company, String startMonth, String endMonth) {
		CardCashSaleBeen bean =new CardCashSaleBeen();
            try{
            	bean.setCardSale(jdbcTemplate.queryForObject(" SELECT COALESCE(SUM(rentalAmount),0) FROM rental_tab WHERE company = "+company+" AND "
            			+ "rentalDate >='"+startMonth+"' AND rentalDate <='"+endMonth+"'", Double.class));
            	bean.setCashSale(jdbcTemplate.queryForObject("SELECT COALESCE(SUM(totalDiscountAmount),0) FROM stock_lift_with_discount WHERE "
            			+ "companyName = "+company+" AND stockDiscountDate >='"+startMonth+"' AND stockDiscountDate <='"+endMonth+"'", Double.class));
            }catch(Exception e){
            	e.printStackTrace();
            	return bean;
            }

		
		return bean;
	}
	@Override
	public List<TotalPriceDateWiseBean> getSaleCanvasPieChart(String startDate, String endDate) {
		List<TotalPriceDateWiseBean> beanList = new ArrayList<TotalPriceDateWiseBean>();
		String QUERY = "SELECT ROUND(SUM(s.`sale`/p.packQty),1 )AS sale,p.category,"
				+ "(SELECT t.categoryName FROM `category_tab` t WHERE t.categoryId = p.`category`) AS categoryName,"
				+ "SUM(s.totalPrice) AS amount FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND s.`saleDate` >='"+startDate+"' "
				+ "AND s.`saleDate` <='"+endDate+"' AND s.store_id = "+userSession.getStoreId()+" GROUP BY p.`category` ORDER BY p.`category`";
		try {
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if (rows != null && rows.size() > 0) {
				for (Map row : rows) {
					TotalPriceDateWiseBean bean = new TotalPriceDateWiseBean();
					// String str = (row.get("categoryName").toString() +
					// "-"+(row.get("saleInCase").toString())+"C");
					bean.setValue(row.get("amount") != null ? Double.parseDouble(row.get("amount").toString()) : 0);
					bean.setLabel(row.get("categoryName") != null ? row.get("categoryName").toString() : "");
					bean.setSaleInCase(row.get("sale") != null ? Double.parseDouble(row.get("sale").toString()) : 0);
					// bean.setLabel(str);
					bean.setDashboardSaleBeanList(getSplitForACategory(
							row.get("category") != null ? Long.parseLong(row.get("category").toString()) : 0, startDate,
							endDate));
					beanList.add(bean);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return beanList;
		}
	   return beanList;
	}
	private List<DashboardSaleBean> getSplitForACategory(long category, String startDate, String endDate) {
		List<DashboardSaleBean> beanList = new ArrayList<DashboardSaleBean>();
		String QUERY = "SELECT p.`shortBrandName`,p.`realBrandNo`,ROUND(SUM(s.`sale`/p.packQty),1 ) AS sale,SUM(s.`totalPrice`) AS amount FROM product p "
				+ "INNER JOIN sale s ON p.`brandNoPackQty`=s.`brandNoPackQty` WHERE p.`category`="+category+" AND s.`saleDate` >='"+startDate+"' AND "
				+ "s.`saleDate` <='"+endDate+"' AND p.`active` AND s.`store_id`="+userSession.getStoreId()+" GROUP BY p.`realBrandNo` ORDER BY p.`realBrandNo`";
		try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						DashboardSaleBean bean = new DashboardSaleBean();
						bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
						bean.setShortName(row.get("shortBrandName").toString());
						bean.setSaleInCase(row.get("sale")!=null?Double.parseDouble(row.get("sale").toString()):0);
						bean.setAmount(row.get("amount")!=null?Double.parseDouble(row.get("amount").toString()):0);
						if((row.get("sale")!=null?Double.parseDouble(row.get("sale").toString()):0) > 0)
						beanList.add(bean);
					}
				}
		 }catch(Exception e){
			 e.printStackTrace();
			 return beanList;
		 }
		return beanList;
	}
	@Override
	public VendorAdjustmentGetArrearsBean getExistingSingleCompanyArrears(String discountMonth, String company) {
		int companyId = -1,storeId = userSession.getStoreId();
		try{
			companyId = jdbcTemplate.queryForObject("SELECT companyId FROM company_tab WHERE companyName = ?", new Object[]{company},Integer.class);
		}catch(Exception e){
			log.error("Error In PoizinThirdDaoImpl getExistingSingleCompanyArrears(--)"+e);
			e.printStackTrace();
		}
		VendorAdjustmentGetArrearsBean bean = new VendorAdjustmentGetArrearsBean();
      String QUERY ="SELECT (SELECT t.`companyName` FROM `company_tab` t  WHERE t.`companyId`="+companyId+") AS company,"
      		+ "(SELECT COALESCE(SUM(ds.`adj_amount`),0) FROM `vendor_adjustment` ds WHERE ds.vendor_month = '"+discountMonth+"' AND ds.`company`="+companyId+" AND ds.store_id="+storeId+") AS vendorAmt,"
      		+ "((COALESCE(SUM(d.`totalDiscountAmount`),0)  + (SELECT COALESCE(SUM(rentalAmount),0) FROM rental_tab  WHERE "
      		+ " rentalDate<='"+discountMonth+"' AND company="+companyId+" AND store_id="+storeId+")) - ((SELECT COALESCE(SUM(transactionAmt+adjCheck),0)"
      		+ " FROM discount_transaction WHERE  MONTH <='"+discountMonth+"' AND companyId="+companyId+" AND store_id="+storeId+")  + (SELECT COALESCE(SUM(adj_amount),0) FROM vendor_adjustment  "
      		+ " WHERE vendor_month<='"+discountMonth+"' AND company="+companyId+" AND store_id="+storeId+"))) AS arrears FROM `stock_lift_with_discount` d WHERE d.`companyName`="+companyId+" AND store_id="+storeId+" "
      		+ "AND d.`stockDiscountDate` <= '"+discountMonth+"'";
     // System.out.println(QUERY);
      try{
    	  if(companyId > 0){
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						bean.setCompanyName(row.get("company")!=null?row.get("company").toString():"");
						bean.setVendorAmt(row.get("vendorAmt")!=null?Double.parseDouble(row.get("vendorAmt").toString()):0);
						bean.setArrears(row.get("arrears")!=null?Double.parseDouble(row.get("arrears").toString()):0);
					}
				}
    	  }
		 }catch(Exception e){
			 e.printStackTrace();
			 return bean;
		 }
		
		return bean;
	}
	@Override
	public RentalAndBreakageBean getExistingRentalForAMonth(String company, String month) {
		int companyId = -1;
		try{
			companyId = jdbcTemplate.queryForObject("SELECT companyId FROM company_tab WHERE companyName = ?", new Object[]{company},Integer.class);
		}catch(Exception e){
			log.error("Error In PoizinThirdDaoImpl getExistingRentalForAMonth(--)"+e);
			e.printStackTrace();
		}
		RentalAndBreakageBean bean = new RentalAndBreakageBean();
		log.info("PoizonThirdDaoImpl :: getExistingRentalForAMonth()");
		String QUERY = "SELECT rentalAmount,company,rentalDate FROM rental_tab WHERE `rentalDate`=? AND company = ? AND store_id =? ";
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY, new Object[]{month,companyId,userSession.getStoreId()});
			if(rows != null && rows.size() >0){
				for(Map row : rows){
					bean.setAmount(row.get("rentalAmount")!=null?Double.parseDouble(row.get("rentalAmount").toString()):0);
					bean.setCompany(row.get("company")!=null?Integer.parseInt(row.get("company").toString()):0);
					bean.setMonth(row.get("rentalDate")!=null?row.get("rentalDate").toString():"");
				}
				
			}
		}catch(Exception e){
			log.error("Error in PoizonThirdDaoImpl :: getExistingRentalForAMonth() "+e);
			e.printStackTrace();
			return bean;
		}
		return bean;
	}
	@Override
	public String saveOrUpdateRentalForAMonth(RentalAndBreakageBean bean, String companyId) {
     log.info("PoizonThirdDaoImpl :: saveOrUpdateRentalForAMonth()");
     int company = -1;
		try{
			company = jdbcTemplate.queryForObject("SELECT companyId FROM company_tab WHERE companyName = ?", new Object[]{companyId},Integer.class);
		}catch(Exception e){
			log.error("Error In PoizinThirdDaoImpl getExistingSingleCompanyArrears(--)"+e);
			e.printStackTrace();
		}
     String result = null;
     try{
    	 if(company > 0){
     	 List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT rentalAmount,company,rentalDate FROM rental_tab WHERE `rentalDate`=? AND company = ? AND store_id = ?", new Object[]{bean.getMonth(),company,userSession.getStoreId()});
     	 if(rows !=null && rows.size() > 0){
     		 int val = jdbcTemplate.update("UPDATE rental_tab SET rentalAmount = ? WHERE rentalDate=? AND company=? AND store_id = ?",new Object[]{bean.getAmount(),bean.getMonth(),company,userSession.getStoreId()});
     	   if(val > 0)
     		   result = "Rental Updated Successfully";
     	 }else{
     		 int val = jdbcTemplate.update("INSERT INTO `rental_tab` (company,rentalAmount,rentalDate,store_id) VALUES(?,?,?,?)",new Object[]{company,bean.getAmount(),bean.getMonth(),userSession.getStoreId()});
     		 if(val > 0)
     			 result = "Rental Saved Successfully";
     	 }
    	}
     }catch(Exception e){
    	 log.error("Error in PoizonThirdDaoImpl :: saveOrUpdateRentalForAMonth() "+e);
     	e.printStackTrace();
     	result =  "Something went wrong";
     }
		return result;
	}
	@Override
	public RentalAndBreakageBean getExistingBreakageForAMonth(String month) {
		log.info("PoizonThirdDaoImpl :: getExistingBreakageForAMonth()");
		RentalAndBreakageBean bean = new RentalAndBreakageBean();
		String QUERY = "SELECT breckageAmount,breckageDate,breckageComment FROM breckage_tab WHERE breckageDate=? AND store_id= ?";
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY, new Object[]{month,userSession.getStoreId()});
			if(rows != null && rows.size() > 0){
				for(Map row : rows){
					bean.setAmount(row.get("breckageAmount")!=null?Double.parseDouble(row.get("breckageAmount").toString()):0);
					bean.setMonth(row.get("breckageDate")!=null?row.get("breckageDate").toString():"");
					bean.setComment(row.get("breckageComment")!=null?row.get("breckageComment").toString():"");
				}
			}
		}catch(Exception e){
			log.error("Error in PoizonThirdDaoImpl :: getExistingBreakageForAMonth() "+e);
			e.printStackTrace();
			return bean;
		}
		return bean;
	}
	@Override
	public String saveOrUpdateBreakageForAMonth(RentalAndBreakageBean bean) {
	     log.info("PoizonThirdDaoImpl :: saveOrUpdateBreakageForAMonth()");
	     String result = null;
	     try{
	     	 List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT breckageAmount,breckageDate,breckageComment FROM breckage_tab WHERE `breckageDate`=? AND store_id=?", new Object[]{bean.getMonth(),userSession.getStoreId()});
	     	 if(rows !=null && rows.size() > 0){
	     		 int val = jdbcTemplate.update("UPDATE breckage_tab SET breckageAmount = ?, breckageComment = ? WHERE breckageDate=? AND store_id=?",new Object[]{bean.getAmount(),bean.getComment(),bean.getMonth(),userSession.getStoreId()});
	     	   if(val > 0)
	     		   result = "Breakage Updated Successfully";
	     	 }else{
	     		 int val = jdbcTemplate.update("INSERT INTO `breckage_tab` (breckageAmount,breckageDate,breckageComment,store_id) VALUES(?,?,?,?)",new Object[]{bean.getAmount(),bean.getMonth(),bean.getComment(),userSession.getStoreId()});
	     		 if(val > 0)
	     			 result = "Breakage Saved Successfully";
	     	 }
	     }catch(Exception e){
	    	 log.error("Error in PoizonThirdDaoImpl :: saveOrUpdateBreakageForAMonth() "+e);
	     	e.printStackTrace();
	     	result =  "Something went wrong";
	     }
			return result;
		}
	@Override
	public List<DiscountTransactionBean> getAllPendingChecks() {
		log.info("PoizinThirdDaoImpl :: getAllPendingChecks()");
		String QUERY = "SELECT d.transactionId,c.companyName, d.companyId, d.`month`,d.`transactionAmt`,d.bank,d.`checkNo`,d.`fromBank`,CASE WHEN d.`received`= 1 THEN 'true' ELSE 'false' END AS receivedbol "
				+ "FROM company_tab c INNER JOIN  discount_transaction d ON c.`companyId`= d.companyId WHERE d.received =0 AND d.store_id = "+userSession.getStoreId()+" AND d.transactionDate IS NULL ORDER BY d.month ASC";
		List<DiscountTransactionBean> beanList = new ArrayList<DiscountTransactionBean>();
		
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
		if(rows != null && rows.size() > 0){
			for(Map row : rows){
				DiscountTransactionBean bean = new DiscountTransactionBean();
				bean.setId(row.get("transactionId")!=null?Long.parseLong(row.get("transactionId").toString()):0);
				bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
				bean.setMonths(row.get("month")!=null?row.get("month").toString():"");
				bean.setTransactionAmt(row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0);
				bean.setCheckNo(row.get("checkNo")!=null?row.get("checkNo").toString():"");
				bean.setFromBank(row.get("fromBank")!=null?row.get("fromBank").toString():"");
				bean.setBank(row.get("bank")!=null?row.get("bank").toString():"");
				//bean.setReceived(row.get("receivedbol")!=null?Boolean.parseBoolean(row.get("receivedbol").toString()):false);
				bean.setReceived(true);// To enable check box.
				bean.setCompanyId(row.get("companyId")!=null?Long.parseLong(row.get("companyId").toString()):0);
				beanList.add(bean);
			}
		}
		
		return beanList;
	}
	/***
     * This method is used to get update transaction column and chnage received check once Check got deposited in bank
     * */
	@Override
	public String updateReceivedCheckStatus(int trid, int companyid, String month, String transactionDate, String clearedBank) {
		log.info("PoizinThirdDaoImpl :: updateReceivedCheckStatus(-)");
		String result = null;
		try{
			int val = jdbcTemplate.update("UPDATE discount_transaction SET  transactionDate=?, bank=?, received=? WHERE transactionId = ? AND companyId= ? AND month=? AND store_id=?", new Object[]{transactionDate, clearedBank, 1, trid, companyid,month,userSession.getStoreId()});
		    if(val > 0){
		    	result = "Updated successfully";
		    }else{
		    	result = "Something went wrong";
		    }
		}catch(Exception e){
			log.error("Error in updateReceivedCheckStatus:: "+e);
			e.printStackTrace();
			result = "Something went wrong";
		}
		return result;
	}
	
	/**
	 * This methos is used to get saved indent estimate to download as in pdf format
	 * */
	@Override
	public DateAndIndentBean downloadendentEstimateDiscountAsPdf(boolean flag) {
		log.info("PoizonThirdDaoImpl :: downloadendentEstimateDiscountAsPdf()");
		DateAndIndentBean dateAndIndentBean = new DateAndIndentBean();
		List<IndentEstimatePDF> listBean = new ArrayList<IndentEstimatePDF>();
		List<IndentEstimatePDF> listBeerBean = new ArrayList<IndentEstimatePDF>();
		long beercase=0,liquorcase=0,totalcase=0;double indentAmt=0,specialMrp=0,totalInvestment=0;
		String QUERY = "SELECT brandName,brandNo,productType,noOfCases,investMent,totalSpecialMrp,l2Val,l1Val,qVal,pVal,nVal,dVal,lbVal,sbVal,"
				+ "tinVal,DATE_FORMAT(date, '%b %d %Y') As date FROM temp_indent_estimate WHERE flag = ?"; 
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY, new Object[]{flag});
		if(rows != null && rows.size() > 0){
			for(Map row : rows){
				dateAndIndentBean.setDate(row.get("date")!=null?row.get("date").toString():"");
				IndentEstimatePDF bean = new IndentEstimatePDF();
				if((row.get("productType")!=null?row.get("productType").toString():"").contentEquals("LIQUOR")){
					bean.setIndentCase(row.get("noOfCases")!=null?row.get("noOfCases").toString():"");
					bean.setBrandNo(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0);
					bean.setName(row.get("brandName")!=null?row.get("brandName").toString():"");
					bean.setL2Case(row.get("l2Val")!=null?row.get("l2Val").toString():"");
					bean.setL1Case(row.get("l1Val")!=null?row.get("l1Val").toString():"");
					bean.setqCase(row.get("qVal")!=null?row.get("qVal").toString():"");
					bean.setpCase(row.get("pVal")!=null?row.get("pVal").toString():"");
					bean.setnCase(row.get("nVal")!=null?row.get("nVal").toString():"");
					bean.setdCase(row.get("dVal")!=null?row.get("dVal").toString():"");
					liquorcase += row.get("noOfCases")!=null?Long.parseLong(row.get("noOfCases").toString()):0;
					indentAmt += row.get("investMent")!=null?Double.parseDouble(row.get("investMent").toString()):0;
					specialMrp += row.get("totalSpecialMrp")!=null?Double.parseDouble(row.get("totalSpecialMrp").toString()):0;
					listBean.add(bean);
				}else{
					bean.setIndentCase(row.get("noOfCases")!=null?row.get("noOfCases").toString():"");
					bean.setBrandNo(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0);
					bean.setName(row.get("brandName")!=null?row.get("brandName").toString():"");
					bean.setLbCase(row.get("lbVal")!=null?row.get("lbVal").toString():"");
					bean.setSbCase(row.get("sbVal")!=null?row.get("sbVal").toString():"");
					bean.setTinCase(row.get("tinVal")!=null?row.get("tinVal").toString():"");
					beercase += row.get("noOfCases")!=null?Long.parseLong(row.get("noOfCases").toString()):0;
					indentAmt += row.get("investMent")!=null?Double.parseDouble(row.get("investMent").toString()):0;
					specialMrp += row.get("totalSpecialMrp")!=null?Double.parseDouble(row.get("totalSpecialMrp").toString()):0;
				    listBeerBean.add(bean);
				}
				
			}
		}
		dateAndIndentBean.setIndentEstimatePDF(listBean);
		dateAndIndentBean.setIndentEstimateBeerPDF(listBeerBean);
		dateAndIndentBean.setLiquorCases(liquorcase);
		dateAndIndentBean.setBeerCases(beercase);
		dateAndIndentBean.setTotalCase(liquorcase + beercase);
		dateAndIndentBean.setSpecialMrp(Math.round(specialMrp));
		dateAndIndentBean.setTcs(Math.round(indentAmt / 100));
		dateAndIndentBean.setIndentAmt(Math.round(indentAmt));
		dateAndIndentBean.setTotalInvestment(Math.round((indentAmt / 100) + specialMrp + indentAmt));
		
		
		return dateAndIndentBean;
	}
	/**
	 * This method is used to download discount pdf
	 * */
	@Override
	public TargetWithBudget downloadtargetanddiscpercase(String company, String date) {
		
		int storeId=userSession.getStoreId();
    log.info("PoizinThirdDaoImpl :: downloadtargetanddiscpercase()");
    int companyId = -1;double totalBudget=0;String month=null;
	try{
		companyId = jdbcTemplate.queryForObject("SELECT companyId FROM company_tab WHERE companyName = ?", new Object[]{company},Integer.class);
	}catch(Exception e){
		log.error("Error in PoizinThirdDaoImpl :: downloadtargetanddiscpercase() "+e);
		e.printStackTrace();
	}
	String QUERY = "SELECT DISTINCT p1.realBrandNo, p1.`shortBrandName`, ROUND(AVG((SELECT i.packQtyRate FROM invoice i "
			+ "WHERE i.brandNoPackQty=p1.brandNoPackQty AND i.invoiceDateAsPerSheet <= (SELECT LAST_DAY('"+date+"')) AND i.store_id="+storeId+"  ORDER BY `invoiceId` DESC LIMIT 1)),0) AS casePrice,(SELECT target FROM stock_lift_with_discount d "
			+ "WHERE d.brandNo=p1.realBrandNo AND stockDiscountDate='"+date+"' AND d.store_id="+storeId+") AS targetCase,"
			+ "(SELECT discountAmount FROM stock_lift_with_discount d WHERE d.brandNo=p1.realBrandNo AND stockDiscountDate='"+date+"' AND d.store_id="+storeId+") AS discountAmount,DATE_FORMAT('"+date+"', '%M %Y') AS date  FROM product"
			+ " p1 WHERE p1.`active`=1 AND p1.`company` = "+companyId+" AND (SELECT target FROM stock_lift_with_discount d "
			+ "WHERE d.brandNo=p1.realBrandNo AND stockDiscountDate='"+date+"' AND d.store_id="+storeId+") > 0 GROUP BY p1.realBrandNo";	
	
	TargetWithBudget targetWithBudget = new TargetWithBudget();
	List<DownloadTargetCase> targetCaseList = new ArrayList<DownloadTargetCase>();
	targetWithBudget.setCompany(company);
	   try{
		   List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
		   if(rows != null && rows.size()>0){
			   for(Map row : rows){
				   DownloadTargetCase bean = new DownloadTargetCase();
				   totalBudget +=((row.get("targetCase")!=null?Long.parseLong(row.get("targetCase").toString()):0) * (row.get("casePrice")!=null?Double.parseDouble(row.get("casePrice").toString()):0));
				   bean.setTargetCase(row.get("targetCase")!=null?Long.parseLong(row.get("targetCase").toString()):0);
				   bean.setDiscPerCase(row.get("discountAmount")!=null?Double.parseDouble(row.get("discountAmount").toString()):0);
				   bean.setBrand(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
				   month = row.get("date")!=null?row.get("date").toString():"";
				   targetCaseList.add(bean);
			   }
			   
		   }
		   targetWithBudget.setBudget(totalBudget);
		   targetWithBudget.setMonth(month);
		   targetWithBudget.setTargetCaseList(targetCaseList);
	   }catch(Exception e){
		   log.error("Error in PoizinThirdDaoImpl :: downloadtargetanddiscpercase() "+e);
		   e.printStackTrace();
		   return targetWithBudget;
	   }
	   return targetWithBudget;
	}
	
	
	
}
