package com.zambient.poizon.dao;

import java.util.List;

import com.zambient.poizon.bean.BalanceSheetBean;
import com.zambient.poizon.bean.CompareSaleBean;
import com.zambient.poizon.bean.DiscountCalculationBean;
import com.zambient.poizon.bean.SaleListBean;

public interface HybridDao {
	public String appUserLogin();
	public BalanceSheetBean getLatestDayDiff();
	public String getProductsForIndent();
	public SaleListBean generateSaleDataPdf(String saledate);
	public List<DiscountCalculationBean> getDiscountCalculationList(String month);
	public List<CompareSaleBean> getWeeklyOrMonthlySaleComparisionData(String startDate, String endDate, int id);

}
