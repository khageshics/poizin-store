package com.zambient.poizon.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.zambient.poizon.bean.BalanceSheetBean;
import com.zambient.poizon.bean.CompareSaleBean;
import com.zambient.poizon.bean.DiscountCalculationBean;
import com.zambient.poizon.bean.MobileViewInHouseStock;
import com.zambient.poizon.bean.SaleBean;
import com.zambient.poizon.bean.SaleListBean;
import com.zambient.poizon.utill.UserSession;

@PropertySource("classpath:/com/zambient/poizon/resources/poizon.properties")
@Repository("hybridDao")
public class HybridDaoImpl implements HybridDao{

	final static Logger log = Logger.getLogger(HybridDaoImpl.class);
    @Autowired
	private JdbcTemplate jdbcTemplate;
    @Autowired 
	private UserSession userSession;
    
	@Override
	public String appUserLogin() {
		String result="";
		try{
			result = jdbcTemplate.queryForObject("SELECT user_id FROM admin", String.class);
		}catch(Exception e){
			log.log(Level.WARN, "appUserLogin in HybridDaoImpl exception", e);
			e.printStackTrace();
		}
		return result;
	}
	@Override
	public BalanceSheetBean getLatestDayDiff() {
		BalanceSheetBean bean = new BalanceSheetBean();
		String QUERY= "SELECT SUM(s.`totalPrice`) AS totalPrice,"
				+ "(SELECT UPPER(DATE_FORMAT(creditDate, '%d-' '%b-' '%Y')) FROM bank_credit_tab ORDER BY creditDate DESC LIMIT 1)"
				+ " AS retentionDate,(SELECT creditAmount FROM bank_credit_tab WHERE creditDate = "
				+ "(SELECT creditDate FROM bank_credit_tab ORDER BY creditDate DESC LIMIT 1)) AS retention,"
				+ "(((SELECT `cardSale` FROM `card_cash_sale` "
				+ "WHERE `date`=s.saleDate)+(SELECT `cashSale` FROM `card_cash_sale` WHERE `date`=s.saleDate)+(SELECT `chequeSale` FROM "
				+ "`card_cash_sale` WHERE `date`=s.saleDate)+(SELECT SUM(`totalAmount`) FROM `expense_master` WHERE "
				+ "`expenseMasterDate`=s.saleDate))-(SUM(s.`totalPrice`))) AS diff  FROM sale s WHERE s.`saleDate`=(SELECT saleDate FROM sale ORDER BY saleDate DESC LIMIT 1)";
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
			for(Map row : rows){
				bean.setTotalAmount(row.get("totalPrice")!=null?Double.parseDouble(row.get("totalPrice").toString()):0);
				bean.setDate(row.get("retentionDate")!=null?row.get("retentionDate").toString():"");
				bean.setRetention(row.get("retention")!=null?Double.parseDouble(row.get("retention").toString()):0);
				bean.setTotalPrice(row.get("diff")!=null?Double.parseDouble(row.get("diff").toString()):0);
			}
		}
	}catch(Exception e){
		e.printStackTrace();
		return bean;
	}
	return bean;
	
	}
	
	@Override
	public String getProductsForIndent() {
		int days = 2;
		String startDate,endDate = null;
		String products ="";
		endDate = jdbcTemplate.queryForObject("SELECT saleDate FROM sale ORDER BY saleDate DESC LIMIT 1", String.class);
		startDate =   jdbcTemplate.queryForObject("SELECT DATE_ADD((SELECT saleDate FROM sale ORDER BY saleDate DESC LIMIT 1), INTERVAL -30 DAY) AS startDate", String.class); 
		String QUERY="SELECT DISTINCT p1.brandNo, p1.`shortBrandName`,(SELECT d.targetCase FROM discount d WHERE d.brandNo=p1.brandNo AND"
				+ " d.estimateDate=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+endDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))) AS commitment,"
				+ "(SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE   s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND "
				+ "p.brandNo=p1.brandNo  AND p.packType='2L') AS 2LStock,(SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE "
				+ " s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='1L') AS 1LStock,"
				+ "(SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE   s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty"
				+ " AND p.brandNo=p1.brandNo  AND p.packType='Q') AS QStock, (SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE "
				+ "s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='P') AS PStock,"
				+ "(SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE   s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty "
				+ "AND p.brandNo=p1.brandNo  AND p.packType='N') AS NStock, (SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE"
				+ " s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='D') AS DStock,"
				+ " (SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE   s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty "
				+ "AND p.brandNo=p1.brandNo  AND p.packType='SB') AS SBStock,(SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE "
				+ "s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='LB') AS LBStock, "
				+ "(SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE   s.saleDate ='"+endDate+"'  AND s.brandNoPackQty=p.brandNoPackQty "
				+ "AND p.brandNo=p1.brandNo  AND p.packType='TIN') AS TINStock, (SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE "
				+ "s.saleDate ='"+endDate+"'  AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='X') AS XStock,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"'"
				+ " AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='2L') AS 2L,(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) "
				+ "FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  "
				+ "AND p.packType='1L') AS 1L,(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND "
				+ "s.saleDate <='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo AND p.packType='Q') AS Q,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='P') AS P,(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) "
				+ "FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo "
				+ " AND p.packType='N') AS N,(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND "
				+ "s.saleDate <='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='D') AS D,(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='SB') AS SB,(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='LB') AS LB,(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='TIN') AS TIN,(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"'  AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='X') AS X FROM product p1 GROUP BY p1.brandNo ORDER BY p1.brandNo ASC";
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					double needCases = 0;
					if((row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0) > 0){
					    double value = Math.ceil((Math.round(((row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0)/30) * 100.0) / 100.0)*(days- Math.floor((row.get("PStock")!=null?Double.parseDouble(row.get("PStock").toString()):0)/(Math.round(((row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0)/30) * 100.0) / 100.0))));
					if(value > 0)
						needCases +=value;
					}
					if((row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0) > 0){
						double value = Math.ceil((Math.round(((row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0)/30) * 100.0) / 100.0)*(days- Math.floor((row.get("QStock")!=null?Double.parseDouble(row.get("QStock").toString()):0)/(Math.round(((row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0)/30) * 100.0) / 100.0))));
						if(value > 0)
							needCases +=value;
					}
					if((row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0) > 0){
						double value = Math.ceil((Math.round(((row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0)/30) * 100.0) / 100.0)*(days- Math.floor((row.get("2LStock")!=null?Double.parseDouble(row.get("2LStock").toString()):0)/(Math.round(((row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0)/30) * 100.0) / 100.0))));
						if(value > 0)
							needCases +=value;
					}
					if((row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0) > 0){
						double value = Math.ceil((Math.round(((row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0)/30) * 100.0) / 100.0)*(days- Math.floor((row.get("1LStock")!=null?Double.parseDouble(row.get("1LStock").toString()):0)/(Math.round(((row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0)/30) * 100.0) / 100.0))));
						if(value > 0)
							needCases +=value;
					}
					if((row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0) > 0){
						double value = Math.ceil((Math.round(((row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0)/30) * 100.0) / 100.0)*(days- Math.floor((row.get("NStock")!=null?Double.parseDouble(row.get("NStock").toString()):0)/(Math.round(((row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0)/30) * 100.0) / 100.0))));
						if(value > 0)
							needCases +=value;
					}
					if((row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0) > 0){
						double value = Math.ceil((Math.round(((row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0)/30) * 100.0) / 100.0)*(days- Math.floor((row.get("SBStock")!=null?Double.parseDouble(row.get("SBStock").toString()):0)/(Math.round(((row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0)/30) * 100.0) / 100.0))));
					if(value > 0)
						needCases +=value;
					}
					if((row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0) > 0){
						double value = Math.ceil((Math.round(((row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0)/30) * 100.0) / 100.0)*(days- Math.floor((row.get("DStock")!=null?Double.parseDouble(row.get("DStock").toString()):0)/(Math.round(((row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0)/30) * 100.0) / 100.0))));
						if(value > 0)
							needCases +=value;
					}
					if((row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0) > 0){
						double value = Math.ceil((Math.round(((row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0)/30) * 100.0) / 100.0)*(days- Math.floor((row.get("LBStock")!=null?Double.parseDouble(row.get("LBStock").toString()):0)/(Math.round(((row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0)/30) * 100.0) / 100.0))));
						if(value > 0)
							needCases +=value;
					}
					if((row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0) > 0){
						double value = Math.ceil((Math.round(((row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0)/30) * 100.0) / 100.0)*(days- Math.floor((row.get("TINStock")!=null?Double.parseDouble(row.get("TINStock").toString()):0)/(Math.round(((row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0)/30) * 100.0) / 100.0))));
						if(value > 0)
							needCases +=value;
					}
					if((row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0) > 0){
						double value = Math.ceil((Math.round(((row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0)/30) * 100.0) / 100.0)*(days- Math.floor((row.get("XStock")!=null?Double.parseDouble(row.get("XStock").toString()):0)/(Math.round(((row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0)/30) * 100.0) / 100.0))));
						if(value > 0)
							needCases +=value;
					}
					long brandNo = row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0;
					double commitment = row.get("commitment")!=null?Long.parseLong(row.get("commitment").toString()):0;
					double liftedCase = jdbcTemplate.queryForObject("SELECT COALESCE(ROUND(CEIL(SUM(s.receivedBottles/p.packQty))),0) AS liftedCase "
							+ "FROM product p INNER JOIN invoice s ON p.`brandNoPackQty` = s.`brandNoPackQty`"
							+ " AND `invoiceDateAsPerSheet`>=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+endDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01')) AND "
				             + " invoiceDateAsPerSheet <= (SELECT LAST_DAY('"+endDate+"')) AND p.brandNo="+brandNo+"", Double.class);
					String brand = row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"";
                  if(needCases > 0 || (commitment - liftedCase) > 0){
                	  products += brand+", ";
                  }
                 
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return products;
		}
		return products;
	}
	@Override
	public SaleListBean generateSaleDataPdf(String saleDate) {
    log.info("HybridDaoImpl:: generateSaleDataPdf()");
    
    SaleListBean saleListBean = new SaleListBean();
    
		String QUERY = "SELECT saleDate FROM sale ORDER BY `saleDate` DESC LIMIT 1";
		String QUERY2 = "SELECT p.`shortBrandName`,p.`realBrandNo`,p.`packType`,p.`packQty`,s.`opening`,s.`received`,s.`sale`,s.`closing`,s.`unitPrice`,s.`totalPrice` "
				+ "FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`=? AND (s.`sale` || s.`closing` ) > 0"
				+ " ORDER BY p.saleSheetOrder ASC, p.`shortBrandName`,p.`packType` DESC";
        try{
        	//String saleDate = jdbcTemplate.queryForObject(QUERY, String.class);
        	saleListBean.setDate(saleDate);
        	List<SaleBean> saleBean = new ArrayList<SaleBean>();
        	List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY2, new Object[]{saleDate});
        	if(rows != null && rows.size() >0){
        		for(Map row : rows){
        			SaleBean bean = new SaleBean();
        			bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
        			bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
        			bean.setPackType(row.get("packType")!=null?row.get("packType").toString():"");
        			bean.setPackQty(row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0);
        			bean.setOpening(row.get("opening")!=null?Long.parseLong(row.get("opening").toString()):0);
        			bean.setReceived(row.get("received")!=null?Long.parseLong(row.get("received").toString()):0);
        			bean.setUnitPrice(row.get("unitPrice")!=null?Double.parseDouble(row.get("unitPrice").toString()):0);
        			bean.setTotalSale(row.get("sale")!=null?Long.parseLong(row.get("sale").toString()):0);
        			bean.setClosing(row.get("closing")!=null?Long.parseLong(row.get("closing").toString()):0);
        			bean.setTotalPrice(row.get("totalPrice")!=null?Double.parseDouble(row.get("totalPrice").toString()):0);
        			saleBean.add(bean);
        		}
        		saleListBean.setSaleBean(saleBean);
        	}
        }catch(Exception e){
        	e.printStackTrace();
        	log.error("Error in HybridDaoImpl:: generateSaleDataPdf() "+e);
        	return saleListBean;
        }
		return saleListBean;
	}
	@Override
	public List<DiscountCalculationBean> getDiscountCalculationList(String month) {
		
		int storeId=userSession.getStoreId();
		List<DiscountCalculationBean> listBean = new ArrayList<DiscountCalculationBean>();
		
		String QUERY = "SELECT p.`shortBrandName`,p.`realBrandNo`,p.brandNoPackQty,p.`packType`,p.`packQty`,COALESCE(ROUND(CEIL(SUM(s.sale/p.packQty))),0) AS sale,p.category,"
				+ "(SELECT i.bottleSaleMrp FROM invoice i WHERE i.invoiceDateAsPerSheet <= (SELECT LAST_DAY('"+month+"')) AND i.brandNoPackQty = p.`brandNoPackQty` AND i.store_id="+storeId+" "
				+ " ORDER BY i.invoiceDateAsPerSheet DESC LIMIT 1) AS btlmrp,"
				+ " (SELECT i.packQtyRate FROM invoice i WHERE i.invoiceDateAsPerSheet <= (SELECT LAST_DAY('"+month+"')) AND i.brandNoPackQty = p.`brandNoPackQty` AND i.store_id="+storeId+" "
				+ " ORDER BY i.invoiceDateAsPerSheet DESC LIMIT 1) AS packQtyRate,"
				+ "  (SELECT i.specialMargin FROM invoice i WHERE i.invoiceDateAsPerSheet <= (SELECT LAST_DAY('"+month+"')) AND i.brandNoPackQty = p.`brandNoPackQty` AND i.store_id="+storeId+" "
				+ " ORDER BY i.invoiceDateAsPerSheet DESC LIMIT 1) AS specialMargin, (SELECT d.discountAmount FROM `stock_lift_with_discount` d WHERE "
				+ "d.stockDiscountDate <= '"+month+"' AND d.brandNo = p.`realBrandNo` AND d.store_id="+storeId+" ORDER BY d.stockDiscountDate DESC LIMIT 1) AS discount,"
				+ " (SELECT t.categoryName FROM `category_tab` t WHERE t.categoryId=p.`category`) AS categoryName FROM product p INNER JOIN sale s"
				+ " ON p.`brandNoPackQty` = s.`brandNoPackQty` AND s.store_id="+storeId+" AND s.saleDate >= (SELECT (SELECT s.saleDate FROM sale s "
				+ "WHERE s.saleDate <= (SELECT LAST_DAY('"+month+"')) AND s.store_id="+storeId+" ORDER BY s.saleDate DESC LIMIT 1) - INTERVAL 1 MONTH) AND s.`saleDate` <= (SELECT s.saleDate "
				+ "FROM sale s WHERE s.saleDate <= (SELECT LAST_DAY('"+month+"')) AND s.store_id="+storeId+" ORDER BY s.saleDate DESC LIMIT 1) AND p.`active`=1 GROUP BY p.`brandNoPackQty` "
				+ "ORDER BY p.category DESC ";
		try {
			
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if (rows != null && rows.size() > 0) {
				for (Map row : rows) {
					DiscountCalculationBean bean = new DiscountCalculationBean();
					bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
					bean.setProductName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
					bean.setPackType(row.get("packType")!=null?row.get("packType").toString():"");
					bean.setPackQty(row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0);
					bean.setSale(row.get("sale")!=null?Long.parseLong(row.get("sale").toString()):0);
					bean.setCategoryId(row.get("category")!=null?Long.parseLong(row.get("category").toString()):0);
					bean.setBtlMrp(row.get("btlmrp")!=null?Double.parseDouble(row.get("btlmrp").toString()):0);
					bean.setDiscInPer(row.get("discount")!=null?Double.parseDouble(row.get("discount").toString()):0);
					bean.setCategoryName(row.get("categoryName")!=null?row.get("categoryName").toString():"");
					bean.setCaseRate(row.get("packQtyRate")!=null?Double.parseDouble(row.get("packQtyRate").toString()):0);
					bean.setSpecialMargin(row.get("specialMargin")!=null?Double.parseDouble(row.get("specialMargin").toString()):0);
					bean.setBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0);
					listBean.add(bean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error("Error in getDiscountCalculationList "+e);
			return listBean;
		}
		return listBean;
	}
	@Override
	public List<CompareSaleBean> getWeeklyOrMonthlySaleComparisionData(String startDate, String endDate, int id) {
		int storeId = userSession.getStoreId();
		List<CompareSaleBean> beanList = new ArrayList<CompareSaleBean>();
		String QUERY = null,comment1=null,comment2=null;
		if(id==2) {
			QUERY = "SELECT p.`brandNoPackQty`,p.`shortBrandName`,p.`realBrandNo`,p.`quantity`,p.`packQty`,p.`packType`,p.`category`,p.`company`,"
					+ "(SELECT c.companyName FROM company_tab c WHERE c.companyId = p.`company`) AS companyName,"
					+ "(SELECT c.color FROM company_tab c WHERE c.companyId = p.`company`) AS companycolor,"
					+ "(SELECT c.categoryName FROM category_tab c WHERE c.categoryId = p.`category`) AS categoryName,"
					+ "(SELECT c.color FROM category_tab c WHERE c.categoryId = p.`category`) AS categorycolor,"
					+ "(SELECT c.companyOrder FROM company_tab c WHERE c.companyId = p.`company`) AS companyOrder,"
					+ "(SELECT c.categoryOrder FROM category_tab c WHERE c.categoryId = p.`category`) AS categoryOrder,SUM(s.`sale`) AS currentsale,SUM(s.`totalPrice`) AS currentprice,"
					+ "(SELECT SUM(s1.`sale`) FROM sale s1 WHERE s1.store_id="+storeId+" AND s1.saleDate >=(SELECT DATE_SUB('"+startDate+"', INTERVAL 1 WEEK)) AND "
					+ "s1.saleDate < '"+startDate+"' AND s1.brandNoPackQty = p.brandNoPackQty) AS previousSale,"
					+ "(SELECT SUM(s1.`totalPrice`) FROM sale s1 WHERE s1.store_id="+storeId+" AND s1.saleDate >=(SELECT DATE_SUB('"+startDate+"', INTERVAL 1 WEEK)) AND "
					+ "s1.saleDate < '"+startDate+"' AND s1.brandNoPackQty = p.brandNoPackQty) AS previousprice,"
					+ "DATE_FORMAT('"+startDate+"', '%d-%m-%Y') AS crurrentSatrt,"
					+ "DATE_FORMAT('"+endDate+"', '%d-%m-%Y') AS crurrentEnd,DATE_FORMAT((SELECT DATE_SUB('"+startDate+"', INTERVAL 1 WEEK)), '%d-%m-%Y') AS previousStart,"
					+ " DATE_FORMAT((SELECT DATE_SUB('"+startDate+"', INTERVAL 1 DAY)), '%d-%m-%Y') AS previousend FROM product p INNER JOIN sale s "
					+ "ON p.`brandNoPackQty` = s.`brandNoPackQty` AND s.store_id="+storeId+" AND s.`saleDate` >='"+startDate+"' AND s.`saleDate` <='"+endDate+"'"
					+ " GROUP BY p.`brandNoPackQty` HAVING (currentsale OR previousSale) > 0";
		}else {
			QUERY = "SELECT p.`brandNoPackQty`,p.`shortBrandName`,p.`realBrandNo`,p.`quantity`,p.`packQty`,p.`packType`,p.`category`,"
					+ "p.`company`,(SELECT c.companyName FROM company_tab c WHERE c.companyId = p.`company`) AS companyName,"
					+ "(SELECT c.color FROM company_tab c WHERE c.companyId = p.`company`) AS companycolor,(SELECT c.categoryName FROM "
					+ "category_tab c WHERE c.categoryId = p.`category`) AS categoryName,(SELECT c.color FROM category_tab c WHERE "
					+ "c.categoryId = p.`category`) AS categorycolor,(SELECT c.companyOrder FROM company_tab c WHERE c.companyId = p.`company`) AS companyOrder,"
					+ "(SELECT c.categoryOrder FROM category_tab c WHERE c.categoryId = p.`category`) AS categoryOrder,SUM(s.`sale`) AS currentsale,SUM(s.totalPrice) AS currentprice,"
					+ "(SELECT SUM(s1.`sale`) FROM sale s1 WHERE s1.store_id="+storeId+" AND s1.saleDate >=(SELECT DATE_SUB('"+startDate+"', INTERVAL 1 MONTH)) AND"
					+ " s1.saleDate < '"+startDate+"' AND s1.brandNoPackQty = p.brandNoPackQty) AS previousSale,"
					+ "(SELECT SUM(s1.`totalPrice`) FROM sale s1 WHERE s1.store_id="+storeId+" AND  s1.saleDate >=(SELECT DATE_SUB('"+startDate+"', INTERVAL 1 MONTH)) AND "
					+ "s1.saleDate < '"+startDate+"' AND s1.brandNoPackQty = p.brandNoPackQty) AS previousprice,DATE_FORMAT('"+startDate+"', '%d-%m-%Y') "
					+ "AS crurrentSatrt,DATE_FORMAT('"+endDate+"', '%d-%m-%Y') AS crurrentEnd,  DATE_FORMAT((SELECT DATE_SUB('"+startDate+"', INTERVAL 1 MONTH)), '%d-%m-%Y') AS previousStart,"
					+ "DATE_FORMAT((SELECT DATE_SUB('"+startDate+"', INTERVAL 1 DAY)), '%d-%m-%Y') AS previousend,  DATE_FORMAT((SELECT DATE_SUB('"+startDate+"', INTERVAL 1 DAY)), '%b-%Y') AS showpreviousmonth,"
					+ " DATE_FORMAT('"+endDate+"', '%b-%Y') AS showcurrentmonth FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty`"
					+ " AND s.`saleDate` >='"+startDate+"' AND s.store_id="+storeId+" AND s.`saleDate` <='"+endDate+"' GROUP BY p.`brandNoPackQty` HAVING (currentsale OR previousSale) > 0";
		}
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size() > 0){
				for(Map row : rows){
					CompareSaleBean bean = new CompareSaleBean();
					bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
					bean.setBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0);
					bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
					bean.setQuantity(row.get("quantity")!=null?row.get("quantity").toString():"");
					bean.setPackQty(row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0);
					bean.setPackType(row.get("packType")!=null?row.get("packType").toString():"");
					bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
					bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
					bean.setCategoryColor(row.get("categorycolor")!=null?row.get("categorycolor").toString():"");
					bean.setCompanyColor(row.get("companycolor")!=null?row.get("companycolor").toString():"");
					bean.setCategoryId(row.get("category")!=null?Long.parseLong(row.get("category").toString()):0);
					bean.setCompanyId(row.get("company")!=null?Long.parseLong(row.get("company").toString()):0);
					bean.setCurrentSale(row.get("currentsale")!=null?Long.parseLong(row.get("currentsale").toString()):0);
					bean.setPreviousSale(row.get("previousSale")!=null?Long.parseLong(row.get("previousSale").toString()):0);
					bean.setCompanyOrder(row.get("companyOrder")!=null?Long.parseLong(row.get("companyOrder").toString()):0);
					bean.setCategoryOrder(row.get("categoryOrder")!=null?Long.parseLong(row.get("categoryOrder").toString()):0);
					bean.setCurrentStartDate(row.get("crurrentSatrt")!=null?row.get("crurrentSatrt").toString():"");
					bean.setCurrentEndDate(row.get("crurrentEnd")!=null?row.get("crurrentEnd").toString():"");
					bean.setPreviousStartDate(row.get("previousStart")!=null?row.get("previousStart").toString():"");
					bean.setPreviousEndDate(row.get("previousend")!=null?row.get("previousend").toString():"");
					bean.setCurrentSalePrice(row.get("currentprice")!=null?Double.parseDouble(row.get("currentprice").toString()):0);
					bean.setPreviousSalePrice(row.get("previousprice")!=null?Double.parseDouble(row.get("previousprice").toString()):0);
					
					if(id==2) {
						comment1 = (row.get("crurrentSatrt")!=null?row.get("crurrentSatrt").toString():"")+" To "+(row.get("crurrentEnd")!=null?row.get("crurrentEnd").toString():"");
						comment2 = (row.get("previousStart")!=null?row.get("previousStart").toString():"")+" To "+(row.get("previousend")!=null?row.get("previousend").toString():"");
						
					}else {
						comment1 = (row.get("showcurrentmonth")!=null?row.get("showcurrentmonth").toString():"");
						comment2 = (row.get("showpreviousmonth")!=null?row.get("showpreviousmonth").toString():"");
					}
					bean.setShowCurrentSelectedDate(comment1);
					bean.setShowPreviousSelectedDate(comment2);
					beanList.add(bean);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			return beanList;
		}
		return beanList;
	}
}
