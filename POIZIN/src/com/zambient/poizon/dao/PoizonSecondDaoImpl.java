
package com.zambient.poizon.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.zambient.poizon.bean.AddNewBrandBean;
import com.zambient.poizon.bean.AdministrationExpensesBean;
import com.zambient.poizon.bean.BankCreditAmountBean;
import com.zambient.poizon.bean.CallDiscountBean;
import com.zambient.poizon.bean.CommentForSaleBean;
import com.zambient.poizon.bean.CommulativeDiscountBean;
import com.zambient.poizon.bean.CommulativeInvoiceBean;
import com.zambient.poizon.bean.DiscountAndMonthBean;
import com.zambient.poizon.bean.DiscountAsPerCompanyBeen;
import com.zambient.poizon.bean.DiscountEstimationBean;
import com.zambient.poizon.bean.DiscountEstimationDetails;
import com.zambient.poizon.bean.DiscountMonthWiseBean;
import com.zambient.poizon.bean.DiscountTransactionBean;
import com.zambient.poizon.bean.DiscountWithDate;
import com.zambient.poizon.bean.EditProductBean;
import com.zambient.poizon.bean.ExpenseCategoryAndDate;
import com.zambient.poizon.bean.ExpenseCategoryBean;
import com.zambient.poizon.bean.InvestmentBean;
import com.zambient.poizon.bean.InvoiceDateBean;
import com.zambient.poizon.bean.MaxMinDiscountBean;
import com.zambient.poizon.bean.MonthlyExpensesBean;
import com.zambient.poizon.bean.MrpRoundOffBean;
import com.zambient.poizon.bean.PieChartBean;
import com.zambient.poizon.bean.ProductListBean;
import com.zambient.poizon.bean.ProductOrderBy;
import com.zambient.poizon.bean.ProductPerformanceBean;
import com.zambient.poizon.bean.ProductPerformanceWithComaparisonBean;
import com.zambient.poizon.bean.ProductWithStatement;
import com.zambient.poizon.bean.ProfitAndLossBean;
import com.zambient.poizon.bean.SaleBean;
import com.zambient.poizon.bean.SaleWithDiscountBean;
import com.zambient.poizon.bean.SplitPackType;
import com.zambient.poizon.bean.StockInBean;
import com.zambient.poizon.bean.StockLiftCompanyWithChecksBean;
import com.zambient.poizon.bean.StockLiftWithDiscountEstimate;
import com.zambient.poizon.bean.TillDateBalanceSheetChart;
import com.zambient.poizon.bean.TillMonthStockLiftWithDiscount;
import com.zambient.poizon.bean.UploadAndViewInvoiceBean;
import com.zambient.poizon.bean.UploadAndViewWithYearBean;
import com.zambient.poizon.bean.VendorAdjustmentBean;
import com.zambient.poizon.bean.addNewIndentBean;
import com.zambient.poizon.bean.category;
import com.zambient.poizon.bean.categoryLineChart;
import com.zambient.poizon.bean.companyBean;
import com.zambient.poizon.bean.data;
import com.zambient.poizon.bean.dataset;
import com.zambient.poizon.utill.UserSession;

@PropertySource("classpath:/com/zambient/poizon/resources/poizon.properties")
@Repository("poizonSecondDao")
public class PoizonSecondDaoImpl implements PoizonSecondDao{
	final static Logger log = Logger.getLogger(PoizonSecondDaoImpl.class);
    @Autowired
	private JdbcTemplate jdbcTemplate;
    
    @Autowired 
	private UserSession userSession;
    
    @Value("${discountDate.1}")
	private String discountDate1;
    
    @Value("${discountDate.2}")
	private String discountDate2;
    
    @Value("${discountDate.4}")
	private String discountDate4;
    
    @Value("${discountDate.5}")
   	private String discountDate5;
    
    @Value("${discountDate.6}")
   	private String discountDate6;
  
    /*Old method not used any where*/
	@Override
	public List<InvoiceDateBean> getTotalDiscountDate() {
		String Query="SELECT DISTINCT CAST(discountdate AS DATE) AS DATE FROM `discount`";
		List<InvoiceDateBean> invoiceDateList = new ArrayList<InvoiceDateBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(Query);
			System.out.println("row size>> "+rows.size());
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					InvoiceDateBean invoiceDateBean = new InvoiceDateBean();
					invoiceDateBean.setDate(row.get("DATE").toString());
					invoiceDateList.add(invoiceDateBean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return invoiceDateList;
		}finally {
			
		}
		return invoiceDateList;
	}

	/*Old method not used any where*/
	@Override
	public List<DiscountEstimationBean> getbuildDiscountIntentData(String sdate) {
    String QUERY="SELECT brandNo,BrandName,company,category,caseRate,noOfCases,cashBackOnPerCase,priceToPurches,cashBackOnOrder,profitPercentageDiscount,L2,L1,Q,P,N,D,SB,LB,"
                  +"TIN,estimateDays,(SELECT t.color FROM `company_tab` t WHERE `company`=t.`companyName`) AS color,(SELECT t.companyOrder FROM `company_tab` t WHERE `company`=t.`companyName`) AS companyorder,"
                  + "CAST(discountdate AS DATE) AS discountDate FROM discount WHERE (CAST(discountdate AS DATE)) = '"+sdate+"'";
    //System.out.println(QUERY);
       List<DiscountEstimationBean> discountList = new ArrayList<DiscountEstimationBean>();
       try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			System.out.println("row size>> "+rows.size());
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					DiscountEstimationBean bean = new DiscountEstimationBean();
					bean.setBrandName(row.get("BrandName").toString());
					bean.setBrandNo(Long.parseLong(row.get("brandNo").toString()));
					bean.setCompany(row.get("company").toString());
					bean.setCategory(row.get("category").toString());
					bean.setCaseRate(Double.parseDouble(row.get("caseRate").toString()));
					bean.setNoOfCases(Long.parseLong(row.get("noOfCases").toString()));
					bean.setCashBackOnOrder(Double.parseDouble(row.get("cashBackOnOrder").toString()));
					bean.setPriceToPurches(Double.parseDouble(row.get("priceToPurches").toString()));
					bean.setCashBackOnPerPrice(Double.parseDouble(row.get("cashBackOnPerCase").toString()));
					bean.setProfitPercentageDiscount(row.get("profitPercentageDiscount").toString());
					bean.setL2(Double.parseDouble(row.get("L2").toString()));
					bean.setL1(Double.parseDouble(row.get("L1").toString()));
					bean.setQ(Double.parseDouble(row.get("Q").toString()));
					bean.setP(Double.parseDouble(row.get("P").toString()));
					bean.setN(Double.parseDouble(row.get("N").toString()));
					bean.setD(Double.parseDouble(row.get("C").toString()));
					bean.setSB(Double.parseDouble(row.get("SB").toString()));
					bean.setLB(Double.parseDouble(row.get("LB").toString()));
					bean.setTIN(Double.parseDouble(row.get("TIN").toString()));
					bean.setEstimateDays(Long.parseLong(row.get("estimateDays").toString()));
					bean.setColor(row.get("color").toString());
					bean.setOrderBy(Long.parseLong(row.get("companyorder").toString()));
					discountList.add(bean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return discountList;
		}
		return discountList;
	}

	/**
	 * This method is used to get Lifted stock and Discount amount for Enter discount page
	 * */
	@Override
	public List<StockLiftWithDiscountEstimate> getStockLiftingWithDiscount(String date) {
		
		String startDate=null;
		String endDate=null;
		List<StockLiftWithDiscountEstimate> stockList = new ArrayList<StockLiftWithDiscountEstimate>();
		
		String QUERY="SELECT p.realBrandNo,p.shortBrandName,p.packQty,p.category,p.company, (SELECT t.categoryName FROM `category_tab` t WHERE p.`category`=t.`categoryId`) "
				+ "AS categoryName,(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,"
				+ "(SELECT t.color FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS color,"
				+ "(SELECT t.color FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryColor,"
				+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p.`company`=t.`companyId`)  AS companyorder,"
				+ "(SELECT d.discountAmount FROM stock_lift_with_discount d WHERE d.brandNo=p.realBrandNo AND d.stockDiscountDate='"+date+"' AND d.store_id = "+userSession.getStoreId()+") AS discountAmount,"
				+ "(SELECT d.target FROM stock_lift_with_discount d WHERE d.brandNo=p.realBrandNo AND d.stockDiscountDate='"+date+"' AND d.store_id = "+userSession.getStoreId()+") AS targetCase,"
				+ " (SELECT d.adjustment FROM stock_lift_with_discount d WHERE d.brandNo=p.realBrandNo AND d.stockDiscountDate='"+date+"' AND d.store_id = "+userSession.getStoreId()+") AS adjustment,"
				+ "(SELECT dt.totalDiscountAmount FROM stock_lift_with_discount dt WHERE dt.brandNo=p.realBrandNo AND dt.stockDiscountDate='"+date+"' AND dt.store_id = "+userSession.getStoreId()+") "
				+ " AS totalDiscountAmount,ROUND(AVG((SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.invoiceDateAsPerSheet <= "
				+ "(SELECT LAST_DAY('"+date+"')) AND i.store_id = "+userSession.getStoreId()+"  ORDER BY `invoiceId` DESC LIMIT 1)),0) AS casePrice,SUM(i.receivedBottles) AS received  FROM product p LEFT JOIN "
				+ "invoice i  ON p.`brandNoPackQty` = i.`brandNoPackQty` AND i.`invoiceDateAsPerSheet`>='"+date+"' "
				+ "AND i.`invoiceDateAsPerSheet`<=(SELECT LAST_DAY('"+date+"')) AND i.store_id = "+userSession.getStoreId()+" GROUP BY realBrandNo ORDER BY p.company ASC";
		//System.out.println(QUERY);
		try{
			try {
			endDate = jdbcTemplate.queryForObject("SELECT `saleDate` FROM sale where saleDate<=(SELECT LAST_DAY('"+date+"')) AND store_id = "+userSession.getStoreId()+" ORDER BY `saleDate` DESC LIMIT 1", String.class);
			}catch(Exception e){
				endDate = jdbcTemplate.queryForObject("SELECT  DATE_SUB(invoiceDate, INTERVAL 1 DAY) FROM invoice WHERE store_id = "+userSession.getStoreId()+" ORDER BY `invoiceDate` ASC LIMIT 1 ", String.class);
			}
			if(endDate != null && endDate !="" ){
			    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			    Date date1 = null;
				try {
					date1 = dateFormat.parse(endDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				Calendar cal = new GregorianCalendar();
				cal.setTime(date1);
				cal.add(Calendar.DAY_OF_MONTH, -30);
				Date today60 = cal.getTime();
				startDate = dateFormat.format(today60);
			 }
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					StockLiftWithDiscountEstimate bean = new StockLiftWithDiscountEstimate();
					bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
					bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
					bean.setComapnyId(row.get("company")!=null?Long.parseLong(row.get("company").toString()):0);
					bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
					bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
					bean.setColor(row.get("color")!=null?row.get("color").toString():"");
					bean.setCategoryColor(row.get("categoryColor")!=null?row.get("categoryColor").toString():"");
					bean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
					bean.setDiscount(row.get("discountAmount")!=null?Double.parseDouble(row.get("discountAmount").toString()):0);
					bean.setTargetCase(row.get("targetCase")!=null?Long.parseLong(row.get("targetCase").toString()):0);
					bean.setAdjustment(row.get("adjustment")!=null?Long.parseLong(row.get("adjustment").toString()):0);
					bean.setReceived(row.get("received")!=null?Long.parseLong(row.get("received").toString()):0);
					StockInBean stockInBean = getIndentEstimateSoldDataAndStock(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0, startDate, endDate);
					bean.setLiftedCase(stockInBean.getQtyBtl());
					//System.out.println(stockInBean.getQtyBtl());
					bean.setLiftedAndAdjCase(stockInBean.getQtyBtl()+(row.get("adjustment")!=null?Long.parseLong(row.get("adjustment").toString()):0));
					bean.setInHouseStock(stockInBean.getStockInId());
					bean.setTotalDiscount((double) Math.round((stockInBean.getQtyBtl()+(row.get("adjustment")!=null?Long.parseLong(row.get("adjustment").toString()):0))*(row.get("discountAmount")!=null?Double.parseDouble(row.get("discountAmount").toString()):0)));
					bean.setLastMonthSold(stockInBean.getTotalPrice());
					bean.setCaseRate(stockInBean.getCasePrice());
					bean.setCaseRateList(getCaseRateForAllCategory(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0, startDate, endDate));
					//bean.setCaseRate(row.get("casePrice")!=null?Double.parseDouble(row.get("casePrice").toString()):0);
					if(((row.get("targetCase")!=null?Long.parseLong(row.get("targetCase").toString()):0) - (stockInBean.getQtyBtl())) > 0)
					bean.setPending((long) ((row.get("targetCase")!=null?Long.parseLong(row.get("targetCase").toString()):0) - (stockInBean.getQtyBtl())));
					else
						bean.setPending((long)0);
					if((row.get("received")!=null?Long.parseLong(row.get("received").toString()):0) > 0)
						bean.setMaxMinDiscountBean(getMaximunLowestAndPreviousDiscount(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0,date));
					stockList.add(bean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return stockList;
		}finally {
			
		}
		return stockList;
	
	}
/**
 * This method is used to get Max,Mon and previous month discount amount for a brand
 * */
	private List<MaxMinDiscountBean> getMaximunLowestAndPreviousDiscount(long brandNo, String date) {
		int storeId= userSession.getStoreId();
		List<MaxMinDiscountBean>  listData = new ArrayList<MaxMinDiscountBean>();
		String MIN_QUERY = "SELECT MIN(s.`discountAmount`) AS minDisc,DATE_FORMAT(s.`stockDiscountDate`, '%b' '-%y') AS stockDiscountDate,"
				+ "(SELECT ROUND(SUM(COALESCE(i.`receivedBottles`,0)/p.`packQty`)) FROM product p INNER JOIN invoice i ON p.`brandNoPackQty`=i.`brandNoPackQty` "
				+ "WHERE i.store_id = "+storeId+"  AND i.`invoiceDateAsPerSheet` >= stockDiscountDate "
				+ "AND i.`invoiceDateAsPerSheet` <= (SELECT LAST_DAY(s.stockDiscountDate)) AND p.`brandNo` = s.brandNo AND s.store_id = "+storeId+" GROUP BY p.`brandNo` ) AS minCase"
				+ " FROM `stock_lift_with_discount` s WHERE s.`brandNo`=? "
				+ " AND  s.discountAmount = ( SELECT MIN(discountAmount) FROM stock_lift_with_discount WHERE  brandNo=s.`brandNo` AND s.store_id = "+storeId+" AND discountAmount > 0) AND s.store_id = "+storeId+"";
		String LAST_MONTH_QUERY = "SELECT s.`discountAmount`, DATE_FORMAT(s.`stockDiscountDate`, '%b' '-%y') AS stockDiscountDate,(SELECT ROUND(SUM(COALESCE(i.`receivedBottles`,0)/p.`packQty`)) FROM product p"
				+ " INNER JOIN invoice i ON p.`brandNoPackQty`=i.`brandNoPackQty` WHERE i.`invoiceDateAsPerSheet` >= s.stockDiscountDate "
				+ "AND i.`invoiceDateAsPerSheet` <= (SELECT LAST_DAY(s.stockDiscountDate)) AND p.`brandNo` = s.brandNo AND s.store_id = "+storeId+" GROUP BY p.`brandNo` ) AS lastmonthCase"
				+ " FROM `stock_lift_with_discount` s WHERE s.`brandNo`=?  AND  s.stockDiscountDate = ( SELECT '"+date+"' - INTERVAL 1 MONTH LIMIT 1) AND s.store_id = "+storeId+"";
		
		String MAX_QUERY = "SELECT MAX(s.`discountAmount`) AS maxDisc, DATE_FORMAT(s.`stockDiscountDate`, '%b' '-%y') AS stockDiscountDate,"
				+ "(SELECT ROUND(SUM(COALESCE(i.`receivedBottles`,0)/p.`packQty`)) FROM product p INNER JOIN invoice i ON p.`brandNoPackQty`=i.`brandNoPackQty` "
				+ "WHERE i.`invoiceDateAsPerSheet` >= stockDiscountDate "
				+ "AND i.`invoiceDateAsPerSheet` <= (SELECT LAST_DAY(s.stockDiscountDate)) AND p.`brandNo` = s.brandNo AND s.store_id = "+storeId+" GROUP BY p.`brandNo` ) AS maxCase"
				+ " FROM `stock_lift_with_discount` s WHERE s.`brandNo`=? "
				+ " AND  s.discountAmount = ( SELECT MAX(discountAmount) FROM stock_lift_with_discount WHERE  brandNo=s.`brandNo` AND store_id = "+storeId+" AND discountAmount > 0) AND s.store_id = "+storeId+"";
		try{
			 List<Map<String, Object>> rows1 = jdbcTemplate.queryForList(MIN_QUERY, new Object[]{brandNo});
			 List<Map<String, Object>> rows2 = jdbcTemplate.queryForList(LAST_MONTH_QUERY, new Object[]{brandNo});
			 List<Map<String, Object>> rows3 = jdbcTemplate.queryForList(MAX_QUERY, new Object[]{brandNo});
				MaxMinDiscountBean bean = new MaxMinDiscountBean();
				if (rows1 != null && rows1.size() > 0) {
					for (Map row : rows1) {
						bean.setMinDisc(row.get("minDisc") != null ? Double.parseDouble(row.get("minDisc").toString()) : 0);
						bean.setMinDiscCase(row.get("minCase") != null ? Long.parseLong(row.get("minCase").toString()) : 0);
						bean.setMinDiscDate(row.get("stockDiscountDate")!=null?row.get("stockDiscountDate").toString():"");
					}
				}else{
					bean.setMinDisc((double)0);
					bean.setMinDiscCase((long)0);
					bean.setMinDiscDate("");
				}
				if (rows2 != null && rows2.size() > 0) {
					for (Map row : rows2) {
						bean.setPrevoiusDisc(row.get("discountAmount") != null ? Double.parseDouble(row.get("discountAmount").toString()) : 0);
						bean.setPreviousDiscCase(row.get("lastmonthCase") != null ? Long.parseLong(row.get("lastmonthCase").toString()) : 0);
						bean.setPreviousDiscDate(row.get("stockDiscountDate")!=null?row.get("stockDiscountDate").toString():"");
					}
				}else{
					bean.setPrevoiusDisc((double)0);
					bean.setPreviousDiscCase((long)0);
					bean.setPreviousDiscDate("");
				}
				if (rows3 != null && rows3.size() > 0) {
					for (Map row : rows3) {
						bean.setMaxDisc(row.get("maxDisc") != null ? Double.parseDouble(row.get("maxDisc").toString()) : 0);
						bean.setMaxDiscCase(row.get("maxCase") != null ? Long.parseLong(row.get("maxCase").toString()) : 0);
						bean.setMaxDiscDate(row.get("stockDiscountDate")!=null?row.get("stockDiscountDate").toString():"");
					}
				}else{
					bean.setMaxDisc((double)0);
					bean.setMaxDiscCase((long)0);
					bean.setMaxDiscDate("");
				}
				listData.add(bean);
		}catch(Exception e){
			e.printStackTrace();
		}
		return listData;
	}

	private List<PieChartBean> getCaseRateForAllCategory(long brandNo, String startDate, String endDate) {
		List<PieChartBean> beanList = new ArrayList<PieChartBean>();
		String QUERY = "SELECT DISTINCT p.`realBrandNo`,p.`brandNoPackQty`,p.packType,"
				+ "(SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty "
				+ "AND i.invoiceDateAsPerSheet <= (SELECT LAST_DAY('"+startDate+"'))  AND i.store_id = "+userSession.getStoreId()+"  ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1)"
				+ " AS casePrice FROM product p WHERE p.`realBrandNo`="+brandNo+"";
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					PieChartBean bean = new PieChartBean();
					bean.setLabel(row.get("packType")!=null?row.get("packType").toString():"");
					bean.setValue(row.get("casePrice")!=null?Double.parseDouble(row.get("casePrice").toString()):0);
					beanList.add(bean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return beanList;
		}
		return beanList;
	}

	/**
	 * This method is used to post or save discount per case, target and adjustment (Enter discount)
	 * */
	@Override
	public synchronized String saveStockLiftWithDiscountData(JSONArray discountDetails, String strDate) {
		
		int storeId=userSession.getStoreId();
		log.info("PoizonSecondDaoImpl.saveStockLiftWithDiscountData discountDetails= "+discountDetails +"  startdate>>> "+strDate);
		//saveDiscountEstimateFromStockLift(discountDetails,strDate);
		String result=null;
		String QUERY="INSERT INTO `stock_lift_with_discount`(`brandNo`,companyName,discountAmount,totalDiscountAmount,stockDiscountDate,adjustment,target,store_id) VALUES(?,?,?,?,?,?,?,?)";
		synchronized(this){
			try {
				for (int i = 0; i < discountDetails.length(); ++i) {
				 JSONObject rec;
				    rec = discountDetails.getJSONObject(i);
				    List<Map<String, Object>> flag = jdbcTemplate.queryForList("SELECT * FROM stock_lift_with_discount WHERE brandNo = "+Integer.parseInt(rec.getString("brandNo"))+" AND stockDiscountDate= '"+strDate+"' AND store_id="+storeId+"");
				    
				   // System.out.println("flag>> "+flag.size());
				    if(flag.size()>0 && flag != null){
				    	int val = jdbcTemplate.update("UPDATE stock_lift_with_discount SET discountAmount="+Double.parseDouble(rec.getString("discountAmount"))+", totalDiscountAmount="+Double.parseDouble(rec.getString("discountTotalAmount"))+",adjustment="+rec.getInt("adjustment")+",target="+rec.getInt("target")+" WHERE brandNo = "+Integer.parseInt(rec.getString("brandNo"))+" AND stockDiscountDate= '"+strDate+"' AND store_id="+storeId+"");
				    	 if(val > 0){
						    	result="Discount Data saved successfully";
						    }
				    
				    }else{
				    	 int val=0;
				    	if(Double.parseDouble(rec.getString("discountTotalAmount")) > 0 || rec.getInt("adjustment") > 0 || rec.getInt("target") > 0)
				     val = jdbcTemplate.update(QUERY,new Object[]{Integer.parseInt(rec.getString("brandNo")),Integer.parseInt(rec.getString("companyId")),Double.parseDouble(rec.getString("discountAmount")),Double.parseDouble(rec.getString("discountTotalAmount")),strDate,rec.getInt("adjustment"),rec.getInt("target"),storeId});
				    if(val > 0){
				    	result="Discount Data saved successfully";
				    }
				    }
				    
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				result="Something Went Wrong.";
				log.log(Level.WARN, "saveStockLiftWithDiscountData in PoizonSecondDaoImpl exception", e);
				e.printStackTrace();
			}
			}
		return result;
	}

	private void saveDiscountEstimateFromStockLift(JSONArray discountDetails, String strDate) {
		String QUERY="insert into discount (brandNo,caseRate,inhouseStock,targetCase,discPerCase,totalDiscount,investment,lastMonthSold,clearnsPeriodWithoutInhouseStock,"
				+ "clearnsPeriodWithInhouseStock,rateOfInterestWithInHouseStock,rateOfInterestWithoutInHouseStock,estimateDate) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		synchronized(this){
		try {
			for (int i = 0; i < discountDetails.length(); ++i) {
				 JSONObject rec;
				    rec = discountDetails.getJSONObject(i);
				       if((rec.getDouble("discountAmount")) > 0){
				    	   int val = jdbcTemplate.update("UPDATE discount SET discPerCase="+rec.getDouble("discountAmount")+""
					    			+ ", totalDiscount="+Double.parseDouble(rec.getString("discountTotalAmount"))+""
					    			+ ", rateOfInterestWithInHouseStock="+Double.parseDouble(rec.getString("rateOfInterestWithInhouse"))+""
					    			+ ", rateOfInterestWithoutInHouseStock="+Double.parseDouble(rec.getString("rateOfInterestWithoutInhouse"))+""
					    			+ " WHERE brandNo = "+Integer.parseInt(rec.getString("brandNo"))+" AND estimateDate = '"+strDate+"'");
					    
					    	 if((val < 0) || (val == 0)){
					    		jdbcTemplate.update(QUERY,new Object[]{Integer.parseInt(rec.getString("brandNo")),Double.parseDouble(rec.getString("caseRate")),
								    		Integer.parseInt(rec.getString("inHouseStock")),0,rec.getDouble("discountAmount"),
								    		Double.parseDouble(rec.getString("totalDiscount")),0,Integer.parseInt(rec.getString("lastMonthSold")),
								    		0,0,Double.parseDouble(rec.getString("rateOfInterestWithInhouse")),
								    		Double.parseDouble(rec.getString("rateOfInterestWithoutInhouse")),strDate});
								    
							    }
				       }
				    	
				}
			} catch (JSONException e) {
			// TODO Auto-generated catch block
			log.log(Level.WARN, "saveDiscountEsitmateDetails in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
		}
		}
		
	}

	/**
	 * This method is used to post or save band credited amount in db
	 * */
	@Override
	public String saveBankCreditAmount(BankCreditAmountBean bankCreditAmountBean) {
		log.info("PoizonSecondDaoImpl.saveBankCreditAmount bankCreditAmountBean= "+bankCreditAmountBean.toString());
		String result=null;
		try {
		int val = jdbcTemplate.update("update `bank_credit_tab` set creditAmount="+bankCreditAmountBean.getBankCredit()+",retention="+bankCreditAmountBean.getRetention()+" where creditDate='"+bankCreditAmountBean.getBankdate()+"' AND store_id="+userSession.getStoreId()+"");
		System.out.println("val>> "+val);
		if(val > 0){
			result="Bank Credit Amount Updated Successfully";
		}else{
			jdbcTemplate.update("INSERT INTO `bank_credit_tab` (creditAmount,creditDate,retention,store_id) VALUES(?,?,?,?)",bankCreditAmountBean.getBankCredit(),bankCreditAmountBean.getBankdate(),bankCreditAmountBean.getRetention(),userSession.getStoreId());
			result="Bank Credit Amount Saved Successfully";
		}
		}catch( Exception e){
			result="Something Went Wrong.";
			log.log(Level.WARN, "saveBankCreditAmount in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
		}
		return result;
	}
	/**
	 * this method is used to get discount amount and details for each company
	 * */
	@Override
	public List<DiscountAsPerCompanyBeen> getCompanyWiseDiscountAmount(String strDate) {
		log.info("PoizonSecondDaoImpl.getCompanyWiseDiscountAmount");
		String QUERY="SELECT ct.`companyId`,ct.`companyName`,ct.`color`,ct.enterprise,"
				+ "((SELECT SUM(sd.`totalDiscountAmount`) FROM `stock_lift_with_discount` sd WHERE ct.`companyId`=sd.companyName AND sd.`stockDiscountDate`='"+strDate+"')) AS discountAmount,"
				+ "(SELECT d.`chequeAmount` FROM `discount_as_per_company` d WHERE ct.`companyId`=d.companyId AND d.chequeDate='"+strDate+"') AS chequeAmount,"
				+ "(SELECT d.`adjustmentCheque` FROM `discount_as_per_company` d WHERE ct.`companyId`=d.companyId AND d.chequeDate='"+strDate+"') AS adjustmentCheque,"
				+ "(SELECT d.rentals FROM `discount_as_per_company` d WHERE ct.`companyId`=d.companyId AND d.chequeDate='"+strDate+"') AS rentals,"
				+ "(SELECT d.`comment` FROM `discount_as_per_company` d WHERE ct.`companyId`=d.companyId AND d.chequeDate='"+strDate+"') AS comment,"
				+ "(((SELECT COALESCE(SUM(totalDiscountAmount),0) FROM stock_lift_with_discount WHERE stockDiscountDate >="
				+ "(SELECT `stockDiscountDate` FROM `stock_lift_with_discount` ORDER BY stockDiscountDate ASC LIMIT 1) AND stockDiscountDate<='"+strDate+"'"
				+ "AND companyName=ct.`companyId`)+(SELECT COALESCE(SUM(rentals),0) FROM discount_as_per_company WHERE chequeDate >="
				+ " (SELECT `stockDiscountDate` FROM `stock_lift_with_discount` ORDER BY stockDiscountDate ASC LIMIT 1) AND chequeDate<='"+strDate+"' AND companyId=ct.`companyId`))-(SELECT COALESCE(SUM(chequeAmount+adjustmentCheque),0) FROM discount_as_per_company WHERE chequeDate >="
				+ "(SELECT `stockDiscountDate` FROM `stock_lift_with_discount` ORDER BY stockDiscountDate ASC LIMIT 1) AND chequeDate<='"+strDate+"' AND companyId=ct.`companyId`))"
				+ " AS arrears FROM `company_tab` ct ORDER BY ct.`companyOrder`";
		System.out.println(QUERY);
		List<DiscountAsPerCompanyBeen> discountAsPerCompanyBeen = new ArrayList<DiscountAsPerCompanyBeen>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					DiscountAsPerCompanyBeen bean = new DiscountAsPerCompanyBeen();
					bean.setCompany(row.get("companyId")!=null?Long.parseLong(row.get("companyId").toString()):0);
					bean.setCompanyName(row.get("companyName")!=null?row.get("companyName").toString():"");
					bean.setEnterprise(row.get("enterprise")!=null?row.get("enterprise").toString():"");
					bean.setDiscountAmount(row.get("discountAmount")!=null?Double.parseDouble(row.get("discountAmount").toString()):0);
					bean.setArrears(row.get("arrears")!=null?Double.parseDouble(row.get("arrears").toString()):0);
					bean.setColor(row.get("color")!=null?row.get("color").toString():"");
					//bean.setBrandNo(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0);
					bean.setChequeAmount(row.get("chequeAmount")!=null?Double.parseDouble(row.get("chequeAmount").toString()):0);
					bean.setAdjustmentCheque(row.get("adjustmentCheque")!=null?Double.parseDouble(row.get("adjustmentCheque").toString()):0);
					bean.setRentals(row.get("rentals")!=null?Double.parseDouble(row.get("rentals").toString()):0);
					bean.setComment(row.get("comment")!=null?row.get("comment").toString():"");
					discountAsPerCompanyBeen.add(bean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return discountAsPerCompanyBeen;
		}finally {
			
		}
		return discountAsPerCompanyBeen;
	}
	/**
	 * this method is used to maintan product order 
	 * */
	@Override
	public String addOrderForAProduct(ProductOrderBy productOrderBy) {
		log.info("PoizonSecondDaoImpl.addOrderForAProduct");
		String result = null;
		double brandNumAndPackQty = Double.parseDouble(Long.toString(productOrderBy.getBrandNo()).concat(Long.toString(productOrderBy.getPackQty())));
		try{
			int val = jdbcTemplate.update("UPDATE product SET productOrder = productOrder + 1 WHERE productOrder >="+productOrderBy.getOrderValue()+"");
			//if(val > 0){
				//jdbcTemplate.update("UPDATE product SET `productOrder` = "+productOrderBy.getOrderValue()+" WHERE `brandNoPackQty`="+brandNumAndPackQty+" AND productOrder=0");
			       jdbcTemplate.update("UPDATE product SET `productOrder` = "+productOrderBy.getOrderValue()+" WHERE `brandNoPackQty`="+brandNumAndPackQty+"");
				result="Order Manage Successfully";
			//}
			//else{
				//result="Please try again";
			//}
			
		}catch(Exception e){
			result="Something Went Wrong.";
			log.log(Level.WARN, "addOrderForAProduct in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * this method is used to write a comment in related to daily sale
	 * */
	@Override
	public String addCommentForSale(CommentForSaleBean commentForSaleBean) {
		log.info("PoizonSecondDaoImpl.addCommentForSale");
		String result = null;
		try{
			int val = jdbcTemplate.update("update `comment_in_sale` set comment='"+commentForSaleBean.getSaleComment()+"' where commentDate='"+commentForSaleBean.getCommentDate()+"' AND store_id="+userSession.getStoreId());
			System.out.println("val>> "+val);
			if(val > 0){
				result="Sale Comment Updated Successfully";
			}else{
				jdbcTemplate.update("INSERT INTO `comment_in_sale` (comment,commentDate,store_id) VALUES(?,?,?)",commentForSaleBean.getSaleComment(),commentForSaleBean.getCommentDate(),userSession.getStoreId());
				result="Sale Comment Saved Successfully";
			}
		}catch(Exception e){
			result="Something Went Wrong.";
			log.log(Level.WARN, "addCommentForSale in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * this method is used to get comment in related to daily sale
	 * */
	@Override
	public CommentForSaleBean getSaleCommentData(String date) {
		CommentForSaleBean bean = new CommentForSaleBean();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT comment FROM comment_in_sale WHERE commentDate='"+date+"'");
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					bean.setSaleComment(row.get("comment")!=null?row.get("comment").toString():"No Data");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return bean;
		}finally {
			
		}
		
		return bean;
	}
	/**
	 * this method is used to save invoice mrp rounding up
	 * */
	@Override
	public String saveMrpRoundOff(MrpRoundOffBean mrpRoundOffBean) {
		log.info("PoizonSecondDaoImpl.saveMrpRoundOff");
		String result = null;
		try{
			int val = jdbcTemplate.update("update `invoice_mrp_round_off` set mrpRoundOff='"+mrpRoundOffBean.getMrpRoundOff()+"',turnoverTax='"+mrpRoundOffBean.getTurnOverTax()+"',tcsVal='"+mrpRoundOffBean.getTcsVal()+"',retailer_credit_balance='"+mrpRoundOffBean.getRetailerCreditVal()+"' where mrpRoundOffDate='"+mrpRoundOffBean.getDate()+"' AND store_id="+userSession.getStoreId());
			System.out.println("val>> "+val);
			if(val > 0){
				result="MrpRoundOff Updated Successfully";
			}else{
				jdbcTemplate.update("INSERT INTO `invoice_mrp_round_off` (mrpRoundOff,turnoverTax,tcsVal,mrpRoundOffDate,mrpRoundOffDateAsCopy,retailer_credit_balance,store_id) VALUES(?,?,?,?,?,?,?)",mrpRoundOffBean.getMrpRoundOff(),mrpRoundOffBean.getTurnOverTax(),mrpRoundOffBean.getTcsVal(),mrpRoundOffBean.getDate(),mrpRoundOffBean.getDateAsPerCopy(),mrpRoundOffBean.getRetailerCreditVal(),userSession.getStoreId());
				result="MrpRoundOff Saved Successfully";
			}
		}catch(Exception e){
			result="Something Went Wrong.";
			log.log(Level.WARN, "saveMrpRoundOff in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * this method is used to get all product to maintan in order
	 * */
	@Override
	public List<AddNewBrandBean> getBrandDetailsForProductOrder() {
		log.info("PoizonSecondDaoImpl.getBrandDetailsForProductOrder");
		String Query="SELECT DISTINCT p.`productId`, p.brandNo,p.brandName,p.packQty,p.`productOrder` FROM product p ORDER BY p.`productOrder`";
		List<AddNewBrandBean> addNewBrandList = new ArrayList<AddNewBrandBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(Query);
			System.out.println("row size>> "+rows.size());
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					AddNewBrandBean addNewBrandBean = new AddNewBrandBean();
					addNewBrandBean.setProductId(Long.parseLong(row.get("productId").toString()));
					addNewBrandBean.setBrandNo(Long.parseLong(row.get("brandNo").toString()));
					addNewBrandBean.setBrandName(row.get("brandName").toString());
					addNewBrandBean.setPackQty(Long.parseLong(row.get("packQty").toString()));
					addNewBrandBean.setMatch(Long.parseLong(row.get("productOrder").toString()));
					addNewBrandList.add(addNewBrandBean);
				}
			}
		}catch(Exception e){
			log.log(Level.WARN, "getBrandDetailsForProductOrder in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
			return addNewBrandList;
		}finally {
			
		}
		return addNewBrandList;
	}

	/**
	 * This method is used to get company list
	 * */
	@Override
	public List<companyBean> getCompanyList() {
		log.info("PoizonSecondDaoImpl.getCompanyList()");
		String QUERY="SELECT `companyId`,`companyName`,color,`companyOrder` FROM `company_tab`";
		List<companyBean> companyList = new ArrayList<companyBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					companyBean company = new companyBean();
					company.setId(row.get("companyId")!=null?Long.parseLong(row.get("companyId").toString()):0);
					company.setName(row.get("companyName")!=null?row.get("companyName").toString():"");
					company.setColor(row.get("color")!=null?row.get("color").toString():"");
					company.setOrder(row.get("companyOrder")!=null?Long.parseLong(row.get("companyOrder").toString()):0);
					companyList.add(company);
				}
			}
		}catch(Exception e){
			log.log(Level.WARN, "getCompanyList in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
			return companyList;
		}
		return companyList;
	}

	/**
	 * this method is used to get all distinct category from category_tab table
	 * */
	@Override
	public List<companyBean> getCategoryList() {
		log.info("PoizonSecondDaoImpl.getCategoryList()");
		String QUERY="SELECT `categoryId`,`categoryName`,`categoryOrder` FROM `category_tab`";
		List<companyBean> categoryList = new ArrayList<companyBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					companyBean category = new companyBean();
					category.setId(row.get("categoryId")!=null?Long.parseLong(row.get("categoryId").toString()):0);
					category.setName(row.get("categoryName")!=null?row.get("categoryName").toString():"");
					category.setOrder(row.get("categoryOrder")!=null?Long.parseLong(row.get("categoryOrder").toString()):0);
					categoryList.add(category);
				}
			}
		}catch(Exception e){
			log.log(Level.WARN, "getCategoryList in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
			return categoryList;
		}
		return categoryList;
	}
	
	/**
	 * this method is used to save Monthly expenses
	 * */
	@Override
	public String saveMonthlyExpensesData(JSONArray expensesJson) {
		log.info("PoizonSecondDaoImpl.saveMonthlyExpensesData expensesJson= "+expensesJson);
		String QUERY="insert into monthlyexpenses (debitCredit,amount,subject,comment,monthlyExpensesDate,store_id) values(?,?,?,?,?,?)";
		String result=null;
		synchronized(this){
		try {
			for (int i = 0; i < expensesJson.length(); ++i) {
			 JSONObject rec;
			    rec = expensesJson.getJSONObject(i);
			   try{
					int val=jdbcTemplate.update(QUERY,new Object[]{rec.getInt("debitCredit"),rec.getDouble("amount"),rec.getInt("subject"),rec.get("comment"),rec.get("date"),userSession.getStoreId()});
						if(val>0)
							result="Monthly Expenses Added Successfully";
				}catch(Exception e){
					result="something went wrong";
					log.log(Level.WARN, "saveMonthlyExpensesData in PoizonSecondDaoImpl exception", e);
					e.printStackTrace();
					}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			result="Something Went Wrong.";
			log.log(Level.WARN, "saveMonthlyExpensesData in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
		}
		}
		return result;
	}
	
	/**
	 * this method is used to get Monthly expenses
	 * */
	@Override
	public List<MonthlyExpensesBean> getMonthlyExpenses(String expensesdate) {
		log.info("PoizonSecondDaoImpl.getMonthlyExpenses expensesdate= "+expensesdate);
		String QUERY="SELECT m.`monthlyExpenseId`,m.`amount`,m.`comment`,m.`monthlyExpensesDate`,"
				+ "(SELECT `monthlyExpensecategoryName` c FROM `monthly_expense_category` c WHERE c.categoryId = m.subject) AS subject,"
				+ "CASE m.debitCredit WHEN 0 THEN 'Debit' WHEN 1 THEN 'Credit' END AS debitCredit FROM `monthlyexpenses` m WHERE m.`monthlyExpensesDate`='"+expensesdate+"' AND m.store_id = "+userSession.getStoreId()+"";
		List<MonthlyExpensesBean> MonthlyExpensesBeanList = new ArrayList<MonthlyExpensesBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					MonthlyExpensesBean bean = new MonthlyExpensesBean();
					bean.setMonthlyExpenseId(row.get("monthlyExpenseId")!=null?Long.parseLong(row.get("monthlyExpenseId").toString()):0);
					bean.setDebitCredit(row.get("debitCredit")!=null?row.get("debitCredit").toString():"");
					bean.setAmount(row.get("amount")!=null?Double.parseDouble(row.get("amount").toString()):0);
					bean.setSubject(row.get("subject")!=null?row.get("subject").toString():"");
					bean.setComment(row.get("comment")!=null?row.get("comment").toString():"");
					bean.setDate(row.get("monthlyExpensesDate")!=null?row.get("monthlyExpensesDate").toString():"");
					MonthlyExpensesBeanList.add(bean);
				}
			}
		}catch(Exception e){
			log.log(Level.WARN, "getCategoryList in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
			return MonthlyExpensesBeanList;
		}
		return MonthlyExpensesBeanList;
	}

	/**
	 * this method is used to delete Monthly expenses
	 * */
	@Override
	public String deleteMonthlyExpenses(int expenseMonthlyId) {
		String result=null;
		try{
    		int row = jdbcTemplate.update("DELETE FROM monthlyexpenses WHERE monthlyExpenseId=?", new Object[]{expenseMonthlyId});
    		if(row >0) result="Expenses deleted successfully";
    	}catch(Exception e){
    		result="Something went wrong";
    		e.printStackTrace();
    		return result;
    	}
		return result;
	}


	@Override
	public List<ExpenseCategoryBean> getMonthlyExpenseCategory() {
     String QUERY="SELECT categoryId,`monthlyExpensecategoryName`,hoverExpenses FROM `monthly_expense_category`";
     List<ExpenseCategoryBean> expenseCategoryBeanList = new ArrayList<ExpenseCategoryBean>();
     try{
    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					ExpenseCategoryBean bean = new ExpenseCategoryBean();
					bean.setCategoryId(row.get("categoryId")!=null?Long.parseLong(row.get("categoryId").toString()):0);
					bean.setExpenseName(row.get("monthlyExpensecategoryName")!=null?row.get("monthlyExpensecategoryName").toString():"");
					bean.setExpenseDate(row.get("hoverExpenses")!=null?row.get("hoverExpenses").toString():"");
					expenseCategoryBeanList.add(bean);
				}
			}
     }catch(Exception e){
    	 log.log(Level.WARN, "getMonthlyExpenseCategory in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
			return expenseCategoryBeanList;
     }
       return expenseCategoryBeanList;
	}


	public double calculateDiscount(Double bno,String date,long kota,long sale,double price) throws ParseException{
	    //System.out.println("date>> "+date+" Brand No>> "+bno+"  kota>> "+kota+"  sale>> "+sale+"  price>> "+price);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = sdf.parse(date);
        Date date2 = sdf.parse(discountDate1);
        double profit=0;
        if (date1.after(date2)) {
        	double kotapercentage=0;
        	double salepercentage=0;
        	if(sale >= kota){
        		if(kota < 0)
        			kota=0;
        		kotapercentage= ((kota*price)*25)/100;
        		//System.out.println("kotapercentage>> "+kotapercentage);
        		long saleKota = sale-kota;
        		salepercentage = ((saleKota*price)*5)/100;
        		//System.out.println("salepercentage>>> "+salepercentage);
        		profit=kotapercentage+salepercentage;
        	}else{
        		salepercentage= ((sale*price)*25)/100;
        		profit=salepercentage;
        		//System.out.println("sub else part profit>> "+profit);
        	}
        }else{
        	profit = ((sale*price)*25)/100;
        	//System.out.println("supr else part profit>> "+profit);
        }
       // System.out.println("total profit >> "+profit);
		return profit;
	}

	/*Old method not used any where*/
	@Override
	public List<ProductPerformanceBean> getProductPerformance(int brandNo) {
		log.info("PoizonSecondDaoImpl.getProductPerformance()"+brandNo);
		List<ProductPerformanceBean> productPerformanceBean = new ArrayList<ProductPerformanceBean>();
		ArrayList<String> arrlist = new ArrayList<String>();
		arrlist.add("2017-10-01");
		 String QUERY="SELECT DISTINCT saleDate FROM sale WHERE `saleDate` LIKE '%-01';";
	     try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						arrlist.add(row.get("saleDate")!=null?row.get("saleDate").toString():"");
					}
				}
				if(arrlist.size() > 0){
					for (String saledate : arrlist) { 	
						double caserate=0,investment=0,noOfcases=0,govtProfit=0,discountAmount=0,stockLiftDiscount=0,totalProductDiscount=0;
						long ttlSale=0;
						long index=0;
						String month=null;
					 List<Map<String, Object>> secrows = jdbcTemplate.queryForList("SELECT DISTINCT p.brandNo,p.brandNoPackQty,p.shortBrandName,p.packQty,p.specialMargin,"
					 		+ "SUM(s.sale) AS sale,(SUM(s.sale)/p.`packQty`) AS cases,"
					 		+ "(SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.invoiceDate <= "
					 		+ "(SELECT LAST_DAY('"+saledate+"')) ORDER BY `invoiceId` DESC LIMIT 1) AS packQtyRate,"
					 		+ "((TRUNCATE(SUM(s.sale)/p.`packQty`,1))*(SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.invoiceDate <= "
					 		+ "(SELECT LAST_DAY('"+saledate+"')) ORDER BY `invoiceId` DESC LIMIT 1)) AS investment,"
					 		+ "((SELECT COALESCE(SUM(`closing`),0) FROM sale k WHERE k.brandNoPackQty = p.`brandNoPackQty` AND k.`saleDate`=(SELECT DATE_SUB('"+discountDate2+"', INTERVAL 1 DAY)))-(SELECT COALESCE(SUM(s1.sale),0) FROM sale s1 WHERE s1.brandNoPackQty = p.`brandNoPackQty` AND s1.`saleDate` >= '"+discountDate2+"' "
				            + " AND s1.`saleDate` <= (SELECT DATE_SUB('"+saledate+"', INTERVAL 1 DAY)))) AS salekota,"
				            + "(SELECT COALESCE(SUM(sale),0) FROM sale ss WHERE ss.brandNoPackQty=p.brandNoPackQty AND saleDate <'"+saledate+"') AS firstSale,"
				            + "(SELECT COALESCE(SUM(sale),0) FROM sale ss WHERE ss.brandNoPackQty=p.brandNoPackQty AND saleDate <=(SELECT LAST_DAY('"+saledate+"'))) AS secondSale,"
					 		+ "(SELECT st.discountAmount FROM `stock_lift_with_discount` st WHERE st.brandNo=p.brandNo AND st.stockDiscountDate = '"+saledate+"') AS discountAmount,"
					 		+ "ROUND((((SELECT st.discountAmount FROM `stock_lift_with_discount` st WHERE st.brandNo=p.brandNo AND st.stockDiscountDate = '"+saledate+"')/p.packQty)"
					 		+ "*SUM(s.sale)),1) AS stockLiftProfit,(SELECT i.SingleBottelRate FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.invoiceDate <= "
					 		+ "(SELECT LAST_DAY('"+saledate+"')) ORDER BY `invoiceId` DESC LIMIT 1) AS singleBtlRate,(SELECT DATE_FORMAT('"+saledate+"', '%M' ' %Y')) AS date "
					 				+ "FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND p.`brandNo`="+brandNo+"  AND `saleDate`>='"+saledate+"'"
					 				+ " AND `saleDate`<=(SELECT LAST_DAY('"+saledate+"')) GROUP BY brandNoPackQty ORDER BY p.brandNo ASC");
					 for(Map secrow : secrows){
						 DiscountWithDate discountWithDateBean = new DiscountWithDate();
						 noOfcases=noOfcases+(secrow.get("cases")!=null?Double.parseDouble(secrow.get("cases").toString()):0);
						 ttlSale=ttlSale+(secrow.get("sale")!=null?Long.parseLong(secrow.get("sale").toString()):0);
						 caserate=caserate+(secrow.get("packQtyRate")!=null?Double.parseDouble(secrow.get("packQtyRate").toString()):0);
						 investment=investment+(secrow.get("investment")!=null?Double.parseDouble(secrow.get("investment").toString()):0);
						 discountAmount=secrow.get("discountAmount")!=null?Double.parseDouble(secrow.get("discountAmount").toString()):0;
						 month=secrow.get("date")!=null?secrow.get("date").toString():"";
						 discountWithDateBean = calculateDiscountByBrandNoPackQty(secrow.get("brandNoPackQty")!=null?Double.parseDouble(secrow.get("brandNoPackQty").toString()):0,
								 saledate,secrow.get("firstSale")!=null?Long.parseLong(secrow.get("firstSale").toString()):0,secrow.get("secondSale")!=null?Long.parseLong(secrow.get("secondSale").toString()):0,
										 secrow.get("specialMargin")!=null?Double.parseDouble(secrow.get("specialMargin").toString()):0);
						 govtProfit=govtProfit+(discountWithDateBean.getGovtProfit());
						 stockLiftDiscount=stockLiftDiscount+(discountWithDateBean.getValue());
						 index++;	
					 }
					 if(month !=null && month != ""){
					 ProductPerformanceBean bean = new ProductPerformanceBean();
					 bean.setCaseRate(caserate/index);
					 bean.setCasesSold(noOfcases);
					 bean.setSale(ttlSale);
					 bean.setTotalPrice(investment);
					 bean.setStockLiftDiscount(stockLiftDiscount);
					 bean.setDiscountAmount(discountAmount);
					 bean.setDate(month);
					 bean.setGovtProfit(govtProfit);
					 bean.setTotalProductDiscount(govtProfit+stockLiftDiscount);
					 productPerformanceBean.add(bean);
					 }
					}
				}
	     }catch(Exception e){
	    	 log.log(Level.WARN, "getProductPerformance in PoizonSecondDaoImpl exception", e);
				e.printStackTrace();
				return productPerformanceBean;
	     }
		return productPerformanceBean;
	}
	/**
	 * This method is used to get distinct brand name and brand number
	 * */
	@Override
	public List<AddNewBrandBean> getDistinctBrandNameAndNo() {
		List<AddNewBrandBean> addNewBrandList =  new ArrayList<AddNewBrandBean>();
		 try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT DISTINCT realBrandNo, TRIM(`brandName`) AS brandName FROM product");
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						AddNewBrandBean bean = new AddNewBrandBean();
						bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
						bean.setBrandName(row.get("brandName")!=null?row.get("brandName").toString():"");
						addNewBrandList.add(bean);
					}
				}
	     }catch(Exception e){
	    	 log.log(Level.WARN, "getMonthlyExpenseCategory in PoizonSecondDaoImpl exception", e);
				e.printStackTrace();
				return addNewBrandList;
	     }
	       return addNewBrandList;
		}
	/**
	 * This method is used to get discount, received check and rentals to till selected months 
	 * */
	@Override
	public List<TillMonthStockLiftWithDiscount> getTillMonthStockLiftWithDiscount(String date,int company) {
		log.info("PoizonSecondDaoImpl.getTillMonthStockLiftWithDiscount()");
		List<TillMonthStockLiftWithDiscount> tillMonthStockLiftWithDiscounts = new ArrayList<TillMonthStockLiftWithDiscount>();
		ArrayList<String> arrlist = new ArrayList<String>();
		 String QUERY="SELECT DISTINCT stockDiscountDate FROM stock_lift_with_discount WHERE `stockDiscountDate` LIKE '%-01' AND `stockDiscountDate` <= '"+date+"' ORDER BY stockDiscountDate DESC";
		 try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						arrlist.add(row.get("stockDiscountDate")!=null?row.get("stockDiscountDate").toString():"");
					}
				}
				//System.out.println(arrlist);
				if(arrlist.size() > 0){
					for (String stockDate : arrlist) {
						List<Map<String, Object>> rowdata = jdbcTemplate.queryForList("SELECT COALESCE(SUM(d.`totalDiscountAmount`),0) AS discountamount,UPPER(DATE_FORMAT('"+stockDate+"', '%b' ' %y')) AS DATE,"
								+ "(SELECT COALESCE(SUM(ds.`rentalAmount`),0) FROM `rental_tab` ds WHERE ds.rentalDate = '"+stockDate+"' AND ds.`company`="+company+") AS rental,"
								+ "(SELECT COALESCE(SUM(ds.`transactionAmt`),0) FROM `discount_transaction` ds WHERE ds.month = '"+stockDate+"' AND ds.`companyId`="+company+") AS chequeAmount,"
								+ "(SELECT COALESCE(SUM(ds.`adjCheck`),0) FROM `discount_transaction` ds WHERE ds.month = '"+stockDate+"' AND ds.`companyId`="+company+") AS adjustmentCheque,"
								+ "(((SELECT COALESCE(SUM(totalDiscountAmount),0) FROM stock_lift_with_discount WHERE stockDiscountDate >=(SELECT `stockDiscountDate` "
								+ "FROM `stock_lift_with_discount` ORDER BY stockDiscountDate ASC LIMIT 1) AND stockDiscountDate<='"+stockDate+"'AND companyName="+company+")+"
								+ "(SELECT COALESCE(SUM(rentalAmount),0) FROM rental_tab WHERE rentalDate >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount`"
								+ " ORDER BY stockDiscountDate ASC LIMIT 1) AND rentalDate<='"+stockDate+"' AND company="+company+"))-(SELECT COALESCE(SUM(transactionAmt+adjCheck),0)"
								+ "  FROM discount_transaction WHERE MONTH >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount` ORDER BY stockDiscountDate ASC LIMIT 1)"
								+ "   AND MONTH<='"+stockDate+"' AND companyId="+company+")) AS arrears FROM `stock_lift_with_discount` d WHERE d.`companyName`="+company+" AND d.`stockDiscountDate` = '"+stockDate+"'");
						if(rowdata != null && rowdata.size()>0) {
							for(Map row : rowdata){
								TillMonthStockLiftWithDiscount bean = new TillMonthStockLiftWithDiscount();
								bean.setDiscounts(row.get("discountamount")!=null?Double.parseDouble(row.get("discountamount").toString()):0);
								bean.setDate(row.get("date")!=null?row.get("date").toString():"");
								bean.setRentals(row.get("rental")!=null?Double.parseDouble(row.get("rental").toString()):0);
								bean.setReceived(row.get("chequeAmount")!=null?Double.parseDouble(row.get("chequeAmount").toString()):0);
								bean.setAdjustmentCheque(row.get("adjustmentCheque")!=null?Double.parseDouble(row.get("adjustmentCheque").toString()):0);
								bean.setTotal((row.get("discountamount")!=null?Double.parseDouble(row.get("discountamount").toString()):0)+(row.get("rental")!=null?Double.parseDouble(row.get("rental").toString()):0));
								//bean.setDues(((row.get("discountamount")!=null?Double.parseDouble(row.get("discountamount").toString()):0)+(row.get("rental")!=null?Double.parseDouble(row.get("rental").toString()):0))-(row.get("chequeAmount")!=null?Double.parseDouble(row.get("chequeAmount").toString()):0));
								bean.setDues(row.get("arrears")!=null?Double.parseDouble(row.get("arrears").toString()):0);
								bean.setSaleBean(getTillMonthDiscountDetails(stockDate,company));
								//bean.setDiscountAsPerCompanyBeen(getDiscountAsPerCompanyWise(stockDate,company));
								bean.setDiscountAsPerCompanyBeenList(getChecksDetailsWithImage(stockDate,company));
								tillMonthStockLiftWithDiscounts.add(bean);
							}
						}
					}
				}
	     }catch(Exception e){
	    	 log.log(Level.WARN, "getTillMonthStockLiftWithDiscount in PoizonSecondDaoImpl exception", e);
	    	 e.printStackTrace();
	    	 return tillMonthStockLiftWithDiscounts;
	    	 }
		return tillMonthStockLiftWithDiscounts;
	}


	private List<DiscountTransactionBean> getChecksDetailsWithImage(String stockDate, int company) {
		List<DiscountTransactionBean> discountAsPerCompanyBeenList = new ArrayList<DiscountTransactionBean>();
		String QUERY="SELECT dt.companyId,dt.month,dt.transactionType,dt.transactionAmt,DATE_FORMAT(dt.transactionDate, '%d ' '%b ' '%Y') AS transactionDate,dt.adjCheck,dt.imageName,dt.imagePath,dt.bank,"
				+ "(SELECT r.rentalAmount FROM rental_tab r WHERE dt.companyId = r.company AND dt.month=r.rentalDate) AS rentalAmount"
				+ " FROM discount_transaction dt"
				+ " WHERE dt.month='"+stockDate+"' AND dt.companyId="+company+"";
         
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					DiscountTransactionBean bean = new DiscountTransactionBean();
					bean.setCompanyId(row.get("companyId")!=null?Long.parseLong(row.get("companyId").toString()):0);
					bean.setTransactionType(row.get("transactionType")!=null?row.get("transactionType").toString():"");
					bean.setTransactionAmt(row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0);
					bean.setAdjCheck(row.get("adjCheck")!=null?Double.parseDouble(row.get("adjCheck").toString()):0);
					bean.setImageName(row.get("imageName")!=null?row.get("imageName").toString():"");
					bean.setNgalleryImage(row.get("imagePath")!=null?row.get("imagePath").toString():"");
					bean.setRental(row.get("rentalAmount")!=null?Double.parseDouble(row.get("rentalAmount").toString()):0);
					bean.setTransactionDate(row.get("transactionDate")!=null?row.get("transactionDate").toString():"");
					bean.setBank(row.get("bank")!=null?row.get("bank").toString():"");
					discountAsPerCompanyBeenList.add(bean);
				}
			}
		}catch(Exception e){
			log.log(Level.WARN, "getChecksDetailsWithImage private method in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
			return discountAsPerCompanyBeenList;
		}
		return discountAsPerCompanyBeenList;
	}


	/*private DiscountAsPerCompanyBeen getDiscountAsPerCompanyWise(String date, int company) {
		DiscountAsPerCompanyBeen bean = new DiscountAsPerCompanyBeen();
		String QUERY = "SELECT ct.`companyId`,ct.`companyName`,"
				+ "((SELECT COALESCE(SUM(sd.`totalDiscountAmount`),0) FROM `stock_lift_with_discount` sd WHERE ct.`companyId`=sd.companyName AND sd.`stockDiscountDate`='"+date+"')) AS discountAmount,"
				+ " (SELECT COALESCE(SUM(d.`chequeAmount`),0) FROM `discount_as_per_company` d WHERE ct.`companyId`=d.companyId AND d.chequeDate='"+date+"')  AS chequeAmount,"
				+ "(SELECT COALESCE(SUM(d.`adjustmentCheque`),0) FROM `discount_as_per_company` d WHERE ct.`companyId`=d.companyId AND d.chequeDate='"+date+"')  AS adjustmentCheque,"
				+ "(SELECT COALESCE(SUM(d.rentals),0) FROM `discount_as_per_company` d WHERE ct.`companyId`=d.companyId AND d.chequeDate='"+date+"') AS rentals,"
				+ " (SELECT d.`comment` FROM `discount_as_per_company` d WHERE ct.`companyId`=d.companyId AND d.chequeDate='"+date+"') AS comment,"
				+ " (((SELECT COALESCE(SUM(totalDiscountAmount),0) FROM stock_lift_with_discount WHERE stockDiscountDate >= (SELECT `stockDiscountDate` FROM `stock_lift_with_discount` ORDER BY stockDiscountDate ASC LIMIT 1) AND"
				+ " stockDiscountDate<='"+date+"'AND companyName=ct.`companyId`)+(SELECT COALESCE(SUM(rentals),0) FROM discount_as_per_company WHERE chequeDate >= "
				+ " (SELECT `stockDiscountDate` FROM `stock_lift_with_discount` ORDER BY stockDiscountDate ASC LIMIT 1) "
				+ " AND chequeDate<='"+date+"' AND companyId=ct.`companyId`))-(SELECT COALESCE(SUM(chequeAmount+adjustmentCheque),0) "
				+ " FROM discount_as_per_company WHERE chequeDate >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount` "
				+ " ORDER BY stockDiscountDate ASC LIMIT 1) AND chequeDate<='"+date+"' AND companyId=ct.`companyId`)) AS arrears  FROM `company_tab` ct WHERE ct.`companyId`="+company+"";
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					bean.setCompanyName(row.get("companyName")!=null?row.get("companyName").toString():"");
					bean.setDiscountAmount(row.get("discountAmount")!=null?Double.parseDouble(row.get("discountAmount").toString()):0);
					bean.setChequeAmount(row.get("chequeAmount")!=null?Double.parseDouble(row.get("chequeAmount").toString()):0);
					bean.setAdjustmentCheque(row.get("adjustmentCheque")!=null?Double.parseDouble(row.get("adjustmentCheque").toString()):0);
					bean.setRentals(row.get("rentals")!=null?Double.parseDouble(row.get("rentals").toString()):0);
					bean.setComment(row.get("comment")!=null?row.get("comment").toString():"");
					bean.setArrears(row.get("arrears")!=null?Double.parseDouble(row.get("arrears").toString()):0);
				}
			}
		}catch(Exception e){
			log.log(Level.WARN, "getDiscountAsPerCompanyWise private method in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
			return bean;
		}
		return bean;
	}*/


	private List<SaleBean> getTillMonthDiscountDetails(String date, int company) {
		List<SaleBean> saleBean= new ArrayList<SaleBean>();
		String QUERY = "SELECT p.brandNo,p.matchName,p.matchs,p.shortBrandName,(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,"
				+ "(SELECT COALESCE(SUM(d.discountAmount),0) FROM stock_lift_with_discount d WHERE d.brandNo=p.brandNo AND d.stockDiscountDate='"+date+"') AS discountAmount,"
				+ "(SELECT COALESCE(SUM(dt.totalDiscountAmount),0) FROM stock_lift_with_discount dt WHERE dt.brandNo=p.brandNo AND dt.stockDiscountDate='"+date+"')  AS totalDiscountAmount,"
				+ "(CEIL(SUM(i.receivedBottles/i.packQty))+(SELECT COALESCE(SUM(d.adjustment),0) FROM stock_lift_with_discount d WHERE d.brandNo=p.brandNo AND d.stockDiscountDate='"+date+"')) AS totalcases"
				+ "  FROM product p INNER JOIN invoice i  ON p.`brandNoPackQty` = i.`brandNoPackQty` AND `invoiceDateAsPerSheet`>='"+date+"'"
				+ " AND `invoiceDateAsPerSheet`<=(SELECT LAST_DAY('"+date+"')) AND p.`company`="+company+" GROUP BY p.brandNo";
		try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						SaleBean bean = new SaleBean();
						bean.setBrandNo(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0);
						bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
						bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
						bean.setUnitPrice(row.get("discountAmount")!=null?Double.parseDouble(row.get("discountAmount").toString()):0);
						bean.setTotalPrice(row.get("totalDiscountAmount")!=null?Double.parseDouble(row.get("totalDiscountAmount").toString()):0);
						bean.setCaseQty(row.get("totalcases")!=null?Long.parseLong(row.get("totalcases").toString()):0);
						bean.setInvoiceId(row.get("matchs")!=null?Long.parseLong(row.get("matchs").toString()):0);
						bean.setProductType(row.get("matchName")!=null?row.get("matchName").toString():"");
						saleBean.add(bean);
					}
				}
		}catch(Exception e){
			log.log(Level.WARN, "getTillMonthDiscountDetails in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
			return saleBean;
		}
		return saleBean;
	}

/**
 * This method is used to get latest credit amount
 * */
	@Override
	public double getLastDateCredit(String date) {
		log.info("PoizonSecondDaoImpl.getLastDateCredit()");
		double value=0;
		try{
			System.out.println("SELECT SUM(`cashSale`) AS caseprice FROM `card_cash_sale` WHERE DATE >= "
	    	 		+ "(SELECT `creditDate` FROM `bank_credit_tab` WHERE store_id="+userSession.getStoreId()+" ORDER BY creditDate DESC LIMIT 1) AND DATE < '"+date+"' AND store_id="+userSession.getStoreId()+"");
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT SUM(`cashSale`) AS caseprice FROM `card_cash_sale` WHERE DATE >= "
	    	 		+ "(SELECT `creditDate` FROM `bank_credit_tab` WHERE store_id="+userSession.getStoreId()+" ORDER BY creditDate DESC LIMIT 1) AND DATE < '"+date+"' AND store_id="+userSession.getStoreId()+"");
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						value =	 row.get("caseprice")!=null?Double.parseDouble(row.get("caseprice").toString()):0;
					}
				}
	     }catch(Exception e){
	    	 log.log(Level.WARN, "getLastDateCredit in PoizonSecondDaoImpl exception", e);
				e.printStackTrace();
				return value;
	     }
	       return value;
		}

	private String geneColor(Random r) {
        StringBuilder color = new StringBuilder(Integer.toHexString(r
                .nextInt(16777215)));
        while (color.length() < 6) {
            color.append("0");
        }

        return color.append("#").reverse().toString();

    }

	/**
	 * This method is used to save or post check with details for company
	 * */
	@Override
	public String addNewTransaction(DiscountTransactionBean discountTransactionBean) {
		String result=null;
		log.info("PoizonSecondDaoImpl.addNewTransaction Bean>> "+discountTransactionBean.toString());
		 String QUERY="insert into discount_transaction(companyId,month,transactionType,transactionAmt,transactionDate,adjCheck,imagePath,imageName,bank,checkNo,fromBank,comment,received,store_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try{
				int val=jdbcTemplate.update(QUERY,new Object[]{discountTransactionBean.getCompany(),discountTransactionBean.getMonths(),discountTransactionBean.getTransactionType(),discountTransactionBean.getTransactionAmt(),discountTransactionBean.getTransactionDate(),discountTransactionBean.getAdjCheck(),discountTransactionBean.getNgalleryImage(),discountTransactionBean.getImageName(),discountTransactionBean.getBank(),discountTransactionBean.getCheckNo(),discountTransactionBean.getFromBank(),discountTransactionBean.getComment(),discountTransactionBean.getCheckBox(),userSession.getStoreId()});
				if(val>0)
					result="Transaction inserted successfully";
		}catch(Exception e){
			result="something went wrong";
			log.log(Level.WARN, "addNewTransaction in PoizonDaoImpl exception", e);
			e.printStackTrace();
			}
		return result;
	}

	/**
	 * This method is used to get existing check details for selected company
	 * */
	@Override
	public List<DiscountTransactionBean> getDiscountTransactionDetails(String date, int company) {
		List<DiscountTransactionBean> beanList = new ArrayList<DiscountTransactionBean>();
		String QUERY="SELECT transactionId,companyId,MONTH,transactionType,transactionAmt,DATE_FORMAT(transactionDate,'%d-%m-%Y') AS transactionDate,adjCheck,imagePath,imageName,bank,checkNo,fromBank,comment,"
				+ "UPPER(DATE_FORMAT(MONTH, '%b' ' %y')) AS monthName,"
				+ "(SELECT c.companyName FROM company_tab c WHERE c.companyId=dt.companyId) AS companyName FROM discount_transaction dt WHERE MONTH='"+date+"' AND companyId="+company+" AND `received`=1  AND store_id ="+userSession.getStoreId()+"";
		try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						DiscountTransactionBean bean = new DiscountTransactionBean();
						bean.setId(row.get("transactionId")!=null?Long.parseLong(row.get("transactionId").toString()):0);
						bean.setCompanyId(row.get("companyId")!=null?Long.parseLong(row.get("companyId").toString()):0);
						bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
						bean.setRealMonth(row.get("MONTH")!=null?row.get("MONTH").toString():"");
						bean.setMonths(row.get("monthName")!=null?row.get("monthName").toString():"");
						bean.setTransactionType(row.get("transactionType")!=null?row.get("transactionType").toString():"");
						bean.setTransactionAmt(row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0);
						bean.setTransactionDate(row.get("transactionDate")!=null?row.get("transactionDate").toString():"");
						/*bean.setRental(row.get("rental")!=null?Double.parseDouble(row.get("rental").toString()):0);*/
						bean.setAdjCheck(row.get("adjCheck")!=null?Double.parseDouble(row.get("adjCheck").toString()):0);
						bean.setNgalleryImage(row.get("imageName")!=null?row.get("imageName").toString():"");
						bean.setBank(row.get("bank")!=null?row.get("bank").toString():"");
						bean.setCheckNo(row.get("checkNo")!=null?row.get("checkNo").toString():"");
						bean.setFromBank(row.get("fromBank")!=null?row.get("fromBank").toString():"");
						bean.setComment(row.get("comment")!=null?row.get("comment").toString():"");
						beanList.add(bean);
					}
				}
	     }catch(Exception e){
	    	 log.log(Level.WARN, "getDiscountTransactionDetails in PoizonSecondDaoImpl exception", e);
				e.printStackTrace();
				return beanList;
	     }
		
		return beanList;
	}

	/**
	 * This method is used to delete check with details
	 * */
	@Override
	public String deleteDiscountTransactionDetails(int id) {
		String result=null;
		try{
    		int row = jdbcTemplate.update("DELETE FROM discount_transaction WHERE transactionId=? AND store_id=?", new Object[]{id,userSession.getStoreId()});
    		if(row >0) result="Data deleted successfully";
    	}catch(Exception e){
    		result="Something went wrong";
    		e.printStackTrace();
    		return result;
    	}
		return result;
	}

	/**
	 * This method is used to get sold cases and discount amount for those cases based on dates selection.
	 * For discount calculation we are doing from starting 
	 * */
	@Override
	public List<SaleWithDiscountBean> getSaleWithDiscount(String startdate, String enddate) {
            List<SaleWithDiscountBean> saleBeanList =  new ArrayList<SaleWithDiscountBean>();
          String QUERY="SELECT p.brandNoPackQty,p.realBrandNo,p.packQty,p.quantity,p.company,p.category,p.packType,p.shortBrandName,COALESCE(SUM(sale),0) AS sale,"
          		+ "(SELECT t.categoryName FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryName,"
          		+ "(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,"
          		+ "(SELECT t.color FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS color,"
          		+ "(SELECT t.color FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryColor,"
          		+ "COALESCE(SUM(totalPrice),0) AS totalPrice,"
          		+ "(SELECT COALESCE(SUM(totalPrice),0) FROM sale ss WHERE ss.brandNoPackQty=p.brandNoPackQty AND saleDate <'"+startdate+"' AND ss.store_id = "+userSession.getStoreId()+") AS commulativeTotalPrice,"
          		+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyorder,"
          		+ "(SELECT t.categoryOrder FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryOrder,"
          		+ "(SELECT COALESCE(SUM(sale),0) FROM sale ss WHERE ss.brandNoPackQty=p.brandNoPackQty AND saleDate <'"+startdate+"' AND ss.store_id = "+userSession.getStoreId()+") AS commulativeSale "
          		+ "FROM product p INNER JOIN sale s ON p.brandNoPackQty=s.brandNoPackQty WHERE saleDate<='"+enddate+"' AND s.store_id = "+userSession.getStoreId()+"  GROUP BY p.brandNoPackQty ORDER BY p.realBrandNo";
         // System.out.println(QUERY); 
          try{
   	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
   				if(rows != null && rows.size()>0) {
   					for(Map row : rows){
   						SaleWithDiscountBean bean = new SaleWithDiscountBean();
   						DiscountWithDate discountWithDateBean = new DiscountWithDate();
   						DiscountWithDate discountWithDateBeanForGovt = new DiscountWithDate();
   						bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
   						bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
   						bean.setQuantity(row.get("quantity")!=null?row.get("quantity").toString():"");
   						bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
   						bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
   						bean.setPackQty(row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0);
   						bean.setCompanyId(row.get("company")!=null?Long.parseLong(row.get("company").toString()):0);
   						bean.setCategoryId(row.get("category")!=null?Long.parseLong(row.get("category").toString()):0);
   						bean.setPackType(row.get("packType")!=null?row.get("packType").toString():"");
   						bean.setColor(row.get("color")!=null?row.get("color").toString():"");
   						bean.setCategoryColor(row.get("categoryColor")!=null?row.get("categoryColor").toString():"");
   						bean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
   						bean.setTotalPrice((row.get("totalPrice")!=null?Double.parseDouble(row.get("totalPrice").toString()):0)-(row.get("commulativeTotalPrice")!=null?Double.parseDouble(row.get("commulativeTotalPrice").toString()):0));
   						bean.setTotalSale((row.get("sale")!=null?Long.parseLong(row.get("sale").toString()):0)-(row.get("commulativeSale")!=null?Long.parseLong(row.get("commulativeSale").toString()):0));
   						bean.setBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0);
   						discountWithDateBean = calculateDiscountByBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0,
   								startdate,row.get("commulativeSale")!=null?Long.parseLong(row.get("commulativeSale").toString()):0,row.get("sale")!=null?Long.parseLong(row.get("sale").toString()):0,0);
   						discountWithDateBeanForGovt = calculateMarginByBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0,
   								startdate,row.get("commulativeSale")!=null?Long.parseLong(row.get("commulativeSale").toString()):0,row.get("sale")!=null?Long.parseLong(row.get("sale").toString()):0,0);
   						bean.setDiscount(discountWithDateBean.getValue());
   						bean.setDiscountMonth(discountWithDateBean.getDate());
   						bean.setGovtDiscount(discountWithDateBeanForGovt.getGovtProfit());
   						bean.setCostPrice(discountWithDateBeanForGovt.getCostPrice());
   						bean.setCategoryOrder(row.get("categoryOrder")!=null?Long.parseLong(row.get("categoryOrder").toString()):0);
   						saleBeanList.add(bean);
   						
   					}
   				}
   	     }catch(Exception e){
   				e.printStackTrace();
   				return saleBeanList;
   	     }
		return saleBeanList;
	}


	private DiscountWithDate calculateDiscountByBrandNoPackQty(double brandNopackQty, String date, long firstSale, long secondSale,double specialMargin) {
		DiscountWithDate discountWithDateBean = new DiscountWithDate();
		double totalDiscountAmount =0;
		String QUERY ="SELECT p.brandNoPackQty,p.realBrandNo,SUM(receivedBottles) AS receivedBottles,LAST_DAY(i.invoiceDateAsPerSheet - INTERVAL 0 MONTH) AS date,"
				+ "UPPER(DATE_FORMAT((LAST_DAY(i.invoiceDateAsPerSheet - INTERVAL 0 MONTH)), '%b' ' %y')) AS discountDate,"
				+ "(SELECT COALESCE(SUM(d.discountAmount/p.packQty),0) FROM stock_lift_with_discount d WHERE d.brandNo=p.realBrandNo AND d.store_id="+userSession.getStoreId()+" "
				+ "AND stockDiscountDate=(SELECT CONCAT(DATE_FORMAT(LAST_DAY(i.invoiceDateAsPerSheet - INTERVAL 0 MONTH),'%Y-%m-'),'01'))) AS discount,"
				+ "(SELECT i.SingleBottelRate FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.invoiceDateAsPerSheet <= date AND i.store_id="+userSession.getStoreId()+" ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1) AS singleBtlRate,"
				+ " (SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.invoiceDateAsPerSheet <= DATE AND i.store_id="+userSession.getStoreId()+" ORDER BY `invoiceDateAsPerSheet`"
				+ " DESC LIMIT 1) AS bottleSaleMrp  "
				+ " FROM product p INNER JOIN invoice i ON p.brandNoPackQty=i.brandNoPackQty WHERE "
				+ " p.brandNoPackQty = "+brandNopackQty+" AND i.store_id="+userSession.getStoreId()+" GROUP BY (LAST_DAY(invoiceDateAsPerSheet - INTERVAL 0 MONTH)) ORDER BY (LAST_DAY(invoiceDateAsPerSheet - INTERVAL 0 MONTH))";
		    //System.out.println(QUERY);
		List<CommulativeInvoiceBean> commulativeInvoiceBeanList = new ArrayList<CommulativeInvoiceBean>();
		      try{
		   	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
		   				if(rows != null && rows.size()>0) {
		   					for(Map row : rows){
		   						CommulativeInvoiceBean invoiceBean = new CommulativeInvoiceBean();
		   						invoiceBean.setTotalInvoiceBtls(row.get("receivedBottles")!=null?Long.parseLong(row.get("receivedBottles").toString()):0);
		   						invoiceBean.setInvoiceDate(row.get("discountDate")!=null?row.get("discountDate").toString():"");
		   						invoiceBean.setDiscoutnPerBtl(row.get("discount")!=null?Double.parseDouble(row.get("discount").toString()):0);
		   						invoiceBean.setInvoiceRealDate(row.get("date")!=null?row.get("date").toString():"");
		   						invoiceBean.setSingleBtlRate(row.get("singleBtlRate")!=null?Double.parseDouble(row.get("singleBtlRate").toString()):0);
		   						invoiceBean.setBtlSaleMrp(row.get("bottleSaleMrp")!=null?Double.parseDouble(row.get("bottleSaleMrp").toString()):0);
		   						commulativeInvoiceBeanList.add(invoiceBean);
		   						
		   					}
		   				}
		   				DiscountWithDate firstDiscountAmountBean = getFistDateDiscount(commulativeInvoiceBeanList,firstSale,specialMargin);
		   				DiscountWithDate secondDiscountAmountBean = getFistDateDiscount(commulativeInvoiceBeanList,secondSale,specialMargin);
		   				discountWithDateBean.setDate(secondDiscountAmountBean.getDate());
		   				discountWithDateBean.setValue(secondDiscountAmountBean.getValue()-firstDiscountAmountBean.getValue());
		   			
		   	     }catch(Exception e){
		   				e.printStackTrace();
		   	     }
		return discountWithDateBean;
	}


	private DiscountWithDate getFistDateDiscount(List<CommulativeInvoiceBean> commulativeInvoiceBeanList, long firstSale, double specialMargin) throws ParseException {
		  //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		    DiscountWithDate discountWithDate = new DiscountWithDate();
		     List<CommulativeDiscountBean> commulativeDiscountBeanList = new ArrayList<CommulativeDiscountBean>();
		     double discountAmt=0;
		     String discDate=null;
		     double govtDiscAmt=0,costPrice=0;
		     long sale_qty_1=0;
		        for(CommulativeInvoiceBean Inv_qty : commulativeInvoiceBeanList){
		        	if(firstSale >0){
		        		 CommulativeDiscountBean bean = new CommulativeDiscountBean();
		        		 sale_qty_1=firstSale-Inv_qty.getTotalInvoiceBtls();
		        		 if(sale_qty_1<=0)
		        			{
		        			 bean.setBtls(firstSale);
		        			 bean.setDiscAmount(Inv_qty.getDiscoutnPerBtl());
		        			 bean.setDate(Inv_qty.getInvoiceDate());
		        			 bean.setBtlRate(Inv_qty.getSingleBtlRate());
		        			
		        			}
		        		 else{
		        			 bean.setBtls(Inv_qty.getTotalInvoiceBtls());
		        			 bean.setDiscAmount(Inv_qty.getDiscoutnPerBtl());
		        			 bean.setDate(Inv_qty.getInvoiceDate());
		        			 bean.setBtlRate(Inv_qty.getSingleBtlRate());
		        			 bean.setCostPrice(Inv_qty.getTotalInvoiceBtls() * Inv_qty.getSingleBtlRate());
		        			
		        				 
		        		 }
		        		 commulativeDiscountBeanList.add(bean);
		        		 firstSale = sale_qty_1;
		        	 }
		         }
		        for(CommulativeDiscountBean dic_amt: commulativeDiscountBeanList)
		        {
		        	discDate=dic_amt.getDate();
		        	discountAmt=discountAmt+(dic_amt.getBtls() * dic_amt.getDiscAmount());
		        	
		        }
		        discountWithDate.setDate(discDate);
		        discountWithDate.setValue(discountAmt);
		     
		return discountWithDate;
	}
	private DiscountWithDate calculateMarginByBrandNoPackQty(double brandNopackQty, String date, long firstSale, long secondSale,double specialMargin) {
		DiscountWithDate discountWithDateBean = new DiscountWithDate();
		double totalDiscountAmount =0;
		
		/*
		 * String QUERY
		 * ="SELECT brandNoPackQty,SUM(receivedBottles) AS received,SingleBottelRate,bottleSaleMrp,invoiceDateAsPerSheet,specialMargin "
		 * + "FROM invoice WHERE  receivedBottles > 0 AND `brandNoPackQty`="
		 * +brandNopackQty+" AND store_id = "+userSession.getStoreId()
		 * +" GROUP BY invoiceDateAsPerSheet ORDER BY invoiceDateAsPerSheet" ;
		 */
		 
		
		 String QUERY ="SELECT i.brandNoPackQty,SUM(i.receivedBottles) AS received,i.SingleBottelRate,i.bottleSaleMrp,i.invoiceDateAsPerSheet,i.`specialMargin`,s.turnoverTax "
		 +"FROM invoice i INNER JOIN invoice_mrp_round_off s ON i.`invoiceDateAsPerSheet` = s.`mrpRoundOffDateAsCopy` AND  receivedBottles > 0 AND i.`brandNoPackQty`="+brandNopackQty+" AND i.`store_id` = "+userSession.getStoreId()+" "
		 		+ "AND s.`store_id` = "+userSession.getStoreId()+" GROUP BY i.invoiceDateAsPerSheet ORDER BY i.invoiceDateAsPerSheet ASC";
		List<CommulativeInvoiceBean> commulativeInvoiceBeanList = new ArrayList<CommulativeInvoiceBean>();
		      try{
		   	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
		   				if(rows != null && rows.size()>0) {
		   					for(Map row : rows){
		   						CommulativeInvoiceBean invoiceBean = new CommulativeInvoiceBean();
		   						invoiceBean.setTotalInvoiceBtls(row.get("received")!=null?Long.parseLong(row.get("received").toString()):0);
		   						invoiceBean.setInvoiceRealDate(row.get("invoiceDateAsPerSheet")!=null?row.get("invoiceDateAsPerSheet").toString():"");
		   						invoiceBean.setSingleBtlRate(row.get("SingleBottelRate")!=null?Double.parseDouble(row.get("SingleBottelRate").toString()):0);
		   						invoiceBean.setBtlSaleMrp(row.get("bottleSaleMrp")!=null?Double.parseDouble(row.get("bottleSaleMrp").toString()):0);
		   						invoiceBean.setSpecialMargin(row.get("specialMargin")!=null?Double.parseDouble(row.get("specialMargin").toString()):0);
		   						invoiceBean.setTurnOverTax(row.get("turnoverTax")!=null?Double.parseDouble(row.get("turnoverTax").toString()):0);
		   						commulativeInvoiceBeanList.add(invoiceBean);
		   						
		   					}
		   				}
		   				DiscountWithDate firstDiscountAmountBean = getFistDateDiscountForGovt(commulativeInvoiceBeanList,firstSale,specialMargin);
		   				DiscountWithDate secondDiscountAmountBean = getFistDateDiscountForGovt(commulativeInvoiceBeanList,secondSale,specialMargin);
		   				discountWithDateBean.setGovtProfit(secondDiscountAmountBean.getGovtProfit()-firstDiscountAmountBean.getGovtProfit());
		   				discountWithDateBean.setCostPrice(secondDiscountAmountBean.getCostPrice()-firstDiscountAmountBean.getCostPrice());
		   				
		   	     }catch(Exception e){
		   				e.printStackTrace();
		   	     }
		return discountWithDateBean;
	}
	private DiscountWithDate getFistDateDiscountForGovt(List<CommulativeInvoiceBean> commulativeInvoiceBeanList, long firstSale, double specialMargin) throws ParseException {
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		    DiscountWithDate discountWithDate = new DiscountWithDate();
		     List<CommulativeDiscountBean> commulativeDiscountBeanList = new ArrayList<CommulativeDiscountBean>();
		     double govtDiscAmt=0,costPrice=0;
		     long sale_qty_1=0;
		        for(CommulativeInvoiceBean Inv_qty : commulativeInvoiceBeanList){
		        	if(firstSale >0){
		        		 CommulativeDiscountBean bean = new CommulativeDiscountBean();
		        		 sale_qty_1=firstSale-Inv_qty.getTotalInvoiceBtls();
		        		 if(sale_qty_1<=0)
		        			{
		        			 /*if((sdf.parse(Inv_qty.getInvoiceRealDate()).after(sdf.parse(discountDate1)) && sdf.parse(Inv_qty.getInvoiceRealDate()).before(sdf.parse(discountDate4))) || (sdf.parse(Inv_qty.getInvoiceRealDate()).after(sdf.parse(discountDate5)) && sdf.parse(Inv_qty.getInvoiceRealDate()).before(sdf.parse(discountDate6))))*/
		        			 if(Inv_qty.getTurnOverTax() > 0){
		        				 bean.setGovtProfit((firstSale * Inv_qty.getSingleBtlRate()) + (((firstSale * Inv_qty.getSingleBtlRate())*14.5)/100) + (firstSale * Inv_qty.getSpecialMargin()) + ((firstSale * Inv_qty.getSingleBtlRate())*1/100));
		        				 bean.setCostPrice(firstSale * (Inv_qty.getSingleBtlRate() + (Inv_qty.getSingleBtlRate() * 14.5)/100));
		        			 } 
		        			 else{
		        				 bean.setGovtProfit(((firstSale * Inv_qty.getSingleBtlRate()) + (firstSale * Inv_qty.getSpecialMargin())) + ((firstSale * Inv_qty.getSingleBtlRate())*1/100));
		        				 bean.setCostPrice(firstSale * Inv_qty.getSingleBtlRate());
		        			 }
		        			}
		        		 else{
		        			 /*if((sdf.parse(Inv_qty.getInvoiceRealDate()).after(sdf.parse(discountDate1)) && sdf.parse(Inv_qty.getInvoiceRealDate()).before(sdf.parse(discountDate4))) || (sdf.parse(Inv_qty.getInvoiceRealDate()).after(sdf.parse(discountDate5)) && sdf.parse(Inv_qty.getInvoiceRealDate()).before(sdf.parse(discountDate6))))*/
		        			 if(Inv_qty.getTurnOverTax() > 0){
		        				 bean.setGovtProfit((Inv_qty.getTotalInvoiceBtls() * Inv_qty.getSingleBtlRate()) + (((Inv_qty.getTotalInvoiceBtls() * Inv_qty.getSingleBtlRate())*14.5)/100) + (Inv_qty.getTotalInvoiceBtls() * Inv_qty.getSpecialMargin()) + ((Inv_qty.getTotalInvoiceBtls() * Inv_qty.getSingleBtlRate())*1/100));
		        				 bean.setCostPrice(Inv_qty.getTotalInvoiceBtls() * (Inv_qty.getSingleBtlRate() + (Inv_qty.getSingleBtlRate() * 14.5)/100));
		        			 } 
		        			 else{
		        				 bean.setGovtProfit(((Inv_qty.getTotalInvoiceBtls() * Inv_qty.getSingleBtlRate()) + (Inv_qty.getTotalInvoiceBtls() * Inv_qty.getSpecialMargin())) + ((Inv_qty.getTotalInvoiceBtls() * Inv_qty.getSingleBtlRate())*1/100));
		        				 bean.setCostPrice(Inv_qty.getTotalInvoiceBtls() * Inv_qty.getSingleBtlRate());
		        			 }
		        				 
		        		 }
		        		 commulativeDiscountBeanList.add(bean);
		        		 firstSale = sale_qty_1;
		        	 }
		         }
		        for(CommulativeDiscountBean dic_amt: commulativeDiscountBeanList)
		        {
		        	  govtDiscAmt=govtDiscAmt+(dic_amt.getGovtProfit());
		        	  costPrice=costPrice+(dic_amt.getCostPrice());
		        }
		         discountWithDate.setGovtProfit(govtDiscAmt);
		         discountWithDate.setCostPrice(costPrice);
		return discountWithDate;
	}
	/**
	 * This method is used to get stock lift and discount amount
	 * */
	@Override
	public List<StockLiftCompanyWithChecksBean> getStockLiftWithDiscountTransactionDetails(String date) {
		List<StockLiftCompanyWithChecksBean> StocktransactionBeanList = new ArrayList<StockLiftCompanyWithChecksBean>();
		String QUERY="SELECT ct.`companyId`,ct.`companyName`,ct.`color`,ct.companyOrder,ct.enterprise,"
				+ "((SELECT SUM(sd.`totalDiscountAmount`) FROM `stock_lift_with_discount` sd WHERE ct.`companyId`=sd.companyName "
				+ "AND sd.`stockDiscountDate`='"+date+"')) AS discountAmount,(((SELECT COALESCE(SUM(totalDiscountAmount),0) FROM "
				+ "stock_lift_with_discount  WHERE stockDiscountDate >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount` "
				+ "ORDER BY stockDiscountDate ASC LIMIT 1)  AND stockDiscountDate<='"+date+"'AND companyName=ct.`companyId`)"
				+ "+(SELECT COALESCE(SUM(rentalAmount),0) FROM rental_tab   WHERE rentalDate >=  (SELECT `stockDiscountDate` FROM `stock_lift_with_discount` "
				+ "ORDER BY stockDiscountDate ASC LIMIT 1)    AND rentalDate<='"+date+"'   AND company=ct.`companyId`))-"
				+ "(SELECT COALESCE(SUM(transactionAmt+adjCheck),0)    FROM discount_transaction WHERE MONTH >=(SELECT `stockDiscountDate` "
				+ "  FROM `stock_lift_with_discount` ORDER BY    stockDiscountDate ASC LIMIT 1)   AND MONTH<='"+date+"' AND companyId=ct.`companyId`)) "
				+ "AS arrears,(SELECT r.rentalAmount FROM rental_tab r WHERE ct.companyId = r.company AND r.rentalDate='"+date+"') AS rentalAmount,"
			    + "(SELECT SUM(transactionAmt) FROM discount_transaction dt WHERE dt.month='"+date+"' AND companyId= ct.`companyId`) AS checksAmt,"
			    + "   (SELECT SUM(adjCheck) FROM discount_transaction dt WHERE dt.month='"+date+"' AND companyId= ct.`companyId`) AS adjCheck"
				+ " FROM `company_tab` ct ORDER BY ct.`companyOrder`";
			try{
		    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
					if(rows != null && rows.size()>0) {
						for(Map row : rows){
							StockLiftCompanyWithChecksBean bean = new StockLiftCompanyWithChecksBean();
							bean.setCompanyId(row.get("companyId")!=null?Long.parseLong(row.get("companyId").toString()):0);
							bean.setColor(row.get("color")!=null?row.get("color").toString():"");
							bean.setEnterprise(row.get("enterprise")!=null?row.get("enterprise").toString():"");
							bean.setRental(row.get("rentalAmount")!=null?Double.parseDouble(row.get("rentalAmount").toString()):0);
							bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
							bean.setCompanyOrder(row.get("companyOrder")!=null?Long.parseLong(row.get("companyOrder").toString()):0);
							bean.setTotalDiscount(row.get("discountAmount")!=null?Double.parseDouble(row.get("discountAmount").toString()):0);
							bean.setArrears(row.get("arrears")!=null?Double.parseDouble(row.get("arrears").toString()):0);
							bean.setAdjAmt(row.get("adjCheck")!=null?Double.parseDouble(row.get("adjCheck").toString()):0);
							bean.setCheckAmt(row.get("checksAmt")!=null?Double.parseDouble(row.get("checksAmt").toString()):0);
							bean.setDiscountTransactionBeanList(getChecksDetailsWithImage(row.get("companyId")!=null?Long.parseLong(row.get("companyId").toString()):0,date));
							StocktransactionBeanList.add(bean);
						}
					}
			}catch(Exception e){
				e.printStackTrace();
				return StocktransactionBeanList;
			}
		
		return StocktransactionBeanList;
	}


	private List<DiscountTransactionBean> getChecksDetailsWithImage(long companyId, String date) {
		List<DiscountTransactionBean>  transactionBeanList = new ArrayList<DiscountTransactionBean>();
		String QUERY="SELECT dt.companyId,dt.month,dt.transactionType,dt.transactionAmt,UPPER(DATE_FORMAT(dt.transactionDate, '%d ' '%b ' '%Y')) AS transactionDate,dt.adjCheck,dt.imageName,dt.imagePath,"
				+ "dt.bank  FROM discount_transaction dt WHERE dt.month='"+date+"' AND dt.companyId="+companyId+"";
		try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						DiscountTransactionBean bean = new DiscountTransactionBean();
						bean.setCompanyId(row.get("companyId")!=null?Long.parseLong(row.get("companyId").toString()):0);
						bean.setMonths(row.get("month")!=null?row.get("month").toString():"");
						bean.setTransactionType(row.get("transactionType")!=null?row.get("transactionType").toString():"");
						bean.setTransactionAmt(row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0);
						bean.setTransactionDate(row.get("transactionDate")!=null?row.get("transactionDate").toString():"");
						bean.setAdjCheck(row.get("adjCheck")!=null?Double.parseDouble(row.get("adjCheck").toString()):0);
						bean.setImageName(row.get("imageName")!=null?row.get("imageName").toString():"");
						bean.setNgalleryImage(row.get("imagePath")!=null?row.get("imagePath").toString():"");
						bean.setBank(row.get("bank")!=null?row.get("bank").toString():"");
						transactionBeanList.add(bean);
					}
				}
		}catch(Exception e){
			e.printStackTrace();
			return transactionBeanList;
		}
		return transactionBeanList;
	}

	/**
	 * This method is used to get discount to till selected month based on selected company
	 * */
	@Override
	public List<DiscountAndMonthBean> getDiscountCompanyWiseForAllMonth(String date, int company) {
		int storeId = userSession.getStoreId();
		 List<DiscountAndMonthBean>  discountAndMonthBeanList = new ArrayList<DiscountAndMonthBean>();
		ArrayList<String> arrlist = new ArrayList<String>();
		//arrlist.add("2017-10-01");
		// String QUERY="SELECT DISTINCT saleDate FROM sale WHERE `saleDate` LIKE '%-01' AND saleDate <='"+date+"' ORDER BY saleDate DESC;";
		String QUERY = "SELECT DISTINCT DATE_FORMAT(saleDate ,'%Y-%m-01') AS saleDate FROM sale WHERE  saleDate <='"+date+"' AND store_id="+storeId+" ORDER BY saleDate DESC";
	     try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						arrlist.add(row.get("saleDate")!=null?row.get("saleDate").toString():"");
					}
				}
				if(arrlist.size() > 0){
					for (String saledate : arrlist) {
						double totalDisc=0;String dateVal=null;double discountVal=0;double totalSum=0;
						DiscountAndMonthBean discountAndMonthBean = new DiscountAndMonthBean();
						List<DiscountMonthWiseBean>   discountMonthWiseBeanList = new ArrayList<DiscountMonthWiseBean>();
					 List<Map<String, Object>> secrows = jdbcTemplate.queryForList("SELECT DISTINCT p.realBrandNo,p.matchName,p.`matchs`,p.packQty,p.packType,UPPER(DATE_FORMAT('"+saledate+"', '%b' ' %y')) AS date,"
					 		+ "(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,i.packQtyRate,"
					 		+ "(SELECT d.discountAmount FROM stock_lift_with_discount d WHERE d.store_id = "+storeId+" AND d.brandNo=p.realBrandNo AND d.stockDiscountDate='"+saledate+"') AS discountAmount,"
					 		+ "(SELECT d.adjustment FROM stock_lift_with_discount d WHERE d.store_id = "+storeId+" AND d.brandNo=p.realBrandNo AND d.stockDiscountDate='"+saledate+"') AS adjustment,"
					 		+ "(SELECT dt.totalDiscountAmount FROM stock_lift_with_discount dt WHERE dt.store_id = "+storeId+" AND dt.brandNo=p.realBrandNo AND dt.stockDiscountDate='"+saledate+"')  "
					 		+ " AS totalDiscountAmount, SUM(i.caseQty) AS caseQty,CEIL(SUM(i.receivedBottles/i.packQty)) AS totalcases,"
					 		+ " (SELECT SUM(dt.totalDiscountAmount) FROM stock_lift_with_discount dt WHERE dt.store_id = "+storeId+" AND dt.stockDiscountDate='"+saledate+"' AND dt.companyName="+company+")  "
					 		+ " AS discountSum  FROM product p LEFT JOIN invoice i  ON p.`brandNoPackQty`  = i.`brandNoPackQty`  WHERE i.`store_id` = "+storeId+" AND p.`company`="+company+" AND "
					 		+ " i.`invoiceDateAsPerSheet`>='"+saledate+"' "
					 		+ " AND `invoiceDateAsPerSheet`<=(SELECT LAST_DAY('"+saledate+"' - INTERVAL 0 MONTH)) GROUP BY p.realBrandNo");
					 for(Map secrow : secrows){
						 DiscountMonthWiseBean bean = new DiscountMonthWiseBean();
						  discountVal += secrow.get("discountAmount")!=null?Double.parseDouble(secrow.get("discountAmount").toString()):0;
						  totalSum = secrow.get("discountAmount")!=null?Double.parseDouble(secrow.get("discountAmount").toString()):0;
						  if(totalSum>0){
							  bean.setMatch(secrow.get("matchs")!=null?Long.parseLong(secrow.get("matchs").toString()):0);
						 bean.setBrand(secrow.get("matchName")!=null?secrow.get("matchName").toString():"");
						 bean.setCase((secrow.get("totalcases")!=null?Long.parseLong(secrow.get("totalcases").toString()):0)+(secrow.get("adjustment")!=null?Long.parseLong(secrow.get("adjustment").toString()):0));
						 bean.setDiscount(secrow.get("discountAmount")!=null?Double.parseDouble(secrow.get("discountAmount").toString()):0);
						 bean.setTotal(secrow.get("totalDiscountAmount")!=null?Double.parseDouble(secrow.get("totalDiscountAmount").toString()):0);
						 bean.setDate(saledate);
						 discountMonthWiseBeanList.add(bean);
						 totalDisc=secrow.get("discountSum")!=null?Double.parseDouble(secrow.get("discountSum").toString()):0;
						 dateVal=secrow.get("date")!=null?secrow.get("date").toString():"";
						 }
					 }
					 if(discountVal >0){
					 discountAndMonthBean.setDiscountMonthWiseBean(discountMonthWiseBeanList);
					 discountAndMonthBean.setMonth(dateVal);
					 discountAndMonthBean.setTotalDiscount(totalDisc);
					 discountAndMonthBeanList.add(discountAndMonthBean);
					 }
					
					}
				}
	     }catch(Exception e){
	    	 log.log(Level.WARN, "getDiscountCompanyWiseForAllMonth in PoizonSecondDaoImpl exception", e);
				e.printStackTrace();
				return discountAndMonthBeanList;
	     }
		return discountAndMonthBeanList;
	}

	/**
	 * This method is used to get discount for all months with received checks based on selected company
	 * */
	@Override
	public List<DiscountAsPerCompanyBeen> getDiscountCompanyWiseForAllMonthWithReceived(int company) {
		List<DiscountAsPerCompanyBeen> discountBeanList = new ArrayList<DiscountAsPerCompanyBeen>();
		String QUERY="SELECT month,transactionAmt,adjCheck,DATE_FORMAT(transactionDate, '%d-%b-%Y') AS transactionDate,bank,transactionType,checkNo,fromBank FROM discount_transaction WHERE store_id = "+userSession.getStoreId()+" AND companyId="+company+" AND received=1 AND transactionDate IS NOT NULL ORDER BY MONTH DESC";
		     try{
		    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
					if(rows != null && rows.size()>0) {
						for(Map row : rows){
							DiscountAsPerCompanyBeen bean = new DiscountAsPerCompanyBeen();
							bean.setEnterprise(row.get("transactionDate")!=null?row.get("transactionDate").toString():"");
							/*bean.setDiscountAmount((row.get("discountamount")!=null?Double.parseDouble(row.get("discountamount").toString()):0)
									+(row.get("rental")!=null?Double.parseDouble(row.get("rental").toString()):0));*/
							bean.setChequeAmount(row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0);
							bean.setAdjustmentCheque(row.get("adjCheck")!=null?Double.parseDouble(row.get("adjCheck").toString()):0);
							bean.setCompanyName(row.get("fromBank")!=null?row.get("fromBank").toString():"");
							bean.setComment(row.get("transactionType")!=null?row.get("transactionType").toString():"");
							bean.setColor(row.get("checkNo")!=null?row.get("checkNo").toString():"");
							discountBeanList.add(bean);
						}
					}
		     }catch(Exception e){
		    	 e.printStackTrace();
		    	 return discountBeanList;
		     }
		
		return discountBeanList;
	}

	/**
	 * This method is used to get rental amount for all months based on selected company and till date
	 * */
	@Override
	public double getUpToMonthRental(String date, int company) {
		double rental = 0;
		try{
		 List<Map<String, Object>> rentalrows = jdbcTemplate.queryForList("SELECT COALESCE(SUM(ds.`rentalAmount`),0) as rental "
	    	 		+ " FROM `rental_tab` ds WHERE ds.store_id = "+userSession.getStoreId()+" AND ds.rentalDate<='"+date+"' AND ds.`company`="+company+"");
				if(rentalrows != null && rentalrows.size()>0) {
					for(Map rentalrow : rentalrows){
						rental = rentalrow.get("rental")!=null?Double.parseDouble(rentalrow.get("rental").toString()):0;
					}
				}
		}catch(Exception e){
			e.printStackTrace();
			return rental;
			
		}
		return rental;
	}
	/**
	 * This method is used to save investment details
	 * */
	@Override
	public String addNewInvestment(InvestmentBean investmentBean) {
		log.info("PoizonSecondDaoImpl.addNewInvestment");
		 String result = null;
		 String QUERY="insert into investment(amount,type,bankName,date,store_id) values(?,?,?,?,?)";
		try{
				int val=jdbcTemplate.update(QUERY,new Object[]{investmentBean.getAmount(),investmentBean.getTransactionType(),investmentBean.getBank(),investmentBean.getBankdate(),userSession.getStoreId()});
				if(val>0)
					result="Investment inserted successfully";
		}catch(Exception e){
			result="something went wrong";
			log.log(Level.WARN, "addNewInvestment in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
			}
		
		return result;
	}
	/**
	 * This method is used to get investment details
	 * */
	@Override
	public List<InvestmentBean> getInvestmentData(String date) {
		List<InvestmentBean> listBean = new ArrayList<InvestmentBean>();
		String QUERY="SELECT amount,type,bankName FROM investment WHERE date='"+date+"' AND `store_id`="+userSession.getStoreId()+"";
		try{
			 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
					if(rows != null && rows.size()>0) {
						for(Map row : rows){
							InvestmentBean bean = new InvestmentBean();
							bean.setAmount(row.get("amount")!=null?Double.parseDouble(row.get("amount").toString()):0);
							bean.setBank(row.get("bankName")!=null?row.get("bankName").toString():"");
							bean.setTransactionType(row.get("type")!=null?row.get("type").toString():"");
							listBean.add(bean);
						}
					}
			}catch(Exception e){
				e.printStackTrace();
				return listBean;
			}
		return listBean;
	}


	@Override
	public ProductWithStatement getProductWithInvestment(long brandNo) {
		ProductWithStatement bean = new ProductWithStatement();
		String QUERY="SELECT p.brandNo,ROUND(SUM(s.sale/p.packQty),0) sale,"
				+ "ROUND(AVG((SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty ORDER BY `invoiceDate` DESC LIMIT 1)),0)"
				+ " AS casePrice FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`>=(SELECT CONCAT(DATE_FORMAT(LAST_DAY((SELECT saleDate FROM sale ORDER BY saleDate DESC LIMIT 1) - INTERVAL 1 MONTH),'%Y-%m-'),'01')) "
				+ "AND `saleDate`<=(SELECT LAST_DAY((SELECT saleDate FROM sale ORDER BY saleDate DESC LIMIT 1) - INTERVAL 1 MONTH )) AND brandNo="+brandNo+"";
		try{
			 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
					if(rows != null && rows.size()>0) {
						for(Map row : rows){
							bean.setBrandNo(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0);
							bean.setSale(row.get("sale")!=null?Long.parseLong(row.get("sale").toString()):0);
							//bean.setInvoicePrice(row.get("casePrice")!=null?Long.parseLong(row.get("casePrice").toString()):0);
							bean.setInvoicePrice(row.get("casePrice")!=null?Double.parseDouble(row.get("casePrice").toString()):0);
						}
					}
			}catch(Exception e){
				e.printStackTrace();
				return bean;
			}
		return bean;
	}

	/**
	 * This method is used to get discount estimate details
	 * */
	@Override
	public List<DiscountEstimationDetails> getDiscountEsitmateDetails(String date) {
		List<DiscountEstimationDetails> discountList = new ArrayList<DiscountEstimationDetails>();
		String startDate=null;
		String endDate=null;
		String QUERY="SELECT DISTINCT p1.brandNo, p1.`shortBrandName`,p1.`category`,"
				+ "(SELECT t.color FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS color,"
				+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyOrder,"
				+ "(SELECT t.categoryName FROM `category_tab` t WHERE p1.`category`=t.`categoryId`) AS categoryName,p1.`company`,"
				+ " (SELECT t.companyName FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyName,"
				+ " ROUND(AVG((SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p1.brandNoPackQty AND i.invoiceDateAsPerSheet"
				+ " <= (SELECT LAST_DAY('"+date+"'))  ORDER BY `invoiceId` DESC LIMIT 1)),0) AS casePrice,"
				+ " (SELECT caseRate FROM discount d WHERE d.brandNo=p1.brandNo AND estimateDate='"+date+"') AS estimateCaseRate,"
				+ " (SELECT inhouseStock FROM discount d WHERE d.brandNo=p1.brandNo AND estimateDate='"+date+"') AS estimateinhouseStock,"
				+ "(SELECT targetCase FROM discount d WHERE d.brandNo=p1.brandNo AND estimateDate='"+date+"') AS targetCase,"
				+ "(SELECT discPerCase FROM discount d WHERE d.brandNo=p1.brandNo AND estimateDate='"+date+"') AS discPerCase,"
				+ "(SELECT investment FROM discount d WHERE d.brandNo=p1.brandNo AND estimateDate='"+date+"') AS investment,"
				+ "(SELECT lastMonthSold FROM discount d WHERE d.brandNo=p1.brandNo AND estimateDate='"+date+"') AS lastMonthSold,"
				+ "(SELECT clearnsPeriodWithInhouseStock FROM discount d WHERE d.brandNo=p1.brandNo AND estimateDate='"+date+"') AS clearnsPeriodWithInhouseStock,"
				+ "(SELECT clearnsPeriodWithoutInhouseStock FROM discount d WHERE d.brandNo=p1.brandNo AND estimateDate='"+date+"') AS clearnsPeriodWithoutInhouseStock,"
				+ "(SELECT pending FROM discount d WHERE d.brandNo=p1.brandNo AND estimateDate='"+date+"') AS pending,"
				+ "(SELECT totalDiscount FROM discount d WHERE d.brandNo=p1.brandNo AND estimateDate='"+date+"') AS totalDiscount,"
				+ "(SELECT rateOfInterestWithInHouseStock FROM discount d WHERE d.brandNo=p1.brandNo AND estimateDate='"+date+"') "
				+ "AS rateOfInterestWithInHouseStock,"
				+ "(SELECT rateOfInterestWithoutInHouseStock FROM discount d WHERE d.brandNo=p1.brandNo AND estimateDate='"+date+"') "
				+ "AS rateOfInterestWithoutInHouseStock,(SELECT estimateDate FROM discount d WHERE d.brandNo=p1.brandNo AND estimateDate='"+date+"') "
				+ "AS estimateDate,(SELECT LAST_DAY('"+date+"')) AS endDate"
				+ "  FROM product p1 WHERE p1.`active`=1 GROUP BY p1.brandNo ORDER BY p1.brandNo ASC";
		System.out.println(QUERY);
		try{
			 List<Map<String, Object>> dateRow = jdbcTemplate.queryForList("SELECT `saleDate` FROM sale where saleDate<=(SELECT LAST_DAY('"+date+"')) ORDER BY `saleId` DESC LIMIT 1");
			 if(dateRow != null && dateRow.size()>0) {
					for(Map row : dateRow){
						endDate=row.get("saleDate")!=null?row.get("saleDate").toString():"";
					}
			 }
			 if(endDate != null && endDate !="" ){
			    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			    Date date1 = null;
				try {
					date1 = dateFormat.parse(endDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				Calendar cal = new GregorianCalendar();
				cal.setTime(date1);
				cal.add(Calendar.DAY_OF_MONTH, -30);
				Date today60 = cal.getTime();
				startDate = dateFormat.format(today60);
			 }
		System.out.println("startDate>>>"+startDate+"  endDate>>>"+endDate);
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
		if(rows != null && rows.size()>0) {
			for(Map row : rows){
				DiscountEstimationDetails bean = new DiscountEstimationDetails();
				bean.setBrandNo(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0);
				bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
                bean.setCompanyColor(row.get("color")!=null?row.get("color").toString():"");
                bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
				bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
				bean.setCompanyId(row.get("company")!=null?Long.parseLong(row.get("company").toString()):-1);
				bean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
				bean.setTargetCase(row.get("targetCase")!=null?Long.parseLong(row.get("targetCase").toString()):0);
				bean.setDicsPerCase(row.get("discPerCase")!=null?Double.parseDouble(row.get("discPerCase").toString()):0);
				StockInBean stockInBean = getLastMonthSoldData(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0, startDate, endDate);
				bean.setLiftedCase(stockInBean.getQtyBtl());
				if((row.get("targetCase")!=null?Long.parseLong(row.get("targetCase").toString()):0) > 0 || (row.get("discPerCase")!=null?Double.parseDouble(row.get("discPerCase").toString()):0) > 0){
					bean.setLastMonthSold(row.get("lastMonthSold")!=null?Double.parseDouble(row.get("lastMonthSold").toString()):0);
					bean.setInHouseStock(row.get("estimateinhouseStock")!=null?Double.parseDouble(row.get("estimateinhouseStock").toString()):0);
					bean.setCaseRate(row.get("estimateCaseRate")!=null?Double.parseDouble(row.get("estimateCaseRate").toString()):0);
					bean.setPending(row.get("pending")!=null?Long.parseLong(row.get("pending").toString()):0);
				}else{
					bean.setLastMonthSold(stockInBean.getTotalPrice());
					bean.setInHouseStock(stockInBean.getStockInId());
					bean.setCaseRate(row.get("casePrice")!=null?Double.parseDouble(row.get("casePrice").toString()):0);
					bean.setPending((long)0);
				}
				
				//bean.setCaseRateEstimation(row.get("estimateCaseRate")!=null?Double.parseDouble(row.get("estimateCaseRate").toString()):0);
				//bean.setEstimateinhouseStock(row.get("estimateinhouseStock")!=null?Long.parseLong(row.get("estimateinhouseStock").toString()):0);
				bean.setTargetCase(row.get("targetCase")!=null?Long.parseLong(row.get("targetCase").toString()):0);
				bean.setDicsPerCase(row.get("discPerCase")!=null?Double.parseDouble(row.get("discPerCase").toString()):0);
				bean.setInvestment(row.get("investment")!=null?Double.parseDouble(row.get("investment").toString()):0);
				bean.setClearnsPeriod(row.get("clearnsPeriodWithInhouseStock")!=null?Double.parseDouble(row.get("clearnsPeriodWithInhouseStock").toString()):0);
				bean.setClearnsPeriodWithoutInhouseStock(row.get("clearnsPeriodWithoutInhouseStock")!=null?Double.parseDouble(row.get("clearnsPeriodWithoutInhouseStock").toString()):0);
				bean.setRateOfInterestWithInhouse(row.get("rateOfInterestWithInHouseStock")!=null?Double.parseDouble(row.get("rateOfInterestWithInHouseStock").toString()):0);
				bean.setRateOfInterestWithoutInhouse(row.get("rateOfInterestWithoutInHouseStock")!=null?Double.parseDouble(row.get("rateOfInterestWithoutInHouseStock").toString()):0);
				bean.setDate(row.get("estimateDate")!=null?row.get("estimateDate").toString():"");
				bean.setTotalDiscount(row.get("totalDiscount")!=null?Double.parseDouble(row.get("totalDiscount").toString()):0);
				discountList.add(bean);
			}
		}
		}catch(Exception e){
			e.printStackTrace();
			return discountList;
		}
	return discountList;
	}

	private StockInBean getIndentEstimateSoldDataAndStock(long brandNo,String startDate,String endDate) {
		StockInBean bean = new StockInBean();
		int storeId= userSession.getStoreId();
		String QUERY="SELECT ROUND(COALESCE(SUM(s.sale/p.packQty),0),0) AS sale"
				+ " FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`>='"+startDate+"'"
				+ "AND `saleDate`<='"+endDate+"' AND realBrandNo="+brandNo+" AND s.store_id="+storeId;
		String QUERY2="SELECT ROUND(COALESCE(SUM(s.closing/p.packQty),0) + (SELECT COALESCE(SUM(r.`receivedBottles`/p.packQty),0) FROM product p INNER JOIN "
				+ "invoice r ON p.`brandNoPackQty` = r.`brandNoPackQty` AND r.`invoiceDate` > '"+endDate+"' AND r.`invoiceDate` <= LAST_DAY('"+endDate+"') AND realBrandNo="+brandNo+" AND r.store_id="+storeId+"),0) AS inHouseStock "
						+ "FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`='"+endDate+"' AND realBrandNo="+brandNo+" AND s.store_id="+storeId;
		String QUERY3="SELECT COALESCE(ROUND(CEIL(SUM(s.receivedBottles/p.packQty))),0) AS liftedCase "
				+ "FROM product p INNER JOIN invoice s ON p.`brandNoPackQty` = s.`brandNoPackQty`"
				+ " AND `invoiceDateAsPerSheet`>=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+endDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01')) AND "
				+ " invoiceDateAsPerSheet <= (SELECT LAST_DAY('"+endDate+"')) AND p.realBrandNo="+brandNo+" AND s.store_id="+storeId;
		String QUERY4 = "SELECT DISTINCT ROUND(AVG((SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty "
				+ "AND i.invoiceDateAsPerSheet <= (SELECT LAST_DAY('"+endDate+"')) AND i.store_id="+storeId+" ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1)),0) AS casePrice "
						+ "FROM product p WHERE p.`realBrandNo`="+brandNo+"";
		String QUERY5 = "SELECT GROUP_CONCAT( DISTINCT COALESCE(DATE_FORMAT(i.`invoiceDateAsPerSheet`,'%D'),'') ORDER BY `invoiceDateAsPerSheet` DESC) AS stockDate "
				+ "FROM product p INNER JOIN invoice i ON p.`brandNoPackQty` = i.`brandNoPackQty` AND "
				+ " i.`invoiceDateAsPerSheet`>=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+endDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01')) AND"
						+ " i.`invoiceDateAsPerSheet`<=(SELECT LAST_DAY('"+endDate+"')) AND i.`receivedBottles` > 0 AND p.`realBrandNo` = "+brandNo+" AND store_id="+storeId;
		
		try{
			// cases = jdbcTemplate.queryForObject(QUERY, Double.class);
			 bean.setTotalPrice(jdbcTemplate.queryForObject(QUERY, Double.class));// this is for getting total sale for a product based on selected month
			 bean.setStockInId(jdbcTemplate.queryForObject(QUERY2, Double.class));// this is for getting inhouse stock for a product based on selected month
			 bean.setQtyBtl(jdbcTemplate.queryForObject(QUERY3, Double.class)); // this is for getting Lifted stock for a product based on selected month
			 
			 bean.setCasePrice(jdbcTemplate.queryForObject(QUERY4, Double.class));
			 bean.setDate(jdbcTemplate.queryForObject(QUERY5, String.class));
			}catch(Exception e){
				e.printStackTrace();
				return bean;
			}
		return bean;
	}
	private StockInBean getLastMonthSoldData(long brandNo,String startDate,String endDate) {
		StockInBean bean = new StockInBean();
		String QUERY="SELECT ROUND(SUM(s.sale/p.packQty),0) AS sale"
				+ " FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`>='"+startDate+"'"
				+ "AND `saleDate`<='"+endDate+"' AND brandNo="+brandNo+"";
		String QUERY2="SELECT COALESCE(ROUND(SUM(s.closing/p.packQty),0),0) AS inHouseStock  "
				+ "FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`='"+endDate+"' AND brandNo="+brandNo+"";
		String QUERY3="SELECT COALESCE(ROUND(CEIL(SUM(s.receivedBottles/p.packQty))),0) AS liftedCase "
				+ "FROM product p INNER JOIN invoice s ON p.`brandNoPackQty` = s.`brandNoPackQty`"
				+ " AND `invoiceDateAsPerSheet`>=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+endDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01')) AND "
				+ " invoiceDateAsPerSheet <= (SELECT LAST_DAY('"+endDate+"')) AND p.brandNo="+brandNo+"";
		try{
			// cases = jdbcTemplate.queryForObject(QUERY, Double.class);
			 bean.setTotalPrice(jdbcTemplate.queryForObject(QUERY, Double.class));
			 bean.setStockInId(jdbcTemplate.queryForObject(QUERY2, Double.class));
			 bean.setQtyBtl(jdbcTemplate.queryForObject(QUERY3, Double.class));
			}catch(Exception e){
				e.printStackTrace();
				return bean;
			}
		return bean;
	}

	/**
	 * This method is used to save discount estimate details
	 * */
	@Override
	public String saveDiscountEsitmateDetails(JSONArray discountDetails) {
		    saveDiscountInStockLifting(discountDetails);
		
		String QUERY="insert into discount (brandNo,caseRate,inhouseStock,targetCase,discPerCase,totalDiscount,investment,lastMonthSold,clearnsPeriodWithoutInhouseStock,"
				+ "clearnsPeriodWithInhouseStock,rateOfInterestWithInHouseStock,rateOfInterestWithoutInHouseStock,estimateDate,pending) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		String result="Data Inserted Successfully";
		synchronized(this){
		try {
			for (int i = 0; i < discountDetails.length(); ++i) {
				 JSONObject rec;
				    rec = discountDetails.getJSONObject(i);
				    	int val = jdbcTemplate.update("UPDATE discount SET targetCase="+rec.get("target")+", discPerCase="+rec.get("discountAmount")+""
				    			+ ", totalDiscount="+Double.parseDouble(rec.getString("totalDiscount"))+", investment="+Double.parseDouble(rec.getString("investment"))+", clearnsPeriodWithoutInhouseStock="+Double.parseDouble(rec.getString("estimationMonthExcludeInhouse"))+""
				    			+ ", clearnsPeriodWithInhouseStock="+Double.parseDouble(rec.getString("estimationMonthIncludeInhouse"))+""
				    			+ ", rateOfInterestWithInHouseStock="+Double.parseDouble(rec.getString("rateOfInterestWithInhouse"))+""
				    			+ ", rateOfInterestWithoutInHouseStock="+Double.parseDouble(rec.getString("rateOfInterestWithoutInhouse"))+""
				    			+ ", pending="+rec.get("pending")+""
				    			+ " WHERE brandNo = "+Integer.parseInt(rec.getString("brandNo"))+" AND estimateDate = '"+rec.getString("date")+"'");
				    
				    	 if((val < 0) || (val == 0)){
				    		jdbcTemplate.update(QUERY,new Object[]{Integer.parseInt(rec.getString("brandNo")),Double.parseDouble(rec.getString("caseRate")),
							    		Integer.parseInt(rec.getString("inHouseStock")),rec.get("target"),rec.get("discountAmount"),
							    		Double.parseDouble(rec.getString("totalDiscount")),Double.parseDouble(rec.getString("investment")),Integer.parseInt(rec.getString("lastMonthSold")),
							    		Double.parseDouble(rec.getString("estimationMonthExcludeInhouse")),Double.parseDouble(rec.getString("estimationMonthIncludeInhouse")),Double.parseDouble(rec.getString("rateOfInterestWithInhouse")),
							    		Double.parseDouble(rec.getString("rateOfInterestWithoutInhouse")),rec.getString("date"),rec.get("pending")});
							    
						    }
				    
				    
				}
			} catch (JSONException e) {
			// TODO Auto-generated catch block
			result="Something Went Wrong.";
			log.log(Level.WARN, "saveDiscountEsitmateDetails in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
		}
		}
		return result;
	}
public void saveDiscountInStockLifting(JSONArray discountDetails){
	String QUERY="INSERT INTO `stock_lift_with_discount`(`brandNo`,companyName,discountAmount,totalDiscountAmount,stockDiscountDate,adjustment,target) VALUES(?,?,?,?,?,?,?)";
	synchronized(this){
		try {
			for (int i = 0; i < discountDetails.length(); ++i) {
			 JSONObject rec;
			    rec = discountDetails.getJSONObject(i);
			    List<Map<String, Object>> flag = jdbcTemplate.queryForList("SELECT * FROM stock_lift_with_discount WHERE brandNo = "+Integer.parseInt(rec.getString("brandNo"))+" AND stockDiscountDate= '"+rec.getString("date")+"'");
			    
			   // System.out.println("flag>> "+flag.size());
			    if(flag.size()>0 && flag != null){
			    	int val = jdbcTemplate.update("UPDATE stock_lift_with_discount SET discountAmount="+rec.getDouble("discountAmount")+", totalDiscountAmount="+rec.getDouble("liftedAndDiscount")+",adjustment="+0+",target="+rec.getInt("target")+" WHERE brandNo = "+Integer.parseInt(rec.getString("brandNo"))+" AND stockDiscountDate= '"+rec.getString("date")+"'");
			    }else{
			    	 int val=0;
			    	if(rec.getDouble("liftedAndDiscount") > 0 || rec.getInt("target") > 0)
			     val = jdbcTemplate.update(QUERY,new Object[]{Integer.parseInt(rec.getString("brandNo")),Integer.parseInt(rec.getString("companyId")),rec.getDouble("discountAmount"),rec.getDouble("liftedAndDiscount"),rec.getString("date"),0,rec.getInt("target")});
			    }
			    
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			log.log(Level.WARN, "saveDiscountInStockLifting in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
		}
		}
	
}
/**
 * This method is used to get and calculate profit or loss data for selecetd start and end date
 * */
	@Override
	public ProfitAndLossBean getProfitAndLossData(String firstDate, String lastDate) {
		ProfitAndLossBean profitAndLossBean = new ProfitAndLossBean();
		List<AdministrationExpensesBean> administrationExpensesBeanList = new ArrayList<>();
		String QUERY="SELECT ROUND(SUM(totalPrice), 2) AS sale,"
				+ "(SELECT ROUND(SUM(`mrpRoundOff`), 2) FROM invoice_mrp_round_off WHERE `mrpRoundOffDateAsCopy`>='"+firstDate+"' AND `mrpRoundOffDateAsCopy`<='"+lastDate+"' AND store_id = "+userSession.getStoreId()+")"
				+ " AS mrpRoundOff,(SELECT ROUND(SUM(`tcsVal`), 2) FROM invoice_mrp_round_off"
				+ " WHERE `mrpRoundOffDateAsCopy`>='"+firstDate+"' AND `mrpRoundOffDateAsCopy`<='"+lastDate+"' AND store_id = "+userSession.getStoreId()+") AS tcsVal,"
				+ "(SELECT ROUND(SUM(`totalPrice`), 2) FROM invoice WHERE `invoiceDateAsPerSheet`>='"+firstDate+"' AND `invoiceDateAsPerSheet`<='"+lastDate+"' AND store_id = "+userSession.getStoreId()+")"
				+ " AS purchase,(SELECT SUM(transactionAmt)  FROM discount_transaction WHERE transactionDate>='"+firstDate+"' AND "
				+ "transactionDate <='"+lastDate+"' AND received=1 AND store_id = "+userSession.getStoreId()+" AND transactionDate IS NOT NULL )AS transactionAmt, (SELECT SUM(turnoverTax) FROM invoice_mrp_round_off WHERE "
				+ "mrpRoundOffDateAsCopy >='"+firstDate+"' AND mrpRoundOffDateAsCopy <='"+lastDate+"' AND store_id = "+userSession.getStoreId()+") AS turnoverTax,(SELECT SUM(breckageAmount) "
				+ "FROM breckage_tab WHERE breckageDate >='"+firstDate+"' AND breckageDate <='"+lastDate+"' AND store_id = "+userSession.getStoreId()+") AS breckageAmount,(SELECT COALESCE(SUM(amount),0) "
				+ "FROM monthlyexpenses WHERE debitCredit = 0 AND SUBJECT = 11 AND monthlyExpensesDate >='"+firstDate+"' AND monthlyExpensesDate <='"+lastDate+"' AND store_id = "+userSession.getStoreId()+")"
				+ " AS licenseFee,(SELECT COALESCE(SUM(amount),0) FROM monthlyexpenses WHERE debitCredit = 1 AND SUBJECT = 10 AND monthlyExpensesDate >='"+firstDate+"' "
				+ "AND monthlyExpensesDate <='"+lastDate+"' AND store_id = "+userSession.getStoreId()+") AS sittingAmt,"
				+ "(SELECT COALESCE(SUM(amount),0) FROM monthlyexpenses WHERE debitCredit = 0 AND SUBJECT != 11 AND SUBJECT !=10 AND monthlyExpensesDate >='"+firstDate+"' "
				+ " AND monthlyExpensesDate <='"+lastDate+"' AND store_id = "+userSession.getStoreId()+") AS monthlyexpense "
				+ " FROM sale WHERE saleDate>='"+firstDate+"' AND saleDate<='"+lastDate+"' AND store_id = "+userSession.getStoreId()+"";
		//System.out.println(QUERY);
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						profitAndLossBean.setMrpRoundOff(row.get("mrpRoundOff")!=null?Double.parseDouble(row.get("mrpRoundOff").toString()):0);
						profitAndLossBean.setTcsAmt(row.get("tcsVal")!=null?Double.parseDouble(row.get("tcsVal").toString()):0);
						profitAndLossBean.setPurchase(row.get("purchase")!=null?Double.parseDouble(row.get("purchase").toString()):0);
						profitAndLossBean.setSales(row.get("sale")!=null?Double.parseDouble(row.get("sale").toString()):0);
						profitAndLossBean.setReceivedAmt(row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0);
						profitAndLossBean.setTurnOverTax(row.get("turnoverTax")!=null?Double.parseDouble(row.get("turnoverTax").toString()):0);
						profitAndLossBean.setBreackage(row.get("breckageAmount")!=null?Double.parseDouble(row.get("breckageAmount").toString()):0);
						profitAndLossBean.setLicenseFee(row.get("licenseFee")!=null?Double.parseDouble(row.get("licenseFee").toString()):0);
						profitAndLossBean.setSittingAmt(row.get("sittingAmt")!=null?Double.parseDouble(row.get("sittingAmt").toString()):0);
						profitAndLossBean.setMonthlyExpenses(row.get("monthlyexpense")!=null?Double.parseDouble(row.get("monthlyexpense").toString()):0);
						StockInBean stockInBean = getOpeningAndClosingStock(firstDate,lastDate);
						profitAndLossBean.setOpeningStock(stockInBean.getStockInId());
						profitAndLossBean.setClosingStock(stockInBean.getQtyBtl());
						profitAndLossBean.setAdministrationExpensesBean(getAdministrationExpenses(firstDate,lastDate));
					}
			 }
		}catch(Exception e){
			e.printStackTrace();
			return profitAndLossBean;
		}
		
		return profitAndLossBean;
	}


	private List<AdministrationExpensesBean> getAdministrationExpenses(String firstDate, String lastDate) {
		String QUERY = "SELECT DISTINCT ex.categoryId,UPPER(ex.expenseName) AS expenseName,"
				+ "(SELECT SUM(ch.expenseChildAmount) FROM expense_child ch WHERE ch.categoryId=ex.categoryId "
				+ "AND (SELECT m.expenseMasterId FROM expense_master m WHERE ch.expenseMasterId = m.expenseMasterId "
				+ " AND m.expenseMasterDate >='"+firstDate+"' AND m.expenseMasterDate <='"+lastDate+"' AND m.store_id = "+userSession.getStoreId()+") = ch.expenseMasterId) AS amount"
				+ " FROM expense_category ex INNER JOIN expense_child ch ON ex.categoryId = ch.categoryId HAVING amount > 0";
		List<AdministrationExpensesBean> administrationExpensesList = new ArrayList<AdministrationExpensesBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						AdministrationExpensesBean bean = new AdministrationExpensesBean();
						bean.setName(row.get("expenseName")!=null?row.get("expenseName").toString():"");
						bean.setValue(row.get("amount")!=null?Double.parseDouble(row.get("amount").toString()):0);
						administrationExpensesList.add(bean);
					}
			 }
		}catch(Exception e){
			e.printStackTrace();
			return administrationExpensesList;
		}
		return administrationExpensesList;
	}


	private StockInBean getOpeningAndClosingStock(String firstDate, String lastDate) {
		StockInBean bean = new StockInBean();
		String QUERY = "SELECT p.shortBrandName,p.realBrandNo,p.brandNoPackQty,(SELECT (s1.opening+s1.received) FROM sale s1 WHERE saleDate='"+firstDate+"' "
				+ "AND s1.brandNoPackQty = p.brandNoPackQty AND s1.store_id = "+userSession.getStoreId()+") AS openingStock,(SELECT SingleBottelRate FROM invoice inv "
				+ "WHERE inv.brandNoPackQty=p.brandNoPackQty AND invoiceDateAsPerSheet <='"+firstDate+"' AND inv.store_id = "+userSession.getStoreId()+" ORDER BY invoiceDateAsPerSheet DESC LIMIT 1) "
				+ "AS openingStockPrice, (SELECT s1.closing FROM sale s1 WHERE saleDate='"+lastDate+"' AND s1.brandNoPackQty = p.brandNoPackQty AND s1.store_id = "+userSession.getStoreId()+")"
				+ " AS closingStock,(SELECT SingleBottelRate FROM invoice inv WHERE inv.brandNoPackQty=p.brandNoPackQty AND invoiceDateAsPerSheet"
				+ " <='"+lastDate+"' AND inv.store_id = "+userSession.getStoreId()+" ORDER BY invoiceDateAsPerSheet DESC LIMIT 1) AS closingStockPrice "
				+ "FROM product p INNER JOIN sale s WHERE p.brandNoPackQty = s.brandNoPackQty AND s.store_id = "+userSession.getStoreId()+" GROUP BY p.brandNoPackQty";
		
		try{
		   double openingStock=0,closingStock=0;
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						openingStock += ((row.get("openingStock")!=null?Double.parseDouble(row.get("openingStock").toString()):0)*(row.get("openingStockPrice")!=null?Double.parseDouble(row.get("openingStockPrice").toString()):0));
						closingStock += ((row.get("closingStock")!=null?Double.parseDouble(row.get("closingStock").toString()):0)*(row.get("closingStockPrice")!=null?Double.parseDouble(row.get("closingStockPrice").toString()):0));
					}
			 }
			 bean.setStockInId(openingStock);
			 bean.setQtyBtl(closingStock);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return bean;
	}

	/**
	 * This method is used to get product details to a category products for selected dates
	 * */
	@Override
	public List<ProductPerformanceWithComaparisonBean> getProductComparisionWithPerformance(String startDate,String endDate, int category) {
		List<ProductPerformanceWithComaparisonBean> dataList = new ArrayList<ProductPerformanceWithComaparisonBean>();
		String QUERY = "SELECT p.brandNoPackQty,p.realBrandNo,p.`quantity`,p.`packQty`,p.company,p.category,p.shortBrandName,"
				+ "LAST_DAY(saleDate - INTERVAL 0 MONTH) AS comulativeEndDate,COALESCE(SUM(sale),0) AS sale,COALESCE(SUM(totalPrice),0)  AS totalPrice,"
				+ "(SELECT COALESCE(SUM(totalPrice),0) FROM sale ss WHERE ss.brandNoPackQty=p.brandNoPackQty AND saleDate <'"+startDate+"' AND ss.store_id = "+userSession.getStoreId()+") AS commulativeTotalPrice,"
				+ " (SELECT COALESCE(SUM(sale),0) FROM sale ss  WHERE ss.brandNoPackQty=p.brandNoPackQty AND saleDate <'"+startDate+"' AND ss.store_id = "+userSession.getStoreId()+") AS commulativeSale,"
						+ " (SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.invoiceDateAsPerSheet <='"+endDate+"'  AND i.store_id ="+userSession.getStoreId()+" ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1) AS casePrice"
				+ "    FROM product p INNER JOIN sale s ON p.brandNoPackQty=s.brandNoPackQty WHERE s.saleDate<='"+endDate+"' "
				+ "AND p.`category`="+category+" AND s.store_id = "+userSession.getStoreId()+" GROUP BY p.brandNoPackQty ORDER BY p.realBrandNo";
		//System.out.println(QUERY);
		try{
  	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
  				if(rows != null && rows.size()>0) {
  					for(Map row : rows){
  						if((row.get("sale")!=null?Long.parseLong(row.get("sale").toString()):0)-(row.get("commulativeSale")!=null?Long.parseLong(row.get("commulativeSale").toString()):0) > 0){
  						ProductPerformanceWithComaparisonBean bean = new ProductPerformanceWithComaparisonBean();
  						DiscountWithDate discountWithDateBean = new DiscountWithDate();
  						DiscountWithDate discountWithDateBeanForGovt = new DiscountWithDate();
  						bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
  						bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
  						bean.setPackQty(row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0);
  						bean.setCaseRate(row.get("casePrice")!=null?Double.parseDouble(row.get("casePrice").toString()):0);
  						bean.setTotalPrice((row.get("totalPrice")!=null?Double.parseDouble(row.get("totalPrice").toString()):0)-(row.get("commulativeTotalPrice")!=null?Double.parseDouble(row.get("commulativeTotalPrice").toString()):0));
  						bean.setTotalSale((row.get("sale")!=null?Long.parseLong(row.get("sale").toString()):0)-(row.get("commulativeSale")!=null?Long.parseLong(row.get("commulativeSale").toString()):0));
  						bean.setBrandNopackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0);
  						discountWithDateBean = calculateDiscountByBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0,
  								startDate,row.get("commulativeSale")!=null?Long.parseLong(row.get("commulativeSale").toString()):0,row.get("sale")!=null?Long.parseLong(row.get("sale").toString()):0,0);
  						discountWithDateBeanForGovt = calculateMarginByBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0,
  								startDate,row.get("commulativeSale")!=null?Long.parseLong(row.get("commulativeSale").toString()):0,row.get("sale")!=null?Long.parseLong(row.get("sale").toString()):0,0);
  						bean.setCompanyDiscount(discountWithDateBean.getValue());
  						bean.setGovtMaegin(discountWithDateBeanForGovt.getGovtProfit());
  						bean.setCostPrice(discountWithDateBeanForGovt.getCostPrice());
  						dataList.add(bean);
  						}
  						
  					}
  				}
  	     }catch(Exception e){
  				e.printStackTrace();
  				return dataList;
  	     }
		return dataList;
	}

	/**
	 * This method is used to get Enterprice for selected company
	 * */
	@Override
	public category getCompanyWithEnterPrice(int company) {
		
		category cat = new category();
		String QUERY="SELECT companyName,enterprise FROM company_tab WHERE companyId="+company+"";
		try{
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
		 if(rows != null && rows.size()>0) {
				for(Map row : rows){
					cat.setLabel(row.get("companyName")!=null?row.get("companyName").toString():"");
					cat.setColor(row.get("enterprise")!=null?row.get("enterprise").toString():"");
				}
		 }
	}catch(Exception e){
		e.printStackTrace();
	}
	return cat;
			
	}

	/*Old method not used any where*/
	@Override
	public List<ExpenseCategoryAndDate> getExpenseDataByCategory(String startDate, String endDate, int[] expenseId) {
		String expense = "";
		for(int i=0; i<expenseId.length; i++){
			expense += Integer.toString(expenseId[i]);
			if(!(i==expenseId.length-1)){
				expense += ",";
			}
		}
		String QUERY="SELECT DATE_FORMAT(e.`expenseMasterDate`, '%d-' '%b-' '%Y') AS expenseMasterDate,e.`expenseMasterId`,"
				+ "(SELECT SUM(c.`expenseChildAmount`)  FROM `expense_child` c  WHERE c.`expenseMasterId` = e.`expenseMasterId` AND c.`categoryId` IN("+expense+")) AS expenseChildAmount "
				+ " FROM `expense_master` e WHERE e.`expenseMasterDate` >= '"+startDate+"' AND e.`expenseMasterDate` <= '"+endDate+"' ORDER BY e.`expenseMasterDate` DESC";
		List<ExpenseCategoryAndDate> beanList = new ArrayList<ExpenseCategoryAndDate>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						ExpenseCategoryAndDate bean = new ExpenseCategoryAndDate();
						bean.setEdate(row.get("expenseMasterDate")!=null?row.get("expenseMasterDate").toString():"");
						bean.setTotal(row.get("expenseChildAmount")!=null?Double.parseDouble(row.get("expenseChildAmount").toString()):0);
						bean.setExpenseCategoryBean(getExpensesDetails(row.get("expenseMasterId")!=null?Long.parseLong(row.get("expenseMasterId").toString()):0,expense));
						beanList.add(bean);
					}
			 }
		}catch(Exception e){
			e.printStackTrace();
			return beanList;
		}
		return beanList;
	}
  public List<ExpenseCategoryBean> getExpensesDetails(long masterId,String expense){
	  String QUERY="SELECT c.`comment`,c.`expenseChildAmount`,"
	  		+ "(SELECT e.`expenseName` FROM `expense_category` e WHERE e.`categoryId` = c.`categoryId`) AS categoryName "
	  		+ "FROM `expense_child` c  WHERE c.`expenseMasterId` ="+masterId+" AND c.`categoryId` IN ("+expense+")";
	  List<ExpenseCategoryBean> beanList = new ArrayList<ExpenseCategoryBean>();
	  try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						ExpenseCategoryBean bean = new ExpenseCategoryBean();
						bean.setExpenseDate(row.get("comment")!=null?row.get("comment").toString():"");
						bean.setExpenseAmount(row.get("expenseChildAmount")!=null?Double.parseDouble(row.get("expenseChildAmount").toString()):0);
						bean.setExpenseName(row.get("categoryName")!=null?row.get("categoryName").toString():"");
						beanList.add(bean);
					}
			 }
		}catch(Exception e){
			e.printStackTrace();
			return beanList;
		}
	  
	  return beanList;
  }
  /**
	 * This method is used to get product list
	 * */
	@Override
	public List<ProductListBean> getProductList() {
		String QUERY = "SELECT p.`brandNo`,p.`shortBrandName`,p.brandNoPackQty,p.`packQty`,p.`quantity`,p.`packType`,p.`productType`,p.`active`,"
				+ " CASE WHEN p.`active`= 1 THEN 'true' ELSE 'false' END AS actbol,(SELECT t.companyName FROM `company_tab` t WHERE t.companyId=p.`company`) AS company,"
				+ "(SELECT t.color FROM `company_tab` t WHERE t.companyId=p.`company`) AS color,"
				+ "(SELECT t.companyOrder FROM `company_tab` t WHERE t.companyId=p.`company`) AS companyOrder,"
				+ "(SELECT t.categoryName FROM `category_tab` t WHERE t.categoryId=p.`category`) AS category FROM product p";
		List<ProductListBean> listBean = new ArrayList<ProductListBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						ProductListBean bean = new ProductListBean();
						bean.setBrandNo(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0);
						bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
						bean.setBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0);
						bean.setPackQty(row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0);
						bean.setQuantity(row.get("quantity")!=null?row.get("quantity").toString():"");
						bean.setPackType(row.get("packType")!=null?row.get("packType").toString():"");
						bean.setProductType(row.get("productType")!=null?row.get("productType").toString():"");
						bean.setActive(row.get("active")!=null?Long.parseLong(row.get("active").toString()):-1);
						bean.setActiveBol(row.get("actbol")!=null?Boolean.parseBoolean(row.get("actbol").toString()):false);
						bean.setCompany(row.get("company")!=null?row.get("company").toString():"");
						bean.setCategory(row.get("category")!=null?row.get("category").toString():"");
						bean.setColor(row.get("color")!=null?row.get("color").toString():"");
						bean.setCompanyOrder(row.get("companyOrder")!=null?Long.parseLong(row.get("companyOrder").toString()):0);
						listBean.add(bean);
						
					}
			 }
		}catch(Exception e){
			e.printStackTrace();
			return listBean;
		}
		return listBean;
	}

	/**
	 * This method is used to make product active or inactive
	 * */
	@Override
	public String setActiveOrInActive(double brandNoPackQty, int activaVal) {
		String result=null;
		try{
    		int row = jdbcTemplate.update("UPDATE product set active="+activaVal+" WHERE brandNoPackQty="+brandNoPackQty+"");
    		if(row >0) result="Data Updated successfully";
    	}catch(Exception e){
    		result="Something went wrong";
    		e.printStackTrace();
    		return result;
    	}
		return result;
	}


	@Override
	public List<DiscountEstimationDetails> getNewDiscountEsitmateDetails(String date) {
		String startDate=null;
		String endDate=null;
		List<DiscountEstimationDetails> beanList = new ArrayList<DiscountEstimationDetails>();
		String QUERY="SELECT DISTINCT p1.realBrandNo, p1.`shortBrandName`,p1.productType,p1.saleSheetOrder,p1.`category`,(SELECT t.color FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS color,"
				+ "(SELECT t.color FROM `category_tab` t WHERE p1.`category`=t.`categoryId`) AS categoryColor,"
				+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyOrder,(SELECT t.categoryName FROM `category_tab` t "
				+ "WHERE p1.`category`=t.`categoryId`) AS categoryName,p1.`company`, (SELECT t.companyName FROM `company_tab` t WHERE p1.`company`=t.`companyId`) "
				+ "AS companyName, ROUND(AVG((SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p1.brandNoPackQty AND i.invoiceDateAsPerSheet "
				+ "<= (SELECT LAST_DAY('"+date+"'))  ORDER BY `invoiceId` DESC LIMIT 1)),0) AS casePrice,(SELECT target FROM stock_lift_with_discount d "
				+ "WHERE d.brandNo=p1.realBrandNo AND stockDiscountDate='"+date+"') AS targetCase,"
				+ "(SELECT adjustment FROM stock_lift_with_discount d WHERE d.brandNo=p1.realBrandNo AND stockDiscountDate='"+date+"') AS adjustment,"
				+ "(SELECT discountAmount FROM stock_lift_with_discount d "
				+ "WHERE d.brandNo=p1.realBrandNo AND stockDiscountDate='"+date+"') AS discountAmount,(SELECT LAST_DAY('"+date+"')) AS endDate  FROM product"
					+ " p1 WHERE p1.`active`=1 GROUP BY p1.realBrandNo  ORDER BY p1.realBrandNo ASC";
		//System.out.println(QUERY);
			try{
				List<Map<String, Object>> dateRow = jdbcTemplate.queryForList("SELECT `saleDate` FROM sale where saleDate<=(SELECT LAST_DAY('"+date+"')) ORDER BY `saleId` DESC LIMIT 1");
				 if(dateRow != null && dateRow.size()>0) {
						for(Map row : dateRow){
							endDate=row.get("saleDate")!=null?row.get("saleDate").toString():"";
						}
				 }
				 if(endDate != null && endDate !="" ){
				    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				    Date date1 = null;
					try {
						date1 = dateFormat.parse(endDate);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					Calendar cal = new GregorianCalendar();
					cal.setTime(date1);
					cal.add(Calendar.DAY_OF_MONTH, -29);
					Date today60 = cal.getTime();
					startDate = dateFormat.format(today60);
				 }
				 System.out.println("startDate: "+startDate+" endDate: "+endDate);
				List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				 if(rows != null && rows.size()>0) {
						for(Map row : rows){
							DiscountEstimationDetails bean = new DiscountEstimationDetails();
							bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
							bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
							bean.setCompanyColor(row.get("color")!=null?row.get("color").toString():"");
							bean.setCategoryColor(row.get("categoryColor")!=null?row.get("categoryColor").toString():"");
							bean.setCompanyOrder(row.get("companyOrder")!=null?Long.parseLong(row.get("companyOrder").toString()):0);
							bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
							bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
							bean.setCaseRate(row.get("casePrice")!=null?Double.parseDouble(row.get("casePrice").toString()):0);
							bean.setTargetCase(row.get("targetCase")!=null?Long.parseLong(row.get("targetCase").toString()):0);
							bean.setDicsPerCase(row.get("discountAmount")!=null?Double.parseDouble(row.get("discountAmount").toString()):0);
							StockInBean stockInBean = getIndentEstimateSoldDataAndStock(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0, startDate, endDate);
							bean.setLiftedCase(stockInBean.getQtyBtl());
							bean.setLastMonthSold(stockInBean.getTotalPrice());
							bean.setInHouseStock(stockInBean.getStockInId());
							bean.setPending(row.get("adjustment")!=null?Long.parseLong(row.get("adjustment").toString()):0);
							bean.setCompanyId(row.get("company")!=null?Long.parseLong(row.get("company").toString()):0);
							bean.setSaleSheetOrder(row.get("saleSheetOrder")!=null?Long.parseLong(row.get("saleSheetOrder").toString()):0);
							bean.setSplitPackType(getSplitInhouse(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0, endDate));
							bean.setCategoryId(row.get("category")!=null?Long.parseLong(row.get("category").toString()):0);
							bean.setMaxMinDiscountBean(getMaximunLowestAndPreviousDiscount(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0,date));
							bean.setProductType(row.get("productType")!=null?row.get("productType").toString():"");
							beanList.add(bean);
						}
				 }
			}catch(Exception e){
				e.printStackTrace();
				return beanList;
			}
		return beanList;
	}


	private SplitPackType getSplitInhouse(long brandNo, String endDate) {
		int storeId=userSession.getStoreId();
		SplitPackType bean = new SplitPackType();
		String QUERY = "SELECT DISTINCT p1.realBrandNo, p1.`shortBrandName`,p1.`category`,p1.productType,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) "
				+ "FROM sale s , product p WHERE s.store_id="+storeId+" AND s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo "
				+ " AND p.packType='2L') FROM invoice r , product p WHERE r.store_id="+storeId+" AND  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty"
				+ " AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2LStock,(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT"
				+ " TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p WHERE s.store_id="+storeId+" AND s.saleDate ='"+endDate+"' AND "
				+ "s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') FROM invoice r , product p WHERE r.store_id="+storeId+" AND "
				+ "r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1LStock,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) "
				+ "FROM sale s , product p WHERE s.store_id="+storeId+" AND s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo "
				+ " AND p.packType='Q') FROM invoice r , product p WHERE r.store_id="+storeId+" AND r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty "
				+ "AND p.realBrandNo=p1.realBrandNo  AND p.packType='Q') AS QStock, (SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT "
				+ "TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
				+ "WHERE s.store_id="+storeId+" AND s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') FROM invoice r ,"
				+ " product p WHERE r.store_id="+storeId+" AND r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') "
				+ "AS PStock,(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2)"
				+ " FROM sale s , product p WHERE s.store_id="+storeId+" AND s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') "
				+ "FROM invoice r , product p WHERE r.store_id="+storeId+" AND  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  "
				+ "AND p.packType='N') AS NStock,(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT "
				+ "TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p WHERE s.store_id="+storeId+" AND s.saleDate ='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') FROM invoice r , product p WHERE r.store_id="+storeId+" AND "
				+ "r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS DStock,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s ,"
				+ " product p WHERE s.store_id="+storeId+" AND s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') FROM invoice r ,"
				+ " product p WHERE r.store_id="+storeId+" AND  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB')"
				+ " AS SBStock,(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2)"
				+ " FROM sale s , product p WHERE s.store_id="+storeId+" AND s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') "
				+ "FROM invoice r , product p WHERE r.store_id="+storeId+" AND r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  "
				+ "AND p.packType='LB') AS LBStock,(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT "
				+ "TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p WHERE s.store_id="+storeId+" AND s.saleDate ='"+endDate+"' AND "
				+ "s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') FROM invoice r , product p WHERE r.store_id="+storeId+" AND "
				+ "r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TINStock,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s ,"
				+ " product p WHERE s.store_id="+storeId+" AND s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') FROM invoice r ,"
				+ " product p WHERE r.store_id="+storeId+" AND  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty =p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS XStock"
				+ " FROM product p1 WHERE p1.`realBrandNo`="+brandNo+"";
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						bean.setL2Stock(row.get("2LStock")!=null?Double.parseDouble(row.get("2LStock").toString()):0);
						bean.setL1Stock(row.get("1LStock")!=null?Double.parseDouble(row.get("1LStock").toString()):0);
						bean.setQStock(row.get("QStock")!=null?Double.parseDouble(row.get("QStock").toString()):0);
						bean.setPStock(row.get("PStock")!=null?Double.parseDouble(row.get("PStock").toString()):0);
						bean.setNStock(row.get("NStock")!=null?Double.parseDouble(row.get("NStock").toString()):0);
						bean.setDStock(row.get("DStock")!=null?Double.parseDouble(row.get("DStock").toString()):0);
						bean.setLBStock(row.get("LBStock")!=null?Double.parseDouble(row.get("LBStock").toString()):0);
						bean.setSBStock(row.get("SBStock")!=null?Double.parseDouble(row.get("SBStock").toString()):0);
						bean.setTINStock(row.get("TINStock")!=null?Double.parseDouble(row.get("TINStock").toString()):0);
					}
			 }
		}catch(Exception e){
			e.printStackTrace();
			return bean;
		}
		return bean;
	}

	/**
	 * This method is used to get product details for update product
	 * */
	@Override
	public List<EditProductBean> editProductDetails(int brandNo) {
      String QUERY="SELECT p.`brandNoPackQty`,p.`brandName`,p.`brandNo`,p.`productType`,p.`quantity`,p.`packQty`,p.`packType`,p.`category`,p.productGrouping,"
      		+ "p.`company`,p.`matchs`,p.`shortBrandName`,p.`specialMargin`,p.`matchName`,"
      		+ "(SELECT c.companyName FROM `company_tab` c WHERE c.companyId = p.`company`) AS companyName,"
      		+ "(SELECT c.groupName FROM `product_group` c WHERE c.group_id = p.`productGrouping`) AS groupName,"
      		+ "(SELECT c.categoryName FROM `category_tab` c WHERE c.categoryId = p.`category`) AS categoryName"
      		+ " FROM product p WHERE p.`brandNo`="+brandNo+"";
      List<EditProductBean> beanList = new ArrayList<EditProductBean>();
      try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						EditProductBean bean = new EditProductBean();
						bean.setBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0);
						bean.setBrandName(row.get("brandName")!=null?row.get("brandName").toString():"");
						bean.setBrandNo(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0);
						bean.setProductType(row.get("productType")!=null?row.get("productType").toString():"");
						bean.setQuantity(row.get("quantity")!=null?row.get("quantity").toString():"");
						bean.setPackQty(row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0);
						bean.setPackType(row.get("packType")!=null?row.get("packType").toString():"");
						bean.setCategoryId(row.get("category")!=null?Long.parseLong(row.get("category").toString()):0);
						bean.setCompanyId(row.get("company")!=null?Long.parseLong(row.get("company").toString()):0);
						bean.setMatchValue(row.get("matchs")!=null?Long.parseLong(row.get("matchs").toString()):0);
						bean.setShortBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
						bean.setMargin(row.get("specialMargin")!=null?Double.parseDouble(row.get("specialMargin").toString()):0);
						bean.setMatchName(row.get("matchName")!=null?row.get("matchName").toString():"");
						bean.setCategoryName(row.get("categoryName")!=null?row.get("categoryName").toString():"");
						bean.setCompanyName(row.get("companyName")!=null?row.get("companyName").toString():"");
						String productType = row.get("productType")!=null?row.get("productType").toString():"";
						bean.setProducttype(jdbcTemplate.queryForList("SELECT DISTINCT `productType` FROM `product` WHERE `productType` != '"+productType+"'",String.class));
						String quantity = row.get("quantity")!=null?row.get("quantity").toString():"";
					    bean.setQuantities(jdbcTemplate.queryForList("SELECT DISTINCT `quantity` FROM `product` WHERE `quantity` != '"+quantity+"'",String.class));
					    long packQty= row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0;
						bean.setPackqty(jdbcTemplate.queryForList("SELECT DISTINCT `packQty` FROM `product` WHERE `packQty` != "+packQty+"",Long.class));
						String packType = row.get("packType")!=null?row.get("packType").toString():"";
						bean.setPacktype(jdbcTemplate.queryForList("SELECT DISTINCT `packType` FROM `product` WHERE `packType` != '"+packType+"'",String.class));
					    bean.setCompanyList(getAllCompanyList(row.get("company")!=null?Long.parseLong(row.get("company").toString()):0));
						bean.setCategoryList(getAllCategoryListData(row.get("category")!=null?Long.parseLong(row.get("category").toString()):0));
						bean.setGroupingList(getAllGroupingListData(row.get("productGrouping")!=null?Long.parseLong(row.get("productGrouping").toString()):0));
						bean.setGroupId(row.get("productGrouping")!=null?Long.parseLong(row.get("productGrouping").toString()):0);
						bean.setGroupName(row.get("groupName")!=null?row.get("groupName").toString():"");
					    beanList.add(bean);
					}
			 }
		}catch(Exception e){
			e.printStackTrace();
			return beanList;
		}
		return beanList;
	}


	private List<companyBean> getAllGroupingListData(long grouping) {
		String QUERY = "SELECT t.`group_id`,t.`groupName` FROM product_group t WHERE `group_id` !="+grouping;
		List<companyBean> beanList = new ArrayList<companyBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						companyBean bean = new companyBean();
						bean.setId(row.get("group_id")!=null?Long.parseLong(row.get("group_id").toString()):0);
						bean.setName(row.get("groupName")!=null?row.get("groupName").toString():"");
						beanList.add(bean);
					}
			 }
		}catch(Exception e){
			e.printStackTrace();
			return beanList;
		}
		return beanList;
	}

	private List<companyBean> getAllCategoryListData(long category) {
		String QUERY = "SELECT t.`categoryName`,t.`categoryId` FROM category_tab t WHERE `categoryId` !="+category+"";
		List<companyBean> beanList = new ArrayList<companyBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						companyBean bean = new companyBean();
						bean.setId(row.get("categoryId")!=null?Long.parseLong(row.get("categoryId").toString()):0);
						bean.setName(row.get("categoryName")!=null?row.get("categoryName").toString():"");
						beanList.add(bean);
					}
			 }
		}catch(Exception e){
			e.printStackTrace();
			return beanList;
		}
		return beanList;
	}


	private List<companyBean> getAllCompanyList(long company) {
		String QUERY = "SELECT t.`companyName`,t.`companyId` FROM company_tab t WHERE `companyId` !="+company+"";
		List<companyBean> beanList = new ArrayList<companyBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						companyBean bean = new companyBean();
						bean.setId(row.get("companyId")!=null?Long.parseLong(row.get("companyId").toString()):0);
						bean.setName(row.get("companyName")!=null?row.get("companyName").toString():"");
						beanList.add(bean);
					}
			 }
		}catch(Exception e){
			e.printStackTrace();
			return beanList;
		}
		return beanList;
	}

	/**
	 * This method is used to update products
	 * */
	@Override
	public String updateSingleProductDetails(JSONArray jsonData) {
		String result=null;
		synchronized(this){
		try {
			for (int i = 0; i < jsonData.length(); ++i) {
			 JSONObject rec;
			    rec = jsonData.getJSONObject(i);
			    try{
					/*int val=jdbcTemplate.update("UPDATE product set brandName='"+rec.getString("brandName")+"',productType='"+rec.getString("productType")+"',quantity='"+rec.getString("quantity")+"',"
							+ "category="+rec.getInt("category")+",company="+rec.getInt("company")+",matchs="+rec.getInt("matchValue")+",shortBrandName='"+rec.getString("sortName")+"',"
									+ "specialMargin="+rec.getDouble("margin")+",matchName='"+rec.getString("matchName")+"' WHERE brandNoPackQty = "+rec.getDouble("brandNoPackQty")+"");
						*/
			    	int val=jdbcTemplate.update("UPDATE product set brandName=?,productType=?,quantity=?,category=?,company=?,matchs=?,shortBrandName=?,"
									+ "specialMargin=?,matchName=?,productGrouping=? WHERE brandNoPackQty = ?",rec.getString("brandName"),rec.getString("productType"),rec.getString("quantity"),
									rec.getInt("category"),rec.getInt("company"),rec.getInt("matchValue"),rec.getString("sortName"),rec.getDouble("margin"),rec.getString("matchName"),rec.getInt("groupId"),rec.getDouble("brandNoPackQty"));
						
			    	if(val>0)
							result="Data Updated Successfully";
				}catch(Exception e){
					result="something went wrong";
					e.printStackTrace();
					}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			result="Something Went Wrong.";
			e.printStackTrace();
		}
		}
		return result;
	}

	/**
	 * This method is used to sale for single product to pie chart
	 * */
	@Override
	public List<PieChartBean> getSaleByBrandNoPackQty(double brandNoPackQty, String sdate, String edate) {
		int storeId=userSession.getStoreId();
		
		String QUERY = "SELECT SUM(s.`sale`) AS sale,DATE_FORMAT(s.`saleDate`,'%a') AS saleDate FROM sale s WHERE s.store_id="+storeId+" AND s.`saleDate`>='"+sdate+"' AND s.`saleDate`<='"+edate+"' AND s.`brandNoPackQty`="+brandNoPackQty+" GROUP BY s.`saleDate`";
		List<PieChartBean> beanList = new ArrayList<PieChartBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						PieChartBean bean = new PieChartBean();
						bean.setLabel(row.get("saleDate")!=null?row.get("saleDate").toString():"");
						bean.setValue(row.get("sale")!=null?Double.parseDouble(row.get("sale").toString()):0);
						beanList.add(bean);
					}
			 }
		}catch(Exception e){
			e.printStackTrace();
			return beanList;
		}
		return beanList;
	}

	/**
	 * This method is used to get product list with short name
	 * */
	@Override
	public List<AddNewBrandBean> getProductListWithShortName(double brandNopackQty, int categoryId) {
	       String QUERY = "SELECT p.`shortBrandName`,p.`realBrandNo`,p.`quantity`,p.`brandNoPackQty` FROM product p WHERE p.`brandNoPackQty` !="+brandNopackQty+" AND p.`category`="+categoryId+" ORDER BY p.`company` ASC";
	       List<AddNewBrandBean> beanList = new ArrayList<AddNewBrandBean>();
			try{
				List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				 if(rows != null && rows.size()>0) {
						for(Map row : rows){
							AddNewBrandBean bean = new AddNewBrandBean();
							bean.setShortBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
							bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
							bean.setQuantity(row.get("quantity")!=null?row.get("quantity").toString():"");
							bean.setBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0);
							beanList.add(bean);
						}
				 }
			}catch(Exception e){
				e.printStackTrace();
				return beanList;
			}
			return beanList;
		}


	@Override
	public TillDateBalanceSheetChart getMultiChartBeanSaleData(String sdate, String edate, int[] brandNoPackQty) {
		
		int storeId=userSession.getStoreId();
		
		TillDateBalanceSheetChart tillDateBalanceSheetChart = new TillDateBalanceSheetChart();
		List<categoryLineChart> labels = new ArrayList<categoryLineChart>();
		ArrayList<String> arrlist = new ArrayList<String>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT DISTINCT `saleDate`,DATE_FORMAT(`saleDate`,'%a') AS saleDateformate FROM sale WHERE store_id="+storeId+" AND saleDate >='"+sdate+"' AND `saleDate`<='"+edate+"' ORDER BY saleDate ASC");
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						categoryLineChart dateLabel = new categoryLineChart();
						arrlist.add(row.get("saleDate")!=null?row.get("saleDate").toString():"");
						dateLabel.setLabel(row.get("saleDateformate")!=null?row.get("saleDateformate").toString():"");
						labels.add(dateLabel);
					}
			 }
			 List<dataset> outerdataset = new ArrayList<dataset>();
			  if(arrlist.size() > 0){
				  for(int quantity : brandNoPackQty){
				  dataset dataSetForsale = new dataset();
				  dataSetForsale.setSeriesname(jdbcTemplate.queryForObject("SELECT `shortBrandName` FROM `product` WHERE `brandNoPackQty`="+quantity+"", String.class));
				  List<data> listdataSetForsale = new ArrayList<data>();
				  for (String saledate : arrlist) { 
					  data dataVal = new data();
					 List<Double> tempList =  jdbcTemplate.queryForList("SELECT s.`sale` FROM sale s WHERE s.store_id="+storeId+" AND saleDate ='"+saledate+"' AND s.`brandNoPackQty`="+quantity+"",Double.class);
					  if(tempList.size() > 0)
					  dataVal.setValue(tempList.get(0));
					  else
					   dataVal.setValue((double) 0);
					  listdataSetForsale.add(dataVal);
				  }
				  dataSetForsale.setData(listdataSetForsale);
				  outerdataset.add(dataSetForsale);
				}
				 
			  }
			  tillDateBalanceSheetChart.setCategoryLineChart(labels);
			  tillDateBalanceSheetChart.setDataset(outerdataset);
		}catch(Exception e){
			e.printStackTrace();
			return tillDateBalanceSheetChart;
		}
		return tillDateBalanceSheetChart;
	}

	/**
	 * This method is used to upload invoice
	 * */
	@Override
	public String addNewIndentFile(addNewIndentBean indentBean) {
		String month = jdbcTemplate.queryForObject("SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+indentBean.getIndentDate()+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01')", String.class);
		String result=null;
		 String QUERY="insert into indent_files(month,date,filePath,completeFilePath,store_id) values(?,?,?,?,?)";
		try{
				int val=jdbcTemplate.update(QUERY,new Object[]{month,indentBean.getIndentDate(),indentBean.getImageName(),indentBean.getNgalleryImage(),userSession.getStoreId()});
				if(val>0)
					result="Indent Saved successfully";
		}catch(Exception e){
			result="something went wrong";
			log.log(Level.WARN, "Indent in PoizonSecDaoImpl exception", e);
			e.printStackTrace();
			}
		return result;
	}

	/**
	 * This method is used to get uploaded invoice file
	 * */
	@Override
	public List<UploadAndViewWithYearBean> getIndentListBasedOnMonth() {
		List<UploadAndViewWithYearBean> superBeanList = new ArrayList<UploadAndViewWithYearBean>();
		try {
			List<Map<String, Object>> daterowssuper = jdbcTemplate.queryForList(
					"SELECT DISTINCT DATE_FORMAT(MONTH,'%Y') AS yeardate,DATE_FORMAT(MONTH,'%Y-' '01-' '01') AS startdate,DATE_FORMAT(MONTH,'%Y-' '12-' '31') AS endDate  FROM `indent_files` WHERE store_id = "+userSession.getStoreId()+" ORDER BY MONTH DESC");
			{
				if (daterowssuper != null && daterowssuper.size() > 0) {
					for (Map daterowuper : daterowssuper) {
						UploadAndViewWithYearBean uploadAndViewWithYearBean = new UploadAndViewWithYearBean();
						uploadAndViewWithYearBean.setYear(
								daterowuper.get("yeardate") != null ? daterowuper.get("yeardate").toString() : "");
						List<UploadAndViewInvoiceBean> beanList = new ArrayList<UploadAndViewInvoiceBean>();
						List<Map<String, Object>> daterows = jdbcTemplate.queryForList(
								"SELECT DISTINCT month,DATE_FORMAT(month, '%M' ' %Y') AS monthwithformat FROM `indent_files` WHERE MONTH >= '"
										+ daterowuper.get("startdate") + "' AND MONTH <= '" + daterowuper.get("endDate")
										+ "' AND store_id = "+userSession.getStoreId()+" ORDER BY MONTH DESC");
						if (daterows != null && daterows.size() > 0) {
							for (Map daterow : daterows) {
								UploadAndViewInvoiceBean invoiceBean = new UploadAndViewInvoiceBean();
								invoiceBean.setDate(daterow.get("monthwithformat") != null
										? daterow.get("monthwithformat").toString() : "");
								List<Map<String, Object>> rows = jdbcTemplate.queryForList(
										"SELECT DATE_FORMAT(i.`date`,'%e-' '%b-' '%y') AS date,i.`month`,i.`filePath`,i.completeFilePath FROM `indent_files` i WHERE i.`month`='"
												+ daterow.get("month") + "' AND i.store_id = "+userSession.getStoreId()+"  ORDER BY i.`date` DESC");
								List<addNewIndentBean> addNewIndentBeanList = new ArrayList<addNewIndentBean>();
								if (rows != null && rows.size() > 0) {
									for (Map row : rows) {
										addNewIndentBean bean = new addNewIndentBean();
										bean.setIndentDate(row.get("date") != null ? row.get("date").toString() : "");
										bean.setIndentMonth(
												row.get("month") != null ? row.get("month").toString() : "");
										bean.setImageName(
												row.get("filePath") != null ? row.get("filePath").toString() : "");
										bean.setNgalleryImage(row.get("completeFilePath") != null
												? row.get("completeFilePath").toString() : "");
										addNewIndentBeanList.add(bean);
									}
								}
								invoiceBean.setAddNewIndentBeanList(addNewIndentBeanList);
								invoiceBean.setInvoiceAmt(jdbcTemplate.queryForObject(
										"SELECT ROUND(SUM(totalPrice) + (SELECT SUM(mrpRoundOff + turnoverTax + tcsVal) FROM invoice_mrp_round_off "
												+ "WHERE store_id = "+userSession.getStoreId()+" AND mrpRoundOffDateAsCopy >='" + daterow.get("month")
												+ "' AND mrpRoundOffDateAsCopy <=(SELECT LAST_DAY('"
												+ daterow.get("month")
												+ "')))) AS tx FROM invoice WHERE store_id = "+userSession.getStoreId()+" AND invoiceDateAsPerSheet >='"
												+ daterow.get("month")
												+ "' AND invoiceDateAsPerSheet <=(SELECT LAST_DAY('"
												+ daterow.get("month") + "'))",
										Double.class));
								beanList.add(invoiceBean);
							}
						}
						uploadAndViewWithYearBean.setUploadAndViewInvoiceBean(beanList);
						uploadAndViewWithYearBean.setTotalInvoice(jdbcTemplate.queryForObject("SELECT ROUND(SUM(totalPrice) + (SELECT SUM(mrpRoundOff + turnoverTax + tcsVal) FROM invoice_mrp_round_off WHERE store_id = "+userSession.getStoreId()+" AND mrpRoundOffDateAsCopy >='" + daterowuper.get("startdate")+ "' "
								+ "AND mrpRoundOffDateAsCopy <='"+daterowuper.get("enddate")+"')) AS tx FROM invoice WHERE store_id = "+userSession.getStoreId()+" AND invoiceDateAsPerSheet >='"+ daterowuper.get("startdate")+ "'"
										+ " AND invoiceDateAsPerSheet <='"+daterowuper.get("enddate")+"'",Double.class));
						superBeanList.add(uploadAndViewWithYearBean);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return superBeanList;
		}
		return superBeanList;
	}


	@Override
	public List<DiscountAsPerCompanyBeen> getDiscountForAllCompanies(String month) {
		/*String QUERY = "SELECT COALESCE(SUM(d.`totalDiscountAmount`),0) AS discountamount,"
				+ "(SELECT t.`companyName` FROM `company_tab` t  WHERE t.`companyId` = d.`companyName`) AS company,"
				+ "(SELECT t.`companyOrder` FROM `company_tab` t  WHERE t.`companyId` = d.`companyName`) AS companyOrder,"
				+ "(SELECT COALESCE(SUM(ds.`rentalAmount`),0) FROM `rental_tab` ds WHERE ds.rentalDate = '"+month+"' AND ds.`company` = d.`companyName`) AS rental,"
				+ "((SELECT COALESCE(SUM(ds.`transactionAmt`),0) FROM `discount_transaction` ds WHERE ds.month = '"+month+"'"
				+ " AND ds.`companyId`= d.`companyName`) +(SELECT COALESCE(SUM(ds.`adjCheck`),0) FROM `discount_transaction` ds WHERE ds.month = '"+month+"' "
				+ "AND ds.`companyId`=d.`companyName`)) AS chequeAmount,(SELECT COALESCE(SUM(ds.`adjCheck`),0) FROM `discount_transaction` ds "
				+ "WHERE ds.month = '"+month+"' AND ds.`companyId` = d.`companyName`) AS adjustmentCheque,(((SELECT COALESCE(SUM(totalDiscountAmount),0)"
				+ " FROM stock_lift_with_discount WHERE stockDiscountDate >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount` ORDER BY stockDiscountDate ASC"
				+ " LIMIT 1) AND  stockDiscountDate<='"+month+"' AND companyName=d.`companyName`)+(SELECT COALESCE(SUM(rentalAmount),0) FROM rental_tab "
				+ "  WHERE rentalDate >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount` ORDER BY stockDiscountDate ASC LIMIT 1) AND rentalDate<='"+month+"' "
				+ "AND company=d.`companyName`))- (SELECT COALESCE(SUM(transactionAmt+adjCheck),0) FROM discount_transaction WHERE "
				+ "MONTH >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount`  ORDER BY stockDiscountDate ASC LIMIT 1) AND MONTH<='"+month+"'"
				+ " AND companyId=d.`companyName`)) AS arrears FROM `stock_lift_with_discount` d WHERE d.`stockDiscountDate` = '"+month+"'";
		System.out.println(QUERY);*/
		List<DiscountAsPerCompanyBeen> beanList = new ArrayList<DiscountAsPerCompanyBeen>(); 
		try{
			List<Map<String, Object>> companyRows = jdbcTemplate.queryForList("SELECT `companyId` FROM `company_tab`");
			 if(companyRows != null && companyRows.size()>0) {
					for(Map companyrow : companyRows){
			List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT (SELECT COALESCE(SUM(t.`totalDiscountAmount`),0) FROM stock_lift_with_discount t "
					+ "WHERE t.companyName = d.companyName AND t.stockDiscountDate = '"+month+"') AS discountamount,"
					+ "(SELECT t.`companyName` FROM `company_tab` t  WHERE t.`companyId` = d.`companyName`) AS company,"
					+ "(SELECT t.`companyOrder` FROM `company_tab` t  WHERE t.`companyId` = d.`companyName`) AS companyOrder,"
					+ "(SELECT COALESCE(SUM(ds.`rentalAmount`),0) FROM `rental_tab` ds WHERE ds.rentalDate = '"+month+"' AND ds.`company` = d.`companyName`) AS rental,"
					+ "((SELECT COALESCE(SUM(ds.`transactionAmt`),0) FROM `discount_transaction` ds WHERE ds.month = '"+month+"'"
					+ " AND ds.`companyId`= d.`companyName`) +(SELECT COALESCE(SUM(ds.`adjCheck`),0) FROM `discount_transaction` ds WHERE ds.month = '"+month+"' "
					+ "AND ds.`companyId`=d.`companyName`)) AS chequeAmount,(SELECT COALESCE(SUM(ds.`adjCheck`),0) FROM `discount_transaction` ds "
					+ "WHERE ds.month = '"+month+"' AND ds.`companyId` = d.`companyName`) AS adjustmentCheque,(((SELECT COALESCE(SUM(totalDiscountAmount),0)"
					+ " FROM stock_lift_with_discount WHERE stockDiscountDate >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount` ORDER BY stockDiscountDate ASC"
					+ " LIMIT 1) AND  stockDiscountDate<='"+month+"' AND companyName=d.`companyName`)+(SELECT COALESCE(SUM(rentalAmount),0) FROM rental_tab "
					+ "  WHERE rentalDate >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount` ORDER BY stockDiscountDate ASC LIMIT 1) AND rentalDate<='"+month+"' "
					+ "AND company=d.`companyName`))- (SELECT COALESCE(SUM(transactionAmt+adjCheck),0) FROM discount_transaction WHERE "
					+ "MONTH >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount`  ORDER BY stockDiscountDate ASC LIMIT 1) AND MONTH<='"+month+"'"
					+ " AND companyId=d.`companyName`)) AS arrears FROM `stock_lift_with_discount` d WHERE d.`companyName` = "+companyrow.get("companyId")+" LIMIT 1");
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						DiscountAsPerCompanyBeen bean = new DiscountAsPerCompanyBeen();
						bean.setDiscountAmount(row.get("discountamount")!=null?Double.parseDouble(row.get("discountamount").toString()):0);
						bean.setCompanyName(row.get("company")!=null?row.get("company").toString():"");
						bean.setCompany(row.get("companyOrder")!=null?Long.parseLong(row.get("companyOrder").toString()):0);
						bean.setRentals(row.get("rental")!=null?Double.parseDouble(row.get("rental").toString().toString()):0);
						bean.setChequeAmount(row.get("chequeAmount")!=null?Double.parseDouble(row.get("chequeAmount").toString()):0);
						bean.setAdjustmentCheque(row.get("adjustmentCheque")!=null?Double.parseDouble(row.get("adjustmentCheque").toString()):0);
						bean.setArrears(row.get("arrears")!=null?Double.parseDouble(row.get("arrears").toString()):0);
						beanList.add(bean);
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return beanList;
		}
		return beanList;
	}


	@Override
	public List<DiscountAndMonthBean> getDiscountForSelectedMonth(String startMonth, String endMonth, int company) {
		
		int storeId=userSession.getStoreId();
		
		 List<DiscountAndMonthBean>  discountAndMonthBeanList = new ArrayList<DiscountAndMonthBean>();
		ArrayList<String> arrlist = new ArrayList<String>();
		//arrlist.add("2017-10-01");
		 //String QUERY="SELECT DISTINCT saleDate FROM sale WHERE `saleDate` LIKE '%-01' AND saleDate >='"+startMonth+"' AND saleDate <='"+endMonth+"' AND store_id="+storeId+" ORDER BY saleDate DESC;";
	    String QUERY = "SELECT DISTINCT DATE_FORMAT(saleDate ,'%Y-%m-01') AS saleDate FROM sale WHERE saleDate >='"+startMonth+"' AND saleDate <='"+endMonth+"' AND store_id="+storeId+" ORDER BY saleDate DESC";
		 try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						arrlist.add(row.get("saleDate")!=null?row.get("saleDate").toString():"");
					}
				}
				if(arrlist.size() > 0){
					for (String saledate : arrlist) {
						double totalDisc=0;String dateVal=null;double discountVal=0;double totalSum=0;
						DiscountAndMonthBean discountAndMonthBean = new DiscountAndMonthBean();
						List<DiscountMonthWiseBean>   discountMonthWiseBeanList = new ArrayList<DiscountMonthWiseBean>();
					 List<Map<String, Object>> secrows = jdbcTemplate.queryForList("SELECT DISTINCT p.realBrandNo,p.matchName,p.`matchs`,p.packQty,p.packType,UPPER(DATE_FORMAT('"+saledate+"', '%b' ' %y')) AS date,"
					 		+ "(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,i.packQtyRate,"
					 		+ "(SELECT d.discountAmount FROM stock_lift_with_discount d WHERE d.brandNo=p.realBrandNo AND d.store_id="+storeId+" AND d.stockDiscountDate='"+saledate+"') AS discountAmount,"
					 		+ "(SELECT d.adjustment FROM stock_lift_with_discount d WHERE d.brandNo=p.realBrandNo AND d.stockDiscountDate='"+saledate+"' AND d.store_id="+storeId+") AS adjustment,"
					 		+ "(SELECT dt.totalDiscountAmount FROM stock_lift_with_discount dt WHERE dt.brandNo=p.realBrandNo AND dt.stockDiscountDate='"+saledate+"' AND dt.store_id="+storeId+")  "
					 		+ " AS totalDiscountAmount, SUM(i.caseQty) AS caseQty,CEIL(SUM(i.receivedBottles/i.packQty)) AS totalcases,"
					 		+ " (SELECT SUM(dt.totalDiscountAmount) FROM stock_lift_with_discount dt WHERE dt.stockDiscountDate='"+saledate+"' AND dt.companyName="+company+" AND dt.store_id="+storeId+")  "
					 		+ " AS discountSum  FROM product p LEFT JOIN invoice i  ON p.`brandNoPackQty`  = i.`brandNoPackQty`  WHERE p.`company`="+company+" AND"
					 		+ " `invoiceDateAsPerSheet`>='"+saledate+"' "
					 		+ " AND `invoiceDateAsPerSheet`<=(SELECT LAST_DAY('"+saledate+"' - INTERVAL 0 MONTH)) AND i.store_id="+storeId+" GROUP BY p.realBrandNo");
					 for(Map secrow : secrows){
						 DiscountMonthWiseBean bean = new DiscountMonthWiseBean();
						  discountVal += secrow.get("discountAmount")!=null?Double.parseDouble(secrow.get("discountAmount").toString()):0;
						  totalSum = secrow.get("discountAmount")!=null?Double.parseDouble(secrow.get("discountAmount").toString()):0;
						  if(totalSum>0){
							  bean.setMatch(secrow.get("matchs")!=null?Long.parseLong(secrow.get("matchs").toString()):0);
						 bean.setBrand(secrow.get("matchName")!=null?secrow.get("matchName").toString():"");
						 bean.setCase((secrow.get("totalcases")!=null?Long.parseLong(secrow.get("totalcases").toString()):0)+(secrow.get("adjustment")!=null?Long.parseLong(secrow.get("adjustment").toString()):0));
						 bean.setDiscount(secrow.get("discountAmount")!=null?Double.parseDouble(secrow.get("discountAmount").toString()):0);
						 bean.setTotal(secrow.get("totalDiscountAmount")!=null?Double.parseDouble(secrow.get("totalDiscountAmount").toString()):0);
						 bean.setDate(saledate);
						 discountMonthWiseBeanList.add(bean);
						 totalDisc=secrow.get("discountSum")!=null?Double.parseDouble(secrow.get("discountSum").toString()):0;
						 dateVal=secrow.get("date")!=null?secrow.get("date").toString():"";
						 }
					 }
					 if(discountVal >0){
					 discountAndMonthBean.setDiscountMonthWiseBean(discountMonthWiseBeanList);
					 discountAndMonthBean.setMonth(dateVal);
					 discountAndMonthBean.setTotalDiscount(totalDisc);
					 discountAndMonthBeanList.add(discountAndMonthBean);
					 }
					
					}
				}
	     }catch(Exception e){
	    	 log.log(Level.WARN, "getDiscountCompanyWiseForAllMonth in PoizonSecondDaoImpl exception", e);
				e.printStackTrace();
				return discountAndMonthBeanList;
	     }
		return discountAndMonthBeanList;
	}


	@Override
	public double getRentalForSelected(String startMonth, String endMonth, int company) {
		double rental = 0;
		try{
			
			int storeId=userSession.getStoreId();
		 List<Map<String, Object>> rentalrows = jdbcTemplate.queryForList("SELECT COALESCE(SUM(ds.`rentalAmount`),0) as rental "
	    	 		+ " FROM `rental_tab` ds WHERE rentalDate>='"+startMonth+"' AND rentalDate<='"+endMonth+"' AND ds.`company`="+company+" AND ds.store_id="+storeId+" ");
				if(rentalrows != null && rentalrows.size()>0) {
					for(Map rentalrow : rentalrows){
						rental = rentalrow.get("rental")!=null?Double.parseDouble(rentalrow.get("rental").toString()):0;
					}
				}
		}catch(Exception e){
			e.printStackTrace();
			return rental;
			
		}
		return rental;
	}


	@Override
	public List<DiscountAsPerCompanyBeen> getDiscountChecksForSelectedMonths(String startMonth, String endMonth,
			int company) {
		int storeId=userSession.getStoreId();
		List<DiscountAsPerCompanyBeen> discountBeanList = new ArrayList<DiscountAsPerCompanyBeen>();
		String QUERY="SELECT MONTH,transactionAmt,adjCheck,DATE_FORMAT(transactionDate, '%d-%b-%Y') AS transactionDate,bank,transactionType,checkNo,fromBank FROM discount_transaction "
				+ "WHERE companyId="+company+" AND MONTH >='"+startMonth+"' AND MONTH <='"+endMonth+"' AND received=1 AND store_id="+storeId+" AND transactionDate IS NOT NULL ORDER BY MONTH DESC";
		     try{
		    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
					if(rows != null && rows.size()>0) {
						for(Map row : rows){
							DiscountAsPerCompanyBeen bean = new DiscountAsPerCompanyBeen();
							bean.setEnterprise(row.get("transactionDate")!=null?row.get("transactionDate").toString():"");
							/*bean.setDiscountAmount((row.get("discountamount")!=null?Double.parseDouble(row.get("discountamount").toString()):0)
									+(row.get("rental")!=null?Double.parseDouble(row.get("rental").toString()):0));*/
							bean.setChequeAmount(row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0);
							bean.setAdjustmentCheque(row.get("adjCheck")!=null?Double.parseDouble(row.get("adjCheck").toString()):0);
							bean.setCompanyName(row.get("fromBank")!=null?row.get("fromBank").toString():"");
							bean.setComment(row.get("transactionType")!=null?row.get("transactionType").toString():"");
							bean.setColor(row.get("checkNo")!=null?row.get("checkNo").toString():"");
							discountBeanList.add(bean);
						}
					}
		     }catch(Exception e){
		    	 e.printStackTrace();
		    	 return discountBeanList;
		     }
		
		return discountBeanList;
	}


	@Override
	public PieChartBean getArrearsForPreviousDays(String startMonth, int company) {
		
		int storeId=userSession.getStoreId();
		PieChartBean bean = new PieChartBean();
		
		String QUERY="SELECT COALESCE(SUM(sd.totalDiscountAmount),0) AS totalDiscountAmount,"
				+ "(SELECT COALESCE(SUM(t.rentalAmount),0) FROM `rental_tab` t WHERE t.store_id="+storeId+" AND t.company="+company+" AND "
				+ " t.rentalDate < '"+startMonth+"')AS rentalAmount,(SELECT COALESCE(SUM(ds.`adj_amount`),0) FROM `vendor_adjustment` ds WHERE"
						+ " ds.company="+company+" AND ds.vendor_month< '"+startMonth+"' AND ds.store_id="+storeId+") AS vendorAmt,"
				+ "(SELECT COALESCE(SUM(dt.transactionAmt + dt.adjCheck),0)"
				+ " FROM discount_transaction dt WHERE dt.companyId="+company+" AND dt.month < '"+startMonth+"' AND dt.received=1 AND dt.store_id="+storeId+" AND dt.transactionDate IS NOT NULL) AS transactionAmt,"
				+ "(SELECT DATE_FORMAT( DATE_SUB('"+startMonth+"', INTERVAL 1 MONTH), '%b %Y')) AS date "
				+ "FROM stock_lift_with_discount sd WHERE sd.store_id="+storeId+" AND sd.companyName ="+company+" AND sd.`stockDiscountDate` < '"+startMonth+"'";
		
		 try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						double rental = row.get("rentalAmount")!=null?Double.parseDouble(row.get("rentalAmount").toString()):0;
						double totalDiscount = row.get("totalDiscountAmount")!=null?Double.parseDouble(row.get("totalDiscountAmount").toString()):0;
						double transactionAmt = row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0;
						//double vendorAmt = row.get("vendorAmt")!=null?Double.parseDouble(row.get("vendorAmt").toString()):0;
						bean.setLabel(row.get("date")!=null?row.get("date").toString():"");
						bean.setValue((rental+totalDiscount)-(transactionAmt));
					}
				}
	     }catch(Exception e){
	    	 e.printStackTrace();
	    	 return bean;
	     }
		
		return bean;
	}

	/**
	 * This method is used to save vendor adjustment or vendor discount
	 * */
	@Override
	public String saveVendorAdjustment(VendorAdjustmentBean bean, String company) {
		System.out.println("company :: "+company);
		log.info("PoizinSecondDaoImpl::saveVendorAdjustment");
		int companyId = -1;
		try{
			companyId = jdbcTemplate.queryForObject("SELECT companyId FROM company_tab WHERE companyName = ?", new Object[]{company},Integer.class);
		}catch(Exception e){
			log.error("Error In PoizinThirdDaoImpl getExistingSingleCompanyArrears(--)"+e);
			e.printStackTrace();
		}
		
		String result = null;
        try{
        	if(companyId > 0){
        	 List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT adj_amount FROM `vendor_adjustment` WHERE vendor_month = ? AND company=? AND store_id=?", new Object[]{bean.getMonth(),companyId,userSession.getStoreId()});
        	 if(rows !=null && rows.size() > 0){
        		 int val = jdbcTemplate.update("UPDATE vendor_adjustment SET adj_amount = ?, vendor_comment = ? WHERE vendor_month=? AND company=? AND store_id=?",new Object[]{bean.getAmount(),bean.getComment(),bean.getMonth(),companyId,userSession.getStoreId()});
        	   if(val > 0)
        		   result = "Updated Successfully";
        	 }else{
        		 int val = jdbcTemplate.update("INSERT INTO `vendor_adjustment` (company,adj_amount,vendor_month,vendor_comment,store_id) VALUES(?,?,?,?,?)",new Object[]{companyId,bean.getAmount(),bean.getMonth(),bean.getComment(),userSession.getStoreId()});
        		 if(val > 0)
        			 result = "Saved Successfully";
        	 }
        	}
        }catch(Exception e){
        	e.printStackTrace();
        	result =  "Something went wrong";
        }
		return result;

	}
	@Override
	public List<CallDiscountBean> callForDiscountData(String month) {
		
		int storeId=userSession.getStoreId();
		
		log.info("PoizinDaoImpl: callForDiscountData() date:: "+month);
		List<CallDiscountBean> beanList = new ArrayList<CallDiscountBean>();
		try{
			
			String endDate=null;
			try {
			  endDate = jdbcTemplate.queryForObject("SELECT `saleDate` FROM sale WHERE saleDate<=(SELECT LAST_DAY('"+month+"')) AND store_id="+storeId+"  ORDER BY `saleDate` DESC LIMIT 1", String.class);
			}catch(Exception exc) {
				
				try {
				  endDate = jdbcTemplate.queryForObject("SELECT `invoiceDateAsPerSheet` FROM invoice WHERE store_id="+storeId+"  ORDER BY `invoiceDateAsPerSheet` ASC LIMIT 1", String.class);

				}catch(Exception exce) {
					
				}
			}
			 
			String startDate=null;
			try {
			  startDate = jdbcTemplate.queryForObject("SELECT '"+endDate+"' -INTERVAL 29 DAY", String.class);
			}catch(Exception exc) {
				

			}
			
			String QUERY = "SELECT DISTINCT p1.realBrandNo, p1.`shortBrandName`,p1.productType,p1.saleSheetOrder,p1.`category`,p1.`company`,p1.productGrouping,"
					+ "(SELECT g.groupName FROM product_group g WHERE g.group_id = p1.productGrouping) AS groupName,"
					+ "(SELECT t.color FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS color,"
					+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyOrder,"
					+ " (SELECT t.companyName FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyName,"
					+ "  ROUND(AVG((SELECT i.packQtyRate FROM invoice i WHERE i.store_id="+storeId+" AND i.brandNoPackQty=p1.brandNoPackQty "
					+ "AND i.invoiceDateAsPerSheet <= (SELECT LAST_DAY('"+month+"'))  ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1)),0) AS casePrice,"
					+ "(SELECT target FROM stock_lift_with_discount d WHERE d.brandNo=p1.realBrandNo AND stockDiscountDate='"+month+"' AND d.store_id="+storeId+") AS targetCase,"
					+ " (SELECT adjustment FROM stock_lift_with_discount d WHERE d.brandNo=p1.realBrandNo AND stockDiscountDate='"+month+"' AND d.store_id="+storeId+") AS adjustment,"
					+ " (SELECT discountAmount FROM stock_lift_with_discount d WHERE d.brandNo=p1.realBrandNo AND stockDiscountDate='"+month+"' AND d.store_id="+storeId+") AS discountAmount,"
					+ "(SELECT LAST_DAY('"+month+"')) AS endDate  FROM product p1 WHERE p1.`active`=1 GROUP BY p1.realBrandNo  ORDER BY p1.productGrouping ASC";
			
			//System.out.println(QUERY);
			
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size() > 0){
				for(Map row : rows){
					CallDiscountBean bean = new CallDiscountBean();
					bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
					bean.setName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
					bean.setCaseRate(row.get("casePrice")!=null?Double.parseDouble(row.get("casePrice").toString()):0);
					StockInBean stockInBean = getIndentEstimateSoldDataAndStock(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0, startDate, endDate);
				    bean.setLiftedCase(stockInBean.getQtyBtl());
				    bean.setInhouseStock(stockInBean.getStockInId());
				    bean.setLastMonthSold(stockInBean.getTotalPrice());
				    bean.setStockDates(stockInBean.getDate());
				    bean.setCompanyColor(row.get("color")!=null?row.get("color").toString():"");
				    bean.setProductGrouping(row.get("productGrouping")!=null?Long.parseLong(row.get("productGrouping").toString()):0);
				    bean.setCompanyOrder(row.get("companyOrder")!=null?Long.parseLong(row.get("companyOrder").toString()):0);
				    bean.setTargetCase(row.get("targetCase")!=null?Long.parseLong(row.get("targetCase").toString()):0);
				    bean.setAdjustment(row.get("adjustment")!=null?Long.parseLong(row.get("adjustment").toString()):0);
				    bean.setDiscountAmount(row.get("discountAmount")!=null?Double.parseDouble(row.get("discountAmount").toString()):0);
				    bean.setCompanyId(row.get("company")!=null?Long.parseLong(row.get("company").toString()):0);
				    bean.setCompanyName(row.get("companyName")!=null?row.get("companyName").toString():"");
				    bean.setProductType(row.get("productType")!=null?row.get("productType").toString():"");
				    bean.setGroupName(row.get("groupName")!=null?row.get("groupName").toString():"");
				    bean.setSplitPackType(getSplitInhouse(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0, endDate));
				    bean.setMaxMinDiscountBean(getMaximunLowestAndPreviousDiscount(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0,month));
				    bean.setInvestment((double)0);
				    beanList.add(bean);
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
			return beanList;
		}
		return beanList;
	}

	@Override
	public String saveGroupingDiscountEstimateData(JSONArray discountDetails, String date) {
		
		int storeId=userSession.getStoreId();
		
		log.info("PoizonSecondDaoImpl.saveStockLiftWithDiscountData discountDetails= "+discountDetails +"  startdate>>> "+date);
		String systemDate = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDateTime.now());
	String result=null;
	String QUERY="INSERT INTO `stock_lift_with_discount`(`brandNo`,companyName,discountAmount,totalDiscountAmount,stockDiscountDate,adjustment,target,currentIndent,systemDate,store_id) VALUES(?,?,?,?,?,?,?,?,?,?)";
	synchronized(this){
		try {
			for (int i = 0; i < discountDetails.length(); ++i) {
			 JSONObject rec;
			    rec = discountDetails.getJSONObject(i);
			    List<Map<String, Object>> flag = jdbcTemplate.queryForList("SELECT * FROM stock_lift_with_discount WHERE brandNo = "+Integer.parseInt(rec.getString("brandNo"))+" AND stockDiscountDate= '"+date+"'  AND store_id="+storeId+"");
			    
			   // System.out.println("flag>> "+flag.size());
			    if(flag.size()>0 && flag != null){
			    	int val = jdbcTemplate.update("UPDATE stock_lift_with_discount SET discountAmount="+Double.parseDouble(rec.getString("discountAmount"))+", totalDiscountAmount="+Double.parseDouble(rec.getString("discountTotalAmount"))+",adjustment="+rec.getInt("adjustment")+",target="+rec.getInt("target")+",currentIndent="+rec.getInt("currentIndent")+",systemDate='"+systemDate+"' WHERE store_id="+storeId+" AND brandNo = "+Integer.parseInt(rec.getString("brandNo"))+" AND stockDiscountDate= '"+date+"'");
			    	 if(val > 0){
					    	result="Discount Data saved successfully";
					    }
			    }else{
			    	 int val=0;
			    	if(Double.parseDouble(rec.getString("discountTotalAmount")) > 0 || rec.getInt("adjustment") > 0 || rec.getInt("target") > 0 || rec.getInt("currentIndent") > 0)
			     val = jdbcTemplate.update(QUERY,new Object[]{Integer.parseInt(rec.getString("brandNo")),Integer.parseInt(rec.getString("companyId")),Double.parseDouble(rec.getString("discountAmount")),Double.parseDouble(rec.getString("discountTotalAmount")),date,rec.getInt("adjustment"),rec.getInt("target"),rec.getInt("currentIndent"),systemDate,storeId});
			    if(val > 0){
			    	result="Discount Data saved successfully";
			    }
			    }
			    
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			result="Something Went Wrong.";
			log.log(Level.WARN, "saveGroupingDiscountEstimateData in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
		}
		}
	return result;}

	@Override
	public String saveSchemeListData(JSONArray schemeJson, String date) {
		try{
		jdbcTemplate.update("DELETE FROM temp_indent_estimate");
		}catch(Exception e){e.printStackTrace();}
		 String QUERY="insert into temp_indent_estimate (brandNo,brandName,caseRate,noOfCases,target,lastMonthSold,companyOrder,companyColor,inHouseStock,commitment,productType,"
		 		+ "x,p,q,l2,l1,n,sb,d,lb,tin,l2SpecialMargin,l1SpecialMargin,qspecialMargin,pspecialMargin,nspecialMargin,dspecialMargin,sbspecialMargin,lbspecialMargin,tinspecialMargin,"
		 		+ "xspecialMargin,liftedCase,l2Stock,l1Stock,qstock,pstock,nstock,dstock,sbstock,lbstock,tinstock,xstock,date,days,company,category,pending,investMent,l2Val,l1Val,qVal,pVal,nVal,"
		 		+ "dVal,lbVal,sbVal,tinVal,xVal,totalSpecialMrp,l2NeedCase,l1NeedCase,qNeedCase,pNeedCase,nNeedCase,dNeedCase,lbNeedCase,"
		 		+ "sbNeedCase,tinNeedCase,xNeedCase,l2sale,l1sale,qsale,psale,nsale,dsale,sbsale,lbsale,tinsale,xsale,l2perday,l1perday,qperday,pperday,nperday,dperday,lbperday,sbperday,xperday,tinperday,flag) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		 String result=null;
			synchronized(this){
			try {
				for (int i = 0; i < schemeJson.length(); ++i) {
				 JSONObject rec;
				    rec = schemeJson.getJSONObject(i);
					int val=jdbcTemplate.update(QUERY,new Object[]{rec.getInt("brandNo"),rec.getString("brandName"),rec.getDouble("caseRate"),rec.getInt("noOfCases"),rec.getInt("target"),rec.getInt("lastMonthSold"),
							rec.getInt("companyOrder"),rec.getString("companyColor"),rec.getInt("inHouseStock"),rec.getInt("target"),rec.getString("productType"),rec.getDouble("x"),rec.getDouble("p"),rec.getDouble("q"),rec.getDouble("l2"),rec.getDouble("l1"),rec.getDouble("n"),
							rec.getDouble("sb"),rec.getDouble("d"),rec.getDouble("lb"),rec.getDouble("tin"),rec.getDouble("l2SpecialMargin"),rec.getDouble("l1SpecialMargin"),
							rec.getDouble("qspecialMargin"),rec.getDouble("pspecialMargin"),rec.getDouble("nspecialMargin"),rec.getDouble("dspecialMargin"),rec.getDouble("sbspecialMargin"),rec.getDouble("lbspecialMargin"),
							rec.getDouble("tinspecialMargin"),rec.getDouble("xspecialMargin"),rec.getInt("liftedCase"),rec.getDouble("l2Stock"),rec.getDouble("l1Stock"),
							rec.getDouble("qstock"),rec.getDouble("pstock"),rec.getDouble("nstock"),rec.getDouble("dstock"),rec.getDouble("sbstock"),rec.getDouble("lbstock"),
							rec.getDouble("tinstock"),rec.getDouble("xstock"),date,0,rec.getString("company"),rec.getString("category"),
							rec.getInt("pending"),rec.getInt("otherInvestment"),rec.getInt("l2NeedCase"),rec.getInt("l1NeedCase"),rec.getInt("qNeedCase"),rec.getInt("pNeedCase"),rec.getInt("nNeedCase"),rec.getInt("dNeedCase"),rec.getInt("lbNeedCase"),rec.getInt("sbNeedCase"),rec.getInt("tinNeedCase"),0,rec.getDouble("totalSpecialMrp"),
							rec.getInt("l2NeedCase"),rec.getInt("l1NeedCase"),rec.getInt("qNeedCase"),rec.getInt("pNeedCase"),rec.getInt("nNeedCase"),rec.getInt("dNeedCase"),
							rec.getInt("lbNeedCase"),rec.getInt("sbNeedCase"),rec.getInt("tinNeedCase"),rec.getInt("xNeedCase"),
							rec.getDouble("l2"),rec.getDouble("l1"),rec.getDouble("q"),rec.getDouble("p"),rec.getDouble("n"),rec.getDouble("d"),
							rec.getDouble("sb"),rec.getDouble("lb"),
							rec.getDouble("tin"),rec.getDouble("x"),rec.getDouble("l2perday"),rec.getDouble("l1perday"),rec.getDouble("qperday"),rec.getDouble("pperday"),
							rec.getDouble("nperday"),rec.getDouble("dperday"),rec.getDouble("lbperday"),rec.getDouble("sbperday"),rec.getDouble("xperday"),rec.getDouble("tinperday"),true});
					result="Indent Updated successfully";
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				result="Field should not be empty!";
				log.log(Level.WARN, "saveTempIndent in PoizonDaoImpl exception", e);
				e.printStackTrace();
			}
			}
			return result;
	}

	@Override
	public List<companyBean> getGroupList() {
		log.info("PoizonSecondDaoImpl.getGroupList()");
		String QUERY="SELECT group_id,groupName FROM product_group ORDER BY group_id";
		List<companyBean> companyList = new ArrayList<companyBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					companyBean company = new companyBean();
					company.setId(row.get("group_id")!=null?Long.parseLong(row.get("group_id").toString()):0);
					company.setName(row.get("groupName")!=null?row.get("groupName").toString():"");
					companyList.add(company);
				}
			}
		}catch(Exception e){
			log.log(Level.WARN, "getGroupList in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
			return companyList;
		}
		return companyList;
	}

}
