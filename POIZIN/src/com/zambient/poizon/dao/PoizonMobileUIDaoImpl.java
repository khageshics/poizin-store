package com.zambient.poizon.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.twilio.rest.chat.v1.service.User;
import com.zambient.poizon.bean.BalanceSheetBean;
import com.zambient.poizon.bean.BalanceSheetWithBreckage;
import com.zambient.poizon.bean.CreditChildBean;
import com.zambient.poizon.bean.CreditMasterBean;
import com.zambient.poizon.bean.DiscountTransactionBean;
import com.zambient.poizon.bean.MobileViewInHouseStock;
import com.zambient.poizon.bean.MobileViewInhouseStockCategoryWise;
import com.zambient.poizon.bean.MobileViewStockDiscountInner;
import com.zambient.poizon.bean.MobileViewStockLiftingDiscount;
import com.zambient.poizon.bean.SaleAndAmmountBean;
import com.zambient.poizon.bean.SaleBean;
import com.zambient.poizon.bean.SaleListBean;
import com.zambient.poizon.bean.TurnovertaxBean;
import com.zambient.poizon.bean.VendorAdjustmentGetArrearsBean;
import com.zambient.poizon.bean.data;
import com.zambient.poizon.utill.UserSession;

@PropertySource("classpath:/com/zambient/poizon/resources/poizon.properties")
@Repository("poizonMobileUIDao")
public class PoizonMobileUIDaoImpl implements PoizonMobileUIDao {
	final static Logger log = Logger.getLogger(PoizonMobileUIDaoImpl.class);
    @Autowired
	private JdbcTemplate jdbcTemplate;
    
    @Autowired 
	private UserSession userSession;
    
    @Value("${discountDate.1}")
   	private String discountDate1;
       
    @Value("${discountDate.2}")
   	private String discountDate2;
       
    @Value("${discountDate.4}")
   	private String discountDate4;
       
    @Value("${discountDate.5}")
    private String discountDate5;
    
    @Value("${discountDate.6}")
   	private String discountDate6;
    
    /**
	 * This method is used to get current  Inhouse stock
	 * */
	@Override
	public List<MobileViewInHouseStock> mobileViewInHouseStock() {
		List<MobileViewInHouseStock> mobileViewInHouseStock= new ArrayList<MobileViewInHouseStock>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT p.`shortBrandName`,p.`brandNo`,ROUND(SUM(s.`closing`/p.`packQty`)) AS closing,SUM(s.`closing`) AS stockInBtls, "
					+ "(SELECT t.color FROM `company_tab` t WHERE p.company=t.`companyId`) AS color,"
					+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p.company=t.`companyId`) AS companyOrder"
					+ " FROM product p INNER JOIN sale s ON p.`brandNoPackQty`=s.`brandNoPackQty` "
					+ "AND `saleDate`=(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1) GROUP BY p.`brandNo` ORDER BY p.`brandNo`");
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					MobileViewInHouseStock bean = new MobileViewInHouseStock();
					bean.setName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
					bean.setStock(row.get("closing")!=null?Long.parseLong(row.get("closing").toString()):0);
					bean.setStockInBottles(row.get("stockInBtls")!=null?Long.parseLong(row.get("stockInBtls").toString()):0);
					bean.setColor(row.get("color")!=null?row.get("color").toString():"");
					bean.setOrder(row.get("companyOrder")!=null?Long.parseLong(row.get("companyOrder").toString()):0);
					bean.setMobileViewInhouseStockCategoryWise(getForAllCategory(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0));
					mobileViewInHouseStock.add(bean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return mobileViewInHouseStock;
		}
		return mobileViewInHouseStock;
	}

	private List<MobileViewInhouseStockCategoryWise> getForAllCategory(long brandNo) {
		List<MobileViewInhouseStockCategoryWise> mobileViewInhouseStockCategoryWise = new ArrayList<MobileViewInhouseStockCategoryWise>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT p.`quantity`,s.closing,TRUNCATE(s.`closing`/p.`packQty`,0) AS casestock,"
					+ " (s.`closing`-(p.`packQty`*(TRUNCATE(s.`closing`/p.`packQty`,0)))) AS btls FROM product p INNER JOIN sale s ON p.`brandNoPackQty`=s.`brandNoPackQty` "
					+ "AND p.`brandNo`="+brandNo+"  AND `saleDate`=(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1)");
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					MobileViewInhouseStockCategoryWise bean = new MobileViewInhouseStockCategoryWise();
					bean.setQuantity(row.get("quantity")!=null?row.get("quantity").toString():"");
					bean.setClosing(row.get("closing")!=null?Long.parseLong(row.get("closing").toString()):0);
					bean.setCaseQty(row.get("casestock")!=null?Long.parseLong(row.get("casestock").toString()):0);
					bean.setBottle(row.get("btls")!=null?Long.parseLong(row.get("btls").toString()):0);
					mobileViewInhouseStockCategoryWise.add(bean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return mobileViewInhouseStockCategoryWise;
		}
		return mobileViewInhouseStockCategoryWise;
	}


	/*@Override
	public double InHouseStockAmount() {
		double result=0;
		String Query="SELECT ROUND(SUM(((SELECT i.SingleBottelRate FROM invoice i WHERE i.brandNoPackQty=s2.brandNoPackQty AND "
				+ "`invoiceDate`<=(SELECT MAX(`invoiceDateAsPerSheet`) FROM invoice WHERE `invoiceDateAsPerSheet`<=( SELECT `saleDate` FROM sale ORDER BY "
				+ "`saleDate` DESC LIMIT 1)) ORDER BY `invoiceId` DESC LIMIT 1)*s2.`closing`))+ (SELECT COALESCE(SUM(`totalPrice`),0) FROM `invoice` "
				+ "WHERE `invoiceDate` > (SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1)),0) AS inhouseAmount FROM sale s2 "
				+ " WHERE s2.`saleDate`= (SELECT MAX(`saleDate`) FROM sale WHERE `saleDate`<=(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1))";
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(Query);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					result=row.get("inhouseAmount")!=null?Double.parseDouble(row.get("inhouseAmount").toString()):0;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}*/
	/**
	 * This method is used to get current Inhouse stock Amount
	 * */
	@Override
	public double InHouseStockAmount() {
		double result=0;
		String date = null;
		try {
			date = jdbcTemplate.queryForObject("SELECT `saleDate` FROM sale WHERE store_id = "+userSession.getStoreId()+" ORDER BY `saleDate` DESC LIMIT 1", String.class);
		}catch (Exception e) {
			date = jdbcTemplate.queryForObject("SELECT DATE_SUB(`invoiceDate`, INTERVAL 1 DAY) AS invoiceDate FROM invoice WHERE `store_id` = "+userSession.getStoreId()+" ORDER BY `invoiceDateAsPerSheet` ASC LIMIT 1", String.class);
		}
		String Query="SELECT p.`shortBrandName`,p.`realBrandNo`,p.`brandNoPackQty`FROM product p WHERE "
				+ "(SELECT COALESCE(SUM(s.closing),0) + (SELECT COALESCE(SUM(`receivedBottles`),0) FROM invoice r WHERE r.invoiceDate > '"+date+"'"
				+ " AND r.brandNoPackQty = p.`brandNoPackQty` AND r.store_id = "+userSession.getStoreId()+") FROM sale s WHERE s.`saleDate`='"+date+"' "
				+ "AND s.brandNoPackQty = p.`brandNoPackQty` AND s.store_id = "+userSession.getStoreId()+") > 0 GROUP BY p.`brandNoPackQty` ORDER BY p.`realBrandNo`";
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(Query);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					result += getexactInvoiceValue(2,date,Double.parseDouble(row.get("brandNoPackQty").toString()));
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return result;
		}
		return result;
	}

	@Override
	public List<SaleBean> InHouseStockAmountWithSale() {
		//String date = jdbcTemplate.queryForObject("SELECT `saleDate` FROM sale WHERE `store_id` = "+userSession.getStoreId()+" ORDER BY `saleDate` DESC LIMIT 1", String.class);
		String date = null;
		int size = 0;
		try {
			date = jdbcTemplate.queryForObject("SELECT `saleDate` FROM sale WHERE store_id = "+userSession.getStoreId()+" ORDER BY `saleDate` DESC LIMIT 1", String.class);
		}catch (Exception e) {
			date = jdbcTemplate.queryForObject("SELECT DATE_SUB(`invoiceDate`, INTERVAL 1 DAY) AS invoiceDate FROM invoice WHERE `store_id` = "+userSession.getStoreId()+" ORDER BY `invoiceDateAsPerSheet` ASC LIMIT 1", String.class);
		}
		//int size = jdbcTemplate.queryForObject("SELECT COUNT(DISTINCT saleDate) FROM sale WHERE saleDate > (SELECT `saleDate` FROM sale WHERE `store_id`= "+userSession.getStoreId()+" ORDER BY `saleDate` DESC LIMIT 1) AND `store_id`= "+userSession.getStoreId()+"",Integer.class);
		
		List<SaleBean> saleBeanList =  new ArrayList<SaleBean>();
		/*
		 * String
		 * QUERY="SELECT p.`shortBrandName`,p.`realBrandNo`,p.`quantity`,p.`packQty`,p.saleSheetOrder,p.`company`,p.category,s.closing,p.`brandNoPackQty`,"
		 * +
		 * "ROUND(((SELECT i.SingleBottelRate FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND"
		 * +
		 * " `invoiceDate`<=(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1) ORDER BY `invoiceId` DESC LIMIT 1)*s.`closing`),0)  AS total,"
		 * + "TRUNCATE(s.`closing`/p.`packQty`,0) AS casestock," +
		 * "(SELECT t.color FROM `company_tab` t WHERE t.companyId=p.`company`) AS color,"
		 * +
		 * "(SELECT t.companyOrder FROM `company_tab` t WHERE t.companyId=p.`company`) AS companyOrder,"
		 * +
		 * "(SELECT t.companyName FROM `company_tab` t WHERE t.companyId=p.`company`) AS companyName,"
		 * +
		 * "(SELECT t.color FROM `category_tab` t WHERE t.categoryId=p.`category`) AS categoryColor,"
		 * +
		 * "(SELECT t.categoryOrder FROM `category_tab` t WHERE t.categoryId=p.`category`) AS categoryOrder,"
		 * +
		 * "(SELECT t.categoryName FROM `category_tab` t WHERE t.categoryId=p.`category`) AS categoryName,"
		 * +
		 * " (s.`closing`-(p.`packQty`*(TRUNCATE(s.`closing`/p.`packQty`,0)))) AS btls, "
		 * + " (SELECT TRUNCATE((SELECT COALESCE(s1.closing,0) FROM sale s1 " +
		 * " WHERE s1.`brandNoPackQty`=p.brandNoPackQty AND s1.saleDate =(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1))"
		 * +
		 * " /(SELECT TRUNCATE(COALESCE(SUM(s2.`sale`)/14,0),2) FROM sale s2  WHERE s2.`brandNoPackQty`= p.brandNoPackQty"
		 * +
		 * "  AND s2.saleDate >=(SELECT (SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1) -INTERVAL 14 DAY) "
		 * +
		 * "  AND s2.saleDate <=(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1)),0)) AS noOfDays,"
		 * +
		 * "ROUND(((SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty  AND `invoiceDate`<=(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1) ORDER BY `invoiceDate`"
		 * + " DESC LIMIT 1)*s.`closing`),0) AS totalPrice " +
		 * "FROM product p INNER JOIN sale s ON p.`brandNoPackQty`=s.`brandNoPackQty`" +
		 * "  AND `saleDate`=(SELECT `saleDate` FROM sale ORDER BY `saleDate` DESC LIMIT 1) AND s.`closing` > 0 ORDER BY p.`realBrandNo`"
		 * ;
		 */
		String QUERY2 = "SELECT p.`shortBrandName`,p.`realBrandNo`,p.`quantity`,p.`packQty`,p.saleSheetOrder,p.`company`,p.category,p.`brandNoPackQty`,"
				+ "(SELECT t.color FROM `company_tab` t WHERE t.companyId=p.`company`) AS color,(SELECT t.companyOrder FROM `company_tab` t WHERE t.companyId=p.`company`) "
				+ "AS companyOrder,(SELECT t.companyName FROM `company_tab` t WHERE t.companyId=p.`company`) AS companyName,(SELECT t.color FROM `category_tab` t "
				+ "WHERE t.categoryId=p.`category`) AS categoryColor,(SELECT t.categoryOrder FROM `category_tab` t WHERE t.categoryId=p.`category`) AS categoryOrder,"
				+ "(SELECT t.categoryName FROM `category_tab` t WHERE t.categoryId=p.`category`) AS categoryName,"
				+ "(SELECT TRUNCATE((COALESCE(SUM(s.closing),0) + (SELECT COALESCE(SUM(`receivedBottles`),0) FROM invoice r WHERE r.invoiceDate > '"+date+"'  "
				+ "AND r.brandNoPackQty = p.`brandNoPackQty` AND r.store_id = "+userSession.getStoreId()+"))/p.`packQty`,0) FROM sale s WHERE s.`saleDate`='"+date+"' AND s.brandNoPackQty = p.`brandNoPackQty` AND s.store_id = "+userSession.getStoreId()+")"
				+ " AS casestock, (SELECT COALESCE(SUM(s.closing),0) + (SELECT COALESCE(SUM(`receivedBottles`),0) FROM invoice r WHERE r.invoiceDate > '"+date+"' "
				+ "AND r.brandNoPackQty = p.`brandNoPackQty` AND r.store_id = "+userSession.getStoreId()+") FROM sale s WHERE s.`saleDate`='"+date+"' AND s.brandNoPackQty = p.`brandNoPackQty` AND s.store_id = "+userSession.getStoreId()+") AS closing,"
				+ "  (SELECT i.SingleBottelRate FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.store_id = "+userSession.getStoreId()+" ORDER BY `invoiceDate` DESC LIMIT 1) AS SingleBottelRate,"
				+ "(SELECT i.bottleSaleMrp FROM invoice i WHERE  i.brandNoPackQty=p.brandNoPackQty AND i.store_id = "+userSession.getStoreId()+"  ORDER BY `invoiceDate` DESC LIMIT 1) AS bottleSaleMrp,"
				+ "(SELECT TRUNCATE((SELECT COALESCE(SUM(s.closing),0) + (SELECT COALESCE(SUM(r.`receivedBottles`),0) FROM invoice r WHERE r.invoiceDate > '"+date+"'"
				+ " AND r.brandNoPackQty = p.`brandNoPackQty` AND r.store_id = "+userSession.getStoreId()+") FROM sale s WHERE s.`saleDate`='"+date+"' AND s.brandNoPackQty = p.`brandNoPackQty` AND s.store_id = "+userSession.getStoreId()+") /"
				+ "(SELECT TRUNCATE(COALESCE(SUM(s2.`sale`)/14,0),2) FROM sale s2  WHERE s2.`brandNoPackQty`= p.brandNoPackQty  "
				+ " AND s2.saleDate >=(SELECT (SELECT `saleDate` FROM sale WHERE store_id = "+userSession.getStoreId()+" ORDER BY `saleDate` DESC LIMIT 1) -INTERVAL 14 DAY) "
				+ " AND s2.saleDate <=(SELECT `saleDate` FROM sale WHERE store_id = "+userSession.getStoreId()+" ORDER BY `saleDate` DESC LIMIT 1) AND s2.store_id = "+userSession.getStoreId()+"),0)) AS noOfDays"
				+ " FROM product p WHERE (SELECT COALESCE(SUM(s.closing),0) + (SELECT COALESCE(SUM(r.`receivedBottles`),0) FROM invoice r"
				+ " WHERE r.invoiceDate > '"+date+"' AND r.brandNoPackQty = p.`brandNoPackQty` AND r.store_id = "+userSession.getStoreId()+") FROM sale s WHERE s.`saleDate`='"+date+"' "
				+ "AND s.brandNoPackQty = p.`brandNoPackQty` AND s.store_id = "+userSession.getStoreId()+") > 0 GROUP BY p.`brandNoPackQty` ORDER BY p.`realBrandNo`";
		//System.out.println(QUERY2);
		try{
		/*	if(size > 0){
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					SaleBean bean = new SaleBean();
					bean.setNoOfReturnsBtl(row.get("saleSheetOrder")!=null?Long.parseLong(row.get("saleSheetOrder").toString()):0);
					bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
					bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
					bean.setQuantity(row.get("quantity")!=null?row.get("quantity").toString():"");
					bean.setPackQty(row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0);
					bean.setClosing(row.get("closing")!=null?Long.parseLong(row.get("closing").toString()):0);
					bean.setCaseQty(row.get("casestock")!=null?Long.parseLong(row.get("casestock").toString()):0);
					bean.setColor(row.get("color")!=null?row.get("color").toString():"");
					bean.setCompanyOrder(row.get("companyOrder")!=null?Long.parseLong(row.get("companyOrder").toString()):0);
					bean.setQtyBottels(row.get("btls")!=null?Long.parseLong(row.get("btls").toString()):0);
					bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
					bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
					bean.setCategoryOrder(row.get("categoryOrder")!=null?Long.parseLong(row.get("categoryOrder").toString()):0);
					bean.setSalePrimaryKey(row.get("categoryColor")!=null?row.get("categoryColor").toString():""); // category color
					bean.setSaleId(row.get("company")!=null?Long.parseLong(row.get("company").toString()):0); // companyId
					bean.setInvoiceId(row.get("category")!=null?Long.parseLong(row.get("category").toString()):0); //categoryId
					bean.setProductId(row.get("noOfDays")!=null?Long.parseLong(row.get("noOfDays").toString()):0);
					bean.setTotalPrice(row.get("total")!=null?Double.parseDouble(row.get("total").toString()):0);
					bean.setCummulativePrice(row.get("totalPrice")!=null?Double.parseDouble(row.get("totalPrice").toString()):0);
					if((Long.parseLong(row.get("closing").toString())) > 0)
						bean.setSingleBottelPrice((double) Math.round(getexactInvoiceValue(1,date,Double.parseDouble(row.get("brandNoPackQty").toString()))));
					else 
						bean.setSingleBottelPrice(0.0);
					saleBeanList.add(bean);
					
				}
			}
			}else{*/
				List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY2);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						long closing  = row.get("closing")!=null?Long.parseLong(row.get("closing").toString()):0;
						long caseStock  = row.get("casestock")!=null?Long.parseLong(row.get("casestock").toString()):0;
						long btls = closing - ((row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0) * caseStock);
						double btlSaleMrp = row.get("bottleSaleMrp")!=null?Double.parseDouble(row.get("bottleSaleMrp").toString()):0;
						double singleBtlRate = row.get("SingleBottelRate")!=null?Double.parseDouble(row.get("SingleBottelRate").toString()):0;
						SaleBean bean = new SaleBean();
						bean.setNoOfReturnsBtl(row.get("saleSheetOrder")!=null?Long.parseLong(row.get("saleSheetOrder").toString()):0);
						bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
						bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
						bean.setQuantity(row.get("quantity")!=null?row.get("quantity").toString():"");
						bean.setPackQty(row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0);
						bean.setClosing(closing);
						bean.setCaseQty(caseStock);
						bean.setColor(row.get("color")!=null?row.get("color").toString():"");
						bean.setCompanyOrder(row.get("companyOrder")!=null?Long.parseLong(row.get("companyOrder").toString()):0);
						bean.setQtyBottels(btls);
						bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
						bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
						bean.setCategoryOrder(row.get("categoryOrder")!=null?Long.parseLong(row.get("categoryOrder").toString()):0);
						bean.setSalePrimaryKey(row.get("categoryColor")!=null?row.get("categoryColor").toString():""); // category color
						bean.setSaleId(row.get("company")!=null?Long.parseLong(row.get("company").toString()):0); // companyId
						bean.setInvoiceId(row.get("category")!=null?Long.parseLong(row.get("category").toString()):0); //categoryId
						bean.setProductId(row.get("noOfDays")!=null?Long.parseLong(row.get("noOfDays").toString()):0);
						bean.setTotalPrice((double) Math.round(closing * singleBtlRate));
						bean.setCummulativePrice((double) Math.round(closing * btlSaleMrp));
						if(closing > 0)
							bean.setSingleBottelPrice((double) Math.round(getexactInvoiceValue(2,date,Double.parseDouble(row.get("brandNoPackQty").toString()))));
						else 
							bean.setSingleBottelPrice(0.0);
						saleBeanList.add(bean);
						
					}
				}
			//}
		}catch(Exception e){
			e.printStackTrace();
			return saleBeanList;
		}
		return saleBeanList;
	}
	private double getexactInvoiceValue(int num,String startDate, double brandNoPackQty) {
		double exactInvoice=0;
		long totalSale = jdbcTemplate.queryForObject("SELECT COALESCE(SUM(s.sale),0) AS sale FROM sale s WHERE s.`brandNoPackQty`="+brandNoPackQty+" AND s.`saleDate` <='"+startDate+"' AND s.`store_id` = "+userSession.getStoreId()+"",Long.class);
		long stockVal =0,cnt=0;
	List<TurnovertaxBean> beanList = getInvoiceList(num,startDate,brandNoPackQty);
	for(TurnovertaxBean bean : beanList){
		stockVal = stockVal + bean.getStock();
 		if(stockVal >= totalSale){
 			if(cnt == 0){
				long val = stockVal - totalSale;
				exactInvoice += val * bean.getInvoiceRate();
				cnt++;
			}else{
			exactInvoice += bean.getStock() * bean.getInvoiceRate();
			cnt++;
			}	
 		}	
		
	}
	return exactInvoice;
}
	private List<TurnovertaxBean> getInvoiceList(int num, String startDate, double brandNoPackQty) {
		List<TurnovertaxBean> beanList = new ArrayList<TurnovertaxBean>();
		String QUERY = null;
		if(num == 1){
			QUERY = "SELECT p.`realBrandNo`,p.`brandNoPackQty`, i.`receivedBottles`, i.`invoiceDateAsPerSheet`,i.`SingleBottelRate` "
					+ "FROM product p INNER JOIN invoice i  ON p.`brandNoPackQty`=i.`brandNoPackQty`  WHERE i.`receivedBottles` > 0 AND i.`invoiceDateAsPerSheet` <= '"+startDate+"' AND p.`brandNoPackQty`="+brandNoPackQty+"  GROUP BY i.`invoiceDateAsPerSheet`  ";
		}else{
			/*
			 * QUERY =
			 * "SELECT p.`realBrandNo`,p.`brandNoPackQty`, i.`receivedBottles`, i.`invoiceDateAsPerSheet`,i.`SingleBottelRate` "
			 * +
			 * "FROM product p INNER JOIN invoice i  ON p.`brandNoPackQty`=i.`brandNoPackQty`  WHERE i.`receivedBottles` > 0  AND p.`brandNoPackQty`="
			 * +brandNoPackQty+"  GROUP BY i.`invoiceDateAsPerSheet`  ";
			 */
			 QUERY = "SELECT i.brandNoPackQty,SUM(i.receivedBottles) AS receivedBottles,i.SingleBottelRate,i.bottleSaleMrp,i.invoiceDateAsPerSheet," + 
			 		"s.turnoverTax " + 
			 		"FROM invoice i INNER JOIN invoice_mrp_round_off s ON i.`invoiceDateAsPerSheet` = s.`mrpRoundOffDateAsCopy` AND  receivedBottles > 0 " + 
			 		"AND i.`brandNoPackQty`="+brandNoPackQty+" AND i.`store_id` = "+userSession.getStoreId()+" AND s.`store_id` = "+userSession.getStoreId()+" " + 
			 		"GROUP BY i.invoiceDateAsPerSheet ORDER BY i.invoiceDateAsPerSheet ASC";
		}
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					TurnovertaxBean bean = new TurnovertaxBean();
					String invoicedate = row.get("invoiceDateAsPerSheet")!=null?row.get("invoiceDateAsPerSheet").toString():"";
					long received = row.get("receivedBottles")!=null?Long.parseLong(row.get("receivedBottles").toString()):0;
					double price = row.get("SingleBottelRate")!=null?Double.parseDouble(row.get("SingleBottelRate").toString()):0;
					/*if((sdf.parse(invoicedate).after(sdf.parse(discountDate1)) && sdf.parse(invoicedate).before(sdf.parse(discountDate4))) || (sdf.parse(invoicedate).after(sdf.parse(discountDate5)) && sdf.parse(invoicedate).before(sdf.parse(discountDate6))))*/
					if((row.get("turnoverTax")!=null?Double.parseDouble(row.get("turnoverTax").toString()):0) > 0){
						bean.setDate(invoicedate);
						bean.setInvoiceRate(price + (price * 14.5)/100);
						bean.setStock(received);
						beanList.add(bean);
       			      } 
       			 else{
       				bean.setDate(invoicedate);
					bean.setInvoiceRate(price);
					bean.setStock(received);
					beanList.add(bean);
       			  }
				}
			}
		   }catch(Exception e){
			e.printStackTrace();
			return beanList;
			}
		return beanList;
	}
	/**
	 * This method is used to get Day wise balance sheet
	 * */
	@Override
	public BalanceSheetWithBreckage dayWiseBalanceSheet(String sdate, String edate) {
		System.out.println(edate);
		BalanceSheetWithBreckage balanceSheetWithBreckage = new BalanceSheetWithBreckage();
		List<BalanceSheetBean> balanceShetBeanList = new ArrayList<BalanceSheetBean>();
     String QUERY = "SELECT UPPER(DATE_FORMAT(s.saleDate, '%d ' '%b ' '%Y')) AS saleDate, SUM(s.`totalPrice`) AS totalPrice, DAYNAME(s.saleDate) AS day, "
     		+ "(SELECT SUM(`totalAmount`) FROM `expense_master` WHERE `expenseMasterDate`=s.saleDate AND `store_id`="+userSession.getStoreId()+") AS totalAmount,"
     		+ "(SELECT `cardSale` FROM `card_cash_sale` WHERE `date`=s.saleDate AND `store_id`="+userSession.getStoreId()+") AS cardSale,"
     		+ "(SELECT `cashSale` FROM `card_cash_sale` WHERE `date`=s.saleDate AND `store_id`="+userSession.getStoreId()+") AS cashSale,"
     		+ "(SELECT `chequeSale` FROM `card_cash_sale` WHERE `date`=s.saleDate AND `store_id`="+userSession.getStoreId()+") AS chequeSale,"
     		+ "(SELECT `expenseMasterId` FROM `expense_master` WHERE `expenseMasterDate`=s.`saleDate` AND `store_id`="+userSession.getStoreId()+") AS expenseMasterId,"
     		+ "(((SELECT `cardSale` FROM `card_cash_sale` WHERE `date`=s.saleDate AND `store_id`="+userSession.getStoreId()+")+(SELECT `cashSale` FROM `card_cash_sale` "
     		+ "WHERE `date`=s.saleDate AND `store_id`="+userSession.getStoreId()+")+(SELECT `chequeSale` FROM `card_cash_sale` "
     		+ "WHERE `date`=s.saleDate AND `store_id`="+userSession.getStoreId()+")+(SELECT SUM(`totalAmount`) FROM `expense_master` WHERE `expenseMasterDate`=s.saleDate AND `store_id`="+userSession.getStoreId()+"))-(SUM(s.`totalPrice`))) AS diff "
     		+ " FROM sale s WHERE s.`saleDate`>='"+sdate+"' AND s.`saleDate`<='"+edate+"' AND s.`store_id`="+userSession.getStoreId()+" GROUP BY s.`saleDate`  ORDER BY s.saleDate DESC";
   try{
	   List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
		if(rows != null && rows.size()>0) {
			for(Map row : rows){
				BalanceSheetBean bean = new BalanceSheetBean();
				bean.setDate(row.get("saleDate")!=null?row.get("saleDate").toString():"");
				bean.setTotalPrice(row.get("totalPrice")!=null?Double.parseDouble(row.get("totalPrice").toString()):0);
				bean.setDay(row.get("day")!=null?row.get("day").toString():"");
				bean.setTotalAmount(row.get("totalAmount")!=null?Double.parseDouble(row.get("totalAmount").toString()):0);
				bean.setCradSale(row.get("cardSale")!=null?Double.parseDouble(row.get("cardSale").toString()):0);
				bean.setCashSale(row.get("cashSale")!=null?Double.parseDouble(row.get("cashSale").toString()):0);
				bean.setChequeSale(row.get("chequeSale")!=null?Double.parseDouble(row.get("chequeSale").toString()):0);
				bean.setExpenseMasterId(row.get("expenseMasterId")!=null?Long.parseLong(row.get("expenseMasterId").toString()):-1);
				bean.setRetention(row.get("diff")!=null?Double.parseDouble(row.get("diff").toString()):0);
				balanceShetBeanList.add(bean);
			}
		}
		balanceSheetWithBreckage.setBalanceSheetBean(balanceShetBeanList);
		balanceSheetWithBreckage.setData(getBreckageAmount(sdate,edate));
   }catch(Exception e){
	   e.printStackTrace();
	   return balanceSheetWithBreckage;
   }
		return balanceSheetWithBreckage;
	}
	private data getBreckageAmount(String sDate, String eDate) {
		data dataval = new data(); 
		String QUERY="SELECT SUM(breckageAmount) AS breckage FROM breckage_tab WHERE "
				+ "breckageDate>=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+sDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01')) AND breckageDate<='"+eDate+"' AND store_id="+userSession.getStoreId()+"";
		try{
			 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						dataval.setValue(row.get("breckage")!=null?Double.parseDouble(row.get("breckage").toString()):0);
					}
			 }
		}catch(Exception e){
			e.printStackTrace();
			return dataval;
		}
        
		return dataval;
	}
	/**
	 * This method is used to get no. of sold cases with product wise in between the selecetd dates.
	 * And also getting available current stock and prediction no. of days for currect stock.
	 * */
	@Override
	public SaleAndAmmountBean getSaleReports(String newSdate, String newEdate) {
		String previousDate=null;
		int dayIndex;
		Date date1 =null;
		try {
			 date1=new SimpleDateFormat("yyyy-MM-dd").parse(newSdate);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} 
	    /* SimpleDateFormat simpleDateformat = new SimpleDateFormat("E");
	        System.out.println(simpleDateformat.format(date1));*/
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(date1);
	        dayIndex = calendar.get(Calendar.DAY_OF_WEEK); // th
	        switch (dayIndex) { 
	        case 2: 
	        	previousDate = jdbcTemplate.queryForObject("SELECT DATE_SUB('"+newSdate+"', INTERVAL 4 DAY )", String.class);
	            break; 
	        case 3: 
	        	previousDate = jdbcTemplate.queryForObject("SELECT DATE_SUB('"+newSdate+"', INTERVAL 1 DAY )", String.class); 
	            break; 
	        case 4: 
	        	previousDate = jdbcTemplate.queryForObject("SELECT DATE_SUB('"+newSdate+"', INTERVAL 1 DAY )", String.class); 
	            break; 
	        case 5: 
	        	previousDate = jdbcTemplate.queryForObject("SELECT DATE_SUB('"+newSdate+"', INTERVAL 1 DAY )", String.class);
	            break; 
	        case 6: 
	        	previousDate = jdbcTemplate.queryForObject("SELECT DATE_SUB('"+newSdate+"', INTERVAL 7 DAY )", String.class);
	            break; 
	        case 7: 
	        	previousDate = jdbcTemplate.queryForObject("SELECT DATE_SUB('"+newSdate+"', INTERVAL 7 DAY )", String.class);
	            break; 
	        case 1: 
	        	previousDate = jdbcTemplate.queryForObject("SELECT DATE_SUB('"+newSdate+"', INTERVAL 7 DAY )", String.class);
	            break; 
	        default: 
	        	previousDate = newSdate; 
	            break; 
	        } 
	        String QUERY = null;
	        int size = jdbcTemplate.queryForObject("SELECT COUNT(DISTINCT saleDate) FROM sale WHERE saleDate > '"+newEdate+"' AND store_id="+userSession.getStoreId()+"",Integer.class);
	        if(size > 0){
		 QUERY = "SELECT DISTINCT p.productId,p.brandNoPackQty,p.realBrandNo,p.shortBrandName,p.productType,p.quantity,p.packQty,p.packType,p.category,"
		 		+ "(SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty = p.brandNoPackQty AND i.store_id="+userSession.getStoreId()+" ORDER BY i.invoiceDateAsPerSheet DESC LIMIT 1) AS saleRate,"
				+ "(SELECT sc.closing FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='"+newEdate+"' AND sc.store_id="+userSession.getStoreId()+") as stock,"
				+ "((SELECT sc.closing FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='"+newEdate+"' AND sc.store_id="+userSession.getStoreId()+")/p.packQty) AS closingCase,"
				+ "((SELECT sc.closing FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='"+newEdate+"' AND sc.store_id="+userSession.getStoreId()+")-(p.`packQty`*(TRUNCATE((SELECT sc.closing FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='"+newEdate+"' AND sc.store_id="+userSession.getStoreId()+")/p.`packQty`,0)))) AS closing,"
				+ "(SELECT t.categoryName FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryName,p.company,"
				+ "(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,"
				+ "(SELECT t.`companyId` FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyId,"
				+ "(SELECT t.color FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS color,"
				+ "(SELECT t.color FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryColor,(SELECT t.categoryOrder FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryOrder,s.`unitPrice`,"
				+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyorder,SUM(s.sale) AS sale,"
				+ "(SELECT SUM(s1.sale) FROM sale s1 WHERE s1.brandNoPackQty = p.brandNoPackQty AND s1.saleDate='"+previousDate+"' AND s1.store_id="+userSession.getStoreId()+") AS previousSale,"
				+ "(SUM(s.sale)/p.`packQty`) AS cases,SUM(s.totalPrice) total,"
				+ "(SELECT TRUNCATE((SELECT COALESCE(s1.closing,0) FROM sale s1 WHERE s1.`brandNoPackQty`=p.brandNoPackQty AND s1.saleDate ='"+newEdate+"' AND s1.store_id="+userSession.getStoreId()+")/(SELECT TRUNCATE(COALESCE(SUM(s2.`sale`)/14,0),2) FROM sale s2 "
				+ "WHERE s2.`brandNoPackQty`= p.brandNoPackQty AND s2.saleDate >=(SELECT '"+newEdate+"' -INTERVAL 14 DAY) AND s2.saleDate <='"+newEdate+"' AND s2.store_id="+userSession.getStoreId()+"),0)) AS noOfDays FROM product p "
				+ "INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`>='"+newSdate+"' AND `saleDate`<='"+newEdate+"' AND s.store_id="+userSession.getStoreId()+""
				+ " GROUP BY brandNoPackQty ORDER BY p.productId ASC";
	        }else{
	        	QUERY = "SELECT DISTINCT p.productId,p.brandNoPackQty,p.realBrandNo,p.shortBrandName,p.productType,p.quantity,p.packQty,p.packType,p.category,"
	        			+ "(SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty = p.brandNoPackQty AND i.store_id="+userSession.getStoreId()+" ORDER BY i.invoiceDateAsPerSheet DESC LIMIT 1) AS saleRate,"
	        			+ "(SELECT COALESCE(SUM(sc.closing),0) + (SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r WHERE r.invoiceDate > '"+newEdate+"' "
	        			+ "AND r.brandNoPackQty = p.`brandNoPackQty` AND r.store_id="+userSession.getStoreId()+") FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='"+newEdate+"' AND sc.store_id="+userSession.getStoreId()+") AS stock,"
	        			+ "(SELECT COALESCE(SUM(sc.closing),0) + (SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r WHERE r.invoiceDate > '"+newEdate+"' "
	        			+ "AND r.brandNoPackQty = p.`brandNoPackQty` AND r.store_id="+userSession.getStoreId()+") FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` "
	        			+ "AND sc.saleDate='"+newEdate+"' AND sc.store_id="+userSession.getStoreId()+")/p.packQty AS closingCase,((SELECT COALESCE(SUM(sc.closing),0) + (SELECT COALESCE(SUM(r.receivedBottles),0)"
	        			+ " FROM invoice r WHERE r.invoiceDate > '"+newEdate+"' AND r.brandNoPackQty = p.`brandNoPackQty` AND r.store_id="+userSession.getStoreId()+") FROM sale sc WHERE sc.brandNoPackQty = "
	        			+ "p.`brandNoPackQty` AND sc.saleDate='"+newEdate+"' AND sc.store_id="+userSession.getStoreId()+") - (p.`packQty`*(TRUNCATE((SELECT COALESCE(SUM(sc.closing),0) + "
	        			+ "(SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r WHERE r.invoiceDate > '"+newEdate+"' AND r.brandNoPackQty = p.`brandNoPackQty` AND r.store_id="+userSession.getStoreId()+") "
	        			+ "FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='"+newEdate+"' AND sc.store_id="+userSession.getStoreId()+")/p.packQty,0)))) AS closing, "
	        			+ "(SELECT t.categoryName FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryName,p.company,(SELECT t.companyName FROM"
	        			+ " `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,(SELECT t.`companyId` FROM `company_tab` t WHERE p.`company`=t.`companyId`) "
	        			+ "AS companyId,(SELECT t.color FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS color,(SELECT t.color FROM `category_tab` t "
	        			+ "WHERE p.`category`=t.`categoryId`) AS categoryColor,(SELECT t.categoryOrder FROM `category_tab` t WHERE p.`category`=t.`categoryId`)"
	        			+ " AS categoryOrder,s.`unitPrice`,(SELECT t.companyOrder FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyorder,SUM(s.sale) AS sale,"
	        			+ "(SELECT SUM(s1.sale) FROM sale s1 WHERE s1.brandNoPackQty = p.brandNoPackQty AND s1.saleDate='"+previousDate+"' AND s1.store_id="+userSession.getStoreId()+") AS previousSale,"
	        			+ "(SUM(s.sale)/p.`packQty`) AS cases,SUM(s.totalPrice) total,(SELECT TRUNCATE((SELECT COALESCE(SUM(sc.closing),0) + "
	        			+ "(SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r WHERE r.invoiceDate > '"+newEdate+"' AND r.brandNoPackQty = p.`brandNoPackQty` AND r.store_id="+userSession.getStoreId()+") "
	        			+ "FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='"+newEdate+"'  AND sc.store_id="+userSession.getStoreId()+")/(SELECT TRUNCATE(COALESCE(SUM(s2.`sale`)/14,0),2) "
	        			+ "FROM sale s2 WHERE s2.`brandNoPackQty`= p.brandNoPackQty AND s2.saleDate >=(SELECT '"+newEdate+"' -INTERVAL 14 DAY)"
	        			+ " AND s2.saleDate <='"+newEdate+"'  AND s2.store_id="+userSession.getStoreId()+"),0)) AS noOfDays FROM product p INNER JOIN sale s "
	        			+ " ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`>='"+newSdate+"' AND `saleDate`<='"+newEdate+"'  AND s.store_id="+userSession.getStoreId()+" GROUP BY brandNoPackQty "
	        			+ " ORDER BY p.productId ASC";
	        }
		SaleAndAmmountBean saleAndAmmountBean = new SaleAndAmmountBean();
		List<SaleBean> saleBeanList = new ArrayList<SaleBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					SaleBean saleBean = new SaleBean();
					saleBean.setProductId(Long.parseLong(row.get("productId").toString()));
					saleBean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
					saleBean.setBrandNo(Long.parseLong(row.get("realBrandNo").toString()));
					saleBean.setBrandName(row.get("shortBrandName").toString());
					saleBean.setProductType(row.get("productType").toString());
					saleBean.setQuantity(row.get("quantity").toString());
					saleBean.setPackQty(Long.parseLong(row.get("packQty").toString()));
					saleBean.setPackType(row.get("packType").toString());
					saleBean.setUnitPrice(row.get("unitPrice")!=null?Double.parseDouble(row.get("unitPrice").toString()):0);
					saleBean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
					saleBean.setTotalSale(row.get("sale")!= null?Long.parseLong(row.get("sale").toString()):0);
					saleBean.setTotalPrice(row.get("total")!=null?Double.parseDouble(row.get("total").toString()):0);
					saleBean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
					saleBean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
					saleBean.setInvoiceId(row.get("companyId")!=null?Long.parseLong(row.get("companyId").toString()):-1);
					saleBean.setColor(row.get("color")!=null?row.get("color").toString():"");
					saleBean.setCummulativePrice(row.get("cases")!=null?Double.parseDouble(row.get("cases").toString()):0);
					saleBean.setClosing(row.get("closing")!=null?Long.parseLong(row.get("closing").toString()):0);
					saleBean.setBottleSaleMrp(row.get("closingCase")!=null?Double.parseDouble(row.get("closingCase").toString()):0);
					saleBean.setNoOfReturnsBtl(row.get("noOfDays")!=null?Long.parseLong(row.get("noOfDays").toString()):0);
					saleBean.setQtyBottels(row.get("stock")!=null?Long.parseLong(row.get("stock").toString()):0);
					saleBean.setSaleId(row.get("category")!=null?Long.parseLong(row.get("category").toString()):0);
					saleBean.setInvoiceDate(row.get("categoryColor")!=null?row.get("categoryColor").toString():"");
					saleBean.setCategoryOrder(row.get("categoryOrder")!=null?Long.parseLong(row.get("categoryOrder").toString()):0);
					saleBean.setReceived(row.get("previousSale")!=null?Long.parseLong(row.get("previousSale").toString()):0);
					saleBean.setOpening((long) (row.get("saleRate")!=null?Double.parseDouble(row.get("saleRate").toString()):0));
					saleBeanList.add(saleBean);
					saleAndAmmountBean.setSaleBean(saleBeanList);
				}
			}
			saleAndAmmountBean.setAmount(jdbcTemplate.queryForObject("SELECT SUM(`totalAmount`) AS totalAmount FROM `expense_master` WHERE `expenseMasterDate`>='"+newSdate.toString()+"' AND expenseMasterDate <='"+newEdate.toString()+"' AND store_id = "+userSession.getStoreId()+"", Double.class));
			saleAndAmmountBean.setDiff(jdbcTemplate.queryForObject("SELECT (((SELECT SUM(`cardSale`) FROM `card_cash_sale` WHERE `date` >='"+newSdate.toString()+"' AND `date` <='"+newEdate.toString()+"' AND store_id = "+userSession.getStoreId()+")+(SELECT SUM(`cashSale`) "
					+ "FROM `card_cash_sale` WHERE `date` >='"+newSdate.toString()+"' AND `date` <='"+newEdate.toString()+"' AND store_id = "+userSession.getStoreId()+")+(SELECT SUM(`chequeSale`) FROM `card_cash_sale` "
					+ "WHERE `date` >='"+newSdate.toString()+"' AND `date` <='"+newEdate.toString()+"' AND store_id = "+userSession.getStoreId()+")+(SELECT SUM(`totalAmount`) FROM `expense_master` WHERE `expenseMasterDate` >='"+newSdate.toString()+"'"
					+ " AND `expenseMasterDate` <='"+newEdate.toString()+"' AND store_id = "+userSession.getStoreId()+"))-(SUM(s.`totalPrice`))) AS diff  FROM sale s WHERE s.`saleDate`>='"+newSdate.toString()+"' "
					+ "AND s.`saleDate`<='"+newEdate.toString()+"' AND s.store_id = "+userSession.getStoreId()+" ORDER BY saleDate DESC", Double.class));
			saleAndAmmountBean.setTotal(jdbcTemplate.queryForObject("SELECT (((SELECT SUM(`cardSale`) FROM `card_cash_sale` WHERE `date` >='"+newSdate.toString()+"' AND `date` <='"+newEdate.toString()+"' AND store_id = "+userSession.getStoreId()+")+(SELECT SUM(`cashSale`) "
					+ "FROM `card_cash_sale` WHERE `date` >='"+newSdate.toString()+"' AND `date` <='"+newEdate.toString()+"' AND store_id = "+userSession.getStoreId()+")+(SELECT SUM(`chequeSale`) FROM `card_cash_sale` "
					+ "WHERE `date` >='"+newSdate.toString()+"' AND `date` <='"+newEdate.toString()+"' AND store_id = "+userSession.getStoreId()+"))) AS totalAmt  FROM sale s WHERE s.`saleDate`>='"+newSdate.toString()+"' "
					+ "AND s.`saleDate`<='"+newEdate.toString()+"' AND store_id = "+userSession.getStoreId()+" ORDER BY saleDate DESC LIMIT 1", Double.class));
		}catch(Exception e){
			e.printStackTrace();
			return saleAndAmmountBean;
		}finally {
			
		}
		return saleAndAmmountBean;
	}

	private Double calculateTotalPrice(double brandNoPackQty, String newSdate, String newEdate) {
		double totalPrice=0;
               try{
            	   List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT (unitPrice*SUM(sale)) AS total FROM sale WHERE"
            	   		+ " `saleDate`>='"+newSdate+"' AND `saleDate`<='"+newEdate+"' AND `brandNoPackQty`="+brandNoPackQty+" GROUP BY brandNoPackQty,`unitPrice`");
            	   if(rows != null && rows.size()>0) {
       				for(Map row : rows){
       					totalPrice=totalPrice+(row.get("total")!=null?Double.parseDouble(row.get("total").toString()):0);
       				}
            	   }
               
               }catch(Exception e){
            	   e.printStackTrace();
            	   return totalPrice;
               }

		return totalPrice;
	}
	/**
	 * This method is used to get no. of stock lifted, getting total discount and other details for each month.
	 * */
	@Override
	public List<MobileViewStockLiftingDiscount> stockLiftingWithDiscountReport() {
		int storeId = userSession.getStoreId();
		List<MobileViewStockLiftingDiscount> mobileViewStockLiftingDiscount = new ArrayList<MobileViewStockLiftingDiscount>();
		ArrayList<Integer> arrlist = new ArrayList<Integer>();
		 String QUERY="SELECT `companyId` FROM `company_tab`";
	     try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						arrlist.add(row.get("companyId")!=null?Integer.parseInt(row.get("companyId").toString()):-1);
					}
				}
				//arrlist.add(1);
				
				if(arrlist.size() > 0){
					for (int companyid : arrlist) {
						List<MobileViewStockDiscountInner> mobileViewStockDiscountInner = new ArrayList<MobileViewStockDiscountInner>();
						MobileViewStockLiftingDiscount mobileViewStockLiftingDiscountBean = new MobileViewStockLiftingDiscount();
						String cName=null,colorval=null;
						long companyOrder=0;
						double totalDiscountAmt=0,totalReceived=0,totalAdjustment=0,vendoraAdj = 0, creditAmount=0;
						List<Map<String, Object>> rowdata = jdbcTemplate.queryForList("SELECT c.stockDiscountDate FROM `stock_lift_with_discount` c WHERE c.`store_id`="+storeId+" "
								+ "UNION SELECT MONTH FROM discount_transaction r WHERE r.store_id="+storeId+" ORDER BY stockDiscountDate DESC");
						if(rowdata != null && rowdata.size()>0) {
							for(Map row : rowdata){
								 MobileViewStockDiscountInner bean = new MobileViewStockDiscountInner();
								 List<Map<String, Object>> outerrows = jdbcTemplate.queryForList("SELECT COALESCE(SUM(d.`totalDiscountAmount`),0) AS discountamount,(SELECT t.`companyName` FROM `company_tab` t  WHERE t.`companyId`="+companyid+") AS company,"
											+ "(SELECT t.`companyOrder` FROM `company_tab` t  WHERE t.`companyId`="+companyid+") AS companyOrder,"
											+ "(SELECT t.`color` FROM `company_tab` t  WHERE t.`companyId`="+companyid+") AS color,"
											+ "UPPER(DATE_FORMAT('"+row.get("stockDiscountDate")+"', '%b' ' %y')) AS DATE,"
											+ "(SELECT COALESCE(SUM(ds.`rentalAmount`),0) FROM `rental_tab` ds WHERE ds.store_id = "+storeId+" AND ds.rentalDate = '"+row.get("stockDiscountDate")+"' AND ds.`company`="+companyid+") AS rental,"
											+ "(SELECT COALESCE(SUM(ds.`adj_amount`),0) FROM `vendor_adjustment` ds WHERE ds.store_id = "+storeId+" AND ds.vendor_month = '"+row.get("stockDiscountDate")+"' AND ds.`company`="+companyid+") AS vendorAmt,"
											+ "((SELECT COALESCE(SUM(ds.`transactionAmt`),0) FROM `discount_transaction` ds WHERE ds.store_id = "+storeId+" AND ds.month = '"+row.get("stockDiscountDate")+"' AND ds.`companyId`="+companyid+" AND ds.received=1 AND ds.transactionDate IS NOT NULL)"
											+ " +(SELECT COALESCE(SUM(ds.`adjCheck`),0) FROM `discount_transaction` ds WHERE ds.store_id = "+storeId+" AND ds.month = '"+row.get("stockDiscountDate")+"' AND ds.`companyId`="+companyid+" AND ds.received=1 AND ds.transactionDate IS NOT NULL)) "
											+ "AS chequeAmount, (SELECT COALESCE(SUM(ds.`adjCheck`),0) FROM `discount_transaction` ds WHERE ds.store_id = "+storeId+" AND ds.month = '"+row.get("stockDiscountDate")+"' AND ds.`companyId`="+companyid+" AND ds.received=1 AND ds.transactionDate IS NOT NULL)"
											+ " AS adjustmentCheque,(SELECT COALESCE(SUM(cm.totalAmount - cm.received ),0) FROM credit_master cm WHERE cm.store_id = "+storeId+" AND "
											+ "cm.masterDate >='"+row.get("stockDiscountDate")+"' AND "
											+ "cm.masterDate <= LAST_DAY('"+row.get("stockDiscountDate")+"') AND cm.company = "+companyid+") AS creditAmt,"
											+ " (((SELECT COALESCE(SUM(totalDiscountAmount),0) FROM stock_lift_with_discount WHERE store_id = "+storeId+" AND stockDiscountDate"
											+ " >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount` WHERE store_id = "+storeId+"  ORDER BY stockDiscountDate ASC LIMIT 1) AND stockDiscountDate<='"+row.get("stockDiscountDate")+"' "
											+ "AND companyName="+companyid+")+(SELECT COALESCE(SUM(rentalAmount),0) FROM rental_tab  WHERE store_id = "+storeId+" AND rentalDate >=(SELECT `stockDiscountDate`"
											+ " FROM `stock_lift_with_discount` WHERE store_id = "+storeId+" ORDER BY stockDiscountDate ASC LIMIT 1) AND rentalDate<='"+row.get("stockDiscountDate")+"' AND company="+companyid+")+"
											+ "(SELECT COALESCE(SUM(cm.totalAmount - cm.received ),0) FROM credit_master cm WHERE cm.store_id = "+storeId+" AND cm.masterDate >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount` WHERE store_id = "+storeId+" ORDER BY stockDiscountDate ASC LIMIT 1) "
											+ "AND cm.masterDate <= LAST_DAY('"+row.get("stockDiscountDate")+"') AND cm.company = "+companyid+"))-"
											+ " ((SELECT COALESCE(SUM(transactionAmt+adjCheck),0) FROM discount_transaction WHERE store_id = "+storeId+" AND MONTH >=(SELECT `stockDiscountDate` FROM"
											+ " `stock_lift_with_discount` WHERE store_id = "+storeId+" ORDER BY stockDiscountDate ASC LIMIT 1) AND MONTH<='"+row.get("stockDiscountDate")+"' AND companyId="+companyid+" AND received=1 AND transactionDate IS NOT NULL) "
											+ "+ (SELECT COALESCE(SUM(adj_amount),0) FROM vendor_adjustment  WHERE store_id = "+storeId+" AND vendor_month >=(SELECT `stockDiscountDate` FROM `stock_lift_with_discount` "
											+ " WHERE store_id = "+storeId+" ORDER BY stockDiscountDate ASC LIMIT 1) AND vendor_month<='"+row.get("stockDiscountDate")+"' AND company="+companyid+"))) AS arrears "
											+ "FROM `stock_lift_with_discount` d WHERE d.store_id = "+storeId+" AND d.`companyName`="+companyid+"  AND d.`stockDiscountDate` = '"+row.get("stockDiscountDate")+"'");
								
								 if(outerrows != null && outerrows.size()>0) {
										for(Map outerrow : outerrows){
									    bean.setDate(outerrow.get("date")!=null?outerrow.get("date").toString():"");
									    bean.setDiscount(outerrow.get("discountamount")!=null?Double.parseDouble(outerrow.get("discountamount").toString()):0);
									    bean.setRentals(outerrow.get("rental")!=null?Double.parseDouble(outerrow.get("rental").toString()):0);
									    bean.setVendorAmount(outerrow.get("vendorAmt")!=null?Double.parseDouble(outerrow.get("vendorAmt").toString()):0);
									    bean.setRecieved(outerrow.get("chequeAmount")!=null?Double.parseDouble(outerrow.get("chequeAmount").toString()):0);
									    bean.setCompanyId(companyid);
									    bean.setRealDate(row.get("stockDiscountDate")!=null?row.get("stockDiscountDate").toString():"");
									    bean.setDiscountAsPerCompanyBeenList(getDiscountAsPerCompanyWise(row.get("stockDiscountDate")!=null?row.get("stockDiscountDate").toString():"",companyid));
                                        bean.setArrears(outerrow.get("arrears")!=null?Double.parseDouble(outerrow.get("arrears").toString()):0);
                                        mobileViewStockDiscountInner.add(bean);
                                        totalDiscountAmt=totalDiscountAmt+(outerrow.get("discountamount")!=null?Double.parseDouble(outerrow.get("discountamount").toString()):0)+(outerrow.get("rental")!=null?Double.parseDouble(outerrow.get("rental").toString()):0);
                                        totalReceived=totalReceived+(outerrow.get("chequeAmount")!=null?Double.parseDouble(outerrow.get("chequeAmount").toString()):0);
                                        cName=outerrow.get("company")!=null?outerrow.get("company").toString():""; 
                                        companyOrder=outerrow.get("companyOrder")!=null?Long.parseLong(outerrow.get("companyOrder").toString()):0;
                                        colorval=outerrow.get("color")!=null?outerrow.get("color").toString():"";
                                        vendoraAdj += outerrow.get("vendorAmt")!=null?Double.parseDouble(outerrow.get("vendorAmt").toString()):0;
                                        creditAmount += outerrow.get("creditAmt")!=null?Double.parseDouble(outerrow.get("creditAmt").toString()):0;
										}
									}
							}
						}
						mobileViewStockLiftingDiscountBean.setCompany(cName);
						mobileViewStockLiftingDiscountBean.setCompanyOrder(companyOrder);
						mobileViewStockLiftingDiscountBean.setColor(colorval);
						mobileViewStockLiftingDiscountBean.setDuesAmt((totalDiscountAmt + creditAmount) - (totalReceived+vendoraAdj));
						mobileViewStockLiftingDiscountBean.setMobileViewStockDiscountInner(mobileViewStockDiscountInner);
						mobileViewStockLiftingDiscount.add(mobileViewStockLiftingDiscountBean);
						mobileViewStockLiftingDiscountBean.setTotalVendorAmount(vendoraAdj);
					}
					
				}
	     }catch(Exception e){
	    	 e.printStackTrace();
	    	 return mobileViewStockLiftingDiscount;
	    	 }
		return mobileViewStockLiftingDiscount;
	}

	private List<DiscountTransactionBean> getDiscountAsPerCompanyWise(String stockDate, int company) {
		List<DiscountTransactionBean> discountAsPerCompanyBeenList = new ArrayList<DiscountTransactionBean>();
		String QUERY="SELECT dt.companyId,dt.month,dt.transactionType,dt.transactionAmt,UPPER(DATE_FORMAT(dt.transactionDate, '%d ' '%b ' '%Y')) AS transactionDate,dt.adjCheck,dt.imageName,dt.imagePath,dt.bank,"
				+ "(SELECT r.rentalAmount FROM rental_tab r WHERE dt.companyId = r.company AND dt.month=r.rentalDate) AS rentalAmount"
				+ " FROM discount_transaction dt"
				+ " WHERE dt.month='"+stockDate+"' AND dt.companyId="+company+" AND dt.received=1 AND dt.transactionDate IS NOT NULL";
         
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					DiscountTransactionBean bean = new DiscountTransactionBean();
					bean.setCompanyId(row.get("companyId")!=null?Long.parseLong(row.get("companyId").toString()):0);
					bean.setTransactionType(row.get("transactionType")!=null?row.get("transactionType").toString():"");
					bean.setTransactionAmt(row.get("transactionAmt")!=null?Double.parseDouble(row.get("transactionAmt").toString()):0);
					bean.setAdjCheck(row.get("adjCheck")!=null?Double.parseDouble(row.get("adjCheck").toString()):0);
					bean.setImageName(row.get("imageName")!=null?row.get("imageName").toString():"");
					bean.setNgalleryImage(row.get("imagePath")!=null?row.get("imagePath").toString():"");
					bean.setRental(row.get("rentalAmount")!=null?Double.parseDouble(row.get("rentalAmount").toString()):0);
					bean.setTransactionDate(row.get("transactionDate")!=null?row.get("transactionDate").toString():"");
					bean.setBank(row.get("bank")!=null?row.get("bank").toString():"");
					discountAsPerCompanyBeenList.add(bean);
				}
			}
		}catch(Exception e){
			log.log(Level.WARN, "getDiscountAsPerCompanyWise private method in PoizonMobileUiDaoImpl exception", e);
			e.printStackTrace();
			return discountAsPerCompanyBeenList;
		}
		return discountAsPerCompanyBeenList;
	}
	/**
	 * This method is used to get complete discount details for select company and till date
	 * */
	@Override
	public List<SaleBean> getTillMonthDiscountDetails(String date, int companyid) {
		
		int storeId=userSession.getStoreId();
		
		List<SaleBean> saleBean= new ArrayList<SaleBean>();
		String QUERY = "SELECT p.realBrandNo,p.matchName,p.matchs,p.saleSheetOrder,p.shortBrandName,(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,"
				+ "(SELECT COALESCE(SUM(d.discountAmount),0) FROM stock_lift_with_discount d WHERE d.store_id="+storeId+" AND d.brandNo=p.realBrandNo AND d.stockDiscountDate='"+date+"') AS discountAmount,"
				+ "(SELECT COALESCE(SUM(dt.totalDiscountAmount),0) FROM stock_lift_with_discount dt WHERE dt.store_id="+storeId+" AND dt.brandNo=p.realBrandNo AND dt.stockDiscountDate='"+date+"')  AS totalDiscountAmount,"
				+ "(CEIL(SUM(i.receivedBottles/i.packQty))+(SELECT COALESCE(SUM(d.adjustment),0) FROM stock_lift_with_discount d WHERE d.store_id="+storeId+" AND d.brandNo=p.realBrandNo AND d.stockDiscountDate='"+date+"')) AS totalcases"
				+ "  FROM product p INNER JOIN invoice i  ON p.`brandNoPackQty` = i.`brandNoPackQty` AND `invoiceDateAsPerSheet`>='"+date+"'"
				+ " AND `invoiceDateAsPerSheet`<=(SELECT LAST_DAY('"+date+"')) AND p.`company`="+companyid+" AND i.store_id="+storeId+" GROUP BY p.realBrandNo";
		try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						SaleBean bean = new SaleBean();
						bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
						bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
						bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
						bean.setUnitPrice(row.get("discountAmount")!=null?Double.parseDouble(row.get("discountAmount").toString()):0);
						bean.setTotalPrice(row.get("totalDiscountAmount")!=null?Double.parseDouble(row.get("totalDiscountAmount").toString()):0);
						bean.setCaseQty(row.get("totalcases")!=null?Long.parseLong(row.get("totalcases").toString()):0);
						bean.setInvoiceId(row.get("matchs")!=null?Long.parseLong(row.get("matchs").toString()):0);
						bean.setProductType(row.get("matchName")!=null?row.get("matchName").toString():"");
						bean.setCompanyOrder(row.get("saleSheetOrder")!=null?Long.parseLong(row.get("saleSheetOrder").toString()):0);
						saleBean.add(bean);
					}
				}
		}catch(Exception e){
			log.log(Level.WARN, "getLastDateCredit in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
			return saleBean;
		}
		return saleBean;
	}

	private long getStockDetails(String date, int companyid, long brandNo) {
		ArrayList<Long> stockArr = new ArrayList<Long>();
		double totalCase=0;long noOfdate=5;
		String QUERY ="SELECT DISTINCT (SELECT (s.closing/p.packQty) FROM sale s WHERE s.brandNoPackQty = p.brandNoPackQty AND s.saleDate= "
				+ "(SELECT MAX(saleDate) FROM sale WHERE saleDate<= (SELECT LAST_DAY('"+date+"')))) AS closingCase,"
				+ "((SELECT s.closing FROM sale s WHERE s.brandNoPackQty = p.brandNoPackQty AND s.saleDate= "
				+ "(SELECT MAX(saleDate) FROM sale WHERE saleDate<= (SELECT LAST_DAY('"+date+"'))))-(SELECT TRUNCATE(SUM(s.closing/p.packQty),0)*p.packQty FROM sale s "
				+ "WHERE s.brandNoPackQty = p.brandNoPackQty AND s.saleDate= (SELECT MAX(saleDate) FROM sale WHERE saleDate<= (SELECT LAST_DAY('"+date+"'))))) AS Btls,"
				+ " TRUNCATE((SELECT (s.closing) FROM sale s WHERE s.brandNoPackQty = p.brandNoPackQty AND s.saleDate= "
				+ "(SELECT MAX(saleDate) FROM sale WHERE saleDate<= (SELECT LAST_DAY('"+date+"'))))/(SELECT TRUNCATE(SUM(s.sale)/14,2) FROM sale s "
				+ "WHERE s.brandNoPackQty = p.brandNoPackQty AND s.saleDate>=(SELECT (SELECT MAX(saleDate) FROM sale WHERE saleDate<= (SELECT LAST_DAY('"+date+"'))) -INTERVAL 14 DAY) "
				+ "  AND s.saleDate<=(SELECT MAX(saleDate) FROM sale WHERE saleDate<= (SELECT LAST_DAY('"+date+"')))),0) AS days "
				+ "FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND brandNo="+brandNo+" AND company="+companyid+"";
		try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						totalCase+=row.get("closingCase")!=null?Double.parseDouble(row.get("closingCase").toString()):0;
						if((row.get("days")!=null?Long.parseLong(row.get("days").toString()):0) <=1)
							noOfdate=row.get("days")!=null?Long.parseLong(row.get("days").toString()):0;
					}
				}
				//stockArr.add(Math.round(totalCase));
				//stockArr.add(noOfdate);
		}catch(Exception e){
			e.printStackTrace();
			return Math.round(totalCase);
		}
		return Math.round(totalCase);
	}
	/**
	 * This method is used to get Lifted no. of cases, target and prediction days
	 * */
	@Override
	public List<SaleBean> getStockLiftingReports(String date) {
		int storeId= userSession.getStoreId();String saleDate = null;
		int size = jdbcTemplate.queryForObject("SELECT COUNT(DISTINCT saleDate) FROM sale WHERE store_id = "+storeId+" AND saleDate > LAST_DAY('"+date+"')", Integer.class);
		try {
		 saleDate = jdbcTemplate.queryForObject("SELECT `saleDate` FROM sale WHERE store_id="+storeId+" ORDER BY saleDate DESC LIMIT 1", String.class);
		}catch (Exception e) {
			saleDate = jdbcTemplate.queryForObject("SELECT `invoiceDate` FROM invoice WHERE store_id = "+storeId+" ORDER BY `invoiceDate` ASC LIMIT 1", String.class);
		}
		List<SaleBean> saleBeanList = new ArrayList<SaleBean>();
		String QUERY="SELECT DISTINCT p.realBrandNo,p.`shortBrandName`,p.`quantity`,p.packQty,p.category,p.company,"
				+ "(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,"
				+ "(SELECT t.categoryName FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryName,"
				+ "(SELECT t.color FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryColor,"
				+ "(SELECT t.categoryOrder FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryOrder,"
				+ "(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,"
				+ "(SELECT t.color FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS color,"
				+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyorder,"
				+ "(SELECT ROUND(SUM(`totalPrice`), 0)  FROM invoice WHERE store_id ="+storeId+" AND `invoiceDateAsPerSheet`>='2019-11-01' AND `invoiceDateAsPerSheet`<=(SELECT LAST_DAY('"+date+"'))) "
				+ "AS cummulativePrice, (SELECT ROUND(SUM(`tcsVal`), 0)  FROM invoice_mrp_round_off WHERE store_id ="+storeId+" AND  `mrpRoundOffDateAsCopy`>='2019-11-01' "
				+ "AND `mrpRoundOffDateAsCopy`<=(SELECT LAST_DAY('"+date+"'))) AS tcsVal,"
				+ "CEIL(SUM(i.receivedBottles)/p.packQty) AS caseQty,"
				+ "(SELECT (s.closing/p.packQty) FROM sale s WHERE s.store_id ="+storeId+" AND  s.brandNoPackQty = p.brandNoPackQty AND s.saleDate= (SELECT MAX(saleDate) FROM sale "
				+ "WHERE store_id ="+storeId+" AND  saleDate<= (SELECT LAST_DAY('"+date+"')))) AS closing,"
				+ "(SELECT (TRUNCATE((s.closing/p.packQty),0)) FROM sale s WHERE s.store_id ="+storeId+" AND  s.brandNoPackQty = p.brandNoPackQty AND s.saleDate= (SELECT MAX(saleDate) "
				+ "FROM sale WHERE store_id ="+storeId+" AND  saleDate<= (SELECT LAST_DAY('"+date+"')))) AS closingCase,((SELECT s.closing FROM sale s WHERE s.store_id ="+storeId+" AND  s.brandNoPackQty = p.brandNoPackQty "
				+ "AND s.saleDate= (SELECT MAX(saleDate) FROM sale WHERE store_id ="+storeId+" AND  saleDate<= (SELECT LAST_DAY('"+date+"'))))-((SELECT (TRUNCATE((s.closing/p.packQty),0)) "
				+ "FROM sale s WHERE s.store_id ="+storeId+" AND  s.brandNoPackQty = p.brandNoPackQty AND s.saleDate= (SELECT MAX(saleDate) FROM sale "
				+ "WHERE store_id ="+storeId+" AND  saleDate<= (SELECT LAST_DAY('"+date+"'))))*p.packQty)) AS Btls, (SELECT COALESCE(SUM(d.target),0) FROM stock_lift_with_discount d "
				+ "WHERE d.store_id ="+storeId+" AND  d.brandNo=p.realBrandNo AND d.stockDiscountDate='"+date+"') AS target,"
				+ "TRUNCATE((SELECT (s.closing) FROM sale s WHERE s.store_id ="+storeId+" AND  s.brandNoPackQty = p.brandNoPackQty AND s.saleDate=(SELECT MAX(saleDate) FROM sale"
				+ " WHERE store_id ="+storeId+" AND  saleDate<= (SELECT LAST_DAY('"+date+"'))))/(SELECT TRUNCATE(SUM(s.sale)/14,2) FROM sale s WHERE s.store_id ="+storeId+" AND  s.brandNoPackQty = p.brandNoPackQty AND s.saleDate>=(SELECT (SELECT MAX(saleDate) FROM sale"
				+ " WHERE store_id ="+storeId+" AND  saleDate<= (SELECT LAST_DAY('"+date+"'))) -INTERVAL 14 DAY)   AND s.saleDate<=(SELECT MAX(saleDate) FROM sale "
				+ " WHERE store_id ="+storeId+" AND  saleDate<= (SELECT LAST_DAY('"+date+"')))),0) AS days"
						+ " FROM product p INNER JOIN invoice i  "
				+ "ON p.`brandNoPackQty` = i.`brandNoPackQty` AND i.store_id ="+storeId+"  AND `invoiceDateAsPerSheet`>='"+date+"' AND `invoiceDateAsPerSheet`<=(SELECT LAST_DAY('"+date+"'))    GROUP BY p.brandNoPackQty ORDER BY p.realBrandNo ASC";
		
		String QUERY2 = "SELECT DISTINCT p.realBrandNo,p.`shortBrandName`,p.`quantity`,p.packQty,p.category,p.company,"
				+ "(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,(SELECT t.categoryName FROM `category_tab` t"
				+ " WHERE p.`category`=t.`categoryId`) AS categoryName,(SELECT t.color FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryColor,"
				+ " (SELECT t.categoryOrder FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryOrder,(SELECT t.companyName FROM `company_tab` t "
				+ " WHERE p.`company`=t.`companyId`) AS companyName,(SELECT t.color FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS color,(SELECT t.companyOrder"
				+ "  FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyorder,(SELECT ROUND(SUM(`totalPrice`), 0)  FROM invoice "
				+ "WHERE store_id="+storeId+" AND `invoiceDateAsPerSheet`>='2019-11-01' AND `invoiceDateAsPerSheet`<=(SELECT LAST_DAY('"+date+"'))) AS cummulativePrice,"
				+ "(SELECT ROUND(SUM(`tcsVal`), 0)  FROM invoice_mrp_round_off WHERE store_id="+storeId+" AND `mrpRoundOffDateAsCopy`>='2019-11-01' "
				+ "AND `mrpRoundOffDateAsCopy`<=(SELECT LAST_DAY('"+date+"'))) AS tcsVal,CEIL(SUM(i.receivedBottles)/p.packQty) AS caseQty,"
				+ "(SELECT ((COALESCE(SUM(s.closing),0) + (SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r WHERE r.store_id="+storeId+" AND  r.invoiceDate > '"+saleDate+"' AND r.brandNoPackQty = p.`brandNoPackQty`))/p.packQty) FROM sale s "
				+ "WHERE s.store_id="+storeId+" AND  s.brandNoPackQty = p.brandNoPackQty AND s.saleDate= (SELECT MAX(saleDate) FROM sale"
				+ " WHERE store_id="+storeId+" AND  saleDate<= (SELECT LAST_DAY('"+date+"')))) AS closing,"
				+ "(SELECT TRUNCATE((COALESCE(SUM(s.closing),0) + (SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r"
				+ " WHERE r.store_id="+storeId+" AND  r.invoiceDate > '"+saleDate+"' AND r.brandNoPackQty = p.`brandNoPackQty`))/p.packQty,0)"
				+ " FROM sale s WHERE s.store_id="+storeId+" AND s.brandNoPackQty = p.brandNoPackQty AND s.saleDate= (SELECT MAX(saleDate) FROM sale"
				+ " WHERE store_id="+storeId+" AND  saleDate<= (SELECT LAST_DAY('"+date+"')))) AS closingCase,"
				+ "((SELECT COALESCE(SUM(s.closing),0)+ (SELECT COALESCE(SUM(r.receivedBottles),0)"
				+ " FROM invoice r WHERE r.store_id="+storeId+" AND  r.invoiceDate > '"+saleDate+"' AND r.brandNoPackQty = p.`brandNoPackQty`) "
				+ "FROM sale s WHERE s.store_id="+storeId+" AND  s.brandNoPackQty = p.brandNoPackQty AND s.saleDate= (SELECT MAX(saleDate) FROM sale "
				+ "WHERE store_id="+storeId+" AND  saleDate<= (SELECT LAST_DAY('"+date+"'))))-(((SELECT TRUNCATE((COALESCE(SUM(s.closing),0) + (SELECT COALESCE(SUM(r.receivedBottles),0) "
				+ "FROM invoice r WHERE r.store_id="+storeId+" AND  r.invoiceDate > '"+saleDate+"' "
				+ "AND r.brandNoPackQty = p.`brandNoPackQty`))/p.packQty,0) FROM sale s WHERE s.store_id="+storeId+" AND  s.brandNoPackQty = p.brandNoPackQty AND "
				+ "s.saleDate= (SELECT MAX(saleDate) FROM sale WHERE store_id="+storeId+" AND  saleDate<= (SELECT LAST_DAY('"+date+"')))))*p.packQty)) AS Btls, "
				+ "(SELECT COALESCE(SUM(d.target),0) FROM stock_lift_with_discount d  WHERE d.store_id="+storeId+" AND d.brandNo=p.realBrandNo AND d.stockDiscountDate='"+date+"') AS target,"
				+ " TRUNCATE((SELECT COALESCE(SUM(s.closing),0) + (SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r WHERE"
				+ " r.store_id="+storeId+" AND  r.invoiceDate > '"+saleDate+"' AND r.brandNoPackQty = p.`brandNoPackQty`) FROM sale s "
				+ "WHERE s.store_id="+storeId+" AND  s.brandNoPackQty = p.brandNoPackQty AND s.saleDate=(SELECT MAX(saleDate) FROM sale WHERE "
				+ " store_id="+storeId+" AND  saleDate<= (SELECT LAST_DAY('"+date+"'))))/(SELECT TRUNCATE(SUM(s.sale)/14,2) FROM sale s WHERE s.store_id="+storeId+" AND  s.brandNoPackQty = p.brandNoPackQty "
				+ "AND s.saleDate>=(SELECT (SELECT MAX(saleDate) FROM sale WHERE store_id="+storeId+" AND  saleDate<= (SELECT LAST_DAY('"+date+"'))) -INTERVAL 14 DAY) "
				+ "AND s.saleDate<=(SELECT MAX(saleDate) FROM sale WHERE store_id="+storeId+" AND  saleDate<= (SELECT LAST_DAY('"+date+"')))),0) AS days"
				+ " FROM product p INNER JOIN invoice i  ON p.`brandNoPackQty` = i.`brandNoPackQty`"
				+ " AND `invoiceDateAsPerSheet`>='"+date+"' AND `invoiceDateAsPerSheet`<=(SELECT LAST_DAY('"+date+"')) AND i.store_id="+storeId+" "
				+ "GROUP BY p.brandNoPackQty ORDER BY p.realBrandNo ASC";
		try{
			if(size > 0){
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						SaleBean bean = new SaleBean();
						bean.setInvoiceId(row.get("company")!=null?Long.parseLong(row.get("company").toString()):0);
						bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
						bean.setQuantity(row.get("quantity")!=null?row.get("quantity").toString():"");
						bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
						bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
						bean.setColor(row.get("color")!=null?row.get("color").toString():"");
						bean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
						bean.setCaseQty(row.get("caseQty")!=null?Long.parseLong(row.get("caseQty").toString()):0);
						bean.setPackQty(row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0);
						bean.setClosing(row.get("closingCase")!=null?Long.parseLong(row.get("closingCase").toString()):0);
						bean.setCummulativePrice(row.get("closing")!=null?Double.parseDouble(row.get("closing").toString()):0);//closing value
						bean.setNoOfReturnsBtl(row.get("Btls")!=null?Long.parseLong(row.get("Btls").toString()):0);// closing in btls
						bean.setSaleId(row.get("target")!=null?Long.parseLong(row.get("target").toString()):0);
						bean.setReceived(row.get("days")!=null?Long.parseLong(row.get("days").toString()):0);
						bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
						bean.setCategoryOrder(row.get("categoryOrder")!=null?Long.parseLong(row.get("categoryOrder").toString()):0);
						bean.setSalePrimaryKey(row.get("categoryColor")!=null?row.get("categoryColor").toString():"");
						bean.setOpening(row.get("category")!=null?Long.parseLong(row.get("category").toString()):0);
						bean.setUnitPrice(row.get("cummulativePrice")!=null?Double.parseDouble(row.get("cummulativePrice").toString()):0);
						saleBeanList.add(bean);
						
					}
				}
			}else{
				List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY2);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						SaleBean bean = new SaleBean();
						bean.setInvoiceId(row.get("company")!=null?Long.parseLong(row.get("company").toString()):0);
						bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
						bean.setQuantity(row.get("quantity")!=null?row.get("quantity").toString():"");
						bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
						bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
						bean.setColor(row.get("color")!=null?row.get("color").toString():"");
						bean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
						bean.setCaseQty(row.get("caseQty")!=null?Long.parseLong(row.get("caseQty").toString()):0);
						bean.setPackQty(row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0);
						bean.setClosing(row.get("closingCase")!=null?Long.parseLong(row.get("closingCase").toString()):0);
						bean.setCummulativePrice(row.get("closing")!=null?Double.parseDouble(row.get("closing").toString()):0);//closing value
						bean.setNoOfReturnsBtl(row.get("Btls")!=null?Long.parseLong(row.get("Btls").toString()):0);// closing in btls
						bean.setSaleId(row.get("target")!=null?Long.parseLong(row.get("target").toString()):0);
						bean.setReceived(row.get("days")!=null?Long.parseLong(row.get("days").toString()):0);
						bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
						bean.setCategoryOrder(row.get("categoryOrder")!=null?Long.parseLong(row.get("categoryOrder").toString()):0);
						bean.setSalePrimaryKey(row.get("categoryColor")!=null?row.get("categoryColor").toString():"");
						bean.setOpening(row.get("category")!=null?Long.parseLong(row.get("category").toString()):0);
						bean.setUnitPrice(row.get("cummulativePrice")!=null?Double.parseDouble(row.get("cummulativePrice").toString()):0);
						saleBeanList.add(bean);
						
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return saleBeanList;
		}
		return saleBeanList;
	}

	@Override
	public List<SaleBean> downloadIndent() {
		List<SaleBean> saleBeanList = new ArrayList<SaleBean>();
		String QUERY="SELECT p.productId,p.brandNoPackQty,p.brandNo,p.`shortBrandName`,p.productType,p.packQty,p.`quantity`,"
				+ "(SELECT t.categoryName FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryName,"
				+ "(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,"
				+ "(SELECT t.color FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS color,"
				+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyorder,"
				+ "SUM(s.`sale`) AS sale,(SELECT closing FROM sale s1 WHERE s1.`brandNoPackQty`=p.brandNoPackQty AND s1.saleDate =(SELECT `saleDate` FROM `sale` ORDER BY `saleDate` DESC LIMIT 1)) AS closing,"
				+ "ROUND((SUM(s.`sale`)/14)/p.`packQty`,2) AS perdaystock,s.`saleDate`,s.totalPrice,TRUNCATE((SELECT closing FROM sale s1"
				+ " WHERE s1.`brandNoPackQty`=p.brandNoPackQty AND s1.saleDate =(SELECT `saleDate` FROM `sale` ORDER BY `saleDate` DESC LIMIT 1))/(TRUNCATE(SUM(s.`sale`)/14,2)),0) AS noOfDays"
				+ " FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` "
				+ "WHERE s.saleDate >=(SELECT (SELECT `saleDate` FROM `sale` ORDER BY `saleDate` DESC LIMIT 1) -INTERVAL 14 DAY) AND s.saleDate <=(SELECT `saleDate` FROM `sale` ORDER BY `saleDate` DESC LIMIT 1) GROUP BY `brandNoPackQty` ORDER BY p.brandNo";
	System.out.println(QUERY);
		try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						SaleBean bean = new SaleBean();
						bean.setBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0);
						bean.setBrandNo(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0);
						bean.setProductType(row.get("productType")!=null?row.get("productType").toString():"");
						bean.setPackQty(row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0);
						bean.setQuantity(row.get("quantity")!=null?row.get("quantity").toString():"");
						bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
						bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
						bean.setColor(row.get("color")!=null?row.get("color").toString():"");
						bean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
						bean.setClosing(row.get("closing")!=null?Long.parseLong(row.get("closing").toString()):0);
						bean.setSingleBottelPrice(row.get("perdaystock")!=null?Double.parseDouble(row.get("perdaystock").toString()):0);
						bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
						bean.setInvoiceId(row.get("noOfDays")!=null?Long.parseLong(row.get("noOfDays").toString()):0);
						bean.setTotalSale(row.get("sale")!=null?Long.parseLong(row.get("sale").toString()):0);
						saleBeanList.add(bean);
						
					}
				}
		}catch(Exception e){
			e.printStackTrace();
			return saleBeanList;
		}
		return saleBeanList;
	}
	/*this  method is used to get opening and received to closing*/
	@Override
	public SaleListBean getSaleDetailsForClosing() {
		String saleDate = null;
		try{
			saleDate = jdbcTemplate.queryForObject("SELECT `saleDate` FROM sale WHERE store_id = "+userSession.getStoreId()+" ORDER BY `saleDate` DESC LIMIT 1", String.class);
		}catch(Exception e){e.printStackTrace();}
		
		SaleListBean saleListBean = new SaleListBean();
		List<SaleBean> saleList = new ArrayList<SaleBean>();
		String QUERY="SELECT (SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.store_id = "+userSession.getStoreId()+" ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1) AS `unitPrice`,"
				+ "(SELECT t.categoryOrder FROM category_tab t WHERE t.categoryId = p.category) AS categoryOrder,p.brandNoPackQty,p.brandNo,"
				+ "p.`shortBrandName`,p.productType,p.quantity,p.packQty,s.sale,s.closing,s.totalPrice,IFNULL(s.saleDate,'"+saleDate+"') AS saleDate,"
				+ "(COALESCE(s.closing,0)+(SELECT COALESCE(SUM(i.receivedBottles),0) FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty "
				+ "AND i.invoiceDate=((SELECT '"+saleDate+"' +INTERVAL 1 DAY)) AND i.store_id = "+userSession.getStoreId()+" )) AS closingandreceived,"
				+ "(SELECT COALESCE(SUM(i.receivedBottles),0) FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty "
				+ "AND i.invoiceDate=((SELECT '"+saleDate+"' +INTERVAL 1 DAY)) AND i.store_id = "+userSession.getStoreId()+") received, "
				+ "s.closing,p.packType FROM product p LEFT JOIN sale s ON p.brandNoPackQty=s.brandNoPackQty AND s.store_id = "+userSession.getStoreId()+" HAVING `saleDate`='"+saleDate+"' AND closingandreceived > 0 ORDER BY p.saleSheetOrder ASC, p.`shortBrandName`,p.`packType` DESC";
		//System.out.println(QUERY);
		try{
			long conditionVal = 0;
			long countVal = 0;
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						SaleBean bean = new SaleBean();
						bean.setUnitPrice(row.get("unitPrice")!=null?Double.parseDouble(row.get("unitPrice").toString()):0);
						bean.setBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0);
						bean.setBrandNo(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0);
						bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
						bean.setProductType(row.get("productType")!=null?row.get("productType").toString():"");
						bean.setQuantity(row.get("quantity")!=null?row.get("quantity").toString():"");
						bean.setPackQty(row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0);
						bean.setSaleDate(row.get("saleDate")!=null?row.get("saleDate").toString():"");
						bean.setClosing(row.get("closing")!=null?Long.parseLong(row.get("closing").toString()):0);
						bean.setReceived(row.get("received")!=null?Long.parseLong(row.get("received").toString()):0);
						bean.setOpening(row.get("closing")!=null?Long.parseLong(row.get("closing").toString()):0);
						bean.setPackType(row.get("packType")!=null?row.get("packType").toString():"");
						bean.setCategoryOrder(row.get("categoryOrder")!=null?Long.parseLong(row.get("categoryOrder").toString()):0);
						bean.setNoOfReturnsBtl(countVal);
						saleList.add(bean);
					}
				}
				saleListBean.setSaleBean(saleList);
				List<Map<String, Object>> dateRows = jdbcTemplate.queryForList("SELECT UPPER(DATE_FORMAT((SELECT `saleDate` FROM `sale` ORDER BY `saleDate` DESC LIMIT 1) +INTERVAL 1 DAY, '%Y-%m-%d')) AS date");
				if(dateRows != null && dateRows.size()>0) {
					for(Map daterow : dateRows){
						saleListBean.setDate(daterow.get("date")!=null?daterow.get("date").toString():"");
					   }
					}
		}catch(Exception e){
			e.printStackTrace();
			return saleListBean;
		}
		return saleListBean;}

	private Long getCountOfBrand(long brandNo, String saleDate) {
	String QUERY="SELECT (SELECT t.categoryOrder FROM category_tab t WHERE t.categoryId = p.category) AS categoryOrder,p.brandNo,"
			+ "IFNULL(s.saleDate,'"+saleDate+"') AS saleDate,"
			+ "(s.closing+(SELECT COALESCE(SUM(i.receivedBottles),0) FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty "
			+ " AND i.invoiceDate=((SELECT '"+saleDate+"' +INTERVAL 1 DAY)))) AS closingandreceived "
			+ "FROM product p LEFT JOIN sale s ON p.brandNoPackQty=s.brandNoPackQty HAVING `saleDate`='"+saleDate+"' "
			+ " AND closingandreceived > 0 AND p.`brandNo`="+brandNo+"";
	List<Map<String, Object>> dateRows = jdbcTemplate.queryForList(QUERY);
       return (long) dateRows.size();
    }
	/**
	  * This method is used to get vendor discount for selected months and company
	  * */
	@Override
	public List<VendorAdjustmentGetArrearsBean> getVendorDiscount(String date, int companyId) {
		int storeId=userSession.getStoreId();
		List<VendorAdjustmentGetArrearsBean> listdata = new ArrayList<VendorAdjustmentGetArrearsBean>();
		String QUERY ="SELECT adj_amount,vendor_month,vendor_comment, DATE_FORMAT(vendor_month, '%b-%Y') AS date FROM vendor_adjustment "
				+ "WHERE company="+companyId+" AND adj_amount !=0 AND store_id="+storeId+" ORDER BY vendor_month DESC";
		try{
	    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						VendorAdjustmentGetArrearsBean bean = new VendorAdjustmentGetArrearsBean();
						bean.setVendorAmt(row.get("adj_amount")!=null?Double.parseDouble(row.get("adj_amount").toString()):0);
						bean.setComment(row.get("vendor_comment")!=null?row.get("vendor_comment").toString():"");
						bean.setDate(row.get("date")!=null?row.get("date").toString():"");
						listdata.add(bean);
					}
				}
		}catch(Exception e){
			e.printStackTrace();
			return listdata;
		}
		
		return listdata;
	}
  /**
   * This method is used to post sale detail for a single day by Mobile UI
   * */
	@Override
	public String saveSaleDetails(JSONArray saleDetailsJson) {
		log.info("PoizonMobileUIDaoImpl.saveSaleDetails saleDetailsJson= " + saleDetailsJson);
		String result = null;
		String QUERY2="insert into sale (salePrimaryKey,brandNoPackQty,opening,received,sale,closing,unitPrice,totalPrice,saleDate,totalReturnBottle) values(?,?,?,?,?,?,?,?,?,?)";
		synchronized (this) {
			try {
				for (int i = 0; i < saleDetailsJson.length(); ++i) {
					JSONObject rec = saleDetailsJson.getJSONObject(i);
					int brandNoPackQty = rec.getInt("brandNoPackQty");
					String packQtyWithBrandNo = String.valueOf(brandNoPackQty);
					String salePrimaryKey = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
					
					
					int val=jdbcTemplate.update(QUERY2,new Object[]{packQtyWithBrandNo.concat(salePrimaryKey),
							rec.getDouble("brandNoPackQty"),rec.getInt("opening"),rec.getInt("received"),
							rec.getInt("totalSale"),rec.getInt("closing"),rec.getDouble("unitPrice"),
							rec.getDouble("totalPrice"),rec.getString("saleDate"),rec.getInt("noOfReturnsBtl")});
					if(rec.getInt("noOfReturnsBtl") > 0 && val > 0){
						Double amount=(rec.getInt("returnbtl"))*(rec.getDouble("unitPrice"));
						jdbcTemplate.update("insert into return_tab (brandNoPackQty,noOfReturBottle,pricePerBottle,returnAmount,returnDate) values(?,?,?,?,?)",
								new Object[]{rec.getDouble("brandNoPackQty"),rec.getInt("noOfReturnsBtl"),rec.getDouble("unitPrice"),amount,rec.getString("saleDate")});
						
					}
					result="Sales Updated successfully";
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				result="Something Went Wrong.";
				log.log(Level.WARN, "saveSaleDetails in PoizonMobileDaoImpl exception", e);
				e.printStackTrace();
			}
		}
		return result;
	}

@Override
public SaleListBean getSaleForClosingByNewLogic() {
	System.out.println("Shiva");
	String saleDate = null;
	try{
		saleDate = jdbcTemplate.queryForObject("SELECT `saleDate` FROM sale ORDER BY `saleId` DESC LIMIT 1", String.class);
	}catch(Exception e){e.printStackTrace();}
	
	SaleListBean saleListBean = new SaleListBean();
	List<SaleBean> saleList = new ArrayList<SaleBean>();
	String QUERY="SELECT (SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty ORDER BY `invoiceId` DESC LIMIT 1) AS `unitPrice`,"
			+ "(SELECT t.categoryOrder FROM category_tab t WHERE t.categoryId = p.category) AS categoryOrder,p.brandNoPackQty,p.brandNo,"
			+ "p.`shortBrandName`,p.productType,p.quantity,p.packQty,s.sale,s.closing,s.totalPrice,IFNULL(s.saleDate,'"+saleDate+"') AS saleDate,"
			+ "(COALESCE(s.closing,0)+(SELECT COALESCE(SUM(i.receivedBottles),0) FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty "
			+ "AND i.invoiceDate=((SELECT '"+saleDate+"' +INTERVAL 1 DAY)))) AS closingandreceived,"
			+ "(SELECT COALESCE(SUM(i.receivedBottles),0) FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty "
			+ "AND i.invoiceDate=((SELECT '"+saleDate+"' +INTERVAL 1 DAY))) received, "
			+ "s.closing,p.packType FROM product p LEFT JOIN sale s ON p.brandNoPackQty=s.brandNoPackQty HAVING `saleDate`='"+saleDate+"' ORDER BY p.saleSheetOrder ASC, p.`shortBrandName`,p.`packType` DESC";
	
	try{
		List<Map<String, Object>> dateRows = jdbcTemplate.queryForList("SELECT UPPER(DATE_FORMAT((SELECT `saleDate` FROM `sale` ORDER BY `saleDate` DESC LIMIT 1) +INTERVAL 1 DAY, '%Y-%m-%d')) AS date");
		if(dateRows != null && dateRows.size()>0) {
			for(Map daterow : dateRows){
				saleListBean.setDate(daterow.get("date")!=null?daterow.get("date").toString():"");
			   }
			}
    	 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					SaleBean bean = new SaleBean();
					bean.setUnitPrice(row.get("unitPrice")!=null?Double.parseDouble(row.get("unitPrice").toString()):0);
					bean.setBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0);
					bean.setBrandNo(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0);
					bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
					bean.setProductType(row.get("productType")!=null?row.get("productType").toString():"");
					bean.setQuantity(row.get("quantity")!=null?row.get("quantity").toString():"");
					bean.setPackQty(row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0);
					bean.setSaleDate(saleListBean.getDate());
					bean.setClosing((row.get("closing")!=null?Long.parseLong(row.get("closing").toString()):0) + (row.get("received")!=null?Long.parseLong(row.get("received").toString()):0));
					bean.setReceived(row.get("received")!=null?Long.parseLong(row.get("received").toString()):0);
					bean.setOpening(row.get("closing")!=null?Long.parseLong(row.get("closing").toString()):0);
					bean.setPackType(row.get("packType")!=null?row.get("packType").toString():"");
					bean.setCategoryOrder(row.get("categoryOrder")!=null?Long.parseLong(row.get("categoryOrder").toString()):0);
					bean.setTotalSale((long) 0);
					bean.setNoOfReturnsBtl((long) 0);
					bean.setTotalPrice((double)0);
					saleList.add(bean);
				}
			}
			saleListBean.setSaleBean(saleList);
	}catch(Exception e){
		e.printStackTrace();
		return saleListBean;
	}
	return saleListBean;
}

@Override
public List<CreditChildBean> getVendorCreditListWithRangeWise(String startMonth, String endMonth, int company) {
	
	int storeId=userSession.getStoreId();
	
    String QUERY = "SELECT c.creditId,c.brandNoPackQty,c.bottles,c.price,c.totalPrice,c.totalDiscount,c.discount,c.afterIncludingDiscount,c.cases,DATE_FORMAT(date,'%d-%b-%Y') AS date,name,"
    		+ " (SELECT p.shortBrandName FROM product p WHERE p.brandNoPackQty=c.brandNoPackQty ) AS shortBrandName FROM credit_tab c WHERE c.company = ? "
    		+ "  AND DATE >= ? AND DATE <= LAST_DAY(?) AND c.store_id="+storeId+" ORDER BY DATE DESC";
    List<CreditChildBean> listData = new ArrayList<CreditChildBean>();
    try{
    	List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY,new Object[]{company,startMonth,endMonth});
    	if(rows != null && rows.size()>0){
    		for(Map row : rows){
    			CreditChildBean bean = new CreditChildBean();
    			bean.setAfterIncludingDiscount(row.get("afterIncludingDiscount")!=null?Double.parseDouble(row.get("afterIncludingDiscount").toString()):0);
    			bean.setBottles(row.get("bottles")!=null?Long.parseLong(row.get("bottles").toString()):0);
    			bean.setBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0);
    			bean.setCreditId(row.get("creditId")!=null?Long.parseLong(row.get("creditId").toString()):0);
    			bean.setDiscount(row.get("discount")!=null?Double.parseDouble(row.get("discount").toString()):0);
    			bean.setPrice(row.get("price")!=null?Double.parseDouble(row.get("price").toString()):0);
    			bean.setTotalPrice(row.get("totalPrice")!=null?Double.parseDouble(row.get("totalPrice").toString()):0);
    			bean.setProductName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
    			bean.setCases(row.get("cases")!=null?Long.parseLong(row.get("cases").toString()):0);
    			bean.setTotalDiscount(row.get("totalDiscount")!=null?Double.parseDouble(row.get("totalDiscount").toString()):0);
    			bean.setName(row.get("name")!=null?row.get("name").toString():"");
    			bean.setDate(row.get("date")!=null?row.get("date").toString():"");
    			listData.add(bean);
    		}
    	}
    	
    }catch(Exception e){
    	e.printStackTrace();
    	return listData;
    }
    return listData;
	}

@Override
public CreditMasterBean getTotalCreditAmountAndReceived(String startMonth, String endMonth, int company) {
	
String QUERY = "SELECT SUM(totalAmount) AS totalAmount, SUM(received) AS received FROM `credit_master` WHERE masterDate >= ? "
		+ " AND masterDate <= LAST_DAY(?) AND store_id="+userSession.getStoreId()+" AND company = ?";
CreditMasterBean bean = new CreditMasterBean();
try{
	List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY,new Object[]{startMonth,endMonth,company});
	if(rows != null && rows.size()>0){
		for(Map row : rows){
			bean.setTotalAmount(row.get("totalAmount")!=null?Double.parseDouble(row.get("totalAmount").toString()):0);
			bean.setReceived(row.get("received")!=null?Double.parseDouble(row.get("received").toString()):0);
		}
	}
	
}catch(Exception e){
	e.printStackTrace();
	return bean;
}
	return bean;
}

@Override
public List<CreditChildBean> getVendorCreditListToTillMonth(String date, int company) {

	String QUERY = "SELECT c.creditId,c.brandNoPackQty,c.bottles,c.price,c.totalPrice,c.totalDiscount,c.discount,c.afterIncludingDiscount,c.cases,DATE_FORMAT(c.date,'%d-%b-%Y') AS date,c.name, "
			+ "(SELECT p.shortBrandName FROM product p WHERE p.brandNoPackQty=c.brandNoPackQty ) AS shortBrandName FROM credit_tab c WHERE c.company = ? "
			+ "   AND c.date <= LAST_DAY(?) AND c.store_id = ? ORDER BY c.date DESC";
	 List<CreditChildBean> listData = new ArrayList<CreditChildBean>();
	    try{
	    	List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY,new Object[]{company,date,userSession.getStoreId()});
	    	if(rows != null && rows.size()>0){
	    		for(Map row : rows){
	    			CreditChildBean bean = new CreditChildBean();
	    			bean.setAfterIncludingDiscount(row.get("afterIncludingDiscount")!=null?Double.parseDouble(row.get("afterIncludingDiscount").toString()):0);
	    			bean.setBottles(row.get("bottles")!=null?Long.parseLong(row.get("bottles").toString()):0);
	    			bean.setBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0);
	    			bean.setCreditId(row.get("creditId")!=null?Long.parseLong(row.get("creditId").toString()):0);
	    			bean.setDiscount(row.get("discount")!=null?Double.parseDouble(row.get("discount").toString()):0);
	    			bean.setPrice(row.get("price")!=null?Double.parseDouble(row.get("price").toString()):0);
	    			bean.setTotalPrice(row.get("totalPrice")!=null?Double.parseDouble(row.get("totalPrice").toString()):0);
	    			bean.setProductName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
	    			bean.setCases(row.get("cases")!=null?Long.parseLong(row.get("cases").toString()):0);
	    			bean.setTotalDiscount(row.get("totalDiscount")!=null?Double.parseDouble(row.get("totalDiscount").toString()):0);
	    			bean.setName(row.get("name")!=null?row.get("name").toString():"");
	    			bean.setDate(row.get("date")!=null?row.get("date").toString():"");
	    			listData.add(bean);
	    		}
	    	}
	    	
	    }catch(Exception e){
	    	e.printStackTrace();
	    	return listData;
	    }
	    return listData;
}

@Override
public CreditMasterBean getTotalCreditAmountAndReceivedTillMonth(String date, int company) {
	String QUERY = "SELECT SUM(totalAmount) AS totalAmount, SUM(received) AS received FROM `credit_master` WHERE  "
			+ " masterDate <= LAST_DAY(?)  AND company = ? AND store_id = ?";
	CreditMasterBean bean = new CreditMasterBean();
	try{
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY,new Object[]{date,company,userSession.getStoreId()});
		if(rows != null && rows.size()>0){
			for(Map row : rows){
				bean.setTotalAmount(row.get("totalAmount")!=null?Double.parseDouble(row.get("totalAmount").toString()):0);
				bean.setReceived(row.get("received")!=null?Double.parseDouble(row.get("received").toString()):0);
			}
		}
		
	}catch(Exception e){
		e.printStackTrace();
		return bean;
	}
		return bean;
}

}