package com.zambient.poizon.dao;

import java.util.List;

import org.json.JSONArray;

import com.zambient.poizon.bean.BalanceSheetWithBreckage;
import com.zambient.poizon.bean.CreditChildBean;
import com.zambient.poizon.bean.CreditMasterBean;
import com.zambient.poizon.bean.MobileViewInHouseStock;
import com.zambient.poizon.bean.MobileViewStockLiftingDiscount;
import com.zambient.poizon.bean.SaleAndAmmountBean;
import com.zambient.poizon.bean.SaleBean;
import com.zambient.poizon.bean.SaleListBean;
import com.zambient.poizon.bean.VendorAdjustmentGetArrearsBean;

public interface PoizonMobileUIDao {
	public List<MobileViewInHouseStock> mobileViewInHouseStock();
	public double InHouseStockAmount();
	public List<SaleBean> InHouseStockAmountWithSale();
	public BalanceSheetWithBreckage dayWiseBalanceSheet(String sdate, String edate);
	public SaleAndAmmountBean getSaleReports(String newSdate,String newEdate);
	public List<MobileViewStockLiftingDiscount> stockLiftingWithDiscountReport();
	public List<SaleBean> getStockLiftingReports(String date);
	public List<SaleBean> downloadIndent();
	public SaleListBean getSaleDetailsForClosing();
	public List<SaleBean> getTillMonthDiscountDetails(String date,int companyId);
	public List<VendorAdjustmentGetArrearsBean> getVendorDiscount(String date,int companyId);
	public String saveSaleDetails(JSONArray saleDetailsJson);
	public SaleListBean getSaleForClosingByNewLogic();
	public List<CreditChildBean> getVendorCreditListWithRangeWise(String startMonth,String endMonth,int company);
	public CreditMasterBean getTotalCreditAmountAndReceived(String startMonth,String endMonth,int company);
	public List<CreditChildBean> getVendorCreditListToTillMonth(String date,int company);
	public CreditMasterBean getTotalCreditAmountAndReceivedTillMonth(String date, int company);

}
