package com.zambient.poizon.dao;

import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.zambient.poizon.bean.AddNewBrandBean;
import com.zambient.poizon.bean.BalanceSheetBean;
import com.zambient.poizon.bean.BalanceSheetWithBreckage;
import com.zambient.poizon.bean.CardCashSaleBeen;
import com.zambient.poizon.bean.ChartDataBean;
import com.zambient.poizon.bean.CreditChildBean;
import com.zambient.poizon.bean.CreditMasterBean;
import com.zambient.poizon.bean.CreditPaymentBean;
import com.zambient.poizon.bean.DateAndValueBean;
import com.zambient.poizon.bean.DiscountEstimationBean;
import com.zambient.poizon.bean.DiscountEstimationWithDate;
import com.zambient.poizon.bean.DropDownsBean;
import com.zambient.poizon.bean.ExpenseBean;
import com.zambient.poizon.bean.ExpenseCategoryAndDate;
import com.zambient.poizon.bean.ExpenseCategoryBean;
import com.zambient.poizon.bean.InvoiceDateBean;
import com.zambient.poizon.bean.InvoiceReportWithMrpRoundOffBean;
import com.zambient.poizon.bean.MessageBean;
import com.zambient.poizon.bean.MrpRoundOffBean;
import com.zambient.poizon.bean.NewSaleBean;
import com.zambient.poizon.bean.NoDiscountProductsBean;
import com.zambient.poizon.bean.PendingLiftedCaseDiscountBean;
import com.zambient.poizon.bean.SaleAndAmmountBean;
import com.zambient.poizon.bean.SaleBean;
import com.zambient.poizon.bean.SaleBeanAndCashCardExpensesBean;
import com.zambient.poizon.bean.SchemeEstimateBean;
import com.zambient.poizon.bean.StockInBean;
import com.zambient.poizon.bean.TotalPriceDateWiseBean;
import com.zambient.poizon.bean.TurnovertaxBean;
import com.zambient.poizon.bean.UpdateExpenseAndMasterExpense;
import com.zambient.poizon.bean.UpdateExpensesBean;
import com.zambient.poizon.bean.category;
import com.zambient.poizon.bean.data;
import com.zambient.poizon.utill.CommonUtil;
import com.zambient.poizon.utill.SaleDateUtil;
import com.zambient.poizon.utill.UserSession;

@PropertySource("classpath:/com/zambient/poizon/resources/poizon.properties")
@Repository("poizonDao")
public class PoizonDaoImpl implements PoizonDao{
	
	final static Logger log = Logger.getLogger(PoizonDaoImpl.class);
    @Autowired
	private JdbcTemplate jdbcTemplate;
    
    @Autowired 
	private UserSession userSession;
    
    @Value("${discountDate.1}")
   	private String discountDate1;
       
    @Value("${discountDate.2}")
   	private String discountDate2;
       
    @Value("${discountDate.4}")
   	private String discountDate4;
       
    @Value("${discountDate.5}")
    private String discountDate5;
    
    @Value("${discountDate.6}")
   	private String discountDate6;
    
 
     
    /** 
	 * This method is used to add new product to product tab including all details of that product.
	 * And this will return String message as Success or failure.
	 * Here first before persist the object we are checking is it already available or if its available then returning its already exit otherwise save in tab.  
	 * */
	@Override
	public String addNewBrandName(AddNewBrandBean addNewBrandBean) {
		log.info("PoizonDaoImpl.addNewBrandName Bean>> "+addNewBrandBean);
		 String result = null;
		 double brandNumAndPackQty = Double.parseDouble(Long.toString(addNewBrandBean.getBrandNo()).concat(Long.toString(addNewBrandBean.getPackQty())));
		 String QUER2="select brandNoPackQty from product where brandNoPackQty = "+brandNumAndPackQty;
		 String QUERY="insert into product(brandNoPackQty,brandNo,brandName,productType,quantity,packQty,packType,category,company,active,matchs,shortBrandName,specialMargin,matchName,realBrandNo,productGrouping) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUER2);
			if(rows != null && rows.size()>0){
				result="Product Allready Existed.";
			}else{
				int val=jdbcTemplate.update(QUERY,new Object[]{brandNumAndPackQty,addNewBrandBean.getBrandNo(),addNewBrandBean.getBrandName(),addNewBrandBean.getProductType(),addNewBrandBean.getQuantity(),addNewBrandBean.getPackQty(),addNewBrandBean.getPackType(),addNewBrandBean.getCategory(),addNewBrandBean.getCompany(),1,addNewBrandBean.getMatch(),addNewBrandBean.getShortBrandName(),0,addNewBrandBean.getMatchName(),addNewBrandBean.getRealBrandNo(),addNewBrandBean.getGroup()});
				if(val>0)
					result="Product inserted successfully";
			}
		}catch(Exception e){
			result="something went wrong";
			log.log(Level.WARN, "addNewBrandName in PoizonDaoImpl exception", e);
			e.printStackTrace();
			}
		
		return result;
	}
	/**
	 * This method is used to get all product list with details from product table.
	 * */
	@Override
	public List<AddNewBrandBean> getBrandDetails() {
		log.info("PoizonDaoImpl.getBrandDetails");
		//String Query="select productId,brandNoPackQty,brandNo,brandName,productType,quantity,packQty,packType from product";
		String Query="SELECT DISTINCT p.`productId`, p.brandNoPackQty, p.brandNo,p.brandName,p.quantity,p.category,p.productType,p.packQty,p.packType,p.matchs,(SELECT ct.categoryName FROM `category_tab` ct WHERE p.category=ct.categoryId)AS categoryname,(SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty ORDER BY `invoiceId` DESC LIMIT 1) AS unitPrice FROM product p";
		List<AddNewBrandBean> addNewBrandList = new ArrayList<AddNewBrandBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(Query);
			//System.out.println("row size>> "+rows.size());
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					AddNewBrandBean addNewBrandBean = new AddNewBrandBean();
					addNewBrandBean.setProductId(Long.parseLong(row.get("productId").toString()));
					addNewBrandBean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
					addNewBrandBean.setBrandNo(Long.parseLong(row.get("brandNo").toString()));
					addNewBrandBean.setBrandName(row.get("brandName").toString());
					addNewBrandBean.setProductType(row.get("productType").toString());
					addNewBrandBean.setQuantity(row.get("quantity").toString());
					addNewBrandBean.setPackQty(Long.parseLong(row.get("packQty").toString()));
					addNewBrandBean.setPackType(row.get("packType").toString());
					addNewBrandBean.setMatch(row.get("matchs")!=null?Long.parseLong(row.get("matchs").toString()):0);
					//addNewBrandBean.setCategory(row.get("categoryname").toString());
					addNewBrandBean.setUnitPrice(row.get("unitPrice")!=null?Double.parseDouble(row.get("unitPrice").toString()):0);
					addNewBrandList.add(addNewBrandBean);
				}
			}
		}catch(Exception e){
			log.log(Level.WARN, "getBrandDetails in PoizonDaoImpl exception", e);
			e.printStackTrace();
			return addNewBrandList;
		}finally {
			
		}
		return addNewBrandList;
	}
	/**
	 * This method is used to get single product with details from product table.
	 * And returns that product in json format.
	 * */

	@Override
	public List<AddNewBrandBean> getSingleBrandDetail(String brandNoAndPackQty) {
		log.info("PoizonDaoImpl.getSingleBrandDetail brandNoAndPackQty= "+brandNoAndPackQty);
		String QUERY=null;
		List<Map<String, Object>> brandNoAndPackQtyrows = jdbcTemplate.queryForList("SELECT brandNoPackQty FROM sale WHERE brandNoPackQty="+brandNoAndPackQty+" AND store_id ="+userSession.getStoreId());
		if(brandNoAndPackQtyrows != null && brandNoAndPackQtyrows.size()>0) {
		 //QUERY="SELECT DISTINCT p.productId,p.brandNoPackQty,p.brandNo,p.brandName,p.productType,p.quantity,p.packQty,p.packType,MAX(i.SingleBottelRate) AS SingleBottelRate, MAX(i.packQtyRate) AS packQtyRate,MAX(s.unitPrice) AS unitPrice,MAX(s.saleId) AS saleId FROM product p,invoice i,sale s WHERE p.brandNoPackQty = "+brandNoAndPackQty+" AND i.brandNoPackQty = "+brandNoAndPackQty+" AND s.brandNoPackQty ="+brandNoAndPackQty;
			QUERY="SELECT DISTINCT p.productId,p.brandNoPackQty,p.brandNo,p.realBrandNo,p.brandName,p.productType,p.quantity,p.packQty,p.packType,(SELECT i.SingleBottelRate FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.store_id ="+userSession.getStoreId()+" ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1) AS SingleBottelRate,"
					+ " (SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.store_id ="+userSession.getStoreId()+" ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1) AS packQtyRate,(SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.store_id ="+userSession.getStoreId()+" ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1) AS bottleSaleMrp,(SELECT i.specialMargin FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.store_id ="+userSession.getStoreId()+" ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1) AS specialMargin,"
					+ "MAX(s.saleId) AS saleId FROM product p,invoice i,sale s WHERE p.brandNoPackQty = "+brandNoAndPackQty+" AND i.brandNoPackQty = "+brandNoAndPackQty+" AND s.brandNoPackQty ="+brandNoAndPackQty+" AND i.store_id ="+userSession.getStoreId()+" AND s.store_id ="+userSession.getStoreId()+"";
		}
		else{
			QUERY="select productId,brandNoPackQty,brandNo,realBrandNo,brandName,productType,quantity,packQty,packType from product where brandNoPackQty="+brandNoAndPackQty;
		}
		List<AddNewBrandBean> addNewBrandList = new ArrayList<AddNewBrandBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			System.out.println("row size>> "+rows.size());
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					AddNewBrandBean addNewBrandBean = new AddNewBrandBean();
					addNewBrandBean.setProductId(Long.parseLong(row.get("productId").toString()));
					addNewBrandBean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
					addNewBrandBean.setBrandNo(Long.parseLong(row.get("brandNo").toString()));
					addNewBrandBean.setRealBrandNo(Long.parseLong(row.get("realBrandNo").toString()));
					addNewBrandBean.setBrandName(row.get("brandName").toString());
					addNewBrandBean.setProductType(row.get("productType").toString());
					addNewBrandBean.setQuantity(row.get("quantity").toString());
					addNewBrandBean.setPackQty(Long.parseLong(row.get("packQty").toString()));
					addNewBrandBean.setPackType(row.get("packType").toString());
					addNewBrandBean.setSingleBottelRate(row.get("SingleBottelRate")!= null?Double.parseDouble(row.get("SingleBottelRate").toString()):0);
					addNewBrandBean.setPackQtyRate(row.get("packQtyRate")!= null?Double.parseDouble(row.get("packQtyRate").toString()):0);
					addNewBrandBean.setUnitPrice(row.get("bottleSaleMrp")!= null?Double.parseDouble(row.get("bottleSaleMrp").toString()):0);
					addNewBrandBean.setSaleId(row.get("saleId")!=null?Long.parseLong(row.get("saleId").toString()):-1);
					addNewBrandBean.setSpecialMargin(row.get("specialMargin")!=null?Double.parseDouble(row.get("specialMargin").toString()):0);
					addNewBrandList.add(addNewBrandBean);
				}
			}
		}catch(Exception e){
			log.log(Level.WARN, "getSingleBrandDetail in PoizonDaoImpl exception", e);
			e.printStackTrace();
			return addNewBrandList;
		}finally {
			
		}
		return addNewBrandList;

	}
	/**
	 * This method is used to authenticateAdmin admin.
	 * */
	@Override
	public String authenticateAdmin() {
		log.info("PoizonDaoImpl.authenticateAdmin");
		String result="";
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT user_id FROM admin");
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					result=row.get("user_id").toString();
				}
			}
		}catch(Exception e){
			log.log(Level.WARN, "authenticateAdmin in PoizonDaoImpl exception", e);
			e.printStackTrace();
		}
		return result;
	}
	/**
	 * This method is used to get all products to update closing.
	 * product details getting from 'product' tab.
	 * And opening and received getting from 'sale' & 'invoice' table based on date selection.
	 * */
	@Override
	public SaleAndAmmountBean getSingleSaleBrandDetailForClosing() {
		log.info("PoizonDaoImpl.getSingleSaleBrandDetailForClosing");
		//String QUERY1="SELECT p.productId,(SELECT i.receivedBottles FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.invoiceDate='"+saleDate+"') AS `receivedBottles`,(SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty) AS `unitPrice`,p.brandNoPackQty,p.brandNo,p.brandName,p.productType,p.quantity,p.packQty,p.packType,s.saleId,s.salePrimaryKey,s.sale,s.closing,s.totalPrice,s.saleDate,s.opening FROM product p LEFT JOIN sale s ON p.brandNoPackQty=s.brandNoPackQty ORDER BY p.brandName ASC";
		SaleAndAmmountBean saleAndAmmountBeanList= new SaleAndAmmountBean();
		String saleDate=null;
		String dateLable=null;
		try{
			 List<Map<String, Object>> dateRow = jdbcTemplate.queryForList("SELECT `saleDate` FROM sale WHERE store_id = "+userSession.getStoreId()+" ORDER BY `saleDate` DESC LIMIT 1");
			 if(dateRow != null && dateRow.size()>0) {
					for(Map row : dateRow){
						saleDate=row.get("saleDate")!=null?row.get("saleDate").toString():"";
					}
			 }
			 if(saleDate != null && saleDate !="" ){
				 System.out.println("If Part>>> ");
			   List<SaleBean> saleBeanList = new ArrayList<SaleBean>();
			   
			   dateLable = jdbcTemplate.queryForObject("SELECT DATE_ADD('"+saleDate+"', INTERVAL 1 DAY)", String.class);
			   
				  System.out.println("previusDate>> "+dateLable);
				  log.info("Next Day date: "+dateLable);
				  List<Map<String, Object>> secRows = jdbcTemplate.queryForList("SELECT p.productId,(SELECT i.receivedBottles FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.invoiceDate='"+dateLable+"' AND i.store_id = "+userSession.getStoreId()+") AS `receivedBottles`,(SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.store_id = "+userSession.getStoreId()+" ORDER BY `invoiceId` DESC LIMIT 1) AS `unitPrice`,p.brandNoPackQty,p.brandNo,p.shortBrandName,p.productType,p.quantity,p.packQty,p.packType,s.saleId,s.salePrimaryKey,s.sale,s.closing,s.totalPrice,IFNULL(s.saleDate,'"+saleDate+"') AS saleDate,s.opening,s.received FROM product p LEFT JOIN sale s ON p.brandNoPackQty=s.brandNoPackQty AND s.store_id = "+userSession.getStoreId()+" HAVING `saleDate`='"+saleDate+"' ORDER BY p.saleSheetOrder ASC, p.`shortBrandName`,p.`packType` DESC");
					if(secRows != null && secRows.size()>0) {
						for(Map row : secRows){
							SaleBean saleBean = new SaleBean();
							saleBean.setProductId(Long.parseLong(row.get("productId").toString()));
							saleBean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
							saleBean.setReceived(row.get("receivedBottles")!=null?Long.parseLong(row.get("receivedBottles").toString()):0);
							saleBean.setBrandNo(Long.parseLong(row.get("brandNo").toString()));
							saleBean.setBrandName(row.get("shortBrandName").toString());
							saleBean.setProductType(row.get("productType").toString());
							saleBean.setQuantity(row.get("quantity").toString());
							saleBean.setPackQty(Long.parseLong(row.get("packQty").toString()));
							saleBean.setPackType(row.get("packType").toString());
							saleBean.setSaleId(row.get("saleId")!=null?Long.parseLong(row.get("saleId").toString()):-1);
							saleBean.setSalePrimaryKey(row.get("salePrimaryKey")!=null?row.get("salePrimaryKey").toString():"");
							saleBean.setTotalSale((long) 0);
							saleBean.setClosing((long) 0);
							saleBean.setUnitPrice(row.get("unitPrice")!=null?Double.parseDouble(row.get("unitPrice").toString()):0);
							saleBean.setTotalPrice((double) 0);
							saleBean.setSaleDate(dateLable);
							//long receive=row.get("received")!=null?Integer.parseInt(row.get("received").toString()):0;
							long opening=row.get("closing")!=null?Integer.parseInt(row.get("closing").toString()):0;
							saleBean.setOpening(opening);
							saleBeanList.add(saleBean);
						}
					}
			saleAndAmmountBeanList.setSaleBean(saleBeanList);
			saleAndAmmountBeanList.setExpenseDate(dateLable);
			 }else{
				 System.out.println("else Part>>>");
				 String zeroindexdate=null;
				  List<SaleBean> saleBeanList = new ArrayList<SaleBean>();
				 List<Map<String, Object>> idateRow = jdbcTemplate.queryForList("SELECT DISTINCT `invoiceDate` FROM `invoice` WHERE store_id = "+userSession.getStoreId()+" AND `invoiceDate` IS NOT NULL");
				 if(idateRow != null && idateRow.size()>0) {
						for(Map row : idateRow){
							zeroindexdate=row.get("invoiceDate")!=null?row.get("invoiceDate").toString():"";
						}
				 }
				 List<Map<String, Object>> secRows = jdbcTemplate.queryForList("SELECT p.productId,(SELECT i.receivedBottles FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.store_id = "+userSession.getStoreId()+") AS receivedBottles,"
				 		+ "(SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.store_id = "+userSession.getStoreId()+") AS unitPrice,p.brandNoPackQty,p.brandNo,p.shortBrandName,p.productType,"
				 		+ "p.quantity,p.packQty,p.packType,s.saleId,s.salePrimaryKey,s.sale,s.closing,s.totalPrice,s.opening FROM product p LEFT JOIN sale s ON p.brandNoPackQty=s.brandNoPackQty AND  s.store_id = "+userSession.getStoreId()+""
				 		+ " ORDER BY p.saleSheetOrder ASC, p.`shortBrandName`,p.`packType` DESC");
				 if(secRows != null && secRows.size()>0) {
						for(Map row : secRows){
							SaleBean saleBean = new SaleBean();
							saleBean.setProductId(Long.parseLong(row.get("productId").toString()));
							saleBean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
							saleBean.setReceived(row.get("receivedBottles")!=null?Long.parseLong(row.get("receivedBottles").toString()):0);
							saleBean.setBrandNo(Long.parseLong(row.get("brandNo").toString()));
							saleBean.setBrandName(row.get("shortBrandName").toString());
							saleBean.setProductType(row.get("productType").toString());
							saleBean.setQuantity(row.get("quantity").toString());
							saleBean.setPackQty(Long.parseLong(row.get("packQty").toString()));
							saleBean.setPackType(row.get("packType").toString());
							saleBean.setSaleId(row.get("saleId")!=null?Long.parseLong(row.get("saleId").toString()):-1);
							saleBean.setSalePrimaryKey(row.get("salePrimaryKey")!=null?row.get("salePrimaryKey").toString():"");
							saleBean.setTotalSale(row.get("sale")!=null?Long.parseLong(row.get("sale").toString()):0);
							saleBean.setClosing(row.get("closing")!=null?Long.parseLong(row.get("closing").toString()):0);
							saleBean.setUnitPrice(row.get("unitPrice")!=null?Double.parseDouble(row.get("unitPrice").toString()):0);
							saleBean.setTotalPrice(row.get("totalPrice")!=null?Double.parseDouble(row.get("totalPrice").toString()):0);
							saleBean.setSaleDate(zeroindexdate);
							saleBean.setOpening(row.get("opening")!=null?Long.parseLong(row.get("opening").toString()):0);
							saleBeanList.add(saleBean);
						}
					}
				 saleAndAmmountBeanList.setSaleBean(saleBeanList);
					saleAndAmmountBeanList.setExpenseDate(zeroindexdate);
			 }
			 
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
			return saleAndAmmountBeanList;	
	}
	/**
	 * This method is used to save sale for the day and return the message like success or failed.
	 * */
	@Override
	public String saveSingleSaleBrandDetail(JSONArray saleDetailsJson) {
		log.info("PoizonDaoImpl.saveSingleSaleBrandDetail saleDetailsJson= "+saleDetailsJson);
		String result=null;
		synchronized(this){
		try {
			for (int i = 0; i < saleDetailsJson.length(); ++i) {
			 JSONObject rec;
			    rec = saleDetailsJson.getJSONObject(i);
				Double sdate=Double.parseDouble(rec.getString("brandNoPackQty"));
				String newdate=String.valueOf(sdate.intValue());
				Date date1 = new Date();
				String startdate=rec.getString("saleDate");
				String fileName = new SimpleDateFormat("yyyyMMddhhmmss").format(date1);
				//System.out.println("FineNAme>> "+fileName);
                String QUERY2="insert into sale (salePrimaryKey,brandNoPackQty,opening,received,sale,closing,unitPrice,totalPrice,saleDate,totalReturnBottle,store_id) values(?,?,?,?,?,?,?,?,?,?,?)";
				int val=jdbcTemplate.update(QUERY2,new Object[]{newdate.concat(fileName),Double.parseDouble(rec.getString("brandNoPackQty")),Long.parseLong(rec.getString("opening")),Long.parseLong(rec.getString("received")),Long.parseLong(rec.getString("totalSale")),Long.parseLong(rec.getString("closing")),Double.parseDouble(rec.getString("unitPrice")),Double.parseDouble(rec.getString("totalPrice")),startdate,Long.parseLong(rec.getString("returnbtl")),userSession.getStoreId()});
				long returnbtlsval = Long.parseLong(rec.getString("returnbtl"));
				if(returnbtlsval > 0 && val > 0){
					Double amount=(returnbtlsval)*(Double.parseDouble(rec.getString("unitPrice")));
					jdbcTemplate.update("insert into return_tab (brandNoPackQty,noOfReturBottle,pricePerBottle,returnAmount,returnDate,store_id) values(?,?,?,?,?,?)",new Object[]{Double.parseDouble(rec.getString("brandNoPackQty")),returnbtlsval,Double.parseDouble(rec.getString("unitPrice")),amount,startdate,userSession.getStoreId()});
					
				}
				result="Sales Updated successfully";
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			result="Something Went Wrong.";
			log.log(Level.WARN, "saveSingleSaleBrandDetail in PoizonDaoImpl exception", e);
			e.printStackTrace();
		}
		}
		return result;
	}
	
	@Override
	public List<InvoiceDateBean> getTotalInvoiceDate() {
		
		String Query="SELECT DISTINCT DATE_FORMAT(invoiceDate,'%d-%m-%Y') AS invoiceDate FROM invoice WHERE store_id = "+userSession.getStoreId()+"";
		List<InvoiceDateBean> invoiceDateList = new ArrayList<InvoiceDateBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(Query);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					InvoiceDateBean invoiceDateBean = new InvoiceDateBean();
					invoiceDateBean.setDate(row.get("invoiceDate").toString());
					invoiceDateList.add(invoiceDateBean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return invoiceDateList;
		}finally {
			
		}
		return invoiceDateList;
		
	}
	/**
	 * This method is used to get distinct sale date list from 'sale' table.
	 * */
	@Override
	public List<InvoiceDateBean> getTotalSaleDate() {

		String Query="SELECT DISTINCT DATE_FORMAT(saleDate,'%d-%m-%Y') AS saleDate FROM sale WHERE store_id="+userSession.getStoreId();

		List<InvoiceDateBean> invoiceDateList = new ArrayList<InvoiceDateBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(Query);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					InvoiceDateBean invoiceDateBean = new InvoiceDateBean();
					invoiceDateBean.setDate(row.get("saleDate").toString());
					invoiceDateList.add(invoiceDateBean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return invoiceDateList;
		}finally {
			
		}
		return invoiceDateList;
	}
	/**
	 * This method is used to get single date invoice details from 'invoice' tab.
	 * */
	@Override
	public InvoiceReportWithMrpRoundOffBean getSingleInvoiceDetailDateWise(String idate) {
		log.info("PoizonDaoImpl.getSingleInvoiceDetailDateWise idate= "+idate);
		String QUERY="SELECT p.productId,p.brandNoPackQty,p.realBrandNo,p.brandName,p.productType,p.quantity,p.packQty,p.packType,"
				+ "i.`invoiceId`,i.`packQty`,i.`SingleBottelRate`,i.`packQtyRate`,i.`caseQty`,i.`QtyBottels`,i.`totalPrice`,DATE_FORMAT(i.`invoiceDate`,'%d-%m-%Y') AS invoiceDate,DATE_FORMAT(i.`invoiceDateAsPerSheet`,'%d-%m-%Y') AS invoiceDateAsPerSheet,i.specialMargin,"
				+ "(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.companyId) AS companyName,"
				+ "i.bottleSaleMrp,(SELECT t.categoryName FROM `category_tab` t WHERE p.`category`=t.categoryId) AS categoryName,"
				+ "(SELECT t.color FROM `company_tab` t WHERE p.`company`=t.companyId) AS color,"
				+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p.`company`=t.companyId) AS companyorder,"
				+ "(SELECT ROUND(SUM(`totalPrice`), 2)  FROM invoice WHERE `invoiceDate`<='"+idate+"' AND store_id = "+userSession.getStoreId()+") AS cummulativePrice,"
				+ "(SELECT ROUND(COALESCE(SUM(`mrpRoundOff`),0),2) FROM `invoice_mrp_round_off` WHERE `mrpRoundOffDate` <='"+idate+"' AND store_id = "+userSession.getStoreId()+") AS mrpRoundOff FROM product p "
			    + "INNER JOIN invoice i ON p.brandNoPackQty=i.brandNoPackQty AND invoiceDate = '"+idate+"' AND i.store_id = "+userSession.getStoreId()+"";
		///System.out.println(QUERY);
		InvoiceReportWithMrpRoundOffBean invoiceReportWithMrpRoundOffBean = new InvoiceReportWithMrpRoundOffBean();
		List<SaleBean> saleBeanList = new ArrayList<SaleBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					SaleBean saleBean = new SaleBean();
					saleBean.setProductId(Long.parseLong(row.get("productId").toString()));
					saleBean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
					saleBean.setBrandNo(Long.parseLong(row.get("realBrandNo").toString()));
					saleBean.setBrandName(row.get("brandName").toString());
					saleBean.setProductType(row.get("productType").toString());
					saleBean.setQuantity(row.get("quantity").toString());
					saleBean.setPackQty(Long.parseLong(row.get("packQty").toString()));
					saleBean.setPackType(row.get("packType").toString());
					saleBean.setInvoiceId(Long.parseLong(row.get("invoiceId").toString()));
					saleBean.setSingleBottelPrice(Double.parseDouble(row.get("SingleBottelRate").toString()));
					saleBean.setUnitPrice(Double.parseDouble(row.get("packQtyRate").toString()));
					saleBean.setCaseQty(Long.parseLong(row.get("caseQty").toString()));
					saleBean.setQtyBottels(Long.parseLong(row.get("QtyBottels").toString()));
					saleBean.setTotalPrice(Double.parseDouble(row.get("totalPrice").toString()));
					saleBean.setInvoiceDate(row.get("invoiceDate").toString());
					saleBean.setInvoiceDateAsCopy(row.get("invoiceDateAsPerSheet")!=null?row.get("invoiceDateAsPerSheet").toString():"");
					saleBean.setBottleSaleMrp(Double.parseDouble(row.get("bottleSaleMrp").toString()));
					saleBean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
					saleBean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
					saleBean.setColor(row.get("color")!=null?row.get("color").toString():"");
					saleBean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
					saleBean.setCummulativePrice(Double.parseDouble(row.get("cummulativePrice").toString())+Double.parseDouble(row.get("mrpRoundOff").toString()));
					saleBean.setCurrentMonthSold(row.get("specialMargin")!=null?Double.parseDouble(row.get("specialMargin").toString()):0);
					saleBeanList.add(saleBean);
				}
			}
			invoiceReportWithMrpRoundOffBean.setSaleBean(saleBeanList);
			MrpRoundOffBean mrpRoundOffBean = new MrpRoundOffBean();
			List<Map<String, Object>> expenseRow = jdbcTemplate.queryForList("SELECT `mrpRoundOff`,turnoverTax,tcsVal,`mrpRoundOffDate`,retailer_credit_balance FROM `invoice_mrp_round_off` WHERE `mrpRoundOffDate`='"+idate+"' AND store_id = "+userSession.getStoreId()+"");
			if(expenseRow != null && expenseRow.size()>0) {
				for(Map row : expenseRow){
					mrpRoundOffBean.setMrpRoundOff(row.get("mrpRoundOff")!=null?Double.parseDouble(row.get("mrpRoundOff").toString()):0);
					mrpRoundOffBean.setTurnOverTax(row.get("turnoverTax")!=null?Double.parseDouble(row.get("turnoverTax").toString()):0);
					mrpRoundOffBean.setTcsVal(row.get("tcsVal")!=null?Double.parseDouble(row.get("tcsVal").toString()):0);
					mrpRoundOffBean.setDate(row.get("mrpRoundOffDate")!=null?row.get("mrpRoundOffDate").toString():"");
					mrpRoundOffBean.setRetailerCreditVal(row.get("retailer_credit_balance")!=null?Double.parseDouble(row.get("retailer_credit_balance").toString()):0);
				   }
				}
			else{
				mrpRoundOffBean.setMrpRoundOff(Double.parseDouble("0"));
				mrpRoundOffBean.setTurnOverTax(Double.parseDouble("0"));
				mrpRoundOffBean.setTcsVal(Double.parseDouble("0"));
				mrpRoundOffBean.setDate("");
				mrpRoundOffBean.setRetailerCreditVal(Double.parseDouble("0"));
			}
			invoiceReportWithMrpRoundOffBean.setMrpRoundOffBean(mrpRoundOffBean);
		}catch(Exception e){
			e.printStackTrace();
			return invoiceReportWithMrpRoundOffBean;
		}finally {
			
		}
		return invoiceReportWithMrpRoundOffBean;
	}
	/**
	 * Old method not used any where
	 * */
	@Override
	public List<SaleBean> getProfitPercentage() {
		String QUERY="SELECT DISTINCT p.productId,p.brandNoPackQty,p.brandNo,p.brandName,p.productType,p.quantity,p.packQty,p.packType,i.SingleBottelRate FROM product p INNER JOIN invoice i ON p.`brandNoPackQty` = i.`brandNoPackQty`";
		List<SaleBean> saleBeanList = new ArrayList<SaleBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					SaleBean saleBean = new SaleBean();
					saleBean.setProductId(Long.parseLong(row.get("productId").toString()));
					saleBean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
					saleBean.setBrandNo(Long.parseLong(row.get("brandNo").toString()));
					saleBean.setBrandName(row.get("brandName").toString());
					saleBean.setProductType(row.get("productType").toString());
					saleBean.setQuantity(row.get("quantity").toString());
					saleBean.setPackQty(Long.parseLong(row.get("packQty").toString()));
					saleBean.setPackType(row.get("packType").toString());
					saleBean.setSingleBottelPrice(Double.parseDouble(row.get("SingleBottelRate").toString()));
					//double price=Double.parseDouble(row.get("unitPrice").toString());
						Double brandNumberPack=Double.parseDouble(row.get("brandNoPackQty").toString());
						//System.out.println("khagesh>> "+brandNumberPack.intValue());
						List<Map<String, Object>> saleprice = jdbcTemplate.queryForList("SELECT MAX(`unitPrice`)  AS unitPrice from sale WHERE brandNoPackQty= "+brandNumberPack.intValue());
						if(saleprice != null && saleprice.size()>0) {
							for(Map row1 : saleprice){
								saleBean.setUnitPrice(Double.parseDouble(row1.get("unitPrice").toString()));
							}
					
					}
					saleBeanList.add(saleBean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return saleBeanList;
		}finally {
			
		}
		
		return saleBeanList;
	}
	/**
	 * This method is used to get Lifted no. of cases and sum of mrprounding off, tcs tournover tax for the selecetd dates.
	 * */
	@Override
	public List<SaleBean> getStockLifting(String newSdate,String newEdate) {
		//String QUERY="SELECT DISTINCT p.productId, p.brandNoPackQty,p.brandNo,p.shortBrandName,p.productType,p.quantity,p.packQty,p.packType,p.category,(SELECT t.categoryName FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryName,p.company,(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,i.packQtyRate,(SELECT t.color FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS color,(SELECT t.companyOrder FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyorder,i.`bottleSaleMrp`, CEIL(SUM(i.receivedBottles)/p.packQty) AS caseQty, (SELECT GROUP_CONCAT( DISTINCT `invoiceDateAsPerSheet`) FROM invoice  WHERE `SingleBottelRate`=i.`SingleBottelRate` AND `brandNoPackQty`=i.`brandNoPackQty` AND `invoiceDateAsPerSheet`>='"+newSdate.toString()+"' AND `invoiceDateAsPerSheet`<='"+newEdate.toString()+"' AND `receivedBottles`>0) AS invoicedate, (packQtyRate*(CEIL(SUM(i.receivedBottles)/p.packQty))) total FROM product p INNER JOIN invoice i  ON p.`brandNoPackQty` = i.`brandNoPackQty` AND `invoiceDateAsPerSheet`>='"+newSdate.toString()+"' AND `invoiceDateAsPerSheet`<='"+newEdate.toString()+"'  GROUP BY brandNoPackQty,packQtyRate,SingleBottelRate ORDER BY p.company ASC";
		String QUERY="SELECT DISTINCT p.productId, p.brandNoPackQty,p.realBrandNo,p.shortBrandName,p.productType,p.quantity,p.packQty,p.packType,p.category,"
				+ "(SELECT t.categoryName FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryName,p.company,"
				+ "(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,i.packQtyRate,"
				+ "(SELECT t.color FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS color,(SELECT t.color FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryColor,(SELECT t.companyOrder FROM `company_tab` t WHERE"
				+ " p.`company`=t.`companyId`) AS companyorder,(SELECT t.categoryOrder FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryOrder,i.`bottleSaleMrp`, TRUNCATE(SUM(i.receivedBottles)/p.packQty,0) AS caseQty,"
				+ "(SUM(i.receivedBottles)/p.packQty) AS caseWithBtl,"
				+ " (SUM(i.receivedBottles)-(p.`packQty`*(TRUNCATE(SUM(i.receivedBottles)/p.`packQty`,0)))) AS btls,"
				+ "(SELECT ROUND(SUM(`totalPrice`), 0)  FROM invoice WHERE `invoiceDateAsPerSheet`>='2019-11-01' AND `invoiceDateAsPerSheet`<='"+newEdate+"' AND store_id = "+userSession.getStoreId()+") AS cummulativePrice,"
				+ " (SELECT ROUND(SUM(`tcsVal`), 0)  FROM invoice_mrp_round_off WHERE `mrpRoundOffDateAsCopy`>='2019-11-01' AND `mrpRoundOffDateAsCopy`<='"+newEdate+"' AND store_id = "+userSession.getStoreId()+") AS tcsVal,"
						+ " (SELECT ROUND(SUM(`mrpRoundOff`), 2) FROM invoice_mrp_round_off WHERE `mrpRoundOffDateAsCopy`>='"+newSdate+"' AND `mrpRoundOffDateAsCopy`<='"+newEdate+"' AND store_id = "+userSession.getStoreId()+") AS mrpRoundOff,"
						+ "(SELECT ROUND(SUM(`tcsVal`), 0) FROM invoice_mrp_round_off WHERE `mrpRoundOffDateAsCopy`>='"+newSdate+"' AND `mrpRoundOffDateAsCopy`<='"+newEdate+"' AND store_id = "+userSession.getStoreId()+") AS tcs, "
						+ " (SELECT ROUND(SUM(`turnoverTax`), 0) FROM invoice_mrp_round_off WHERE `mrpRoundOffDateAsCopy`>='"+newSdate+"' AND `mrpRoundOffDateAsCopy`<='"+newEdate+"' AND store_id = "+userSession.getStoreId()+") AS turnoverTax, "
				+ "  (SELECT GROUP_CONCAT( DISTINCT DATE_FORMAT(`invoiceDateAsPerSheet`,'%d-%m-%Y')) FROM invoice   WHERE `SingleBottelRate`=i.`SingleBottelRate` AND"
				+ " `brandNoPackQty`=i.`brandNoPackQty` AND `invoiceDateAsPerSheet`>='"+newSdate+"' AND `invoiceDateAsPerSheet`<='"+newEdate+"' AND `receivedBottles`>0 AND store_id = "+userSession.getStoreId()+")"
				+ " AS invoicedate, SUM(i.totalPrice) total   FROM product p INNER JOIN invoice i  ON p.`brandNoPackQty` = i.`brandNoPackQty`"
				+ " AND `invoiceDateAsPerSheet`>='"+newSdate+"'   AND `invoiceDateAsPerSheet`<='"+newEdate+"' AND store_id = "+userSession.getStoreId()+" GROUP BY brandNoPackQty,packQtyRate,SingleBottelRate"
				+ " ORDER BY p.company ASC";
		//System.out.println(QUERY);
		List<SaleBean> stockList = new ArrayList<SaleBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					SaleBean saleBean = new SaleBean();
					saleBean.setProductId(Long.parseLong(row.get("productId").toString()));
					saleBean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
					saleBean.setBrandNo(Long.parseLong(row.get("realBrandNo").toString()));
					saleBean.setBrandName(row.get("shortBrandName").toString());
					saleBean.setProductType(row.get("productType").toString());
					saleBean.setQuantity(row.get("quantity").toString());
					saleBean.setPackQty(Long.parseLong(row.get("packQty").toString()));
					saleBean.setPackType(row.get("packType").toString());
					saleBean.setUnitPrice(row.get("packQtyRate")!=null?Double.parseDouble(row.get("packQtyRate").toString()):0);
					saleBean.setColor(row.get("color")!=null?row.get("color").toString():"");
					saleBean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
					saleBean.setBottleSaleMrp(row.get("bottleSaleMrp")!=null?Double.parseDouble(row.get("bottleSaleMrp").toString()):0);
					saleBean.setCaseQty(row.get("caseQty")!= null?Long.parseLong(row.get("caseQty").toString()):0);
					saleBean.setQtyBottels(row.get("btls")!= null?Long.parseLong(row.get("btls").toString()):0);
					saleBean.setCummulativePrice(row.get("cummulativePrice")!=null?Double.parseDouble(row.get("cummulativePrice").toString()):0);
					saleBean.setCurrentMonthSold(row.get("caseWithBtl")!=null?Double.parseDouble(row.get("caseWithBtl").toString()):0);
					saleBean.setTotalPrice(row.get("total")!=null?Double.parseDouble(row.get("total").toString()):0);
					saleBean.setSaleDate(row.get("invoicedate")!=null?row.get("invoicedate").toString():"");
					saleBean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
					saleBean.setCompany(row.get("companyName").toString()!=null?row.get("companyName").toString():"");
					saleBean.setTarget(row.get("mrpRoundOff")!=null?Double.parseDouble(row.get("mrpRoundOff").toString()):0);// For mrp round off
					saleBean.setLastMonthSold(row.get("tcs")!=null?Double.parseDouble(row.get("tcs").toString()):0);// For tcs value
					saleBean.setSingleBottelPrice(row.get("turnoverTax")!=null?Double.parseDouble(row.get("turnoverTax").toString()):0); // for turnOver Tax
					saleBean.setCategoryOrder(row.get("categoryOrder")!=null?Long.parseLong(row.get("categoryOrder").toString()):0);
					saleBean.setSalePrimaryKey(row.get("categoryColor")!=null?row.get("categoryColor").toString():"");
				    stockList.add(saleBean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return stockList;
		}finally {
			
		}
		return stockList;
	}
	/**
	 * This method is used to save invoice data to 'invoice' table.
	 * As input getting data in json format.
	 * */
	@Override
	public String saveInvoiceJsonData(JSONArray invoiceJson) {
		log.info("PoizonDaoImpl.saveInvoiceJsonData invoiceJson= "+invoiceJson);
		String QUERY="insert into invoice (brandNoPackQty,caseQty,packQty,QtyBottels,SingleBottelRate,packQtyRate,bottleSaleMrp,totalPrice,receivedBottles,invoiceDate,invoiceDateAsPerSheet,specialMargin,store_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		String result=null;
		synchronized(this){
		try {
			for (int i = 0; i < invoiceJson.length(); ++i) {
			 JSONObject rec;
			    rec = invoiceJson.getJSONObject(i);
			    double singleBtlPrice=rec.getDouble("SingleBottelRate");
			    int ttlBtls=rec.getInt("caseQty")*rec.getInt("packQtyId")+rec.getInt("QtyBottels");
				//Double total=(double) ((rec.getInt("caseQty")*rec.getInt("packQtyId")+rec.getInt("QtyBottels"))*singleBtlPrice);
			    Double total=ttlBtls*singleBtlPrice;
				Double totalPrice = (double) Math.round(total);
			    try{
					int val=jdbcTemplate.update(QUERY,new Object[]{rec.getDouble("brandNoPackQtyId"),rec.getInt("caseQty"),rec.getInt("packQtyId"),rec.getInt("QtyBottels"),singleBtlPrice,rec.getDouble("packQtyRate"),rec.getDouble("EachBottleMrp"),rec.getDouble("totalValue"),ttlBtls,rec.get("date"),rec.get("dateAsCopy"),rec.getDouble("marginVal"),userSession.getStoreId()});
						if(val>0)
							result="Invoice Added Successfully";
				}catch(Exception e){
					result="something went wrong";
					log.log(Level.WARN, "saveInvoiceJsonData in PoizonDaoImpl exception", e);
					e.printStackTrace();
					}
			}
			if(invoiceJson.length() > 0)
				updateStockLiftingDiscount(invoiceJson.getJSONObject(0).getString("dateAsCopy"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			result="Something Went Wrong.";
			log.log(Level.WARN, "saveInvoiceJsonData in PoizonDaoImpl exception", e);
			e.printStackTrace();
		}
		}
		return result;
	}
	private void updateStockLiftingDiscount(String date) {
    log.info("PoizonDaoImpl :: updateStockLiftingDiscount()");
    String QUERY = "SELECT COALESCE(ROUND(CEIL(SUM(s.receivedBottles/p.packQty))),0) AS liftedCase, p.`shortBrandName`,p.`realBrandNo`,"
    		+ "(SELECT COALESCE(SUM(d.discountAmount),0) FROM stock_lift_with_discount d WHERE d.brandNo=p.realBrandNo AND store_id="+userSession.getStoreId()+""
    		+ " AND d.stockDiscountDate=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+date+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))) AS discountAmount,"
    		+ "(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+date+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01')) AS stockDate"
    		+ " FROM product p INNER JOIN invoice s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND s.store_id="+userSession.getStoreId()+" "
    		+ "AND s.`invoiceDateAsPerSheet`>= (SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+date+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))"
    		+ " AND  s.invoiceDateAsPerSheet <= (SELECT LAST_DAY('"+date+"')) GROUP BY p.`realBrandNo`";
    try{
    	List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
    	if(rows.size() > 0 && rows !=null){
        for(Map row : rows){
        	double caseQty = row.get("liftedCase")!=null?Double.parseDouble(row.get("liftedCase").toString()):0;
        	int brandNo = row.get("realBrandNo")!=null?Integer.parseInt(row.get("realBrandNo").toString()):0;
        	double discountAmt = row.get("discountAmount")!=null?Double.parseDouble(row.get("discountAmount").toString()):0;
        	double totatDiscount = caseQty * discountAmt;
        	String discDate = row.get("stockDate")!=null?row.get("stockDate").toString():"";
        	jdbcTemplate.update("UPDATE stock_lift_with_discount SET  totalDiscountAmount=? WHERE brandNo = ? AND stockDiscountDate= ? AND store_id= ?", new Object[]{totatDiscount, brandNo, discDate, userSession.getStoreId()});
        }
    		
    	}
    }catch(Exception e){
    	e.printStackTrace();
    }
		
	}
	/*Old method not used any where*/
	@Override
	public List<SaleBean> getTopSaleDetails(String startDate,String endDate,int type) {
		System.out.println("getTopSaleDetails daoimpl ");
		String QUERY=null;
		if(type==1)
		 QUERY="SELECT s.`saleId`,p.`brandName`,p.`quantity`,s.`brandNoPackQty`,SUM(s.`sale`) AS sale ,SUM(s.`totalPrice`) AS totalPrice FROM sale s INNER JOIN `product` p ON s.`brandNoPackQty`=p.`brandNoPackQty` WHERE s.`saleDate`>='"+startDate+"' AND s.`saleDate`<='"+endDate+"'  GROUP BY p.`brandNoPackQty` ORDER BY totalPrice DESC LIMIT 10";
		else if(type==2)
			QUERY="SELECT s.`saleId`,p.`brandName`,p.`quantity`,s.`brandNoPackQty`,SUM(s.`sale`) AS sale ,SUM(s.`totalPrice`) AS totalPrice FROM sale s INNER JOIN `product` p ON s.`brandNoPackQty`=p.`brandNoPackQty` WHERE s.`saleDate`>='"+startDate+"' AND s.`saleDate`<='"+endDate+"' GROUP BY p.`brandNoPackQty` ORDER BY sale DESC LIMIT 10";
		else if(type==3)
			QUERY="SELECT s.`saleId`,p.`brandName`,p.`quantity`,s.`brandNoPackQty`,SUM(s.`sale`) AS sale ,SUM(s.`totalPrice`) AS totalPrice FROM sale s INNER JOIN `product` p ON s.`brandNoPackQty`=p.`brandNoPackQty` WHERE s.`saleDate`>='"+startDate+"' AND s.`saleDate`<='"+endDate+"'  AND p.`category` IN(1,2) GROUP BY p.`brandNoPackQty`ORDER BY sale DESC LIMIT 5";
		else if(type==4)
			QUERY="SELECT s.`saleId`,p.`brandName`,p.`quantity`,s.`brandNoPackQty`,SUM(s.`sale`) AS sale ,SUM(s.`totalPrice`) AS totalPrice FROM sale s INNER JOIN `product` p ON s.`brandNoPackQty`=p.`brandNoPackQty` WHERE s.`saleDate`>='"+startDate+"' AND s.`saleDate`<='"+endDate+"'  AND p.`category` NOT IN(1,2) GROUP BY p.`brandNoPackQty`ORDER BY sale DESC LIMIT 5";
		else
			QUERY="SELECT s.`saleId`,p.`brandName`,p.`quantity`,s.`brandNoPackQty`,SUM(s.`sale`) AS sale ,SUM(s.`totalPrice`) AS totalPrice FROM sale s INNER JOIN `product` p ON s.`brandNoPackQty`=p.`brandNoPackQty` WHERE s.`saleDate`>='"+startDate+"' AND s.`saleDate`<='"+endDate+"'  AND p.`category` IN('DUTY PAID') GROUP BY p.`brandNoPackQty`ORDER BY s.`sale` DESC LIMIT 5";
		List<SaleBean> saleBeanList = new ArrayList<SaleBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					SaleBean saleBean = new SaleBean();
					saleBean.setSaleId(Long.parseLong(row.get("saleId").toString()));
					saleBean.setBrandName(row.get("brandName").toString());
					saleBean.setQuantity(row.get("quantity").toString());
					saleBean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
					saleBean.setTotalSale(Long.parseLong(row.get("sale").toString()));
					saleBean.setTotalPrice(Double.parseDouble(row.get("totalPrice").toString()));
					//saleBean.setSaleDate(row.get("saleDate").toString());
					saleBeanList.add(saleBean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return saleBeanList;
		}finally {
			
		}
		return saleBeanList;
	}
	/**
	 * This method is used to get no. of sold cases with product wise in between the selecetd dates.
	 * And also getting available current stock and prediction no. of days for currect stock.
	 * */
	@Override
	public SaleAndAmmountBean getSaleReports(String newSdate, String newEdate)  {
		
		int storeId=userSession.getStoreId();
		String previousDate=null;
		int dayIndex;
		Date date1 =null;
		     try {
				 date1=new SimpleDateFormat("yyyy-MM-dd").parse(newSdate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			} 
		        Calendar calendar = Calendar.getInstance();
		        calendar.setTime(date1);
		        dayIndex = calendar.get(Calendar.DAY_OF_WEEK); // th
		        switch (dayIndex) { 
		        case 2: 
		        	previousDate = jdbcTemplate.queryForObject("SELECT DATE_SUB('"+newSdate+"', INTERVAL 4 DAY )", String.class);
		            break; 
		        case 3: 
		        	previousDate = jdbcTemplate.queryForObject("SELECT DATE_SUB('"+newSdate+"', INTERVAL 1 DAY )", String.class); 
		            break; 
		        case 4: 
		        	previousDate = jdbcTemplate.queryForObject("SELECT DATE_SUB('"+newSdate+"', INTERVAL 1 DAY )", String.class); 
		            break; 
		        case 5: 
		        	previousDate = jdbcTemplate.queryForObject("SELECT DATE_SUB('"+newSdate+"', INTERVAL 1 DAY )", String.class);
		            break; 
		        case 6: 
		        	previousDate = jdbcTemplate.queryForObject("SELECT DATE_SUB('"+newSdate+"', INTERVAL 7 DAY )", String.class);
		            break; 
		        case 7: 
		        	previousDate = jdbcTemplate.queryForObject("SELECT DATE_SUB('"+newSdate+"', INTERVAL 7 DAY )", String.class);
		            break; 
		        case 1: 
		        	previousDate = jdbcTemplate.queryForObject("SELECT DATE_SUB('"+newSdate+"', INTERVAL 7 DAY )", String.class);
		            break; 
		        default: 
		        	previousDate = newSdate; 
		            break; 
		        } 
		        System.out.println("dayIndex :: "+dayIndex+" previousDate:: "+previousDate);
		        String QUERY = null;
		        int size = jdbcTemplate.queryForObject("SELECT COUNT(DISTINCT saleDate) FROM sale WHERE saleDate > '"+newEdate+"' AND store_id = "+userSession.getStoreId()+"",Integer.class);
		        if(size > 0){
			/*
			 * QUERY =
			 * "SELECT DISTINCT p.productId,p.brandNoPackQty,p.realBrandNo,p.shortBrandName,p.productType,p.quantity,p.packQty,p.packType,p.category,"
			 * +
			 * "(SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty = p.brandNoPackQty ORDER BY i.invoiceDateAsPerSheet DESC LIMIT 1) AS saleRate,"
			 * +
			 * "(SELECT sc.closing FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='"
			 * +newEdate+"') AS clsoing," +
			 * "((SELECT sc.closing FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='"
			 * +newEdate+"')/p.packQty) AS closingCase," +
			 * "((SELECT sc.closing FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='"
			 * +newEdate+"')-(p.`packQty`*(TRUNCATE((SELECT sc.closing FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='"
			 * +newEdate+"')/p.`packQty`,0)))) AS BtlClosing," +
			 * "(SELECT t.categoryName FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryName,p.company,"
			 * +
			 * "(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,"
			 * +
			 * "(SELECT t.color FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS color,(SELECT t.color FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryColor,s.`unitPrice`,"
			 * +
			 * "(SELECT t.companyOrder FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyorder,(SELECT t.categoryOrder FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryOrder,SUM(s.sale) AS sale,"
			 * +
			 * "(SELECT SUM(s1.sale) FROM sale s1 WHERE s1.brandNoPackQty = p.brandNoPackQty AND s1.saleDate='"
			 * +previousDate+"') AS previousSale," +
			 * "TRUNCATE(SUM(s.sale)/p.`packQty`,0) AS cases, (SUM(s.sale)/p.`packQty`) AS realCases,SUM(s.totalPrice) total,"
			 * +
			 * "(SELECT TRUNCATE((SELECT COALESCE(s1.closing,0) FROM sale s1 WHERE s1.`brandNoPackQty`=p.brandNoPackQty AND s1.saleDate ='"
			 * +newEdate+"')/(SELECT TRUNCATE(COALESCE(SUM(s2.`sale`)/14,0),2) FROM sale s2 "
			 * + "WHERE s2.`brandNoPackQty`= p.brandNoPackQty AND s2.saleDate >=(SELECT '"
			 * +newEdate+"' -INTERVAL 14 DAY) AND s2.saleDate <='"
			 * +newEdate+"'),0)) AS noOfDays FROM product p " +
			 * "INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`>='"
			 * +newSdate+"' AND `saleDate`<='"+newEdate+"' " +
			 * "GROUP BY brandNoPackQty ORDER BY p.productId ASC";
			 * 
			 */
		        QUERY = "SELECT DISTINCT p.productId,p.brandNoPackQty,p.realBrandNo,p.shortBrandName,p.productType,p.quantity,p.packQty,p.packType,p.category,"
		        	 	+ "(SELECT sc.closing FROM sale sc WHERE sc.store_id=" + storeId + " AND sc.brandNoPackQty = p.`brandNoPackQty`"
		        	 	+ " AND sc.saleDate='" + newEdate + "') AS clsoing,"
		        	 	+ "((SELECT sc.closing FROM sale sc WHERE sc.store_id=" + storeId + " AND sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='" + newEdate + "')/p.packQty) AS closingCase,"
		        	 	+ "((SELECT sc.closing FROM sale sc WHERE sc.store_id=" + storeId + " AND sc.brandNoPackQty = p.`brandNoPackQty` "
		        	 	+ "AND sc.saleDate='" + newEdate + "')-(p.`packQty`*(TRUNCATE((SELECT sc.closing FROM sale sc WHERE sc.store_id=" + storeId + " AND "
		        	 	+ "sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='" + newEdate + "')/p.`packQty`,0)))) AS BtlClosing,"
		        	 	+ "(SELECT t.categoryName FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryName,p.company," + "(SELECT t.companyName FROM "
		        	 	+ "`company_tab` t WHERE p.`company`=t.`companyId`) AS companyName," + "(SELECT t.color FROM `company_tab` t WHERE p.`company`=t.`companyId`) "
		        	 	+ "AS color,(SELECT t.color FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryColor,s.`unitPrice`,(SELECT t.companyOrder FROM "
		        	 	+ "`company_tab` t WHERE p.`company`=t.`companyId`) AS companyorder,(SELECT t.categoryOrder FROM `category_tab` t WHERE p.`category`=t.`categoryId`) "
		        	 	+ "AS categoryOrder,SUM(s.sale) AS sale,(SELECT SUM(s1.sale) FROM sale s1 WHERE s1.store_id=" + storeId + " AND s1.brandNoPackQty"
		        	 	+ " = p.brandNoPackQty AND s1.saleDate='" + previousDate + "') AS previousSale,TRUNCATE(SUM(s.sale)/p.`packQty`,0) AS cases, "
		        	 	+ "(SUM(s.sale)/p.`packQty`) AS realCases,SUM(s.totalPrice) total,(SELECT TRUNCATE((SELECT COALESCE(s1.closing,0) FROM sale s1 WHERE  "
		        	 	+ "s1.store_id=" + storeId + " AND s1.`brandNoPackQty`=p.brandNoPackQty AND s1.saleDate ='" + newEdate + "')/(SELECT TRUNCATE(COALESCE(SUM(s2.`sale`)/14,0),2) FROM "
		        	 	+ "sale s2 WHERE s2.store_id=" + storeId + " AND s2.`brandNoPackQty`= p.brandNoPackQty AND s2.saleDate >=(SELECT '" + newEdate + "' -INTERVAL 14 DAY) "
		        	 	+ "AND s2.saleDate <='" + newEdate + "'),0)) AS noOfDays FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND "
		        	 	+ "`saleDate`>='" + newSdate + "' AND s.store_id=" + storeId + "  AND `saleDate`<='" + newEdate + "' GROUP BY brandNoPackQty "
		        	 	+ "ORDER BY p.productId ASC";
		        }
		        else{
			/*
			 * QUERY
			 * ="SELECT DISTINCT p.productId,p.brandNoPackQty,p.realBrandNo,p.shortBrandName,p.productType,p.quantity,p.packQty,p.packType,p.category,"
			 * +
			 * "(SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty = p.brandNoPackQty ORDER BY i.invoiceDateAsPerSheet DESC LIMIT 1) AS saleRate,"
			 * +
			 * "(SELECT COALESCE(SUM(sc.closing),0) + (SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r WHERE r.invoiceDate > '"
			 * +newEdate+"' " +
			 * "AND r.brandNoPackQty = p.`brandNoPackQty`) FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='"
			 * +newEdate+"') " +
			 * "AS clsoing,(SELECT COALESCE(SUM(sc.closing),0) + (SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r WHERE r.invoiceDate > '"
			 * +newEdate+"' " +
			 * "AND r.brandNoPackQty = p.`brandNoPackQty`) FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='"
			 * +newEdate+"')/p.packQty AS closingCase," +
			 * "((SELECT COALESCE(SUM(sc.closing),0) + (SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r WHERE r.invoiceDate > '"
			 * +newEdate+"'" +
			 * " AND r.brandNoPackQty = p.`brandNoPackQty`) FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='"
			 * +newEdate+"') -" +
			 * " (p.`packQty`*(TRUNCATE((SELECT COALESCE(SUM(sc.closing),0) + (SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r"
			 * + " WHERE r.invoiceDate > '"
			 * +newEdate+"' AND r.brandNoPackQty = p.`brandNoPackQty`) FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` "
			 * + "AND sc.saleDate='"
			 * +newEdate+"')/p.packQty,0)))) AS BtlClosing, (SELECT t.categoryName FROM `category_tab` t WHERE p.`category`=t.`categoryId`)"
			 * +
			 * " AS categoryName,p.company,(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,(SELECT t.color "
			 * +
			 * "FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS color,(SELECT t.color FROM `category_tab` t WHERE p.`category`=t.`categoryId`) "
			 * +
			 * "AS categoryColor,s.`unitPrice`,(SELECT t.companyOrder FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyorder,(SELECT t.categoryOrder "
			 * +
			 * "FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryOrder,SUM(s.sale) AS sale,(SELECT SUM(s1.sale) FROM sale s1 "
			 * + "WHERE s1.brandNoPackQty = p.brandNoPackQty AND s1.saleDate='"
			 * +previousDate+"') AS previousSale,TRUNCATE(SUM(s.sale)/p.`packQty`,0) AS cases,"
			 * +
			 * " (SUM(s.sale)/p.`packQty`) AS realCases,SUM(s.totalPrice) total,(SELECT TRUNCATE((SELECT COALESCE(SUM(sc.closing),0) +"
			 * +
			 * " (SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r WHERE r.invoiceDate > '"
			 * +newEdate+"' AND r.brandNoPackQty = p.`brandNoPackQty`) " +
			 * "FROM sale sc WHERE sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='"
			 * +newEdate+"')/(SELECT TRUNCATE(COALESCE(SUM(s2.`sale`)/14,0),2) FROM " +
			 * "sale s2 WHERE s2.`brandNoPackQty`= p.brandNoPackQty AND s2.saleDate >=(SELECT '"
			 * +newEdate+"' -INTERVAL 14 DAY) AND s2.saleDate <='"+newEdate+"'),0)) " +
			 * "AS noOfDays  FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`>='"
			 * +newSdate+"' AND " + " `saleDate`<='"
			 * +newEdate+"' GROUP BY brandNoPackQty ORDER BY p.`realBrandNo` ASC";
			 */
		        QUERY = "SELECT DISTINCT p.productId,p.brandNoPackQty,p.realBrandNo,p.shortBrandName,p.productType,p.quantity,p.packQty,p.packType,p.category,(SELECT COALESCE(SUM(sc.closing),0) + "
		            	+ "(SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r WHERE r.store_id=" + storeId + " AND r.invoiceDate > '" + newEdate + "' " 
		        		+ "AND r.brandNoPackQty = p.`brandNoPackQty`)"
		            	+ " FROM sale sc WHERE sc.store_id=" + storeId + " AND sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='" + newEdate + "') " 
		        		+ "AS clsoing,(SELECT COALESCE(SUM(sc.closing),0) + (SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r WHERE r.store_id=" + storeId + " AND r.invoiceDate > '" + newEdate + "' " 
		            	+ "AND r.brandNoPackQty = p.`brandNoPackQty`) FROM sale sc WHERE sc.store_id=" + storeId + " AND sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='" + newEdate + "')/p.packQty AS closingCase," 
		        		+ "((SELECT COALESCE(SUM(sc.closing),0) + (SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r WHERE r.store_id=" + storeId + " AND r.invoiceDate > '" + newEdate + "'" + " AND r.brandNoPackQty = p.`brandNoPackQty`) FROM sale sc WHERE sc.store_id=" + storeId + " AND sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='" + newEdate + "') -" 
		            	+ " (p.`packQty`*(TRUNCATE((SELECT COALESCE(SUM(sc.closing),0) + (SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r WHERE r.store_id=" + storeId + " AND  r.invoiceDate > '" + newEdate + "' AND r.brandNoPackQty = p.`brandNoPackQty`) FROM sale sc WHERE sc.store_id=" + storeId + " AND sc.brandNoPackQty = p.`brandNoPackQty` " 
		        		+ "AND sc.saleDate='" + newEdate + "')/p.packQty,0)))) AS BtlClosing, (SELECT t.categoryName FROM `category_tab` t WHERE p.`category`=t.`categoryId`)" + " AS categoryName,p.company,(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,(SELECT t.color " + "FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS color,(SELECT t.color FROM `category_tab` t WHERE p.`category`=t.`categoryId`) " 
		            	+ "AS categoryColor,s.`unitPrice`,(SELECT t.companyOrder FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyorder,(SELECT t.categoryOrder " + "FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryOrder,SUM(s.sale) AS sale,(SELECT SUM(s1.sale) FROM sale s1 WHERE s1.brandNoPackQty = p.brandNoPackQty AND s1.store_id=" + storeId + " AND s1.saleDate='" + previousDate + "') AS previousSale,TRUNCATE(SUM(s.sale)/p.`packQty`,0) AS cases," 
		        		+ " (SUM(s.sale)/p.`packQty`) AS realCases,SUM(s.totalPrice) total,(SELECT TRUNCATE((SELECT COALESCE(SUM(sc.closing),0) + (SELECT COALESCE(SUM(r.receivedBottles),0) FROM invoice r WHERE r.store_id=" + storeId + " AND r.invoiceDate > '" + newEdate + "' AND r.brandNoPackQty = p.`brandNoPackQty`) " 
		            	+ "FROM sale sc WHERE sc.store_id=" + storeId + " AND sc.brandNoPackQty = p.`brandNoPackQty` AND sc.saleDate='" + newEdate + "')/(SELECT TRUNCATE(COALESCE(SUM(s2.`sale`)/14,0),2) FROM " 
		            	+ "sale s2 WHERE s2.store_id=" + storeId + " AND s2.`brandNoPackQty`= p.brandNoPackQty AND s2.saleDate >=(SELECT '" + newEdate + "' -INTERVAL 14 DAY) AND s2.saleDate <='" + newEdate + "'),0)) " 
		            	+ "AS noOfDays  FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND s.store_id=" + storeId + " AND `saleDate`>='" + newSdate + "' AND " + " `saleDate`<='" + newEdate + "' GROUP BY brandNoPackQty ORDER BY p.`realBrandNo` ASC";

		        	
		        	
		        }
		        System.out.println(QUERY);
		SaleAndAmmountBean saleAndAmmountBean = new SaleAndAmmountBean();
		List<SaleBean> saleBeanList = new ArrayList<SaleBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					SaleBean saleBean = new SaleBean();
					saleBean.setProductId(Long.parseLong(row.get("category").toString()));
					saleBean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
					saleBean.setBrandNo(Long.parseLong(row.get("realBrandNo").toString()));
					saleBean.setBrandName(row.get("shortBrandName").toString());
					saleBean.setProductType(row.get("productType").toString());
					saleBean.setQuantity(row.get("quantity").toString());
					saleBean.setPackQty(Long.parseLong(row.get("packQty").toString()));
					saleBean.setPackType(row.get("packType").toString());
					saleBean.setUnitPrice(row.get("unitPrice")!=null?Double.parseDouble(row.get("unitPrice").toString()):0);
					saleBean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
					saleBean.setTotalSale(row.get("sale")!= null?Long.parseLong(row.get("sale").toString()):0);
					saleBean.setTotalPrice(row.get("total")!=null?Double.parseDouble(row.get("total").toString()):0);
					saleBean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
					saleBean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
					saleBean.setColor(row.get("color")!=null?row.get("color").toString():"");
					saleBean.setCaseQty(row.get("cases")!=null?Long.parseLong(row.get("cases").toString()):0);
					saleBean.setCummulativePrice(row.get("clsoing")!=null?Double.parseDouble(row.get("clsoing").toString()):0);
					saleBean.setBottleSaleMrp(row.get("closingCase")!=null?Double.parseDouble(row.get("closingCase").toString()):0);
					saleBean.setQtyBottels(row.get("BtlClosing")!=null?Long.parseLong(row.get("BtlClosing").toString()):0);
					//saleBean.setReceived(Math.round(calculateTotalCase(Long.parseLong(row.get("realBrandNo").toString()),newSdate,newEdate)));
					saleBean.setSaleId(row.get("noOfDays")!=null?Long.parseLong(row.get("noOfDays").toString()):0);
					saleBean.setCurrentMonthSold(row.get("realCases")!=null?Double.parseDouble(row.get("realCases").toString()):0);
					saleBean.setNoOfReturnsBtl(row.get("previousSale")!=null?Long.parseLong(row.get("previousSale").toString()):0);
					saleBean.setCategoryOrder(row.get("categoryOrder")!=null?Long.parseLong(row.get("categoryOrder").toString()):0);
					saleBean.setInvoiceDateAsCopy(row.get("categoryColor")!=null?row.get("categoryColor").toString():"");
					saleBean.setOpening((long) (row.get("saleRate")!=null?Double.parseDouble(row.get("saleRate").toString()):0));
					if(row.get("packType").toString().equalsIgnoreCase("2L"))
						saleBean.setClosing((long)1);
					else if(row.get("packType").toString().equalsIgnoreCase("1L"))
						saleBean.setClosing((long)2);
					else if(row.get("packType").toString().equalsIgnoreCase("Q"))
						saleBean.setClosing((long)3);
					else if(row.get("packType").toString().equalsIgnoreCase("P"))
						saleBean.setClosing((long)4);
					else if(row.get("packType").toString().equalsIgnoreCase("N"))
						saleBean.setClosing((long)5);
					else if(row.get("packType").toString().equalsIgnoreCase("D"))
						saleBean.setClosing((long)6);
					else if(row.get("packType").toString().equalsIgnoreCase("LB"))
						saleBean.setClosing((long)7);
					else if(row.get("packType").toString().equalsIgnoreCase("SB"))
						saleBean.setClosing((long)8);
					else if(row.get("packType").toString().equalsIgnoreCase("TIN"))
						saleBean.setClosing((long)9);
					else
						saleBean.setClosing((long)10);
					saleBeanList.add(saleBean);
					saleAndAmmountBean.setSaleBean(saleBeanList);
				}
			}
			
			
			/*
			 * List<Map<String, Object>> expenseRow = jdbcTemplate.
			 * queryForList("SELECT SUM(`totalAmount`) AS totalAmount FROM `expense_master` WHERE `expenseMasterDate`>='"
			 * +newSdate.toString()+"' AND expenseMasterDate <='"+newEdate.toString()+"'");
			 */	
			
            final List<Map<String, Object>> expenseRow = (List<Map<String, Object>>)this.jdbcTemplate.queryForList("SELECT SUM(`totalAmount`) AS totalAmount FROM `expense_master` WHERE store_id=" + storeId + " AND `expenseMasterDate`>='" + newSdate.toString() + "' AND expenseMasterDate <='" + newEdate.toString() + "'");

			
			if(expenseRow != null && expenseRow.size()>0) {
				for(Map row : expenseRow){
					saleAndAmmountBean.setAmount(row.get("totalAmount")!=null?Double.parseDouble(row.get("totalAmount").toString()):0);
				   }
				}
		}catch(Exception e){
			e.printStackTrace();
			return saleAndAmmountBean;
		}finally {
			
		}
		return saleAndAmmountBean;
	}
	
	
	private double calculateTotalCase(long brandNo,String newSdate,String newEdate) {
		double newCase=0;
	try{
		List<Map<String, Object>> expenseRow = jdbcTemplate.queryForList("SELECT (SUM(sale)/p.packQty) AS sale FROM product p INNER JOIN sale s "
				+ "ON p.`brandNoPackQty`= s.`brandNoPackQty` AND p.`realBrandNo`="+brandNo+" AND s.`saleDate`>='"+newSdate+"'"
				+ " AND s.`saleDate`<='"+newEdate+"' AND s.store_id = "+userSession.getStoreId()+" GROUP BY p.brandNoPackQty");
		if(expenseRow != null && expenseRow.size()>0) {
			for(Map row : expenseRow){
				newCase = newCase+(row.get("sale")!=null?Double.parseDouble(row.get("sale").toString()):0);
			   }
			}
	}catch(Exception e){
		e.printStackTrace();
		return newCase;
	}
	return newCase;
		
	}
	/**
	 * This method is used to get all expense category from 'expense_category' tab.
	 * */
	@Override
	public ExpenseCategoryAndDate getExpenseCategory() {
		System.out.println("getExpenseCategory Daoimpl()");
		String QUERY="SELECT `categoryId`,`expenseName`,`expenseDate` FROM `expense_category`";
		ExpenseCategoryAndDate categoryAndDateBean = new ExpenseCategoryAndDate(); 
		List<ExpenseCategoryBean> expenseCategoryList= new ArrayList<ExpenseCategoryBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					ExpenseCategoryBean expenseCategoryBean = new ExpenseCategoryBean();
					expenseCategoryBean.setCategoryId(Long.parseLong(row.get("categoryId").toString()));
					expenseCategoryBean.setExpenseName(row.get("expenseName").toString());
					expenseCategoryBean.setExpenseDate(row.get("expenseDate").toString());
					expenseCategoryList.add(expenseCategoryBean);
				}
			}
			categoryAndDateBean.setExpenseCategoryBean(expenseCategoryList);
			String edate = jdbcTemplate.queryForObject("SELECT DATE_ADD((SELECT `expenseMasterDate` FROM `expense_master` WHERE store_id = "+userSession.getStoreId()+" ORDER BY `expenseMasterId`  DESC LIMIT 1), INTERVAL 1 DAY)", String.class);
			if(edate !=null && edate !="")
				categoryAndDateBean.setEdate(edate);
			else {
				categoryAndDateBean.setEdate(jdbcTemplate.queryForObject("SELECT `invoiceDate` FROM `invoice` WHERE store_id = "+userSession.getStoreId()+" ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1",String.class));
			}
			/*
			 * try { categoryAndDateBean.setEdate(); }catch (Exception e) {
			 * e.printStackTrace(); System.out.println("khagesh==========");
			 * categoryAndDateBean.setEdate(jdbcTemplate.
			 * queryForObject("SELECT `invoiceDate` FROM `invoice` WHERE store_id = "
			 * +userSession.getStoreId()+" ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1",
			 * String.class)); }
			 */
		}catch(Exception e){
			e.printStackTrace();
			return categoryAndDateBean;
		}
		
		return categoryAndDateBean;
	}
	/**
	 * This method is used to save expenses for the date
	 * */
	@Override
	public String saveExpenses(JSONArray expenseJson) {
		log.info("PoizonDaoImpl.saveExpenses expenseJson= "+expenseJson);
		String QUERY1="INSERT INTO expense_master(totalAmount,expenseMasterDate,store_id) VALUES(?,?,?)";
		String QUERY2="SELECT expenseMasterId FROM expense_master WHERE store_id="+userSession.getStoreId()+" ORDER BY expenseMasterId DESC LIMIT 1";
		String QUERY3="INSERT INTO expense_child (categoryId,expenseMasterId,expenseChildAmount,COMMENT) VALUES(?,?,?,?)";
		String result=null;
		String sdate=null;
		int masterId = 0;
		synchronized(this){
		try{
			for (int i = expenseJson.length()-1; i >= 0; --i) {
				JSONObject rec;
				 rec = expenseJson.getJSONObject(i);
				if(i==expenseJson.length()-1){
					sdate=rec.getString("expenseDate");
					double ttlAmount=Math.round(rec.getDouble("totalAmount"));
					int val=jdbcTemplate.update(QUERY1,new Object[]{ttlAmount,sdate,userSession.getStoreId()});
					if(val>0)
					{
						List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY2);
						if(rows != null && rows.size()>0) {
							for(Map row : rows){
								masterId=Integer.parseInt(row.get("expenseMasterId").toString());
							}
						}	
					}
				}else{
					
					int val=jdbcTemplate.update(QUERY3,new Object[]{Integer.parseInt(rec.getString("categoryId")),masterId,Double.parseDouble(rec.getString("amount")),rec.get("commment")});
				}
			}
			result="Expenses Added Successfully";
		}catch(Exception e){
			result="Somethig went wrong";
			log.log(Level.WARN, "saveExpenses in PoizonDaoImpl exception", e);
			e.printStackTrace();
		}
		}
		return result;
	}
	@Override
	public List<SaleBean> getUpdateSale() {
		String QUERY1="SELECT saleDate FROM sale ORDER BY saleId  DESC LIMIT 1,1";
		String date=null;
		List<SaleBean> saleBeanList = new ArrayList<SaleBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY1);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					date=row.get("saleDate").toString();
				}
			}
			if(date != null && date !=""){
			String QUERY2="SELECT p.productId,p.brandNoPackQty,p.brandNo,p.brandName,p.productType,p.quantity,p.packQty,p.packType,s.saleId,s.salePrimaryKey,s.sale,s.closing,s.unitPrice,s.totalPrice,s.saleDate,s.opening,s.received FROM product p INNER JOIN sale s ON p.brandNoPackQty=s.brandNoPackQty AND s.saleDate='"+date+"' ORDER BY p.productId ASC";	
			List<Map<String, Object>> saleRows = jdbcTemplate.queryForList(QUERY2);
			if(saleRows != null && saleRows.size()>0) {
				for(Map row : saleRows){
					SaleBean saleBean = new SaleBean();
					saleBean.setProductId(Long.parseLong(row.get("productId").toString()));
					saleBean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
					saleBean.setBrandNo(Long.parseLong(row.get("brandNo").toString()));
					saleBean.setBrandName(row.get("brandName").toString());
					saleBean.setProductType(row.get("productType").toString());
					saleBean.setQuantity(row.get("quantity").toString());
					saleBean.setPackQty(Long.parseLong(row.get("packQty").toString()));
					saleBean.setPackType(row.get("packType").toString());
					saleBean.setSaleId(Long.parseLong(row.get("saleId").toString()));
					saleBean.setSalePrimaryKey(row.get("salePrimaryKey").toString());
					saleBean.setTotalSale(Long.parseLong(row.get("sale").toString()));
					saleBean.setClosing(Long.parseLong(row.get("closing").toString()));
					saleBean.setReceived(row.get("received")!=null?Long.parseLong(row.get("received").toString()):0);
					saleBean.setUnitPrice(Double.parseDouble(row.get("unitPrice").toString()));
					saleBean.setTotalPrice(Double.parseDouble(row.get("totalPrice").toString()));
					saleBean.setSaleDate(row.get("saleDate")!= null?row.get("saleDate").toString():"");
					saleBean.setOpening(Long.parseLong(row.get("opening").toString()));
					saleBeanList.add(saleBean);
				 }
			  }
				
				
			}
		}catch(Exception e){
			e.printStackTrace();
			return saleBeanList;
		}
		return saleBeanList;
	}
	/*This method is used to update the sale */
	@Override
	public String updateSaleDetails(JSONArray saleDetailsJson) {
		log.info("PoizonDaoImpl.updateSaleDetails saleDetailsJson= "+saleDetailsJson);
		String result=null;
		synchronized(this){
		try {
			for (int i = 0; i < saleDetailsJson.length(); ++i) {
			 JSONObject rec;
			    rec = saleDetailsJson.getJSONObject(i);
			    String QUERY="UPDATE sale SET opening="+rec.getInt("opening")+",`sale`="+rec.getInt("totalSale")+",closing="+rec.getInt("closing")+",`totalPrice`="+rec.getDouble("totalPrice")+" WHERE `saleId`="+rec.getInt("saleId")+" AND `brandNoPackQty`="+rec.getInt("brandNoPackQty")+" AND store_id="+userSession.getStoreId();
			    int val = jdbcTemplate.update(QUERY);
			    if(val > 0){
			    	result="Sales Updated successfully";
			    }
			    
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			result="Something Went Wrong.";
			log.log(Level.WARN, "updateSaleDetails in PoizonDaoImpl exception", e);
			e.printStackTrace();
		}
		}
		return result;
	}
	/*This method is used to update cash or card amount for the latest date*/
	@Override
	public CardCashSaleBeen getCashCardSale() {
		String QUERY="SELECT id,`cardSale`,`cashSale`,chequeSale,`date` FROM `card_cash_sale` WHERE store_id = "+userSession.getStoreId()+" ORDER BY `date` DESC LIMIT 1";
		CardCashSaleBeen cardCashSaleBeen = new CardCashSaleBeen();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					cardCashSaleBeen.setId(Long.parseLong(row.get("id").toString()));
					cardCashSaleBeen.setCardSale(row.get("cardSale")!=null?Double.parseDouble(row.get("cardSale").toString()):0.0);
					cardCashSaleBeen.setCashSale(row.get("cashSale")!=null?Double.parseDouble(row.get("cashSale").toString()):0.0);
					cardCashSaleBeen.setChequeSale(row.get("chequeSale")!=null?Double.parseDouble(row.get("chequeSale").toString()):0);
					cardCashSaleBeen.setDate(row.get("date")!=null?row.get("date").toString():"");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return cardCashSaleBeen;
	}
	/*This method is used to save cash or card amount for the day*/
	@Override
	public String saveCardCashSale(CardCashSaleBeen cardCashSaleBeen) {
		log.info("PoizonDaoImpl.saveCardCashSale cardCashSaleBeen = "+cardCashSaleBeen);
		System.out.println("cardCashSaleBeen>> "+cardCashSaleBeen);
		String result=null;
		try{
			if(cardCashSaleBeen.getId() == -1 || cardCashSaleBeen.getId() == null){
			int val=jdbcTemplate.update("insert into card_cash_sale (cardSale,cashSale,chequeSale,date,store_id) values(?,?,?,?,?)",new Object[]{cardCashSaleBeen.getCardSale(),cardCashSaleBeen.getCashSale(),cardCashSaleBeen.getChequeSale(),cardCashSaleBeen.getDate(),userSession.getStoreId()});
			if(val>0)
				result="Card/Cash Sale data Saved successfully";
			}else{
				jdbcTemplate.update("UPDATE card_cash_sale SET cardSale = "+cardCashSaleBeen.getCardSale()+", cashSale = "+cardCashSaleBeen.getCashSale()+", chequeSale = "+cardCashSaleBeen.getChequeSale()+" WHERE id = "+cardCashSaleBeen.getId()+" AND DATE = '"+cardCashSaleBeen.getDate()+"' AND store_id = "+userSession.getStoreId()+"");
				result="Card/Cash Sale data updated successfully";
			}
			
		}catch(Exception e){
			result="Something went wrong";
			log.log(Level.WARN, "saveCardCashSale in PoizonDaoImpl exception", e);
			e.printStackTrace();
		}
		
		return result;
	}
	/**
	 * This method is used to get Single day sale for each products.
	 * */
	@Override
	public SaleBeanAndCashCardExpensesBean getSaleDayWiseReports(String startDate) {
		String QUERY="SELECT p.productId,p.brandNoPackQty,p.realBrandNo,p.brandName,p.productType,p.quantity,p.packQty,p.packType,s.opening,s.received,s.totalReturnBottle,s.`closing`,s.sale,s.unitPrice,s.totalPrice FROM product p INNER JOIN sale s ON p.brandNoPackQty=s.brandNoPackQty AND s.saleDate='"+startDate+"' AND s.`store_id` = "+userSession.getStoreId()+" ORDER BY p.productId ASC";
		SaleBeanAndCashCardExpensesBean saleBeanAndCashCardExpensesBean = new SaleBeanAndCashCardExpensesBean();
		List<SaleBean> saleListBean = new ArrayList<SaleBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					SaleBean editSaleBean = new SaleBean();
					editSaleBean.setProductId(Long.parseLong(row.get("productId").toString()));
					editSaleBean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
					editSaleBean.setBrandNo(Long.parseLong(row.get("realBrandNo").toString()));
					editSaleBean.setBrandName(row.get("brandName").toString());
					editSaleBean.setProductType(row.get("productType").toString());
					editSaleBean.setQuantity(row.get("quantity").toString());
					editSaleBean.setPackQty(Long.parseLong(row.get("packQty").toString()));
					editSaleBean.setPackType(row.get("packType").toString());
					editSaleBean.setOpening(Long.parseLong(row.get("opening").toString()));
					editSaleBean.setReceived(Long.parseLong(row.get("received").toString()));
					editSaleBean.setNoOfReturnsBtl(Long.parseLong(row.get("totalReturnBottle").toString()));
					editSaleBean.setClosing(Long.parseLong(row.get("closing").toString()));
					editSaleBean.setTotalSale(Long.parseLong(row.get("sale").toString()));
					editSaleBean.setUnitPrice(Double.parseDouble(row.get("unitPrice").toString()));
					editSaleBean.setTotalPrice(Double.parseDouble(row.get("totalPrice").toString()));
					saleListBean.add(editSaleBean);
				}
			}
			saleBeanAndCashCardExpensesBean.setSaleBean(saleListBean);
			List<Map<String, Object>> expenserow = jdbcTemplate.queryForList("SELECT `totalAmount`FROM `expense_master` WHERE `expenseMasterDate`='"+startDate+"' AND store_id = "+userSession.getStoreId()+"");
			if(expenserow != null && expenserow.size()>0) {
				for(Map row : expenserow){
					saleBeanAndCashCardExpensesBean.setExpenseAmount(row.get("totalAmount")!=null?Double.parseDouble(row.get("totalAmount").toString()):0);
				  }
				}
			List<Map<String, Object>> cardcashrow = jdbcTemplate.queryForList("SELECT `cardSale`,`cashSale` FROM `card_cash_sale` WHERE `date`='"+startDate+"' AND store_id = "+userSession.getStoreId()+"");
			if(cardcashrow != null && cardcashrow.size()>0) {
				for(Map row : cardcashrow){
					saleBeanAndCashCardExpensesBean.setCardAmount(row.get("cardSale")!=null?Double.parseDouble(row.get("cardSale").toString()):0);
					saleBeanAndCashCardExpensesBean.setCashAmount(row.get("cashSale")!=null?Double.parseDouble(row.get("cashSale").toString()):0);
				  }
				}
		}catch(Exception e){
			e.printStackTrace();
			return saleBeanAndCashCardExpensesBean;
		}
		return saleBeanAndCashCardExpensesBean;
	}
	/*This method is used to get latest cash card date from 'cash_card_tab'*/
	@Override
	public String getCashCardSaleDate() {
		String result = null;
		try {
			result = jdbcTemplate.queryForObject("SELECT DATE_ADD((SELECT `date` FROM `card_cash_sale` WHERE store_id = "+userSession.getStoreId()+" ORDER BY `id` DESC LIMIT 1), INTERVAL 1 DAY)", String.class);
			if(result == null || result == "")
			result = jdbcTemplate.queryForObject("SELECT `invoiceDate` FROM `invoice` WHERE store_id ="+userSession.getStoreId()+" ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1", String.class);
		}catch (Exception e) {
			e.printStackTrace();
		}
		return result;
		//return  
	}
	/**
	 * This method is used to get latest epenses data from 'expense_master' and 'expense_child' to updated latest expenses
	 * */
	@Override
	public UpdateExpensesBean getExpensesData() {
		String QUERY=" SELECT expenseMasterId,`totalAmount`,`expenseMasterDate` FROM expense_master WHERE store_id = "+userSession.getStoreId()+" ORDER BY expenseMasterDate DESC LIMIT 1";
		long expenseMasterId=-1;
		UpdateExpensesBean updateExpensesBean = new UpdateExpensesBean();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
		try{
		if(rows != null && rows.size()>0) {
			for(Map row : rows){
				expenseMasterId=row.get("expenseMasterId")!=null?Long.parseLong(row.get("expenseMasterId").toString()):-1;
				updateExpensesBean.setExpenseMasterId(row.get("expenseMasterId")!=null?Long.parseLong(row.get("expenseMasterId").toString()):-1);
				updateExpensesBean.setTotalAmount(row.get("totalAmount")!=null?Double.parseDouble(row.get("totalAmount").toString()):0);
				updateExpensesBean.setExpenseMasterDate(row.get("expenseMasterDate")!=null?row.get("expenseMasterDate").toString():"");
			}
		}
		if(expenseMasterId!=-1){
			List<UpdateExpenseAndMasterExpense> updateExpenseAndMasterExpenseList = new ArrayList<UpdateExpenseAndMasterExpense>();
			List<Map<String, Object>> updateExpenses = jdbcTemplate.queryForList(" SELECT echild.`expenseChildId`,echild.`categoryId`,echild.`expenseMasterId`,echild.`expenseChildAmount`,echild.comment, ecat.`expenseName` FROM `expense_child`echild INNER JOIN `expense_category`ecat ON `echild`.`categoryId`=`ecat`.`categoryId` WHERE `expenseMasterId`="+expenseMasterId);
			if(updateExpenses != null && updateExpenses.size()>0) {
				for(Map row : updateExpenses){
					UpdateExpenseAndMasterExpense updateExpenseAndMasterExpense = new UpdateExpenseAndMasterExpense();
					updateExpenseAndMasterExpense.setExpenseChildId(Long.parseLong(row.get("expenseChildId").toString()));
					updateExpenseAndMasterExpense.setCategoryId(Long.parseLong(row.get("categoryId").toString()));
					updateExpenseAndMasterExpense.setExpenseMasterId(Long.parseLong(row.get("expenseMasterId").toString()));
					updateExpenseAndMasterExpense.setExpenseChildAmount(Double.parseDouble(row.get("expenseChildAmount").toString()));
					updateExpenseAndMasterExpense.setComment(row.get("comment")!=null?row.get("comment").toString():"");
					updateExpenseAndMasterExpense.setExpenseName(row.get("expenseName")!=null?row.get("expenseName").toString():"");
					updateExpenseAndMasterExpenseList.add(updateExpenseAndMasterExpense);
				}
			}
			updateExpensesBean.setUpdateExpenseAndMasterExpense(updateExpenseAndMasterExpenseList);
		}
		}catch(Exception e){
			e.printStackTrace();
			return updateExpensesBean;
		}
		return updateExpensesBean;
	}
	/**
	 * This method is used to update latest epenses data in 'expense_master' and 'expense_child'.
	 * */
	@Override
	public String updateUpdateExpenses(JSONArray expenseJson) {
		log.info("PoizonDaoImpl.updateUpdateExpenses expenseJson = "+expenseJson);
		String result=null;
		synchronized(this){
			try{
				for (int i = expenseJson.length()-1; i >= 0; --i) {
					JSONObject rec;
					 rec = expenseJson.getJSONObject(i);
					if(i==expenseJson.length()-1){
						int val=jdbcTemplate.update("UPDATE expense_master SET totalAmount="+rec.getDouble("totalAmount")+" WHERE expenseMasterId="+Integer.parseInt(rec.getString("catmasterId")));
							
					}
					else{
						jdbcTemplate.update("UPDATE expense_child SET expenseChildAmount="+Double.parseDouble(rec.getString("amount"))+" ,COMMENT='"+rec.get("comment")+"' WHERE expenseChildId="+Integer.parseInt(rec.getString("expensechild")));
					}
				}
				result="Expenses Updated Successfully";
			}catch(Exception e){
				result="Somethig went wrong";
				log.log(Level.WARN, "updateUpdateExpenses in PoizonDaoImpl exception", e);
				e.printStackTrace();
			}
			}
		return result;
	}
	/*This method is used to get sale from 'sale' tab to update the sale*/
	@Override
	public List<SaleBean> getSingleSaleDetailForUpdate(String brandNoAndPackQty, String date) {
		String QUERY="SELECT `saleId`,`brandNoPackQty`,`opening`,`received`,sale,`closing`,`unitPrice`,`totalPrice`,`saleDate`,totalReturnBottle FROM sale WHERE `brandNoPackQty`='"+brandNoAndPackQty+"' AND `saleDate`>= '"+date+"' AND store_id="+userSession.getStoreId()+"";
		List<SaleBean> saleBeanList = new ArrayList<SaleBean>();
		try{
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
		if(rows != null && rows.size()>0) {
			for(Map row : rows){
				SaleBean bean =new SaleBean();
				bean.setSaleId(Long.parseLong(row.get("saleId").toString()));
				bean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
				bean.setOpening(Long.parseLong(row.get("opening").toString()));
				bean.setReceived(Long.parseLong(row.get("received").toString()));
				bean.setTotalSale(Long.parseLong(row.get("sale").toString()));
				bean.setClosing(Long.parseLong(row.get("closing").toString()));
				bean.setUnitPrice(Double.parseDouble(row.get("unitPrice").toString()));
				bean.setTotalPrice(Double.parseDouble(row.get("totalPrice").toString()));
				bean.setNoOfReturnsBtl(Long.parseLong(row.get("totalReturnBottle").toString()));
				bean.setSaleDate(row.get("saleDate").toString());
				saleBeanList.add(bean);
			}
		}
		
	}catch(Exception e){
	e.printStackTrace();
	return saleBeanList;
	}
		return saleBeanList;
	}
	/**
	 * This method is used to get in-house stock for all product based on selected date.
	 * */
	@Override
	public List<SaleBean> getInHouseStockValue(String startDate) {
		
		int storeId=userSession.getStoreId();
		System.out.println("PoizonDaoImpl.getInHouseStockValue()");
		int size = jdbcTemplate.queryForObject("SELECT COUNT(DISTINCT saleDate) FROM sale WHERE saleDate > '"+startDate+"' AND `store_id`="+userSession.getStoreId()+"",Integer.class);
		List<SaleBean> saleBeanList = new ArrayList<SaleBean>();
		if(size > 0){
			/*
			 * String
			 * QUERY="SELECT p.`productId`,p.`shortBrandName`,p.`realBrandNo`,p.`brandNoPackQty`,p.`packQty`,p.`packType`,p.`productType`,p.`quantity`,"
			 * +
			 * "(SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty = p.brandNoPackQty ORDER BY i.invoiceDateAsPerSheet DESC LIMIT 1) AS saleRate,"
			 * +
			 * "(SELECT t.companyOrder FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyorder,"
			 * +
			 * "(SELECT t.categoryOrder FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryOrder,"
			 * +
			 * "(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,"
			 * +
			 * "(SELECT t.color FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS color,"
			 * +
			 * "(SELECT t.color FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryColor,(SELECT ct.categoryName FROM `category_tab` "
			 * +
			 * " ct WHERE p.category=ct.categoryId) AS categoryname,ROUND(((SELECT i.SingleBottelRate FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty "
			 * + " AND `invoiceDate`<='"
			 * +startDate+"' ORDER BY `invoiceDate` DESC LIMIT 1)*s.`closing`),0) AS total,s.`closing`,TRUNCATE(s.`closing`/p.`packQty`,0) "
			 * +
			 * "AS casestock, (s.`closing`-(p.`packQty`*(TRUNCATE(s.`closing`/p.`packQty`,0)))) AS btls,"
			 * +
			 * "ROUND(((SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty  AND `invoiceDate`<='"
			 * +startDate+"' ORDER BY `invoiceDate`" +
			 * " DESC LIMIT 1)*s.`closing`),0) AS totalPrice," +
			 * "((SELECT COALESCE(SUM(s.closing),0) + (SELECT COALESCE(SUM(`receivedBottles`),0) FROM invoice r WHERE r.invoiceDate > '"
			 * +startDate+"'" +
			 * " AND r.brandNoPackQty = p.`brandNoPackQty`) FROM sale s WHERE s.`saleDate`='"
			 * +startDate+"' AND s.brandNoPackQty = p.`brandNoPackQty`)/(SELECT " +
			 * "TRUNCATE(COALESCE(SUM(s2.`sale`)/14,0),2) FROM sale s2 WHERE s2.`brandNoPackQty`= p.brandNoPackQty AND "
			 * +
			 * "s2.saleDate >=(SELECT '"+startDate+"' -INTERVAL 14 DAY) AND s2.saleDate <='"
			 * +startDate+"')) AS days" +
			 * "  FROM product p INNER JOIN sale s ON (s.`brandNoPackQty`=p.`brandNoPackQty`)  WHERE `saleDate`='"
			 * +startDate+"' GROUP BY p.`brandNoPackQty` ORDER BY p.productId ASC";
			 */
             String QUERY = "SELECT p.`productId`,p.`shortBrandName`,p.`realBrandNo`,p.`brandNoPackQty`,p.`packQty`,p.`packType`,p.`productType`,p.`quantity`,"
             		+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyorder,(SELECT t.categoryOrder FROM `category_tab` t "
             		+ "WHERE p.`category`=t.`categoryId`) AS categoryOrder,(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,"
             		+ "(SELECT t.color FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS color,(SELECT t.color FROM `category_tab` t WHERE p.`category`=t.`categoryId`) "
             		+ "AS categoryColor,(SELECT ct.categoryName FROM `category_tab`  ct WHERE p.category=ct.categoryId) AS categoryname,ROUND(((SELECT i.SingleBottelRate"
             		+ " FROM invoice i WHERE i.store_id=" + storeId + " AND i.brandNoPackQty=p.brandNoPackQty AND `invoiceDate`<='" + startDate + "'"
             		+ " ORDER BY `invoiceDate` DESC LIMIT 1)*s.`closing`),0) AS total,s.`closing`,TRUNCATE(s.`closing`/p.`packQty`,0) " + "AS casestock,"
             		+ " (s.`closing`-(p.`packQty`*(TRUNCATE(s.`closing`/p.`packQty`,0)))) AS btls,ROUND(((SELECT i.bottleSaleMrp FROM invoice i WHERE "
             		+ "i.store_id=" + storeId + " AND i.brandNoPackQty=p.brandNoPackQty  AND `invoiceDate`<='" + startDate + "' "
             		+ "ORDER BY `invoiceDate`" + " DESC LIMIT 1)*s.`closing`),0) AS totalPrice,((SELECT COALESCE(SUM(s.closing),0) + (SELECT COALESCE(SUM(`receivedBottles`),0)"
             		+ " FROM invoice r WHERE r.store_id=" + storeId + " AND r.invoiceDate > '" + startDate + "'" + " AND r.brandNoPackQty = p.`brandNoPackQty`) FROM sale s"
             		+ " WHERE s.store_id=" + storeId + " AND s.`saleDate`='" + startDate + "' AND s.brandNoPackQty = p.`brandNoPackQty`)/(SELECT TRUNCATE(COALESCE(SUM(s2.`sale`)/14,0),2)"
             		+ " FROM sale s2 WHERE s2.store_id=" + storeId + " AND s2.`brandNoPackQty`= p.brandNoPackQty AND s2.saleDate >=(SELECT '" + startDate + "' -INTERVAL 14 DAY) AND "
             		+ "s2.saleDate <='" + startDate + "')) AS days" + "  FROM product p INNER JOIN sale s ON (s.`brandNoPackQty`=p.`brandNoPackQty`)  WHERE s.store_id=" + storeId + " "
             		+ "AND `saleDate`='" + startDate + "' GROUP BY p.`brandNoPackQty` ORDER BY p.productId ASC";

			//System.out.println(QUERY);
			try{
				List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						SaleBean bean =new SaleBean();
						bean.setTarget(row.get("days")!=null?Double.parseDouble(row.get("days").toString()):0);
						bean.setProductId(Long.parseLong(row.get("productId").toString()));
						bean.setBrandName(row.get("shortBrandName").toString());
						bean.setBrandNo(Long.parseLong(row.get("realBrandNo").toString()));
						bean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
						bean.setPackQty(Long.parseLong(row.get("packQty").toString()));
						bean.setPackType(row.get("packType").toString());
						bean.setProductType(row.get("productType").toString());
						bean.setQuantity(row.get("quantity").toString());
						bean.setCompanyOrder(Long.parseLong(row.get("companyorder").toString()));
						bean.setCompany(row.get("companyName").toString());
						bean.setColor(row.get("color").toString());
						bean.setCategory(row.get("categoryname").toString());
						bean.setCaseQty(Long.parseLong(row.get("casestock").toString()));
						bean.setQtyBottels(Long.parseLong(row.get("btls").toString()));
						bean.setCummulativePrice(row.get("totalPrice")!=null?Double.parseDouble(row.get("totalPrice").toString()):0);
						bean.setTotalPrice(row.get("total")!=null?Double.parseDouble(row.get("total").toString()):0);
						bean.setClosing(Long.parseLong(row.get("closing").toString()));
						bean.setCategoryOrder(row.get("categoryOrder")!=null?Long.parseLong(row.get("categoryOrder").toString()):0);
						bean.setSalePrimaryKey(row.get("categoryColor")!=null?row.get("categoryColor").toString():"");
						bean.setOpening((long) (row.get("saleRate")!=null?Double.parseDouble(row.get("saleRate").toString()):0));
						if((Long.parseLong(row.get("closing").toString())) > 0)
							bean.setSingleBottelPrice((double) Math.round(getexactInvoiceValue(1,startDate,Double.parseDouble(row.get("brandNoPackQty").toString()))));
						else 
							bean.setSingleBottelPrice(0.0);
						saleBeanList.add(bean);
					}
				}
				
				}catch(Exception e){
				e.printStackTrace();
				return saleBeanList;
				}
		}else{
		String QUERY="SELECT p.productId,p.`shortBrandName`,p.`realBrandNo`,p.`brandNoPackQty`,p.`packQty`,p.`packType`,p.`productType`,p.`quantity`,"
				+ "(SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty = p.brandNoPackQty AND i.store_id = "+userSession.getStoreId()+" ORDER BY i.invoiceDateAsPerSheet DESC LIMIT 1) AS saleRate,"
				+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyorder,"
				+ "(SELECT t.categoryOrder FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryOrder,"
				+ "(SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName,"
				+ "(SELECT t.color FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS color,(SELECT t.color FROM `category_tab` t WHERE p.`category`=t.`categoryId`)"
				+ " AS categoryColor,(SELECT ct.categoryName FROM `category_tab`  ct WHERE p.category=ct.categoryId) AS categoryname,"
				+ " (SELECT i.SingleBottelRate FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.store_id = "+userSession.getStoreId()+" ORDER BY `invoiceDate` DESC LIMIT 1) AS SingleBottelRate,"
				+ " (SELECT TRUNCATE((COALESCE(SUM(s.closing),0) + (SELECT COALESCE(SUM(`receivedBottles`),0) FROM invoice r WHERE r.invoiceDate > '"+startDate+"'  AND r.brandNoPackQty "
				+ "= p.`brandNoPackQty` AND r.store_id = "+userSession.getStoreId()+"))/p.`packQty`,0) FROM sale s WHERE s.`saleDate`='"+startDate+"' AND s.brandNoPackQty = p.`brandNoPackQty` AND s.store_id = "+userSession.getStoreId()+") AS casestock,"
				+ " (SELECT COALESCE(SUM(s.closing),0) + (SELECT COALESCE(SUM(`receivedBottles`),0) FROM invoice r WHERE r.invoiceDate > '"+startDate+"' "
				+ "AND r.brandNoPackQty = p.`brandNoPackQty` AND r.store_id = "+userSession.getStoreId()+") FROM sale s WHERE s.`saleDate`='"+startDate+"' AND s.brandNoPackQty = p.`brandNoPackQty` AND s.store_id = "+userSession.getStoreId()+") AS closing, "
				+ " (SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty AND i.store_id = "+userSession.getStoreId()+""
				+ " ORDER BY `invoiceDate` DESC LIMIT 1) AS bottleSaleMrp,"
				+ "((SELECT COALESCE(SUM(s.closing),0) +  (SELECT COALESCE(SUM(`receivedBottles`),0) FROM invoice r WHERE r.invoiceDate > '"+startDate+"' "
				+ "AND r.brandNoPackQty = p.`brandNoPackQty` AND r.store_id = "+userSession.getStoreId()+") FROM sale s WHERE s.`saleDate`='"+startDate+"' AND s.brandNoPackQty = p.`brandNoPackQty` AND s.store_id = "+userSession.getStoreId()+")/(SELECT "
				+ "TRUNCATE(COALESCE(SUM(s2.`sale`)/14,0),2) FROM sale s2 WHERE s2.`brandNoPackQty`= p.brandNoPackQty "
				+ "AND s2.saleDate >=(SELECT '"+startDate+"' -INTERVAL 14 DAY) AND s2.saleDate <='"+startDate+"' AND s2.store_id = "+userSession.getStoreId()+")) AS days"
				+ " FROM product p GROUP BY p.`brandNoPackQty`"
				+ " ORDER BY p.productId ASC";
		//System.out.println(QUERY);
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					long closing  = row.get("closing")!=null?Long.parseLong(row.get("closing").toString()):0;
					long caseStock  = row.get("casestock")!=null?Long.parseLong(row.get("casestock").toString()):0;
					long btls = closing - ((row.get("packQty")!=null?Long.parseLong(row.get("packQty").toString()):0) * caseStock);
					double btlSaleMrp = row.get("bottleSaleMrp")!=null?Double.parseDouble(row.get("bottleSaleMrp").toString()):0;
					double singleBtlRate = row.get("SingleBottelRate")!=null?Double.parseDouble(row.get("SingleBottelRate").toString()):0;
					SaleBean bean =new SaleBean();
					bean.setTarget(row.get("days")!=null?Double.parseDouble(row.get("days").toString()):0);
					bean.setProductId(Long.parseLong(row.get("productId").toString()));
					bean.setBrandName(row.get("shortBrandName").toString());
					bean.setBrandNo(Long.parseLong(row.get("realBrandNo").toString()));
					bean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
					bean.setPackQty(Long.parseLong(row.get("packQty").toString()));
					bean.setPackType(row.get("packType").toString());
					bean.setProductType(row.get("productType").toString());
					bean.setQuantity(row.get("quantity").toString());
					bean.setCompanyOrder(Long.parseLong(row.get("companyorder").toString()));
					bean.setCompany(row.get("companyName").toString());
					bean.setColor(row.get("color").toString());
					bean.setCategory(row.get("categoryname").toString());
					bean.setCaseQty(caseStock);
					bean.setQtyBottels(btls);
					bean.setCummulativePrice((double) Math.round(closing * btlSaleMrp));
					bean.setTotalPrice((double) Math.round(closing * singleBtlRate));
					bean.setClosing(closing);
					if(closing > 0)
						bean.setSingleBottelPrice((double) Math.round(getexactInvoiceValue(2,startDate,Double.parseDouble(row.get("brandNoPackQty").toString()))));
					else 
						bean.setSingleBottelPrice(0.0);
					bean.setCategoryOrder(row.get("categoryOrder")!=null?Long.parseLong(row.get("categoryOrder").toString()):0);
					bean.setSalePrimaryKey(row.get("categoryColor")!=null?row.get("categoryColor").toString():"");
					bean.setOpening((long) (row.get("saleRate")!=null?Double.parseDouble(row.get("saleRate").toString()):0));
					saleBeanList.add(bean);
				}
			}
			
			}catch(Exception e){
			e.printStackTrace();
			return saleBeanList;
			}
		}
		return saleBeanList;
	}
	private double getexactInvoiceValue(int num,String startDate, double brandNoPackQty) {
		double exactInvoice=0;long totalSale =0;
		try{
			 totalSale = jdbcTemplate.queryForObject("SELECT COALESCE(SUM(s.sale),0) AS sale FROM sale s WHERE s.`brandNoPackQty`="+brandNoPackQty+" AND s.`saleDate` <='"+startDate+"' AND s.`store_id`="+userSession.getStoreId()+"",Long.class);
		}catch(Exception e){e.printStackTrace();}
			long stockVal =0,cnt=0;
		List<TurnovertaxBean> beanList = getInvoiceList(num,startDate,brandNoPackQty);
		for(TurnovertaxBean bean : beanList){
			stockVal = stockVal + bean.getStock();
     		if(stockVal >= totalSale){
     			if(cnt == 0){
					long val = stockVal - totalSale;
					exactInvoice += val * bean.getInvoiceRate();
					cnt++;
				}else{
				exactInvoice += bean.getStock() * bean.getInvoiceRate();
				cnt++;
				}	
     		}	
			
		}
		return exactInvoice;
	}
	private List<TurnovertaxBean> getInvoiceList(int num, String startDate, double brandNoPackQty) {
		List<TurnovertaxBean> beanList = new ArrayList<TurnovertaxBean>();
		String QUERY = null;
		if(num == 1){
			QUERY = "SELECT p.`realBrandNo`,p.`brandNoPackQty`, i.`receivedBottles`, i.`invoiceDateAsPerSheet`,i.`SingleBottelRate` "
					+ "FROM product p INNER JOIN invoice i  ON p.`brandNoPackQty`=i.`brandNoPackQty`  WHERE i.`receivedBottles` > 0 AND i.`invoiceDateAsPerSheet` <= '"+startDate+"' AND p.`brandNoPackQty`="+brandNoPackQty+" AND i.`store_id`="+userSession.getStoreId()+"  GROUP BY i.`invoiceDateAsPerSheet`";
		}else{
			QUERY = "SELECT p.`realBrandNo`,p.`brandNoPackQty`, i.`receivedBottles`, i.`invoiceDateAsPerSheet`,i.`SingleBottelRate` "
					+ "FROM product p INNER JOIN invoice i  ON p.`brandNoPackQty`=i.`brandNoPackQty`  WHERE i.`receivedBottles` > 0  AND p.`brandNoPackQty`="+brandNoPackQty+" AND i.`store_id`="+userSession.getStoreId()+"  GROUP BY i.`invoiceDateAsPerSheet`  ";
			 
		}
		 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					TurnovertaxBean bean = new TurnovertaxBean();
					String invoicedate = row.get("invoiceDateAsPerSheet")!=null?row.get("invoiceDateAsPerSheet").toString():"";
					long received = row.get("receivedBottles")!=null?Long.parseLong(row.get("receivedBottles").toString()):0;
					double price = row.get("SingleBottelRate")!=null?Double.parseDouble(row.get("SingleBottelRate").toString()):0;
					//(sdf.parse(Inv_qty.getInvoiceRealDate()).after(sdf.parse(discountDate1)) && sdf.parse(Inv_qty.getInvoiceRealDate()).before(sdf.parse(discountDate4))) || (sdf.parse(Inv_qty.getInvoiceRealDate()).after(sdf.parse(discountDate5)) && sdf.parse(Inv_qty.getInvoiceRealDate()).before(sdf.parse(discountDate6)))
					
					//if((sdf.parse(invoicedate).after(sdf.parse(discountDate1)) && sdf.parse(invoicedate).before(sdf.parse(discountDate4))) || sdf.parse(invoicedate).after(sdf.parse(discountDate5)))
       			    if((sdf.parse(invoicedate).after(sdf.parse(discountDate1)) && sdf.parse(invoicedate).before(sdf.parse(discountDate4))) || (sdf.parse(invoicedate).after(sdf.parse(discountDate5)) && sdf.parse(invoicedate).before(sdf.parse(discountDate6)))) 
					{
						bean.setDate(invoicedate);
						bean.setInvoiceRate(price + (price * 14.5)/100);
						bean.setStock(received);
						beanList.add(bean);
       			      } 
       			 else{
       				bean.setDate(invoicedate);
					bean.setInvoiceRate(price);
					bean.setStock(received);
					beanList.add(bean);
       			  }
				}
			}
		   }catch(Exception e){
			e.printStackTrace();
			return beanList;
			}
		return beanList;
	}
	
	//new implementation
	/**
	 * This method is used to generate balance based on selected dates
	 * Getting sale, card,cash & checque amt, expenses details, bank credited amt and retention.
	 * */
	@Override
	public BalanceSheetWithBreckage getBalanceSheet(String sDate,String eDate) {
		BalanceSheetWithBreckage balanceSheetWithBreckage = new BalanceSheetWithBreckage();
		String QUERY="SELECT s.saleDate FROM sale s WHERE s.`saleDate`>='"+sDate+"' AND s.`saleDate`<='"+eDate+"' AND s.store_id="+userSession.getStoreId()+" UNION SELECT creditDate FROM bank_credit_tab t WHERE t.`creditDate`>='"+sDate+"' AND t.`creditDate`<='"+eDate+"' AND t.store_id="+userSession.getStoreId()+" ORDER BY saleDate";
		List<BalanceSheetBean> balanceSheetList = new ArrayList<BalanceSheetBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					//System.out.println(row.get("saleDate")+"khagesh");
					List<Map<String, Object>> balancerows = jdbcTemplate.queryForList("SELECT SUM(totalPrice) AS totalPrice,saleDate,"
							+ "(SELECT `totalAmount` FROM `expense_master` WHERE `expenseMasterDate`='"+row.get("saleDate")+"' AND store_id = "+userSession.getStoreId()+") AS totalAmount,"
							+ "(SELECT `expenseMasterId` FROM `expense_master` WHERE `expenseMasterDate`='"+row.get("saleDate")+"' AND store_id = "+userSession.getStoreId()+") AS expenseMasterId,"
							+ "(SELECT `cardSale` FROM `card_cash_sale` WHERE `date`='"+row.get("saleDate")+"' AND store_id = "+userSession.getStoreId()+") AS cardSale,"
							+ "(SELECT `cashSale` FROM `card_cash_sale` WHERE `date`='"+row.get("saleDate")+"' AND store_id = "+userSession.getStoreId()+") AS cashSale,"
							+ "(SELECT `chequeSale` FROM `card_cash_sale` WHERE `date`='"+row.get("saleDate")+"' AND store_id = "+userSession.getStoreId()+") AS chequeSale,"
						    + "(SELECT `creditAmount` FROM `bank_credit_tab` WHERE `creditDate`='"+row.get("saleDate")+"' AND store_id ="+userSession.getStoreId()+") AS creditamount,"
						    + "(SELECT SUM(amount) FROM investment WHERE date ='"+row.get("saleDate")+"' AND store_id = "+userSession.getStoreId()+") AS investment,"
						    + "(SELECT `retention` FROM `bank_credit_tab` WHERE `creditDate`='"+row.get("saleDate")+"' AND store_id = "+userSession.getStoreId()+") AS retention"
						    + " FROM sale WHERE `saleDate`='"+row.get("saleDate")+"' AND store_id = "+userSession.getStoreId());
					if(balancerows != null && balancerows.size()>0) {
						for(Map balancerow : balancerows){
							BalanceSheetBean bean = new BalanceSheetBean();
							bean.setDate(row.get("saleDate")!=null?row.get("saleDate").toString():"");
							bean.setTotalPrice(balancerow.get("totalPrice")!=null?Double.parseDouble(balancerow.get("totalPrice").toString()):0);
							bean.setTotalAmount(balancerow.get("totalAmount")!=null?Double.parseDouble(balancerow.get("totalAmount").toString()):0);
							bean.setExpenseMasterId(balancerow.get("expenseMasterId")!=null?Long.parseLong(balancerow.get("expenseMasterId").toString()):0);
							bean.setCradSale(balancerow.get("cardSale")!=null?Double.parseDouble(balancerow.get("cardSale").toString()):0);
							bean.setCashSale(balancerow.get("cashSale")!=null?Double.parseDouble(balancerow.get("cashSale").toString()):0);
							bean.setChequeSale(balancerow.get("chequeSale")!=null?Double.parseDouble(balancerow.get("chequeSale").toString()):0);
							bean.setCreditedAmount(balancerow.get("creditamount")!=null?Double.parseDouble(balancerow.get("creditamount").toString()):0);
							bean.setRetention(balancerow.get("retention")!=null?Double.parseDouble(balancerow.get("retention").toString()):0);
							bean.setInvestment(balancerow.get("investment")!=null?Double.parseDouble(balancerow.get("investment").toString()):0);
							balanceSheetList.add(bean);
						}
					}
				}
			}
			balanceSheetWithBreckage.setBalanceSheetBean(balanceSheetList);
			balanceSheetWithBreckage.setData(getBreckageAmount(sDate,eDate));
			
		}catch(Exception e){
		e.printStackTrace();
		return balanceSheetWithBreckage;
		}
			return balanceSheetWithBreckage;
		}
		
	private data getBreckageAmount(String sDate, String eDate) {
		data dataval = new data(); 
		String QUERY="SELECT SUM(breckageAmount) AS breckage FROM breckage_tab WHERE "
				+ "breckageDate>=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+sDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01')) AND breckageDate<='"+eDate+"' AND `store_id`="+userSession.getStoreId()+"";
		try{
			 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			 if(rows != null && rows.size()>0) {
					for(Map row : rows){
						dataval.setValue(row.get("breckage")!=null?Double.parseDouble(row.get("breckage").toString()):0);
					}
			 }
		}catch(Exception e){
			e.printStackTrace();
			return dataval;
		}
        
		return dataval;
	}
	/*This method is used for Edit the bottle mrp*/
	@Override
	public List<SaleBean> getDetailsforEditMrp() {
		String QUERY="SELECT DISTINCT p.productId,p.brandNoPackQty,p.brandNo,p.brandName,p.productType,p.quantity,p.packQty,p.packType,(SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty ORDER BY `invoiceId` DESC LIMIT 1) AS `bottleSaleMrp`,(SELECT i.`SingleBottelRate` FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty ORDER BY `invoiceId` DESC LIMIT 1) AS `SingleBottelRate`,(SELECT i.`packQtyRate` FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty ORDER BY `invoiceId` DESC LIMIT 1) AS `packQtyRate` FROM product p INNER JOIN invoice i ON p.brandNoPackQty=i.brandNoPackQty ORDER BY (SELECT t.categoryOrder FROM category_tab t WHERE t.categoryId = p.category) ASC, p.`shortBrandName`,p.`packType` DESC";
		List<SaleBean> saleBeanList=new ArrayList<SaleBean>();
		
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					SaleBean bean= new SaleBean();
					bean.setProductId(Long.parseLong(row.get("productId").toString()));
					bean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
					bean.setBrandNo(Long.parseLong(row.get("brandNo").toString()));
					bean.setBrandName(row.get("brandName").toString());
					bean.setProductType(row.get("productType").toString());
					bean.setQuantity(row.get("quantity").toString());
					bean.setPackQty(Long.parseLong(row.get("packQty").toString()));
					bean.setPackType(row.get("packType").toString());
					bean.setBottleSaleMrp(row.get("bottleSaleMrp")!=null?Double.parseDouble(row.get("bottleSaleMrp").toString()):0);
					bean.setSingleBottelPrice(row.get("SingleBottelRate")!=null?Double.parseDouble(row.get("SingleBottelRate").toString()):0);
					bean.setUnitPrice(row.get("packQtyRate")!=null?Double.parseDouble(row.get("packQtyRate").toString()):0);
					saleBeanList.add(bean);
				}
			}
			
		}catch(Exception e){
		e.printStackTrace();
		return saleBeanList;
		}
			return saleBeanList;
		}
	/*Old method not used any where*/
	@Override
	public List<SaleBean> getProductComparision(String brandNoPackQtys, String startdate,String enddate) {
		String QUERY = "SELECT p.`brandNoPackQty`,p.`brandNo`, p.`brandName`,p.`quantity`,(SELECT SUM(s.`sale`) FROM sale s WHERE s.`brandNoPackQty`=p.`brandNoPackQty` AND s.saleDate>='"+startdate+"' AND s.saleDate<='"+enddate+"') AS totalsale FROM product p WHERE p.`brandNoPackQty` IN("+brandNoPackQtys+")";
		//System.out.println("QUERY>>> "+QUERY);
		List<SaleBean> compairList = new ArrayList<SaleBean>();
		try{
			List<Map<String, Object>> firstrowdata = jdbcTemplate.queryForList(QUERY);
			if(firstrowdata != null && firstrowdata.size()>0) {
				for(Map row : firstrowdata){
					SaleBean bean = new SaleBean();
					bean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
					bean.setBrandNo(Long.parseLong(row.get("brandNo").toString()));
					bean.setBrandName(row.get("brandName").toString());
					bean.setQuantity(row.get("quantity").toString());
					bean.setTotalSale(Long.parseLong(row.get("totalsale").toString()));
					compairList.add(bean);
				}
			}
			
		}catch(Exception e){
		e.printStackTrace();
		return compairList;
		}
		
		return compairList;
	}
	/*Old method not used any where*/
	@Override
	public List<NewSaleBean> getNewSaleReports(String newSdate, String newEdate) {
		//System.out.println("khagesh>>>"+"newSdate>> "+newSdate+"  newEdate>>> "+newEdate);
		String QUERY="SELECT DISTINCT p1.brandNo, p1.`brandName`,(SELECT SUM(s.sale) FROM sale s , product p WHERE s.saleDate>='"+newSdate+"' AND s.saleDate <='"+newEdate+"' AND s.brandNoPackQty=p1.brandNoPackQty AND p.brandNoPackQty=p1.brandNoPackQty  AND p.quantity='2000 ml') AS 2000ml,(SELECT SUM(s.sale) FROM sale s , product p WHERE s.saleDate>='"+newSdate+"' AND s.saleDate <='"+newEdate+"' AND s.brandNoPackQty=p1.brandNoPackQty AND p.brandNoPackQty=p1.brandNoPackQty  AND p.quantity='1000 ml') AS 1000ml,(SELECT SUM(s.sale) FROM sale s , product p WHERE s.saleDate>='"+newSdate+"' AND s.saleDate <='"+newEdate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.quantity='700 ml') AS 700ml,(SELECT SUM(s.sale) FROM sale s , product p WHERE s.saleDate>='"+newSdate+"' AND s.saleDate <='"+newEdate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.quantity='750 ml') AS 750ml,(SELECT SUM(s.sale) FROM sale s , product p WHERE s.saleDate>='"+newSdate+"' AND s.saleDate <='"+newEdate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.quantity='650 ml') AS 650ml,(SELECT SUM(s.sale) FROM sale s , product p WHERE s.saleDate>='"+newSdate+"' AND s.saleDate <='"+newEdate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.quantity='500 ml') AS 500ml,(SELECT SUM(s.sale) FROM sale s , product p WHERE s.saleDate>='"+newSdate+"' AND s.saleDate <='"+newEdate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.quantity='375 ml') AS 375ml,(SELECT SUM(s.sale) FROM sale s , product p WHERE s.saleDate>='"+newSdate+"' AND s.saleDate <='"+newEdate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.quantity='335 ml') AS 335ml,(SELECT SUM(s.sale) FROM sale s , product p WHERE s.saleDate>='"+newSdate+"' AND s.saleDate <='"+newEdate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.quantity='330 ml') AS 330ml,(SELECT SUM(s.sale) FROM sale s , product p WHERE s.saleDate>='"+newSdate+"' AND s.saleDate <='"+newEdate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.quantity='180 ml') AS 180ml,(SELECT SUM(s.sale) FROM sale s , product p WHERE s.saleDate>='"+newSdate+"' AND s.saleDate <='"+newEdate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.quantity='90 ml') AS 90ml,(SELECT SUM(s.sale) FROM sale s , product p WHERE s.saleDate>='"+newSdate+"' AND s.saleDate <='"+newEdate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.quantity='60 ml') AS 60ml FROM product p1 GROUP BY p1.brandNo";
		List<NewSaleBean> newSaleBeenList = new ArrayList<NewSaleBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					//System.out.println(row.toString());
					NewSaleBean bean = new NewSaleBean();
					bean.setBrandNo(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0);
					bean.setBrandName(row.get("brandName")!=null?row.get("brandName").toString():"");
					bean.setTwoThousandML(row.get("2000ml")!=null?Long.parseLong(row.get("2000ml").toString()):0);
					bean.setOneThousandML(row.get("1000ml")!=null?Long.parseLong(row.get("1000ml").toString()):0);
					bean.setSevenFiftyML(row.get("750ml")!=null?Long.parseLong(row.get("750ml").toString()):0);
					bean.setSevenhundredML(row.get("700ml")!=null?Long.parseLong(row.get("700ml").toString()):0);
					bean.setSixFiftyML(row.get("650ml")!=null?Long.parseLong(row.get("650ml").toString()):0);
					bean.setFiveHundredML(row.get("500ml")!=null?Long.parseLong(row.get("500ml").toString()):0);
					bean.setThreeSeventyFiveML(row.get("375ml")!=null?Long.parseLong(row.get("375ml").toString()):0);
					bean.setThreeThirtyFiveML(row.get("335ml")!=null?Long.parseLong(row.get("335ml").toString()):0);
					bean.setThreeThirtyML(row.get("300ml")!=null?Long.parseLong(row.get("300ml").toString()):0);
					bean.setOneEightyML(row.get("180ml")!=null?Long.parseLong(row.get("180ml").toString()):0);
					bean.setNightyML(row.get("90ml")!=null?Long.parseLong(row.get("90ml").toString()):0);
					bean.setSixtyML(row.get("60ml")!=null?Long.parseLong(row.get("60ml").toString()):0);
					newSaleBeenList.add(bean);
					
					
				}
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			return newSaleBeenList;
		}
		
		return newSaleBeenList;
	}
	/*Old method not used any where*/
	@Override
	public List<SaleBean> getStockLiftGraphData(String startDate, String endDate) {
	String QUERY="SELECT DISTINCT  p.brandNoPackQty,p.brandNo,p.brandName,p.productType,p.quantity,p.packQty,p.packType,p.category,(SELECT t.categoryName FROM `category_tab` t WHERE p.`category`=t.`categoryId`) AS categoryName,p.company, (SELECT t.companyName FROM `company_tab` t WHERE p.`company`=t.`companyId`) AS companyName, SUM(i.caseQty) AS caseQty FROM product p INNER JOIN invoice i  ON p.`brandNoPackQty` = i.`brandNoPackQty` AND `invoiceDateAsPerSheet`>='"+startDate+"' AND `invoiceDateAsPerSheet`<='"+endDate+"' AND p.category IN(1,2) GROUP BY brandNoPackQty ORDER BY p.brandName ASC";
	List<SaleBean> graphList = new ArrayList<SaleBean>();
	try{
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
		if(rows != null && rows.size()>0) {
			for(Map row : rows){
				SaleBean bean = new SaleBean();
				bean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
				bean.setBrandNo(Long.parseLong(row.get("brandNo").toString()));
				bean.setBrandName(row.get("brandName").toString());
				bean.setProductType(row.get("productType").toString());
				bean.setQuantity(row.get("quantity").toString());
				bean.setPackQty(Long.parseLong(row.get("packQty").toString()));
				bean.setPackType(row.get("packType").toString());
				bean.setCategory(row.get("categoryName").toString());
				bean.setCompany(row.get("companyName").toString());
				bean.setCaseQty(Long.parseLong(row.get("caseQty").toString()));
				graphList.add(bean);
			}
		}
		
		
	}catch(Exception e){
		e.printStackTrace();
		return graphList;
	}
	
	return graphList;
	}
	/*@Override
	public List<SaleBean> getSameRangeProducts(int unitPrice,String category,String qty) {
		int unitPriceSec= unitPrice+300;
		String QUERY="SELECT DISTINCT p.brandNoPackQty, p.brandNo,p.brandName,p.quantity,p.category,(SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty ORDER BY `invoiceId` DESC LIMIT 1) AS `unitPrice` FROM product p INNER JOIN invoice i  ON p.`brandNoPackQty` = i.`brandNoPackQty` AND p.category='"+category+"' AND p.`quantity`='"+qty+"' AND (SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty ORDER BY `invoiceId` DESC LIMIT 1) >= "+unitPrice+"  AND (SELECT i.bottleSaleMrp FROM invoice i WHERE i.brandNoPackQty=p.brandNoPackQty ORDER BY `invoiceId` DESC LIMIT 1) <= "+unitPriceSec+"";
		//System.out.println(QUERY);
		List<SaleBean> sameProductList = new ArrayList<SaleBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					SaleBean bean = new SaleBean();
					bean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
					bean.setBrandNo(Long.parseLong(row.get("brandNo").toString()));
					bean.setBrandName(row.get("brandName").toString());
					bean.setQuantity(row.get("quantity").toString());
					bean.setCategory(row.get("category").toString());
					bean.setUnitPrice(Double.parseDouble(row.get("unitPrice").toString()));
					sameProductList.add(bean);
				}
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			return sameProductList;
		}
		
		return sameProductList;
	}*/
	/*Old method not used any where*/
	@Override
	public List<SaleBean> getSameRangeProducts(int matchval,String qty) {
		String QUERY="SELECT DISTINCT p.brandNoPackQty, p.brandNo,p.brandName,p.quantity,p.category FROM product p WHERE p.`matchs`="+matchval+" AND p.`quantity`='"+qty+"'";
		//System.out.println(QUERY);
		List<SaleBean> sameProductList = new ArrayList<SaleBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					SaleBean bean = new SaleBean();
					bean.setBrandNoPackQty(Double.parseDouble(row.get("brandNoPackQty").toString()));
					bean.setBrandNo(Long.parseLong(row.get("brandNo").toString()));
					bean.setBrandName(row.get("brandName").toString());
					bean.setQuantity(row.get("quantity").toString());
					bean.setCategory(row.get("category").toString());
					sameProductList.add(bean);
				}
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
			return sameProductList;
		}
		
		return sameProductList;
	}
	/* This method is used to get expense details for a day*/
	@Override
	public List<ExpenseBean> getExpenseDetails(String expenseMasterID) {
		String QUERY="SELECT c.`expenseChildId`,c.`categoryId`,ec.`expenseName`,c.`expenseMasterId`,c.`expenseChildAmount`,c.`comment` FROM `expense_child` c INNER JOIN `expense_category` ec ON c.`categoryId`=ec.`categoryId` WHERE c.`expenseMasterId`="+Integer.parseInt(expenseMasterID)+"";
		//System.out.println(QUERY);
		List<ExpenseBean> expenseList = new ArrayList<ExpenseBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					ExpenseBean bean = new ExpenseBean();
					bean.setName(row.get("expenseName").toString());
					bean.setExpenseChildAmount(Double.parseDouble(row.get("expenseChildAmount").toString()));
					bean.setComment(row.get("comment")!=null?row.get("comment").toString():"");
					expenseList.add(bean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return expenseList;
		}
		return expenseList;
	}
	/**
	 * Old method not used any where
	 * */
	@Override
	public DiscountEstimationWithDate getDiscountEsitmateData() {
		DiscountEstimationWithDate discountEstimationWithDate = new DiscountEstimationWithDate();
		String startDate=null;
		String endDate=null;
		List<DiscountEstimationBean> estimationBeanList = new ArrayList<DiscountEstimationBean>();
		try{
			 List<Map<String, Object>> dateRow = jdbcTemplate.queryForList("SELECT `saleDate`,UPPER(DATE_FORMAT(saleDate, '%b' ' %y')) AS monthAndYear FROM sale ORDER BY `saleId` DESC LIMIT 1");
			 if(dateRow != null && dateRow.size()>0) {
					for(Map row : dateRow){
						endDate=row.get("saleDate")!=null?row.get("saleDate").toString():"";
						discountEstimationWithDate.setDate(row.get("monthAndYear")!=null?row.get("monthAndYear").toString():"");
					}
			 }
			 if(endDate != null && endDate !="" ){
			    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			    Date date = null;
				try {
					date = dateFormat.parse(endDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}
				Calendar cal = new GregorianCalendar();
				cal.setTime(date);
				cal.add(Calendar.DAY_OF_MONTH, -30);
				Date today60 = cal.getTime();
				startDate = dateFormat.format(today60);
			 }
		System.out.println("startDate>>>"+startDate+"  endDate>>>"+endDate);
		String QUERY="SELECT DISTINCT p1.brandNo, p1.`shortBrandName`,p1.`category`,"
				+ "(SELECT d.target FROM stock_lift_with_discount d WHERE d.brandNo=p1.brandNo "
				+ "AND d.stockDiscountDate=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+endDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))) AS target,"
				+ "(SELECT d.targetCase FROM discount d WHERE d.brandNo=p1.brandNo AND d.estimateDate=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+endDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))) AS commitment,"
				+ "(SELECT t.color FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS color,"
				+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyOrder,"
				+ "(SELECT t.categoryName FROM `category_tab` t WHERE p1.`category`=t.`categoryId`)"
				+ " AS categoryName,p1.`company`,(SELECT t.companyName FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyName,"
				+ "ROUND(AVG((SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p1.brandNoPackQty ORDER BY `invoiceId` DESC LIMIT 1)),0) AS casePrice,"
				+ "(SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE   s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty"
				+ " AND p.brandNo=p1.brandNo  AND p.packType='2L') AS 2LStock,(SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE "
				+ " s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='1L') AS 1LStock,"
				+ "(SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE   s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty "
				+ "AND p.brandNo=p1.brandNo  AND p.packType='Q') AS QStock, (SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE"
				+ " s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='P') AS PStock,"
				+ "(SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE   s.saleDate ='"+endDate+"'"
				+ " AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='N') AS NStock,"
				+ " (SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE   s.saleDate ='"+endDate+"'"
				+ "  AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='D') AS DStock, (SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2)"
				+ " FROM sale s , product p WHERE   s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='SB') "
				+ "AS SBStock,(SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE   s.saleDate ='"+endDate+"'"
				+ " AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='LB') AS LBStock,"
				+ " (SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE   s.saleDate ='"+endDate+"'  AND s.brandNoPackQty=p.brandNoPackQty"
				+ " AND p.brandNo=p1.brandNo  AND p.packType='TIN') AS TINStock, (SELECT TRUNCATE(SUM(s.`closing`)/p.packQty,2) FROM sale s , product p WHERE"
				+ " s.saleDate ='"+endDate+"'  AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='X') AS XStock,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"'"
				+ " AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='2L') AS 2L,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='1L') AS 1L,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo AND p.packType='Q') AS Q,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='P') AS P,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='N') AS N,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='D') AS D,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='SB') AS SB,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='LB') AS LB,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='TIN') AS TIN,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+startDate+"' AND s.saleDate <='"+endDate+"'"
				+ "  AND s.brandNoPackQty=p.brandNoPackQty AND p.brandNo=p1.brandNo  AND p.packType='X') AS X FROM product p1 GROUP BY p1.brandNo ORDER BY p1.brandNo ASC";
		//System.out.println(QUERY);
		if(startDate != "" && startDate != null && endDate != "" && endDate !=null){
			
				List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						DiscountEstimationBean bean = new DiscountEstimationBean();
						bean.setBrandNo(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0);
						bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
						bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
						bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
						bean.setTarget(row.get("target")!=null?Long.parseLong(row.get("target").toString()):0);
						bean.setCommitment(row.get("commitment")!=null?Long.parseLong(row.get("commitment").toString()):0);
						StockInBean stockInBean = getLastMonthSoldData(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0, startDate, endDate);
						bean.setLastMonthSold(stockInBean.getTotalPrice());
						bean.setInHouseStock(stockInBean.getStockInId());
						bean.setLiftedCase(stockInBean.getQtyBtl());
						bean.setCaseRate(row.get("casePrice")!=null?Double.parseDouble(row.get("casePrice").toString()):0);
						bean.setCompanyColor(row.get("color")!=null?row.get("color").toString():"");
						bean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
						
						bean.setL2Stock(row.get("2LStock")!=null?Double.parseDouble(row.get("2LStock").toString()):0);
						bean.setL1Stock(row.get("1LStock")!=null?Double.parseDouble(row.get("1LStock").toString()):0);
						bean.setQStock(row.get("QStock")!=null?Double.parseDouble(row.get("QStock").toString()):0);
						bean.setPStock(row.get("PStock")!=null?Double.parseDouble(row.get("PStock").toString()):0);
						bean.setNStock(row.get("NStock")!=null?Double.parseDouble(row.get("NStock").toString()):0);
						bean.setDStock(row.get("DStock")!=null?Double.parseDouble(row.get("DStock").toString()):0);
						bean.setSBStock(row.get("SBStock")!=null?Double.parseDouble(row.get("SBStock").toString()):0);
						bean.setLBStock(row.get("LBStock")!=null?Double.parseDouble(row.get("LBStock").toString()):0);
						bean.setTINStock(row.get("TINStock")!=null?Double.parseDouble(row.get("TINStock").toString()):0);
						bean.setXStock(row.get("XStock")!=null?Double.parseDouble(row.get("XStock").toString()):0);
						
						bean.setL2(row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0);
						bean.setL1(row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0);
						bean.setQ(row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0);
						bean.setP(row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0);
						bean.setN(row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0);
						bean.setSB(row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0);
						bean.setD(row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0);
						bean.setLB(row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0);
						bean.setTIN(row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0);
						bean.setX(row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0);
						bean.setPending(stockInBean.getQtyBtl());
						estimationBeanList.add(bean);
						
					}
				}
			}
		discountEstimationWithDate.setDiscountEstimationBean(estimationBeanList);
		}catch(Exception e){
				e.printStackTrace();
				return discountEstimationWithDate;
			}
		
		// TODO Auto-generated method stub
		return discountEstimationWithDate;
	}

	private StockInBean getLastMonthSoldData(long brandNo,String startDate,String endDate) {
		StockInBean bean = new StockInBean();
		String QUERY="SELECT ROUND(SUM(s.sale/p.packQty),0) AS sale"
				+ " FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`>='"+startDate+"'"
				+ "AND `saleDate`<='"+endDate+"' AND brandNo="+brandNo+"";
		String QUERY2="SELECT ROUND(SUM(s.closing/p.packQty),0) AS inHouseStock  "
				+ "FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`='"+endDate+"' AND brandNo="+brandNo+"";
		String QUERY3="SELECT COALESCE(ROUND(CEIL(SUM(s.receivedBottles/p.packQty))),0) AS liftedCase "
				+ "FROM product p INNER JOIN invoice s ON p.`brandNoPackQty` = s.`brandNoPackQty`"
				+ " AND `invoiceDateAsPerSheet`>=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+endDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01')) AND "
				+ " invoiceDateAsPerSheet <= (SELECT LAST_DAY('"+endDate+"')) AND p.brandNo="+brandNo+"";
		try{
			// cases = jdbcTemplate.queryForObject(QUERY, Double.class);
			 bean.setTotalPrice(jdbcTemplate.queryForObject(QUERY, Double.class));
			 bean.setStockInId(jdbcTemplate.queryForObject(QUERY2, Double.class));
			 bean.setQtyBtl(jdbcTemplate.queryForObject(QUERY3, Double.class));
			}catch(Exception e){
				e.printStackTrace();
				return bean;
			}
		return bean;
	}
	/*Old method not used any where*/
	@Override
	public List<ChartDataBean> getBeerCasesGraphData(String sdate, String edate) {
		 Random r = new Random();
		List<ChartDataBean> chartDataBeanList = new ArrayList<ChartDataBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT DISTINCT `saleDate` FROM sale WHERE saleDate>='"+sdate+"' AND saleDate <='"+edate+"' ");
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					//String color = generateColor(r);
					ChartDataBean chartBean = new ChartDataBean();
					List<category> categoryList = new ArrayList<category>();
					int total=0;
					List<Map<String, Object>> daterows = jdbcTemplate.queryForList("SELECT p.productId,p.brandNoPackQty,p.brandNo,p.brandName,p.`quantity`,p.`category`,p.`company`,p.packQty,s.`sale`,TRUNCATE(s.`sale`/p.`packQty`,0) AS cases,s.`saleDate`,s.totalPrice FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` WHERE s.saleDate ='"+row.get("saleDate").toString()+"' AND category IN(1,2) ORDER BY p.`brandName` ASC");
					if(daterows != null && daterows.size()>0) {
						for(Map daterow : daterows){
							category bean = new category();
							 total+=Integer.parseInt(daterow.get("cases").toString());
							bean.setLabel("");
							bean.setColor(generateColor(r));
							bean.setHovertext(daterow.get("brandName").toString()+"_"+daterow.get("quantity").toString()+", "+daterow.get("cases").toString()+" CASES, $percentValue");
						    bean.setValue(Long.parseLong(daterow.get("cases").toString()));
						    categoryList.add(bean);
						}
					}	
					chartBean.setCategory(categoryList);
					chartBean.setLabel(row.get("saleDate").toString()+", "+total+" CASES");
					chartBean.setColor(generateColor(r));
					chartBean.setValue(Long.parseLong(""+total));
					chartBean.setTooltext(row.get("saleDate").toString()+", "+total+" CASES, $percentValue");
					chartDataBeanList.add(chartBean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return chartDataBeanList;
		}
		
		return chartDataBeanList;
	}
	 private String generateColor(Random r) {
	        StringBuilder color = new StringBuilder(Integer.toHexString(r
	                .nextInt(16777215)));
	        while (color.length() < 6) {
	            color.append("0");
	        }

	        return color.append("#").reverse().toString();

	    }
	 /*Old method not used any where*/
	@Override
	public List<ChartDataBean> getLiquorCasesGraphData(String sdate, String edate) {
		 Random r = new Random();
			List<ChartDataBean> chartDataBeanList = new ArrayList<ChartDataBean>();
			try{
				List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT DISTINCT `saleDate` FROM sale WHERE saleDate>='"+sdate+"' AND saleDate <='"+edate+"' ");
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						//String color = generateColor(r);
						ChartDataBean chartBean = new ChartDataBean();
						List<category> categoryList = new ArrayList<category>();
						int total=0;
						List<Map<String, Object>> daterows = jdbcTemplate.queryForList("SELECT p.productId,p.brandNoPackQty,p.brandNo,p.brandName,p.`quantity`,p.`category`,p.`company`,p.packQty,s.`sale`,TRUNCATE(s.`sale`/p.`packQty`,0) AS cases,s.`saleDate`,s.totalPrice FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` WHERE s.saleDate ='"+row.get("saleDate").toString()+"' AND category NOT IN(1,2) ORDER BY p.`brandName` ASC");
						if(daterows != null && daterows.size()>0) {
							for(Map daterow : daterows){
								category bean = new category();
								 total+=Integer.parseInt(daterow.get("cases").toString());
								bean.setLabel("");
								bean.setColor(generateColor(r));
								bean.setHovertext(daterow.get("brandName").toString()+"_"+daterow.get("quantity").toString()+", "+daterow.get("cases").toString()+" CASES, $percentValue");
							    bean.setValue(Long.parseLong(daterow.get("cases").toString()));
							    categoryList.add(bean);
							}
						}	
						chartBean.setCategory(categoryList);
						chartBean.setLabel(row.get("saleDate").toString()+", "+total+" CASES");
						chartBean.setColor(generateColor(r));
						chartBean.setValue(Long.parseLong(""+total));
						chartBean.setTooltext(row.get("saleDate").toString()+", "+total+" CASES, $percentValue");
						chartDataBeanList.add(chartBean);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				return chartDataBeanList;
			}
			
			return chartDataBeanList;
	}
	/*Old method not used any where*/
	@Override
	public String saveDiscountEsitmateData(JSONArray discountDetails) {
		log.info("PoizonDaoImpl.saveDiscountEsitmateData discountDetails= "+discountDetails);
		String result=null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		 String QUERY="insert into discount(brandNo,BrandName,caseRate,company,category,noOfCases,cashBackOnPerCase,priceToPurches,cashBackOnOrder,profitPercentageDiscount,L2"
		    		+ ",L1,Q,P,N,D,SB,LB,TIN,estimateDays) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		synchronized(this){
		try {
			for (int i = 0; i < discountDetails.length(); ++i) {
			 JSONObject rec;
			    rec = discountDetails.getJSONObject(i);
			    List<Map<String, Object>> flag = jdbcTemplate.queryForList("SELECT * FROM discount WHERE brandNo = "+Integer.parseInt(rec.getString("brandNo"))+" AND (CAST(discountdate AS DATE)) = '"+dateFormat.format(date)+"'");
			    
			    System.out.println("flag>> "+flag.size());
			    if(flag.size()>0 && flag != null){
			    	int val = jdbcTemplate.update("UPDATE discount SET noOfCases="+Integer.parseInt(rec.getString("noofcases"))+", cashBackOnPerCase="+Double.parseDouble(rec.getString("cashBackOnPerPrice"))+""
			    			+ ", priceToPurches="+Double.parseDouble(rec.getString("priceToPurches"))+", cashBackOnOrder="+Double.parseDouble(rec.getString("cashBackOnOrder"))+", profitPercentageDiscount='"+rec.getString("profitDiscount")+"', L2="+Double.parseDouble(rec.getString("L2"))+""
			    					+ ", L1="+Double.parseDouble(rec.getString("L1"))+", Q="+Double.parseDouble(rec.getString("Q"))+", P="+Double.parseDouble(rec.getString("P"))+""
			    							+ ", N="+Double.parseDouble(rec.getString("N"))+", D="+Double.parseDouble(rec.getString("D"))+",SB="+Double.parseDouble(rec.getString("SB"))+""
			    									+ ", LB="+Double.parseDouble(rec.getString("LB"))+", TIN="+Double.parseDouble(rec.getString("TIN"))+", estimateDays="+Integer.parseInt(rec.getString("estimateDays"))+" WHERE brandNo = "+Integer.parseInt(rec.getString("brandNo"))+" AND (CAST(discountdate AS DATE)) = '"+dateFormat.format(date)+"'");
			    
			    	 if(val > 0){
					    	result="Discount Data saved successfully";
					    }
			    
			    }else{
			    int val = jdbcTemplate.update(QUERY,new Object[]{Integer.parseInt(rec.getString("brandNo")),rec.getString("brandName"),Double.parseDouble(rec.getString("caseRate")),
			    		rec.getString("company"),rec.getString("category"),Integer.parseInt(rec.getString("noofcases")),Double.parseDouble(rec.getString("cashBackOnPerPrice")),
			    		Double.parseDouble(rec.getString("priceToPurches")),Double.parseDouble(rec.getString("cashBackOnOrder")),rec.getString("profitDiscount"),
			    		Double.parseDouble(rec.getString("L2")),Double.parseDouble(rec.getString("L1")),Double.parseDouble(rec.getString("Q")),
			    		Double.parseDouble(rec.getString("P")),Double.parseDouble(rec.getString("N")),Double.parseDouble(rec.getString("D")),
			    		Double.parseDouble(rec.getString("SB")),Double.parseDouble(rec.getString("LB")),Double.parseDouble(rec.getString("TIN")),Integer.parseInt(rec.getString("estimateDays"))});
			    if(val > 0){
			    	result="Discount Data saved successfully";
			    }
			    }
			    
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			result="Something Went Wrong.";
			log.log(Level.WARN, "saveDiscountEsitmateData in PoizonDaoImpl exception", e);
			e.printStackTrace();
		}
		}
		return result;
	}
	/**
	 * This method is used to get distinct invoice date list from 'invoice' table.
	 * */
	@Override
	public List<InvoiceDateBean> getTotalInvoiceDateAsPerSheet() {
		String Query="SELECT DISTINCT DATE_FORMAT(invoiceDateAsPerSheet,'%d-%m-%Y') AS invoiceDateAsPerSheet FROM invoice WHERE store_id = "+userSession.getStoreId()+"";
		List<InvoiceDateBean> invoiceDateList = new ArrayList<InvoiceDateBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(Query);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					InvoiceDateBean invoiceDateBean = new InvoiceDateBean();
					invoiceDateBean.setDate(row.get("invoiceDateAsPerSheet").toString());
					invoiceDateList.add(invoiceDateBean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return invoiceDateList;
		}finally {
			
		}
		return invoiceDateList;
		
	}
	/*Old method not used any where*/
	@Override
	public List<TotalPriceDateWiseBean> getSaleAmountMonthWise() {
		List<TotalPriceDateWiseBean> totalPriceDateWiseList = new ArrayList<TotalPriceDateWiseBean>();
		String QUERY="SELECT SUM(totalPrice) AS value,saleDate,(SELECT DATE_FORMAT(saleDate, '%M' ' %Y')) AS label FROM sale GROUP BY (SELECT CONCAT(DATE_FORMAT(LAST_DAY(saleDate - INTERVAL 0 MONTH),'%Y-%m-'),'01')) ORDER BY saleDate";
		
		List<TotalPriceDateWiseBean> analysisList = new ArrayList<TotalPriceDateWiseBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					TotalPriceDateWiseBean saleBean = new TotalPriceDateWiseBean();
					saleBean.setValue(row.get("value").toString()!=null?Double.parseDouble(row.get("value").toString()):0);
					saleBean.setLabel(row.get("label").toString());
					analysisList.add(saleBean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return analysisList;
		}
		return analysisList;
	}
	/**
	 * This method is used to generate indent estimate.
	 * Getting saved discount amount, current stock, lifted stock, target, pending and etc
	 * */
	@Override
	public DiscountEstimationWithDate getDiscountEsitmateDataWithDays(int days,String indentDate) {
		int storeId=userSession.getStoreId();
		DiscountEstimationWithDate discountEstimationWithDate = new DiscountEstimationWithDate();
		String startDate=null,endDate=null,midileDate=null;
		//String dateVal = null;
		List<DiscountEstimationBean> estimationBeanList = new ArrayList<DiscountEstimationBean>();
		try{
			List<Map<String, Object>> flagcheck = jdbcTemplate.queryForList("SELECT DISTINCT flag FROM temp_indent_estimate WHERE DATE='"+indentDate+"'  AND store_id="+storeId+" ");
			
			if(flagcheck != null && flagcheck.size() > 0){
			 boolean flagValue = (boolean) flagcheck.get(0).get("flag");
				if(!flagValue){
					System.out.println("If Block");
					discountEstimationWithDate.setDate(indentDate);
					estimationBeanList = getExistingIndentData(days,indentDate);
					 discountEstimationWithDate.setDiscountEstimationBean(estimationBeanList);
					 discountEstimationWithDate.setFlag(false);
				}else{
					System.out.println("Inner Else block");
					
					List<DiscountEstimationBean> existingBeanList = getExistingIndentData(days,indentDate);
					estimationBeanList.addAll(existingBeanList);
					String brandList = jdbcTemplate.queryForObject("SELECT GROUP_CONCAT(brandNo SEPARATOR ',') AS brandNo FROM temp_indent_estimate WHERE store_id="+storeId+"",String.class);
					
					List<String> dateList = jdbcTemplate.queryForList("SELECT DISTINCT saleDate FROM sale"
							+ " WHERE `saleDate` >= (SELECT DATE_ADD('"+indentDate+"', INTERVAL -60 DAY)) AND saleDate <='"+indentDate+"' AND store_id="+storeId+" ORDER BY saleDate DESC", String.class);
					
					Date date = new SimpleDateFormat("yyyy-MM-dd").parse(indentDate);
					DateFormat simpleDate=new SimpleDateFormat("E");
					String day=simpleDate.format(date);
					DateAndValueBean list = SaleDateUtil.getMatchSaleDate(dateList,day,days);
		            startDate = dateList.get(dateList.size()-1);
		            endDate = dateList.get(0);
		            midileDate = dateList.get(29);
				    System.out.println("SatrtDate>>>"+midileDate+"  Enddate>>>"+endDate);
				    String QUERY="SELECT DISTINCT p1.realBrandNo, p1.`shortBrandName`,p1.`category`,p1.productType,"
							+ "(SELECT d.target FROM stock_lift_with_discount d WHERE d.store_id="+storeId+" AND d.brandNo=p1.realBrandNo "
							+ "AND d.stockDiscountDate=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+indentDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))) AS target,"
							+ "(SELECT d.targetCase FROM discount d WHERE d.brandNo=p1.realBrandNo AND d.estimateDate=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+endDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))) AS commitment,"
							+ "(SELECT t.color FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS color,"
							+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyOrder,"
							+ "(SELECT t.categoryName FROM `category_tab` t WHERE p1.`category`=t.`categoryId`)"
							+ " AS categoryName,p1.`company`,(SELECT t.companyName FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyName,"
							+ "ROUND(AVG((SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p1.brandNoPackQty ORDER BY `invoiceId` DESC LIMIT 1)),0) AS casePrice,"
							+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , "
							+ "product p WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') FROM invoice r ,"
							+ " product p WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2LStock,"
							+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
							+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') FROM invoice r , product p WHERE "
							+ "r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1LStock,"
							+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
							+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='Q') FROM invoice r , product p WHERE "
							+ "  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='Q') AS QStock, "
							+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
							+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') FROM invoice r , product p WHERE "
							+ "  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS PStock,(SELECT "
							+ "TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p"
							+ " WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') FROM invoice r , product p "
							+ "WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS NStock,"
							+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
							+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') FROM invoice r , product p WHERE  "
							+ " r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS DStock,"
							+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s ,"
							+ " product p WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') FROM invoice r ,"
							+ " product p WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SBStock,"
							+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
							+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') FROM invoice r , product p WHERE "
							+ "  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LBStock,"
							+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
							+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') FROM invoice r , product p WHERE "
							+ "  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TINStock, "
							+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s ,"
							+ " product p WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') FROM invoice r ,"
							+ " product p WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty =p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS XStock,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
							+ " AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2L,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1L,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo AND p.packType='Q') AS Q,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS P,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS N,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS D,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SB,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LB,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TIN,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
							+ "  AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS X,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"'"
							+ " AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2Lsale,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1Lsale,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo AND p.packType='Q') AS Qsale,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS Psale,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS Nsale,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS Dsale,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SBsale,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LBsale,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
							+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TINsale,"
							+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"'"
							+ "  AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS Xsale,"
							+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2LSpecilaMrp,"
							+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1LSpecilaMrp,"
							+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='Q') AS QSpecilaMrp,"
							+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS PSpecilaMrp,"
							+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS NSpecilaMrp,"
							+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS DSpecilaMrp,"
							+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SBSpecilaMrp,"
							+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LBSpecilaMrp,"
							+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TINSpecilaMrp,"
							+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS XSpecilaMrp"
							+ " FROM product p1 WHERE p1.`active`=1 AND p1.`realBrandNo` NOT IN ("+brandList+") GROUP BY p1.realBrandNo ORDER BY p1.realBrandNo ASC";
				    //System.out.println(QUERY);
					if(startDate != "" && startDate != null && endDate != "" && endDate !=null){
						
							List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
							if(rows != null && rows.size()>0) {
								for(Map row : rows){
									DiscountEstimationBean bean = new DiscountEstimationBean();
									bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
									bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
									bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
									bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
									bean.setProductType(row.get("productType")!=null?row.get("productType").toString():"");
									bean.setTarget(row.get("target")!=null?Long.parseLong(row.get("target").toString()):0);
									bean.setCommitment(row.get("target")!=null?Long.parseLong(row.get("target").toString()):0);
									StockInBean stockInBean = getIndentEstimateSoldDataAndStock(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0, midileDate, endDate);
									bean.setLastMonthSold(stockInBean.getTotalPrice());
									bean.setInHouseStock(stockInBean.getStockInId());
									bean.setLiftedCase(stockInBean.getQtyBtl());
									bean.setCaseRate(row.get("casePrice")!=null?Double.parseDouble(row.get("casePrice").toString()):0);
									bean.setCompanyColor(row.get("color")!=null?row.get("color").toString():"");
									bean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
									
									bean.setL2Stock(row.get("2LStock")!=null?Double.parseDouble(row.get("2LStock").toString()):0);
									bean.setL1Stock(row.get("1LStock")!=null?Double.parseDouble(row.get("1LStock").toString()):0);
									bean.setQStock(row.get("QStock")!=null?Double.parseDouble(row.get("QStock").toString()):0);
									bean.setPStock(row.get("PStock")!=null?Double.parseDouble(row.get("PStock").toString()):0);
									bean.setNStock(row.get("NStock")!=null?Double.parseDouble(row.get("NStock").toString()):0);
									bean.setDStock(row.get("DStock")!=null?Double.parseDouble(row.get("DStock").toString()):0);
									bean.setSBStock(row.get("SBStock")!=null?Double.parseDouble(row.get("SBStock").toString()):0);
									bean.setLBStock(row.get("LBStock")!=null?Double.parseDouble(row.get("LBStock").toString()):0);
									bean.setTINStock(row.get("TINStock")!=null?Double.parseDouble(row.get("TINStock").toString()):0);
									bean.setXStock(row.get("XStock")!=null?Double.parseDouble(row.get("XStock").toString()):0);
									
									bean.setL2SpecialMargin(row.get("2LSpecilaMrp")!=null?Double.parseDouble(row.get("2LSpecilaMrp").toString()):0);
									bean.setL1SpecialMargin(row.get("1LSpecilaMrp")!=null?Double.parseDouble(row.get("1LSpecilaMrp").toString()):0);
									bean.setQSpecialMargin(row.get("QSpecilaMrp")!=null?Double.parseDouble(row.get("QSpecilaMrp").toString()):0);
									bean.setPSpecialMargin(row.get("PSpecilaMrp")!=null?Double.parseDouble(row.get("PSpecilaMrp").toString()):0);
									bean.setNSpecialMargin(row.get("NSpecilaMrp")!=null?Double.parseDouble(row.get("NSpecilaMrp").toString()):0);
									bean.setDSpecialMargin(row.get("DSpecilaMrp")!=null?Double.parseDouble(row.get("DSpecilaMrp").toString()):0);
									bean.setSBSpecialMargin(row.get("SBSpecilaMrp")!=null?Double.parseDouble(row.get("SBSpecilaMrp").toString()):0);
									bean.setLBSpecialMargin(row.get("LBSpecilaMrp")!=null?Double.parseDouble(row.get("LBSpecilaMrp").toString()):0);
									bean.setTINSpecialMargin(row.get("TINSpecilaMrp")!=null?Double.parseDouble(row.get("TINSpecilaMrp").toString()):0);
									bean.setXSpecialMargin(row.get("XSpecilaMrp")!=null?Double.parseDouble(row.get("XSpecilaMrp").toString()):0);
									
									bean.setL2(row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0);
									bean.setL1(row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0);
									bean.setQ(row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0);
									bean.setP(row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0);
									bean.setN(row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0);
									bean.setSB(row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0);
									bean.setD(row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0);
									bean.setLB(row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0);
									bean.setTIN(row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0);
									bean.setX(row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0);
									
									bean.setL2Sale(row.get("2Lsale")!=null?Double.parseDouble(row.get("2Lsale").toString()):0);
									bean.setL1Sale(row.get("1Lsale")!=null?Double.parseDouble(row.get("1Lsale").toString()):0);
									bean.setQSale(row.get("Qsale")!=null?Double.parseDouble(row.get("Qsale").toString()):0);
									bean.setPSale(row.get("Psale")!=null?Double.parseDouble(row.get("Psale").toString()):0);
									bean.setNSale(row.get("Nsale")!=null?Double.parseDouble(row.get("Nsale").toString()):0);
									bean.setSBSale(row.get("SBsale")!=null?Double.parseDouble(row.get("SBsale").toString()):0);
									bean.setDSale(row.get("Dsale")!=null?Double.parseDouble(row.get("Dsale").toString()):0);
									bean.setLBSale(row.get("LBsale")!=null?Double.parseDouble(row.get("LBsale").toString()):0);
									bean.setTINSale(row.get("TINsale")!=null?Double.parseDouble(row.get("TINsale").toString()):0);
									bean.setXSale(row.get("Xsale")!=null?Double.parseDouble(row.get("Xsale").toString()):0);
									bean.setPending(stockInBean.getQtyBtl());
									
									if((row.get("2Lsale")!=null?Double.parseDouble(row.get("2Lsale").toString()):0) > 0)
										bean.setL2perday((row.get("2LStock")!=null?Double.parseDouble(row.get("2LStock").toString()):0)/((row.get("2Lsale")!=null?Double.parseDouble(row.get("2Lsale").toString()):0)/30));
									else
										bean.setL2perday((double)0);
									if((row.get("1Lsale")!=null?Double.parseDouble(row.get("1Lsale").toString()):0) > 0)
										bean.setL1perday((row.get("1LStock")!=null?Double.parseDouble(row.get("1LStock").toString()):0)/((row.get("1Lsale")!=null?Double.parseDouble(row.get("1Lsale").toString()):0)/30));
									else
										bean.setL1perday((double)0);
									if((row.get("Qsale")!=null?Double.parseDouble(row.get("Qsale").toString()):0) > 0)
									    bean.setQperday((row.get("QStock")!=null?Double.parseDouble(row.get("QStock").toString()):0)/((row.get("Qsale")!=null?Double.parseDouble(row.get("Qsale").toString()):0)/30));
									else
										bean.setQperday((double)0);
									if((row.get("Psale")!=null?Double.parseDouble(row.get("Psale").toString()):0) > 0)
									    bean.setPperday((row.get("PStock")!=null?Double.parseDouble(row.get("PStock").toString()):0)/((row.get("Psale")!=null?Double.parseDouble(row.get("Psale").toString()):0)/30));
									else
										bean.setPperday((double)0);
									if((row.get("Nsale")!=null?Double.parseDouble(row.get("Nsale").toString()):0) > 0)
									    bean.setNperday((row.get("NStock")!=null?Double.parseDouble(row.get("NStock").toString()):0)/((row.get("Nsale")!=null?Double.parseDouble(row.get("Nsale").toString()):0)/30));
									else
										bean.setNperday((double)0);
									if((row.get("Dsale")!=null?Double.parseDouble(row.get("Dsale").toString()):0) > 0)
									    bean.setDperday((row.get("DStock")!=null?Double.parseDouble(row.get("DStock").toString()):0)/((row.get("Dsale")!=null?Double.parseDouble(row.get("Dsale").toString()):0)/30));
									else
										bean.setDperday((double)0);
									if((row.get("LBsale")!=null?Double.parseDouble(row.get("LBsale").toString()):0) > 0)
								    	bean.setLBperday((row.get("LBStock")!=null?Double.parseDouble(row.get("LBStock").toString()):0)/((row.get("LBsale")!=null?Double.parseDouble(row.get("LBsale").toString()):0)/30));
									else
										bean.setLBperday((double)0);
									if((row.get("SBsale")!=null?Double.parseDouble(row.get("SBsale").toString()):0) > 0)
									    bean.setSBperday((row.get("SBStock")!=null?Double.parseDouble(row.get("SBStock").toString()):0)/((row.get("SBsale")!=null?Double.parseDouble(row.get("SBsale").toString()):0)/30));
									else
										bean.setSBperday((double)0);
									if((row.get("Xsale")!=null?Double.parseDouble(row.get("Xsale").toString()):0) > 0)
									    bean.setXperday((row.get("XStock")!=null?Double.parseDouble(row.get("XStock").toString()):0)/((row.get("Xsale")!=null?Double.parseDouble(row.get("Xsale").toString()):0)/30));
									else
										bean.setXperday((double)0);
									if((row.get("TINsale")!=null?Double.parseDouble(row.get("TINsale").toString()):0) > 0)
									    bean.setTINperday((row.get("TINStock")!=null?Double.parseDouble(row.get("TINStock").toString()):0)/((row.get("TINsale")!=null?Double.parseDouble(row.get("TINsale").toString()):0)/30));
									else
										bean.setTINperday((double)0);
									
									double needCases = 0;
									
									if((row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0) > 0){
									    double value = Math.ceil(((row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0)/list.getValue())*(days- ((row.get("PStock")!=null?Double.parseDouble(row.get("PStock").toString()):0)/((row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0)/list.getValue()))));
									    if(value > 0){
										needCases +=value;
										bean.setpNeedCase((long) value);
									    }else
									    	bean.setpNeedCase((long) 0);
									}else{
										bean.setpNeedCase((long) 0);
									}
									if((row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0) > 0){
										double value = Math.ceil(((row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0)/list.getValue())*(days- ((row.get("QStock")!=null?Double.parseDouble(row.get("QStock").toString()):0)/((row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0)/list.getValue()))));
										if(value > 0){
											needCases +=value;
											bean.setqNeedCase((long) value);
										}else
									    	bean.setqNeedCase((long) 0);
									}else{
										bean.setqNeedCase((long) 0);
									}
									if((row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0) > 0){
										double value = Math.ceil(((row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0)/list.getValue())*(days- ((row.get("2LStock")!=null?Double.parseDouble(row.get("2LStock").toString()):0)/((row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0)/list.getValue()))));
										if(value > 0){
											needCases +=value;
											bean.setL2NeedCase((long) value);
										}else
											bean.setL2NeedCase((long) 0);
									}else{
										bean.setL2NeedCase((long) 0);
									}
									if((row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0) > 0){
										double value = Math.ceil(((row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0)/list.getValue())*(days- ((row.get("1LStock")!=null?Double.parseDouble(row.get("1LStock").toString()):0)/((row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0)/list.getValue()))));
										if(value > 0){
											needCases +=value;
											bean.setL1NeedCase((long) value);
										}else
											bean.setL1NeedCase((long) 0);
									}else{
										bean.setL1NeedCase((long) 0);
									}
									if((row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0) > 0){
										double value = Math.ceil(((row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0)/list.getValue())*(days- ((row.get("NStock")!=null?Double.parseDouble(row.get("NStock").toString()):0)/((row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0)/list.getValue()))));
										if(value > 0){
											needCases +=value;
											bean.setnNeedCase((long) value);
										}else
											bean.setnNeedCase((long) 0);
									}else{
										bean.setnNeedCase((long) 0);
									}
									if((row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0) > 0){
										double value = Math.ceil(((row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0)/list.getValue())*(days- ((row.get("SBStock")!=null?Double.parseDouble(row.get("SBStock").toString()):0)/((row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0)/list.getValue()))));
										if(value > 0){
											bean.setSbNeedCase((long) value);
										needCases +=value;
										}else
											bean.setSbNeedCase((long) 0);
									}else{
										bean.setSbNeedCase((long) 0);
									}
									if((row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0) > 0){
										double value = Math.ceil(((row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0)/list.getValue())*(days- ((row.get("DStock")!=null?Double.parseDouble(row.get("DStock").toString()):0)/((row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0)/list.getValue()))));
										if(value > 0){
											needCases +=value;
											bean.setdNeedCase((long) value);
										}else
											bean.setdNeedCase((long) 0);
									}else{
										bean.setdNeedCase((long) 0);
									}
									if((row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0) > 0){
										double value = Math.ceil(((row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0)/list.getValue())*(days- ((row.get("LBStock")!=null?Double.parseDouble(row.get("LBStock").toString()):0)/((row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0)/list.getValue()))));
										if(value > 0){
											needCases +=value;
											bean.setLbNeedCase((long) value);
										}else
											bean.setLbNeedCase((long) 0);
									}else{
										bean.setLbNeedCase((long) 0);
									}
									if((row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0) > 0){
										double value = Math.ceil(((row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0)/list.getValue())*(days- ((row.get("TINStock")!=null?Double.parseDouble(row.get("TINStock").toString()):0)/((row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0)/list.getValue()))));
										if(value > 0){
											needCases +=value;
											bean.setTinNeedCase((long) value);
										}else
											bean.setTinNeedCase((long) 0);
									}else{
										bean.setTinNeedCase((long) 0);
									}
									if((row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0) > 0){
										double value = Math.ceil(((row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0)/list.getValue())*(days- ((row.get("XStock")!=null?Double.parseDouble(row.get("XStock").toString()):0)/((row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0)/list.getValue()))));
										if(value > 0){
											needCases +=value;
											bean.setxNeedCase((long) value);
										}else
											bean.setxNeedCase((long) 0);
									}else{
										bean.setxNeedCase((long) 0);
									}
									bean.setNeedCase((double) Math.round(needCases));
									estimationBeanList.add(bean);
									
								}
							}
						}
					 discountEstimationWithDate.setDiscountEstimationBean(estimationBeanList);
					 discountEstimationWithDate.setFlag(true);
				}
			}else{
              System.out.println("Main Else block");
				List<String> dateList = jdbcTemplate.queryForList("SELECT DISTINCT saleDate FROM sale"
						+ " WHERE `saleDate` >= (SELECT DATE_ADD('"+indentDate+"', INTERVAL -60 DAY)) AND saleDate <='"+indentDate+"' ORDER BY saleDate DESC", String.class);
				Date date = new SimpleDateFormat("yyyy-MM-dd").parse(indentDate);
				DateFormat simpleDate=new SimpleDateFormat("E");
				String day=simpleDate.format(date);
				DateAndValueBean list = SaleDateUtil.getMatchSaleDate(dateList,day,days);
	            //System.out.println(list.getDate()+" ::::::::::::::: "+list.getValue());
	            startDate = dateList.get(dateList.size()-1);
	            endDate = dateList.get(0);
	            midileDate = dateList.get(29);
			    System.out.println("SatrtDate>>>"+midileDate+"  Enddate>>>"+endDate);
			    String QUERY="SELECT DISTINCT p1.realBrandNo, p1.`shortBrandName`,p1.`category`,p1.productType,"
						+ "(SELECT d.target FROM stock_lift_with_discount d WHERE d.brandNo=p1.realBrandNo "
						+ "AND d.stockDiscountDate=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+indentDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))) AS target,"
						+ "(SELECT d.targetCase FROM discount d WHERE d.brandNo=p1.realBrandNo AND d.estimateDate=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+endDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))) AS commitment,"
						+ "(SELECT t.color FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS color,"
						+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyOrder,"
						+ "(SELECT t.categoryName FROM `category_tab` t WHERE p1.`category`=t.`categoryId`)"
						+ " AS categoryName,p1.`company`,(SELECT t.companyName FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyName,"
						+ "ROUND(AVG((SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p1.brandNoPackQty ORDER BY `invoiceId` DESC LIMIT 1)),0) AS casePrice,"
						+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , "
						+ "product p WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') FROM invoice r ,"
						+ " product p WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2LStock,"
						+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
						+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') FROM invoice r , product p WHERE "
						+ "r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1LStock,"
						+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
						+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='Q') FROM invoice r , product p WHERE "
						+ "  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='Q') AS QStock, "
						+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
						+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') FROM invoice r , product p WHERE "
						+ "  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS PStock,(SELECT "
						+ "TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p"
						+ " WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') FROM invoice r , product p "
						+ "WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS NStock,"
						+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
						+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') FROM invoice r , product p WHERE  "
						+ " r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS DStock,"
						+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s ,"
						+ " product p WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') FROM invoice r ,"
						+ " product p WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SBStock,"
						+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
						+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') FROM invoice r , product p WHERE "
						+ "  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LBStock,"
						+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
						+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') FROM invoice r , product p WHERE "
						+ "  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TINStock, "
						+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s ,"
						+ " product p WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') FROM invoice r ,"
						+ " product p WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty =p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS XStock,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
						+ " AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2L,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1L,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo AND p.packType='Q') AS Q,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS P,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS N,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS D,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SB,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LB,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TIN,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
						+ "  AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS X,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"'"
						+ " AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2Lsale,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1Lsale,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo AND p.packType='Q') AS Qsale,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS Psale,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS Nsale,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS Dsale,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SBsale,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LBsale,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
						+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TINsale,"
						+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"'"
						+ "  AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS Xsale,"
						+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2LSpecilaMrp,"
						+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1LSpecilaMrp,"
						+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='Q') AS QSpecilaMrp,"
						+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS PSpecilaMrp,"
						+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS NSpecilaMrp,"
						+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS DSpecilaMrp,"
						+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SBSpecilaMrp,"
						+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LBSpecilaMrp,"
						+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TINSpecilaMrp,"
						+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS XSpecilaMrp"
						+ " FROM product p1 WHERE p1.`active`=1 GROUP BY p1.realBrandNo ORDER BY p1.realBrandNo ASC";
			    System.out.println(QUERY);
				if(startDate != "" && startDate != null && endDate != "" && endDate !=null){
					
						List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
						if(rows != null && rows.size()>0) {
							for(Map row : rows){
								DiscountEstimationBean bean = new DiscountEstimationBean();
								bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
								bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
								bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
								bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
								bean.setProductType(row.get("productType")!=null?row.get("productType").toString():"");
								bean.setTarget(row.get("target")!=null?Long.parseLong(row.get("target").toString()):0);
								bean.setCommitment(row.get("target")!=null?Long.parseLong(row.get("target").toString()):0);
								StockInBean stockInBean = getIndentEstimateSoldDataAndStock(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0, midileDate, endDate);
								bean.setLastMonthSold(stockInBean.getTotalPrice());
								bean.setInHouseStock(stockInBean.getStockInId());
								bean.setLiftedCase(stockInBean.getQtyBtl());
								bean.setCaseRate(row.get("casePrice")!=null?Double.parseDouble(row.get("casePrice").toString()):0);
								bean.setCompanyColor(row.get("color")!=null?row.get("color").toString():"");
								bean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
								
								bean.setL2Stock(row.get("2LStock")!=null?Double.parseDouble(row.get("2LStock").toString()):0);
								bean.setL1Stock(row.get("1LStock")!=null?Double.parseDouble(row.get("1LStock").toString()):0);
								bean.setQStock(row.get("QStock")!=null?Double.parseDouble(row.get("QStock").toString()):0);
								bean.setPStock(row.get("PStock")!=null?Double.parseDouble(row.get("PStock").toString()):0);
								bean.setNStock(row.get("NStock")!=null?Double.parseDouble(row.get("NStock").toString()):0);
								bean.setDStock(row.get("DStock")!=null?Double.parseDouble(row.get("DStock").toString()):0);
								bean.setSBStock(row.get("SBStock")!=null?Double.parseDouble(row.get("SBStock").toString()):0);
								bean.setLBStock(row.get("LBStock")!=null?Double.parseDouble(row.get("LBStock").toString()):0);
								bean.setTINStock(row.get("TINStock")!=null?Double.parseDouble(row.get("TINStock").toString()):0);
								bean.setXStock(row.get("XStock")!=null?Double.parseDouble(row.get("XStock").toString()):0);
								
								bean.setL2SpecialMargin(row.get("2LSpecilaMrp")!=null?Double.parseDouble(row.get("2LSpecilaMrp").toString()):0);
								bean.setL1SpecialMargin(row.get("1LSpecilaMrp")!=null?Double.parseDouble(row.get("1LSpecilaMrp").toString()):0);
								bean.setQSpecialMargin(row.get("QSpecilaMrp")!=null?Double.parseDouble(row.get("QSpecilaMrp").toString()):0);
								bean.setPSpecialMargin(row.get("PSpecilaMrp")!=null?Double.parseDouble(row.get("PSpecilaMrp").toString()):0);
								bean.setNSpecialMargin(row.get("NSpecilaMrp")!=null?Double.parseDouble(row.get("NSpecilaMrp").toString()):0);
								bean.setDSpecialMargin(row.get("DSpecilaMrp")!=null?Double.parseDouble(row.get("DSpecilaMrp").toString()):0);
								bean.setSBSpecialMargin(row.get("SBSpecilaMrp")!=null?Double.parseDouble(row.get("SBSpecilaMrp").toString()):0);
								bean.setLBSpecialMargin(row.get("LBSpecilaMrp")!=null?Double.parseDouble(row.get("LBSpecilaMrp").toString()):0);
								bean.setTINSpecialMargin(row.get("TINSpecilaMrp")!=null?Double.parseDouble(row.get("TINSpecilaMrp").toString()):0);
								bean.setXSpecialMargin(row.get("XSpecilaMrp")!=null?Double.parseDouble(row.get("XSpecilaMrp").toString()):0);
								
								bean.setL2(row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0);
								bean.setL1(row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0);
								bean.setQ(row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0);
								bean.setP(row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0);
								bean.setN(row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0);
								bean.setSB(row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0);
								bean.setD(row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0);
								bean.setLB(row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0);
								bean.setTIN(row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0);
								bean.setX(row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0);
								
								bean.setL2Sale(row.get("2Lsale")!=null?Double.parseDouble(row.get("2Lsale").toString()):0);
								bean.setL1Sale(row.get("1Lsale")!=null?Double.parseDouble(row.get("1Lsale").toString()):0);
								bean.setQSale(row.get("Qsale")!=null?Double.parseDouble(row.get("Qsale").toString()):0);
								bean.setPSale(row.get("Psale")!=null?Double.parseDouble(row.get("Psale").toString()):0);
								bean.setNSale(row.get("Nsale")!=null?Double.parseDouble(row.get("Nsale").toString()):0);
								bean.setSBSale(row.get("SBsale")!=null?Double.parseDouble(row.get("SBsale").toString()):0);
								bean.setDSale(row.get("Dsale")!=null?Double.parseDouble(row.get("Dsale").toString()):0);
								bean.setLBSale(row.get("LBsale")!=null?Double.parseDouble(row.get("LBsale").toString()):0);
								bean.setTINSale(row.get("TINsale")!=null?Double.parseDouble(row.get("TINsale").toString()):0);
								bean.setXSale(row.get("Xsale")!=null?Double.parseDouble(row.get("Xsale").toString()):0);
								bean.setPending(stockInBean.getQtyBtl());
								
								if((row.get("2Lsale")!=null?Double.parseDouble(row.get("2Lsale").toString()):0) > 0)
									bean.setL2perday((row.get("2LStock")!=null?Double.parseDouble(row.get("2LStock").toString()):0)/((row.get("2Lsale")!=null?Double.parseDouble(row.get("2Lsale").toString()):0)/30));
								else
									bean.setL2perday((double)0);
								if((row.get("1Lsale")!=null?Double.parseDouble(row.get("1Lsale").toString()):0) > 0)
									bean.setL1perday((row.get("1LStock")!=null?Double.parseDouble(row.get("1LStock").toString()):0)/((row.get("1Lsale")!=null?Double.parseDouble(row.get("1Lsale").toString()):0)/30));
								else
									bean.setL1perday((double)0);
								if((row.get("Qsale")!=null?Double.parseDouble(row.get("Qsale").toString()):0) > 0)
								    bean.setQperday((row.get("QStock")!=null?Double.parseDouble(row.get("QStock").toString()):0)/((row.get("Qsale")!=null?Double.parseDouble(row.get("Qsale").toString()):0)/30));
								else
									bean.setQperday((double)0);
								if((row.get("Psale")!=null?Double.parseDouble(row.get("Psale").toString()):0) > 0)
								    bean.setPperday((row.get("PStock")!=null?Double.parseDouble(row.get("PStock").toString()):0)/((row.get("Psale")!=null?Double.parseDouble(row.get("Psale").toString()):0)/30));
								else
									bean.setPperday((double)0);
								if((row.get("Nsale")!=null?Double.parseDouble(row.get("Nsale").toString()):0) > 0)
								    bean.setNperday((row.get("NStock")!=null?Double.parseDouble(row.get("NStock").toString()):0)/((row.get("Nsale")!=null?Double.parseDouble(row.get("Nsale").toString()):0)/30));
								else
									bean.setNperday((double)0);
								if((row.get("Dsale")!=null?Double.parseDouble(row.get("Dsale").toString()):0) > 0)
								    bean.setDperday((row.get("DStock")!=null?Double.parseDouble(row.get("DStock").toString()):0)/((row.get("Dsale")!=null?Double.parseDouble(row.get("Dsale").toString()):0)/30));
								else
									bean.setDperday((double)0);
								if((row.get("LBsale")!=null?Double.parseDouble(row.get("LBsale").toString()):0) > 0)
							    	bean.setLBperday((row.get("LBStock")!=null?Double.parseDouble(row.get("LBStock").toString()):0)/((row.get("LBsale")!=null?Double.parseDouble(row.get("LBsale").toString()):0)/30));
								else
									bean.setLBperday((double)0);
								if((row.get("SBsale")!=null?Double.parseDouble(row.get("SBsale").toString()):0) > 0)
								    bean.setSBperday((row.get("SBStock")!=null?Double.parseDouble(row.get("SBStock").toString()):0)/((row.get("SBsale")!=null?Double.parseDouble(row.get("SBsale").toString()):0)/30));
								else
									bean.setSBperday((double)0);
								if((row.get("Xsale")!=null?Double.parseDouble(row.get("Xsale").toString()):0) > 0)
								    bean.setXperday((row.get("XStock")!=null?Double.parseDouble(row.get("XStock").toString()):0)/((row.get("Xsale")!=null?Double.parseDouble(row.get("Xsale").toString()):0)/30));
								else
									bean.setXperday((double)0);
								if((row.get("TINsale")!=null?Double.parseDouble(row.get("TINsale").toString()):0) > 0)
								    bean.setTINperday((row.get("TINStock")!=null?Double.parseDouble(row.get("TINStock").toString()):0)/((row.get("TINsale")!=null?Double.parseDouble(row.get("TINsale").toString()):0)/30));
								else
									bean.setTINperday((double)0);
								
								double needCases = 0;
								
								if((row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0) > 0){
								    double value = Math.ceil(((row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0)/list.getValue())*(days- ((row.get("PStock")!=null?Double.parseDouble(row.get("PStock").toString()):0)/((row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0)/list.getValue()))));
								    if(value > 0){
									needCases +=value;
									bean.setpNeedCase((long) value);
								    }else
								    	bean.setpNeedCase((long) 0);
								}else{
									bean.setpNeedCase((long) 0);
								}
								if((row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0) > 0){
									double value = Math.ceil(((row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0)/list.getValue())*(days- ((row.get("QStock")!=null?Double.parseDouble(row.get("QStock").toString()):0)/((row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0)/list.getValue()))));
									if(value > 0){
										needCases +=value;
										bean.setqNeedCase((long) value);
									}else
								    	bean.setqNeedCase((long) 0);
								}else{
									bean.setqNeedCase((long) 0);
								}
								if((row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0) > 0){
									double value = Math.ceil(((row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0)/list.getValue())*(days- ((row.get("2LStock")!=null?Double.parseDouble(row.get("2LStock").toString()):0)/((row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0)/list.getValue()))));
									if(value > 0){
										needCases +=value;
										bean.setL2NeedCase((long) value);
									}else
										bean.setL2NeedCase((long) 0);
								}else{
									bean.setL2NeedCase((long) 0);
								}
								if((row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0) > 0){
									double value = Math.ceil(((row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0)/list.getValue())*(days- ((row.get("1LStock")!=null?Double.parseDouble(row.get("1LStock").toString()):0)/((row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0)/list.getValue()))));
									if(value > 0){
										needCases +=value;
										bean.setL1NeedCase((long) value);
									}else
										bean.setL1NeedCase((long) 0);
								}else{
									bean.setL1NeedCase((long) 0);
								}
								if((row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0) > 0){
									double value = Math.ceil(((row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0)/list.getValue())*(days- ((row.get("NStock")!=null?Double.parseDouble(row.get("NStock").toString()):0)/((row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0)/list.getValue()))));
									if(value > 0){
										needCases +=value;
										bean.setnNeedCase((long) value);
									}else
										bean.setnNeedCase((long) 0);
								}else{
									bean.setnNeedCase((long) 0);
								}
								if((row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0) > 0){
									double value = Math.ceil(((row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0)/list.getValue())*(days- ((row.get("SBStock")!=null?Double.parseDouble(row.get("SBStock").toString()):0)/((row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0)/list.getValue()))));
									if(value > 0){
										bean.setSbNeedCase((long) value);
									needCases +=value;
									}else
										bean.setSbNeedCase((long) 0);
								}else{
									bean.setSbNeedCase((long) 0);
								}
								if((row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0) > 0){
									double value = Math.ceil(((row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0)/list.getValue())*(days- ((row.get("DStock")!=null?Double.parseDouble(row.get("DStock").toString()):0)/((row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0)/list.getValue()))));
									if(value > 0){
										needCases +=value;
										bean.setdNeedCase((long) value);
									}else
										bean.setdNeedCase((long) 0);
								}else{
									bean.setdNeedCase((long) 0);
								}
								if((row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0) > 0){
									double value = Math.ceil(((row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0)/list.getValue())*(days- ((row.get("LBStock")!=null?Double.parseDouble(row.get("LBStock").toString()):0)/((row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0)/list.getValue()))));
									if(value > 0){
										needCases +=value;
										bean.setLbNeedCase((long) value);
									}else
										bean.setLbNeedCase((long) 0);
								}else{
									bean.setLbNeedCase((long) 0);
								}
								if((row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0) > 0){
									double value = Math.ceil(((row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0)/list.getValue())*(days- ((row.get("TINStock")!=null?Double.parseDouble(row.get("TINStock").toString()):0)/((row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0)/list.getValue()))));
									if(value > 0){
										needCases +=value;
										bean.setTinNeedCase((long) value);
									}else
										bean.setTinNeedCase((long) 0);
								}else{
									bean.setTinNeedCase((long) 0);
								}
								if((row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0) > 0){
									double value = Math.ceil(((row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0)/list.getValue())*(days- ((row.get("XStock")!=null?Double.parseDouble(row.get("XStock").toString()):0)/((row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0)/list.getValue()))));
									if(value > 0){
										needCases +=value;
										bean.setxNeedCase((long) value);
									}else
										bean.setxNeedCase((long) 0);
								}else{
									bean.setxNeedCase((long) 0);
								}
								bean.setNeedCase((double) Math.round(needCases));
								estimationBeanList.add(bean);
								
							}
						}
					}
				 discountEstimationWithDate.setDiscountEstimationBean(estimationBeanList);
				 discountEstimationWithDate.setFlag(true);
			}
		}catch(Exception e){
				e.printStackTrace();
				return discountEstimationWithDate;
			}
		// TODO Auto-generated method stub
		return discountEstimationWithDate;
	}
	
	public List<DiscountEstimationBean> getExistingIndentData(int days, String indentDate){
		int storeId=userSession.getStoreId();
		
		List<DiscountEstimationBean> beanList = new ArrayList<DiscountEstimationBean>();
		try{
		String elseQuery = "SELECT brandNo,brandName,caseRate,noOfCases,target,lastMonthSold,companyOrder,companyColor,inHouseStock,commitment,"
				+ "  productType,X,p,q,l2,l1,n,sb,d,lb,tin,l2SpecialMargin,l1SpecialMargin,qspecialMargin,pspecialMargin,nspecialMargin,dspecialMargin,sbspecialMargin,"
				+ "  lbspecialMargin,tinspecialMargin,xspecialMargin,liftedCase,l2Stock,l1Stock,qstock,pstock,nstock,dstock,sbstock,"
				+ "  lbstock,tinstock,xstock,DATE,days,company,category,`pending`,`investMent`,`l2Val`,`l1Val`,`qVal`,`pVal`,`nVal`,`dVal`,`lbVal`,`sbVal`,"
				+ "`tinVal`,`xVal`,`totalSpecialMrp`,l2NeedCase,l1NeedCase,qNeedCase,pNeedCase,nNeedCase,dNeedCase,lbNeedCase,sbNeedCase,tinNeedCase,xNeedCase,"
				+ "l2sale,l1sale,qsale,psale,nsale,dsale,sbsale,lbsale,tinsale,xsale,l2perday,l1perday,qperday,pperday,nperday,dperday,lbperday,sbperday,xperday,tinperday"
				+ " FROM temp_indent_estimate WHERE DATE='"+indentDate+"' AND store_id="+storeId+"";
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(elseQuery);
		if(rows != null && rows.size()>0) {
			for(Map row : rows){
				DiscountEstimationBean bean = new DiscountEstimationBean();
				bean.setBrandNo(row.get("brandNo")!=null?Long.parseLong(row.get("brandNo").toString()):0);
				bean.setBrandName(row.get("brandName")!=null?row.get("brandName").toString():"");
				bean.setCategory(row.get("category")!=null?row.get("category").toString():"");
				bean.setCompany(row.get("company")!=null?row.get("company").toString():"");
				bean.setProductType(row.get("productType")!=null?row.get("productType").toString():"");
				bean.setTarget(row.get("target")!=null?Long.parseLong(row.get("target").toString()):0);
				bean.setCommitment(row.get("commitment")!=null?Long.parseLong(row.get("commitment").toString()):0);
				bean.setLastMonthSold(row.get("lastMonthSold")!=null?Double.parseDouble(row.get("lastMonthSold").toString()):0);
				bean.setInHouseStock(row.get("inHouseStock")!=null?Double.parseDouble(row.get("inHouseStock").toString()):0);
				bean.setLiftedCase(row.get("liftedCase")!=null?Double.parseDouble(row.get("liftedCase").toString()):0);
				bean.setCaseRate(row.get("caseRate")!=null?Double.parseDouble(row.get("caseRate").toString()):0);
				bean.setCompanyColor(row.get("companyColor")!=null?row.get("companyColor").toString():"");
				bean.setCompanyOrder(row.get("companyOrder")!=null?Long.parseLong(row.get("companyOrder").toString()):0);
				
				bean.setL2Stock(row.get("l2Stock")!=null?Double.parseDouble(row.get("l2Stock").toString()):0);
				bean.setL1Stock(row.get("l1Stock")!=null?Double.parseDouble(row.get("l1Stock").toString()):0);
				bean.setQStock(row.get("qstock")!=null?Double.parseDouble(row.get("qstock").toString()):0);
				bean.setPStock(row.get("pstock")!=null?Double.parseDouble(row.get("pstock").toString()):0);
				bean.setNStock(row.get("nstock")!=null?Double.parseDouble(row.get("nstock").toString()):0);
				bean.setDStock(row.get("dstock")!=null?Double.parseDouble(row.get("dstock").toString()):0);
				bean.setSBStock(row.get("sbstock")!=null?Double.parseDouble(row.get("sbstock").toString()):0);
				bean.setLBStock(row.get("lbstock")!=null?Double.parseDouble(row.get("lbstock").toString()):0);
				bean.setTINStock(row.get("tinstock")!=null?Double.parseDouble(row.get("tinstock").toString()):0);
				bean.setXStock(row.get("xstock")!=null?Double.parseDouble(row.get("xstock").toString()):0);
				
				bean.setL2SpecialMargin(row.get("l2SpecialMargin")!=null?Double.parseDouble(row.get("l2SpecialMargin").toString()):0);
				bean.setL1SpecialMargin(row.get("l1SpecialMargin")!=null?Double.parseDouble(row.get("l1SpecialMargin").toString()):0);
				bean.setQSpecialMargin(row.get("qspecialMargin")!=null?Double.parseDouble(row.get("qspecialMargin").toString()):0);
				bean.setPSpecialMargin(row.get("pspecialMargin")!=null?Double.parseDouble(row.get("pspecialMargin").toString()):0);
				bean.setNSpecialMargin(row.get("nspecialMargin")!=null?Double.parseDouble(row.get("nspecialMargin").toString()):0);
				bean.setDSpecialMargin(row.get("dspecialMargin")!=null?Double.parseDouble(row.get("dspecialMargin").toString()):0);
				bean.setSBSpecialMargin(row.get("sbspecialMargin")!=null?Double.parseDouble(row.get("sbspecialMargin").toString()):0);
				bean.setLBSpecialMargin(row.get("lbspecialMargin")!=null?Double.parseDouble(row.get("lbspecialMargin").toString()):0);
				bean.setTINSpecialMargin(row.get("tinspecialMargin")!=null?Double.parseDouble(row.get("tinspecialMargin").toString()):0);
				bean.setXSpecialMargin(row.get("xspecialMargin")!=null?Double.parseDouble(row.get("xspecialMargin").toString()):0);
				
				bean.setL2(row.get("l2")!=null?Double.parseDouble(row.get("l2").toString()):0);
				bean.setL1(row.get("l1")!=null?Double.parseDouble(row.get("l1").toString()):0);
				bean.setQ(row.get("q")!=null?Double.parseDouble(row.get("q").toString()):0);
				bean.setP(row.get("p")!=null?Double.parseDouble(row.get("p").toString()):0);
				bean.setN(row.get("n")!=null?Double.parseDouble(row.get("n").toString()):0);
				bean.setSB(row.get("sb")!=null?Double.parseDouble(row.get("sb").toString()):0);
				bean.setD(row.get("d")!=null?Double.parseDouble(row.get("d").toString()):0);
				bean.setLB(row.get("lb")!=null?Double.parseDouble(row.get("lb").toString()):0);
				bean.setTIN(row.get("tin")!=null?Double.parseDouble(row.get("tin").toString()):0);
				bean.setX(row.get("x")!=null?Double.parseDouble(row.get("x").toString()):0);
				bean.setPending(row.get("liftedCase")!=null?Double.parseDouble(row.get("liftedCase").toString()):0);
				bean.setNeedCase(row.get("noOfCases")!=null?Double.parseDouble(row.get("noOfCases").toString()):0);
				
				bean.setOtherPending(row.get("pending")!=null?Long.parseLong(row.get("pending").toString()):0);
				bean.setOtherInvestment(row.get("investMent")!=null?Long.parseLong(row.get("investMent").toString()):0);
				bean.setL2Val(row.get("l2Val")!=null?Long.parseLong(row.get("l2Val").toString()):0);
				bean.setL1Val(row.get("l1Val")!=null?Long.parseLong(row.get("l1Val").toString()):0);
				bean.setqVal(row.get("qVal")!=null?Long.parseLong(row.get("qVal").toString()):0);
				bean.setpVal(row.get("pVal")!=null?Long.parseLong(row.get("pVal").toString()):0);
				bean.setnVal(row.get("nVal")!=null?Long.parseLong(row.get("nVal").toString()):0);
				bean.setdVal(row.get("dVal")!=null?Long.parseLong(row.get("dVal").toString()):0);
				bean.setLbVal(row.get("lbVal")!=null?Long.parseLong(row.get("lbVal").toString()):0);
				bean.setSbVal(row.get("sbVal")!=null?Long.parseLong(row.get("sbVal").toString()):0);
				bean.setTinVal(row.get("tinVal")!=null?Long.parseLong(row.get("tinVal").toString()):0);
				bean.setxVal(row.get("xVal")!=null?Long.parseLong(row.get("xVal").toString()):0);
				bean.setTotalSpecialMrp(row.get("totalSpecialMrp")!=null?Double.parseDouble(row.get("totalSpecialMrp").toString()):0);
				
				bean.setL1NeedCase(row.get("l1NeedCase")!=null?Long.parseLong(row.get("l1NeedCase").toString()):0);
				bean.setL2NeedCase(row.get("l2NeedCase")!=null?Long.parseLong(row.get("l2NeedCase").toString()):0);
				bean.setqNeedCase(row.get("qNeedCase")!=null?Long.parseLong(row.get("qNeedCase").toString()):0);
				bean.setpNeedCase(row.get("pNeedCase")!=null?Long.parseLong(row.get("pNeedCase").toString()):0);
				bean.setnNeedCase(row.get("nNeedCase")!=null?Long.parseLong(row.get("nNeedCase").toString()):0);
				bean.setdNeedCase(row.get("dNeedCase")!=null?Long.parseLong(row.get("dNeedCase").toString()):0);
				bean.setLbNeedCase(row.get("lbNeedCase")!=null?Long.parseLong(row.get("lbNeedCase").toString()):0);
				bean.setSbNeedCase(row.get("sbNeedCase")!=null?Long.parseLong(row.get("sbNeedCase").toString()):0);
				bean.setTinNeedCase(row.get("tinNeedCase")!=null?Long.parseLong(row.get("tinNeedCase").toString()):0);
				bean.setxNeedCase(row.get("xNeedCase")!=null?Long.parseLong(row.get("xNeedCase").toString()):0);
				
				bean.setL2Sale(row.get("l2sale")!=null?Double.parseDouble(row.get("l2sale").toString()):0);
				bean.setL1Sale(row.get("l1sale")!=null?Double.parseDouble(row.get("l1sale").toString()):0);
				bean.setQSale(row.get("qsale")!=null?Double.parseDouble(row.get("qsale").toString()):0);
				bean.setPSale(row.get("psale")!=null?Double.parseDouble(row.get("psale").toString()):0);
				bean.setNSale(row.get("nsale")!=null?Double.parseDouble(row.get("nsale").toString()):0);
				bean.setSBSale(row.get("sbsale")!=null?Double.parseDouble(row.get("sbsale").toString()):0);
				bean.setDSale(row.get("dsale")!=null?Double.parseDouble(row.get("dsale").toString()):0);
				bean.setLBSale(row.get("lbsale")!=null?Double.parseDouble(row.get("lbsale").toString()):0);
				bean.setTINSale(row.get("tinsale")!=null?Double.parseDouble(row.get("tinsale").toString()):0);
				bean.setXSale(row.get("xsale")!=null?Double.parseDouble(row.get("xsale").toString()):0);

				bean.setL2perday(row.get("l2perday")!=null?Double.parseDouble(row.get("l2perday").toString()):0);
				bean.setL1perday(row.get("l1perday")!=null?Double.parseDouble(row.get("l1perday").toString()):0);
				bean.setQperday(row.get("qperday")!=null?Double.parseDouble(row.get("qperday").toString()):0);
				bean.setPperday(row.get("pperday")!=null?Double.parseDouble(row.get("pperday").toString()):0);
				bean.setNperday(row.get("nperday")!=null?Double.parseDouble(row.get("nperday").toString()):0);
				bean.setDperday(row.get("dperday")!=null?Double.parseDouble(row.get("dperday").toString()):0);
				bean.setLBperday(row.get("lbperday")!=null?Double.parseDouble(row.get("lbperday").toString()):0);
				bean.setSBperday(row.get("sbperday")!=null?Double.parseDouble(row.get("sbperday").toString()):0);
				bean.setTINperday(row.get("xperday")!=null?Double.parseDouble(row.get("xperday").toString()):0);
				bean.setXperday(row.get("xperday")!=null?Double.parseDouble(row.get("xperday").toString()):0);
				
				beanList.add(bean);
				
			}
		}
		}catch(Exception e){
			e.printStackTrace();
			return beanList;
		}
		return beanList;
		
	}
	/**
	 * This method is used to get exact sold and stock data.
	 * */
	private StockInBean getIndentEstimateSoldDataAndStock(long brandNo,String startDate,String endDate) {
		StockInBean bean = new StockInBean();
		String QUERY="SELECT ROUND(SUM(s.sale/p.packQty),0) AS sale"
				+ " FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`>='"+startDate+"'"
				+ "AND `saleDate`<='"+endDate+"' AND realBrandNo="+brandNo+"";
		String QUERY2="SELECT ROUND(COALESCE(SUM(s.closing/p.packQty),0) + (SELECT COALESCE(SUM(r.`receivedBottles`/p.packQty),0) FROM product p INNER JOIN "
				+ "invoice r ON p.`brandNoPackQty` = r.`brandNoPackQty` AND r.`invoiceDate` > '"+endDate+"' AND realBrandNo="+brandNo+"),0) AS inHouseStock "
						+ "FROM product p INNER JOIN sale s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND `saleDate`='"+endDate+"' AND realBrandNo="+brandNo+"";
		String QUERY3="SELECT COALESCE(ROUND(CEIL(SUM(s.receivedBottles/p.packQty))),0) AS liftedCase "
				+ "FROM product p INNER JOIN invoice s ON p.`brandNoPackQty` = s.`brandNoPackQty`"
				+ " AND `invoiceDateAsPerSheet`>=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+endDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01')) AND "
				+ " invoiceDateAsPerSheet <= (SELECT LAST_DAY('"+endDate+"')) AND p.realBrandNo="+brandNo+"";
		try{
			// cases = jdbcTemplate.queryForObject(QUERY, Double.class);
			 bean.setTotalPrice(jdbcTemplate.queryForObject(QUERY, Double.class));
			 bean.setStockInId(jdbcTemplate.queryForObject(QUERY2, Double.class));
			 bean.setQtyBtl(jdbcTemplate.queryForObject(QUERY3, Double.class));
			}catch(Exception e){
				e.printStackTrace();
				return bean;
			}
		return bean;
	}
	/**
	 * This method is used to get Indent estimate for a single product with all details
	 * */
	@Override
	public DiscountEstimationBean getDiscountEsitmateDataWithBrandNo(int brandNoVal,String inputdate, int noOfDays) {
		DiscountEstimationBean bean = new DiscountEstimationBean();
		DecimalFormat df = new DecimalFormat("#.###");
        df.setRoundingMode(RoundingMode.CEILING);
        String startDate=null,endDate=null,midileDate=null;
		try{
			List<String> dateList = jdbcTemplate.queryForList("SELECT DISTINCT saleDate FROM sale"
					+ " WHERE `saleDate` >= (SELECT DATE_ADD('"+inputdate+"', INTERVAL -60 DAY)) AND saleDate <='"+inputdate+"' ORDER BY saleDate DESC", String.class);
			
			Date date = new SimpleDateFormat("yyyy-MM-dd").parse(inputdate);
			DateFormat simpleDate=new SimpleDateFormat("E");
			String day=simpleDate.format(date);
			DateAndValueBean list = SaleDateUtil.getMatchSaleDate(dateList,day,noOfDays);
            //System.out.println(list);
            startDate = dateList.get(dateList.size()-1);
            endDate = dateList.get(0);
            midileDate = dateList.get(30);
		System.out.println("startDate>>>"+midileDate+"  endDate>>>"+endDate);
		String QUERY="SELECT DISTINCT p1.realBrandNo, p1.`shortBrandName`,p1.`category`,p1.productType,"
				+ "(SELECT d.target FROM stock_lift_with_discount d WHERE d.brandNo=p1.realBrandNo "
				+ "AND d.stockDiscountDate=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+inputdate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))) AS target,"
				+ "(SELECT d.targetCase FROM discount d WHERE d.brandNo=p1.realBrandNo AND d.estimateDate=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+endDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))) AS commitment,"
				+ "(SELECT t.color FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS color,"
				+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyOrder,"
				+ "(SELECT t.categoryName FROM `category_tab` t WHERE p1.`category`=t.`categoryId`)"
				+ " AS categoryName,p1.`company`,(SELECT t.companyName FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyName,"
				+ "ROUND(AVG((SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p1.brandNoPackQty ORDER BY `invoiceId` DESC LIMIT 1)),0) AS casePrice,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
				+ " WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') FROM invoice r , product p"
				+ "  WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2LStock,"
				+ "  (SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s ,"
				+ " product p WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') FROM invoice r , "
				+ "product p WHERE r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1LStock,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , "
				+ "product p WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='Q') FROM invoice r , "
				+ "product p WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='Q') AS QStock, "
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s ,"
				+ " product p WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') FROM invoice r , "
				+ "product p WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS PStock,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , "
				+ "product p WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') FROM invoice r ,"
				+ " product p WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS NStock,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
				+ "  WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') FROM invoice r , product p WHERE  "
				+ "   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS DStock,"
				+ "   (SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s ,"
				+ " product p WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') FROM invoice r ,"
				+ " product p WHERE r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SBStock,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p"
				+ " WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') FROM invoice r , product p WHERE"
				+ " r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LBStock,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s ,"
				+ "product p WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') FROM invoice r ,"
				+ "product p WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TINStock,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p WHERE"
				+ " s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') FROM invoice r , product p WHERE  "
				+ "r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty =p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS XStock,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ " AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2L,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1L,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo AND p.packType='Q') AS Q,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS P,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS N,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS D,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SB,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LB,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TIN,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "  AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS X,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"'"
				+ " AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2Lsale,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1Lsale,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo AND p.packType='Q') AS Qsale,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS Psale,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS Nsale,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS Dsale,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SBsale,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LBsale,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"' "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TINsale,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate>='"+midileDate+"' AND s.saleDate <='"+endDate+"'"
				+ "  AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS Xsale, "
				+ "(SELECT TRUNCATE(i.specialMargin*p.packQty,2) FROM invoice i , product p WHERE i.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L' ORDER BY i.`invoiceDateAsPerSheet` DESC LIMIT 1) AS 2LSpecilaMrp,"
				+ "(SELECT TRUNCATE(i.specialMargin*p.packQty,2) FROM invoice i , product p WHERE i.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L' ORDER BY i.`invoiceDateAsPerSheet` DESC LIMIT 1) AS 1LSpecilaMrp,"
				+ "(SELECT TRUNCATE(i.specialMargin*p.packQty,2) FROM invoice i , product p WHERE i.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='Q' ORDER BY i.`invoiceDateAsPerSheet` DESC LIMIT 1) AS QSpecilaMrp,"
				+ "(SELECT TRUNCATE(i.specialMargin*p.packQty,2) FROM invoice i , product p WHERE i.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P' ORDER BY i.`invoiceDateAsPerSheet` DESC LIMIT 1) AS PSpecilaMrp,"
				+ "(SELECT TRUNCATE(i.specialMargin*p.packQty,2) FROM invoice i , product p WHERE i.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N' ORDER BY i.`invoiceDateAsPerSheet` DESC LIMIT 1) AS NSpecilaMrp,"
				+ "(SELECT TRUNCATE(i.specialMargin*p.packQty,2) FROM invoice i , product p WHERE i.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D' ORDER BY i.`invoiceDateAsPerSheet` DESC LIMIT 1) AS DSpecilaMrp,"
				+ "(SELECT TRUNCATE(i.specialMargin*p.packQty,2) FROM invoice i , product p WHERE i.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB' ORDER BY i.`invoiceDateAsPerSheet` DESC LIMIT 1) AS SBSpecilaMrp,"
				+ "(SELECT TRUNCATE(i.specialMargin*p.packQty,2) FROM invoice i , product p WHERE i.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB' ORDER BY i.`invoiceDateAsPerSheet` DESC LIMIT 1) AS LBSpecilaMrp,"
				+ "(SELECT TRUNCATE(i.specialMargin*p.packQty,2) FROM invoice i , product p WHERE i.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN' ORDER BY i.`invoiceDateAsPerSheet` DESC LIMIT 1) AS TINSpecilaMrp,"
				+ "(SELECT TRUNCATE(i.specialMargin*p.packQty,2) FROM invoice i , product p WHERE i.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X' ORDER BY i.`invoiceDateAsPerSheet` DESC LIMIT 1) AS XSpecilaMrp "
				+ " FROM product p1 WHERE p1.realBrandNo="+brandNoVal+"";
		//System.out.println(QUERY);
		if(startDate != "" && startDate != null && endDate != "" && endDate !=null){
			
				List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
						bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
						bean.setProductType(row.get("productType")!=null?row.get("productType").toString():"");
						bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
						bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
						bean.setTarget(row.get("target")!=null?Long.parseLong(row.get("target").toString()):0);
						bean.setCommitment(row.get("target")!=null?Long.parseLong(row.get("target").toString()):0);
						StockInBean stockInBean = getIndentEstimateSoldDataAndStock(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0, midileDate, endDate);
						if(stockInBean.getTotalPrice() != null)
						bean.setLastMonthSold(stockInBean.getTotalPrice());
						else
							bean.setLastMonthSold((double) 0);
						bean.setInHouseStock(stockInBean.getStockInId());
						bean.setLiftedCase(stockInBean.getQtyBtl());
						bean.setCaseRate(row.get("casePrice")!=null?Double.parseDouble(row.get("casePrice").toString()):0);
						bean.setCompanyColor(row.get("color")!=null?row.get("color").toString():"");
						bean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
						
						bean.setL2Stock(row.get("2LStock")!=null?Double.parseDouble(row.get("2LStock").toString()):0);
						bean.setL1Stock(row.get("1LStock")!=null?Double.parseDouble(row.get("1LStock").toString()):0);
						bean.setQStock(row.get("QStock")!=null?Double.parseDouble(row.get("QStock").toString()):0);
						bean.setPStock(row.get("PStock")!=null?Double.parseDouble(row.get("PStock").toString()):0);
						bean.setNStock(row.get("NStock")!=null?Double.parseDouble(row.get("NStock").toString()):0);
						bean.setDStock(row.get("DStock")!=null?Double.parseDouble(row.get("DStock").toString()):0);
						bean.setSBStock(row.get("SBStock")!=null?Double.parseDouble(row.get("SBStock").toString()):0);
						bean.setLBStock(row.get("LBStock")!=null?Double.parseDouble(row.get("LBStock").toString()):0);
						bean.setTINStock(row.get("TINStock")!=null?Double.parseDouble(row.get("TINStock").toString()):0);
						bean.setXStock(row.get("XStock")!=null?Double.parseDouble(row.get("XStock").toString()):0);
						
						bean.setL2SpecialMargin(row.get("2LSpecilaMrp")!=null?Double.parseDouble(row.get("2LSpecilaMrp").toString()):0);
						bean.setL1SpecialMargin(row.get("1LSpecilaMrp")!=null?Double.parseDouble(row.get("1LSpecilaMrp").toString()):0);
						bean.setQSpecialMargin(row.get("QSpecilaMrp")!=null?Double.parseDouble(row.get("QSpecilaMrp").toString()):0);
						bean.setPSpecialMargin(row.get("PSpecilaMrp")!=null?Double.parseDouble(row.get("PSpecilaMrp").toString()):0);
						bean.setNSpecialMargin(row.get("NSpecilaMrp")!=null?Double.parseDouble(row.get("NSpecilaMrp").toString()):0);
						bean.setDSpecialMargin(row.get("DSpecilaMrp")!=null?Double.parseDouble(row.get("DSpecilaMrp").toString()):0);
						bean.setSBSpecialMargin(row.get("SBSpecilaMrp")!=null?Double.parseDouble(row.get("SBSpecilaMrp").toString()):0);
						bean.setLBSpecialMargin(row.get("LBSpecilaMrp")!=null?Double.parseDouble(row.get("LBSpecilaMrp").toString()):0);
						bean.setTINSpecialMargin(row.get("TINSpecilaMrp")!=null?Double.parseDouble(row.get("TINSpecilaMrp").toString()):0);
						bean.setXSpecialMargin(row.get("XSpecilaMrp")!=null?Double.parseDouble(row.get("XSpecilaMrp").toString()):0);
						
						bean.setL2(row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0);
						bean.setL1(row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0);
						bean.setQ(row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0);
						bean.setP(row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0);
						bean.setN(row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0);
						bean.setSB(row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0);
						bean.setD(row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0);
						bean.setLB(row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0);
						bean.setTIN(row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0);
						bean.setX(row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0);
						
						bean.setL2Sale(row.get("2Lsale")!=null?Double.parseDouble(row.get("2Lsale").toString()):0);
						bean.setL1Sale(row.get("1Lsale")!=null?Double.parseDouble(row.get("1Lsale").toString()):0);
						bean.setQSale(row.get("Qsale")!=null?Double.parseDouble(row.get("Qsale").toString()):0);
						bean.setPSale(row.get("Psale")!=null?Double.parseDouble(row.get("Psale").toString()):0);
						bean.setNSale(row.get("Nsale")!=null?Double.parseDouble(row.get("Nsale").toString()):0);
						bean.setSBSale(row.get("SBsale")!=null?Double.parseDouble(row.get("SBsale").toString()):0);
						bean.setDSale(row.get("Dsale")!=null?Double.parseDouble(row.get("Dsale").toString()):0);
						bean.setLBSale(row.get("LBsale")!=null?Double.parseDouble(row.get("LBsale").toString()):0);
						bean.setTINSale(row.get("TINsale")!=null?Double.parseDouble(row.get("TINsale").toString()):0);
						bean.setXSale(row.get("Xsale")!=null?Double.parseDouble(row.get("Xsale").toString()):0);
						bean.setPending(stockInBean.getQtyBtl());
						
						if((row.get("2Lsale")!=null?Double.parseDouble(row.get("2Lsale").toString()):0) > 0)
							bean.setL2perday((row.get("2LStock")!=null?Double.parseDouble(row.get("2LStock").toString()):0)/((row.get("2Lsale")!=null?Double.parseDouble(row.get("2Lsale").toString()):0)/30));
						else
							bean.setL2perday((double)0);
						if((row.get("1Lsale")!=null?Double.parseDouble(row.get("1Lsale").toString()):0) > 0)
							bean.setL1perday((row.get("1LStock")!=null?Double.parseDouble(row.get("1LStock").toString()):0)/((row.get("1Lsale")!=null?Double.parseDouble(row.get("1Lsale").toString()):0)/30));
						else
							bean.setL1perday((double)0);
						if((row.get("Qsale")!=null?Double.parseDouble(row.get("Qsale").toString()):0) > 0)
						    bean.setQperday((row.get("QStock")!=null?Double.parseDouble(row.get("QStock").toString()):0)/((row.get("Qsale")!=null?Double.parseDouble(row.get("Qsale").toString()):0)/30));
						else
							bean.setQperday((double)0);
						if((row.get("Psale")!=null?Double.parseDouble(row.get("Psale").toString()):0) > 0)
						    bean.setPperday((row.get("PStock")!=null?Double.parseDouble(row.get("PStock").toString()):0)/((row.get("Psale")!=null?Double.parseDouble(row.get("Psale").toString()):0)/30));
						else
							bean.setPperday((double)0);
						if((row.get("Nsale")!=null?Double.parseDouble(row.get("Nsale").toString()):0) > 0)
						    bean.setNperday((row.get("NStock")!=null?Double.parseDouble(row.get("NStock").toString()):0)/((row.get("Nsale")!=null?Double.parseDouble(row.get("Nsale").toString()):0)/30));
						else
							bean.setNperday((double)0);
						if((row.get("Dsale")!=null?Double.parseDouble(row.get("Dsale").toString()):0) > 0)
						    bean.setDperday((row.get("DStock")!=null?Double.parseDouble(row.get("DStock").toString()):0)/((row.get("Dsale")!=null?Double.parseDouble(row.get("Dsale").toString()):0)/30));
						else
							bean.setDperday((double)0);
						if((row.get("LBsale")!=null?Double.parseDouble(row.get("LBsale").toString()):0) > 0)
					    	bean.setLBperday((row.get("LBStock")!=null?Double.parseDouble(row.get("LBStock").toString()):0)/((row.get("LBsale")!=null?Double.parseDouble(row.get("LBsale").toString()):0)/30));
						else
							bean.setLBperday((double)0);
						if((row.get("SBsale")!=null?Double.parseDouble(row.get("SBsale").toString()):0) > 0)
						    bean.setSBperday((row.get("SBStock")!=null?Double.parseDouble(row.get("SBStock").toString()):0)/((row.get("SBsale")!=null?Double.parseDouble(row.get("SBsale").toString()):0)/30));
						else
							bean.setSBperday((double)0);
						if((row.get("Xsale")!=null?Double.parseDouble(row.get("Xsale").toString()):0) > 0)
						    bean.setXperday((row.get("XStock")!=null?Double.parseDouble(row.get("XStock").toString()):0)/((row.get("Xsale")!=null?Double.parseDouble(row.get("Xsale").toString()):0)/30));
						else
							bean.setXperday((double)0);
						if((row.get("TINsale")!=null?Double.parseDouble(row.get("TINsale").toString()):0) > 0)
						    bean.setTINperday((row.get("TINStock")!=null?Double.parseDouble(row.get("TINStock").toString()):0)/((row.get("TINsale")!=null?Double.parseDouble(row.get("TINsale").toString()):0)/30));
						else
							bean.setTINperday((double)0);
						
					}
				}
			}
		}catch(Exception e){
				e.printStackTrace();
				return bean;
			}
		
		// TODO Auto-generated method stub
		return bean;
	}
	/**
	 * This method is used to Save Indent estimate for some time of period
	 * */
	@Override
	public String saveTempIndent(JSONArray indentDetailsJson) {
		try{
		jdbcTemplate.update("DELETE FROM temp_indent_estimate");
		}catch(Exception e){e.printStackTrace();}
		 String QUERY="insert into temp_indent_estimate (brandNo,brandName,caseRate,noOfCases,target,lastMonthSold,companyOrder,companyColor,inHouseStock,commitment,productType,"
		 		+ "x,p,q,l2,l1,n,sb,d,lb,tin,l2SpecialMargin,l1SpecialMargin,qspecialMargin,pspecialMargin,nspecialMargin,dspecialMargin,sbspecialMargin,lbspecialMargin,tinspecialMargin,"
		 		+ "xspecialMargin,liftedCase,l2Stock,l1Stock,qstock,pstock,nstock,dstock,sbstock,lbstock,tinstock,xstock,date,days,company,category,pending,investMent,l2Val,l1Val,qVal,pVal,nVal,"
		 		+ "dVal,lbVal,sbVal,tinVal,xVal,totalSpecialMrp,l2NeedCase,l1NeedCase,qNeedCase,pNeedCase,nNeedCase,dNeedCase,lbNeedCase,"
		 		+ "sbNeedCase,tinNeedCase,xNeedCase,l2sale,l1sale,qsale,psale,nsale,dsale,sbsale,lbsale,tinsale,xsale,l2perday,l1perday,qperday,pperday,nperday,dperday,lbperday,sbperday,xperday,tinperday,flag) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		 String result=null;
			synchronized(this){
			try {
				for (int i = 0; i < indentDetailsJson.length(); ++i) {
				 JSONObject rec;
				    rec = indentDetailsJson.getJSONObject(i);
					int val=jdbcTemplate.update(QUERY,new Object[]{rec.getInt("brandNo"),rec.getString("brandName"),rec.getDouble("caseRate"),rec.getInt("needCase"),rec.getInt("target"),rec.getInt("lastMonthSold"),
							rec.getInt("companyOrder"),rec.getString("companyColor"),rec.getInt("inhousestock"),rec.getInt("commitment"),rec.getString("productType"),rec.getDouble("x"),rec.getDouble("p"),rec.getDouble("q"),rec.getDouble("l2"),rec.getDouble("l1"),rec.getDouble("n"),
							rec.getDouble("sb"),rec.getDouble("d"),rec.getDouble("lb"),rec.getDouble("tin"),rec.getDouble("l2SpecialMargin"),rec.getDouble("l1SpecialMargin"),
							rec.getDouble("qSpecialMargin"),rec.getDouble("pSpecialMargin"),rec.getDouble("nSpecialMargin"),rec.getDouble("dSpecialMargin"),rec.getDouble("sbSpecialMargin"),rec.getDouble("lbSpecialMargin"),
							rec.getDouble("tinSpecialMargin"),rec.getDouble("xSpecialMargin"),rec.getInt("liftedCase"),rec.getDouble("l2Stock"),rec.getDouble("l1Stock"),
							rec.getDouble("qStock"),rec.getDouble("pStock"),rec.getDouble("nStock"),rec.getDouble("dStock"),rec.getDouble("sbStock"),rec.getDouble("lbStock"),
							rec.getDouble("tinStock"),rec.getDouble("xStock"),rec.getString("date"),rec.getInt("noOfDays"),rec.getString("company"),rec.getString("category"),
							rec.getInt("pending"),rec.getInt("investMent"),rec.getInt("l2Val"),rec.getInt("l1Val"),rec.getInt("qVal"),rec.getInt("pVal"),rec.getInt("nVal"),rec.getInt("dVal"),rec.getInt("lbVal"),rec.getInt("sbVal"),rec.getInt("tinVal"),0,rec.getDouble("totalSpecialMrp"),
							rec.getInt("l2NeedCase"),rec.getInt("l1NeedCase"),rec.getInt("qNeedCase"),rec.getInt("pNeedCase"),rec.getInt("nNeedCase"),rec.getInt("dNeedCase"),
							rec.getInt("lbNeedCase"),rec.getInt("sbNeedCase"),rec.getInt("tinNeedCase"),rec.getInt("xNeedCase"),
							rec.getDouble("l2sale"),rec.getDouble("l1sale"),rec.getDouble("qsale"),rec.getDouble("psale"),rec.getDouble("nsale"),rec.getDouble("dsale"),
							rec.getDouble("sbsale"),rec.getDouble("lbsale"),
							rec.getDouble("tinsale"),rec.getDouble("xsale"),rec.getDouble("l2perday"),rec.getDouble("l1perday"),rec.getDouble("qperday"),rec.getDouble("pperday"),
							rec.getDouble("nperday"),rec.getDouble("dperday"),rec.getDouble("lbperday"),rec.getDouble("sbperday"),rec.getDouble("xperday"),rec.getDouble("tinperday"),false});
					result="Indent Updated successfully";
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				result="Field should not be empty!";
				log.log(Level.WARN, "saveTempIndent in PoizonDaoImpl exception", e);
				e.printStackTrace();
			}
			}
			return result;
	}
	/*This method is used to get Date and no. of days from 'tempIndenttab'*/
	@Override
	public MessageBean getDateAndDays() {
		String QUERY = "SELECT DISTINCT days, DATE_FORMAT(DATE,'%d-%m-%Y') AS DATE FROM temp_indent_estimate where store_id="+userSession.getStoreId()+" LIMIT 1";
		MessageBean bean = new MessageBean();
		 List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
		 if(rows != null && rows.size()>0) {
				for(Map row : rows){
					bean.setMessage(row.get("DATE")!=null?row.get("DATE").toString():"");
					bean.setCustomerId(row.get("days")!=null?Long.parseLong(row.get("days").toString()):0);
				}
		 }
		
		
		return bean;
	}
	/**
	 * This method is to distinct categories, pack qty, product type and pack type.
	 * */
	@Override
	public DropDownsBean getDropDownListFromProduct() {
		DropDownsBean bean = new DropDownsBean();
		 bean.setQuantity(jdbcTemplate.queryForList("SELECT DISTINCT `quantity` FROM `product`",String.class));
		 bean.setProductType(jdbcTemplate.queryForList("SELECT DISTINCT `productType` FROM `product`",String.class));
		 bean.setCaseType(jdbcTemplate.queryForList("SELECT DISTINCT `packType` FROM `product`",String.class));
		 bean.setPackQty(jdbcTemplate.queryForList("SELECT DISTINCT `packQty` FROM `product`",Long.class));
		return bean;
	}
/**
 * This method is used to get indent for one Day
 * */
	@Override
	public DiscountEstimationWithDate downloadIndentProduct(int days, String indentDate) {
		System.out.println("Khagesh:::::::");
		DecimalFormat df = new DecimalFormat("#.###");
        df.setRoundingMode(RoundingMode.CEILING);
		DiscountEstimationWithDate discountEstimationWithDate = new DiscountEstimationWithDate();
		String startDate=null,endDate=null,midileDate=null;
		List<DiscountEstimationBean> estimationBeanList = new ArrayList<DiscountEstimationBean>();
		try{
			discountEstimationWithDate.setDate(indentDate);
			List<String> dateList = jdbcTemplate.queryForList("SELECT DISTINCT saleDate FROM sale"
					+ " WHERE `saleDate` >= (SELECT DATE_ADD('"+indentDate+"', INTERVAL -60 DAY)) AND saleDate <='"+indentDate+"' ORDER BY saleDate DESC", String.class);
			
			Date date = new SimpleDateFormat("yyyy-MM-dd").parse(indentDate);
			DateFormat simpleDate=new SimpleDateFormat("E");
			String day=simpleDate.format(date);
			DateAndValueBean list = SaleDateUtil.getMatchSaleDate(dateList,day,days);
            //System.out.println(list.getDate()+" ::::::::::::::: "+list.getValue());
            startDate = dateList.get(dateList.size()-1);
            endDate = dateList.get(0);
            midileDate = dateList.get(30);
		System.out.println("midileDate>>>"+midileDate+"  endDate>>>"+endDate);
		String QUERY="SELECT DISTINCT p1.realBrandNo, p1.`shortBrandName`,p1.`category`,p1.productType,"
				+ "(SELECT d.target FROM stock_lift_with_discount d WHERE d.brandNo=p1.realBrandNo "
				+ "AND d.stockDiscountDate=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+indentDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))) AS target,"
				+ "(SELECT d.targetCase FROM discount d WHERE d.brandNo=p1.realBrandNo AND d.estimateDate=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+endDate+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))) AS commitment,"
				+ "IF( p1.productType ='LIQUOR', '#D6EAF8', '#DAFAC4') AS color,"
				+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyOrder,"
				+ "(SELECT t.categoryName FROM `category_tab` t WHERE p1.`category`=t.`categoryId`)"
				+ " AS categoryName,p1.`company`,(SELECT t.companyName FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyName,"
				+ "ROUND(AVG((SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p1.brandNoPackQty ORDER BY `invoiceId` DESC LIMIT 1)),0) AS casePrice,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , "
				+ "product p WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') FROM invoice r ,"
				+ " product p WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2LStock,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
				+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') FROM invoice r , product p WHERE "
				+ "r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1LStock,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
				+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='Q') FROM invoice r , product p WHERE "
				+ "  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='Q') AS QStock, "
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
				+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') FROM invoice r , product p WHERE "
				+ "  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS PStock,(SELECT "
				+ "TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p"
				+ " WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') FROM invoice r , product p "
				+ "WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS NStock,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
				+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') FROM invoice r , product p WHERE  "
				+ " r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS DStock,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s ,"
				+ " product p WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') FROM invoice r ,"
				+ " product p WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SBStock,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
				+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') FROM invoice r , product p WHERE "
				+ "  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LBStock,"
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
				+ "WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') FROM invoice r , product p WHERE "
				+ "  r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TINStock, "
				+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s ,"
				+ " product p WHERE s.saleDate ='"+endDate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') FROM invoice r ,"
				+ " product p WHERE   r.`invoiceDate` > '"+endDate+"'  AND r.brandNoPackQty =p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS XStock,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ " AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2L,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1L,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo AND p.packType='Q') AS Q,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS P,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS N,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS D,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SB,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LB,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TIN,"
				+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate IN ("+list.getDate()+") "
				+ "  AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS X "
				+ " FROM product p1 WHERE p1.`active`=1 GROUP BY p1.realBrandNo ORDER BY p1.realBrandNo ASC";
		//System.out.println(QUERY);
		if(startDate != "" && startDate != null && endDate != "" && endDate !=null){
			
				List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
				if(rows != null && rows.size()>0) {
					for(Map row : rows){
						DiscountEstimationBean bean = new DiscountEstimationBean();
						bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
						bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
						bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
						bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
						bean.setProductType(row.get("productType")!=null?row.get("productType").toString():"");
						bean.setTarget(row.get("target")!=null?Long.parseLong(row.get("target").toString()):0);
						bean.setCommitment(row.get("target")!=null?Long.parseLong(row.get("target").toString()):0);
						bean.setCaseRate(row.get("casePrice")!=null?Double.parseDouble(row.get("casePrice").toString()):0);
						bean.setCompanyColor(row.get("color")!=null?row.get("color").toString():"");
						bean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
						
						bean.setL2Stock(row.get("2LStock")!=null?Double.parseDouble(row.get("2LStock").toString()):0);
						bean.setL1Stock(row.get("1LStock")!=null?Double.parseDouble(row.get("1LStock").toString()):0);
						bean.setQStock(row.get("QStock")!=null?Double.parseDouble(row.get("QStock").toString()):0);
						bean.setPStock(row.get("PStock")!=null?Double.parseDouble(row.get("PStock").toString()):0);
						bean.setNStock(row.get("NStock")!=null?Double.parseDouble(row.get("NStock").toString()):0);
						bean.setDStock(row.get("DStock")!=null?Double.parseDouble(row.get("DStock").toString()):0);
						bean.setSBStock(row.get("SBStock")!=null?Double.parseDouble(row.get("SBStock").toString()):0);
						bean.setLBStock(row.get("LBStock")!=null?Double.parseDouble(row.get("LBStock").toString()):0);
						bean.setTINStock(row.get("TINStock")!=null?Double.parseDouble(row.get("TINStock").toString()):0);
						bean.setXStock(row.get("XStock")!=null?Double.parseDouble(row.get("XStock").toString()):0);
						
						
						bean.setL2(row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0);
						bean.setL1(row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0);
						bean.setQ(row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0);
						bean.setP(row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0);
						bean.setN(row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0);
						bean.setSB(row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0);
						bean.setD(row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0);
						bean.setLB(row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0);
						bean.setTIN(row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0);
						bean.setX(row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0);
						double needCases = 0;
						
						if((row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0) > 0){
						    double value = Math.ceil(((row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0)/list.getValue())*(days- ((row.get("PStock")!=null?Double.parseDouble(row.get("PStock").toString()):0)/((row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0)/list.getValue()))));
						    if(value > 0){
							needCases +=value;
							bean.setpNeedCase((long) value);
						    }else
						    	bean.setpNeedCase((long) 0);
						}else{
							bean.setpNeedCase((long) 0);
						}
						if((row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0) > 0){
							double value = Math.ceil(((row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0)/list.getValue())*(days- ((row.get("QStock")!=null?Double.parseDouble(row.get("QStock").toString()):0)/((row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0)/list.getValue()))));
							if(value > 0){
								needCases +=value;
								bean.setqNeedCase((long) value);
							}else
						    	bean.setqNeedCase((long) 0);
						}else{
							bean.setqNeedCase((long) 0);
						}
						if((row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0) > 0){
							double value = Math.ceil(((row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0)/list.getValue())*(days- ((row.get("2LStock")!=null?Double.parseDouble(row.get("2LStock").toString()):0)/((row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0)/list.getValue()))));
							if(value > 0){
								needCases +=value;
								bean.setL2NeedCase((long) value);
							}else
								bean.setL2NeedCase((long) 0);
						}else{
							bean.setL2NeedCase((long) 0);
						}
						if((row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0) > 0){
							double value = Math.ceil(((row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0)/list.getValue())*(days- ((row.get("1LStock")!=null?Double.parseDouble(row.get("1LStock").toString()):0)/((row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0)/list.getValue()))));
							if(value > 0){
								needCases +=value;
								bean.setL1NeedCase((long) value);
							}else
								bean.setL1NeedCase((long) 0);
						}else{
							bean.setL1NeedCase((long) 0);
						}
						if((row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0) > 0){
							double value = Math.ceil(((row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0)/list.getValue())*(days- ((row.get("NStock")!=null?Double.parseDouble(row.get("NStock").toString()):0)/((row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0)/list.getValue()))));
							if(value > 0){
								needCases +=value;
								bean.setnNeedCase((long) value);
							}else
								bean.setnNeedCase((long) 0);
						}else{
							bean.setnNeedCase((long) 0);
						}
						if((row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0) > 0){
							double value = Math.ceil(((row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0)/list.getValue())*(days- ((row.get("SBStock")!=null?Double.parseDouble(row.get("SBStock").toString()):0)/((row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0)/list.getValue()))));
							if(value > 0){
								bean.setSbNeedCase((long) value);
							needCases +=value;
							}else
								bean.setSbNeedCase((long) 0);
						}else{
							bean.setSbNeedCase((long) 0);
						}
						if((row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0) > 0){
							double value = Math.ceil(((row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0)/list.getValue())*(days- ((row.get("DStock")!=null?Double.parseDouble(row.get("DStock").toString()):0)/((row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0)/list.getValue()))));
							if(value > 0){
								needCases +=value;
								bean.setdNeedCase((long) value);
							}else
								bean.setdNeedCase((long) 0);
						}else{
							bean.setdNeedCase((long) 0);
						}
						if((row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0) > 0){
							double value = Math.ceil(((row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0)/list.getValue())*(days- ((row.get("LBStock")!=null?Double.parseDouble(row.get("LBStock").toString()):0)/((row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0)/list.getValue()))));
							if(value > 0){
								needCases +=value;
								bean.setLbNeedCase((long) value);
							}else
								bean.setLbNeedCase((long) 0);
						}else{
							bean.setLbNeedCase((long) 0);
						}
						if((row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0) > 0){
							double value = Math.ceil(((row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0)/list.getValue())*(days- ((row.get("TINStock")!=null?Double.parseDouble(row.get("TINStock").toString()):0)/((row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0)/list.getValue()))));
							if(value > 0){
								needCases +=value;
								bean.setTinNeedCase((long) value);
							}else
								bean.setTinNeedCase((long) 0);
						}else{
							bean.setTinNeedCase((long) 0);
						}
						if((row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0) > 0){
							double value = Math.ceil(((row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0)/list.getValue())*(days- ((row.get("XStock")!=null?Double.parseDouble(row.get("XStock").toString()):0)/((row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0)/list.getValue()))));
							if(value > 0){
								needCases +=value;
								bean.setxNeedCase((long) value);
							}else
								bean.setxNeedCase((long) 0);
						}else{
							bean.setxNeedCase((long) 0);
						}
						bean.setNeedCase((double) Math.round(needCases));
						estimationBeanList.add(bean);
						
					}
				}
			}
		 discountEstimationWithDate.setDiscountEstimationBean(estimationBeanList);
		 discountEstimationWithDate.setFlag(true);
		}catch(Exception e){
				e.printStackTrace();
				return discountEstimationWithDate;
			}
		
		// TODO Auto-generated method stub
		return discountEstimationWithDate;
	}
	/**
	 * This method is used to get distinct sale date list from 'sale' table where no. of sale is greater than zero.
	 * */
	@Override
	public List<InvoiceDateBean> getSaleReportSaleDate() {
		String Query="SELECT DISTINCT saleDate FROM sale WHERE totalPrice > 0";
		List<InvoiceDateBean> invoiceDateList = new ArrayList<InvoiceDateBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(Query);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					InvoiceDateBean invoiceDateBean = new InvoiceDateBean();
					invoiceDateBean.setDate(row.get("saleDate").toString());
					invoiceDateList.add(invoiceDateBean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return invoiceDateList;
		}finally {
			
		}
		return invoiceDateList;
	}
	/* checking for the selected date is zero sale or more than zero*/
	@Override
	public boolean saveSaleWithZeroOrNot(String date) {
		int value = 0;
		String QUERY = "SELECT SUM(totalPrice) FROM sale WHERE saleDate='" + date + "'";
		try {
			value = jdbcTemplate.queryForObject(QUERY, Integer.class);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		if (value > 0)
			return true;
		else
			return false;
	}
	@Override
	public String updateLatestMrpForAllProducts(JSONArray invoiceJson) {
		log.info("PoizonDaoImpl.updateLatestMrpForAllProducts invoiceJson= "+invoiceJson);
		String QUERY="insert into invoice (brandNoPackQty,caseQty,packQty,QtyBottels,SingleBottelRate,packQtyRate,bottleSaleMrp,totalPrice,receivedBottles,invoiceDate,invoiceDateAsPerSheet) values(?,?,?,?,?,?,?,?,?,?,?)";
		String result=null;
		synchronized(this){
		try {
			for (int i = 0; i < invoiceJson.length(); ++i) {
			 JSONObject rec;
			    rec = invoiceJson.getJSONObject(i);
			    try{
					int val=jdbcTemplate.update(QUERY,new Object[]{rec.getDouble("brandNoPackQtyId"),0,rec.getInt("packQtyId"),0,rec.getDouble("SingleBottelRate"),rec.getDouble("packQtyRate"),rec.getDouble("EachBottleMrp"),0,0,rec.get("date"),rec.get("date")});
						if(val>0)
							result="Mrp updated Successfully";
				}catch(Exception e){
					result="something went wrong";
					log.log(Level.WARN, "updateLatestMrpForAllProducts in PoizonDaoImpl exception", e);
					e.printStackTrace();
					}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			result="Something Went Wrong.";
			log.log(Level.WARN, "updateLatestMrpForAllProducts in PoizonDaoImpl exception", e);
			e.printStackTrace();
		}
		}
		return result;}
	@Override
	public List<SchemeEstimateBean> getSchemeData(String date) {
	log.info("PoizinDaoImpl: getScheme() date:: "+date);
	String systemDate = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDateTime.now());
	 DecimalFormat decimalFormat = new DecimalFormat("#.#");
	String enddate = jdbcTemplate.queryForObject("SELECT saleDate FROM sale ORDER BY saleDate DESC LIMIT 1", String.class);
	String startdate = jdbcTemplate.queryForObject("SELECT DATE_ADD('"+enddate+"', INTERVAL -29 DAY)", String.class);
	System.out.println("startdate :: "+startdate+ " enddate:: "+enddate);
	List<SchemeEstimateBean> estimationBeanList = new ArrayList<SchemeEstimateBean>();
	String QUERY="SELECT DISTINCT p1.realBrandNo, p1.`shortBrandName`,p1.`category`,p1.productType,p1.productGrouping,"
			+ "(SELECT d.target FROM stock_lift_with_discount d WHERE d.brandNo=p1.realBrandNo "
			+ "AND d.stockDiscountDate=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+date+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))) AS target,"
			+ "(SELECT d.currentIndent FROM stock_lift_with_discount d WHERE d.brandNo=p1.realBrandNo AND systemDate='"+systemDate+"') AS currentIndent,"
			+ "(SELECT t.color FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS color,"
			+ "(SELECT t.companyOrder FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyOrder,"
			+ "(SELECT t.categoryName FROM `category_tab` t WHERE p1.`category`=t.`categoryId`)"
			+ " AS categoryName,p1.`company`,(SELECT t.companyName FROM `company_tab` t WHERE p1.`company`=t.`companyId`) AS companyName,"
			+ "ROUND(AVG((SELECT i.packQtyRate FROM invoice i WHERE i.brandNoPackQty=p1.brandNoPackQty ORDER BY `invoiceId` DESC LIMIT 1)),0) AS casePrice,"
			+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , "
			+ "product p WHERE s.saleDate ='"+enddate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') FROM invoice r ,"
			+ " product p WHERE   r.`invoiceDate` > '"+enddate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2LStock,"
			+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
			+ "WHERE s.saleDate ='"+enddate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') FROM invoice r , product p WHERE "
			+ "r.`invoiceDate` > '"+enddate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1LStock,"
			+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
			+ "WHERE s.saleDate ='"+enddate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='Q') FROM invoice r , product p WHERE "
			+ "  r.`invoiceDate` > '"+enddate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='Q') AS QStock, "
			+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
			+ "WHERE s.saleDate ='"+enddate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') FROM invoice r , product p WHERE "
			+ "  r.`invoiceDate` > '"+enddate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS PStock,(SELECT "
			+ "TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p"
			+ " WHERE s.saleDate ='"+enddate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') FROM invoice r , product p "
			+ "WHERE   r.`invoiceDate` > '"+enddate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS NStock,"
			+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
			+ "WHERE s.saleDate ='"+enddate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') FROM invoice r , product p WHERE  "
			+ " r.`invoiceDate` > '"+enddate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS DStock,"
			+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s ,"
			+ " product p WHERE s.saleDate ='"+enddate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') FROM invoice r ,"
			+ " product p WHERE   r.`invoiceDate` > '"+enddate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SBStock,"
			+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
			+ "WHERE s.saleDate ='"+enddate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') FROM invoice r , product p WHERE "
			+ "  r.`invoiceDate` > '"+enddate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LBStock,"
			+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s , product p "
			+ "WHERE s.saleDate ='"+enddate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') FROM invoice r , product p WHERE "
			+ "  r.`invoiceDate` > '"+enddate+"'  AND r.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TINStock, "
			+ "(SELECT TRUNCATE(COALESCE(SUM(r.receivedBottles)/p.packQty,0),2) + (SELECT TRUNCATE(COALESCE(SUM(s.`closing`)/p.packQty,0),2) FROM sale s ,"
			+ " product p WHERE s.saleDate ='"+enddate+"' AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') FROM invoice r ,"
			+ " product p WHERE   r.`invoiceDate` > '"+enddate+"'  AND r.brandNoPackQty =p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS XStock,"
			+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate >= '"+startdate+"' AND s.`saleDate` <='"+enddate+"' "
			+ " AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2L,"
			+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate >= '"+startdate+"' AND s.`saleDate` <='"+enddate+"' "
			+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1L,"
			+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate >= '"+startdate+"' AND s.`saleDate` <='"+enddate+"' "
			+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo AND p.packType='Q') AS Q,"
			+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate >= '"+startdate+"' AND s.`saleDate` <='"+enddate+"' "
			+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS P,"
			+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate >= '"+startdate+"' AND s.`saleDate` <='"+enddate+"' "
			+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS N,"
			+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate >= '"+startdate+"' AND s.`saleDate` <='"+enddate+"' "
			+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS D,"
			+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate >= '"+startdate+"' AND s.`saleDate` <='"+enddate+"' "
			+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SB,"
			+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate >= '"+startdate+"' AND s.`saleDate` <='"+enddate+"' "
			+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LB,"
			+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2)  FROM sale s , product p WHERE s.saleDate >= '"+startdate+"' AND s.`saleDate` <='"+enddate+"' "
			+ "AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TIN,"
			+ "(SELECT TRUNCATE(SUM(s.`sale`)/p.packQty,2) FROM sale s , product p WHERE s.saleDate >= '"+startdate+"' AND s.`saleDate` <='"+enddate+"' "
			+ "  AND s.brandNoPackQty=p.brandNoPackQty AND p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS X,"
			+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='2L') AS 2LSpecilaMrp,"
			+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='1L') AS 1LSpecilaMrp,"
			+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='Q') AS QSpecilaMrp,"
			+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='P') AS PSpecilaMrp,"
			+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='N') AS NSpecilaMrp,"
			+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='D') AS DSpecilaMrp,"
			+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='SB') AS SBSpecilaMrp,"
			+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='LB') AS LBSpecilaMrp,"
			+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='TIN') AS TINSpecilaMrp,"
			+ "(SELECT TRUNCATE(SUM(p.specialMargin)*p.packQty,2) FROM product p WHERE p.realBrandNo=p1.realBrandNo  AND p.packType='X') AS XSpecilaMrp"
			+ " FROM product p1 WHERE p1.`active`=1 GROUP BY p1.realBrandNo ORDER BY p1.realBrandNo ASC";
	//System.out.println(QUERY);
	try{
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
		if(rows != null && rows.size()>0) {
			for(Map row : rows){
				       SchemeEstimateBean bean = new SchemeEstimateBean();
				       StockInBean stockInBean = getIndentEstimateSoldDataAndStock(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0, startdate, enddate);
			        	bean.setLastMonthSold(stockInBean.getTotalPrice());
				        bean.setInHouseStock(stockInBean.getStockInId());
				        bean.setLiftedCase(stockInBean.getQtyBtl());
					    double noOfCase = 0;
					    if((row.get("currentIndent")!=null?Long.parseLong(row.get("currentIndent").toString()):0) > 0)
					    	noOfCase =  row.get("currentIndent")!=null?Long.parseLong(row.get("currentIndent").toString()):0;
					    else
					        noOfCase = (row.get("target")!=null?Long.parseLong(row.get("target").toString()):0) - stockInBean.getQtyBtl();
					    
					bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
					bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
					bean.setCategory(row.get("categoryName")!=null?row.get("categoryName").toString():"");
					bean.setCompany(row.get("companyName")!=null?row.get("companyName").toString():"");
					bean.setProductType(row.get("productType")!=null?row.get("productType").toString():"");
					bean.setTarget(row.get("target")!=null?Long.parseLong(row.get("target").toString()):0);
					bean.setPending((row.get("target")!=null?Long.parseLong(row.get("target").toString()):0) - stockInBean.getQtyBtl());
					bean.setNoOfCases(noOfCase);
					bean.setCaseRate(row.get("casePrice")!=null?Double.parseDouble(row.get("casePrice").toString()):0);
					bean.setCompanyColor(row.get("color")!=null?row.get("color").toString():"");
					//bean.setCompanyOrder(row.get("companyorder")!=null?Long.parseLong(row.get("companyorder").toString()):0);
					bean.setCompanyOrder(row.get("productGrouping")!=null?Long.parseLong(row.get("productGrouping").toString()):0);
					
					bean.setL2Stock(row.get("2LStock")!=null?Double.parseDouble(row.get("2LStock").toString()):0);
					bean.setL1Stock(row.get("1LStock")!=null?Double.parseDouble(row.get("1LStock").toString()):0);
					bean.setQStock(row.get("QStock")!=null?Double.parseDouble(row.get("QStock").toString()):0);
					bean.setPStock(row.get("PStock")!=null?Double.parseDouble(row.get("PStock").toString()):0);
					bean.setNStock(row.get("NStock")!=null?Double.parseDouble(row.get("NStock").toString()):0);
					bean.setDStock(row.get("DStock")!=null?Double.parseDouble(row.get("DStock").toString()):0);
					bean.setSBStock(row.get("SBStock")!=null?Double.parseDouble(row.get("SBStock").toString()):0);
					bean.setLBStock(row.get("LBStock")!=null?Double.parseDouble(row.get("LBStock").toString()):0);
					bean.setTINStock(row.get("TINStock")!=null?Double.parseDouble(row.get("TINStock").toString()):0);
					bean.setXStock(row.get("XStock")!=null?Double.parseDouble(row.get("XStock").toString()):0);
					
					bean.setL2SpecialMargin(row.get("2LSpecilaMrp")!=null?Double.parseDouble(row.get("2LSpecilaMrp").toString()):0);
					bean.setL1SpecialMargin(row.get("1LSpecilaMrp")!=null?Double.parseDouble(row.get("1LSpecilaMrp").toString()):0);
					bean.setQSpecialMargin(row.get("QSpecilaMrp")!=null?Double.parseDouble(row.get("QSpecilaMrp").toString()):0);
					bean.setPSpecialMargin(row.get("PSpecilaMrp")!=null?Double.parseDouble(row.get("PSpecilaMrp").toString()):0);
					bean.setNSpecialMargin(row.get("NSpecilaMrp")!=null?Double.parseDouble(row.get("NSpecilaMrp").toString()):0);
					bean.setDSpecialMargin(row.get("DSpecilaMrp")!=null?Double.parseDouble(row.get("DSpecilaMrp").toString()):0);
					bean.setSBSpecialMargin(row.get("SBSpecilaMrp")!=null?Double.parseDouble(row.get("SBSpecilaMrp").toString()):0);
					bean.setLBSpecialMargin(row.get("LBSpecilaMrp")!=null?Double.parseDouble(row.get("LBSpecilaMrp").toString()):0);
					bean.setTINSpecialMargin(row.get("TINSpecilaMrp")!=null?Double.parseDouble(row.get("TINSpecilaMrp").toString()):0);
					bean.setXSpecialMargin(row.get("XSpecilaMrp")!=null?Double.parseDouble(row.get("XSpecilaMrp").toString()):0);
					
					bean.setL2(row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0);
					bean.setL1(row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0);
					bean.setQ(row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0);
					bean.setP(row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0);
					bean.setN(row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0);
					bean.setSB(row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0);
					bean.setD(row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0);
					bean.setLB(row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0);
					bean.setTIN(row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0);
					bean.setX(row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0);
					
					if(noOfCase > 0){
					
					double totalSoldCase = (row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0) + (row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0) +
							               (row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0) + (row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0) +
							               (row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0) + (row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0) +
							               (row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0) + (row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0) +
							               (row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0) + (row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0);
                     
					bean.setL2NeedCase((long)Math.round(CommonUtil.splitScheme((row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0), totalSoldCase,noOfCase)));
					bean.setL1NeedCase((long)Math.round(CommonUtil.splitScheme((row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0), totalSoldCase,noOfCase)));
					bean.setqNeedCase((long)Math.round(CommonUtil.splitScheme((row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0), totalSoldCase,noOfCase)));
					bean.setpNeedCase((long)Math.round(CommonUtil.splitScheme((row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0), totalSoldCase,noOfCase)));
					bean.setnNeedCase((long)Math.round(CommonUtil.splitScheme((row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0), totalSoldCase,noOfCase)));
					bean.setdNeedCase((long)Math.round(CommonUtil.splitScheme((row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0), totalSoldCase,noOfCase)));
					bean.setLbNeedCase((long)Math.round(CommonUtil.splitScheme((row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0), totalSoldCase,noOfCase)));
					bean.setSbNeedCase((long)Math.round(CommonUtil.splitScheme((row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0), totalSoldCase,noOfCase)));
					bean.setTinNeedCase((long)Math.round(CommonUtil.splitScheme((row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0), totalSoldCase,noOfCase)));
					bean.setxNeedCase((long)Math.round(CommonUtil.splitScheme((row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0), totalSoldCase,noOfCase)));
					bean.setOtherInvestment(Math.round((noOfCase) * (row.get("casePrice")!=null?Double.parseDouble(row.get("casePrice").toString()):0)));
					
					if((row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0) > 0)
						bean.setL2perday(Double.parseDouble(decimalFormat.format((row.get("2LStock")!=null?Double.parseDouble(row.get("2LStock").toString()):0)/((row.get("2L")!=null?Double.parseDouble(row.get("2L").toString()):0)/30))));
					else
						bean.setL2perday((double)0);
					if((row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0) > 0)
						bean.setL1perday(Double.parseDouble(decimalFormat.format((row.get("1LStock")!=null?Double.parseDouble(row.get("1LStock").toString()):0)/((row.get("1L")!=null?Double.parseDouble(row.get("1L").toString()):0)/30))));
					else
						bean.setL1perday((double)0);
					if((row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0) > 0)
					    bean.setQperday(Double.parseDouble(decimalFormat.format((row.get("QStock")!=null?Double.parseDouble(row.get("QStock").toString()):0)/((row.get("Q")!=null?Double.parseDouble(row.get("Q").toString()):0)/30))));
					else
						bean.setQperday((double)0);
					if((row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0) > 0)
					    bean.setPperday(Double.parseDouble(decimalFormat.format((row.get("PStock")!=null?Double.parseDouble(row.get("PStock").toString()):0)/((row.get("P")!=null?Double.parseDouble(row.get("P").toString()):0)/30))));
					else
						bean.setPperday((double)0);
					if((row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0) > 0)
					    bean.setNperday(Double.parseDouble(decimalFormat.format((row.get("NStock")!=null?Double.parseDouble(row.get("NStock").toString()):0)/((row.get("N")!=null?Double.parseDouble(row.get("N").toString()):0)/30))));
					else
						bean.setNperday((double)0);
					if((row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0) > 0)
					    bean.setDperday(Double.parseDouble(decimalFormat.format((row.get("DStock")!=null?Double.parseDouble(row.get("DStock").toString()):0)/((row.get("D")!=null?Double.parseDouble(row.get("D").toString()):0)/30))));
					else
						bean.setDperday((double)0);
					if((row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0) > 0)
				    	bean.setLBperday(Double.parseDouble(decimalFormat.format((row.get("LBStock")!=null?Double.parseDouble(row.get("LBStock").toString()):0)/((row.get("LB")!=null?Double.parseDouble(row.get("LB").toString()):0)/30))));
					else
						bean.setLBperday((double)0);
					if((row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0) > 0)
					    bean.setSBperday(Double.parseDouble(decimalFormat.format((row.get("SBStock")!=null?Double.parseDouble(row.get("SBStock").toString()):0)/((row.get("SB")!=null?Double.parseDouble(row.get("SB").toString()):0)/30))));
					else
						bean.setSBperday((double)0);
					if((row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0) > 0)
					    bean.setXperday(Double.parseDouble(decimalFormat.format((row.get("XStock")!=null?Double.parseDouble(row.get("XStock").toString()):0)/((row.get("X")!=null?Double.parseDouble(row.get("X").toString()):0)/30))));
					else
						bean.setXperday((double)0);
					if((row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0) > 0)
					    bean.setTINperday(Double.parseDouble(decimalFormat.format((row.get("TINStock")!=null?Double.parseDouble(row.get("TINStock").toString()):0)/((row.get("TIN")!=null?Double.parseDouble(row.get("TIN").toString()):0)/30))));
					else
						bean.setTINperday((double)0);
					
					bean.setTotalSoldCases(totalSoldCase);
					bean.setTotalSpecialMrp((bean.getL2NeedCase() * bean.getL2SpecialMargin()) + (bean.getL1NeedCase() * bean.getL1SpecialMargin())
							+ (bean.getqNeedCase() * bean.getQSpecialMargin()) + (bean.getpNeedCase() * bean.getPSpecialMargin()) + (bean.getnNeedCase() * bean.getNSpecialMargin())
							+ (bean.getdNeedCase() * bean.getDSpecialMargin()) + (bean.getLbNeedCase() * bean.getLBSpecialMargin()) + (bean.getSbNeedCase() * bean.getSBSpecialMargin())
							+ (bean.getTinNeedCase() * bean.getTINSpecialMargin()));
					
					estimationBeanList.add(bean);
					}
				}
			}
	}catch(Exception e){
		log.error("Exception in PoizinDaoImpl get scheme() :: "+e);
		return estimationBeanList;
	}
		return estimationBeanList;
	}
	/**
	 * This method is used to save expenses from mobile or tab
	 * */
	@Override
	public String saveTabExpenses(JSONObject expenseJson) {
    log.info("PoizinController :: saveTabExpenses() ");

	log.info("PoizonDaoImpl.saveExpenses expenseJson= "+expenseJson);
	String QUERY1="INSERT INTO expense_master(totalAmount,expenseMasterDate) VALUES(?,?)";
	String QUERY2="SELECT expenseMasterId FROM expense_master ORDER BY expenseMasterDate DESC LIMIT 1";
	String QUERY3="INSERT INTO expense_child (categoryId,expenseMasterId,expenseChildAmount,COMMENT) VALUES(?,?,?,?)";
	String result=null;
	String sdate=null;
	int masterId = 0;
	synchronized(this){
	try{
				
		int val = jdbcTemplate.update(QUERY1,new Object[]{expenseJson.getDouble("total"),expenseJson.getString("eDate")});
		if(val > 0){
			masterId = jdbcTemplate.queryForObject(QUERY2, Integer.class);
			JSONArray expenseJsonArr = expenseJson.getJSONArray("expenseDetails");
			for (int i = expenseJsonArr.length()-1; i >= 0; --i) {
				JSONObject rec = expenseJsonArr.getJSONObject(i);
				jdbcTemplate.update(QUERY3,new Object[]{rec.getInt("categoryId"),masterId,rec.getDouble("amount"),rec.getString("comment")});
				
			}
		}
		
		result="Expenses Added Successfully";
	}catch(Exception e){
		result="Somethig went wrong";
		log.log(Level.WARN, "saveExpenses in PoizonDaoImpl exception", e);
		e.printStackTrace();
	}
	}
	return result;

	}
	@Override
	public double getSingleProductDetails(int brandNoPackQty, String date) {
          try{
        	  return jdbcTemplate.queryForObject("SELECT bottleSaleMrp FROM invoice WHERE `brandNoPackQty`="+brandNoPackQty+" AND store_id="+userSession.getStoreId()+" ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1", Double.class);
          }catch(Exception e){
        	  e.printStackTrace();
        	  return 0;
          }
	}
	/**
	 * This method is used to post credit data in to credit_tab
	 * */
	@Override
	public String postCreditData(JSONObject creditJson) {
	String result=null,QUERY1=null,QUERY2=null;int val1=0,val2=0;
	QUERY1 = "INSERT INTO credit_master(name,totalAmount,company,masterDate,paid,store_id) VALUES(?,?,?,?,?,?)";
	QUERY2="INSERT INTO credit_tab(cid,company,brandNoPackQty,name,price,cases,bottles,totalPrice,totalDiscount,discount,afterIncludingDiscount,date,paid,store_id) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	try{
		val1 = jdbcTemplate.update(QUERY1,new Object[]{creditJson.getString("name"),creditJson.getDouble("totalAmt"),Integer.parseInt(creditJson.getString("company")),
			creditJson.getString("date"),0,userSession.getStoreId()});
		if(val1 > 0){
		int cid = 	jdbcTemplate.queryForObject("SELECT cid FROM credit_master WHERE store_id = "+userSession.getStoreId()+" ORDER BY cid DESC LIMIT 1", Integer.class);
		for(int i=0; i< creditJson.getJSONArray("creditData").length(); i++){
			JSONObject rec = creditJson.getJSONArray("creditData").getJSONObject(i);
			 val2 = jdbcTemplate.update(QUERY2,new Object[]{cid,Integer.parseInt(rec.getString("company")),Double.parseDouble(rec.getString("brandNoPackQty")),rec.getString("name"),
					rec.getDouble("price"),rec.getInt("cases"),rec.getInt("bottles"),rec.getDouble("totalPrice"),rec.getDouble("totaldiscount"),rec.getDouble("discount"),rec.getDouble("afterIncludeDiscount"),rec.getString("date"),0,userSession.getStoreId()});
		}
		}
		if(val2 > 0){
			result ="Credit data saved successfully!";
		}else{
			result="Something went wrong";
		}
	}catch(Exception e){
		e.printStackTrace();
		log.error("Error :: "+e);
		result="Something went wrong";
	}
		return result;
	}
	@Override
	public List<CreditMasterBean> getExistingCredit() {
		List<CreditMasterBean> listdata = new ArrayList<CreditMasterBean>();
		String QUERY1 = "SELECT cid,name,totalAmount,company,DATE_FORMAT(masterDate, '%d-%b-%y') AS masterDate,paid,received,DATE_FORMAT(receivedDate, '%d-%b-%y') AS receivedDate,"
				+ "(SELECT companyName FROM `company_tab` WHERE companyId = company) AS companyName,IF(paid=0, 'Unclear', 'Clear') AS paidVal,"
				+ "IF(paid=0, 'false', 'true') AS booleanVal FROM `credit_master` WHERE totalAmount > 0 AND store_id = "+userSession.getStoreId()+" ORDER BY masterDate DESC";
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY1);
			if(rows != null && rows.size() > 0){
				for(Map row : rows){
					CreditMasterBean bean = new CreditMasterBean();
					bean.setCompanyId(row.get("company")!=null?Long.parseLong(row.get("company").toString()):0);
					bean.setDate(row.get("masterDate")!=null?row.get("masterDate").toString():"");
					bean.setName(row.get("name")!=null?row.get("name").toString():"");
					bean.setPaidVal(row.get("paidVal")!=null?row.get("paidVal").toString():"");
					bean.setTotalAmount(row.get("totalAmount")!=null?Double.parseDouble(row.get("totalAmount").toString()):0);
					bean.setCreditId(row.get("cid")!=null?Long.parseLong(row.get("cid").toString()):0);
					bean.setCompanyName(row.get("companyName")!=null?row.get("companyName").toString():"");
					bean.setPaid(row.get("booleanVal")!=null?Boolean.parseBoolean(row.get("booleanVal").toString()):false);
					bean.setReceived(row.get("received")!=null?Double.parseDouble(row.get("received").toString()):0);
					bean.setReceivedDate(row.get("receivedDate")!=null?row.get("receivedDate").toString():"");
					bean.setCreditChildBeanList(getChildCreditDetails(row.get("cid")!=null?Long.parseLong(row.get("cid").toString()):0));
					listdata.add(bean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return listdata;
		}
		return listdata;
	}
	private List<CreditChildBean> getChildCreditDetails(long cid) {
		
		int storeId=userSession.getStoreId();
    String QUERY = "SELECT c.creditId,c.brandNoPackQty,c.bottles,c.price,c.totalPrice,c.totalDiscount,c.discount,c.afterIncludingDiscount,c.cases,"
    		+ "(SELECT p.shortBrandName FROM product p WHERE p.brandNoPackQty=c.brandNoPackQty ) AS shortBrandName FROM credit_tab c WHERE c.cid = ? AND c.store_id="+storeId+"";
    List<CreditChildBean> listData = new ArrayList<CreditChildBean>();
    try{
    	List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY,new Object[]{cid});
    	if(rows != null && rows.size()>0){
    		for(Map row : rows){
    			CreditChildBean bean = new CreditChildBean();
    			bean.setAfterIncludingDiscount(row.get("afterIncludingDiscount")!=null?Double.parseDouble(row.get("afterIncludingDiscount").toString()):0);
    			bean.setBottles(row.get("bottles")!=null?Long.parseLong(row.get("bottles").toString()):0);
    			bean.setBrandNoPackQty(row.get("brandNoPackQty")!=null?Double.parseDouble(row.get("brandNoPackQty").toString()):0);
    			bean.setCreditId(row.get("creditId")!=null?Long.parseLong(row.get("creditId").toString()):0);
    			bean.setDiscount(row.get("discount")!=null?Double.parseDouble(row.get("discount").toString()):0);
    			bean.setPrice(row.get("price")!=null?Double.parseDouble(row.get("price").toString()):0);
    			bean.setTotalPrice(row.get("totalPrice")!=null?Double.parseDouble(row.get("totalPrice").toString()):0);
    			bean.setProductName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
    			bean.setCases(row.get("cases")!=null?Long.parseLong(row.get("cases").toString()):0);
    			bean.setTotalDiscount(row.get("totalDiscount")!=null?Double.parseDouble(row.get("totalDiscount").toString()):0);
    			listData.add(bean);
    		}
    	}
    	
    }catch(Exception e){
    	e.printStackTrace();
    	return listData;
    }
    return listData;
	}
	/**
	 * This method is used to update existing credit
	 * */
	@Override
	public String updateExistingCredit(CreditPaymentBean creditPaymentBean) {
    String QUERY = "UPDATE `credit_master` cm  SET cm.`received` = ?, cm.`receivedDate` = ?, cm.comment = ?, modeofpayment = ? WHERE cm.`cid`=? AND cm.store_id = ?";
     try{
    	 int value = jdbcTemplate.update(QUERY, new Object[]{creditPaymentBean.getPaymentAmount(),creditPaymentBean.getPaymentDate(),creditPaymentBean.getPaymentcomment(),creditPaymentBean.getPaymentmode(),creditPaymentBean.getPaymentCreditId(),userSession.getStoreId()});
    	 if(value > 0)
    		 return "Record updated successfully!";
    	 else
    		 return "Something went wrong!";
     }catch(Exception e){
    	 e.printStackTrace();
    	 return "Something went wrong!";
     }
	}
	@Override
	public List<CreditMasterBean> getcompanyWiseExistingCredit(int company, String date) {
		
		int storeId=userSession.getStoreId();
		
       String QUERY = "SELECT cid,name,totalAmount,company,DATE_FORMAT(masterDate, '%d-%b-%y') AS masterDate ,received,DATE_FORMAT(receivedDate, '%d-%b-%y') AS receivedDate "
       		+ " FROM `credit_master` WHERE store_id="+storeId+" AND totalAmount > 0 AND masterDate >= ? AND masterDate <= LAST_DAY(?) AND company = ? "
       		+ "GROUP BY masterDate ORDER BY masterDate DESC";
       List<CreditMasterBean> creditList = new ArrayList<CreditMasterBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY, new Object[]{date,date,company});
			if(rows != null && rows.size() > 0){
			  for(Map row : rows){
				  CreditMasterBean bean = new CreditMasterBean();
				  bean.setCreditId(row.get("cid")!= null ? Long.parseLong(row.get("cid").toString()):0);
				  bean.setName(row.get("name")!=null?row.get("name").toString():"");
				  bean.setTotalAmount(row.get("totalAmount")!=null?Double.parseDouble(row.get("totalAmount").toString()):0);
				  bean.setDate(row.get("masterDate")!= null?row.get("masterDate").toString():"");
				  bean.setReceived(row.get("received")!=null?Double.parseDouble(row.get("received").toString()):0);
				  bean.setReceivedDate(row.get("receivedDate")!= null?row.get("receivedDate").toString():"");
				  bean.setCreditChildBeanList(getChildCreditDetails(row.get("cid")!=null?Long.parseLong(row.get("cid").toString()):0));
				  creditList.add(bean);
			  }	
			}
		}catch(Exception e){
			e.printStackTrace();
			log.error("Error:"+e);
			return creditList;
		}
		return creditList;
	}
	@Override
	public List<NoDiscountProductsBean> remindLiftedCaseToUpdateDiscount(String date) {
		log.info("PoizonController.getMidMailWithLiftedCaseToUpdateDiscount()");
		String QUERY = "SELECT p.realBrandNo,p.shortBrandName, COALESCE(ROUND(CEIL(SUM(i.receivedBottles/p.packQty))),0) AS liftedCase,"
				+ "(SELECT c.companyName FROM company_tab c WHERE c.companyId = p.company) AS companyName,"
				+ "(SELECT COALESCE(SUM(d.discountAmount),0) FROM `stock_lift_with_discount` d WHERE d.brandNo = p.realBrandNo AND "
				+ "d.stockDiscountDate = '"+date+"') AS discountAmount FROM product p LEFT JOIN invoice i  "
				+ "ON p.`brandNoPackQty` = i.`brandNoPackQty` AND i.`invoiceDateAsPerSheet`>='"+date+"' "
				+ " AND `invoiceDateAsPerSheet` <=(SELECT LAST_DAY('"+date+"')) GROUP BY p.realBrandNo   HAVING liftedCase > 0 AND discountAmount = 0 ORDER BY p.`company` ASC";
		List<PendingLiftedCaseDiscountBean> listData = new ArrayList<PendingLiftedCaseDiscountBean>();
		List<NoDiscountProductsBean> mainList =  new ArrayList<NoDiscountProductsBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
			if(rows != null && rows.size() > 0){
				for(Map row : rows){
					PendingLiftedCaseDiscountBean bean = new PendingLiftedCaseDiscountBean();
					bean.setBrandName(row.get("shortBrandName")!=null?row.get("shortBrandName").toString():"");
					bean.setBrandNo(row.get("realBrandNo")!=null?Long.parseLong(row.get("realBrandNo").toString()):0);
					bean.setCases(row.get("liftedCase")!=null?Long.parseLong(row.get("liftedCase").toString()):0);
					bean.setCompanyName(row.get("companyName")!=null?row.get("companyName").toString():"");
					listData.add(bean);
				}
			}
		Set<String> uniqueSet = new HashSet<String>();
		for(PendingLiftedCaseDiscountBean bean : listData)
			uniqueSet.add(bean.getCompanyName());
		
		for (Iterator<String> it = uniqueSet.iterator(); it.hasNext(); ) {
	            String company = it.next();
	            NoDiscountProductsBean bean = new NoDiscountProductsBean();
				 List<PendingLiftedCaseDiscountBean> innerlistData = new ArrayList<PendingLiftedCaseDiscountBean>();
				 for (int j = 0; j < listData.size(); j++) {
					 
				   if(company.equalsIgnoreCase(listData.get(j).getCompanyName())){
					   PendingLiftedCaseDiscountBean innerBean = new PendingLiftedCaseDiscountBean();
					   innerBean.setBrandName(listData.get(j).getBrandName());
					   innerBean.setBrandNo(listData.get(j).getBrandNo());
					   innerBean.setCases(listData.get(j).getCases());
					   innerBean.setCompanyName(listData.get(j).getCompanyName());
					   innerlistData.add(innerBean);
				   }
				 }
				 bean.setCompanyName(company);
				 bean.setListData(innerlistData);
				 mainList.add(bean);
	    }
		
		}catch(Exception e){
			e.printStackTrace();
			log.error("Error In remindLiftedCaseToUpdateDiscount():: "+e);
			return mainList;
		}
		return mainList;
	}
	/**
	 * This method is used to get distinct UNION saledate and invoicedate list from 'sale' And invoice table.
	 * */
	@Override
	public List<InvoiceDateBean> getUnionSaleAndInvoiceDate() {

		String Query="SELECT DISTINCT DATE_FORMAT(invoiceDateAsPerSheet,'%d-%m-%Y') AS customedate, invoiceDateAsPerSheet FROM `invoice` WHERE store_id="+userSession.getStoreId()+" UNION " + 
				"SELECT DISTINCT DATE_FORMAT(saleDate,'%d-%m-%Y') AS customedate, saleDate AS invoiceDateAsPerSheet FROM sale WHERE store_id="+userSession.getStoreId()+" "
				+ " ORDER BY invoiceDateAsPerSheet ASC";

		List<InvoiceDateBean> invoiceDateList = new ArrayList<InvoiceDateBean>();
		try{
			List<Map<String, Object>> rows = jdbcTemplate.queryForList(Query);
			if(rows != null && rows.size()>0) {
				for(Map row : rows){
					InvoiceDateBean invoiceDateBean = new InvoiceDateBean();
					invoiceDateBean.setDate(row.get("customedate").toString());
					invoiceDateList.add(invoiceDateBean);
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return invoiceDateList;
		}finally {
			
		}
		return invoiceDateList;
	}
}
