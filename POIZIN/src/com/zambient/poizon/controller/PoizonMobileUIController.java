package com.zambient.poizon.controller;

import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.zambient.poizon.bean.BalanceSheetWithBreckage;
import com.zambient.poizon.bean.BuildDiscountBean;
import com.zambient.poizon.bean.CardCashSaleBeen;
import com.zambient.poizon.bean.CreditChildBean;
import com.zambient.poizon.bean.CreditMasterBean;
import com.zambient.poizon.bean.DiscountAndMonthBean;
import com.zambient.poizon.bean.DiscountAsPerCompanyBeen;
import com.zambient.poizon.bean.DiscountMonthWiseBean;
import com.zambient.poizon.bean.ExpenseBean;
import com.zambient.poizon.bean.ExpenseCategoryAndDate;
import com.zambient.poizon.bean.InvoiceDateBean;
import com.zambient.poizon.bean.MessageBean;
import com.zambient.poizon.bean.MobileViewInHouseStock;
import com.zambient.poizon.bean.MobileViewStockLiftingDiscount;
import com.zambient.poizon.bean.PieChartBean;
import com.zambient.poizon.bean.SaleAndAmmountBean;
import com.zambient.poizon.bean.SaleBean;
import com.zambient.poizon.bean.SaleListBean;
import com.zambient.poizon.bean.SaleWithDiscountBean;
import com.zambient.poizon.bean.VendorAdjustmentGetArrearsBean;
import com.zambient.poizon.bean.category;
import com.zambient.poizon.bean.companyBean;
import com.zambient.poizon.services.PoizonMobileUIService;
import com.zambient.poizon.services.PoizonSecondService;
import com.zambient.poizon.services.PoizonService;
import com.zambient.poizon.services.PoizonThirdService;
import com.zambient.poizon.utill.CommonUtil;
/**
 * This controller in related to mobile services which we are accessing without login 
 * */
@Controller
public class PoizonMobileUIController {
	final static Logger log = Logger.getLogger(PoizonController.class);
	
	@Value("${poizinPdfFile}")
	private String poizinPdfFile;
	@Value("${images.root.path}")
	private String imageRootPath;
	@Value("${images.retrival.path}")
	private String imagesRetrivalPath;
	@Autowired
	PoizonMobileUIService poizonMobileUIService;
	@Autowired
	PoizonService poizonService;
	@Autowired
	PoizonSecondService poizonSecondService;
	@Autowired
	PoizonThirdService poizonThirdService;
	
	/**
	 * This method is used to get current Inhouse stock value
	 * */
	@RequestMapping(value = "/mobileView/inHouseStockValue", method = RequestMethod.GET)
    public @ResponseBody List<MobileViewInHouseStock> mobileViewInHouseStock(HttpServletRequest request, HttpServletResponse response) {
		log.info("mobileView/inHouseStock");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<MobileViewInHouseStock> mobileViewInHouseStock =poizonMobileUIService.mobileViewInHouseStock();
		return mobileViewInHouseStock;
    }
	/**
	 * This method is used to get current Inhouse stock Amount
	 * */
	@RequestMapping(value = "/mobileView/InHouseStockAmount", method = RequestMethod.GET)
    public @ResponseBody double InHouseStockAmount(HttpServletRequest request, HttpServletResponse response) {
		log.info("mobileView/InHouseStockAmount");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		double result =poizonMobileUIService.InHouseStockAmount();
		return result;
    }
	@RequestMapping(value = "/mobileView/InHouseStockAmountWithSale", method = RequestMethod.GET)
    public @ResponseBody List<SaleBean> InHouseStockAmountWithSale(HttpServletRequest request, HttpServletResponse response) {
		log.info("mobileView/InHouseStockAmountWithSale");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<SaleBean> result =poizonMobileUIService.InHouseStockAmountWithSale();
		return result;
    }
	/**
	 * This method is used to get Day wise balance sheet
	 * */
	@RequestMapping(value = "/mobileView/dayWiseBalanceSheet", method = RequestMethod.GET)
    public @ResponseBody BalanceSheetWithBreckage dayWiseBalanceSheet(HttpServletRequest request, HttpServletResponse response,@RequestParam String sdate, @RequestParam String edate) {
		log.info("PoizonMobileUIController.getBalanceSheet ");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		
 		BalanceSheetWithBreckage balanceSheetList = null;
 		balanceSheetList = poizonMobileUIService.dayWiseBalanceSheet(CommonUtil.chageDateFormat(sdate),CommonUtil.chageDateFormat(edate));
		
		return balanceSheetList;
    }
	/**
	 * This method is used to get disticnt sale dates from sale tab
	 * */
	@RequestMapping(value = "/mobileView/getTotalSaleDates", method = RequestMethod.GET)
    public @ResponseBody List<InvoiceDateBean> getTotalSaleDates(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonMobileUIController.getBalanceSheet ");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		
 		List<InvoiceDateBean> invoiceDateBean = null;
 		invoiceDateBean = poizonService.getTotalSaleDate();
		
		return invoiceDateBean;
    }
	/**
	 * This method is used to get expense details
	 * */
	@RequestMapping(value = "/mobileView/getDayWiseExpenseDetails", method = RequestMethod.GET)
    public @ResponseBody List<ExpenseBean> getDayWiseExpenseDetails(HttpServletRequest request, HttpServletResponse response,@RequestParam("expenseMasterID") String expenseMasterID) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<ExpenseBean> expenseList = null;
		expenseList = poizonService.getExpenseDetails(expenseMasterID);
		return expenseList;
    }
	/**
	 * This method is used to get no. of sold cases with product wise in between the selecetd dates.
	 * And also getting available current stock and prediction no. of days for currect stock.
	 * */
	@RequestMapping(value = "/mobileView/getSaleReports", method = RequestMethod.GET)
    public @ResponseBody SaleAndAmmountBean getSaleReports(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		log.info("PoizonMobileUIController.getSaleReports");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		String sdate = CommonUtil.chageDateFormat(request.getParameter("startDate"));
		String edate = CommonUtil.chageDateFormat(request.getParameter("endDate"));
		SaleAndAmmountBean saleAndAmmountBean = null;
		saleAndAmmountBean = poizonMobileUIService.getSaleReports(sdate,edate);
		return saleAndAmmountBean;
    }
	/**
	 * This method is used to get no. of stock lifted, getting total discount and other details for each month.
	 * */
	@RequestMapping(value = "/mobileView/stockLiftingWithDiscountReport", method = RequestMethod.GET)
    public @ResponseBody List<MobileViewStockLiftingDiscount> stockLiftingWithDiscountReport(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<MobileViewStockLiftingDiscount> expenseList = null;
		expenseList = poizonMobileUIService.stockLiftingWithDiscountReport();
		return expenseList;
    }
	/**
	 * This method is used to get complete discount details for select company and till date
	 * */
	@RequestMapping(value = "/mobileView/stockLiftingWithDiscountReportWithBrandDetails", method = RequestMethod.GET)
    public @ResponseBody List<SaleBean> stockLiftingWithDiscountReportWithBrandDetails(HttpServletRequest request, 
    		HttpServletResponse response,@RequestParam("date") String date,@RequestParam("companyId") int companyId) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<SaleBean> mobileViewStockDiscountSuperInnerList = null;
		mobileViewStockDiscountSuperInnerList = poizonMobileUIService.getTillMonthDiscountDetails(date,companyId);
		return mobileViewStockDiscountSuperInnerList;
    }
	/**
	 * This method is used to get Lifted no. of cases, target and prediction days
	 * */
	@RequestMapping(value = "/mobileView/getStockLiftingReports", method = RequestMethod.GET)
    public @ResponseBody List<SaleBean> getStockLiftingReports(HttpServletRequest request, HttpServletResponse response,@RequestParam("date") String date) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		DateFormat df = new SimpleDateFormat("dd/MMMM/yyyy"); 
 		Date startDate;
 		 startDate = df.parse(date);
 		df = new SimpleDateFormat("yyyy-MM-dd");  
 	   String strDate = df.format(startDate);
		List<SaleBean> saleBean = null;
		saleBean = poizonMobileUIService.getStockLiftingReports(strDate);
		return saleBean;
    }
	
	/*This method is used to get sale details for closing sale by mobile*/
	@RequestMapping(value = "/mobileView/getSaleDetailsForClosing", method = RequestMethod.GET)
    public @ResponseBody SaleListBean getSaleDetailsForClosing(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		System.out.println("PoizonMobileUIController.getSaleForClosingByNewLogic()");
 		SaleListBean saleDeatils = null;
		saleDeatils = poizonMobileUIService.getSaleForClosingByNewLogic();
		return saleDeatils;
    }
	/*This method is used to Post sale details for closing by mobile*/
	@RequestMapping(value = "mobileView/saveSaleDetailsWithClosing", method = RequestMethod.POST)
    public @ResponseBody MessageBean saveSaleDetailsWithClosing(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonMobileUIController.saveSaleDetailsWithClosing>> ");
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		boolean flag=true;
		String result="success";
		MessageBean messageBean = new MessageBean();
		try {
			JSONObject jsonObject = new JSONObject(values);
			
			JSONArray saleDetailsJson = jsonObject.getJSONArray("saleBean");
			//System.out.println(saleDetailsJson);
			for (int i = 0; i < saleDetailsJson.length(); ++i) {
				 JSONObject rec;
				    rec = saleDetailsJson.getJSONObject(i);
				    System.out.println(rec.getDouble("brandNoPackQty"));
				    int totalopening = rec.getInt("opening")+rec.getInt("received")+rec.getInt("noOfReturnsBtl");
				    int totalclosing = rec.getInt("closing")+rec.getInt("totalSale");
				    if(totalopening != totalclosing){
				    	flag=false;
				    	messageBean.setMessage("Something went wrong please try again!");
				    	return messageBean;
				    	//return "Something went wrong please try again!";
				    }
				}
			if(flag)
			result=poizonMobileUIService.saveSaleDetails(saleDetailsJson);
			messageBean.setMessage(result);
			return messageBean;
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
			return messageBean;
		}
    }
	/*This Method is used to get Category list*/
	@RequestMapping(value = "mobileView/getCategoryList", method = RequestMethod.GET)
	public @ResponseBody List<companyBean> getCategoryList(HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonMobileUIController.getCompanyList");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<companyBean> category = null;
 		category = poizonSecondService.getCategoryList();
 		return category;
	}
	/*Old method not using any where*/
	@RequestMapping(value = "mobileView/getCashCardDateForClosing", method = RequestMethod.GET)
	public @ResponseBody MessageBean getCashCardDateForClosing(HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonMobileUIController.getCompanyList");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		MessageBean messageBean = new MessageBean();
		ModelAndView mav = new ModelAndView();
			String date = poizonService.getCashCardSaleDate();
			messageBean.setMessage(date);
			mav.addObject("json",messageBean);
			return messageBean;
	}
	/**
	 * This method is used to get sold cases and discount amount for those cases based on dates selection
	 * */
	@RequestMapping(value = "mobileView/getSaleWithDiscountData", method = RequestMethod.GET)
    public @ResponseBody List<SaleWithDiscountBean> getSaleWithDiscountData(HttpServletRequest request,
    		HttpServletResponse response,@RequestParam("startDate") String startDate,@RequestParam("endDate") String endDate) throws ParseException {
		System.out.println("PoizonMobileUIController.getSaleWithDiscountData");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<SaleWithDiscountBean> saleBeanList = null;
 		saleBeanList = poizonSecondService.getSaleWithDiscount(startDate,endDate);
		return saleBeanList;
    }
	/**
	 * This method is used for to download saved check
	 * */
	@RequestMapping(value = "mobileView/downloadChecks", method = RequestMethod.GET,produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody void downloadChecks(HttpServletRequest request, HttpServletResponse response,@RequestParam("imagePath") String imagePath) throws Exception {
		//System.out.println("khagesh>> "+imagePath);
		CommonUtil.downloadZipFile(request,response,imageRootPath+imagePath);
    }
	/**
	 * This method is used to get company list
	 * */
	@RequestMapping(value = "mobileView/getCompanyList", method = RequestMethod.GET)
	public @ResponseBody List<companyBean> getCompanyList(HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonMobileUIController.getCompanyList");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<companyBean> company = null;
 		company = poizonSecondService.getCompanyList();
 		return company;
	}
/**
 * This method is used to get discount for all months based on selected company
 * */
	@RequestMapping(value = "mobileView/getDiscountCompanyWiseForAllMonth", method = RequestMethod.GET)
    public @ResponseBody List<DiscountAndMonthBean> getDiscountCompanyWiseForAllMonth(HttpServletRequest request,
    		HttpServletResponse response,@RequestParam("date") String date,@RequestParam("company") int company) throws ParseException {
		System.out.println("PoizonSecondController.getDiscountCompanyWiseForAllMonth");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<DiscountAndMonthBean> discountTransactionBeanList = null;
 		discountTransactionBeanList = CommonUtil.getListBasedOnMatch(poizonSecondService.getDiscountCompanyWiseForAllMonth(date,company));
 		
		return discountTransactionBeanList;
    }
	/**
	 * This method is used to get discount for all months with received checks based on selected company
	 * */
	@RequestMapping(value = "mobileView/getDiscountCompanyWiseForAllMonthWithReceived", method = RequestMethod.GET)
    public @ResponseBody List<DiscountAsPerCompanyBeen> getDiscountCompanyWiseForAllMonthWithReceived(HttpServletRequest request,
    		HttpServletResponse response,@RequestParam("company") int company) throws ParseException {
		System.out.println("PoizonSecondController.getDiscountCompanyWiseForAllMonthWithReceived");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<DiscountAsPerCompanyBeen> discountAsPerCompanyBeenList = null;
 		discountAsPerCompanyBeenList = poizonSecondService.getDiscountCompanyWiseForAllMonthWithReceived(company);
		return discountAsPerCompanyBeenList;
    }
	/**
	 * This method is used to get rental amount for all months based on selected company and till date
	 * */
	@RequestMapping(value = "mobileView/getUpToMonthRental", method = RequestMethod.GET)
    public @ResponseBody double getUpToMonthRental(HttpServletRequest request,
    		HttpServletResponse response,@RequestParam("date") String date,@RequestParam("company") int company) throws ParseException {
		System.out.println("PoizonSecondController.getUpToMonthRental");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		double rental = poizonSecondService.getUpToMonthRental(date,company);
		return rental;
    }
	@RequestMapping(value = "mobileView/getCompanyWithEnterPrice", method = RequestMethod.GET)
    public @ResponseBody category getCompanyWithEnterPrice(HttpServletRequest request,
    		HttpServletResponse response,@RequestParam("company") int company) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		category categoryBean = null;
 		categoryBean = poizonSecondService.getCompanyWithEnterPrice(company);
		return categoryBean;
    }
	/**
	 * This method is used to download company discount by mobile view
	 * */
	@RequestMapping(value = "mobileView/downloadDiscountInMobile", method = RequestMethod.GET,produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody void downloadDiscountInMobile(HttpServletRequest request, HttpServletResponse response,@RequestParam("company") int company,
    		@RequestParam("date") String date,@RequestParam("companyName") String companyName) throws Exception {
		boolean value = downLoadCompanyDiscount(request,response,company,date,companyName);
		if(value)
		CommonUtil.downloadZipFile(request,response,poizinPdfFile+"/"+companyName+".pdf");
    }
	 public boolean downLoadCompanyDiscount(HttpServletRequest request, HttpServletResponse response, int company, String date, String companyName) throws ParseException {
			System.out.println("PoizonThirdController.getDiscountCompanyWiseForAllMonthWithReceived");
			response.setContentType("application/json");
	 		response.setHeader("Access-Control-Allow-Origin","*");
	 		response.setHeader("value", "valid");
	 		List<DiscountAsPerCompanyBeen> discountAsPerCompanyBeenList = null;
	 		discountAsPerCompanyBeenList = poizonThirdService.downLoadCompanyDiscount(company,date);
	 		List<DiscountAndMonthBean> discountTransactionBeanList = null;
	 		discountTransactionBeanList = CommonUtil.getListBasedOnMatch(poizonThirdService.downloadDiscountCompanyWiseForAllMonth(date,company));
	 		List<VendorAdjustmentGetArrearsBean> vendorBeanlist = new ArrayList<VendorAdjustmentGetArrearsBean>();
	 		vendorBeanlist = poizonMobileUIService.getVendorDiscount(date,company);
	 		List<CreditChildBean> creditChildBeanList = poizonMobileUIService.getVendorCreditListToTillMonth(date,company);
			CreditMasterBean creditMasterBean = poizonMobileUIService.getTotalCreditAmountAndReceivedTillMonth(date,company);
	 		
	 		String pdfResult = poizinPdfFile+"/"+companyName+".pdf";
	 		
	 		Document document = new Document(PageSize.A4_LANDSCAPE);
	 		try
			{
	 			
	 			double sumOfCheck = 0,totalAdjcheck = 0,totalDiscount = 0, totalRental=0,totalVendorDisc=0;
	 			for(DiscountAsPerCompanyBeen bean : discountAsPerCompanyBeenList){
					totalAdjcheck = totalAdjcheck+bean.getAdjustmentCheque();
					sumOfCheck = sumOfCheck+bean.getChequeAmount();
					totalDiscount = bean.getDiscountAmount();
					totalRental = bean.getRentals();
	 			}
	 			for(VendorAdjustmentGetArrearsBean bean :vendorBeanlist){
	 				totalVendorDisc += bean.getVendorAmt();
	 			}
				PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfResult));
				document.open();
				
				Paragraph header = new Paragraph();
				Font f = new Font(FontFamily.UNDEFINED, 20.0f, Font.NORMAL, new BaseColor(36,58,81));
				header.setAlignment(Element.ALIGN_RIGHT);
				Chunk chunk = new Chunk("POIZIN",f);
				header.add(chunk);
				document.add(header);
				
				// Table for total received,discount annd difference
				PdfPTable tableParent = new PdfPTable(2); // 3 columns.
				tableParent.setWidthPercentage(100); //Width 100%
				tableParent.setSpacingBefore(10f); //Space before table
				tableParent.setSpacingAfter(10f); //Space after table
				
				document.add(new Paragraph(companyName, FontFactory.getFont(FontFactory.HELVETICA, 12,Font.BOLD)));
				
				float[] columnWidthsParent = {1f, 1f};
				tableParent.setWidths(columnWidthsParent);
				
				PdfPCell cellParent1 = new PdfPCell(new Paragraph(new Phrase("Rentals + Total Discount", FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cellParent1.setBorderColor(BaseColor.BLACK);
				cellParent1.setPaddingTop(5);
				cellParent1.setPaddingBottom(5);
				cellParent1.setHorizontalAlignment(Element.ALIGN_CENTER);
				cellParent1.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cellParent2 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalDiscount+totalRental), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cellParent2.setBorderColor(BaseColor.BLACK);
				cellParent2.setPaddingTop(5);
				cellParent2.setPaddingBottom(5);
				cellParent2.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cellParent2.setVerticalAlignment(Element.ALIGN_RIGHT);
				
				PdfPCell cellParent9 = new PdfPCell(new Paragraph(new Phrase("Total Credit Amount", FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cellParent9.setBorderColor(BaseColor.BLACK);
				cellParent9.setPaddingTop(5);
				cellParent9.setPaddingBottom(5);
				cellParent9.setHorizontalAlignment(Element.ALIGN_CENTER);
				cellParent9.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cellParent10 = new PdfPCell(new Paragraph(new Phrase(Double.toString(creditMasterBean.getTotalAmount()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cellParent10.setBorderColor(BaseColor.BLACK);
				cellParent10.setPaddingTop(5);
				cellParent10.setPaddingBottom(5);
				cellParent10.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cellParent10.setVerticalAlignment(Element.ALIGN_RIGHT);
				
				PdfPCell cellParent3 = new PdfPCell(new Paragraph(new Phrase("Total Cheques Received", FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cellParent3.setBorderColor(BaseColor.BLACK);
				cellParent3.setPaddingTop(5);
				cellParent3.setPaddingBottom(5);
				cellParent3.setHorizontalAlignment(Element.ALIGN_CENTER);
				cellParent3.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cellParent4 = new PdfPCell(new Paragraph(new Phrase(Double.toString(sumOfCheck), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cellParent4.setBorderColor(BaseColor.BLACK);
				cellParent4.setPaddingTop(5);
				cellParent4.setPaddingBottom(5);
				cellParent4.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cellParent4.setVerticalAlignment(Element.ALIGN_RIGHT);
				
				PdfPCell cellParent5 = new PdfPCell(new Paragraph(new Phrase("Total Vendor Discount", FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cellParent5.setBorderColor(BaseColor.BLACK);
				cellParent5.setPaddingTop(5);
				cellParent5.setPaddingBottom(5);
				cellParent5.setHorizontalAlignment(Element.ALIGN_CENTER);
				cellParent5.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cellParent6 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalVendorDisc), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cellParent6.setBorderColor(BaseColor.BLACK);
				cellParent6.setPaddingTop(5);
				cellParent6.setPaddingBottom(5);
				cellParent6.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cellParent6.setVerticalAlignment(Element.ALIGN_RIGHT);
				
				PdfPCell cellParent7 = null,cellParent8 = null;
				if(((sumOfCheck+totalAdjcheck+totalVendorDisc+creditMasterBean.getReceived()) - (totalDiscount+totalRental+creditMasterBean.getTotalAmount())) <=0){
					Font red = new Font(FontFamily.HELVETICA, 9, Font.BOLD, BaseColor.RED);
					cellParent7 = new PdfPCell(new Paragraph(new Phrase("Amount due", red)));
					cellParent8 = new PdfPCell(new Paragraph(new Phrase(Double.toString((totalDiscount+totalRental+creditMasterBean.getTotalAmount()) - (sumOfCheck+totalAdjcheck+totalVendorDisc+creditMasterBean.getReceived())), red)));
				
				}else{
					Font green = new Font(FontFamily.HELVETICA, 9, Font.BOLD, BaseColor.GREEN);
					cellParent7 = new PdfPCell(new Paragraph(new Phrase("Amount exceeds", green)));
					cellParent8 = new PdfPCell(new Paragraph(new Phrase(Double.toString((sumOfCheck+totalAdjcheck+totalVendorDisc+creditMasterBean.getReceived()) - (totalDiscount+totalRental+creditMasterBean.getTotalAmount())), green)));
				}
				cellParent7.setBorderColor(BaseColor.BLACK);
				cellParent7.setPaddingTop(5);
				cellParent7.setPaddingBottom(5);
				cellParent7.setHorizontalAlignment(Element.ALIGN_CENTER);
				cellParent7.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				cellParent8.setBorderColor(BaseColor.BLACK);
				cellParent8.setPaddingTop(5);
				cellParent8.setPaddingBottom(5);
				cellParent8.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cellParent8.setVerticalAlignment(Element.ALIGN_RIGHT);
				tableParent.addCell(cellParent1);
				tableParent.addCell(cellParent2);
				tableParent.addCell(cellParent9);
				tableParent.addCell(cellParent10);
				tableParent.addCell(cellParent3);
				tableParent.addCell(cellParent4);
				if(totalVendorDisc !=0){
				tableParent.addCell(cellParent5);
				tableParent.addCell(cellParent6);
				}
				tableParent.addCell(cellParent7);
				tableParent.addCell(cellParent8);
				document.add(tableParent);

				PdfPTable table = new PdfPTable(5); // 3 columns.
				table.setWidthPercentage(100); //Width 100%
				table.setSpacingBefore(10f); //Space before table
				table.setSpacingAfter(10f); //Space after table
				
				// Creating Vendor Table
				PdfPTable vendortable = new PdfPTable(2); // 3 columns.
				vendortable.setWidthPercentage(100); //Width 100%
				vendortable.setSpacingBefore(10f); //Space before table
				vendortable.setSpacingAfter(10f); //Space after table
				
				//Set Column widths
				float[] vendorcolumnWidths = {1f, 1f};
				vendortable.setWidths(vendorcolumnWidths);
				
				PdfPCell vendorcell1 = new PdfPCell(new Paragraph(new Phrase("Month", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
				vendorcell1.setBorderColor(BaseColor.BLACK);
				vendorcell1.setPaddingTop(5);
				vendorcell1.setPaddingBottom(5);
				vendorcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				vendorcell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
				vendorcell1.setBackgroundColor(BaseColor.ORANGE);
				
				PdfPCell vendorcell2 = new PdfPCell(new Paragraph(new Phrase("Vendor Discount Amount", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
				vendorcell2.setBorderColor(BaseColor.BLACK);
				vendorcell2.setPaddingTop(5);
				vendorcell2.setPaddingBottom(5);
				vendorcell2.setHorizontalAlignment(Element.ALIGN_CENTER);
				vendorcell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
				vendorcell2.setBackgroundColor(BaseColor.ORANGE);
				
				vendortable.addCell(vendorcell1);
				vendortable.addCell(vendorcell2);
				
				for(VendorAdjustmentGetArrearsBean bean :vendorBeanlist){
					
					PdfPCell vendorcell11 = new PdfPCell(new Paragraph(new Phrase(bean.getDate(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					vendorcell11.setBorderColor(BaseColor.BLACK);
					vendorcell11.setPaddingTop(5);
					vendorcell11.setPaddingBottom(5);
					vendorcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
					vendorcell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
					
					PdfPCell vendorcell21 = new PdfPCell(new Paragraph(new Phrase(Double.toString(bean.getVendorAmt()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					vendorcell21.setBorderColor(BaseColor.BLACK);
					vendorcell21.setPaddingTop(5);
					vendorcell21.setPaddingBottom(5);
					vendorcell21.setHorizontalAlignment(Element.ALIGN_RIGHT);
					vendorcell21.setVerticalAlignment(Element.ALIGN_RIGHT);
					

					vendortable.addCell(vendorcell11);
					vendortable.addCell(vendorcell21);
					
	 			}
				PdfPCell vendorcell31 = new PdfPCell(new Paragraph(new Phrase("Total Vendor Discount", FontFactory.getFont(FontFactory.HELVETICA,9))));
				vendorcell31.setBorderColor(BaseColor.BLACK);
				vendorcell31.setPaddingTop(5);
				vendorcell31.setPaddingBottom(5);
				vendorcell31.setHorizontalAlignment(Element.ALIGN_RIGHT);
				vendorcell31.setVerticalAlignment(Element.ALIGN_RIGHT);
				
				PdfPCell vendorcell32 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalVendorDisc), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				vendorcell32.setBorderColor(BaseColor.BLACK);
				vendorcell32.setPaddingTop(5);
				vendorcell32.setPaddingBottom(5);
				vendorcell32.setHorizontalAlignment(Element.ALIGN_RIGHT);
				vendorcell32.setVerticalAlignment(Element.ALIGN_RIGHT);
				
				vendortable.addCell(vendorcell31);
				vendortable.addCell(vendorcell32);
				if(totalVendorDisc !=0)
				document.add(vendortable);
				
				// Creating Vendor Credit Table
			    PdfPTable credittable = new PdfPTable(5); // 3 columns.
			    credittable.setWidthPercentage(100); //Width 100%
			    credittable.setSpacingBefore(10f); 
			    credittable.setSpacingAfter(10f);
						
			//Set Column widths
			float[] creditcolumnWidths = {1f, 1f, 1f, 1f, 1f};
			credittable.setWidths(creditcolumnWidths);
						
			PdfPCell creditcell1 = new PdfPCell(new Paragraph(new Phrase("Vendor Name", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			creditcell1.setBorderColor(BaseColor.BLACK);
			creditcell1.setPaddingTop(5);
			creditcell1.setPaddingBottom(5);
			creditcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			creditcell1.setBackgroundColor(BaseColor.ORANGE);
						
			PdfPCell creditcell12 = new PdfPCell(new Paragraph(new Phrase("Date", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			creditcell12.setBorderColor(BaseColor.BLACK);
			creditcell12.setPaddingTop(5);
			creditcell12.setPaddingBottom(5);
			creditcell12.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell12.setVerticalAlignment(Element.ALIGN_MIDDLE);
			creditcell12.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell creditcell16 = new PdfPCell(new Paragraph(new Phrase("Brand Name", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			creditcell16.setBorderColor(BaseColor.BLACK);
			creditcell16.setPaddingTop(5);
			creditcell16.setPaddingBottom(5);
			creditcell16.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell16.setVerticalAlignment(Element.ALIGN_MIDDLE);
			creditcell16.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell creditcell17 = new PdfPCell(new Paragraph(new Phrase("Case / Bottles", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			creditcell17.setBorderColor(BaseColor.BLACK);
			creditcell17.setPaddingTop(5);
			creditcell17.setPaddingBottom(5);
			creditcell17.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell17.setVerticalAlignment(Element.ALIGN_MIDDLE);
			creditcell17.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell creditcell13 = new PdfPCell(new Paragraph(new Phrase("Total Amount", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			creditcell13.setBorderColor(BaseColor.BLACK);
			creditcell13.setPaddingTop(5);
			creditcell13.setPaddingBottom(5);
			creditcell13.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell13.setVerticalAlignment(Element.ALIGN_MIDDLE);
			creditcell13.setBackgroundColor(BaseColor.ORANGE);
			
			
			credittable.addCell(creditcell1);
			credittable.addCell(creditcell12);
			credittable.addCell(creditcell16);
			credittable.addCell(creditcell17);
			credittable.addCell(creditcell13);
			
						
			for(CreditChildBean bean :creditChildBeanList){
							
			PdfPCell creditcell11 = new PdfPCell(new Paragraph(new Phrase(bean.getName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			creditcell11.setBorderColor(BaseColor.BLACK);
			creditcell11.setPaddingTop(5);
			creditcell11.setPaddingBottom(5);
			creditcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
							
			PdfPCell creditcell21 = new PdfPCell(new Paragraph(new Phrase(bean.getDate(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			creditcell21.setBorderColor(BaseColor.BLACK);
			creditcell21.setPaddingTop(5);
			creditcell21.setPaddingBottom(5);
			creditcell21.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell21.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell creditcell81 = new PdfPCell(new Paragraph(new Phrase(bean.getProductName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			creditcell81.setBorderColor(BaseColor.BLACK);
			creditcell81.setPaddingTop(5);
			creditcell81.setPaddingBottom(5);
			creditcell81.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell81.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell creditcell91 = new PdfPCell(new Paragraph(new Phrase(bean.getCases()+" / "+bean.getBottles(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			creditcell91.setBorderColor(BaseColor.BLACK);
			creditcell91.setPaddingTop(5);
			creditcell91.setPaddingBottom(5);
			creditcell91.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell91.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell creditcell31 = new PdfPCell(new Paragraph(new Phrase(Double.toString(bean.getAfterIncludingDiscount()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			creditcell31.setBorderColor(BaseColor.BLACK);
			creditcell31.setPaddingTop(5);
			creditcell31.setPaddingBottom(5);
			creditcell31.setHorizontalAlignment(Element.ALIGN_RIGHT);
			creditcell31.setVerticalAlignment(Element.ALIGN_RIGHT);
			
			credittable.addCell(creditcell11);
			credittable.addCell(creditcell21);
			credittable.addCell(creditcell81);
			credittable.addCell(creditcell91);
			credittable.addCell(creditcell31);
							
		}
		PdfPCell creditcell51 = new PdfPCell(new Paragraph(new Phrase("Total", FontFactory.getFont(FontFactory.HELVETICA, 9))));
		creditcell51.setBorderColor(BaseColor.BLACK);
		creditcell51.setPaddingTop(5);
		creditcell51.setPaddingBottom(5);
		creditcell51.setHorizontalAlignment(Element.ALIGN_RIGHT);
		creditcell51.setVerticalAlignment(Element.ALIGN_RIGHT);
		creditcell51.setColspan(4);
						
		PdfPCell creditcell61 = new PdfPCell(new Paragraph(new Phrase(Double.toString(creditMasterBean.getTotalAmount()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
		creditcell61.setBorderColor(BaseColor.BLACK);
		creditcell61.setPaddingTop(5);
		creditcell61.setPaddingBottom(5);
		creditcell61.setHorizontalAlignment(Element.ALIGN_RIGHT);
		creditcell61.setVerticalAlignment(Element.ALIGN_RIGHT);
		
		credittable.addCell(creditcell51);
		credittable.addCell(creditcell61);
		if(creditMasterBean.getTotalAmount() != 0)
		document.add(credittable);
				
				
				//Set Column widths
				float[] columnWidths = {1f, 1f, 1f, 1f, 1f};
				table.setWidths(columnWidths);
				
				PdfPCell cell1 = new PdfPCell(new Paragraph(new Phrase("Transaction Date", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
				cell1.setBorderColor(BaseColor.BLACK);
				cell1.setPaddingTop(5);
				cell1.setPaddingBottom(5);
				cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell1.setBackgroundColor(BaseColor.ORANGE);
				
				PdfPCell cell2 = new PdfPCell(new Paragraph(new Phrase("Bank", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
				cell2.setBorderColor(BaseColor.BLACK);
				cell2.setPaddingTop(5);
				cell2.setPaddingBottom(5);
				cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell2.setBackgroundColor(BaseColor.ORANGE);

				PdfPCell cell3 = new PdfPCell(new Paragraph(new Phrase("Transaction Type", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
				cell3.setBorderColor(BaseColor.BLACK);
				cell3.setPaddingTop(5);
				cell3.setPaddingBottom(5);
				cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell3.setBackgroundColor(BaseColor.ORANGE);
				
				PdfPCell cell5 = new PdfPCell(new Paragraph(new Phrase("Cheque No", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
				cell5.setBorderColor(BaseColor.BLACK);
				cell5.setPaddingTop(5);
				cell5.setPaddingBottom(5);
				cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell5.setBackgroundColor(BaseColor.ORANGE);
				
				PdfPCell cell4 = new PdfPCell(new Paragraph(new Phrase("Cheque Amount", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
				cell4.setBorderColor(BaseColor.BLACK);
				cell4.setPaddingTop(5);
				cell4.setPaddingBottom(5);
				cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
				cell4.setBackgroundColor(BaseColor.ORANGE);
	      
				table.addCell(cell1);
				table.addCell(cell2);
				table.addCell(cell3);
				table.addCell(cell5);
				table.addCell(cell4);
				for(DiscountAsPerCompanyBeen bean : discountAsPerCompanyBeenList){
					
					PdfPCell cell11 = new PdfPCell(new Paragraph(new Phrase(bean.getEnterprise(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					cell11.setBorderColor(BaseColor.BLACK);
					cell11.setPaddingTop(5);
					cell11.setPaddingBottom(5);
					cell11.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
					
					PdfPCell cell21 = new PdfPCell(new Paragraph(new Phrase(bean.getCompanyName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					cell21.setBorderColor(BaseColor.BLACK);
					cell21.setPaddingTop(5);
					cell21.setPaddingBottom(5);
					cell21.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell21.setVerticalAlignment(Element.ALIGN_MIDDLE);

					PdfPCell cell31 = new PdfPCell(new Paragraph(new Phrase(bean.getComment(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					cell31.setBorderColor(BaseColor.BLACK);
					cell31.setPaddingTop(5);
					cell31.setPaddingBottom(5);
					cell31.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell31.setVerticalAlignment(Element.ALIGN_MIDDLE);
					
					PdfPCell cell51 = new PdfPCell(new Paragraph(new Phrase(bean.getColor(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					cell51.setBorderColor(BaseColor.BLACK);
					cell51.setPaddingTop(5);
					cell51.setPaddingBottom(5);
					cell51.setHorizontalAlignment(Element.ALIGN_CENTER);
					cell51.setVerticalAlignment(Element.ALIGN_MIDDLE);
					
					PdfPCell cell41 = new PdfPCell(new Paragraph(new Phrase(Double.toString(bean.getChequeAmount()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					cell41.setBorderColor(BaseColor.BLACK);
					cell41.setPaddingTop(5);
					cell41.setPaddingBottom(5);
					cell41.setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell41.setVerticalAlignment(Element.ALIGN_RIGHT);
					if(bean.getChequeAmount() > 0){
					table.addCell(cell11);
					table.addCell(cell21);
					table.addCell(cell31);
					table.addCell(cell51);
					table.addCell(cell41);
					}
		 		}
				PdfPCell cellreceived1 = new PdfPCell(new Paragraph(new Phrase("Total Cheques Received", FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cellreceived1.setBorderColor(BaseColor.BLACK);
				cellreceived1.setPaddingTop(5);
				cellreceived1.setPaddingBottom(5);
				cellreceived1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cellreceived1.setVerticalAlignment(Element.ALIGN_RIGHT);
				cellreceived1.setColspan(4);
				PdfPCell cellreceived2 = new PdfPCell(new Paragraph(new Phrase(Double.toString(sumOfCheck), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cellreceived2.setBorderColor(BaseColor.BLACK);
				cellreceived2.setPaddingTop(5);
				cellreceived2.setPaddingBottom(5);
				cellreceived2.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cellreceived2.setVerticalAlignment(Element.ALIGN_RIGHT);
				
				table.addCell(cellreceived1);
				table.addCell(cellreceived2);
				
			/*	PdfPCell cellDiscountRental1 = new PdfPCell(new Paragraph(new Phrase("Rentals + Total Discount", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
				cellDiscountRental1.setBorderColor(BaseColor.BLACK);
				cellDiscountRental1.setPaddingTop(5);
				cellDiscountRental1.setPaddingBottom(5);
				cellDiscountRental1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cellDiscountRental1.setVerticalAlignment(Element.ALIGN_RIGHT);
				cellDiscountRental1.setColspan(4);
				PdfPCell cellDiscountRental2 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalDiscount+totalRental), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
				cellDiscountRental2.setBorderColor(BaseColor.BLACK);
				cellDiscountRental2.setPaddingTop(5);
				cellDiscountRental2.setPaddingBottom(5);
				cellDiscountRental2.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cellDiscountRental2.setVerticalAlignment(Element.ALIGN_RIGHT);
				
				table.addCell(cellDiscountRental1);
				table.addCell(cellDiscountRental2);
				
				PdfPCell celladj1 = new PdfPCell(new Paragraph(new Phrase("Cheque Adjustments", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
				celladj1.setBorderColor(BaseColor.BLACK);
				celladj1.setPaddingTop(5);
				celladj1.setPaddingBottom(5);
				celladj1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				celladj1.setVerticalAlignment(Element.ALIGN_RIGHT);
				celladj1.setColspan(4);
				PdfPCell celladj2 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalAdjcheck), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
				celladj2.setBorderColor(BaseColor.BLACK);
				celladj2.setPaddingTop(5);
				celladj2.setPaddingBottom(5);
				celladj2.setHorizontalAlignment(Element.ALIGN_RIGHT);
				celladj2.setVerticalAlignment(Element.ALIGN_RIGHT);
				
				table.addCell(celladj1);
				table.addCell(celladj2);
				
				PdfPCell cellDifference1 = new PdfPCell(new Paragraph(new Phrase("Difference", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
				cellDifference1.setBorderColor(BaseColor.BLACK);
				cellDifference1.setPaddingTop(5);
				cellDifference1.setPaddingBottom(5);
				cellDifference1.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cellDifference1.setVerticalAlignment(Element.ALIGN_RIGHT);
				cellDifference1.setColspan(4);
				PdfPCell cellDifference2 = new PdfPCell(new Paragraph(new Phrase((Double.toString((sumOfCheck+totalAdjcheck) - (totalDiscount+totalRental))), FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
				cellDifference2.setBorderColor(BaseColor.BLACK);
				cellDifference2.setPaddingTop(5);
				cellDifference2.setPaddingBottom(5);
				cellDifference2.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cellDifference2.setVerticalAlignment(Element.ALIGN_RIGHT);
				
				table.addCell(cellDifference1);
				table.addCell(cellDifference2);*/
				document.add(table);

				// Discount Table
				PdfPTable discounttable = new PdfPTable(4); // 3 columns.
				discounttable.setWidthPercentage(100); //Width 100%
				discounttable.setSpacingBefore(10f); //Space before table
				discounttable.setSpacingAfter(10f); //Space after table
	            
				//Set Column widths
				float[] columnWidthsDiscount = {1f, 1f, 1f, 1f};
				discounttable.setWidths(columnWidthsDiscount);
				
				/*PdfPCell discountcell1 = new PdfPCell(new Paragraph(new Phrase("Month", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
				discountcell1.setBorderColor(BaseColor.BLACK);
				discountcell1.setPaddingLeft(5);
				discountcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
				discountcell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
				*/
				PdfPCell discountcell2 = new PdfPCell(new Paragraph(new Phrase("Brand", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
				discountcell2.setBorderColor(BaseColor.BLACK);
				discountcell2.setPaddingTop(5);
				discountcell2.setPaddingBottom(5);
				discountcell2.setHorizontalAlignment(Element.ALIGN_CENTER);
				discountcell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
				discountcell2.setBackgroundColor(BaseColor.ORANGE);

				PdfPCell discountcell3 = new PdfPCell(new Paragraph(new Phrase("Case", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
				discountcell3.setBorderColor(BaseColor.BLACK);
				discountcell3.setPaddingTop(5);
				discountcell3.setPaddingBottom(5);
				discountcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
				discountcell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
				discountcell3.setBackgroundColor(BaseColor.ORANGE);
				
				PdfPCell discountcell4 = new PdfPCell(new Paragraph(new Phrase("Disc/Case", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
				discountcell4.setBorderColor(BaseColor.BLACK);
				discountcell4.setPaddingTop(5);
				discountcell4.setPaddingBottom(5);
				discountcell4.setHorizontalAlignment(Element.ALIGN_CENTER);
				discountcell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
				discountcell4.setBackgroundColor(BaseColor.ORANGE);
	      
				PdfPCell discountcell5 = new PdfPCell(new Paragraph(new Phrase("Total", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
				discountcell5.setBorderColor(BaseColor.BLACK);
				discountcell5.setPaddingTop(5);
				discountcell5.setPaddingBottom(5);
				discountcell5.setHorizontalAlignment(Element.ALIGN_CENTER);
				discountcell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
				discountcell5.setBackgroundColor(BaseColor.ORANGE);
	      
				//discounttable.addCell(discountcell1);
				discounttable.addCell(discountcell2);
				discounttable.addCell(discountcell3);
				discounttable.addCell(discountcell4);
				discounttable.addCell(discountcell5);
				
				for(DiscountAndMonthBean discountBean : discountTransactionBeanList){
					PdfPCell discountcell11 = new PdfPCell(new Paragraph(new Phrase(discountBean.getMonth(), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
					discountcell11.setBorderColor(BaseColor.BLACK);
					discountcell11.setPaddingTop(5);
					discountcell11.setPaddingBottom(5);
					discountcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
					discountcell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
					discountcell11.setBackgroundColor(BaseColor.LIGHT_GRAY);
					discountcell11.setColspan(5);
					discounttable.addCell(discountcell11);
					for(DiscountMonthWiseBean monthWiseDisc : discountBean.getDiscountMonthWiseBean()){
						
						PdfPCell discountcell21 = new PdfPCell(new Paragraph(new Phrase(monthWiseDisc.getBrand(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
						discountcell21.setBorderColor(BaseColor.BLACK);
						discountcell21.setPaddingLeft(5);
						discountcell21.setPaddingTop(5);
						discountcell21.setPaddingBottom(5);
						discountcell21.setHorizontalAlignment(Element.ALIGN_LEFT);
						discountcell21.setVerticalAlignment(Element.ALIGN_LEFT);

						PdfPCell discountcell31 = new PdfPCell(new Paragraph(new Phrase(Long.toString(monthWiseDisc.getCase()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
						discountcell31.setBorderColor(BaseColor.BLACK);
						discountcell31.setPaddingTop(5);
						discountcell31.setPaddingBottom(5);
						discountcell31.setHorizontalAlignment(Element.ALIGN_CENTER);
						discountcell31.setVerticalAlignment(Element.ALIGN_MIDDLE);
						
						PdfPCell discountcell41 = new PdfPCell(new Paragraph(new Phrase(Double.toString(monthWiseDisc.getDiscount()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
						discountcell41.setBorderColor(BaseColor.BLACK);
						discountcell41.setPaddingTop(5);
						discountcell41.setPaddingBottom(5);
						discountcell41.setHorizontalAlignment(Element.ALIGN_RIGHT);
						discountcell41.setVerticalAlignment(Element.ALIGN_RIGHT);
			      
						PdfPCell discountcell51 = new PdfPCell(new Paragraph(new Phrase(Double.toString(monthWiseDisc.getTotal()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
						discountcell51.setBorderColor(BaseColor.BLACK);
						discountcell51.setPaddingTop(5);
						discountcell51.setPaddingBottom(5);
						discountcell51.setHorizontalAlignment(Element.ALIGN_RIGHT);
						discountcell51.setVerticalAlignment(Element.ALIGN_RIGHT);
						
						discounttable.addCell(discountcell21);
						discounttable.addCell(discountcell31);
						discounttable.addCell(discountcell41);
						discounttable.addCell(discountcell51);
					}
					PdfPCell discountSumcell11 = new PdfPCell(new Paragraph(new Phrase("Total", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
					discountSumcell11.setBorderColor(BaseColor.BLACK);
					discountSumcell11.setPaddingTop(5);
					discountSumcell11.setPaddingBottom(5);
					discountSumcell11.setHorizontalAlignment(Element.ALIGN_RIGHT);
					discountSumcell11.setVerticalAlignment(Element.ALIGN_RIGHT);
					discountSumcell11.setColspan(3);
					discounttable.addCell(discountSumcell11);
					
					PdfPCell discountSumcell12 = new PdfPCell(new Paragraph(new Phrase(Double.toString(discountBean.getTotalDiscount()), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
					discountSumcell12.setBorderColor(BaseColor.BLACK);
					discountSumcell11.setPaddingTop(5);
					discountSumcell11.setPaddingBottom(5);
					discountSumcell12.setHorizontalAlignment(Element.ALIGN_RIGHT);
					discountSumcell12.setVerticalAlignment(Element.ALIGN_RIGHT);
					discounttable.addCell(discountSumcell12);
					
				}
				PdfPCell discountTotalcell11 = new PdfPCell(new Paragraph(new Phrase("Grand Total Discount", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
				discountTotalcell11.setBorderColor(BaseColor.BLACK);
				discountTotalcell11.setPaddingTop(5);
				discountTotalcell11.setPaddingBottom(5);
				discountTotalcell11.setHorizontalAlignment(Element.ALIGN_RIGHT);
				discountTotalcell11.setVerticalAlignment(Element.ALIGN_RIGHT);
				discountTotalcell11.setColspan(3);
				discounttable.addCell(discountTotalcell11);
				
				PdfPCell discountTotalcell12 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalDiscount), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
				discountTotalcell12.setBorderColor(BaseColor.BLACK);
				discountTotalcell12.setPaddingTop(5);
				discountTotalcell12.setPaddingBottom(5);
				discountTotalcell12.setHorizontalAlignment(Element.ALIGN_RIGHT);
				discountTotalcell12.setVerticalAlignment(Element.ALIGN_RIGHT);
				discounttable.addCell(discountTotalcell12);
				
				PdfPCell discountRentalcell11 = new PdfPCell(new Paragraph(new Phrase("Rentals", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
				discountRentalcell11.setBorderColor(BaseColor.BLACK);
				discountRentalcell11.setPaddingTop(5);
				discountRentalcell11.setPaddingBottom(5);
				discountRentalcell11.setHorizontalAlignment(Element.ALIGN_RIGHT);
				discountRentalcell11.setVerticalAlignment(Element.ALIGN_RIGHT);
				discountRentalcell11.setColspan(3);
				if(totalRental !=0)
				discounttable.addCell(discountRentalcell11);
				
				PdfPCell discountRentalcell12 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalRental), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
				discountRentalcell12.setBorderColor(BaseColor.BLACK);
				discountRentalcell12.setPaddingTop(5);
				discountRentalcell12.setPaddingBottom(5);
				discountRentalcell12.setHorizontalAlignment(Element.ALIGN_RIGHT);
				discountRentalcell12.setVerticalAlignment(Element.ALIGN_RIGHT);
				if(totalRental !=0)
				discounttable.addCell(discountRentalcell12);
				
				document.add(discounttable);
				document.close();
				writer.close();
			} catch (Exception e)
			{
				e.printStackTrace();
				return false;
			}
	 		return true;
	    }
	 /**
	  * This method is used to get discont detail for selected months
	  * */
	 @RequestMapping(value = "mobileView/getDiscountForSelectedMonth", method = RequestMethod.GET)
	    public @ResponseBody List<DiscountAndMonthBean> getDiscountCompanyWiseForAllMonth(HttpServletRequest request,
	    		HttpServletResponse response,@RequestParam("startMonth") String startMonth,@RequestParam("endMonth") String endMonth,
	    		@RequestParam("company") int company) throws ParseException {
			System.out.println("PoizonSecondController.getDiscountCompanyWiseForAllMonth");
			response.setContentType("application/json");
	 		response.setHeader("Access-Control-Allow-Origin","*");
	 		response.setHeader("value", "valid");
	 		List<DiscountAndMonthBean> discountTransactionBeanList = null;
	 		discountTransactionBeanList = CommonUtil.getListBasedOnMatch(poizonSecondService.getDiscountForSelectedMonth(startMonth,endMonth,company));
	 		
			return discountTransactionBeanList;
	    }
	 /**
	  * This method is used to get Rental detail for selected months
	  * */
	 @RequestMapping(value = "mobileView/getRentalForSelected", method = RequestMethod.GET)
	    public @ResponseBody double getUpToMonthRental(HttpServletRequest request,
	    		HttpServletResponse response,@RequestParam("startMonth") String startMonth,@RequestParam("endMonth") String endMonth,
	    		@RequestParam("company") int company) throws ParseException {
			System.out.println("PoizonSecondController.getUpToMonthRental");
			response.setContentType("application/json");
	 		response.setHeader("Access-Control-Allow-Origin","*");
	 		response.setHeader("value", "valid");
	 		double rental = poizonSecondService.getRentalForSelected(startMonth,endMonth,company);
			return rental;
	    }
	 /**
	  * This method is used to get discont and received check details for selected months
	  * */
	 @RequestMapping(value = "mobileView/getDiscountChecksForSelectedMonths", method = RequestMethod.GET)
	    public @ResponseBody List<DiscountAsPerCompanyBeen> getDiscountChecksForSelectedMonths(HttpServletRequest request,
	    		HttpServletResponse response,@RequestParam("startMonth") String startMonth,@RequestParam("endMonth") String endMonth,
	    		@RequestParam("company") int company) throws ParseException {
			System.out.println("PoizonSecondController.getDiscountChecksForSelectedMonths");
			response.setContentType("application/json");
	 		response.setHeader("Access-Control-Allow-Origin","*");
	 		response.setHeader("value", "valid");
	 		List<DiscountAsPerCompanyBeen> discountAsPerCompanyBeenList = null;
	 		discountAsPerCompanyBeenList = poizonSecondService.getDiscountChecksForSelectedMonths(startMonth,endMonth,company);
			return discountAsPerCompanyBeenList;
	    }
	 /**
	  * This method is used to get Arrears for previous months
	  * */
	 @RequestMapping(value = "mobileView/getArrearsForPreviousDays", method = RequestMethod.GET)
	    public @ResponseBody PieChartBean getArrearsForPreviousDays(HttpServletRequest request,
	    		HttpServletResponse response,@RequestParam("startMonth") String startMonth,
	    		@RequestParam("company") int company) throws ParseException {
			System.out.println("PoizonSecondController.getArrearsForPreviousDays");
			response.setContentType("application/json");
	 		response.setHeader("Access-Control-Allow-Origin","*");
	 		response.setHeader("value", "valid");
	 		PieChartBean previousArrears = null;
	 		previousArrears = poizonSecondService.getArrearsForPreviousDays(startMonth,company);
	 		
			return previousArrears;
	    }
	 /**
	  * This method is used to download discount, rental, checks and vendor discount in pdf form
	  * */
	 @RequestMapping(value = "mobileView/downloadDiscountForSelectedMonths", method = RequestMethod.GET,produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
	    public @ResponseBody void downloadDiscountForSelectedMonths(HttpServletRequest request, HttpServletResponse response,@RequestParam("company") int company,
	    		@RequestParam("startMonth") String startMonth,@RequestParam("endMonth") String endMonth,@RequestParam("companyName") String companyName,
	    		@RequestParam("customSMonth") String customStartMonth, @RequestParam("customEMonth") String customEndMonth) throws Exception {
			boolean value = generateDiscountForSelectedMonths(request,response,company,startMonth,endMonth,companyName,customStartMonth,customEndMonth);
			if(value)
			CommonUtil.downloadZipFile(request,response,poizinPdfFile+"/"+companyName+".pdf");
	    }

	private boolean generateDiscountForSelectedMonths(HttpServletRequest request, HttpServletResponse response,
			int company, String startMonth, String endMonth, String companyName, String customStartMonth, String customEndMonth) {
		
		PieChartBean previousArrears = poizonSecondService.getArrearsForPreviousDays(startMonth,company);
		List<DiscountAsPerCompanyBeen> discountAsPerCompanyBeenList = null;
 		discountAsPerCompanyBeenList = poizonThirdService.generateCompanyDiscount(company,startMonth,endMonth);
 		CardCashSaleBeen cardCashSaleBeen = poizonThirdService.getTotalDiscountAndRental(company,startMonth,endMonth);
 		List<VendorAdjustmentGetArrearsBean> vendorBeanlist = new ArrayList<VendorAdjustmentGetArrearsBean>();
 		vendorBeanlist = poizonMobileUIService.getVendorDiscount(endMonth,company);
 		List<DiscountAndMonthBean> discountTransactionBeanList = null;
 		discountTransactionBeanList = CommonUtil.getListBasedOnMatch(poizonSecondService.getDiscountForSelectedMonth(startMonth,endMonth,company));
 		List<CreditChildBean> creditChildBeanList = poizonMobileUIService.getVendorCreditListWithRangeWise(startMonth,endMonth,company);
		CreditMasterBean creditMasterBean = poizonMobileUIService.getTotalCreditAmountAndReceived(startMonth,endMonth,company);
		
 		String pdfResult = poizinPdfFile+"/"+companyName+".pdf";
 		
 		Document document = new Document(PageSize.A4_LANDSCAPE);
 		try
		{
 			
 			double sumOfCheck = 0,totalAdjcheck = 0,totalDiscount = cardCashSaleBeen.getCashSale(), totalRental=cardCashSaleBeen.getCardSale(),totalVendorDisc=0,totalVendorCredit=0,totalVendorCreditReceived=0;
 			for(DiscountAsPerCompanyBeen bean : discountAsPerCompanyBeenList){
				totalAdjcheck = totalAdjcheck+bean.getAdjustmentCheque();
				sumOfCheck = sumOfCheck+bean.getChequeAmount();
				//totalDiscount = bean.getDiscountAmount();
				//totalRental = bean.getRentals();
 			}
 			
 			for(VendorAdjustmentGetArrearsBean bean :vendorBeanlist){
 				totalVendorDisc += bean.getVendorAmt();
 			}
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfResult));
			document.open();
			// Table for total received,discount annd difference
			PdfPTable tableParent = new PdfPTable(2); // 3 columns.
			tableParent.setWidthPercentage(100); //Width 100%
			tableParent.setSpacingBefore(10f); //Space before table
			tableParent.setSpacingAfter(10f); //Space after table
			
			Paragraph header = new Paragraph();
			Font f = new Font(FontFamily.UNDEFINED, 20.0f, Font.NORMAL, new BaseColor(36,58,81));
			header.setAlignment(Element.ALIGN_RIGHT);
            Chunk chunk = new Chunk("POIZIN",f);
            header.add(chunk);
            document.add(header);
            
            document.add(new Paragraph(companyName+" From "+customStartMonth+" To "+customEndMonth, FontFactory.getFont(FontFactory.HELVETICA, 12,Font.BOLD)));
            
			float[] columnWidthsParent = {1f, 1f};
			tableParent.setWidths(columnWidthsParent);
			
			PdfPCell cellParent1 = new PdfPCell(new Paragraph(new Phrase("Total Discount Amount", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent1.setBorderColor(BaseColor.BLACK);
			cellParent1.setPaddingTop(5);
			cellParent1.setPaddingBottom(5);
			cellParent1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell cellParent2 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalDiscount+totalRental), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent2.setBorderColor(BaseColor.BLACK);
			cellParent2.setPaddingTop(5);
			cellParent2.setPaddingBottom(5);
			cellParent2.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellParent2.setVerticalAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell cellParent9 = new PdfPCell(new Paragraph(new Phrase("Total Credit Amount", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent9.setBorderColor(BaseColor.BLACK);
			cellParent9.setPaddingTop(5);
			cellParent9.setPaddingBottom(5);
			cellParent9.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent9.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell cellParent10 = new PdfPCell(new Paragraph(new Phrase(Double.toString(creditMasterBean.getTotalAmount()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent10.setBorderColor(BaseColor.BLACK);
			cellParent10.setPaddingTop(5);
			cellParent10.setPaddingBottom(5);
			cellParent10.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellParent10.setVerticalAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell cellParent3 = new PdfPCell(new Paragraph(new Phrase("Total Amount Received", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent3.setBorderColor(BaseColor.BLACK);
			cellParent3.setPaddingTop(5);
			cellParent3.setPaddingBottom(5);
			cellParent3.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent3.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell cellParent4 = new PdfPCell(new Paragraph(new Phrase(Double.toString(sumOfCheck), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent4.setBorderColor(BaseColor.BLACK);
			cellParent4.setPaddingTop(5);
			cellParent4.setPaddingBottom(5);
			cellParent4.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellParent4.setVerticalAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell cellParent5 = new PdfPCell(new Paragraph(new Phrase("Total Vendor Discount", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent5.setBorderColor(BaseColor.BLACK);
			cellParent5.setPaddingTop(5);
			cellParent5.setPaddingBottom(5);
			cellParent5.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent5.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell cellParent6 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalVendorDisc), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent6.setBorderColor(BaseColor.BLACK);
			cellParent6.setPaddingTop(5);
			cellParent6.setPaddingBottom(5);
			cellParent6.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellParent6.setVerticalAlignment(Element.ALIGN_RIGHT);
			PdfPCell cellParent7 = null;
			PdfPCell cellParent8 = null;
			if(((sumOfCheck+totalAdjcheck+totalVendorDisc + creditMasterBean.getReceived()) - (totalDiscount+totalRental+previousArrears.getValue() + creditMasterBean.getTotalAmount())) <= 0){
				  Font red = new Font(FontFamily.HELVETICA, 9, Font.BOLD, BaseColor.RED);
				 cellParent7 = new PdfPCell(new Paragraph(new Chunk("Amount Due", red)));
				 cellParent8 = new PdfPCell(new Paragraph(new Chunk(Double.toString((totalDiscount+totalRental+previousArrears.getValue() + creditMasterBean.getTotalAmount()) - (sumOfCheck+totalAdjcheck+totalVendorDisc + creditMasterBean.getReceived())), red)));
			}else{
				Font green = new Font(FontFamily.HELVETICA, 9, Font.BOLD, BaseColor.GREEN);
				cellParent7 = new PdfPCell(new Paragraph(new Phrase("Amount exceeds", green)));
				cellParent8 = new PdfPCell(new Paragraph(new Chunk(Double.toString((sumOfCheck+totalAdjcheck+totalVendorDisc + creditMasterBean.getReceived()) - (totalDiscount+totalRental+previousArrears.getValue() + creditMasterBean.getTotalAmount())), green)));
			}
			
			cellParent7.setBorderColor(BaseColor.BLACK);
			cellParent7.setPaddingTop(5);
			cellParent7.setPaddingBottom(5);
			cellParent7.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent7.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			cellParent8.setBorderColor(BaseColor.BLACK);
			cellParent8.setPaddingTop(5);
			cellParent8.setPaddingBottom(5);
			cellParent8.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellParent8.setVerticalAlignment(Element.ALIGN_RIGHT);
			tableParent.addCell(cellParent1);
			tableParent.addCell(cellParent2);
			tableParent.addCell(cellParent9);
			tableParent.addCell(cellParent10);
			tableParent.addCell(cellParent3);
			tableParent.addCell(cellParent4);
			if(totalVendorDisc != 0){
			tableParent.addCell(cellParent5);
			tableParent.addCell(cellParent6);
			}
			tableParent.addCell(cellParent7);
			tableParent.addCell(cellParent8);
			document.add(tableParent);
            
			// Creating Vendor Table
			PdfPTable vendortable = new PdfPTable(2); // 3 columns.
			vendortable.setWidthPercentage(100); //Width 100%
			vendortable.setSpacingBefore(10f); //Space before table
			vendortable.setSpacingAfter(10f); //Space after table
			
			//Set Column widths
			float[] vendorcolumnWidths = {1f, 1f};
			vendortable.setWidths(vendorcolumnWidths);
			
			PdfPCell vendorcell1 = new PdfPCell(new Paragraph(new Phrase("Month", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			vendorcell1.setBorderColor(BaseColor.BLACK);
			vendorcell1.setPaddingTop(5);
			vendorcell1.setPaddingBottom(5);
			vendorcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			vendorcell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			vendorcell1.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell vendorcell2 = new PdfPCell(new Paragraph(new Phrase("Vendor Discount Amount", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			vendorcell2.setBorderColor(BaseColor.BLACK);
			vendorcell2.setPaddingTop(5);
			vendorcell2.setPaddingBottom(5);
			vendorcell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			vendorcell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			vendorcell2.setBackgroundColor(BaseColor.ORANGE);
			
			vendortable.addCell(vendorcell1);
			vendortable.addCell(vendorcell2);
			
			for(VendorAdjustmentGetArrearsBean bean :vendorBeanlist){
				
				PdfPCell vendorcell11 = new PdfPCell(new Paragraph(new Phrase(bean.getDate(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				vendorcell11.setBorderColor(BaseColor.BLACK);
				vendorcell11.setPaddingTop(5);
				vendorcell11.setPaddingBottom(5);
				vendorcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
				vendorcell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell vendorcell21 = new PdfPCell(new Paragraph(new Phrase(Double.toString(bean.getVendorAmt()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				vendorcell21.setBorderColor(BaseColor.BLACK);
				vendorcell21.setPaddingTop(5);
				vendorcell21.setPaddingBottom(5);
				vendorcell21.setHorizontalAlignment(Element.ALIGN_RIGHT);
				vendorcell21.setVerticalAlignment(Element.ALIGN_RIGHT);
				

				vendortable.addCell(vendorcell11);
				vendortable.addCell(vendorcell21);
				
 			}
			PdfPCell vendorcell31 = new PdfPCell(new Paragraph(new Phrase("Total Vendor Discount", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			vendorcell31.setBorderColor(BaseColor.BLACK);
			vendorcell31.setPaddingTop(5);
			vendorcell31.setPaddingBottom(5);
			vendorcell31.setHorizontalAlignment(Element.ALIGN_RIGHT);
			vendorcell31.setVerticalAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell vendorcell32 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalVendorDisc), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			vendorcell32.setBorderColor(BaseColor.BLACK);
			vendorcell32.setPaddingTop(5);
			vendorcell32.setPaddingBottom(5);
			vendorcell32.setHorizontalAlignment(Element.ALIGN_RIGHT);
			vendorcell32.setVerticalAlignment(Element.ALIGN_RIGHT);
			
			vendortable.addCell(vendorcell31);
			vendortable.addCell(vendorcell32);
			if(totalVendorDisc != 0)
			document.add(vendortable);
			
			// Creating Vendor Credit Table
			    PdfPTable credittable = new PdfPTable(5); // 3 columns.
			    credittable.setWidthPercentage(100); //Width 100%
			    credittable.setSpacingBefore(10f); 
			    credittable.setSpacingAfter(10f);
						
			//Set Column widths
			float[] creditcolumnWidths = {1f, 1f, 1f, 1f, 1f};
			credittable.setWidths(creditcolumnWidths);
						
			PdfPCell creditcell1 = new PdfPCell(new Paragraph(new Phrase("Vendor Name", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			creditcell1.setBorderColor(BaseColor.BLACK);
			creditcell1.setPaddingTop(5);
			creditcell1.setPaddingBottom(5);
			creditcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			creditcell1.setBackgroundColor(BaseColor.ORANGE);
						
			PdfPCell creditcell12 = new PdfPCell(new Paragraph(new Phrase("Date", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			creditcell12.setBorderColor(BaseColor.BLACK);
			creditcell12.setPaddingTop(5);
			creditcell12.setPaddingBottom(5);
			creditcell12.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell12.setVerticalAlignment(Element.ALIGN_MIDDLE);
			creditcell12.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell creditcell16 = new PdfPCell(new Paragraph(new Phrase("Brand Name", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			creditcell16.setBorderColor(BaseColor.BLACK);
			creditcell16.setPaddingTop(5);
			creditcell16.setPaddingBottom(5);
			creditcell16.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell16.setVerticalAlignment(Element.ALIGN_MIDDLE);
			creditcell16.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell creditcell17 = new PdfPCell(new Paragraph(new Phrase("Case / Bottles", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			creditcell17.setBorderColor(BaseColor.BLACK);
			creditcell17.setPaddingTop(5);
			creditcell17.setPaddingBottom(5);
			creditcell17.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell17.setVerticalAlignment(Element.ALIGN_MIDDLE);
			creditcell17.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell creditcell13 = new PdfPCell(new Paragraph(new Phrase("Total Amount", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			creditcell13.setBorderColor(BaseColor.BLACK);
			creditcell13.setPaddingTop(5);
			creditcell13.setPaddingBottom(5);
			creditcell13.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell13.setVerticalAlignment(Element.ALIGN_MIDDLE);
			creditcell13.setBackgroundColor(BaseColor.ORANGE);
			
			
			credittable.addCell(creditcell1);
			credittable.addCell(creditcell12);
			credittable.addCell(creditcell16);
			credittable.addCell(creditcell17);
			credittable.addCell(creditcell13);
			
						
			for(CreditChildBean bean :creditChildBeanList){
							
			PdfPCell creditcell11 = new PdfPCell(new Paragraph(new Phrase(bean.getName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			creditcell11.setBorderColor(BaseColor.BLACK);
			creditcell11.setPaddingTop(5);
			creditcell11.setPaddingBottom(5);
			creditcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
							
			PdfPCell creditcell21 = new PdfPCell(new Paragraph(new Phrase(bean.getDate(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			creditcell21.setBorderColor(BaseColor.BLACK);
			creditcell21.setPaddingTop(5);
			creditcell21.setPaddingBottom(5);
			creditcell21.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell21.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell creditcell81 = new PdfPCell(new Paragraph(new Phrase(bean.getProductName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			creditcell81.setBorderColor(BaseColor.BLACK);
			creditcell81.setPaddingTop(5);
			creditcell81.setPaddingBottom(5);
			creditcell81.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell81.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell creditcell91 = new PdfPCell(new Paragraph(new Phrase(bean.getCases()+" / "+bean.getBottles(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			creditcell91.setBorderColor(BaseColor.BLACK);
			creditcell91.setPaddingTop(5);
			creditcell91.setPaddingBottom(5);
			creditcell91.setHorizontalAlignment(Element.ALIGN_CENTER);
			creditcell91.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell creditcell31 = new PdfPCell(new Paragraph(new Phrase(Double.toString(bean.getAfterIncludingDiscount()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			creditcell31.setBorderColor(BaseColor.BLACK);
			creditcell31.setPaddingTop(5);
			creditcell31.setPaddingBottom(5);
			creditcell31.setHorizontalAlignment(Element.ALIGN_RIGHT);
			creditcell31.setVerticalAlignment(Element.ALIGN_RIGHT);
			
			credittable.addCell(creditcell11);
			credittable.addCell(creditcell21);
			credittable.addCell(creditcell81);
			credittable.addCell(creditcell91);
			credittable.addCell(creditcell31);
							
		}
		PdfPCell creditcell51 = new PdfPCell(new Paragraph(new Phrase("Total", FontFactory.getFont(FontFactory.HELVETICA, 9))));
		creditcell51.setBorderColor(BaseColor.BLACK);
		creditcell51.setPaddingTop(5);
		creditcell51.setPaddingBottom(5);
		creditcell51.setHorizontalAlignment(Element.ALIGN_RIGHT);
		creditcell51.setVerticalAlignment(Element.ALIGN_RIGHT);
		creditcell51.setColspan(4);
						
		PdfPCell creditcell61 = new PdfPCell(new Paragraph(new Phrase(Double.toString(creditMasterBean.getTotalAmount()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
		creditcell61.setBorderColor(BaseColor.BLACK);
		creditcell61.setPaddingTop(5);
		creditcell61.setPaddingBottom(5);
		creditcell61.setHorizontalAlignment(Element.ALIGN_RIGHT);
		creditcell61.setVerticalAlignment(Element.ALIGN_RIGHT);
		
		credittable.addCell(creditcell51);
		credittable.addCell(creditcell61);
		if(creditMasterBean.getTotalAmount() != 0)
		document.add(credittable);
			
			// Creating Discount Table
			PdfPTable table = new PdfPTable(5); // 3 columns.
			table.setWidthPercentage(100); //Width 100%
			table.setSpacingBefore(10f); //Space before table
			table.setSpacingAfter(10f); //Space after table
            
			//Set Column widths
			float[] columnWidths = {1f, 1f, 1f, 1f, 1f};
			table.setWidths(columnWidths);
			
			PdfPCell cell1 = new PdfPCell(new Paragraph(new Phrase("Transaction Date", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			cell1.setBorderColor(BaseColor.BLACK);
			cell1.setPaddingTop(5);
			cell1.setPaddingBottom(5);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell cell2 = new PdfPCell(new Paragraph(new Phrase("Bank", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			cell2.setBorderColor(BaseColor.BLACK);
			cell2.setPaddingTop(5);
			cell2.setPaddingBottom(5);
			cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell2.setBackgroundColor(BaseColor.ORANGE);

			PdfPCell cell3 = new PdfPCell(new Paragraph(new Phrase("Transaction Type", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			cell3.setBorderColor(BaseColor.BLACK);
			cell3.setPaddingTop(5);
			cell3.setPaddingBottom(5);
			cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell3.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell cell5 = new PdfPCell(new Paragraph(new Phrase("Cheque No", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			cell5.setBorderColor(BaseColor.BLACK);
			cell5.setPaddingTop(5);
			cell5.setPaddingBottom(5);
			cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell5.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell cell4 = new PdfPCell(new Paragraph(new Phrase("Cheque Amount", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			cell4.setBorderColor(BaseColor.BLACK);
			cell4.setPaddingTop(5);
			cell4.setPaddingBottom(5);
			cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell4.setBackgroundColor(BaseColor.ORANGE);
      
			table.addCell(cell1);
			table.addCell(cell2);
			table.addCell(cell3);
			table.addCell(cell5);
			table.addCell(cell4);
			for(DiscountAsPerCompanyBeen bean : discountAsPerCompanyBeenList){
				//totalAdjcheck = totalAdjcheck+bean.getAdjustmentCheque();
				//sumOfCheck = sumOfCheck+bean.getChequeAmount();
				//totalDiscount = bean.getDiscountAmount();
				//totalRental = bean.getRentals();
				
				PdfPCell cell11 = new PdfPCell(new Paragraph(new Phrase(bean.getEnterprise(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell11.setBorderColor(BaseColor.BLACK);
				cell11.setPaddingTop(5);
				cell11.setPaddingBottom(5);
				cell11.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell21 = new PdfPCell(new Paragraph(new Phrase(bean.getCompanyName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell21.setBorderColor(BaseColor.BLACK);
				cell21.setPaddingTop(5);
				cell21.setPaddingBottom(5);
				cell21.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell21.setVerticalAlignment(Element.ALIGN_MIDDLE);

				PdfPCell cell31 = new PdfPCell(new Paragraph(new Phrase(bean.getComment(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell31.setBorderColor(BaseColor.BLACK);
				cell31.setPaddingTop(5);
				cell31.setPaddingBottom(5);
				cell31.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell31.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell51 = new PdfPCell(new Paragraph(new Phrase(bean.getColor(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell51.setBorderColor(BaseColor.BLACK);
				cell51.setPaddingTop(5);
				cell51.setPaddingBottom(5);
				cell51.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell51.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell41 = new PdfPCell(new Paragraph(new Phrase(Double.toString(bean.getChequeAmount()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell41.setBorderColor(BaseColor.BLACK);
				cell41.setPaddingTop(5);
				cell41.setPaddingBottom(5);
				cell41.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell41.setVerticalAlignment(Element.ALIGN_RIGHT);
	            if(bean.getChequeAmount() > 0){
				table.addCell(cell11);
				table.addCell(cell21);
				table.addCell(cell31);
				table.addCell(cell51);
				table.addCell(cell41);
	            }
	 		}
			PdfPCell cellreceived1 = new PdfPCell(new Paragraph(new Phrase("Total Cheques Received", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellreceived1.setBorderColor(BaseColor.BLACK);
			cellreceived1.setPaddingTop(5);
			cellreceived1.setPaddingBottom(5);
			cellreceived1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellreceived1.setVerticalAlignment(Element.ALIGN_RIGHT);
			cellreceived1.setColspan(4);
			PdfPCell cellreceived2 = new PdfPCell(new Paragraph(new Phrase(Double.toString(sumOfCheck), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellreceived2.setBorderColor(BaseColor.BLACK);
			cellreceived2.setPaddingTop(5);
			cellreceived2.setPaddingBottom(5);
			cellreceived2.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellreceived2.setVerticalAlignment(Element.ALIGN_RIGHT);
			
			table.addCell(cellreceived1);
			table.addCell(cellreceived2);
			
		
			document.add(table);

			// Discount Table
			PdfPTable discounttable = new PdfPTable(4); // 3 columns.
			discounttable.setWidthPercentage(100); //Width 100%
			discounttable.setSpacingBefore(10f); //Space before table
			discounttable.setSpacingAfter(10f); //Space after table
            
			//Set Column widths
			float[] columnWidthsDiscount = {1f, 1f, 1f, 1f};
			discounttable.setWidths(columnWidthsDiscount);
			
			/*PdfPCell discountcell1 = new PdfPCell(new Paragraph(new Phrase("Month", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			discountcell1.setBorderColor(BaseColor.BLACK);
			discountcell1.setPaddingLeft(5);
			discountcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			discountcell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			*/
			PdfPCell discountcell2 = new PdfPCell(new Paragraph(new Phrase("Brand", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			discountcell2.setBorderColor(BaseColor.BLACK);
			discountcell2.setPaddingTop(5);
			discountcell2.setPaddingBottom(5);
			discountcell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			discountcell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			discountcell2.setBackgroundColor(BaseColor.ORANGE);

			PdfPCell discountcell3 = new PdfPCell(new Paragraph(new Phrase("Case", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			discountcell3.setBorderColor(BaseColor.BLACK);
			discountcell3.setPaddingTop(5);
			discountcell3.setPaddingBottom(5);
			discountcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			discountcell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
			discountcell3.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell discountcell4 = new PdfPCell(new Paragraph(new Phrase("Disc/Case", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			discountcell4.setBorderColor(BaseColor.BLACK);
			discountcell4.setPaddingTop(5);
			discountcell4.setPaddingBottom(5);
			discountcell4.setHorizontalAlignment(Element.ALIGN_CENTER);
			discountcell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
			discountcell4.setBackgroundColor(BaseColor.ORANGE);
      
			PdfPCell discountcell5 = new PdfPCell(new Paragraph(new Phrase("Total", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			discountcell5.setBorderColor(BaseColor.BLACK);
			discountcell5.setPaddingTop(5);
			discountcell5.setPaddingBottom(5);
			discountcell5.setHorizontalAlignment(Element.ALIGN_CENTER);
			discountcell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
			discountcell5.setBackgroundColor(BaseColor.ORANGE);
      
			//discounttable.addCell(discountcell1);
			discounttable.addCell(discountcell2);
			discounttable.addCell(discountcell3);
			discounttable.addCell(discountcell4);
			discounttable.addCell(discountcell5);
			
			for(DiscountAndMonthBean discountBean : discountTransactionBeanList){
				PdfPCell discountcell11 = new PdfPCell(new Paragraph(new Phrase(discountBean.getMonth(), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
				discountcell11.setBorderColor(BaseColor.BLACK);
				discountcell11.setPaddingTop(5);
				discountcell11.setPaddingBottom(5);
				discountcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
				discountcell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
				discountcell11.setBackgroundColor(BaseColor.LIGHT_GRAY);
				discountcell11.setColspan(5);
				discounttable.addCell(discountcell11);
				for(DiscountMonthWiseBean monthWiseDisc : discountBean.getDiscountMonthWiseBean()){
					
					PdfPCell discountcell21 = new PdfPCell(new Paragraph(new Phrase(monthWiseDisc.getBrand(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					discountcell21.setBorderColor(BaseColor.BLACK);
					discountcell21.setPaddingLeft(5);
					discountcell21.setPaddingTop(5);
					discountcell21.setPaddingBottom(5);
					discountcell21.setHorizontalAlignment(Element.ALIGN_LEFT);
					discountcell21.setVerticalAlignment(Element.ALIGN_LEFT);

					PdfPCell discountcell31 = new PdfPCell(new Paragraph(new Phrase(Long.toString(monthWiseDisc.getCase()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					discountcell31.setBorderColor(BaseColor.BLACK);
					discountcell31.setPaddingTop(5);
					discountcell31.setPaddingBottom(5);
					discountcell31.setHorizontalAlignment(Element.ALIGN_CENTER);
					discountcell31.setVerticalAlignment(Element.ALIGN_MIDDLE);
					
					PdfPCell discountcell41 = new PdfPCell(new Paragraph(new Phrase(Double.toString(monthWiseDisc.getDiscount()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					discountcell41.setBorderColor(BaseColor.BLACK);
					discountcell41.setPaddingTop(5);
					discountcell41.setPaddingBottom(5);
					discountcell41.setHorizontalAlignment(Element.ALIGN_RIGHT);
					discountcell41.setVerticalAlignment(Element.ALIGN_RIGHT);
		      
					PdfPCell discountcell51 = new PdfPCell(new Paragraph(new Phrase(Double.toString(monthWiseDisc.getTotal()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					discountcell51.setBorderColor(BaseColor.BLACK);
					discountcell51.setPaddingTop(5);
					discountcell51.setPaddingBottom(5);
					discountcell51.setHorizontalAlignment(Element.ALIGN_RIGHT);
					discountcell51.setVerticalAlignment(Element.ALIGN_RIGHT);
					
					discounttable.addCell(discountcell21);
					discounttable.addCell(discountcell31);
					discounttable.addCell(discountcell41);
					discounttable.addCell(discountcell51);
				}
				PdfPCell discountSumcell11 = new PdfPCell(new Paragraph(new Phrase("Total", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
				discountSumcell11.setBorderColor(BaseColor.BLACK);
				discountSumcell11.setPaddingTop(5);
				discountSumcell11.setPaddingBottom(5);
				discountSumcell11.setHorizontalAlignment(Element.ALIGN_RIGHT);
				discountSumcell11.setVerticalAlignment(Element.ALIGN_RIGHT);
				discountSumcell11.setColspan(3);
				discounttable.addCell(discountSumcell11);
				
				PdfPCell discountSumcell12 = new PdfPCell(new Paragraph(new Phrase(Double.toString(discountBean.getTotalDiscount()), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
				discountSumcell12.setBorderColor(BaseColor.BLACK);
				discountSumcell11.setPaddingTop(5);
				discountSumcell11.setPaddingBottom(5);
				discountSumcell12.setHorizontalAlignment(Element.ALIGN_RIGHT);
				discountSumcell12.setVerticalAlignment(Element.ALIGN_RIGHT);
				discounttable.addCell(discountSumcell12);
				
			}
			PdfPCell discountTotalcell11 = new PdfPCell(new Paragraph(new Phrase("Grand Total Discount", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			discountTotalcell11.setBorderColor(BaseColor.BLACK);
			discountTotalcell11.setPaddingTop(5);
			discountTotalcell11.setPaddingBottom(5);
			discountTotalcell11.setHorizontalAlignment(Element.ALIGN_RIGHT);
			discountTotalcell11.setVerticalAlignment(Element.ALIGN_RIGHT);
			discountTotalcell11.setColspan(3);
			discounttable.addCell(discountTotalcell11);
			
			PdfPCell discountTotalcell12 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalDiscount), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			discountTotalcell12.setBorderColor(BaseColor.BLACK);
			discountTotalcell12.setPaddingTop(5);
			discountTotalcell12.setPaddingBottom(5);
			discountTotalcell12.setHorizontalAlignment(Element.ALIGN_RIGHT);
			discountTotalcell12.setVerticalAlignment(Element.ALIGN_RIGHT);
			discounttable.addCell(discountTotalcell12);
			
			PdfPCell discountRentalcell11 = new PdfPCell(new Paragraph(new Phrase("Rentals", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			discountRentalcell11.setBorderColor(BaseColor.BLACK);
			discountRentalcell11.setPaddingTop(5);
			discountRentalcell11.setPaddingBottom(5);
			discountRentalcell11.setHorizontalAlignment(Element.ALIGN_RIGHT);
			discountRentalcell11.setVerticalAlignment(Element.ALIGN_RIGHT);
			discountRentalcell11.setColspan(3);
			if(totalRental !=0)
			discounttable.addCell(discountRentalcell11);
			
			PdfPCell discountRentalcell12 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalRental), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			discountRentalcell12.setBorderColor(BaseColor.BLACK);
			discountRentalcell12.setPaddingTop(5);
			discountRentalcell12.setPaddingBottom(5);
			discountRentalcell12.setHorizontalAlignment(Element.ALIGN_RIGHT);
			discountRentalcell12.setVerticalAlignment(Element.ALIGN_RIGHT);
			if(totalRental !=0)
			discounttable.addCell(discountRentalcell12);
			
			document.add(discounttable);
			document.close();
			writer.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
 		return true;
	}
	 /**
	  * This method is used to get vendor discount for selected months and company
	  * */
	@RequestMapping(value = "mobileView/getVendorDiscount", method = RequestMethod.GET)
    public @ResponseBody List<VendorAdjustmentGetArrearsBean> getVendorDiscount(HttpServletRequest request,
    		HttpServletResponse response,@RequestParam("discountMonth") String discountMonth,@RequestParam("company") int company) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<VendorAdjustmentGetArrearsBean> vendorBean = new ArrayList<VendorAdjustmentGetArrearsBean>();
 		vendorBean = poizonMobileUIService.getVendorDiscount(discountMonth,company);
		return vendorBean;
    }
	/**
	 * This method is used to get all expense category from 'expense_category' tab.
	 * */
	@RequestMapping(value = "/mobileView/getExpenseCategory", method = RequestMethod.GET)
    public @ResponseBody ExpenseCategoryAndDate getExpenseCategory(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonMobileUIController.getExpenseCategory");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		ExpenseCategoryAndDate expenseCategoryList = null;
		expenseCategoryList = poizonService.getExpenseCategory();
		
		return expenseCategoryList;
    }
	/*This method is used to get latest cash card date from 'cash_card_tab'*/
	@RequestMapping(value = "/mobileView/getCashCardSaleDate", method = RequestMethod.GET)
    public @ResponseBody MessageBean getCashCardSaleDate(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonController.getCashCardSaleDate ");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		MessageBean messageBean = new MessageBean();
		//ModelAndView mav = new ModelAndView();
			String date = poizonService.getCashCardSaleDate();
			messageBean.setMessage(date);
			//mav.addObject("json",messageBean);
			return messageBean;
		
    }
	/**
	 * This method is used to save expenses for the date
	 * */
	@RequestMapping(value = "/mobileView/saveExpenses", method = RequestMethod.POST)
    public @ResponseBody MessageBean saveExpenses(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonController.saveExpenses values= "+values);
		log.info("Client Ip: "+request.getHeader("x-my-custom-header"));
		ModelAndView mav = new ModelAndView();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		MessageBean messageBean = new MessageBean();
		ObjectMapper mapper = new ObjectMapper();
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			//JSONArray expenseJson = jsonObject.getJSONArray("expenseDetails");
			result=poizonService.saveTabExpenses(jsonObject);
			//System.out.println("khagesh>>"+result);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
    }
	/*This method is used to save cash or card amount for the day*/
	@RequestMapping(value = "/mobileView/saveCardCashSale", method = RequestMethod.POST)
	public @ResponseBody MessageBean saveCardCashSale(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonController.saveCardCashSale values= "+values);
		log.info("Client Ip: "+request.getHeader("x-my-custom-header"));
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		MessageBean messageBean = new MessageBean();
		ModelAndView mav = new ModelAndView();
		ObjectMapper mapper = new ObjectMapper();
		CardCashSaleBeen cardCashSaleBeen = new CardCashSaleBeen();
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			response.setHeader("value", "valid");
			JSONObject appUserserJson = jsonObject.getJSONObject("cardsaleDetails");
			cardCashSaleBeen = mapper.readValue(appUserserJson.toString(), CardCashSaleBeen.class);
		    result = poizonService.saveCardCashSale(cardCashSaleBeen);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
		
	}
	@RequestMapping(value = "mobileView/buildDiscount", method = RequestMethod.GET)
	public @ResponseBody BuildDiscountBean buildDiscount(HttpServletRequest request, HttpServletResponse response, @RequestParam("startMonth") String startMonth,
			@RequestParam("endMonth") String endMonth,@RequestParam("company") int company){
		BuildDiscountBean buildDiscountBean = new BuildDiscountBean();
		double totalChequeAmount=0,adjCheque=0,totalVendorDiscount=0,totalDiscountAmt=0,totalVendorCredit=0,totalVendorCreditReceived=0;
		List<DiscountAndMonthBean>  monthAndDiscountList = CommonUtil.getListBasedOnMatch(poizonSecondService.getDiscountForSelectedMonth(startMonth,endMonth,company));
		buildDiscountBean.setTransactiontransactionData(monthAndDiscountList);
		List<DiscountAsPerCompanyBeen> receivedDataList = poizonSecondService.getDiscountChecksForSelectedMonths(startMonth,endMonth,company);
		buildDiscountBean.setReceivedData(receivedDataList);
		double rental = poizonSecondService.getRentalForSelected(startMonth,endMonth,company);
		PieChartBean previousArrears = poizonSecondService.getArrearsForPreviousDays(startMonth,company);
		category categoryBean = poizonSecondService.getCompanyWithEnterPrice(company);
		List<VendorAdjustmentGetArrearsBean> vendorDiscountList = poizonMobileUIService.getVendorDiscount(endMonth,company);
		buildDiscountBean.setArrearsdetails(vendorDiscountList);
		List<CreditChildBean> creditChildBeanList = poizonMobileUIService.getVendorCreditListWithRangeWise(startMonth,endMonth,company);
		buildDiscountBean.setCreditChildBeanList(creditChildBeanList);
		CreditMasterBean creditMasterBean = poizonMobileUIService.getTotalCreditAmountAndReceived(startMonth,endMonth,company);
		buildDiscountBean.setCreditMasterBean(creditMasterBean);
		/*for(CreditMasterBean creditMasterBean : creditMasterBeanList){
			totalVendorCredit += creditMasterBean.getTotalAmount();
			totalVendorCreditReceived += creditMasterBean.getReceived();
		}*/
		
		for(DiscountAndMonthBean bean: monthAndDiscountList){
			totalDiscountAmt += bean.getTotalDiscount();
		}
		buildDiscountBean.setTotalDiscountAmt(totalDiscountAmt);
		for(DiscountAsPerCompanyBeen bean : receivedDataList){
			totalChequeAmount += bean.getChequeAmount();
			adjCheque += bean.getAdjustmentCheque();
		}
		buildDiscountBean.setAdjCheque(adjCheque);
		buildDiscountBean.setTotalChequeAmount(totalChequeAmount);
		for(VendorAdjustmentGetArrearsBean bean : vendorDiscountList){
			totalVendorDiscount += bean.getVendorAmt();
			
		}
		buildDiscountBean.setTotalVendorDiscount(totalVendorDiscount);
		buildDiscountBean.setRental(rental);
		buildDiscountBean.setPreviousArrears(previousArrears);
		buildDiscountBean.setCategoryBean(categoryBean);
		buildDiscountBean.setTotalVendorCredit(totalVendorCredit);
		buildDiscountBean.setTotalVendorCreditReceived(totalVendorCreditReceived);
		return buildDiscountBean;
		
	}
	@RequestMapping(value = "mobileView/buildmobiledicountpdf", method = RequestMethod.GET)
	public @ResponseBody BuildDiscountBean buildmobiledicountpdf(HttpServletRequest request, HttpServletResponse response, @RequestParam("date") String date,
			@RequestParam("company") int company){
		BuildDiscountBean buildDiscountBean = new BuildDiscountBean();
		double totalChequeAmount=0,adjCheque=0,totalVendorDiscount=0,totalDiscountAmt=0;
		List<DiscountAndMonthBean> discountTransactionBeanList = CommonUtil.getListBasedOnMatch(poizonSecondService.getDiscountCompanyWiseForAllMonth(date,company));
		List<DiscountAsPerCompanyBeen> discountAsPerCompanyBeenList = poizonSecondService.getDiscountCompanyWiseForAllMonthWithReceived(company);
		double rental = poizonSecondService.getUpToMonthRental(date,company);
		List<VendorAdjustmentGetArrearsBean> vendorBean = poizonMobileUIService.getVendorDiscount(date,company);
		List<CreditChildBean> creditChildBeanList = poizonMobileUIService.getVendorCreditListToTillMonth(date,company);
		buildDiscountBean.setCreditChildBeanList(creditChildBeanList);
		CreditMasterBean creditMasterBean = poizonMobileUIService.getTotalCreditAmountAndReceivedTillMonth(date,company);
		buildDiscountBean.setCreditMasterBean(creditMasterBean);
		
		buildDiscountBean.setReceivedData(discountAsPerCompanyBeenList);
		buildDiscountBean.setTransactiontransactionData(discountTransactionBeanList);
		buildDiscountBean.setRental(rental);
		buildDiscountBean.setArrearsdetails(vendorBean);
		
		for(DiscountAndMonthBean bean: discountTransactionBeanList){
			totalDiscountAmt += bean.getTotalDiscount();
		}
		buildDiscountBean.setTotalDiscountAmt(totalDiscountAmt);
		
		for(DiscountAsPerCompanyBeen bean : discountAsPerCompanyBeenList){
			totalChequeAmount += bean.getChequeAmount();
			adjCheque += bean.getAdjustmentCheque();
		}
		buildDiscountBean.setAdjCheque(adjCheque);
		buildDiscountBean.setTotalChequeAmount(totalChequeAmount);
		for(VendorAdjustmentGetArrearsBean bean : vendorBean){
			totalVendorDiscount += bean.getVendorAmt();
			
		}
		buildDiscountBean.setTotalVendorDiscount(totalVendorDiscount);
		
		return buildDiscountBean;
		
		//comment............
	}
	
}
