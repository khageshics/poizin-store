package com.zambient.poizon.controller;


import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zambient.poizon.bean.AddNewBrandBean;
import com.zambient.poizon.bean.BalanceSheetWithBreckage;
import com.zambient.poizon.bean.CardCashSaleBeen;
import com.zambient.poizon.bean.ChartDataBean;
import com.zambient.poizon.bean.CreditMasterBean;
import com.zambient.poizon.bean.CreditPaymentBean;
import com.zambient.poizon.bean.DiscountEstimationBean;
import com.zambient.poizon.bean.DiscountEstimationWithDate;
import com.zambient.poizon.bean.DropDownsBean;
import com.zambient.poizon.bean.ExpenseBean;
import com.zambient.poizon.bean.ExpenseCategoryAndDate;
import com.zambient.poizon.bean.InvoiceDateBean;
import com.zambient.poizon.bean.InvoiceReportWithMrpRoundOffBean;
import com.zambient.poizon.bean.MessageBean;
import com.zambient.poizon.bean.NewSaleBean;
import com.zambient.poizon.bean.NoDiscountProductsBean;
import com.zambient.poizon.bean.PendingLiftedCaseDiscountBean;
import com.zambient.poizon.bean.SaleAndAmmountBean;
import com.zambient.poizon.bean.SaleBean;
import com.zambient.poizon.bean.SaleBeanAndCashCardExpensesBean;
import com.zambient.poizon.bean.SchemeEstimateBean;
import com.zambient.poizon.bean.TotalPriceDateWiseBean;
import com.zambient.poizon.bean.UpdateExpensesBean;
import com.zambient.poizon.services.PoizonService;
import com.zambient.poizon.utill.CommonUtil;
import com.zambient.poizon.utill.PoizinMail;
import com.zambient.poizon.utill.UserSession;

@Controller
public class PoizonController {
	final static Logger log = Logger.getLogger(PoizonController.class);
	
	@Autowired 
	private UserSession userSession;
	@Autowired
	PoizonService poizonService;
	@Value("${poizinPdfFile}")
	private String poizinPdfFile;
	@Autowired
	PoizinMail poizinMail;
	@Value("${mail.from.username}")
	private String mailFromUsername;
	@Value("${mail.from.bccusername}")
	private String mailFromBccUsername;
	
	/** 
	 * This method is used to add new product to product tab including all details of that product.
	 * And this will return String message like Success or failure and redirecting to AddNewBrand jsp page.
	 * */
	@RequestMapping(value="/addNewBrandName",method=RequestMethod.POST)
	public String addNewBrandName(@ModelAttribute("addNewBrandBean") AddNewBrandBean addNewBrandBean,HttpServletRequest request,HttpServletResponse response,RedirectAttributes redirectAttributes){
		log.info("PoizonController.addNewBrandName Bean>> "+addNewBrandBean);
		String result=null;
		if(addNewBrandBean != null)
	    result= poizonService.addNewBrandName(addNewBrandBean);
		redirectAttributes.addFlashAttribute("message", result);
		return "redirect:/addNewBrand";
	}
	/**
	 * This method is used to distinct categories, pack qty, product type and pack type.
	 * */
	@RequestMapping(value = "/getDropDownListFromProduct", method = RequestMethod.GET)
    public @ResponseBody DropDownsBean getDropDownListFromProduct(HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		DropDownsBean dropDownsBean = null;
 		dropDownsBean = poizonService.getDropDownListFromProduct();
		return dropDownsBean;
    }
	/**
	 * This method is used to get all product list with details from product table.
	 * And returns the list data in json format.
	 * */
	@RequestMapping(value = "/getBrandDetails", method = RequestMethod.GET)
    public @ResponseBody List<AddNewBrandBean> getBrandDetails(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonController.getBrandDetails");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<AddNewBrandBean> addNewBrandList = null;
		addNewBrandList = poizonService.getBrandDetails();
		
		return addNewBrandList;
    }
	/**
	 * This method is used to get single product with details from product table.
	 * And returns that product in json format.
	 * */
	@RequestMapping(value = "/getSingleBrandDetail", method = RequestMethod.POST)
    public @ResponseBody List<AddNewBrandBean> getSingleBrandDetail(HttpServletRequest request, HttpServletResponse response,@RequestParam("brandNoAndPackQty") String brandNoAndPackQty) {
		log.info("PoizonController.getSingleBrandDetail> "+brandNoAndPackQty);
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<AddNewBrandBean> addNewBrandList = null;
		if(brandNoAndPackQty.length() !=0)
		addNewBrandList = poizonService.getSingleBrandDetail(brandNoAndPackQty);
		return addNewBrandList;
    }
	/**
	 * This method is used to get all products to update closing.
	 * */
	@RequestMapping(value = "/getSingleSaleBrandDetailForClosing", method = RequestMethod.POST)
    public @ResponseBody SaleAndAmmountBean getSingleSaleBrandDetail(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonController.getSingleSaleBrandDetailForClosing ");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		SaleAndAmmountBean saleBeanList = null;
			saleBeanList = poizonService.getSingleSaleBrandDetailForClosing();
			log.info("saleBeanList>> "+saleBeanList);
		return saleBeanList;
    }
	/**
	 * This method is used to authenticateAdmin admin.
	 * if admin is valid admin then we are creating session and adding userid and redirecting to dashboard.
	 * */
	@RequestMapping("/adminUserLogin")  
    public String adminUserLogin(HttpServletRequest request,RedirectAttributes redirectAttributes,@RequestParam("userId") String userId){
		log.info("PoizonController.adminUserLogin");
	 if(userId == null && userId == "" && userId.trim().length()==0){
		 redirectAttributes.addFlashAttribute("message", "Please Enter User ID");
		 return "redirect:/admin";
	 }
	 String result=poizonService.authenticateAdmin();
	if(result.equals(userId)){
		userSession.setUserId(result);
		userSession.setLoginId(Long.parseLong("123"));
		return "redirect:/admin";
	}
	else{
		 redirectAttributes.addFlashAttribute("message", "Please Enter Correct User ID");
		 return "redirect:/admin";
      } 
   }
	/**
	 * This method is used to authenticateAdmin admin by mobile App.
	 * if admin is valid admin then we are creating session and adding userid and redirecting to dashboard.
	 * */
	@RequestMapping(value ="/appUserLogin", method = RequestMethod.POST)
	public String appUserLogin(HttpServletRequest request, RedirectAttributes redirectAttributes,@RequestParam("userId") String userId) {
		log.info("HybridController.appUserLogin");
		String result = null;
		if (userId != null && userId != "" && userId.trim().length() != 0) {
			result = poizonService.authenticateAdmin();
		}
		if (result.equals(userId)) {
			userSession.setUserId(result);
			userSession.setLoginId(Long.parseLong("123"));
			return "redirect:/mobileViewHome";
			
		} else {
			redirectAttributes.addFlashAttribute("message", "Please Enter Correct User ID");
		  return "redirect:/loginInMobile";
		}
	}
	/**
	 * This method is used to logout from the application
	 * */
	@RequestMapping(value="/logout")
	public String logout(HttpServletRequest request,RedirectAttributes redirectAttribute){
		log.info("PoizonController.logout");
		try{
			userSession.setLoginId(Long.parseLong("0"));
			HttpSession userSession = request.getSession(false);
			userSession.invalidate();
			redirectAttribute.addFlashAttribute("message", "Logout Sucessfully");
			return "redirect:/admin";
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	} 
	/**
	 * This method is used to logout from the mobile application
	 * */
	@RequestMapping(value="/logoutByMobile")
	public String logoutByMobile(HttpServletRequest request,RedirectAttributes redirectAttribute){
		log.info("PoizonController.logoutByMobile");
		try{
			userSession.setLoginId(Long.parseLong("0"));
			HttpSession userSession = request.getSession(false);
			userSession.invalidate();
			redirectAttribute.addFlashAttribute("message", "Logout Sucessfully");
			return "redirect:/loginInMobile";
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	} 
	/**
	 * This method is used to save sale for the day and return the message like success or failed 
	 * */
	@RequestMapping(value = "/saveSingleSaleBrandDetail", method = RequestMethod.POST)
    public @ResponseBody MessageBean saveSingleSaleBrandDetail(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonController.saveSingleSaleBrandDetail>> "+values);
		ModelAndView mav = new ModelAndView();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		MessageBean messageBean = new MessageBean();
		ObjectMapper mapper = new ObjectMapper();
		boolean flag=true;
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			//log.info("Client IP: "+jsonObject.get("ipAddr"));
			JSONArray saleDetailsJson = jsonObject.getJSONArray("saleDetails");
			//System.out.println(saleDetailsJson);
			for (int i = 0; i < saleDetailsJson.length(); ++i) {
				 JSONObject rec;
				    rec = saleDetailsJson.getJSONObject(i);
				    int totalopening = Integer.parseInt(rec.getString("opening"))+Integer.parseInt(rec.getString("received"))+Integer.parseInt(rec.getString("returnbtl"));
				    int totalclosing = Integer.parseInt(rec.getString("closing"))+Integer.parseInt(rec.getString("totalSale"));
				    if(totalopening != totalclosing){
				    	flag=false;
				    	result="Something went wrong please try agian!";
				    }
				}
			if(flag)
			result=poizonService.saveSingleSaleBrandDetail(saleDetailsJson);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
    }
	/**
	 * This method is used to save invoice data to 'invoice' table.
	 * As input getting data in json format.
	 * */
	@RequestMapping(value = "/saveInvoiceJsonData", method = RequestMethod.POST)
    public @ResponseBody MessageBean saveInvoiceJsonData(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		log.info("Client Ip: "+request.getHeader("x-my-custom-header"));
		log.info("PoizonController.saveInvoiceJsonData>> "+values);
		ModelAndView mav = new ModelAndView();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		MessageBean messageBean = new MessageBean();
		ObjectMapper mapper = new ObjectMapper();
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			JSONArray invoiceJson = jsonObject.getJSONArray("invoiceDetails");
			System.out.println(invoiceJson);
			if(invoiceJson.length() !=0)
			result=poizonService.saveInvoiceJsonData(invoiceJson);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
    }
	/**
	 * This method is used to update Mrp for all product
	 * */
	@RequestMapping(value = "/updateLatestMrpForAllProducts", method = RequestMethod.POST)
    public @ResponseBody String updateLatestMrpForAllProducts(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			JSONArray invoiceJson = jsonObject.getJSONArray("invoiceDetails");
			if(invoiceJson.length() !=0)
			result=poizonService.updateLatestMrpForAllProducts(invoiceJson);
			return result;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			return result;
		}
    }
	/**
	 * This method is used to get distinct invoice date list from 'invoice' table.
	 * */
	@RequestMapping(value = "/getTotalInvoiceDate", method = RequestMethod.POST)
    public @ResponseBody List<InvoiceDateBean> getTotalInvoiceDate(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonController.getTotalInvoiceDate");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<InvoiceDateBean> nvoiceDateList = null;
		nvoiceDateList = poizonService.getTotalInvoiceDate();
		return nvoiceDateList;
    }
	/**
	 * This method is used to get distinct sale date list from 'sale' table.
	 * */
	@RequestMapping(value = "/getTotalSaleDate", method = RequestMethod.GET)
    public @ResponseBody List<InvoiceDateBean> getTotalSaleDate(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonController.getTotalInvoiceDate");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<InvoiceDateBean> nvoiceDateList = null;
		nvoiceDateList = poizonService.getTotalSaleDate();
		return nvoiceDateList;
    }
	/**
	 * This method is used to get distinct sale date list from 'sale' table where no. of sale is greater than zero.
	 * */
	@RequestMapping(value = "/getSaleReportSaleDate", method = RequestMethod.POST)
    public @ResponseBody List<InvoiceDateBean> getSaleReportSaleDate(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonController.getSaleReportSaleDate");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<InvoiceDateBean> nvoiceDateList = null;
		nvoiceDateList = poizonService.getSaleReportSaleDate();
		return nvoiceDateList;
    }
	/**
	 * This method is used to get single date invoice details from 'invoice' tab.
	 * */
	@RequestMapping(value = "/getSingleInvoiceDetailDateWise", method = RequestMethod.GET)
    public @ResponseBody InvoiceReportWithMrpRoundOffBean getSingleInvoiceDetailDateWise(HttpServletRequest request, HttpServletResponse response,@RequestParam("ivoicedate") String ivoicedate) {
		log.info("PoizonController.getSingleInvoiceDetailDateWise invoice date= "+ivoicedate);
		System.out.println("ivoicedate>> "+ivoicedate);
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		ivoicedate = CommonUtil.chageDateFormat(ivoicedate);
 		InvoiceReportWithMrpRoundOffBean singleInvoiceList = null;
		singleInvoiceList = poizonService.getSingleInvoiceDetailDateWise(ivoicedate);
		return singleInvoiceList;
    }
/**
 * Old method not used any where
 * */
	@RequestMapping(value = "/getProfitPercentage", method = RequestMethod.POST)
    public @ResponseBody List<SaleBean> getProfitPercentage(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonController.getProfitPercentage");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<SaleBean> profitPercentage = null;
		profitPercentage = poizonService.getProfitPercentage();
		return profitPercentage;
    }
	/**
	 * This method is used to get Lifted no. of cases and sum of mrprounding off, tcs tournover tax for the selecetd dates.
	 * */
	@RequestMapping(value = "/getStockLifting", method = RequestMethod.GET)
    public @ResponseBody List<SaleBean> getStockLifting(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		log.info("PoizonController.getStockLifting");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		String sdate = CommonUtil.chageDateFormat(request.getParameter("startDate"));
		String edate = CommonUtil.chageDateFormat(request.getParameter("endDate"));
		List<SaleBean> stockLifting = null;
		stockLifting = poizonService.getStockLifting(sdate,edate);
		return stockLifting;
    }
	/*Old method not used any where*/
	@RequestMapping(value = "/getSaleAmountMonthWise", method = RequestMethod.GET)
    public @ResponseBody List<TotalPriceDateWiseBean> getSaleAmountMonthWise(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<TotalPriceDateWiseBean> anaylysisList = null;
		//if(getNoOfDays(startDate,endDate))
		 anaylysisList = poizonService.getSaleAmountMonthWise();
		//System.out.println("anaylysisList>> "+anaylysisList);
		return anaylysisList;
    }
	/*Old method not used any where*/
	@RequestMapping(value = "/getTopSaleDetails", method = RequestMethod.POST)
    public @ResponseBody List<SaleBean> getTopSaleDetails(HttpServletRequest request, HttpServletResponse response,@RequestParam("startDate") String startDate,@RequestParam("endDate") String endDate,@RequestParam("type") int type) {
		log.info("PoizonController.getTopSaleDetails Start date = "+startDate+" end date= "+endDate+" type= "+type);
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<SaleBean> saleBeanList = null;
		if(startDate.trim().length()>0 && endDate.trim().length()>0)
			saleBeanList = poizonService.getTopSaleDetails(startDate,endDate,type);
		return saleBeanList;
    }
	/**
	 * This method is used to get no. of sold cases with product wise in between the selecetd dates.
	 * And also getting available current stock and prediction no. of days for currect stock.
	 * */
	@RequestMapping(value = "/getSaleReports", method = RequestMethod.GET)
    public @ResponseBody SaleAndAmmountBean getSaleReports(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		log.info("PoizonController.getSaleReports");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		String sdate = CommonUtil.chageDateFormat(request.getParameter("startDate"));
		String edate = CommonUtil.chageDateFormat(request.getParameter("endDate"));
		SaleAndAmmountBean saleAndAmmountBean = null;
		saleAndAmmountBean = poizonService.getSaleReports(sdate,edate);
		return saleAndAmmountBean;
    }
	/**
	 * This method is used to get all expense category from 'expense_category' tab.
	 * */
	@RequestMapping(value = "/getExpenseCategory", method = RequestMethod.GET)
    public @ResponseBody ExpenseCategoryAndDate getExpenseCategory(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonController.getExpenseCategory");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		ExpenseCategoryAndDate expenseCategoryList = null;
		expenseCategoryList = poizonService.getExpenseCategory();
		
		return expenseCategoryList;
    }
	/**
	 * This method is used to save expenses for the date
	 * */
	@RequestMapping(value = "/saveExpenses", method = RequestMethod.POST)
    public @ResponseBody MessageBean saveExpenses(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonController.saveExpenses values= "+values);
		log.info("Client Ip: "+request.getHeader("x-my-custom-header"));
		ModelAndView mav = new ModelAndView();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		MessageBean messageBean = new MessageBean();
		ObjectMapper mapper = new ObjectMapper();
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			JSONArray expenseJson = jsonObject.getJSONArray("expenseDetails");
			result=poizonService.saveExpenses(expenseJson);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
    }
	@RequestMapping(value = "/getUpdateSale", method = RequestMethod.POST)
    public @ResponseBody List<SaleBean> getUpdateSale(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonController.getUpdateSale");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<SaleBean> saleList = null;
		saleList = poizonService.getUpdateSale();
		
		return saleList;
    }
	/*This method is used to update the sale */
	@RequestMapping(value = "/updateSaleDetails", method = RequestMethod.POST)
    public @ResponseBody MessageBean updateSaleDetails(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonController.updateSaleDetails values= "+values);
		log.info("Client Ip: "+request.getHeader("x-my-custom-header"));
		ModelAndView mav = new ModelAndView();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		MessageBean messageBean = new MessageBean();
		ObjectMapper mapper = new ObjectMapper();
		String result=null;
		boolean flag=true;
		try {
			JSONObject jsonObject = new JSONObject(values);
			JSONArray saleDetailsJson = jsonObject.getJSONArray("saleDetails");
			System.out.println(saleDetailsJson);
			for (int i = 0; i < saleDetailsJson.length(); ++i) {
				 JSONObject rec;
				    rec = saleDetailsJson.getJSONObject(i);
				    int totalopening = rec.getInt("opening")+rec.getInt("received")+rec.getInt("returnval");
				    int totalclosing = rec.getInt("closing")+rec.getInt("totalSale");
				    if(totalopening != totalclosing || totalopening < rec.getInt("closing")){
				    	flag=false;
				    	result="Something went wrong please try agian!";
				    }
				}
			if(flag)
			result=poizonService.updateSaleDetails(saleDetailsJson);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
    }
	/*This method is used to update cash or card amount for the latest date*/
	@RequestMapping(value = "/getCashCardSale", method = RequestMethod.POST)
    public @ResponseBody CardCashSaleBeen getCashCardSale(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonController.getCashCardSale");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		CardCashSaleBeen cardCashSaleBeen = null;
 		cardCashSaleBeen = poizonService.getCashCardSale();
		
		return cardCashSaleBeen;
    }
	/*This method is used to save cash or card amount for the day*/
	@RequestMapping(value = "/saveCardCashSale", method = RequestMethod.POST)
	public @ResponseBody MessageBean saveCardCashSale(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonController.saveCardCashSale values= "+values);
		log.info("Client Ip: "+request.getHeader("x-my-custom-header"));
		MessageBean messageBean = new MessageBean();
		ModelAndView mav = new ModelAndView();
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		CardCashSaleBeen cardCashSaleBeen = new CardCashSaleBeen();
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			response.setHeader("value", "valid");
			JSONObject appUserserJson = jsonObject.getJSONObject("cardsaleDetails");
			cardCashSaleBeen = mapper.readValue(appUserserJson.toString(), CardCashSaleBeen.class);
		    result = poizonService.saveCardCashSale(cardCashSaleBeen);
			/*
			 * if(result != null && result !=""){ boolean value =
			 * poizonService.saveSaleWithZeroOrNot(cardCashSaleBeen.getDate()); if(value)
			 * downloadIndentProduct(cardCashSaleBeen.getDate()); }
			 */
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
		
	}
	/**
	 * This method is used to get Single day sale report for each products.
	 * */
	@RequestMapping(value = "/getSaleDayWiseReports", method = RequestMethod.GET)
    public @ResponseBody SaleBeanAndCashCardExpensesBean getSaleDayWiseReports(HttpServletRequest request, HttpServletResponse response,@RequestParam("startDate") String startDate) {
		log.info("PoizonController.getSaleDayWiseReports startDate= "+startDate);
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		SaleBeanAndCashCardExpensesBean saleDetails = null;
		if(startDate.length() !=0)
			saleDetails = poizonService.getSaleDayWiseReports(CommonUtil.chageDateFormat(startDate));
		//System.out.println("saleDetails>> "+saleDetails);
		return saleDetails;
    }
	/*This method is used to get latest cash card date from 'cash_card_tab'*/
	@RequestMapping(value = "/getCashCardSaleDate", method = RequestMethod.GET)
    public @ResponseBody MessageBean getCashCardSaleDate(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonController.getCashCardSaleDate ");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		MessageBean messageBean = new MessageBean();
		ModelAndView mav = new ModelAndView();
			String date = poizonService.getCashCardSaleDate();
			messageBean.setMessage(date);
			mav.addObject("json",messageBean);
			return messageBean;
		
    }
	/**
	 * This method is used to get latest epenses data from 'expense_master' and 'expense_child' to updated latest expenses
	 * */
	@RequestMapping(value = "/getExpensesData", method = RequestMethod.POST)
    public @ResponseBody UpdateExpensesBean getExpensesData(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonController.getExpensesData");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		UpdateExpensesBean updateExpensesBean = null;
 		updateExpensesBean = poizonService.getExpensesData();
		
		return updateExpensesBean;
    }
	/**
	 * This method is used to update latest epenses data in 'expense_master' and 'expense_child'.
	 * */
	@RequestMapping(value = "/updateUpdateExpenses", method = RequestMethod.POST)
    public @ResponseBody MessageBean updateUpdateExpenses(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonController.updateUpdateExpenses values= "+values);
		log.info("Client Ip: "+request.getHeader("x-my-custom-header"));
		ModelAndView mav = new ModelAndView();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		MessageBean messageBean = new MessageBean();
		ObjectMapper mapper = new ObjectMapper();
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			JSONArray expenseJson = jsonObject.getJSONArray("expenseDetails");
			result=poizonService.updateUpdateExpenses(expenseJson);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
    }
	/*This method is used to get sale from 'sale' tab to update the sale*/
	@RequestMapping(value = "/getSingleSaleDetailForUpdate", method = RequestMethod.POST)
    public @ResponseBody List<SaleBean> getSingleSaleDetailForUpdate(HttpServletRequest request, HttpServletResponse response,@RequestParam("brandNoAndPackQty") String brandNoAndPackQty,@RequestParam("date") String date) {
		log.info("PoizonController.getSingleSaleDetailForUpdate brandNoAndPackQty= "+brandNoAndPackQty+" date= "+date);
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<SaleBean> saleBeanList = null;
 		saleBeanList = poizonService.getSingleSaleDetailForUpdate(brandNoAndPackQty,CommonUtil.chageDateFormat(date));
		
		return saleBeanList;
    }
	/**
	 * This method is used to get in-house stock for all product based on selected date.
	 * */
	@RequestMapping(value = "/getInHouseStockValue", method = RequestMethod.GET)
    public @ResponseBody List<SaleBean> getInHouseStockValue(HttpServletRequest request, HttpServletResponse response,@RequestParam("startDate") String startDate) {
		log.info("PoizonController.getInHouseStockValue startDate= "+startDate);
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<SaleBean> saleBeanList = null;
 		saleBeanList = poizonService.getInHouseStockValue(CommonUtil.chageDateFormat(startDate));
		
		return saleBeanList;
    }
	/**
	 * This method is used to generate balance based on selected dates
	 * Getting sale, card,cash & checque amt, expenses details, bank credited amt and retention.
	 * */
	@RequestMapping(value = "/getBalanceSheet", method = RequestMethod.GET)
    public @ResponseBody BalanceSheetWithBreckage getBalanceSheet(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonController.getBalanceSheet ");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		String sdate = CommonUtil.chageDateFormat(request.getParameter("startDate"));
		String edate = CommonUtil.chageDateFormat(request.getParameter("endDate"));
 		
		BalanceSheetWithBreckage balanceSheetList = null;
 		balanceSheetList = poizonService.getBalanceSheet(sdate,edate);
		
		return balanceSheetList;
    }
	/*This method is used for Edit the bottle mrp*/
	@RequestMapping(value = "/getDetailsforEditMrp", method = RequestMethod.POST)
    public @ResponseBody List<SaleBean> getDetailsforEditMrp(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonController.getDetailsforEditMrp ");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<SaleBean> saleBeanList = null;
 		saleBeanList = poizonService.getDetailsforEditMrp();
		
		return saleBeanList;
    }
	/*Old method not used any where*/
	@RequestMapping(value = "/getProductComparision", method = RequestMethod.POST)
    public @ResponseBody List<SaleBean> getProductComparision(HttpServletRequest request, HttpServletResponse response,@RequestParam("brandNoPackQtys") String brandNoPackQtys,@RequestParam("startdate") String startdate,@RequestParam("enddate") String enddate) {
		log.info("PoizonController.getProductComparision brandNoPackQtys= "+brandNoPackQtys+" startdate= "+startdate+" enddate= "+enddate);
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<SaleBean> saleBeanList = null;
 		saleBeanList = poizonService.getProductComparision(brandNoPackQtys,startdate,enddate);
		
		return saleBeanList;
    }
	/*Old method not used any where*/
	@RequestMapping(value = "/getNewSaleReports", method = RequestMethod.GET)
    public @ResponseBody List<NewSaleBean> getNewSaleReports(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		log.info("PoizonController.getNewSaleReports");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		String sdate=request.getParameter("startDate");
		String edate=request.getParameter("endDate");
		List<NewSaleBean> saleAndAmmountBean = null;
		saleAndAmmountBean = poizonService.getNewSaleReports(sdate,edate);
		return saleAndAmmountBean;
    }
	/*Old method not used any where*/
	@RequestMapping(value = "/getStockLiftGraphData", method = RequestMethod.GET)
    public @ResponseBody List<SaleBean> getStockLiftGraphData(HttpServletRequest request, HttpServletResponse response,@RequestParam("startDate") String startDate,@RequestParam("endDate") String endDate) throws ParseException {
		log.info("PoizonController.getStockLiftGraphData startDate= "+startDate+" endDate= "+endDate);
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<SaleBean> graphList = null;
		graphList = poizonService.getStockLiftGraphData(startDate,endDate);
		return graphList;
    }
	/*@RequestMapping(value = "/getSameRangeProducts", method = RequestMethod.GET)
    public @ResponseBody List<SaleBean> getSameRangeProducts(HttpServletRequest request, HttpServletResponse response,@RequestParam("price") String price,@RequestParam("category") String category,@RequestParam("qty") String qty) throws ParseException {
		log.info("PoizonController.getSameRangeProducts unitPrice= "+price+" category= "+category+" qty= "+qty);
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<SaleBean> sameProductList = null;
		int unitPrice=Integer.parseInt(price);
		sameProductList = poizonService.getSameRangeProducts(unitPrice,category,qty);
		return sameProductList;
    }*/
	/*Old method not used any where*/
	@RequestMapping(value = "/getSameRangeProducts", method = RequestMethod.GET)
    public @ResponseBody List<SaleBean> getSameRangeProducts(HttpServletRequest request, HttpServletResponse response,@RequestParam("match") String match,@RequestParam("qty") String qty) throws ParseException {
		log.info("PoizonController.getSameRangeProducts match= "+match);
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<SaleBean> sameProductList = null;
		int matchval=Integer.parseInt(match);
		sameProductList = poizonService.getSameRangeProducts(matchval,qty);
		return sameProductList;
    }
   /* This method is used to get expense details for a day*/
	@RequestMapping(value = "/getExpenseDetails", method = RequestMethod.GET)
    public @ResponseBody List<ExpenseBean> getExpenseDetails(HttpServletRequest request, HttpServletResponse response,@RequestParam("expenseMasterID") String expenseMasterID) throws ParseException {
		log.info("PoizonController.getExpenseDetails expenseMasterID= "+expenseMasterID);
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<ExpenseBean> expenseList = null;
		expenseList = poizonService.getExpenseDetails(expenseMasterID);
		return expenseList;
    }
	/**
	 * Old method not used any where
	 * */
	@RequestMapping(value = "/getDiscountEsitmateData", method = RequestMethod.GET)
    public @ResponseBody DiscountEstimationWithDate getDiscountEsitmateData(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		log.info("PoizonController.getDiscountEsitmateData");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		DiscountEstimationWithDate discountEstimateList = null;
		discountEstimateList = poizonService.getDiscountEsitmateData();
		return discountEstimateList;
    }
	/**
	 * This method is used to generate indent estimate.
	 * Getting saved discount amount, current stock, lifted stock, target, pending and etc
	 * */
	@RequestMapping(value = "/getDiscountEsitmateDataWithDays", method = RequestMethod.GET)
    public @ResponseBody DiscountEstimationWithDate getDiscountEsitmateDataWithDays(HttpServletRequest request, HttpServletResponse response, 
    		@RequestParam("noOfDays") int noOfDays,@RequestParam("date") String date) throws ParseException {
		log.info("PoizonController.getDiscountEsitmateData");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		DiscountEstimationWithDate discountEstimateList = null;
		discountEstimateList = poizonService.getDiscountEsitmateDataWithDays(noOfDays,CommonUtil.chageDateFormat(date));
		return discountEstimateList;
    }
	/*Old method not used any where*/
	@RequestMapping(value = "/getBeerCasesGraphData", method = RequestMethod.GET)
    public @ResponseBody List<ChartDataBean> getBeerCasesGraphData(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		log.info("PoizonController.getBeerCasesGraphData");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		String sdate=request.getParameter("startDate");
		String edate=request.getParameter("endDate");
		List<ChartDataBean> chartDataList = null;
		//if(getNoOfDays(sdate,edate))
		chartDataList = poizonService.getBeerCasesGraphData(sdate,edate);
		return chartDataList;
    }
	/*Old method not used any where*/
	@RequestMapping(value = "/getLiquorCasesGraphData", method = RequestMethod.GET)
    public @ResponseBody List<ChartDataBean> getLiquorCasesGraphData(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		log.info("PoizonController.getLiquorCasesGraphData");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		String sdate=request.getParameter("startDate");
		String edate=request.getParameter("endDate");
		List<ChartDataBean> chartDataList = null;
		//if(getNoOfDays(sdate,edate))
		chartDataList = poizonService.getLiquorCasesGraphData(sdate,edate);
		//System.out.println("chartDataList>> "+chartDataList);
		return chartDataList;
    }
	/*Old method not used any where*/
	@RequestMapping(value = "/saveDiscountEsitmateData", method = RequestMethod.POST)
    public @ResponseBody MessageBean saveDiscountEsitmateData(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		log.info("Client Ip: "+request.getHeader("x-my-custom-header"));
		log.info("PoizonController.saveDiscountEsitmateData values= "+values);
		System.out.println("values>>"+values);
		ModelAndView mav = new ModelAndView();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		MessageBean messageBean = new MessageBean();
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			JSONArray discountDetails = jsonObject.getJSONArray("discountDetails");
			result=poizonService.saveDiscountEsitmateData(discountDetails);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
    }
	/*This method is used for to get distinct invoice date as per sheet*/
	@RequestMapping(value = "/getTotalInvoiceDateAsPerSheet", method = RequestMethod.GET)
    public @ResponseBody List<InvoiceDateBean> getTotalInvoiceDateAsPerSheet(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonController.getTotalInvoiceDate");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<InvoiceDateBean> nvoiceDateList = null;
		nvoiceDateList = poizonService.getTotalInvoiceDateAsPerSheet();
		return nvoiceDateList;
    }
	/*Old method Not used any where*/
	public boolean getNoOfDays(String firstDate, String secondDate){
		System.out.println("getNoOfDiffDays");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date d1 = null;
		Date d2 = null;
		long diffDays = 0;
		try {
			d1 = format.parse(firstDate);
			d2 = format.parse(secondDate);
			long diff = d2.getTime() - d1.getTime();
			diffDays = diff / (24 * 60 * 60 * 1000);
			
		} catch (ParseException e) {
			e.printStackTrace();
			return false;
		}
		if(diffDays <= 30)
		return true;
		else
		return false;
	}
	/**
	 * This method is used to get Indent estimate for a single product with all details
	 * */
	@RequestMapping(value = "/getDiscountEsitmateDataWithBrandNo", method = RequestMethod.GET)
	public @ResponseBody DiscountEstimationBean getDiscountEsitmateDataWithBrandNo(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("brandNoVal") int brandNoVal,@RequestParam("inputdate") String inputdate,@RequestParam("noOfDays") int noOfDays) throws ParseException {
		log.info("PoizonController.getDiscountEsitmateData");
		response.setContentType("application/json");
			response.setHeader("Access-Control-Allow-Origin","*");
			response.setHeader("value", "valid");
			DiscountEstimationBean discountEstimateList = null;
		discountEstimateList = poizonService.getDiscountEsitmateDataWithBrandNo(brandNoVal,CommonUtil.chageDateFormat(inputdate),noOfDays);
		return discountEstimateList;
	}
	/**
	 * This method is used to Save Indent estimate for some time of period
	 * */
	@RequestMapping(value = "/saveTempIndent", method = RequestMethod.POST)
    public @ResponseBody MessageBean saveTempIndent(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonController.saveTempIndent>> "+values);
		ModelAndView mav = new ModelAndView();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		MessageBean messageBean = new MessageBean();
		ObjectMapper mapper = new ObjectMapper();
		boolean flag=true;
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			JSONArray indentDetailsJson = jsonObject.getJSONArray("indentDetails");
			System.out.println(indentDetailsJson);
			result=poizonService.saveTempIndent(indentDetailsJson);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
    }
	/*This method is used to get Date and no. of days from 'tempIndenttab'*/
	@RequestMapping(value = "/getDateAndDays", method = RequestMethod.GET)
    public @ResponseBody MessageBean getDateAndDays(HttpServletRequest request, HttpServletResponse response) {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		MessageBean messageBean = null;
 		messageBean = poizonService.getDateAndDays();
		return messageBean;
    }
	
    public void downloadIndentProduct(String date) throws ParseException {
 		DiscountEstimationWithDate discountEstimateList = null;
		discountEstimateList = poizonService.downloadIndentProduct(1,date);
		if(discountEstimateList.getDiscountEstimationBean().size() > 0)
			prepareIndentFile(discountEstimateList);
    }
    /**
     * This method is used to generate indent estimate report and send mail.
     * Exceute every day after updating the cash card amount.
     * It won't execute for zero sale 
     * */
	private void prepareIndentFile(DiscountEstimationWithDate discountEstimateList) {
		String strDate = null;
		try {
			DateFormat df = new SimpleDateFormat("dd-MMMM-yyyy"); 
			Date date1=new SimpleDateFormat("yyyy-MM-dd").parse(discountEstimateList.getDate());
			 strDate = df.format(date1);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		List<DiscountEstimationBean> listData = discountEstimateList.getDiscountEstimationBean();
		
		String mailMessage="<html><body>"+
				"<p>Today's indent for 1 day.</p>"+
				"<table style='border-collapse: collapse;border: 1px solid black;text-align: center;'><tr><th style='border: 1px solid black;padding: 5px;'> Name </th><th style='border: 1px solid black;padding: 5px;'> Brand No. </th><th style='border: 1px solid black;padding: 5px;'> Indent Case </th><th style='border: 1px solid black;padding: 5px;'> 2L </th><th style='border: 1px solid black;padding: 5px;'> 1L </th><th style='border: 1px solid black;padding: 5px;'> Q </th><th style='border: 1px solid black;padding: 5px;'> P </th><th style='border: 1px solid black;padding: 5px;'> N </th><th style='border: 1px solid black;padding: 5px;'> D </th><th style='border: 1px solid black;padding: 5px;'> LB </th><th style='border: 1px solid black;padding: 5px;'> SB </th><th style='border: 1px solid black;padding: 5px;'> TIN </th></tr>";
		for(DiscountEstimationBean bean : listData){
			if (bean.getNeedCase() > 0 && bean.getProductType().equals("LIQUOR")) {
				mailMessage += "<tr><td style='border: 1px solid black;'>"+bean.getBrandName()+"</td><td style='border: 1px solid black;'>"+bean.getBrandNo()+"</td><td style='border: 1px solid black;'>"+Math.round(bean.getNeedCase())+"</td>"
						+ "<td style='border: 1px solid black;'>"+Math.round(bean.getL2NeedCase())+"</td><td style='border: 1px solid black;'>"+Math.round(bean.getL1NeedCase())+"</td>"
						+ "<td style='border: 1px solid black;'>"+Math.round(bean.getqNeedCase())+"</td><td style='border: 1px solid black;'>"+Math.round(bean.getpNeedCase())+"</td>"
						+ "<td style='border: 1px solid black;'>"+Math.round(bean.getnNeedCase())+"</td><td style='border: 1px solid black;'>"+Math.round(bean.getdNeedCase())+"</td>"
						+ "<td style='border: 1px solid black;'>"+Math.round(bean.getLbNeedCase())+"</td><td style='border: 1px solid black;'>"+Math.round(bean.getSbNeedCase())+"</td>"
					    + "<td style='border: 1px solid black;'>"+Math.round(bean.getTinNeedCase())+"</td></tr>";
			}
		}
		System.out.println("Shiva Sai:::");
		for(DiscountEstimationBean bean : listData){
			if (bean.getNeedCase() > 0 && bean.getProductType().equals("BEER")) {
				mailMessage += "<tr><td style='border: 1px solid black;'>"+bean.getBrandName()+"</td><td style='border: 1px solid black;'>"+bean.getBrandNo()+"</td><td style='border: 1px solid black;'>"+Math.round(bean.getNeedCase())+"</td>"
						+ "<td style='border: 1px solid black;'>"+Math.round(bean.getL2NeedCase())+"</td><td style='border: 1px solid black;'>"+Math.round(bean.getL1NeedCase())+"</td>"
						+ "<td style='border: 1px solid black;'>"+Math.round(bean.getqNeedCase())+"</td><td style='border: 1px solid black;'>"+Math.round(bean.getpNeedCase())+"</td>"
						+ "<td style='border: 1px solid black;'>"+Math.round(bean.getnNeedCase())+"</td><td style='border: 1px solid black;'>"+Math.round(bean.getdNeedCase())+"</td>"
						+ "<td style='border: 1px solid black;'>"+Math.round(bean.getLbNeedCase())+"</td><td style='border: 1px solid black;'>"+Math.round(bean.getSbNeedCase())+"</td>"
					    + "<td style='border: 1px solid black;'>"+Math.round(bean.getTinNeedCase())+"</td></tr>";
			}
		}
				mailMessage += "</table></body></html>";
				//new SendEmail(mailFromUsername, "", "jeripotula@gmail.com", "Indent for "+strDate,mailMessage, poizinMail).start();
				//new SendEmail(mailFromUsername, "", "khageshics@gmail.com", "Indent for "+strDate,mailMessage, poizinMail).start();
		
	}
	/**
	 * this method is used to get Scheme data and its spliting to each category
	 * */
	@RequestMapping(value = "/getSchemeData", method = RequestMethod.GET)
	public @ResponseBody List<SchemeEstimateBean> getSchemeData(HttpServletRequest request, HttpServletResponse response,@RequestParam("date") String date){
		log.info("PoizinController : getSchemeData()");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<SchemeEstimateBean> discountEstimation = poizonService.getSchemeData(CommonUtil.chageDateFormat(date));
		return discountEstimation;
	}
	
	/**
	 * This method is used to single product details by passing brandnopackqty
	 * */
	@RequestMapping(value = "/getSingleProductDetails", method = RequestMethod.GET)
	public @ResponseBody double getSingleProductDetails(HttpServletRequest request, HttpServletResponse response,@RequestParam("date") String date,
			@RequestParam("brandNoPackQty") int brandNoPackQty){
		log.info("PoizinController : getSingleProductDetails()");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		double result = poizonService.getSingleProductDetails(brandNoPackQty,date);
		return result;
	}
	
	/**
	 * This method is used to post credit data
	 * @throws JSONException 
	 * */
	@RequestMapping(value = "/postCreditData", method = RequestMethod.POST, produces="text/plain")
    public @ResponseBody ResponseEntity<String> postCreditData(@RequestBody String values,HttpServletRequest request, HttpServletResponse response) throws JSONException{
		log.info("PoizonController.postCreditData>> "+values);
		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		JSONObject jsonObj = new JSONObject(values);
		//JSONArray creditJson = jsonObj.getJSONArray("creditData");
 		String result  = poizonService.postCreditData(jsonObj);
		return new ResponseEntity<String>(result, HttpStatus.ACCEPTED);
	}
	
	/***
	 * This method is used to get exsisting credit data
	 * */
	@RequestMapping(value = "/getExistingCredit", method = RequestMethod.GET)
	public @ResponseBody List<CreditMasterBean> getExistingCredit(HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonController.getExistingCredit()");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<CreditMasterBean> beanList = poizonService.getExistingCredit();
		
		return beanList;
	}
	
	/**
	 * This method is used to updateExistingCredit
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 * @throws JSONException 
	 * */
	@RequestMapping(value = "/clearOrUnclear", method =RequestMethod.POST, produces="text/plain")
	public @ResponseBody String updateExistingCredit(@RequestBody String values,HttpServletRequest request, HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException, JSONException{
		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		ObjectMapper mapper = new ObjectMapper();
 		JSONObject jsonObj = new JSONObject(values);
 		CreditPaymentBean creditPaymentBean = mapper.readValue(jsonObj.toString(), CreditPaymentBean.class);
 		String result = poizonService.updateExistingCredit(creditPaymentBean);
 		return result;
	}
	/**
	 * This method is used to get existing credits for a company 
	 * */
	@RequestMapping(value = "/getcompanyWiseExistingCredit", method = RequestMethod.GET)
	public @ResponseBody List<CreditMasterBean> getcompanyWiseExistingCredit(HttpServletRequest request, HttpServletResponse response, @RequestParam("date") String date,
			@RequestParam("company") int company){
		log.info("PoizonController.getExistingCredit()");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<CreditMasterBean> beanList = poizonService.getcompanyWiseExistingCredit(company, date);
		
		return beanList;
	}
	
	 /**
	 * Everymonth in the mid and end we need to get mail to poizin and durga sir 
	 * of items which are discounts zero but we have lifted items
	 * */
	 @Scheduled(cron="0 0 11 5 1/1 ?") // every month on 5th date at 11 Am
	public void reminder1(){
		getMidMailWithLiftedCaseToUpdateDiscount();
    }
	 @Scheduled(cron="0 0 11 15 1/1 ?") // every month on 15th date at 11 Am
	public void reminder2(){
		getMidMailWithLiftedCaseToUpdateDiscount();
    }
	 public void getMidMailWithLiftedCaseToUpdateDiscount(){
    	log.info("PoizonController.getMidMailWithLiftedCaseToUpdateDiscount()");
    	List<NoDiscountProductsBean> listData = poizonService.remindLiftedCaseToUpdateDiscount(DateTimeFormatter.ofPattern("yyyy-MM-01").format(LocalDateTime.now()));
    	String mailData = "<html><body>"
    			   + "<p> Month: "+DateTimeFormatter.ofPattern("MMM-yyyy").format(LocalDateTime.now())+"</p>"
    			          + "<table style='border-collapse: collapse;border: 1px solid black;text-align: center;'>"
    						+ "<tr>"
    						   + "<th style='border: 1px solid black;padding: 5px;'>Company</th>"
    						   + "<th style='border: 1px solid black;padding: 5px;'>Brand No.</th>"
    						   + "<th style='border: 1px solid black;padding: 5px;text-align: left;'>Brand Name</th>"
    						   + "<th style='border: 1px solid black;padding: 5px;'>Lifted Case</th></tr>";
    	for(NoDiscountProductsBean outerbean : listData){
    		int k = 0;
    		for(PendingLiftedCaseDiscountBean bean : outerbean.getListData()){
    	           mailData+= "<tr>";
    	        		          if(k == 0){
    	        		        	  mailData+= "<td rowspan='"+outerbean.getListData().size()+"' style='border: 1px solid black;'>"+bean.getCompanyName()+"</td>";
    	        		          }
    	        	   mailData+= "<td style='border: 1px solid black;'>"+bean.getBrandNo()+"</td>"
    						   + "<td style='border: 1px solid black;text-align: left;'>"+bean.getBrandName()+"</td>"
    						   + "<td style='border: 1px solid black;'>"+bean.getCases()+"</td>"
    						+ "</tr>";
    	        	   k++;
    	}	
    }		
    	mailData+= "</table>"
    			  + "<html><body>";
    	//new SendEmail(mailFromUsername, "", "jeripotula@gmail.com", "No discount products.",mailData, poizinMail).start();
    	//new SendEmail(mailFromUsername, "", "khageshics@gmail.com", "No discount products.",mailData, poizinMail).start();
    }
	 /**
		 * This method is used to get distinct sale and invoice date list from 'invoice',sale table.
		 * */
		@RequestMapping(value = "/getUnionSaleAndInvoiceDate", method = RequestMethod.GET)
	    public @ResponseBody List<InvoiceDateBean> getUnionSaleAndInvoiceDate(HttpServletRequest request, HttpServletResponse response) {
			log.info("PoizonController.getUnionSaleAndInvoiceDate");
			response.setContentType("application/json");
	 		response.setHeader("Access-Control-Allow-Origin","*");
	 		response.setHeader("value", "valid");
			List<InvoiceDateBean> listdate = null;
			listdate = poizonService.getUnionSaleAndInvoiceDate();
			return listdate;
	    }
}
