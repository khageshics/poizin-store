package com.zambient.poizon.controller;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zambient.poizon.bean.AddNewBrandBean;
import com.zambient.poizon.bean.BankCreditAmountBean;
import com.zambient.poizon.bean.CallDiscountBean;
import com.zambient.poizon.bean.CommentForSaleBean;
import com.zambient.poizon.bean.DiscountAndMonthBean;
import com.zambient.poizon.bean.DiscountAsPerCompanyBeen;
import com.zambient.poizon.bean.DiscountEstimationBean;
import com.zambient.poizon.bean.DiscountEstimationDetails;
import com.zambient.poizon.bean.DiscountTransactionBean;
import com.zambient.poizon.bean.EditProductBean;
import com.zambient.poizon.bean.ExpenseCategoryAndDate;
import com.zambient.poizon.bean.ExpenseCategoryBean;
import com.zambient.poizon.bean.InvestmentBean;
import com.zambient.poizon.bean.InvoiceDateBean;
import com.zambient.poizon.bean.MessageBean;
import com.zambient.poizon.bean.MonthlyExpensesBean;
import com.zambient.poizon.bean.MrpRoundOffBean;
import com.zambient.poizon.bean.PieChartBean;
import com.zambient.poizon.bean.ProductListBean;
import com.zambient.poizon.bean.ProductOrderBy;
import com.zambient.poizon.bean.ProductPerformanceBean;
import com.zambient.poizon.bean.ProductPerformanceWithComaparisonBean;
import com.zambient.poizon.bean.ProductWithStatement;
import com.zambient.poizon.bean.ProfitAndLossBean;
import com.zambient.poizon.bean.SaleWithDiscountBean;
import com.zambient.poizon.bean.StockLiftCompanyWithChecksBean;
import com.zambient.poizon.bean.StockLiftWithDiscountEstimate;
import com.zambient.poizon.bean.TillDateBalanceSheetChart;
import com.zambient.poizon.bean.TillMonthStockLiftWithDiscount;
import com.zambient.poizon.bean.UploadAndViewWithYearBean;
import com.zambient.poizon.bean.VendorAdjustmentBean;
import com.zambient.poizon.bean.addNewIndentBean;
import com.zambient.poizon.bean.category;
import com.zambient.poizon.bean.companyBean;
import com.zambient.poizon.services.PoizonSecondService;
import com.zambient.poizon.utill.CommonUtil;
import com.zambient.poizon.utill.UserSession;

@Controller
public class PoizonSecondController {
	final static Logger log = Logger.getLogger(PoizonSecondController.class);
	
	@Value("${images.root.path}")
	private String imageRootPath;
	@Value("${images.retrival.path}")
	private String imagesRetrivalPath;
	@Value("${poizin.indent.file.path}")
	private String poizinIndentFile;
	@Value("${poizin.indent.file.retrival.path}")
	private String poizinIndentFileRetrival;
	/*@Value("${images.zip.path}")
	private String imagesZipPath;*/
	@Autowired 
	private UserSession userSession;
	@Autowired
	PoizonSecondService poizonSecondService;
	
	/*Old method not used any where*/
	@RequestMapping(value = "/getTotalDiscountDate", method = RequestMethod.POST)
    public @ResponseBody List<InvoiceDateBean> getTotalDiscountDate(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonSecondController.getTotalDiscountDate");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<InvoiceDateBean> nvoiceDateList = null;
		nvoiceDateList = poizonSecondService.getTotalDiscountDate();
		return nvoiceDateList;
    }
	/*Old method not used any where*/
	@RequestMapping(value = "/getbuildDiscountIntentData", method = RequestMethod.GET)
    public @ResponseBody List<DiscountEstimationBean> getbuildDiscountIntentData(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		log.info("PoizonSecondController.getbuildDiscountIntentData");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		String sdate=request.getParameter("startDate");
		List<DiscountEstimationBean> retrieveDiscountIntent = null;
		retrieveDiscountIntent = poizonSecondService.getbuildDiscountIntentData(sdate);
		return retrieveDiscountIntent;
    }
	/**
	 * This method is used to get Lifted stock and Discount amount for Enter discount page
	 * */
	@RequestMapping(value = "/getStockLiftingWithDiscount", method = RequestMethod.GET)
    public @ResponseBody List<StockLiftWithDiscountEstimate> getStockLiftingWithDiscount(HttpServletRequest request, HttpServletResponse response,@RequestParam("date") String date) throws ParseException {
		log.info("PoizonSecondController.getStockLiftingWithDiscount");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		DateFormat df = new SimpleDateFormat("dd/MMMM/yyyy"); 
 		Date startDate;
 		 startDate = df.parse(date);
 		df = new SimpleDateFormat("yyyy-MM-dd");  
 	   String strDate = df.format(startDate);  
		List<StockLiftWithDiscountEstimate> stockList = null;
			stockList = poizonSecondService.getStockLiftingWithDiscount(strDate);
			System.out.println();
		log.info("stockList>> "+stockList);
		return stockList;
    }
	/**
	 * This method is used to post or save discount per case, target and adjustment (Enter discount)
	 * */
	@RequestMapping(value = "/saveStockLiftWithDiscountData", method = RequestMethod.POST)
    public @ResponseBody MessageBean saveStockLiftWithDiscountData(@RequestBody String values,HttpServletRequest request, HttpServletResponse response,@RequestParam("date") String date) throws ParseException{
		log.info("Client Ip: "+request.getHeader("x-my-custom-header"));
		log.info("PoizonSecondController.saveStockLiftWithDiscountData values= "+values);
		//System.out.println("values>>"+values);
		ModelAndView mav = new ModelAndView();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		DateFormat df = new SimpleDateFormat("dd/MMMM/yyyy"); 
 		Date startDate;
 		 startDate = df.parse(date);
 		df = new SimpleDateFormat("yyyy-MM-dd");  
 	   String strDate = df.format(startDate);  
 	    System.out.println("Start Date : "+strDate); 
 	 
		MessageBean messageBean = new MessageBean();
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			JSONArray discountDetails = jsonObject.getJSONArray("discountDetails");
			//System.out.println(discountDetails);
			//System.out.println(arreasDetails);
			result=poizonSecondService.saveStockLiftWithDiscountData(discountDetails,strDate);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
    }
	/**
	 * This method is used to post or save band credited amount in db
	 * */
	@RequestMapping(value = "/saveBankCreditAmount", method = RequestMethod.POST)
	public @ResponseBody MessageBean saveBankCreditAmount(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		log.info("Client Ip: "+request.getHeader("x-my-custom-header"));
		log.info("PoizonSecondController.saveBankCreditAmount values= "+values);
		MessageBean messageBean = new MessageBean();
		ModelAndView mav = new ModelAndView();
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		BankCreditAmountBean bankCreditAmountBean = new BankCreditAmountBean();
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			response.setHeader("value", "valid");
			JSONObject appUserserJson = jsonObject.getJSONObject("bankCreditDetails");
			bankCreditAmountBean = mapper.readValue(appUserserJson.toString(), BankCreditAmountBean.class);
			if(bankCreditAmountBean.getBankCredit() !=null && bankCreditAmountBean.getBankdate() != null)
	    	 result = poizonSecondService.saveBankCreditAmount(bankCreditAmountBean);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
		
	}
	/**
	 * this method is used to get discount amount and details for each company
	 * */
	@RequestMapping(value = "/getCompanyWiseDiscountAmount", method = RequestMethod.GET)
    public @ResponseBody List<DiscountAsPerCompanyBeen> getCompanyWiseDiscountAmount(HttpServletRequest request, HttpServletResponse response,@RequestParam("date") String date) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		DateFormat df = new SimpleDateFormat("dd/MMMM/yyyy"); 
 		Date startDate;
 		 startDate = df.parse(date);
 		df = new SimpleDateFormat("yyyy-MM-dd");  
 	   String strDate = df.format(startDate);  
 	 		
		List<DiscountAsPerCompanyBeen> discountAsPerCompanyBeen = null;
		if(strDate!=null)
			discountAsPerCompanyBeen = poizonSecondService.getCompanyWiseDiscountAmount(strDate);
		return discountAsPerCompanyBeen;
    }
	/**
	 * this method is used to maintan product order 
	 * */
	@RequestMapping(value="/addOrderForAProduct",method=RequestMethod.POST)
	public String addOrderForAProduct(@ModelAttribute("productOrderBy") ProductOrderBy productOrderBy,HttpServletRequest request,HttpServletResponse response,RedirectAttributes redirectAttributes){
		log.info("PoizonSecondController.ProductOrderBy Bean>> "+productOrderBy);
		String result=null;
		if(productOrderBy != null)
	    result= poizonSecondService.addOrderForAProduct(productOrderBy);
		redirectAttributes.addFlashAttribute("message", result);
		return "redirect:/productOrderBy";
	}
	/**
	 * this method is used to write a comment in related to daily sale
	 * */
	@RequestMapping(value="/addCommentForSale",method=RequestMethod.POST)
	public String addCommentForSale(HttpServletRequest request,HttpServletResponse response,RedirectAttributes redirectAttributes){
		log.info("PoizonController.addCommentForSale");
		CommentForSaleBean commentForSaleBean = new CommentForSaleBean();
		String date=request.getParameter("date");
		String comment=request.getParameter("comment");
		commentForSaleBean.setCommentDate(date);
		commentForSaleBean.setSaleComment(comment);
		String result=null;
		if(commentForSaleBean != null)
	    result= poizonSecondService.addCommentForSale(commentForSaleBean);
		redirectAttributes.addFlashAttribute("message", result);
		return "redirect:/sale";
	}
	/**
	 * this method is used to get comment in related to daily sale
	 * */
	@RequestMapping(value = "/getSaleCommentData", method = RequestMethod.GET)
    public @ResponseBody CommentForSaleBean getSaleCommentData(HttpServletRequest request, HttpServletResponse response,@RequestParam("date") String date) throws ParseException {
		log.info("PoizonSecondController.getSaleCommentData");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		CommentForSaleBean commentForSaleBean = null;
 		if(date!=null && date!="")
 		commentForSaleBean = poizonSecondService.getSaleCommentData(date);
		return commentForSaleBean;
    }
	/**
	 * this method is used to save invoice mrp rounding up
	 * */
	@RequestMapping(value="/saveMrpRoundOff",method=RequestMethod.POST)
	public String saveMrpRoundOff(HttpServletRequest request,HttpServletResponse response,RedirectAttributes redirectAttributes){
		log.info("PoizonSecondController.saveMrpRoundOff");
		MrpRoundOffBean mrpRoundOffBean = new MrpRoundOffBean();
		mrpRoundOffBean.setDate(CommonUtil.chageDateFormat(request.getParameter("date")));
		mrpRoundOffBean.setDateAsPerCopy(CommonUtil.chageDateFormat(request.getParameter("dateAsPerCopy")));
		mrpRoundOffBean.setMrpRoundOff(Double.parseDouble(request.getParameter("mrproundoff")));
		mrpRoundOffBean.setTurnOverTax(Double.parseDouble(request.getParameter("turnoverTax")));
		mrpRoundOffBean.setTcsVal(Double.parseDouble(request.getParameter("tcsVal")));
		mrpRoundOffBean.setRetailerCreditVal(Double.parseDouble(request.getParameter("retailerCreditVal")));
		String result=null;
		if(mrpRoundOffBean != null)
	    result= poizonSecondService.saveMrpRoundOff(mrpRoundOffBean);
		redirectAttributes.addFlashAttribute("message", result);
		return "redirect:/sale";
	}
	/**
	 * this method is used to get all product to maintan in order
	 * */
	@RequestMapping(value = "/getBrandDetailsForProductOrder", method = RequestMethod.GET)
    public @ResponseBody List<AddNewBrandBean> getBrandDetails(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonSecondController.getBrandDetails");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<AddNewBrandBean> addNewBrandList = null;
		addNewBrandList = poizonSecondService.getBrandDetailsForProductOrder();
		
		return addNewBrandList;
    }
	/**
	 * this method is used to get all distinct company from company_tab table
	 * */
	@RequestMapping(value = "/getCompanyList", method = RequestMethod.GET)
	public @ResponseBody List<companyBean> getCompanyList(HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonSecondController.getCompanyList");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<companyBean> company = null;
 		company = poizonSecondService.getCompanyList();
 		return company;
	}
	/**
	 * this method is used to get all distinct category from category_tab table
	 * */
	@RequestMapping(value = "/getCategoryList", method = RequestMethod.GET)
	public @ResponseBody List<companyBean> getCategoryList(HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonSecondController.getCompanyList");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<companyBean> category = null;
 		category = poizonSecondService.getCategoryList();
 		return category;
	}
	/**
	 * this method is used to save Monthly expenses
	 * */
	@RequestMapping(value = "/saveMonthlyExpensesData", method = RequestMethod.POST)
    public @ResponseBody MessageBean saveMonthlyExpensesData(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonSecController.saveMonthlyExpensesData>> "+values);
		log.info("Client Ip: "+request.getHeader("x-my-custom-header"));
		ModelAndView mav = new ModelAndView();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		MessageBean messageBean = new MessageBean();
		ObjectMapper mapper = new ObjectMapper();
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			JSONArray expensesJson = jsonObject.getJSONArray("monthlyExpensesDetails");
			System.out.println(expensesJson);
			if(expensesJson.length() !=0)
			result=poizonSecondService.saveMonthlyExpensesData(expensesJson);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
    }
	/**
	 * this method is used to save Monthly expenses
	 * */
	@RequestMapping(value = "/getMonthlyExpenses", method = RequestMethod.GET)
    public @ResponseBody List<MonthlyExpensesBean> getMonthlyExpenses(HttpServletRequest request, HttpServletResponse response,@RequestParam("expensesdate") String expensesdate) {
		log.info("PoizonSecondController.getMonthlyExpenses expensesdate date= "+expensesdate);
		System.out.println("expensesdate>> "+expensesdate);
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<MonthlyExpensesBean> monthlyExpensesBeanList = null;
 		monthlyExpensesBeanList = poizonSecondService.getMonthlyExpenses(expensesdate);
		return monthlyExpensesBeanList;
    }
	/**
	 * this method is used to delete Monthly expenses
	 * */
	@RequestMapping(value = "/deleteMonthlyExpenses", method = RequestMethod.GET)
    public @ResponseBody MessageBean deleteMonthlyExpenses(HttpServletRequest request, HttpServletResponse response,@RequestParam("expenseMonthlyId") int expenseMonthlyId,@RequestParam("ip") String ip) {
		log.info("PoizonSecondController.deleteMonthlyExpenses");
		log.info("Client Ip: "+ip);
		ModelAndView mav = new ModelAndView();
		MessageBean messageBean = new MessageBean();
		String result=null;
			result=poizonSecondService.deleteMonthlyExpenses(expenseMonthlyId);
			messageBean.setMessage(result);
			return messageBean;
    }
	/**
	 * this method is used to get Monthly expense category
	 * */
	@RequestMapping(value = "/getMonthlyExpenseCategory", method = RequestMethod.GET)
	public List<ExpenseCategoryBean> getMonthlyExpenseCategory(HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonSecondController.getMonthlyExpenseCategory ");
		List<ExpenseCategoryBean> expenseCategoryBeanList = null;
		expenseCategoryBeanList = poizonSecondService.getMonthlyExpenseCategory();
		return expenseCategoryBeanList;
	}
	/*Old method not used any where*/
	@RequestMapping(value = "/getProductPerformance", method = RequestMethod.GET)
    public @ResponseBody List<ProductPerformanceBean> getProductPerformance(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		log.info("PoizonController.getSaleReports");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		int brandNo=Integer.parseInt(request.getParameter("brandNo"));
 		List<ProductPerformanceBean> productPerformanceBean = null;
 		productPerformanceBean = poizonSecondService.getProductPerformance(brandNo);
		return productPerformanceBean;
    }
	/**
	 * This method is used to get distinct brand name and brand number
	 * */
	@RequestMapping(value = "/getDistinctBrandNameAndNo", method = RequestMethod.GET)
    public @ResponseBody List<AddNewBrandBean> getDistinctBrandNameAndNo(HttpServletRequest request, HttpServletResponse response) {
		log.info("PoizonSecondController.getDistinctBrandNameAndNo ");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<AddNewBrandBean> addNewBrandList = null;
 		addNewBrandList = poizonSecondService.getDistinctBrandNameAndNo();
		return addNewBrandList;
    }
	/**
	 * This method is used to get discount, received check and rentals to till selected months 
	 * */
	@RequestMapping(value = "/getTillMonthStockLiftWithDiscount", method = RequestMethod.GET)
    public @ResponseBody List<TillMonthStockLiftWithDiscount> getTillMonthStockLiftWithDiscount(HttpServletRequest request, HttpServletResponse response,@RequestParam("date") String date,@RequestParam("company") int company) {
		log.info("PoizonSecondController.getDistinctBrandNameAndNo ");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<TillMonthStockLiftWithDiscount> tillMonthStockLiftWithDiscount = null;
 		System.out.println(date);
 		tillMonthStockLiftWithDiscount = poizonSecondService.getTillMonthStockLiftWithDiscount(date,company);
		return tillMonthStockLiftWithDiscount;
    }
	/**
	 * This method is used to get latest credit amount
	 * */
	@RequestMapping(value = "/getLastDateCredit", method = RequestMethod.GET)
    public @ResponseBody double getLastDateCredit(HttpServletRequest request, HttpServletResponse response,@RequestParam("date") String date) {
		log.info("PoizonSecondController.getDistinctBrandNameAndNo ");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		System.out.println(date);
 		double val = poizonSecondService.getLastDateCredit(CommonUtil.chageDateFormat(date));
		return val;
    }

	/**
	 * This method is used to save or post check with details for company
	 * */
	@RequestMapping(value = { "/addNewTransaction" }, method = { RequestMethod.POST })
 	public @ResponseBody String addNewTransaction(Model model,HttpServletRequest request,HttpServletResponse response,RedirectAttributes redirectAttributes,
 			@RequestParam("company") String company,@RequestParam("months") String months,@RequestParam("bank") String bank,@RequestParam("TransactionType") String TransactionType,
 			@RequestParam("transactionAmt") double transactionAmt,@RequestParam("transactionDate") String transactionDate,/*@RequestParam("rental") double rental,*/
 			@RequestParam("adjCheck") double adjCheck,@RequestParam("checkNo") String checkNo,@RequestParam("fromBank") String fromBank,@RequestParam("comment") String comment,
 			@RequestParam(value = "ngalleryImage", required = false) MultipartFile ngalleryImage,@RequestParam("received") boolean received) {
		 log.info("Poizinsecondcontroller:: addNewTransaction");
		String result=null;
		DiscountTransactionBean discountTransactionBean= new DiscountTransactionBean();
		discountTransactionBean.setAdjCheck(adjCheck);
		discountTransactionBean.setCompany(company);
		discountTransactionBean.setMonths(months);
		/*discountTransactionBean.setRental(rental);*/
		discountTransactionBean.setTransactionAmt(transactionAmt);
		discountTransactionBean.setTransactionType(TransactionType);
		discountTransactionBean.setBank(bank);
		discountTransactionBean.setFromBank(fromBank);
		discountTransactionBean.setCheckNo(checkNo);
		discountTransactionBean.setComment(comment);
		discountTransactionBean.setReceived(received);
		if(received){
			discountTransactionBean.setCheckBox(0);
		}
		else{
			discountTransactionBean.setCheckBox(1);
			discountTransactionBean.setTransactionDate(transactionDate);
		}
		System.out.println(discountTransactionBean);
		if(ngalleryImage !=null && discountTransactionBean.toString().length() >0){
			try {
				validateImage(ngalleryImage);
			} catch (RuntimeException re) {
				log.error("Poizincontroller :: addNewTransaction "+re);
				// bindingResult.reject(re.getMessage());
				return "Something went wrong.....";
			}
			try {
				String fileName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
				saveImage(fileName, ngalleryImage, request,months,company);
				String imgType[] = ngalleryImage.getContentType().split("/");
				String outerFolderName = months;
				String innerFolderName = company;
				discountTransactionBean.setImageName( "/" + outerFolderName + "/" + innerFolderName + "/" + fileName + "." + imgType[1]);
				discountTransactionBean.setNgalleryImage(imagesRetrivalPath + outerFolderName + "/" + innerFolderName + "/" + fileName + "." + imgType[1]);
				result = poizonSecondService.addNewTransaction(discountTransactionBean);
				//redirectAttributes.addFlashAttribute("message", result);
			} catch (IOException e) {
				log.error("Poizincontroller :: addNewTransaction "+e);
				//log.info(e.getMessage(), e);
				return "Something went wrong.....";
			}
		}
		return result;
 	}
	/**
	 * This method is used to save check image in drive or folder
	 * */
	private void saveImage(String filename, MultipartFile image, HttpServletRequest request,String months,String company)
			throws RuntimeException, IOException {
		try {
			String imgType[] = image.getContentType().split("/");
			String innerFolderName = company;
			String outerFolderName = months;
			File file = new File(
					imageRootPath + File.separator + outerFolderName + File.separator + innerFolderName + File.separator + filename + "." + imgType[1]);// +"."+imgType[1]
			FileUtils.writeByteArrayToFile(file, image.getBytes());
			log.info("Go to the location:  " + file.toString()
					+ " on your computer and verify that the image has been stored.");
		} catch (IOException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void validateImage(MultipartFile image) {
		if (!(image.getContentType().equalsIgnoreCase("image/jpeg")
				|| image.getContentType().equalsIgnoreCase("image/jpg")
				|| image.getContentType().equalsIgnoreCase("image/png")))
				{
			throw new RuntimeException("Only JPG images are accepted");
		}
	}
	/**
	 * This method is used to get existing check details for selected company
	 * */
	@RequestMapping(value = "/getDiscountTransactionDetails", method = RequestMethod.GET)
    public @ResponseBody List<DiscountTransactionBean> getDiscountTransactionDetails(HttpServletRequest request,
    		HttpServletResponse response,@RequestParam("date") String date,@RequestParam("company") int company) throws ParseException {
		System.out.println("PoizonSecondController.productComparisionCaegoryWise");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<DiscountTransactionBean> discountTransactionBeanList = null;
 		discountTransactionBeanList = poizonSecondService.getDiscountTransactionDetails(date,company);
		return discountTransactionBeanList;
    }
	/**
	 * This method is used to delete check with details
	 * */
	@RequestMapping(value = "/deleteDiscountTransactionDetails", method = RequestMethod.GET)
    public @ResponseBody MessageBean deleteDiscountTransactionDetails(HttpServletRequest request, HttpServletResponse response,
    		@RequestParam("id") int id,@RequestParam("companyId") int companyId,@RequestParam("month") String month,@RequestParam("image") String image) {
		log.info("PoizonSecondController.deleteDiscountTransactionDetails");
		ModelAndView mav = new ModelAndView();
		MessageBean messageBean = new MessageBean();
		String result=null;
			result=poizonSecondService.deleteDiscountTransactionDetails(id);
			messageBean.setMessage(result);
			CommonUtil.deleteImage(month,image,Integer.toString(companyId),imageRootPath);
			return messageBean;
    }
	/**
	 * This method is used to download checks with detail in pdf format
	 * */
	@RequestMapping(value = "/downloadTransactionDetails", method = RequestMethod.GET,produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody void downloadTransactionDetails(HttpServletRequest request, HttpServletResponse response) throws Exception {
		//CommonUtil.zipFolder(imageRootPath,imagesZipPath);	      
		//CommonUtil.downloadZipFile(request,response,imagesZipPath);
    }
	/**
	 * This method is used to get sale with discount for each product quantity wise
	 * */
	@RequestMapping(value = "/getSaleWithDiscount", method = RequestMethod.GET)
    public @ResponseBody List<SaleWithDiscountBean> getSaleWithDiscount(HttpServletRequest request, HttpServletResponse response,@RequestParam("startdate") String startdate,@RequestParam("enddate") String enddate) throws ParseException {
		log.info("PoizonSecondController.getDistinctBrandNameAndNo ");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<SaleWithDiscountBean> saleBeanList = null;
 		saleBeanList = poizonSecondService.getSaleWithDiscount(CommonUtil.chageDateFormat(startdate),CommonUtil.chageDateFormat(enddate));
		return saleBeanList;
    }
	/**
	 * This method is used to get stock lift and discount amount
	 * */
	@RequestMapping(value = "/getStockLiftWithDiscountTransactionDetails", method = RequestMethod.GET)
    public @ResponseBody List<StockLiftCompanyWithChecksBean> getStockLiftWithDiscountTransactionDetails(HttpServletRequest request, HttpServletResponse response,@RequestParam("date") String date) throws ParseException {
		log.info("PoizonSecondController.getStockLiftWithDiscountTransactionDetails ");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		DateFormat df = new SimpleDateFormat("dd/MMMM/yyyy"); 
 		Date startDate;
 		 startDate = df.parse(date);
 		df = new SimpleDateFormat("yyyy-MM-dd");  
 	   String strDate = df.format(startDate);  
 		List<StockLiftCompanyWithChecksBean> DiscountTransactionBeanList = null;
 		DiscountTransactionBeanList = poizonSecondService.getStockLiftWithDiscountTransactionDetails(strDate);
 		System.out.println(DiscountTransactionBeanList);
		return DiscountTransactionBeanList;
    }
	/**
	 * This method is used to download check image
	 * */
	@RequestMapping(value = "/downloadChecks", method = RequestMethod.GET,produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody void downloadChecks(HttpServletRequest request, HttpServletResponse response,@RequestParam("imagePath") String imagePath) throws Exception {
		//System.out.println("khagesh>> "+imagePath);
		CommonUtil.downloadZipFile(request,response,imageRootPath+imagePath);
    }
	/**
	 * This method is used to download uploaded invoice
	 * */
	@RequestMapping(value = "/downloadInvioce", method = RequestMethod.GET,produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody void downloadInvioce(HttpServletRequest request, HttpServletResponse response,@RequestParam("filePath") String filePath) throws Exception {
		CommonUtil.downloadZipFile(request,response,poizinIndentFile+filePath);
    }
	/**
	 * This method is used to get discount to till selected month for a company
	 * */
	@RequestMapping(value = "/getDiscountCompanyWiseForAllMonth", method = RequestMethod.GET)
    public @ResponseBody List<DiscountAndMonthBean> getDiscountCompanyWiseForAllMonth(HttpServletRequest request,
    		HttpServletResponse response,@RequestParam("date") String date,@RequestParam("company") int company) throws ParseException {
		System.out.println("PoizonSecondController.getDiscountCompanyWiseForAllMonth");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<DiscountAndMonthBean> discountTransactionBeanList = null;
 		discountTransactionBeanList = poizonSecondService.getDiscountCompanyWiseForAllMonth(date,company);
 		List<DiscountAndMonthBean> secBeanList = CommonUtil.getListBasedOnMatch(discountTransactionBeanList);
 		System.out.println(secBeanList);
		return secBeanList;
    }
/**
 * This method is used to get discount for all months with received checks based on selected company
 * */
	@RequestMapping(value = "/getDiscountCompanyWiseForAllMonthWithReceived", method = RequestMethod.GET)
    public @ResponseBody List<DiscountAsPerCompanyBeen> getDiscountCompanyWiseForAllMonthWithReceived(HttpServletRequest request,
    		HttpServletResponse response,@RequestParam("company") int company) throws ParseException {
		System.out.println("PoizonSecondController.getDiscountCompanyWiseForAllMonthWithReceived");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<DiscountAsPerCompanyBeen> discountAsPerCompanyBeenList = null;
 		discountAsPerCompanyBeenList = poizonSecondService.getDiscountCompanyWiseForAllMonthWithReceived(company);
		return discountAsPerCompanyBeenList;
    }
	/**
	 * This method is used to get rental amount for all months based on selected company and till date
	 * */
	@RequestMapping(value = "/getUpToMonthRental", method = RequestMethod.GET)
    public @ResponseBody double getUpToMonthRental(HttpServletRequest request,
    		HttpServletResponse response,@RequestParam("date") String date,@RequestParam("company") int company) throws ParseException {
		System.out.println("PoizonSecondController.getUpToMonthRental");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		double rental = poizonSecondService.getUpToMonthRental(date,company);
		return rental;
    }
	/**
	 * This method is used to save investment details
	 * */
	@RequestMapping(value="/addNewInvestment",method=RequestMethod.POST)
	public String addNewInvestment(@ModelAttribute("investmentBean") InvestmentBean investmentBean,HttpServletRequest request,HttpServletResponse response,RedirectAttributes redirectAttributes){
		log.info("PoizonSecondController.investmentBean Bean>> "+investmentBean);
		String result=null;
		if(investmentBean != null)
	    result= poizonSecondService.addNewInvestment(investmentBean);
		redirectAttributes.addFlashAttribute("message", result);
		return "redirect:/investment";
	}
	/**
	 * This method is used to get investment details
	 * */
	@RequestMapping(value = "/getInvestmentData", method = RequestMethod.GET)
    public @ResponseBody List<InvestmentBean> getInvestmentData(HttpServletRequest request, HttpServletResponse response,
    		@RequestParam("date") String date) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<InvestmentBean> investmentBean = null;
 		investmentBean = poizonSecondService.getInvestmentData(CommonUtil.chageDateFormat(date));
		return investmentBean;
    }
	@RequestMapping(value = "/getProductWithInvestment", method = RequestMethod.GET)
    public @ResponseBody ProductWithStatement getProductWithInvestment(HttpServletRequest request, HttpServletResponse response,
    		@RequestParam("brandNo") long brandNo) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		ProductWithStatement investmentBean = null;
 		investmentBean = poizonSecondService.getProductWithInvestment(brandNo);
		return investmentBean;
    }
	/**
	 * This method is used to get discount estimate details
	 * */
	@RequestMapping(value = "/getDiscountEsitmateDetails", method = RequestMethod.GET)
    public @ResponseBody List<DiscountEstimationDetails> getDiscountEsitmateDetails(HttpServletRequest request, HttpServletResponse response,
    		@RequestParam("date") String date) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<DiscountEstimationDetails> investmentBean = null;
 		investmentBean = poizonSecondService.getDiscountEsitmateDetails(date);
		return investmentBean;
    }
	/**
	 * This method is used to save discount estimate details
	 * */
	@RequestMapping(value = "/saveDiscountEsitmateDetails", method = RequestMethod.POST)
    public @ResponseBody MessageBean saveDiscountEsitmateDetails(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		log.info("Client Ip: "+request.getHeader("x-my-custom-header"));
		log.info("PoizonSecondController.saveDiscountEsitmateDetails>> "+values);
		ModelAndView mav = new ModelAndView();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		MessageBean messageBean = new MessageBean();
		ObjectMapper mapper = new ObjectMapper();
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			JSONArray discountDetails = jsonObject.getJSONArray("discountDetails");
			System.out.println(discountDetails);
			if(discountDetails.length() !=0)
			result=poizonSecondService.saveDiscountEsitmateDetails(discountDetails);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
    }
	/**
	 * This method is used to get and calculate profit or loss data for selecetd start and end date
	 * */
	@RequestMapping(value = "/getProfitAndLossData", method = RequestMethod.GET)
    public @ResponseBody ProfitAndLossBean getProfitAndLossData(HttpServletRequest request, HttpServletResponse response,
    		@RequestParam("firstDate") String firstDate, @RequestParam("lastDate") String lastDate) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		ProfitAndLossBean profitAndLossBean = null;
 		profitAndLossBean = poizonSecondService.getProfitAndLossData(CommonUtil.chageDateFormat(firstDate),CommonUtil.chageDateFormat(lastDate));
		return profitAndLossBean;
    }
	/**
	 * This method is used to get product details to a category products for selected dates
	 * */
	@RequestMapping(value = "/getProductComparisionWithPerformance", method = RequestMethod.GET)
    public @ResponseBody List<ProductPerformanceWithComaparisonBean> getProductComparisionWithPerformance(HttpServletRequest request,
    		HttpServletResponse response,@RequestParam("startDate") String startDate,@RequestParam("endDate") String endDate,@RequestParam("category") int category) throws ParseException {
		System.out.println("PoizonController.getProductComparisionBaseOnCategory");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		System.out.println("startDate> "+startDate+" endDate> "+endDate+" category> "+category);
 		List<ProductPerformanceWithComaparisonBean> comparisonList = null;
		//if(getNoOfDays(sdate,edate))
 		comparisonList = poizonSecondService.getProductComparisionWithPerformance(CommonUtil.chageDateFormat(startDate),CommonUtil.chageDateFormat(endDate),category);
		return comparisonList;
    }
	/**
	 * This method is used to get Enterprice for selected company
	 * */
	@RequestMapping(value = "/getCompanyWithEnterPrice", method = RequestMethod.GET)
    public @ResponseBody category getCompanyWithEnterPrice(HttpServletRequest request,
    		HttpServletResponse response,@RequestParam("company") int company) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		category categoryBean = null;
 		categoryBean = poizonSecondService.getCompanyWithEnterPrice(company);
		return categoryBean;
    }
	/*Old method not used any where*/
	@RequestMapping(value = "/getExpenseDataByCategory", method = RequestMethod.GET)
    public @ResponseBody List<ExpenseCategoryAndDate> getExpenseDataByCategory(HttpServletRequest request,
    		HttpServletResponse response,@RequestParam("expenseId") int[] expenseId,@RequestParam("startDate") String startDate,@RequestParam("endDate") String endDate) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<ExpenseCategoryAndDate> list = null;
 		list = poizonSecondService.getExpenseDataByCategory(startDate,endDate,expenseId);
		return list;
    }
	/**
	 * This method is used to get product list
	 * */
	@RequestMapping(value = "/getProductList", method = RequestMethod.GET)
    public @ResponseBody List<ProductListBean> getProductList(HttpServletRequest request,
    		HttpServletResponse response) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<ProductListBean> list = null;
 		list = poizonSecondService.getProductList();
		return list;
    }
	/**
	 * This method is used to make product active or inactive
	 * */
	@RequestMapping(value = "/setActiveOrInActive", method = RequestMethod.GET)
    public @ResponseBody MessageBean setActiveOrInActive(HttpServletRequest request, HttpServletResponse response,
    		@RequestParam("brandNoPackQty") double brandNoPackQty,@RequestParam("activeVal") int activeVal) {
		log.info("PoizonSecondController.deleteDiscountTransactionDetails");
		ModelAndView mav = new ModelAndView();
		MessageBean messageBean = new MessageBean();
		String result=null;
			result=poizonSecondService.setActiveOrInActive(brandNoPackQty,activeVal);
			messageBean.setMessage(result);
			return messageBean;
    }
	@RequestMapping(value = "/getNewDiscountEsitmateDetails", method = RequestMethod.GET)
    public @ResponseBody List<DiscountEstimationDetails> getNewDiscountEsitmateDetails(HttpServletRequest request, HttpServletResponse response,
    		@RequestParam("date") String date) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<DiscountEstimationDetails> investmentBean = null;
 		investmentBean = poizonSecondService.getNewDiscountEsitmateDetails(date);
		return investmentBean;
    }
	@RequestMapping(value = "/saveStockLiftWithDiscountDataFromDiscountEstimate", method = RequestMethod.POST)
    public @ResponseBody MessageBean saveStockLiftWithDiscountDataFromDiscountEstimate(@RequestBody String values,HttpServletRequest request, HttpServletResponse response,@RequestParam("date") String date) throws ParseException{
		log.info("Client Ip: "+request.getHeader("x-my-custom-header"));
		//System.out.println("values>>"+values);
		ModelAndView mav = new ModelAndView();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		MessageBean messageBean = new MessageBean();
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			JSONArray discountDetails = jsonObject.getJSONArray("discountDetails");
			result=poizonSecondService.saveStockLiftWithDiscountData(discountDetails,date);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
    }
	/**
	 * This method is used to get product details for update product
	 * */
	@RequestMapping(value = "/editProductDetails", method = RequestMethod.GET)
    public @ResponseBody List<EditProductBean> editProductDetails(HttpServletRequest request, HttpServletResponse response,
    		@RequestParam("brandNo") int brandNo) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<EditProductBean> editProductBean = null;
 		editProductBean = poizonSecondService.editProductDetails(brandNo);
		return editProductBean;
    }
	/**
	 * This method is used to update products
	 * */
	@RequestMapping(value = "/updateSingleProductDetails", method = RequestMethod.POST)
    public @ResponseBody MessageBean updateSingleProductDetails(@RequestBody String values,HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonController.saveInvoiceJsonData>> "+values);
		ModelAndView mav = new ModelAndView();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		MessageBean messageBean = new MessageBean();
		ObjectMapper mapper = new ObjectMapper();
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			JSONArray jsonData = jsonObject.getJSONArray("editProductJson");
			System.out.println(jsonData);
			if(jsonData.length() !=0)
			result=poizonSecondService.updateSingleProductDetails(jsonData);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
    }
	/**
	 * This method is used to get sale for single product to pie chart
	 * */
	@RequestMapping(value = "/getSaleByBrandNoPackQty", method = RequestMethod.GET)
    public @ResponseBody List<PieChartBean> getSaleByBrandNoPackQty(HttpServletRequest request, HttpServletResponse response,
    		@RequestParam("brandNoPackQty") double brandNoPackQty,@RequestParam("sdate") String sdate,@RequestParam("edate") String edate) throws ParseException {
		log.info("PoizonController::getSaleByBrandNoPackQty");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<PieChartBean> pieChartBean = null;
 		pieChartBean = poizonSecondService.getSaleByBrandNoPackQty(brandNoPackQty,CommonUtil.chageDateFormat(sdate),CommonUtil.chageDateFormat(edate));
		return pieChartBean;
    }
	/**
	 * This method is used to get product list with short name
	 * */
	@RequestMapping(value = "/getProductListWithShortName", method = RequestMethod.GET)
    public @ResponseBody List<AddNewBrandBean> getProductListWithShortName(HttpServletRequest request, HttpServletResponse response,
    		@RequestParam("brandNoPackQty") double brandNoPackQty,@RequestParam("categoryId") int categoryId) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<AddNewBrandBean> addNewBrandBean = null;
 		addNewBrandBean = poizonSecondService.getProductListWithShortName(brandNoPackQty,categoryId);
		return addNewBrandBean;
    }
	@RequestMapping(value = "/getMultiChartBeanSaleData", method = RequestMethod.GET)
    public @ResponseBody TillDateBalanceSheetChart getMultiChartBeanSaleData(HttpServletRequest request, HttpServletResponse response,
    		@RequestParam("sdate") String sdate,@RequestParam("edate") String edate,@RequestParam("targets") int[] brandNoPackQty) {
		System.out.println("PoizonSecondController.getMultiChartBeanSaleData ");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		System.out.println(brandNoPackQty.length);
 		TillDateBalanceSheetChart tillDateBalanceSheetChart = null;
 		tillDateBalanceSheetChart = poizonSecondService.getMultiChartBeanSaleData(CommonUtil.chageDateFormat(sdate),CommonUtil.chageDateFormat(edate),brandNoPackQty);
 		System.out.println(tillDateBalanceSheetChart.toString());
		return tillDateBalanceSheetChart;
    }
	/**
	 * This method is used to upload invoice
	 * */
	@RequestMapping(value = { "/addNewIndentFile" }, method = { RequestMethod.POST })
 	public String addNewIndentFile(Model model,HttpServletRequest request,HttpServletResponse response,RedirectAttributes redirectAttributes,
 			@RequestParam("indentDate") String indentDate,@RequestParam(value = "ngalleryImage", required = false) MultipartFile ngalleryImage) {
		  String result=null;
		addNewIndentBean indentBean= new addNewIndentBean();
		//indentBean.setIndentMonth(indentMonth);
		indentBean.setIndentDate(CommonUtil.chageDateFormat(indentDate));
		if(ngalleryImage !=null && indentBean.toString().length() >0){
			try {
				validatePdf(ngalleryImage);
			} catch (RuntimeException re) {
				log.info(re.getMessage(), re);
				// bindingResult.reject(re.getMessage());
				return "redirect:/UploadAndViewInvoice";
			}
			try {
				String fileName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
				savePdf(fileName, ngalleryImage, request);
				String imgType[] = ngalleryImage.getContentType().split("/");
				//String outerFolderName = indentMonth;
				indentBean.setImageName( "/" + fileName + "." + imgType[1]);
				//discountTransactionBean.setNgalleryImage(imagesRetrivalPath + outerFolderName + "/" + innerFolderName + "/" + fileName + "." + imgType[1]);
				indentBean.setNgalleryImage(poizinIndentFileRetrival + "/" + fileName + "." + imgType[1]);
				result = poizonSecondService.addNewIndentFile(indentBean);
				redirectAttributes.addFlashAttribute("message", result);
			} catch (IOException e) {
				log.info(e.getMessage(), e);
				return "redirect:/mobileViewEnterSaleSheet";
			}
		}
		return "redirect:/UploadAndViewInvoice";
 	}
	private void validatePdf(MultipartFile image) {
		System.out.println(image.getContentType());
		if (!(image.getContentType().equalsIgnoreCase("application/pdf")))
				{
			throw new RuntimeException("Only pdf are accepted");
		}
	}
	private void savePdf(String filename, MultipartFile image, HttpServletRequest request)
			throws RuntimeException, IOException {
		try {
			String imgType[] = image.getContentType().split("/");
			//String outerFolderName = months;
			File file = new File(
					poizinIndentFile + File.separator + filename + "." + imgType[1]);// +"."+imgType[1]
			FileUtils.writeByteArrayToFile(file, image.getBytes());
			log.info("Go to the location:  " + file.toString()
					+ " on your computer and verify that the image has been stored.");
		} catch (IOException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/**
	 * This method is used to get uploaded invoice file
	 * */
	@RequestMapping(value = "/getIndentListBasedOnMonth", method = RequestMethod.GET)
    public @ResponseBody List<UploadAndViewWithYearBean> getIndentListBasedOnMonth(HttpServletRequest request, HttpServletResponse response) {
		System.out.println("PoizonSecondController.getIndentListBasedOnMonth ");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<UploadAndViewWithYearBean> indentList = null;
 		indentList = poizonSecondService.getIndentListBasedOnMonth();
		return indentList;
    }
	@RequestMapping(value = "/getDiscountForAllCompanies", method = RequestMethod.GET)
    public @ResponseBody List<DiscountAsPerCompanyBeen> getDiscountForAllCompanies(HttpServletRequest request, HttpServletResponse response,@RequestParam("month") String month) {
		System.out.println("PoizonSecondController.getDiscountForAllCompanies ");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<DiscountAsPerCompanyBeen> discountAsPerCompanyBeen = null;
 		discountAsPerCompanyBeen = poizonSecondService.getDiscountForAllCompanies(month);
		return discountAsPerCompanyBeen;
    }
	/**
	 * This method is used to save vendor adjustment or vendor discount
	 * */
	@RequestMapping(value = { "/saveVendorAdjustment" }, method = { RequestMethod.POST })
 	public @ResponseBody String saveVendorAdjustment(Model model,HttpServletRequest request,HttpServletResponse response,
 			@RequestParam("vendorMonth") String date, @RequestParam("companyId") String company,@RequestParam("vendorAmount") double vendorAmount,
 			@RequestParam("vendorcomment") String vendorcomment) throws ParseException {
		String result=null;
		DateFormat df = new SimpleDateFormat("dd/MMMM/yyyy"); 
	    Date startDate;
		startDate = df.parse(date);
	    df = new SimpleDateFormat("yyyy-MM-dd");  
	 	String vendorMonth = df.format(startDate); 
		VendorAdjustmentBean bean = new VendorAdjustmentBean();
		bean.setMonth(vendorMonth);
		 // bean.setCompany(companyId);
		bean.setAmount(vendorAmount);
		bean.setComment(vendorcomment);
		if(bean != null && bean.getAmount() > 0){
		result =  poizonSecondService.saveVendorAdjustment(bean,company);
	   }
	    return result;
 	}
	/**
	 * This method is used to get data to prepare grouping discount based on last 30 days sold
	 * */
	@RequestMapping(value = "/callForDiscountData", method = RequestMethod.GET)
	public @ResponseBody List<CallDiscountBean> callForDiscountData(HttpServletRequest request, HttpServletResponse response,@RequestParam("month") String month){
		log.info("PoizinController : callForDiscountData()");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<CallDiscountBean> listData = poizonSecondService.callForDiscountData(month);
		return listData;
	}
	@RequestMapping(value = "/saveGroupingDiscountEstimateData", method = RequestMethod.POST)
    public @ResponseBody MessageBean saveGroupingDiscountEstimateData(@RequestBody String values,HttpServletRequest request, HttpServletResponse response,@RequestParam("date") String date) throws ParseException{
		log.info("Client Ip: "+request.getHeader("x-my-custom-header"));
		//System.out.println("values>>"+values);
		ModelAndView mav = new ModelAndView();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		MessageBean messageBean = new MessageBean();
		String result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			JSONArray discountDetails = jsonObject.getJSONArray("discountDetails");
			result=poizonSecondService.saveGroupingDiscountEstimateData(discountDetails,date);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		} catch (Exception ex) {
			log.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return messageBean;
		}
    }
	/**
	 * This method is used to save Scheme Data
	 * */
	@RequestMapping(value = "/saveSchemeListData", method = RequestMethod.POST, produces="text/plain")
    public @ResponseBody ResponseEntity<String> saveSchemeListData(@RequestBody String values,HttpServletRequest request, HttpServletResponse response
    		,@RequestParam("date") String date) throws JSONException{
		log.info("PoizonController.postCreditData>> "+values);
		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		System.out.println("date:: "+date);
 		//JSONObject jsonObj = new JSONObject(values);
		JSONArray schemeJson = new JSONArray(values);
 		String result  = poizonSecondService.saveSchemeListData(schemeJson, CommonUtil.chageDateFormat(date));
		return new ResponseEntity<String>(result, HttpStatus.ACCEPTED);
	}
	/**
	 * this method is used to get all distinct category from category_tab table
	 * */
	@RequestMapping(value = "/getGroupList", method = RequestMethod.GET)
	public @ResponseBody List<companyBean> getGroupList(HttpServletRequest request, HttpServletResponse response){
		log.info("PoizonSecondController.getGroupList");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<companyBean> category = null;
 		category = poizonSecondService.getGroupList();
 		return category;
	}
}

