
package com.zambient.poizon.controller;



import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.zambient.poizon.bean.CompareSaleBean;
import com.zambient.poizon.bean.DiscountCalculationBean;
import com.zambient.poizon.bean.SaleListBean;
import com.zambient.poizon.services.HybridService;
import com.zambient.poizon.utill.CommonUtil;
import com.zambient.poizon.utill.PoizinMail;
import com.zambient.poizon.utill.UserSession;

@Controller
public class HybridController {
final static Logger log = Logger.getLogger(HybridController.class);
	
	@Value("${poizinPdfFile}")
	private String poizinPdfFile;
	@Autowired 
	private UserSession userSession;
	@Autowired
	HybridService hybridService;
	@Autowired
	PoizinMail poizinMail;
	
	@Value("${mail.from.username}")
	private String mailFromUsername;

	@Value("${mail.from.bccusername}")
	private String mailFromBccUsername;
	
	@Value("${poizin.indent.file.retrival.path}")
	private String poizinIndentFileRetrival;
	
	
	/**
	 * App user login 
	 * */
	/*@RequestMapping(value ="/appUserLogin", method = RequestMethod.POST)
	@ResponseBody
	public String appUserLogin(HttpServletRequest request, RedirectAttributes redirectAttributes,@RequestParam("userId") String userId) {
		log.info("HybridController.appUserLogin");
		String result = null;
		if (userId != null && userId != "" && userId.trim().length() != 0) {
			result = hybridService.appUserLogin();
		}
		if (result.equals(userId)) {
			userSession.setUserId(result);
			userSession.setLoginId(Long.parseLong("123"));
			return "redirect:/mobileViewHome";
			
		} else {
			redirectAttributes.addFlashAttribute("message", "Please Enter Correct User ID");
		  return "redirect:/loginInMobile";
		}
	}*/
	
/**
 * An API with saledate, where we need to get pdf with sale data
 * @throws IOException 
 * */
	
	
	
	
	
	
@RequestMapping(value = "/generateSaleDataPdf", method = RequestMethod.GET)
public @ResponseBody void generateSaleDataPdf(HttpServletRequest request, HttpServletResponse response,@RequestParam("saledate") String saledate) throws IOException{
	log.info("HybridController :: generateSaleDataPdf()");
	boolean flag = false ;
	SaleListBean saleDeatils = hybridService.generateSaleDataPdf(saledate);
	if(saleDeatils.getSaleBean() !=null && saleDeatils.getSaleBean().size() > 0)
		flag = generatepdf(request, response, saleDeatils);
	if(flag)
	CommonUtil.downloadZipFile(request,response,poizinPdfFile+"/Sale_Sheet.pdf");
}
public boolean generatepdf(HttpServletRequest request, HttpServletResponse response,SaleListBean saleDeatils){
	int listSize = (saleDeatils.getSaleBean().size()/2)+2;
	long totalAmount = 0;
	String pdfResult = poizinPdfFile+"/Sale_Sheet.pdf";
	
	Document document = new Document(PageSize.A3);
	try
	{
    PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfResult));
	document.open();
	
	PdfPTable table = new PdfPTable(2); // 3 columns.
	table.setWidthPercentage(108);
	table.setSpacingBefore(10f);
	table.setSpacingAfter(10f);
	
	//Set Column widths
	float[] columnWidths = {1f, 1f};
	table.setWidths(columnWidths);
	
	PdfPCell cell1 = new PdfPCell(new Paragraph(new Phrase("FIST COLUMN")));
	cell1.setBorderColor(BaseColor.BLACK);
	PdfPTable nestedTable  = new PdfPTable(9);
	nestedTable.setWidthPercentage(100);
	
	float[] nestedcolumnWidths = {2f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f};
	nestedTable.setWidths(nestedcolumnWidths);
	
	PdfPCell tablecell1 = new PdfPCell(new Paragraph("POIZIN WINES", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD)));
	tablecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
	tablecell1.setVerticalAlignment(Element.ALIGN_LEFT);
	tablecell1.setPaddingTop(5);
	tablecell1.setPaddingBottom(5);
	tablecell1.setColspan(9);
	tablecell1.setBorder(0);
	
	PdfPCell nestedcell1 = new PdfPCell(new Paragraph(new Phrase("NAME", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
	nestedcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
	nestedcell1.setVerticalAlignment(Element.ALIGN_CENTER);
	PdfPCell nestedcell2 = new PdfPCell(new Paragraph(new Phrase("PT", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
	nestedcell2.setHorizontalAlignment(Element.ALIGN_CENTER);
	nestedcell2.setVerticalAlignment(Element.ALIGN_CENTER);
	PdfPCell nestedcell3 = new PdfPCell(new Paragraph(new Phrase("OB", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
	nestedcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
	nestedcell3.setVerticalAlignment(Element.ALIGN_CENTER);
	PdfPCell nestedcell4 = new PdfPCell(new Paragraph(new Phrase("REC", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
	nestedcell4.setHorizontalAlignment(Element.ALIGN_CENTER);
	nestedcell4.setVerticalAlignment(Element.ALIGN_CENTER);
	PdfPCell nestedcell5 = new PdfPCell(new Paragraph(new Phrase("TTL", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
	nestedcell5.setHorizontalAlignment(Element.ALIGN_CENTER);
	nestedcell5.setVerticalAlignment(Element.ALIGN_CENTER);
	PdfPCell nestedcell6 = new PdfPCell(new Paragraph(new Phrase("CB", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
	nestedcell6.setHorizontalAlignment(Element.ALIGN_CENTER);
	nestedcell6.setVerticalAlignment(Element.ALIGN_CENTER);
	PdfPCell nestedcell7 = new PdfPCell(new Paragraph(new Phrase("SALE", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
	nestedcell7.setHorizontalAlignment(Element.ALIGN_CENTER);
	nestedcell7.setVerticalAlignment(Element.ALIGN_CENTER);
	PdfPCell nestedcell8 = new PdfPCell(new Paragraph(new Phrase("MRP", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
	nestedcell8.setHorizontalAlignment(Element.ALIGN_CENTER);
	nestedcell8.setVerticalAlignment(Element.ALIGN_CENTER);
	PdfPCell nestedcell9 = new PdfPCell(new Paragraph(new Phrase("TOTAL", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
	nestedcell9.setHorizontalAlignment(Element.ALIGN_CENTER);
	nestedcell9.setVerticalAlignment(Element.ALIGN_CENTER);
	
	nestedTable.addCell(tablecell1);
	nestedTable.addCell(nestedcell1);
	nestedTable.addCell(nestedcell2);
	nestedTable.addCell(nestedcell3);
	nestedTable.addCell(nestedcell4);
	nestedTable.addCell(nestedcell5);
	nestedTable.addCell(nestedcell6);
	nestedTable.addCell(nestedcell7);
	nestedTable.addCell(nestedcell8);
	nestedTable.addCell(nestedcell9);
	
    
 		for (int i = 0; i < listSize; i++) { 
 			PdfPCell nestedcell11 =null, nestedcell41=null, nestedcell51=null;
 				nestedcell11 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getBrandName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				/*nestedcell11.setBorder(PdfPCell.NO_BORDER);
				nestedcell11.setCellEvent(new SolidBorder(PdfPCell.TOP));
				nestedcell11.setCellEvent(new SolidBorder(PdfPCell.LEFT));*/
				nestedcell11.setPaddingTop(5);
				nestedcell11.setPaddingBottom(5);
				nestedcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell11.setVerticalAlignment(Element.ALIGN_CENTER);
				//nestedcell11.setRowspan(3);
				
				
				PdfPCell nestedcell21 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getPackType(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell21.setPaddingTop(5);
				nestedcell21.setPaddingBottom(5);
				nestedcell21.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell21.setVerticalAlignment(Element.ALIGN_CENTER);
				
				PdfPCell nestedcell31 = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getOpening()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell31.setPaddingTop(5);
				nestedcell31.setPaddingBottom(5);
				nestedcell31.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell31.setVerticalAlignment(Element.ALIGN_CENTER);
				
				if(saleDeatils.getSaleBean().get(i).getReceived() > 1){
				 nestedcell41 = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getReceived()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				 nestedcell41.setPaddingTop(5);
				 nestedcell41.setPaddingBottom(5);
				 nestedcell41.setHorizontalAlignment(Element.ALIGN_CENTER);
				 nestedcell41.setVerticalAlignment(Element.ALIGN_CENTER);
				 nestedcell51  = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getOpening()+saleDeatils.getSaleBean().get(i).getReceived()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				 nestedcell51.setPaddingTop(5);
				 nestedcell51.setPaddingBottom(5);
				 nestedcell51.setHorizontalAlignment(Element.ALIGN_CENTER);
				 nestedcell51.setVerticalAlignment(Element.ALIGN_CENTER);
				}else{
			     nestedcell41 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9))));	
			     nestedcell51  = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9))));
				}
				PdfPCell nestedcell61 = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getClosing()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell61.setPaddingTop(5);
				nestedcell61.setPaddingBottom(5);
				nestedcell61.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell61.setVerticalAlignment(Element.ALIGN_CENTER);
				
				PdfPCell nestedcell71 = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getTotalSale()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell71.setPaddingTop(5);
				nestedcell71.setPaddingBottom(5);
				nestedcell71.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell71.setVerticalAlignment(Element.ALIGN_CENTER);
				
				PdfPCell nestedcell81 = new PdfPCell(new Paragraph(new Phrase(Integer.toString((saleDeatils.getSaleBean().get(i).getUnitPrice()).intValue()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell81.setPaddingTop(5);
				nestedcell81.setPaddingBottom(5);
				nestedcell81.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell81.setVerticalAlignment(Element.ALIGN_CENTER);
				PdfPCell nestedcell91 = new PdfPCell(new Paragraph(new Phrase(Integer.toString((saleDeatils.getSaleBean().get(i).getTotalPrice()).intValue()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell91.setPaddingTop(5);
				nestedcell91.setPaddingBottom(5);
				nestedcell91.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell91.setVerticalAlignment(Element.ALIGN_CENTER);
				
				nestedTable.addCell(nestedcell11);
				nestedTable.addCell(nestedcell21);
				nestedTable.addCell(nestedcell31);
				nestedTable.addCell(nestedcell41);
				nestedTable.addCell(nestedcell51);
				nestedTable.addCell(nestedcell61);
				nestedTable.addCell(nestedcell71);
				nestedTable.addCell(nestedcell81);
				nestedTable.addCell(nestedcell91);
				
				totalAmount += (saleDeatils.getSaleBean().get(i).getTotalPrice()).intValue();
			}
			
			cell1.addElement(nestedTable);
			cell1.setBorder(0);
			
			//cell1.setBorder(0);
			PdfPCell cell2 = new PdfPCell(new Paragraph(new Phrase("SECOND COLUMN")));
			cell2.setBorderColor(BaseColor.BLACK);
			PdfPTable nestedRightTable  = new PdfPTable(9);
			nestedRightTable.setWidthPercentage(100);
			
			float[] nestedRightcolumnWidths = {2f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f};
			nestedRightTable.setWidths(nestedRightcolumnWidths);
			PdfPCell tablecell2 = new PdfPCell(new Paragraph(new Phrase("Date: "+CommonUtil.chageDateFormatSec(saleDeatils.getDate()), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			tablecell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
			tablecell2.setVerticalAlignment(Element.ALIGN_RIGHT);
			tablecell2.setPaddingTop(5);
			tablecell2.setPaddingBottom(5);
			tablecell2.setColspan(9);
			tablecell2.setBorder(0);
			PdfPCell nestedRightcell1 = new PdfPCell(new Paragraph(new Phrase("NAME", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell1.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell2 = new PdfPCell(new Paragraph(new Phrase("PT", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell2.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell3 = new PdfPCell(new Paragraph(new Phrase("OB", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell3.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell4 = new PdfPCell(new Paragraph(new Phrase("REC", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell4.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell4.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell5 = new PdfPCell(new Paragraph(new Phrase("TTL", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell5.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell5.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell6 = new PdfPCell(new Paragraph(new Phrase("CB", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell6.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell6.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell7 = new PdfPCell(new Paragraph(new Phrase("SALE", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell7.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell7.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell8 = new PdfPCell(new Paragraph(new Phrase("MRP", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell8.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell8.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell9 = new PdfPCell(new Paragraph(new Phrase("TOTAL", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell9.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell9.setVerticalAlignment(Element.ALIGN_CENTER);
			
			nestedRightTable.addCell(tablecell2);
			nestedRightTable.addCell(nestedRightcell1);
			nestedRightTable.addCell(nestedRightcell2);
			nestedRightTable.addCell(nestedRightcell3);
			nestedRightTable.addCell(nestedRightcell4);
			nestedRightTable.addCell(nestedRightcell5);
			nestedRightTable.addCell(nestedRightcell6);
			nestedRightTable.addCell(nestedRightcell7);
			nestedRightTable.addCell(nestedRightcell8);
			nestedRightTable.addCell(nestedRightcell9);
			for (int i = listSize; i < saleDeatils.getSaleBean().size(); i++) { 
	 			PdfPCell nestedRightcell11 =null, nestedRightcell41=null, nestedRightcell51=null;
	 				nestedRightcell11 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getBrandName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
	 				/*nestedRightcell11.setBorder(PdfPCell.NO_BORDER);
	 				nestedRightcell11.setCellEvent(new SolidBorder(PdfPCell.TOP));
	 				nestedRightcell11.setCellEvent(new SolidBorder(PdfPCell.LEFT));*/
	 				nestedRightcell11.setPaddingTop(5);
	 				nestedRightcell11.setPaddingBottom(5);
	 				nestedRightcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
	 				nestedRightcell11.setVerticalAlignment(Element.ALIGN_CENTER);
					
					PdfPCell nestedRightcell21 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getPackType(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					nestedRightcell21.setPaddingTop(5);
					nestedRightcell21.setPaddingBottom(5);
					nestedRightcell21.setHorizontalAlignment(Element.ALIGN_CENTER);
					nestedRightcell21.setVerticalAlignment(Element.ALIGN_CENTER);
					
					PdfPCell nestedRightcell31 = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getOpening()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					nestedRightcell31.setPaddingTop(5);
					nestedRightcell31.setPaddingBottom(5);
					nestedRightcell31.setHorizontalAlignment(Element.ALIGN_CENTER);
					nestedRightcell31.setVerticalAlignment(Element.ALIGN_CENTER);
					
					if(saleDeatils.getSaleBean().get(i).getReceived() > 1){
						nestedRightcell41 = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getReceived()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
						nestedRightcell41.setPaddingTop(5);
						nestedRightcell41.setPaddingBottom(5);
						nestedRightcell41.setHorizontalAlignment(Element.ALIGN_CENTER);
						nestedRightcell41.setVerticalAlignment(Element.ALIGN_CENTER);
						nestedRightcell51  = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getOpening()+saleDeatils.getSaleBean().get(i).getReceived()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
						nestedRightcell51.setPaddingTop(5);
						nestedRightcell51.setPaddingBottom(5);
						nestedRightcell51.setHorizontalAlignment(Element.ALIGN_CENTER);
						nestedRightcell51.setVerticalAlignment(Element.ALIGN_CENTER);
					}else{
						nestedRightcell41 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9))));	
						nestedRightcell51  = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9))));
					}
					PdfPCell nestedRightcell61 = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getClosing()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					nestedRightcell61.setPaddingTop(5);
					nestedRightcell61.setPaddingBottom(5);
					nestedRightcell61.setHorizontalAlignment(Element.ALIGN_CENTER);
					nestedRightcell61.setVerticalAlignment(Element.ALIGN_CENTER);
					
					PdfPCell nestedRightcell71 = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getTotalSale()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					nestedRightcell71.setPaddingTop(5);
					nestedRightcell71.setPaddingBottom(5);
					nestedRightcell71.setHorizontalAlignment(Element.ALIGN_CENTER);
					nestedRightcell71.setVerticalAlignment(Element.ALIGN_CENTER);
					
					PdfPCell nestedRightcell81 = new PdfPCell(new Paragraph(new Phrase(Integer.toString((saleDeatils.getSaleBean().get(i).getUnitPrice()).intValue()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					nestedRightcell81.setPaddingTop(5);
					nestedRightcell81.setPaddingBottom(5);
					nestedRightcell81.setHorizontalAlignment(Element.ALIGN_CENTER);
					nestedRightcell81.setVerticalAlignment(Element.ALIGN_CENTER);
					PdfPCell nestedRightcell91 = new PdfPCell(new Paragraph(new Phrase(Integer.toString((saleDeatils.getSaleBean().get(i).getTotalPrice()).intValue()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					nestedRightcell91.setPaddingTop(5);
					nestedRightcell91.setPaddingBottom(5);
					nestedRightcell91.setHorizontalAlignment(Element.ALIGN_CENTER);
					nestedRightcell91.setVerticalAlignment(Element.ALIGN_CENTER);
					
					nestedRightTable.addCell(nestedRightcell11);
					nestedRightTable.addCell(nestedRightcell21);
					nestedRightTable.addCell(nestedRightcell31);
					nestedRightTable.addCell(nestedRightcell41);
					nestedRightTable.addCell(nestedRightcell51);
					nestedRightTable.addCell(nestedRightcell61);
					nestedRightTable.addCell(nestedRightcell71);
					nestedRightTable.addCell(nestedRightcell81);
					nestedRightTable.addCell(nestedRightcell91);
					
					totalAmount += (saleDeatils.getSaleBean().get(i).getTotalPrice()).intValue();
			  }
			cell2.addElement(nestedRightTable);
			cell2.setBorder(0);
			table.addCell(cell1);
			table.addCell(cell2);
			document.add(table);
			document.add(new Paragraph("Total Sale Amount : "+totalAmount, FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD)));
			document.close();
			writer.close();
			
	}catch (Exception e){
		e.printStackTrace();
		return false;
	}
	return true;
}	
/**
 * This method is used to build Discount calculation
 * */
@RequestMapping(value = "getDiscountCalculationList", method = RequestMethod.GET)
public @ResponseBody List<DiscountCalculationBean> getDiscountCalculationList(HttpServletRequest request, HttpServletResponse response, @RequestParam("month") String month){
	log.info("HybridController :: getDiscountCalculationList() "+month);
	response.setContentType("application/json");
	response.setHeader("Access-Control-Allow-Origin","*");
	response.setHeader("value", "valid");
	List<DiscountCalculationBean> listData = hybridService.getDiscountCalculationList(month);
	return listData;
}
/**
 * This method is used to get Weekly sale to compare with previous week
 * */	
@RequestMapping(value = "getWeeklySaleComparisionData", method = RequestMethod.GET)
public @ResponseBody List<CompareSaleBean> getWeeklySaleComparisionData(HttpServletRequest request, HttpServletResponse response,
		@RequestParam("startDate") String startDate,@RequestParam("endDate") String endDate, @RequestParam("weekormonth") int id){
	log.info("HybridController :: getWeeklySaleComparisionData() ");
	response.setContentType("application/json");
	response.setHeader("Access-Control-Allow-Origin","*");
	response.setHeader("value", "valid");
    List<CompareSaleBean> listData = hybridService.getWeeklyOrMonthlySaleComparisionData(startDate,endDate,id);
	return listData;
}

	 
}
