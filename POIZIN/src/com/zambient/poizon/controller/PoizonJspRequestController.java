package com.zambient.poizon.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zambient.poizon.bean.AddNewBrandBean;
import com.zambient.poizon.bean.InvoiceBo;
import com.zambient.poizon.bean.MonthlyExpensesBean;
import com.zambient.poizon.bean.ProductOrderBy;
import com.zambient.poizon.bean.SaleBean;

@Controller
public class PoizonJspRequestController {
	
	@RequestMapping(value = { "/invoiceDateWiseReport" },method = {RequestMethod.GET})
	public String inventorjsp(){
        return "invoiceDateWiseReport";
	}
	@RequestMapping("/addNewBrand")
	public ModelAndView addNewBrand(){
        return new ModelAndView("addNewBrand", "addNewBrandBean", new AddNewBrandBean());  
	}
	@RequestMapping("/saveInvoice")
	public ModelAndView saveInvoice(){
        return new ModelAndView("invoicepage", "invoiceBo", new InvoiceBo());  
	}
	@RequestMapping("/sale")
	public ModelAndView sale(){
        return new ModelAndView("salepage", "saleBean", new SaleBean());  
	}
	
	@RequestMapping("/admin")
	public ModelAndView admin(){
        return new ModelAndView("admin", "title", "admin page");  
	}
	
	@RequestMapping(value = { "/header" },method = {RequestMethod.GET})
	public String header(ModelMap model) {
	      return "header";
	   }
	@RequestMapping(value = { "/stockLifting" },method = {RequestMethod.GET})
	public String stockLifting(ModelMap model) {
		
	      return "stockLifting";
	   }
	
	@RequestMapping(value = { "/saleReport" },method = {RequestMethod.GET})
	public String saleReport(ModelMap model) {
	      return "saleReport";
	   }
	
	@RequestMapping("/updateSale")  
    public ModelAndView updateSale(Model model, HttpServletRequest request) { 
		   return new ModelAndView("updateSale", "title", "Service  Page");  
      
    }
    @RequestMapping("/saleDayWiseReport")  
    public ModelAndView saleDayWiseReport(Model model, HttpServletRequest request) { 
		   return new ModelAndView("saleDayWiseReport", "title", "saleDayWiseReport  Page");  
      
    }
    @RequestMapping("/inHouseStockValue")  
    public ModelAndView inHouseStockValue(Model model, HttpServletRequest request) { 
		   return new ModelAndView("inHouseStockValue", "title", "inHouseStockValue  Page");  
      
    }
    @RequestMapping("/editMrp")  
    public ModelAndView editMrp(Model model, HttpServletRequest request) { 
		   return new ModelAndView("editMrp", "title", "editMrp  Page");  
      
    }
    @RequestMapping("/stockPredictions")  
    public ModelAndView stockPredictions(Model model, HttpServletRequest request) { 
		   return new ModelAndView("stockPredictions", "title", "stockPredictions  Page");  
      
    }
    @RequestMapping("/balanceSheet")  
    public ModelAndView balanceSheet(Model model, HttpServletRequest request) { 
		   return new ModelAndView("balanceSheet", "title", "balanceSheet  Page");  
      
    }
    @RequestMapping("/discountEstimate")  
    public ModelAndView discountEstimate(Model model, HttpServletRequest request) { 
		   return new ModelAndView("discountEstimate", "title", "discountEstimate  Page");  
      
    }
    @RequestMapping("/retrieveIntent")  
    public ModelAndView retrieveIntent(Model model, HttpServletRequest request) { 
		   return new ModelAndView("retrieveIntent", "title", "retrieveIntent  Page");  
      
    }
    @RequestMapping("/productOrderBy")
	public ModelAndView productOrderBy(){
        return new ModelAndView("productOrderBy", "productOrderBy", new ProductOrderBy());  
	}
    @RequestMapping("/monthlyExpenses")
	public ModelAndView monthlyExpenses(){
        return new ModelAndView("monthlyExpenses", "MonthlyExpensesBean", new MonthlyExpensesBean());  
	}
    
    @RequestMapping("/tillDateBalanceSheet")  
    public ModelAndView tillDateBalanceSheet(Model model, HttpServletRequest request) { 
		   return new ModelAndView("tillDateBalanceSheet", "title", "tillDateBalanceSheet  Page");  
      
    }
    @RequestMapping("/productPerformance")  
    public ModelAndView productPerformance(Model model, HttpServletRequest request) { 
		   return new ModelAndView("productPerformance", "title", "productPerformance  Page");  
      
    }
    @RequestMapping("/tillMonthStockLiftWithDiscount")  
    public ModelAndView tillMonthStockLiftWithDiscount(Model model, HttpServletRequest request) { 
		   return new ModelAndView("tillMonthStockLiftWithDiscount", "title", "tillMonthStockLiftWithDiscount  Page");  
      
    }
    @RequestMapping("/mobileViewInhouseStock")
    public ModelAndView mobileViewInhouseStock(Model model, HttpServletRequest request) { 
		   return new ModelAndView("mobileViewInhouseStock", "title", "mobileViewInhouseStock  Page");  
      
    }
    @RequestMapping("/mobileViewDiscountDues")
    public ModelAndView mobileViewDiscountDues(Model model, HttpServletRequest request) { 
		   return new ModelAndView("mobileViewDiscountDues", "title", "mobileViewDiscountDues  Page");  
      
    }
    @RequestMapping("/mobileViewHome")
    public ModelAndView mobileViewHome() { 
		   return new ModelAndView("mobileViewHome", "title", "mobileViewHome  Page");  
      
    }
    @RequestMapping("/loginInMobile")
	public ModelAndView loginInMobile(){
        return new ModelAndView("loginInMobile", "title", "loginInMobile page");  
	}
    @RequestMapping("/mobileViewDayWiseBalanceSheet")
    public ModelAndView mobileViewDayWiseBalanceSheet(Model model, HttpServletRequest request) { 
		   return new ModelAndView("mobileViewDayWiseBalanceSheet", "title", "mobileViewDayWiseBalanceSheet  Page");  
      
    }
    @RequestMapping("/mobileViewSaleReport")
    public ModelAndView mobileViewSaleReport(Model model, HttpServletRequest request) { 
		   return new ModelAndView("mobileViewSaleReport", "title", "mobileViewSaleReport  Page");  
      
    }
    @RequestMapping("/mobileViewStockLiftingDiscount")
    public ModelAndView mobileViewStockLiftingDiscount(Model model, HttpServletRequest request) { 
		   return new ModelAndView("mobileViewStockLiftingDiscount", "title", "mobileViewStockLiftingDiscount  Page");  
      
    }
    @RequestMapping("/mobileViewStockLifting")
    public ModelAndView mobileViewStockLifting(Model model, HttpServletRequest request) { 
		   return new ModelAndView("mobileViewStockLifting", "title", "mobileViewStockLifting  Page");  
      
    }
    @RequestMapping("/mobileViewDownloadIndent")
    public ModelAndView mobileViewDownloadIndent(Model model, HttpServletRequest request) { 
		   return new ModelAndView("mobileViewDownloadIndent", "title", "mobileViewDownloadIndent  Page");  
      
    }
    @RequestMapping("/productComparisionCategoryWise")
    public ModelAndView productComparisionCategoryWise(Model model, HttpServletRequest request) { 
		   return new ModelAndView("productComparisionCategoryWise", "title", "productComparisionCategoryWise  Page");  
      
    }
    @RequestMapping("/UploadAndViewInvoice")
    public ModelAndView UploadAndViewInvoice(Model model, HttpServletRequest request) { 
		   return new ModelAndView("UploadAndViewInvoice", "title", "UploadAndViewInvoice  Page");  
      
    }
    
    @RequestMapping("/mobileViewProductComparisionCategoryWise")
    public ModelAndView mobileViewProductComparisionCategoryWise(Model model, HttpServletRequest request) { 
		   return new ModelAndView("mobileViewProductComparisionCategoryWise", "title", "mobileViewProductComparisionCategoryWise  Page");  
      
    }
    @RequestMapping("/discountTransaction")
    public ModelAndView discountTransaction(Model model, HttpServletRequest request) { 
		   return new ModelAndView("discountTransaction", "title", "discountTransaction  Page");  
      
    }
    @RequestMapping("/saleWithDiscount")
    public ModelAndView saleWithDiscount(Model model, HttpServletRequest request) { 
		   return new ModelAndView("saleWithDiscount", "title", "saleWithDiscount  Page");  
      
    }
    @RequestMapping("/stockLiftWithDiscountTransaction")
    public ModelAndView stockLiftWithDiscountTransaction(Model model, HttpServletRequest request) { 
		   return new ModelAndView("stockLiftWithDiscountTransaction", "title", "stockLiftWithDiscountTransaction  Page");  
      
    }
    @RequestMapping("/mobileViewSaleWithDiscount")
    public ModelAndView mobileViewSaleWithDiscount(Model model, HttpServletRequest request) { 
		   return new ModelAndView("mobileViewSaleWithDiscount", "title", "mobileViewSaleWithDiscount  Page");  
      
    }
    @RequestMapping("/companyWiseDiscount")
    public ModelAndView companyWiseDiscount(Model model, HttpServletRequest request) { 
		   return new ModelAndView("companyWiseDiscount", "title", "companyWiseDiscount  Page");  
      
    }
    @RequestMapping("/mobileViewCompanyWiseDiscount")
    public ModelAndView mobileViewCompanyWiseDiscount(Model model, HttpServletRequest request) { 
		   return new ModelAndView("mobileViewCompanyWiseDiscount", "title", "mobileViewCompanyWiseDiscount  Page");  
      
    }
    @RequestMapping("/mobileViewInhouseStockWithFilter")
    public ModelAndView mobileViewInhouseStockWithFilter(Model model, HttpServletRequest request) { 
		   return new ModelAndView("mobileViewInhouseStockWithFilter", "title", "mobileViewInhouseStockWithFilter  Page");  
      
    }
    @RequestMapping("/investment")
    public ModelAndView investment(Model model, HttpServletRequest request) { 
		   return new ModelAndView("investment", "title", "investment  Page");  
      
    }
    
    @RequestMapping("/mobileViewDiscountEstimation")
    public ModelAndView mobileViewDiscountEstimation(Model model, HttpServletRequest request) { 
		   return new ModelAndView("mobileViewDiscountEstimation", "title", "mobileViewDiscountEstimation  Page");  
      
    }
    @RequestMapping("/profitAndLossReport")
    public ModelAndView profitAndLossReport(Model model, HttpServletRequest request) { 
		   return new ModelAndView("profitAndLossReport", "title", "profitAndLossReport  Page");  
      
    }
    @RequestMapping("/newIndentEstimate")
    public ModelAndView newIndentEstimate(Model model, HttpServletRequest request) { 
		   return new ModelAndView("newIndentEstimate", "title", "newIndentEstimate  Page");  
      
    }
    @RequestMapping("/expensesReport")
    public ModelAndView expensesReport(Model model, HttpServletRequest request) { 
		   return new ModelAndView("expensesReport", "title", "expensesReport  Page");  
      
    }
    @RequestMapping("/poizinIndentEstimate")// for satti
    public ModelAndView poizinIndentEstimate(Model model, HttpServletRequest request) { 
		   return new ModelAndView("poizinIndentEstimate", "title", "poizinIndentEstimate  Page");  
      
    }
    @RequestMapping("/productList")
    public ModelAndView productList(Model model, HttpServletRequest request) { 
		   return new ModelAndView("productList", "title", "productList  Page");  
      
    }
    @RequestMapping("/newDiscountEstimate")
    public ModelAndView newDiscountEstimate(Model model, HttpServletRequest request) { 
		   return new ModelAndView("newDiscountEstimate", "title", "newDiscountEstimate  Page");  
      
    }
    @RequestMapping("/viewDiscount")
    public ModelAndView viewDiscount(Model model, HttpServletRequest request) { 
		   return new ModelAndView("viewDiscount", "title", "viewDiscount  Page");  
      
    }
    @RequestMapping("/generateDiscountPdf")
    public ModelAndView generateDiscountPdf(Model model, HttpServletRequest request) { 
		   return new ModelAndView("generateDiscountPdf", "title", "generateDiscountPdf  Page");  
      
    }
    @RequestMapping("/rentalandbreakage")
    public ModelAndView vendoradjustment(Model model, HttpServletRequest request) { 
		   return new ModelAndView("vendoradjustment", "title", "vendoradjustment  Page");  
      
    }
    @RequestMapping("/credit")
    public ModelAndView credit(Model model, HttpServletRequest request) { 
		   return new ModelAndView("credit", "title", "credit  Page");  
      
    }
    @RequestMapping("/newSaleReport")
    public ModelAndView newSaleReport(Model model, HttpServletRequest request) { 
		   return new ModelAndView("newSaleReport", "title", "newSaleReport  Page");  
      
    }
    @RequestMapping("/discountcalculation")
    public ModelAndView discountcalculation(Model model, HttpServletRequest request) { 
		   return new ModelAndView("discountcalculation", "title", "discountcalculation  Page");  
      
    }
    @RequestMapping("/mobileViewSaleComparision")
    public ModelAndView mobileViewSaleComparision(Model model, HttpServletRequest request) { 
		   return new ModelAndView("mobileViewSaleComparision", "title", "mobileViewSaleComparision  Page");  
      
    }

}
