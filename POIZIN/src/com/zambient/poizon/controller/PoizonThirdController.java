package com.zambient.poizon.controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.zambient.poizon.bean.DateAndIndentBean;
import com.zambient.poizon.bean.DiscountAndMonthBean;
import com.zambient.poizon.bean.DiscountAsPerCompanyBeen;
import com.zambient.poizon.bean.DiscountMonthWiseBean;
import com.zambient.poizon.bean.DiscountTransactionBean;
import com.zambient.poizon.bean.DownloadTargetCase;
import com.zambient.poizon.bean.ExpenseCategoryAndDate;
import com.zambient.poizon.bean.HomeBean;
import com.zambient.poizon.bean.IndentEstimatePDF;
import com.zambient.poizon.bean.MobileViewStockLiftingDiscount;
import com.zambient.poizon.bean.RentalAndBreakageBean;
import com.zambient.poizon.bean.SaleListBean;
import com.zambient.poizon.bean.TargetWithBudget;
import com.zambient.poizon.bean.TotalPriceDateWiseBean;
import com.zambient.poizon.bean.VendorAdjustmentGetArrearsBean;
import com.zambient.poizon.services.PoizonMobileUIService;
import com.zambient.poizon.services.PoizonSecondService;
import com.zambient.poizon.services.PoizonThirdService;
import com.zambient.poizon.utill.CommonUtil;
import com.zambient.poizon.utill.SolidBorder;
import com.zambient.poizon.utill.UserSession;

@Controller
public class PoizonThirdController {
final static Logger log = Logger.getLogger(PoizonThirdController.class);
	
	@Value("${poizinPdfFile}")
	private String poizinPdfFile;
	@Autowired 
	private UserSession userSession;
	@Autowired
	PoizonThirdService poizonThirdService;
	@Autowired
	PoizonSecondService poizonSecondService;
	@Autowired
	PoizonMobileUIService poizonMobileUIService;
	
	@RequestMapping(value = "/getHomePageDetails", method = RequestMethod.GET)
	public @ResponseBody HomeBean getHomePageDetails(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		log.info("PoizonThirdController.getHomePageDetails");
		response.setContentType("application/json");
			response.setHeader("Access-Control-Allow-Origin","*");
			response.setHeader("value", "valid");
			HomeBean homeBean = null;
			homeBean = poizonThirdService.getHomePageDetails();
		return homeBean;
	}
	@RequestMapping(value = "/getSaleCanvasPieChart", method = RequestMethod.GET)
	public @ResponseBody List<TotalPriceDateWiseBean> getSaleCanvasPieChart(HttpServletRequest request, HttpServletResponse response,
			@RequestParam("startDate") String startDate,@RequestParam("endDate") String endDate) throws ParseException {
		log.info("PoizonThirdController.getSaleCanvasPieChart");
		response.setContentType("application/json");
			response.setHeader("Access-Control-Allow-Origin","*");
			response.setHeader("value", "valid");
			List<TotalPriceDateWiseBean> totalPriceDateWiseBean = null;
			totalPriceDateWiseBean = poizonThirdService.getSaleCanvasPieChart(CommonUtil.chageDateFormat(startDate),CommonUtil.chageDateFormat(endDate));
		return totalPriceDateWiseBean;
	}
	
	@RequestMapping(value = "/downloadDiscount", method = RequestMethod.GET,produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody void downloadDiscount(HttpServletRequest request, HttpServletResponse response,@RequestParam("company") int company,
    		@RequestParam("date") String date,@RequestParam("companyName") String companyName) throws Exception {
		boolean value = downLoadCompanyDiscount(request,response,company,date,companyName);
		if(value)
		CommonUtil.downloadZipFile(request,response,poizinPdfFile+"/"+companyName+".pdf");
    }
	
    public boolean downLoadCompanyDiscount(HttpServletRequest request, HttpServletResponse response, int company, String date, String companyName) throws ParseException {
		System.out.println("PoizonThirdController.downLoadCompanyDiscount");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<DiscountAsPerCompanyBeen> discountAsPerCompanyBeenList = null;
 		discountAsPerCompanyBeenList = poizonThirdService.downLoadCompanyDiscount(company,date);
 		List<DiscountAndMonthBean> secBeanList = null;
 		secBeanList = poizonThirdService.downloadDiscountCompanyWiseForAllMonth(date,company);
 		List<DiscountAndMonthBean>  discountTransactionBeanList= CommonUtil.getListBasedOnMatch(secBeanList);
 		
 		
 		String pdfResult = poizinPdfFile+"/"+companyName+".pdf";
 		
 		Document document = new Document(PageSize.A4_LANDSCAPE);
 		try
		{
 			double sumOfCheck = 0,totalAdjcheck = 0,totalDiscount = 0, totalRental=0;
 			for(DiscountAsPerCompanyBeen bean : discountAsPerCompanyBeenList){
				totalAdjcheck = totalAdjcheck+bean.getAdjustmentCheque();
				sumOfCheck = sumOfCheck+bean.getChequeAmount();
				totalDiscount = bean.getDiscountAmount();
				totalRental = bean.getRentals();
 			}
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfResult));
			document.open();
			PdfPTable tableParent = new PdfPTable(2); // 3 columns.
			tableParent.setWidthPercentage(100); //Width 100%
			tableParent.setSpacingBefore(10f); //Space before table
			tableParent.setSpacingAfter(10f); //Space after table
			
			document.add(new Paragraph(companyName, FontFactory.getFont(FontFactory.HELVETICA, 12,Font.BOLD)));
			
			float[] columnWidthsParent = {1f, 1f};
			tableParent.setWidths(columnWidthsParent);
			
			PdfPCell cellParent1 = new PdfPCell(new Paragraph(new Phrase("Rentals + Total Discount", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent1.setBorderColor(BaseColor.BLACK);
			cellParent1.setPaddingTop(5);
			cellParent1.setPaddingBottom(5);
			cellParent1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell cellParent2 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalDiscount+totalRental), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent2.setBorderColor(BaseColor.BLACK);
			cellParent2.setPaddingTop(5);
			cellParent2.setPaddingBottom(5);
			cellParent2.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellParent2.setVerticalAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell cellParent3 = new PdfPCell(new Paragraph(new Phrase("Total Cheques Received", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent3.setBorderColor(BaseColor.BLACK);
			cellParent3.setPaddingTop(5);
			cellParent3.setPaddingBottom(5);
			cellParent3.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent3.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell cellParent4 = new PdfPCell(new Paragraph(new Phrase(Double.toString(sumOfCheck), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent4.setBorderColor(BaseColor.BLACK);
			cellParent4.setPaddingTop(5);
			cellParent4.setPaddingBottom(5);
			cellParent4.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellParent4.setVerticalAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell cellParent5 = new PdfPCell(new Paragraph(new Phrase("Cheque Adjustments", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent5.setBorderColor(BaseColor.BLACK);
			cellParent5.setPaddingTop(5);
			cellParent5.setPaddingBottom(5);
			cellParent5.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent5.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell cellParent6 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalAdjcheck), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent6.setBorderColor(BaseColor.BLACK);
			cellParent6.setPaddingTop(5);
			cellParent6.setPaddingBottom(5);
			cellParent6.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellParent6.setVerticalAlignment(Element.ALIGN_RIGHT);
			
			PdfPCell cellParent7 = new PdfPCell(new Paragraph(new Phrase("Difference", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent7.setBorderColor(BaseColor.BLACK);
			cellParent7.setPaddingTop(5);
			cellParent7.setPaddingBottom(5);
			cellParent7.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent7.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell cellParent8 = new PdfPCell(new Paragraph(new Phrase(Double.toString((sumOfCheck+totalAdjcheck) - (totalDiscount+totalRental)), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent8.setBorderColor(BaseColor.BLACK);
			cellParent8.setPaddingTop(5);
			cellParent8.setPaddingBottom(5);
			cellParent8.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellParent8.setVerticalAlignment(Element.ALIGN_RIGHT);
			tableParent.addCell(cellParent1);
			tableParent.addCell(cellParent2);
			tableParent.addCell(cellParent3);
			tableParent.addCell(cellParent4);
			tableParent.addCell(cellParent5);
			tableParent.addCell(cellParent6);
			tableParent.addCell(cellParent7);
			tableParent.addCell(cellParent8);
			document.add(tableParent);

			PdfPTable table = new PdfPTable(5); // 3 columns.
			table.setWidthPercentage(100); //Width 100%
			table.setSpacingBefore(10f); //Space before table
			table.setSpacingAfter(10f); //Space after table
            
			//document.add(new Paragraph("Company Wise Discount ("+companyName+")", FontFactory.getFont(FontFactory.HELVETICA, 12,Font.BOLD)));
			
			//Set Column widths
			float[] columnWidths = {1f, 1f, 1f, 1f, 1f};
			table.setWidths(columnWidths);
			
			PdfPCell cell1 = new PdfPCell(new Paragraph(new Phrase("Transaction Date", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			cell1.setBorderColor(BaseColor.BLACK);
			cell1.setPaddingTop(5);
			cell1.setPaddingBottom(5);
			cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell1.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell cell2 = new PdfPCell(new Paragraph(new Phrase("Bank", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			cell2.setBorderColor(BaseColor.BLACK);
			cell2.setPaddingTop(5);
			cell2.setPaddingBottom(5);
			cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell2.setBackgroundColor(BaseColor.ORANGE);

			PdfPCell cell3 = new PdfPCell(new Paragraph(new Phrase("Transaction Type", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			cell3.setBorderColor(BaseColor.BLACK);
			cell3.setPaddingTop(5);
			cell3.setPaddingBottom(5);
			cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell3.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell cell5 = new PdfPCell(new Paragraph(new Phrase("Cheque No/Reference", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			cell5.setBorderColor(BaseColor.BLACK);
			cell5.setPaddingTop(5);
			cell5.setPaddingBottom(5);
			cell5.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell5.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell cell4 = new PdfPCell(new Paragraph(new Phrase("Cheque Amount", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			cell4.setBorderColor(BaseColor.BLACK);
			cell4.setPaddingTop(5);
			cell4.setPaddingBottom(5);
			cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cell4.setBackgroundColor(BaseColor.ORANGE);
      
			table.addCell(cell1);
			table.addCell(cell2);
			table.addCell(cell3);
			table.addCell(cell5);
			table.addCell(cell4);
			for(DiscountAsPerCompanyBeen bean : discountAsPerCompanyBeenList){
				//totalAdjcheck = totalAdjcheck+bean.getAdjustmentCheque();
			//	sumOfCheck = sumOfCheck+bean.getChequeAmount();
				//totalDiscount = bean.getDiscountAmount();
				//totalRental = bean.getRentals();
				
				PdfPCell cell11 = new PdfPCell(new Paragraph(new Phrase(bean.getEnterprise(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell11.setBorderColor(BaseColor.BLACK);
				cell11.setPaddingTop(5);
				cell11.setPaddingBottom(5);
				cell11.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell21 = new PdfPCell(new Paragraph(new Phrase(bean.getCompanyName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell21.setBorderColor(BaseColor.BLACK);
				cell21.setPaddingTop(5);
				cell21.setPaddingBottom(5);
				cell21.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell21.setVerticalAlignment(Element.ALIGN_MIDDLE);

				PdfPCell cell31 = new PdfPCell(new Paragraph(new Phrase(bean.getComment(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell31.setBorderColor(BaseColor.BLACK);
				cell31.setPaddingTop(5);
				cell31.setPaddingBottom(5);
				cell31.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell31.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell51 = new PdfPCell(new Paragraph(new Phrase(bean.getColor(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell51.setBorderColor(BaseColor.BLACK);
				cell51.setPaddingTop(5);
				cell51.setPaddingBottom(5);
				cell51.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell51.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell41 = new PdfPCell(new Paragraph(new Phrase(Double.toString(bean.getChequeAmount()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell41.setBorderColor(BaseColor.BLACK);
				cell41.setPaddingTop(5);
				cell41.setPaddingBottom(5);
				cell41.setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell41.setVerticalAlignment(Element.ALIGN_RIGHT);
	      
				table.addCell(cell11);
				table.addCell(cell21);
				table.addCell(cell31);
				table.addCell(cell51);
				table.addCell(cell41);
	 		}
			PdfPCell cellreceived1 = new PdfPCell(new Paragraph(new Phrase("Total Cheques Received", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			cellreceived1.setBorderColor(BaseColor.BLACK);
			cellreceived1.setPaddingTop(5);
			cellreceived1.setPaddingBottom(5);
			cellreceived1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellreceived1.setVerticalAlignment(Element.ALIGN_RIGHT);
			cellreceived1.setColspan(4);
			PdfPCell cellreceived2 = new PdfPCell(new Paragraph(new Phrase(Double.toString(sumOfCheck), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			cellreceived2.setBorderColor(BaseColor.BLACK);
			cellreceived2.setPaddingTop(5);
			cellreceived2.setPaddingBottom(5);
			cellreceived2.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellreceived2.setVerticalAlignment(Element.ALIGN_RIGHT);
			
			table.addCell(cellreceived1);
			table.addCell(cellreceived2);
			
			/*PdfPCell cellDiscountRental1 = new PdfPCell(new Paragraph(new Phrase("Rentals + Total Discount", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			cellDiscountRental1.setBorderColor(BaseColor.BLACK);
			cellDiscountRental1.setPaddingTop(5);
			cellDiscountRental1.setPaddingBottom(5);
			cellDiscountRental1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellDiscountRental1.setVerticalAlignment(Element.ALIGN_RIGHT);
			cellDiscountRental1.setColspan(4);
			PdfPCell cellDiscountRental2 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalDiscount+totalRental), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			cellDiscountRental2.setBorderColor(BaseColor.BLACK);
			cellDiscountRental2.setPaddingTop(5);
			cellDiscountRental2.setPaddingBottom(5);
			cellDiscountRental2.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellDiscountRental2.setVerticalAlignment(Element.ALIGN_RIGHT);
			
			table.addCell(cellDiscountRental1);
			table.addCell(cellDiscountRental2);
			
			PdfPCell celladj1 = new PdfPCell(new Paragraph(new Phrase("Cheque Adjustments", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			celladj1.setBorderColor(BaseColor.BLACK);
			celladj1.setPaddingTop(5);
			celladj1.setPaddingBottom(5);
			celladj1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			celladj1.setVerticalAlignment(Element.ALIGN_RIGHT);
			celladj1.setColspan(4);
			PdfPCell celladj2 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalAdjcheck), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			celladj2.setBorderColor(BaseColor.BLACK);
			celladj2.setPaddingTop(5);
			celladj2.setPaddingBottom(5);
			celladj2.setHorizontalAlignment(Element.ALIGN_RIGHT);
			celladj2.setVerticalAlignment(Element.ALIGN_RIGHT);
			
			table.addCell(celladj1);
			table.addCell(celladj2);
			
			PdfPCell cellDifference1 = new PdfPCell(new Paragraph(new Phrase("Difference", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			cellDifference1.setBorderColor(BaseColor.BLACK);
			cellDifference1.setPaddingTop(5);
			cellDifference1.setPaddingBottom(5);
			cellDifference1.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellDifference1.setVerticalAlignment(Element.ALIGN_RIGHT);
			cellDifference1.setColspan(4);
			PdfPCell cellDifference2 = new PdfPCell(new Paragraph(new Phrase((Double.toString((sumOfCheck+totalAdjcheck) - (totalDiscount+totalRental))), FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			cellDifference2.setBorderColor(BaseColor.BLACK);
			cellDifference2.setPaddingTop(5);
			cellDifference2.setPaddingBottom(5);
			cellDifference2.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellDifference2.setVerticalAlignment(Element.ALIGN_RIGHT);
			
			table.addCell(cellDifference1);
			table.addCell(cellDifference2);*/
			document.add(table);

			// Discount Table
			PdfPTable discounttable = new PdfPTable(4); // 3 columns.
			discounttable.setWidthPercentage(100); //Width 100%
			discounttable.setSpacingBefore(10f); //Space before table
			discounttable.setSpacingAfter(10f); //Space after table
            
			//Set Column widths
			float[] columnWidthsDiscount = {1f, 1f, 1f, 1f};
			discounttable.setWidths(columnWidthsDiscount);
			
			/*PdfPCell discountcell1 = new PdfPCell(new Paragraph(new Phrase("Month", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			discountcell1.setBorderColor(BaseColor.BLACK);
			discountcell1.setPaddingLeft(5);
			discountcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			discountcell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			*/
			PdfPCell discountcell2 = new PdfPCell(new Paragraph(new Phrase("Brand", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			discountcell2.setBorderColor(BaseColor.BLACK);
			discountcell2.setPaddingTop(5);
			discountcell2.setPaddingBottom(5);
			discountcell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			discountcell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			discountcell2.setBackgroundColor(BaseColor.ORANGE);

			PdfPCell discountcell3 = new PdfPCell(new Paragraph(new Phrase("Case", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			discountcell3.setBorderColor(BaseColor.BLACK);
			discountcell3.setPaddingTop(5);
			discountcell3.setPaddingBottom(5);
			discountcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			discountcell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
			discountcell3.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell discountcell4 = new PdfPCell(new Paragraph(new Phrase("Disc/Case", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			discountcell4.setBorderColor(BaseColor.BLACK);
			discountcell4.setPaddingTop(5);
			discountcell4.setPaddingBottom(5);
			discountcell4.setHorizontalAlignment(Element.ALIGN_CENTER);
			discountcell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
			discountcell4.setBackgroundColor(BaseColor.ORANGE);
      
			PdfPCell discountcell5 = new PdfPCell(new Paragraph(new Phrase("Total", FontFactory.getFont(FontFactory.HELVETICA, 10,Font.BOLD))));
			discountcell5.setBorderColor(BaseColor.BLACK);
			discountcell5.setPaddingTop(5);
			discountcell5.setPaddingBottom(5);
			discountcell5.setHorizontalAlignment(Element.ALIGN_CENTER);
			discountcell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
			discountcell5.setBackgroundColor(BaseColor.ORANGE);
      
			//discounttable.addCell(discountcell1);
			discounttable.addCell(discountcell2);
			discounttable.addCell(discountcell3);
			discounttable.addCell(discountcell4);
			discounttable.addCell(discountcell5);
			
			for(DiscountAndMonthBean discountBean : discountTransactionBeanList){
				PdfPCell discountcell11 = new PdfPCell(new Paragraph(new Phrase(discountBean.getMonth(), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
				discountcell11.setBorderColor(BaseColor.BLACK);
				discountcell11.setPaddingTop(5);
				discountcell11.setPaddingBottom(5);
				discountcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
				discountcell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
				discountcell11.setBackgroundColor(BaseColor.LIGHT_GRAY);
				discountcell11.setColspan(5);
				discounttable.addCell(discountcell11);
				for(DiscountMonthWiseBean monthWiseDisc : discountBean.getDiscountMonthWiseBean()){
					
					PdfPCell discountcell21 = new PdfPCell(new Paragraph(new Phrase(monthWiseDisc.getBrand(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					discountcell21.setBorderColor(BaseColor.BLACK);
					discountcell21.setPaddingLeft(5);
					discountcell21.setPaddingTop(5);
					discountcell21.setPaddingBottom(5);
					discountcell21.setHorizontalAlignment(Element.ALIGN_LEFT);
					discountcell21.setVerticalAlignment(Element.ALIGN_LEFT);

					PdfPCell discountcell31 = new PdfPCell(new Paragraph(new Phrase(Long.toString(monthWiseDisc.getCase()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					discountcell31.setBorderColor(BaseColor.BLACK);
					discountcell31.setPaddingTop(5);
					discountcell31.setPaddingBottom(5);
					discountcell31.setHorizontalAlignment(Element.ALIGN_CENTER);
					discountcell31.setVerticalAlignment(Element.ALIGN_MIDDLE);
					
					PdfPCell discountcell41 = new PdfPCell(new Paragraph(new Phrase(Double.toString(monthWiseDisc.getDiscount()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					discountcell41.setBorderColor(BaseColor.BLACK);
					discountcell41.setPaddingTop(5);
					discountcell41.setPaddingBottom(5);
					discountcell41.setHorizontalAlignment(Element.ALIGN_RIGHT);
					discountcell41.setVerticalAlignment(Element.ALIGN_RIGHT);
		      
					PdfPCell discountcell51 = new PdfPCell(new Paragraph(new Phrase(Double.toString(monthWiseDisc.getTotal()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					discountcell51.setBorderColor(BaseColor.BLACK);
					discountcell51.setPaddingTop(5);
					discountcell51.setPaddingBottom(5);
					discountcell51.setHorizontalAlignment(Element.ALIGN_RIGHT);
					discountcell51.setVerticalAlignment(Element.ALIGN_RIGHT);
					
					discounttable.addCell(discountcell21);
					discounttable.addCell(discountcell31);
					discounttable.addCell(discountcell41);
					discounttable.addCell(discountcell51);
				}
				PdfPCell discountSumcell11 = new PdfPCell(new Paragraph(new Phrase("Total", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
				discountSumcell11.setBorderColor(BaseColor.BLACK);
				discountSumcell11.setPaddingTop(5);
				discountSumcell11.setPaddingBottom(5);
				discountSumcell11.setHorizontalAlignment(Element.ALIGN_RIGHT);
				discountSumcell11.setVerticalAlignment(Element.ALIGN_RIGHT);
				discountSumcell11.setColspan(3);
				discounttable.addCell(discountSumcell11);
				
				PdfPCell discountSumcell12 = new PdfPCell(new Paragraph(new Phrase(Double.toString(discountBean.getTotalDiscount()), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
				discountSumcell12.setBorderColor(BaseColor.BLACK);
				discountSumcell11.setPaddingTop(5);
				discountSumcell11.setPaddingBottom(5);
				discountSumcell12.setHorizontalAlignment(Element.ALIGN_RIGHT);
				discountSumcell12.setVerticalAlignment(Element.ALIGN_RIGHT);
				discounttable.addCell(discountSumcell12);
				
			}
			PdfPCell discountTotalcell11 = new PdfPCell(new Paragraph(new Phrase("Grand Total Discount", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			discountTotalcell11.setBorderColor(BaseColor.BLACK);
			discountTotalcell11.setPaddingTop(5);
			discountTotalcell11.setPaddingBottom(5);
			discountTotalcell11.setHorizontalAlignment(Element.ALIGN_RIGHT);
			discountTotalcell11.setVerticalAlignment(Element.ALIGN_RIGHT);
			discountTotalcell11.setColspan(3);
			discounttable.addCell(discountTotalcell11);
			
			PdfPCell discountTotalcell12 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalDiscount), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			discountTotalcell12.setBorderColor(BaseColor.BLACK);
			discountTotalcell12.setPaddingTop(5);
			discountTotalcell12.setPaddingBottom(5);
			discountTotalcell12.setHorizontalAlignment(Element.ALIGN_RIGHT);
			discountTotalcell12.setVerticalAlignment(Element.ALIGN_RIGHT);
			discounttable.addCell(discountTotalcell12);
			
			PdfPCell discountRentalcell11 = new PdfPCell(new Paragraph(new Phrase("Rentals", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			discountRentalcell11.setBorderColor(BaseColor.BLACK);
			discountRentalcell11.setPaddingTop(5);
			discountRentalcell11.setPaddingBottom(5);
			discountRentalcell11.setHorizontalAlignment(Element.ALIGN_RIGHT);
			discountRentalcell11.setVerticalAlignment(Element.ALIGN_RIGHT);
			discountRentalcell11.setColspan(3);
			discounttable.addCell(discountRentalcell11);
			
			PdfPCell discountRentalcell12 = new PdfPCell(new Paragraph(new Phrase(Double.toString(totalRental), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			discountRentalcell12.setBorderColor(BaseColor.BLACK);
			discountRentalcell12.setPaddingTop(5);
			discountRentalcell12.setPaddingBottom(5);
			discountRentalcell12.setHorizontalAlignment(Element.ALIGN_RIGHT);
			discountRentalcell12.setVerticalAlignment(Element.ALIGN_RIGHT);
			discounttable.addCell(discountRentalcell12);
			
			document.add(discounttable);
			document.close();
			writer.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
 		return true;
    }
    /**
     * This method is used to download sale sheet excluding zero stock
     * */
    @RequestMapping(value = "/downloadSaleDetailsForClosing", method = RequestMethod.GET,produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody void downloadSaleDetailsForClosing(HttpServletRequest request, HttpServletResponse response) throws Exception {
		boolean value = getSaleDetailsForDownload(request,response);
		if(value)
		CommonUtil.downloadZipFile(request,response,poizinPdfFile+"/Sale_Sheet.pdf");
    }
    
    public boolean getSaleDetailsForDownload(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		System.out.println("getSaleDetailsForDownload");
		SaleListBean saleDeatils = null;
		saleDeatils = poizonMobileUIService.getSaleDetailsForClosing();
		int listSize = (saleDeatils.getSaleBean().size()/2)+2;
		System.out.println("listSize>>"+listSize);
        String pdfResult = poizinPdfFile+"/Sale_Sheet.pdf";
    	
    	Document document = new Document(PageSize.A3);
 		try
		{
 			float cheapOpening = 0,cheapReceived = 0,cheapMrp=0;
 			String cheapName="",packQtyCheap="";
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfResult));
			document.open();
			
			/*Paragraph header = new Paragraph();
			Font f = new Font(FontFamily.UNDEFINED, 20.0f, Font.NORMAL, new BaseColor(36,58,81));
			header.setAlignment(Element.ALIGN_RIGHT);
			Chunk chunk = new Chunk("POIZIN",f);
			header.add(chunk);
			document.add(header);*/
			
			PdfPTable table = new PdfPTable(2); // 3 columns.
			table.setWidthPercentage(108); //Width 100%
			table.setSpacingBefore(10f); //Space before table
			table.setSpacingAfter(10f); //Space after table
            
			//Set Column widths
			float[] columnWidths = {1f, 1f};
			table.setWidths(columnWidths);
			
			PdfPCell cell1 = new PdfPCell(new Paragraph(new Phrase("FIST COLUMN")));
			cell1.setBorderColor(BaseColor.BLACK);
			PdfPTable nestedTable  = new PdfPTable(9);
			nestedTable.setWidthPercentage(100);
			
			float[] nestedcolumnWidths = {2f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f};
			nestedTable.setWidths(nestedcolumnWidths);
			PdfPCell tablecell1 = new PdfPCell(new Paragraph(".", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD)));
			tablecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			tablecell1.setVerticalAlignment(Element.ALIGN_LEFT);
			tablecell1.setPaddingTop(5);
			tablecell1.setPaddingBottom(5);
			tablecell1.setColspan(9);
			tablecell1.setBorder(0);
			
			PdfPCell nestedcell1 = new PdfPCell(new Paragraph(new Phrase("NAME", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell1.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell2 = new PdfPCell(new Paragraph(new Phrase("PT", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell2.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell3 = new PdfPCell(new Paragraph(new Phrase("OB", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell3.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell4 = new PdfPCell(new Paragraph(new Phrase("REC", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell4.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell4.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell5 = new PdfPCell(new Paragraph(new Phrase("TTL", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell5.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell5.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell6 = new PdfPCell(new Paragraph(new Phrase("CB", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell6.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell6.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell7 = new PdfPCell(new Paragraph(new Phrase("SALE", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell7.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell7.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell8 = new PdfPCell(new Paragraph(new Phrase("MRP", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell8.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell8.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell9 = new PdfPCell(new Paragraph(new Phrase("TOTAL", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell9.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell9.setVerticalAlignment(Element.ALIGN_CENTER);
			
			nestedTable.addCell(tablecell1);
			nestedTable.addCell(nestedcell1);
			nestedTable.addCell(nestedcell2);
			nestedTable.addCell(nestedcell3);
			nestedTable.addCell(nestedcell4);
			nestedTable.addCell(nestedcell5);
			nestedTable.addCell(nestedcell6);
			nestedTable.addCell(nestedcell7);
			nestedTable.addCell(nestedcell8);
			nestedTable.addCell(nestedcell9);
					      
		    
 		for (int i = 0; i < listSize; i++) { 
 			if((saleDeatils.getSaleBean().get(i).getBrandNo() != 672) && (saleDeatils.getSaleBean().get(i).getBrandNo() != 670) && (saleDeatils.getSaleBean().get(i).getBrandNo() != 680) 
 					&& (saleDeatils.getSaleBean().get(i).getBrandNo() != 673) && (saleDeatils.getSaleBean().get(i).getBrandNo() != 684) && (saleDeatils.getSaleBean().get(i).getBrandNo() != 671)
 					&& (saleDeatils.getSaleBean().get(i).getBrandNo() != 677) && (saleDeatils.getSaleBean().get(i).getBrandNo() != 699)){
 			PdfPCell nestedcell11 =null, nestedcell41=null, nestedcell51=null;
 			if(saleDeatils.getSaleBean().get(i).isConditonVal() == true){
				 nestedcell11 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell11.setBorder(PdfPCell.NO_BORDER);
				//nestedcell11.setCellEvent(new SolidBorder(PdfPCell.TOP));
				nestedcell11.setCellEvent(new SolidBorder(PdfPCell.LEFT));
 			}
 			else if(saleDeatils.getSaleBean().get(i).isConditonVal() == false && saleDeatils.getSaleBean().get(i).getNoOfReturnsBtl()==1){
				 nestedcell11 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getBrandName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell11.setBorder(PdfPCell.NO_BORDER);
				nestedcell11.setCellEvent(new SolidBorder(PdfPCell.TOP));
				nestedcell11.setCellEvent(new SolidBorder(PdfPCell.LEFT));
				nestedcell11.setPaddingTop(5);
				nestedcell11.setPaddingBottom(5);
				nestedcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell11.setVerticalAlignment(Element.ALIGN_CENTER);
 			}
 			else{
 				 nestedcell11 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getBrandName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell11.setBorder(PdfPCell.NO_BORDER);
				nestedcell11.setCellEvent(new SolidBorder(PdfPCell.TOP));
				nestedcell11.setCellEvent(new SolidBorder(PdfPCell.LEFT));
				nestedcell11.setPaddingTop(5);
				nestedcell11.setPaddingBottom(5);
				nestedcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell11.setVerticalAlignment(Element.ALIGN_CENTER);
 			}
				
				PdfPCell nestedcell21 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getPackType(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell21.setPaddingTop(5);
				nestedcell21.setPaddingBottom(5);
				nestedcell21.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell21.setVerticalAlignment(Element.ALIGN_CENTER);
				
				PdfPCell nestedcell31 = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getOpening()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell31.setPaddingTop(5);
				nestedcell31.setPaddingBottom(5);
				nestedcell31.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell31.setVerticalAlignment(Element.ALIGN_CENTER);
				
				if(saleDeatils.getSaleBean().get(i).getReceived() > 1){
				 nestedcell41 = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getReceived()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				 nestedcell41.setPaddingTop(5);
				 nestedcell41.setPaddingBottom(5);
				 nestedcell41.setHorizontalAlignment(Element.ALIGN_CENTER);
				 nestedcell41.setVerticalAlignment(Element.ALIGN_CENTER);
				 nestedcell51  = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getOpening()+saleDeatils.getSaleBean().get(i).getReceived()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				 nestedcell51.setPaddingTop(5);
				 nestedcell51.setPaddingBottom(5);
				 nestedcell51.setHorizontalAlignment(Element.ALIGN_CENTER);
				 nestedcell51.setVerticalAlignment(Element.ALIGN_CENTER);
				}else{
			     nestedcell41 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));	
			     nestedcell51  = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				}
				PdfPCell nestedcell61 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				PdfPCell nestedcell71 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				PdfPCell nestedcell81 = new PdfPCell(new Paragraph(new Phrase(Integer.toString((saleDeatils.getSaleBean().get(i).getUnitPrice()).intValue()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell81.setPaddingTop(5);
				nestedcell81.setPaddingBottom(5);
				nestedcell81.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell81.setVerticalAlignment(Element.ALIGN_CENTER);
				PdfPCell nestedcell91 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				
				nestedTable.addCell(nestedcell11);
				nestedTable.addCell(nestedcell21);
				nestedTable.addCell(nestedcell31);
				nestedTable.addCell(nestedcell41);
				nestedTable.addCell(nestedcell51);
				nestedTable.addCell(nestedcell61);
				nestedTable.addCell(nestedcell71);
				nestedTable.addCell(nestedcell81);
				nestedTable.addCell(nestedcell91);
 		       }else
 		       {
 		    	  cheapOpening += saleDeatils.getSaleBean().get(i).getOpening();
 		    	  cheapReceived += saleDeatils.getSaleBean().get(i).getReceived();
 		    	  cheapMrp = (saleDeatils.getSaleBean().get(i).getUnitPrice()).longValue();
 		    	  cheapName = saleDeatils.getSaleBean().get(i).getBrandName();
 		    	 packQtyCheap = saleDeatils.getSaleBean().get(i).getPackType();
 		       }
			}
			
			cell1.addElement(nestedTable);
			cell1.setBorder(0);
			
			//cell1.setBorder(0);
			PdfPCell cell2 = new PdfPCell(new Paragraph(new Phrase("SECOND COLUMN")));
			cell2.setBorderColor(BaseColor.BLACK);
			PdfPTable nestedRightTable  = new PdfPTable(9);
			nestedRightTable.setWidthPercentage(100);
			
			float[] nestedRightcolumnWidths = {2f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f};
			nestedRightTable.setWidths(nestedRightcolumnWidths);
			PdfPCell tablecell2 = new PdfPCell(new Paragraph(new Phrase("Date: "+saleDeatils.getDate(), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			tablecell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
			tablecell2.setVerticalAlignment(Element.ALIGN_RIGHT);
			tablecell2.setPaddingTop(5);
			tablecell2.setPaddingBottom(5);
			tablecell2.setColspan(9);
			tablecell2.setBorder(0);
			PdfPCell nestedRightcell1 = new PdfPCell(new Paragraph(new Phrase("NAME", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell1.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell2 = new PdfPCell(new Paragraph(new Phrase("PT", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell2.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell3 = new PdfPCell(new Paragraph(new Phrase("OB", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell3.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell4 = new PdfPCell(new Paragraph(new Phrase("REC", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell4.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell4.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell5 = new PdfPCell(new Paragraph(new Phrase("TTL", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell5.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell5.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell6 = new PdfPCell(new Paragraph(new Phrase("CB", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell6.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell6.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell7 = new PdfPCell(new Paragraph(new Phrase("SALE", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell7.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell7.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell8 = new PdfPCell(new Paragraph(new Phrase("MRP", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell8.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell8.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell9 = new PdfPCell(new Paragraph(new Phrase("TOTAL", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell9.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell9.setVerticalAlignment(Element.ALIGN_CENTER);
			
			nestedRightTable.addCell(tablecell2);
			nestedRightTable.addCell(nestedRightcell1);
			nestedRightTable.addCell(nestedRightcell2);
			nestedRightTable.addCell(nestedRightcell3);
			nestedRightTable.addCell(nestedRightcell4);
			nestedRightTable.addCell(nestedRightcell5);
			nestedRightTable.addCell(nestedRightcell6);
			nestedRightTable.addCell(nestedRightcell7);
			nestedRightTable.addCell(nestedRightcell8);
			nestedRightTable.addCell(nestedRightcell9);
					      
		    
 		for (int i = listSize; i < saleDeatils.getSaleBean().size(); i++) { 
 			if((saleDeatils.getSaleBean().get(i).getBrandNo() != 672) && (saleDeatils.getSaleBean().get(i).getBrandNo() != 670) && (saleDeatils.getSaleBean().get(i).getBrandNo() != 680) 
 					&& (saleDeatils.getSaleBean().get(i).getBrandNo() != 673) && (saleDeatils.getSaleBean().get(i).getBrandNo() != 684) && (saleDeatils.getSaleBean().get(i).getBrandNo() != 671)
 					&& (saleDeatils.getSaleBean().get(i).getBrandNo() != 677) && (saleDeatils.getSaleBean().get(i).getBrandNo() != 699)){
 			PdfPCell nestedRightcell11 =null, nestedRightcell41=null, nestedRightcell51=null;
 			if(saleDeatils.getSaleBean().get(i).isConditonVal() == true){
 				nestedRightcell11 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9))));
				 nestedRightcell11.setBorder(PdfPCell.NO_BORDER);
				//nestedcell11.setCellEvent(new SolidBorder(PdfPCell.TOP));
				 nestedRightcell11.setCellEvent(new SolidBorder(PdfPCell.LEFT));
 			}
 			else if(saleDeatils.getSaleBean().get(i).isConditonVal() == false && saleDeatils.getSaleBean().get(i).getNoOfReturnsBtl()==1){
 				nestedRightcell11 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getBrandName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
 				nestedRightcell11.setBorder(PdfPCell.NO_BORDER);
 				nestedRightcell11.setCellEvent(new SolidBorder(PdfPCell.TOP));
 				nestedRightcell11.setCellEvent(new SolidBorder(PdfPCell.LEFT));
 				nestedRightcell11.setPaddingTop(5);
 				nestedRightcell11.setPaddingBottom(5);
 				nestedRightcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedRightcell11.setVerticalAlignment(Element.ALIGN_CENTER);
 			}
 			else{
 				nestedRightcell11 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getBrandName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
 				nestedRightcell11.setBorder(PdfPCell.NO_BORDER);
 				nestedRightcell11.setCellEvent(new SolidBorder(PdfPCell.TOP));
 				nestedRightcell11.setCellEvent(new SolidBorder(PdfPCell.LEFT));
 				nestedRightcell11.setPaddingTop(5);
 				nestedRightcell11.setPaddingBottom(5);
 				nestedRightcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
 				nestedRightcell11.setVerticalAlignment(Element.ALIGN_CENTER);
 			}
				
				PdfPCell nestedRightcell21 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getPackType(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedRightcell21.setPaddingTop(5);
				nestedRightcell21.setPaddingBottom(5);
				nestedRightcell21.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedRightcell21.setVerticalAlignment(Element.ALIGN_CENTER);
				
				PdfPCell nestedRightcell31 = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getOpening()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedRightcell31.setPaddingTop(5);
				nestedRightcell31.setPaddingBottom(5);
				nestedRightcell31.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedRightcell31.setVerticalAlignment(Element.ALIGN_CENTER);
				
				if(saleDeatils.getSaleBean().get(i).getReceived() > 1){
					nestedRightcell41 = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getReceived()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					nestedRightcell41.setPaddingTop(5);
					nestedRightcell41.setPaddingBottom(5);
					nestedRightcell41.setHorizontalAlignment(Element.ALIGN_CENTER);
					nestedRightcell41.setVerticalAlignment(Element.ALIGN_CENTER);
					nestedRightcell51  = new PdfPCell(new Paragraph(new Phrase(Long.toString(saleDeatils.getSaleBean().get(i).getOpening()+saleDeatils.getSaleBean().get(i).getReceived()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
					nestedRightcell51.setPaddingTop(5);
					nestedRightcell51.setPaddingBottom(5);
					nestedRightcell51.setHorizontalAlignment(Element.ALIGN_CENTER);
					nestedRightcell51.setVerticalAlignment(Element.ALIGN_CENTER);
				}else{
					nestedRightcell41 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));	
					nestedRightcell51  = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				}
				PdfPCell nestedRightcell61 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				PdfPCell nestedRightcell71 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				PdfPCell nestedRightcell81 = new PdfPCell(new Paragraph(new Phrase(Integer.toString((saleDeatils.getSaleBean().get(i).getUnitPrice()).intValue()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedRightcell81.setPaddingTop(5);
				nestedRightcell81.setPaddingBottom(5);
				nestedRightcell81.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedRightcell81.setVerticalAlignment(Element.ALIGN_CENTER);
				PdfPCell nestedRightcell91 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				
				nestedRightTable.addCell(nestedRightcell11);
				nestedRightTable.addCell(nestedRightcell21);
				nestedRightTable.addCell(nestedRightcell31);
				nestedRightTable.addCell(nestedRightcell41);
				nestedRightTable.addCell(nestedRightcell51);
				nestedRightTable.addCell(nestedRightcell61);
				nestedRightTable.addCell(nestedRightcell71);
				nestedRightTable.addCell(nestedRightcell81);
				nestedRightTable.addCell(nestedRightcell91);
			  }else
		       {
		    	  cheapOpening += saleDeatils.getSaleBean().get(i).getOpening();
		    	  cheapReceived += saleDeatils.getSaleBean().get(i).getReceived();
		    	  cheapMrp = (saleDeatils.getSaleBean().get(i).getUnitPrice()).longValue();
		    	  cheapName = saleDeatils.getSaleBean().get(i).getBrandName();
		    	  packQtyCheap = saleDeatils.getSaleBean().get(i).getPackType();
		       }
 			if(i == saleDeatils.getSaleBean().size()-1){
 				PdfPCell nestedRightcellcheap11 = new PdfPCell(new Paragraph(new Phrase("CHEAP", FontFactory.getFont(FontFactory.HELVETICA, 9))));
 				nestedRightcellcheap11.setBorder(PdfPCell.NO_BORDER);
 				nestedRightcellcheap11.setCellEvent(new SolidBorder(PdfPCell.TOP));
 				nestedRightcellcheap11.setCellEvent(new SolidBorder(PdfPCell.BOTTOM));
 				nestedRightcellcheap11.setCellEvent(new SolidBorder(PdfPCell.LEFT));
 				nestedRightcellcheap11.setPaddingTop(5);
 				nestedRightcellcheap11.setPaddingBottom(5);
 				nestedRightcellcheap11.setHorizontalAlignment(Element.ALIGN_CENTER);
 				nestedRightcellcheap11.setVerticalAlignment(Element.ALIGN_CENTER);
 				
 				PdfPCell nestedRightcellcheap21 = new PdfPCell(new Paragraph(new Phrase(packQtyCheap, FontFactory.getFont(FontFactory.HELVETICA, 9))));
 				nestedRightcellcheap21.setCellEvent(new SolidBorder(PdfPCell.TOP));
 				nestedRightcellcheap21.setCellEvent(new SolidBorder(PdfPCell.LEFT));
 				nestedRightcellcheap21.setPaddingTop(5);
 				nestedRightcellcheap21.setPaddingBottom(5);
 				nestedRightcellcheap21.setHorizontalAlignment(Element.ALIGN_CENTER);
 				nestedRightcellcheap21.setVerticalAlignment(Element.ALIGN_CENTER);
 				
 				PdfPCell nestedRightcellcheap31 = new PdfPCell(new Paragraph(new Phrase(Long.toString((long) cheapOpening), FontFactory.getFont(FontFactory.HELVETICA, 9))));
 				nestedRightcellcheap31.setCellEvent(new SolidBorder(PdfPCell.TOP));
 				nestedRightcellcheap31.setCellEvent(new SolidBorder(PdfPCell.LEFT));
 				nestedRightcellcheap31.setPaddingTop(5);
 				nestedRightcellcheap31.setPaddingBottom(5);
 				nestedRightcellcheap31.setHorizontalAlignment(Element.ALIGN_CENTER);
 				nestedRightcellcheap31.setVerticalAlignment(Element.ALIGN_CENTER);
 				
 				PdfPCell nestedRightcellcheap41,nestedRightcellcheap51;
 				if(cheapReceived > 0){
 					nestedRightcellcheap41 = new PdfPCell(new Paragraph(new Phrase(Long.toString((long) cheapReceived), FontFactory.getFont(FontFactory.HELVETICA, 9))));
 					nestedRightcellcheap41.setCellEvent(new SolidBorder(PdfPCell.TOP));
 					nestedRightcellcheap41.setCellEvent(new SolidBorder(PdfPCell.LEFT));
 					nestedRightcellcheap41.setPaddingTop(5);
 					nestedRightcellcheap41.setPaddingBottom(5);
 					nestedRightcellcheap41.setHorizontalAlignment(Element.ALIGN_CENTER);
 					nestedRightcellcheap41.setVerticalAlignment(Element.ALIGN_CENTER);
 					nestedRightcellcheap51  = new PdfPCell(new Paragraph(new Phrase(Long.toString((long) (cheapReceived+cheapOpening)), FontFactory.getFont(FontFactory.HELVETICA, 9))));
 					nestedRightcellcheap51.setCellEvent(new SolidBorder(PdfPCell.TOP));
 					nestedRightcellcheap51.setCellEvent(new SolidBorder(PdfPCell.LEFT));
 					nestedRightcellcheap51.setPaddingTop(5);
 					nestedRightcellcheap51.setPaddingBottom(5);
 					nestedRightcellcheap51.setHorizontalAlignment(Element.ALIGN_CENTER);
 					nestedRightcellcheap51.setVerticalAlignment(Element.ALIGN_CENTER);
 				}else{
 					nestedRightcellcheap41	= new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));	
 					nestedRightcellcheap51	= new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));	
 				}
 				PdfPCell nestedRightcellcheap61 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
 				PdfPCell nestedRightcellcheap71 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				PdfPCell nestedRightcellcheap81 = new PdfPCell(new Paragraph(new Phrase(Integer.toString((int) (cheapMrp)), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedRightcellcheap81.setPaddingTop(5);
				nestedRightcellcheap81.setPaddingBottom(5);
				nestedRightcellcheap81.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedRightcellcheap81.setVerticalAlignment(Element.ALIGN_CENTER);
				PdfPCell nestedRightcellcheap91 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				nestedRightTable.addCell(nestedRightcellcheap11);
				nestedRightTable.addCell(nestedRightcellcheap21);
				nestedRightTable.addCell(nestedRightcellcheap31);
				nestedRightTable.addCell(nestedRightcellcheap41);
				nestedRightTable.addCell(nestedRightcellcheap51);
				nestedRightTable.addCell(nestedRightcellcheap61);
				nestedRightTable.addCell(nestedRightcellcheap71);
				nestedRightTable.addCell(nestedRightcellcheap81);
				nestedRightTable.addCell(nestedRightcellcheap91);
 			}
		  }
			cell2.addElement(nestedRightTable);
			cell2.setBorder(0);
			table.addCell(cell1);
			table.addCell(cell2);
			document.add(table);
			// For expenses
			PdfPTable expensetable = new PdfPTable(2); // 3 columns.
			expensetable.setWidthPercentage(108); //Width 100%
			expensetable.setSpacingBefore(20f); //Space before table
			expensetable.setSpacingAfter(10f);
			
			float[] expensecolumnWidths = {1f, 1f};
			expensetable.setWidths(expensecolumnWidths);
			PdfPCell expensecell1 = new PdfPCell(new Paragraph(new Phrase("Expenses")));
			
			PdfPTable nestedExpenseTable  = new PdfPTable(2);
			nestedExpenseTable.setWidthPercentage(100);
			float[] nestedExpensecolumnWidths = {2f, 1f};
			nestedExpenseTable.setWidths(nestedExpensecolumnWidths);
			PdfPCell expenseNestedcell1 = new PdfPCell(new Paragraph(new Phrase("Expenses", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			expenseNestedcell1.setPaddingTop(5);
			expenseNestedcell1.setPaddingBottom(5);
			PdfPCell expenseNestedcell2 = new PdfPCell(new Paragraph(new Phrase("Amount", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			expenseNestedcell2.setPaddingTop(5);
			expenseNestedcell2.setPaddingBottom(5);
			
			PdfPCell expenseNestedcell3 = new PdfPCell(new Paragraph(new Phrase("1)")));
			expenseNestedcell3.setPaddingTop(5);
			expenseNestedcell3.setPaddingBottom(5);
			PdfPCell expenseNestedcell4 = new PdfPCell(new Paragraph(new Phrase("")));
			expenseNestedcell4.setPaddingTop(5);
			expenseNestedcell4.setPaddingBottom(5);
			PdfPCell expenseNestedcell5 = new PdfPCell(new Paragraph(new Phrase("2)")));
			expenseNestedcell5.setPaddingTop(5);
			expenseNestedcell5.setPaddingBottom(5);
			PdfPCell expenseNestedcell6 = new PdfPCell(new Paragraph(new Phrase("")));
			expenseNestedcell6.setPaddingTop(5);
			expenseNestedcell6.setPaddingBottom(5);
			PdfPCell expenseNestedcell7 = new PdfPCell(new Paragraph(new Phrase("3)")));
			expenseNestedcell7.setPaddingTop(5);
			expenseNestedcell7.setPaddingBottom(5);
			PdfPCell expenseNestedcell8 = new PdfPCell(new Paragraph(new Phrase("")));
			expenseNestedcell8.setPaddingTop(5);
			expenseNestedcell8.setPaddingBottom(5);
			PdfPCell expenseNestedcell9 = new PdfPCell(new Paragraph(new Phrase("4)")));
			expenseNestedcell9.setPaddingTop(5);
			expenseNestedcell9.setPaddingBottom(5);
			PdfPCell expenseNestedcell10 = new PdfPCell(new Paragraph(new Phrase("")));
			expenseNestedcell10.setPaddingTop(5);
			expenseNestedcell10.setPaddingBottom(5);
			PdfPCell expenseNestedcell11 = new PdfPCell(new Paragraph(new Phrase("5)")));
			expenseNestedcell11.setPaddingTop(5);
			expenseNestedcell11.setPaddingBottom(5);
			PdfPCell expenseNestedcell12 = new PdfPCell(new Paragraph(new Phrase("")));
			expenseNestedcell12.setPaddingTop(5);
			expenseNestedcell12.setPaddingBottom(5);
			PdfPCell expenseNestedcell13 = new PdfPCell(new Paragraph(new Phrase("Total")));
			expenseNestedcell13.setPaddingTop(5);
			expenseNestedcell13.setPaddingBottom(5);
			PdfPCell expenseNestedcell14 = new PdfPCell(new Paragraph(new Phrase("")));
			expenseNestedcell14.setPaddingTop(5);
			expenseNestedcell14.setPaddingBottom(5);
			nestedExpenseTable.addCell(expenseNestedcell1);
			nestedExpenseTable.addCell(expenseNestedcell2);
			nestedExpenseTable.addCell(expenseNestedcell3);
			nestedExpenseTable.addCell(expenseNestedcell4);
			nestedExpenseTable.addCell(expenseNestedcell5);
			nestedExpenseTable.addCell(expenseNestedcell6);
			nestedExpenseTable.addCell(expenseNestedcell7);
			nestedExpenseTable.addCell(expenseNestedcell8);
			nestedExpenseTable.addCell(expenseNestedcell9);
			nestedExpenseTable.addCell(expenseNestedcell10);
			nestedExpenseTable.addCell(expenseNestedcell11);
			nestedExpenseTable.addCell(expenseNestedcell12);
			nestedExpenseTable.addCell(expenseNestedcell13);
			nestedExpenseTable.addCell(expenseNestedcell14);
			
			expensecell1.addElement(nestedExpenseTable);
			expensecell1.setBorder(0);
			expensetable.addCell(expensecell1);
			PdfPCell expensecell2 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			expensecell2.setBorder(0);
			expensetable.addCell(expensecell2);
			document.add(expensetable);
			
			// For Last Table
						PdfPTable lastTable  = new PdfPTable(3);
						lastTable.setWidthPercentage(100);
						float[] lasttablecolumnWidths = {1f, 1f, 1f};
						lastTable.setWidths(lasttablecolumnWidths);
						PdfPCell lasttablecell1 = new PdfPCell(new Paragraph(new Phrase("Sale Sheet", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
						lasttablecell1.setPaddingTop(5);
						lasttablecell1.setPaddingBottom(5);
						PdfPCell lasttablecell2 = new PdfPCell(new Paragraph(new Phrase("Shop Cash", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
						lasttablecell2.setPaddingTop(5);
						lasttablecell2.setPaddingBottom(5);
						PdfPCell lasttablecell3 = new PdfPCell(new Paragraph(new Phrase("Difference", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
						lasttablecell3.setPaddingTop(5);
						lasttablecell3.setPaddingBottom(5);
						
						PdfPCell lasttablecell4 = new PdfPCell(new Paragraph(new Phrase(".")));
						lasttablecell4.setPaddingTop(5);
						lasttablecell4.setPaddingBottom(5);
						PdfPCell lasttablecell5 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell5.setPaddingTop(5);
						lasttablecell5.setPaddingBottom(5);
						PdfPCell lasttablecell6 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell6.setPaddingTop(5);
						lasttablecell6.setPaddingBottom(5);
						
						PdfPCell lasttablecell7 = new PdfPCell(new Paragraph(new Phrase(".")));
						lasttablecell7.setPaddingTop(5);
						lasttablecell7.setPaddingBottom(5);
						PdfPCell lasttablecell8 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell8.setPaddingTop(5);
						lasttablecell8.setPaddingBottom(5);
						PdfPCell lasttablecell9 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell9.setPaddingTop(5);
						lasttablecell9.setPaddingBottom(5);
						
						PdfPCell lasttablecell10 = new PdfPCell(new Paragraph(new Phrase(".")));
						lasttablecell10.setPaddingTop(5);
						lasttablecell10.setPaddingBottom(5);
						PdfPCell lasttablecell11 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell11.setPaddingTop(5);
						lasttablecell11.setPaddingBottom(5);
						PdfPCell lasttablecell12 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell12.setPaddingTop(5);
						lasttablecell12.setPaddingBottom(5);
						
						PdfPCell lasttablecell13 = new PdfPCell(new Paragraph(new Phrase(".")));
						lasttablecell13.setPaddingTop(5);
						lasttablecell13.setPaddingBottom(5);
						PdfPCell lasttablecell14 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell14.setPaddingTop(5);
						lasttablecell14.setPaddingBottom(5);
						PdfPCell lasttablecell15 = new PdfPCell(new Paragraph(new Phrase("")));
						
						PdfPCell lasttablecell16 = new PdfPCell(new Paragraph(new Phrase(".")));
						lasttablecell16.setPaddingTop(5);
						lasttablecell16.setPaddingBottom(5);
						PdfPCell lasttablecell17 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell17.setPaddingTop(5);
						lasttablecell17.setPaddingBottom(5);
						PdfPCell lasttablecell18 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell18.setPaddingTop(5);
						lasttablecell18.setPaddingBottom(5);
						lastTable.addCell(lasttablecell1);
						lastTable.addCell(lasttablecell2);
						lastTable.addCell(lasttablecell3);
						lastTable.addCell(lasttablecell4);
						lastTable.addCell(lasttablecell5);
						lastTable.addCell(lasttablecell6);
						lastTable.addCell(lasttablecell7);
						lastTable.addCell(lasttablecell8);
						lastTable.addCell(lasttablecell9);
						lastTable.addCell(lasttablecell10);
						lastTable.addCell(lasttablecell11);
						lastTable.addCell(lasttablecell12);
						lastTable.addCell(lasttablecell13);
						lastTable.addCell(lasttablecell14);
						lastTable.addCell(lasttablecell15);
						lastTable.addCell(lasttablecell16);
						lastTable.addCell(lasttablecell17);
						lastTable.addCell(lasttablecell18);
						document.add(lastTable);
			
			document.close();
			writer.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
 		System.out.println("Close");
 		return true;
    }
    
    
    /**
     * This method is used for to download sale sheet with including zero sale also.
     * */
    @RequestMapping(value = "/downloadEmptySaleDetailsForClosing", method = RequestMethod.GET,produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody void downloadEmptySaleDetailsForClosing(HttpServletRequest request, HttpServletResponse response) throws Exception {
		boolean value = getEmptySaleDetailsForDownload(request,response);
		if(value)
		CommonUtil.downloadZipFile(request,response,poizinPdfFile+"/Sale_Sheet.pdf");
    }
    
    public boolean getEmptySaleDetailsForDownload(HttpServletRequest request, HttpServletResponse response) throws ParseException {
		SaleListBean saleDeatils = null;
		saleDeatils = poizonMobileUIService.getSaleDetailsForClosing();
		int listSize = (saleDeatils.getSaleBean().size()/2)+2;
        String pdfResult = poizinPdfFile+"/Sale_Sheet.pdf";
    	
    	Document document = new Document(PageSize.A3);
 		try
		{
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfResult));
			document.open();
			
			PdfPTable table = new PdfPTable(2); // 3 columns.
			table.setWidthPercentage(108); //Width 100%
			table.setSpacingBefore(10f); //Space before table
			table.setSpacingAfter(10f); //Space after table
            
			//Set Column widths
			float[] columnWidths = {1f, 1f};
			table.setWidths(columnWidths);
			
			PdfPCell cell1 = new PdfPCell(new Paragraph(new Phrase("FIST COLUMN")));
			cell1.setBorderColor(BaseColor.BLACK);
			PdfPTable nestedTable  = new PdfPTable(9);
			nestedTable.setWidthPercentage(100);
			
			float[] nestedcolumnWidths = {2f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f};
			nestedTable.setWidths(nestedcolumnWidths);
			PdfPCell tablecell1 = new PdfPCell(new Paragraph(".", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD)));
			tablecell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			tablecell1.setVerticalAlignment(Element.ALIGN_LEFT);
			tablecell1.setPaddingTop(5);
			tablecell1.setPaddingBottom(5);
			tablecell1.setColspan(9);
			tablecell1.setBorder(0);
			
			PdfPCell nestedcell1 = new PdfPCell(new Paragraph(new Phrase("NAME", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell1.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell2 = new PdfPCell(new Paragraph(new Phrase("PT", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell2.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell3 = new PdfPCell(new Paragraph(new Phrase("OB", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell3.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell4 = new PdfPCell(new Paragraph(new Phrase("REC", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell4.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell4.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell5 = new PdfPCell(new Paragraph(new Phrase("TTL", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell5.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell5.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell6 = new PdfPCell(new Paragraph(new Phrase("CB", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell6.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell6.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell7 = new PdfPCell(new Paragraph(new Phrase("SALE", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell7.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell7.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell8 = new PdfPCell(new Paragraph(new Phrase("MRP", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell8.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell8.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedcell9 = new PdfPCell(new Paragraph(new Phrase("TOTAL", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedcell9.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedcell9.setVerticalAlignment(Element.ALIGN_CENTER);
			
			nestedTable.addCell(tablecell1);
			nestedTable.addCell(nestedcell1);
			nestedTable.addCell(nestedcell2);
			nestedTable.addCell(nestedcell3);
			nestedTable.addCell(nestedcell4);
			nestedTable.addCell(nestedcell5);
			nestedTable.addCell(nestedcell6);
			nestedTable.addCell(nestedcell7);
			nestedTable.addCell(nestedcell8);
			nestedTable.addCell(nestedcell9);
					      
		    
 		for (int i = 0; i < listSize; i++) { 
 			PdfPCell nestedcell11 =null, nestedcell41=null, nestedcell51=null;
 			if(saleDeatils.getSaleBean().get(i).isConditonVal() == true){
				 nestedcell11 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell11.setBorder(PdfPCell.NO_BORDER);
				//nestedcell11.setCellEvent(new SolidBorder(PdfPCell.TOP));
				nestedcell11.setCellEvent(new SolidBorder(PdfPCell.LEFT));
 			}
 			else if(saleDeatils.getSaleBean().get(i).isConditonVal() == false && saleDeatils.getSaleBean().get(i).getNoOfReturnsBtl()==1){
				 nestedcell11 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getBrandName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell11.setBorder(PdfPCell.NO_BORDER);
				nestedcell11.setCellEvent(new SolidBorder(PdfPCell.TOP));
				nestedcell11.setCellEvent(new SolidBorder(PdfPCell.LEFT));
				nestedcell11.setPaddingTop(5);
				nestedcell11.setPaddingBottom(5);
				nestedcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell11.setVerticalAlignment(Element.ALIGN_CENTER);
 			}
 			else{
 				 nestedcell11 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getBrandName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell11.setBorder(PdfPCell.NO_BORDER);
				nestedcell11.setCellEvent(new SolidBorder(PdfPCell.TOP));
				nestedcell11.setCellEvent(new SolidBorder(PdfPCell.LEFT));
				nestedcell11.setPaddingTop(5);
				nestedcell11.setPaddingBottom(5);
				nestedcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell11.setVerticalAlignment(Element.ALIGN_CENTER);
 			}
				
				PdfPCell nestedcell21 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getPackType(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell21.setPaddingTop(5);
				nestedcell21.setPaddingBottom(5);
				nestedcell21.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell21.setVerticalAlignment(Element.ALIGN_CENTER);
				
				PdfPCell nestedcell31 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell31.setPaddingTop(5);
				nestedcell31.setPaddingBottom(5);
				nestedcell31.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell31.setVerticalAlignment(Element.ALIGN_CENTER);
				
			     nestedcell41 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));	
			     nestedcell51  = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				PdfPCell nestedcell61 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				PdfPCell nestedcell71 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				PdfPCell nestedcell81 = new PdfPCell(new Paragraph(new Phrase(Integer.toString((saleDeatils.getSaleBean().get(i).getUnitPrice()).intValue()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedcell81.setPaddingTop(5);
				nestedcell81.setPaddingBottom(5);
				nestedcell81.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedcell81.setVerticalAlignment(Element.ALIGN_CENTER);
				PdfPCell nestedcell91 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				
				nestedTable.addCell(nestedcell11);
				nestedTable.addCell(nestedcell21);
				nestedTable.addCell(nestedcell31);
				nestedTable.addCell(nestedcell41);
				nestedTable.addCell(nestedcell51);
				nestedTable.addCell(nestedcell61);
				nestedTable.addCell(nestedcell71);
				nestedTable.addCell(nestedcell81);
				nestedTable.addCell(nestedcell91);
			}
			
			cell1.addElement(nestedTable);
			cell1.setBorder(0);
			
			//cell1.setBorder(0);
			PdfPCell cell2 = new PdfPCell(new Paragraph(new Phrase("SECOND COLUMN")));
			cell2.setBorderColor(BaseColor.BLACK);
			PdfPTable nestedRightTable  = new PdfPTable(9);
			nestedRightTable.setWidthPercentage(100);
			
			float[] nestedRightcolumnWidths = {2f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f};
			nestedRightTable.setWidths(nestedRightcolumnWidths);
			PdfPCell tablecell2 = new PdfPCell(new Paragraph(new Phrase(".    ", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			tablecell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
			tablecell2.setVerticalAlignment(Element.ALIGN_RIGHT);
			tablecell2.setPaddingTop(5);
			tablecell2.setPaddingBottom(5);
			tablecell2.setColspan(9);
			tablecell2.setBorder(0);
			PdfPCell nestedRightcell1 = new PdfPCell(new Paragraph(new Phrase("NAME", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell1.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell2 = new PdfPCell(new Paragraph(new Phrase("PT", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell2.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell3 = new PdfPCell(new Paragraph(new Phrase("OB", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell3.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell4 = new PdfPCell(new Paragraph(new Phrase("REC", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell4.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell4.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell5 = new PdfPCell(new Paragraph(new Phrase("TTL", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell5.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell5.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell6 = new PdfPCell(new Paragraph(new Phrase("CB", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell6.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell6.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell7 = new PdfPCell(new Paragraph(new Phrase("SALE", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell7.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell7.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell8 = new PdfPCell(new Paragraph(new Phrase("MRP", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell8.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell8.setVerticalAlignment(Element.ALIGN_CENTER);
			PdfPCell nestedRightcell9 = new PdfPCell(new Paragraph(new Phrase("TOTAL", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			nestedRightcell9.setHorizontalAlignment(Element.ALIGN_CENTER);
			nestedRightcell9.setVerticalAlignment(Element.ALIGN_CENTER);
			
			nestedRightTable.addCell(tablecell2);
			nestedRightTable.addCell(nestedRightcell1);
			nestedRightTable.addCell(nestedRightcell2);
			nestedRightTable.addCell(nestedRightcell3);
			nestedRightTable.addCell(nestedRightcell4);
			nestedRightTable.addCell(nestedRightcell5);
			nestedRightTable.addCell(nestedRightcell6);
			nestedRightTable.addCell(nestedRightcell7);
			nestedRightTable.addCell(nestedRightcell8);
			nestedRightTable.addCell(nestedRightcell9);
					      
		    
 		for (int i = listSize; i < saleDeatils.getSaleBean().size(); i++) { 
 			PdfPCell nestedRightcell11 =null, nestedRightcell41=null, nestedRightcell51=null;
 			if(saleDeatils.getSaleBean().get(i).isConditonVal() == true){
 				nestedRightcell11 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9))));
				 nestedRightcell11.setBorder(PdfPCell.NO_BORDER);
				//nestedcell11.setCellEvent(new SolidBorder(PdfPCell.TOP));
				 nestedRightcell11.setCellEvent(new SolidBorder(PdfPCell.LEFT));
 			}
 			else if(saleDeatils.getSaleBean().get(i).isConditonVal() == false && saleDeatils.getSaleBean().get(i).getNoOfReturnsBtl()==1){
 				nestedRightcell11 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getBrandName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
 				nestedRightcell11.setBorder(PdfPCell.NO_BORDER);
 				nestedRightcell11.setCellEvent(new SolidBorder(PdfPCell.TOP));
 				nestedRightcell11.setCellEvent(new SolidBorder(PdfPCell.LEFT));
 				nestedRightcell11.setPaddingTop(5);
 				nestedRightcell11.setPaddingBottom(5);
 				nestedRightcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedRightcell11.setVerticalAlignment(Element.ALIGN_CENTER);
 			}
 			else{
 				nestedRightcell11 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getBrandName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
 				nestedRightcell11.setBorder(PdfPCell.NO_BORDER);
 				nestedRightcell11.setCellEvent(new SolidBorder(PdfPCell.TOP));
 				nestedRightcell11.setCellEvent(new SolidBorder(PdfPCell.LEFT));
 				nestedRightcell11.setPaddingTop(5);
 				nestedRightcell11.setPaddingBottom(5);
 				nestedRightcell11.setHorizontalAlignment(Element.ALIGN_CENTER);
 				nestedRightcell11.setVerticalAlignment(Element.ALIGN_CENTER);
 			}
				
				PdfPCell nestedRightcell21 = new PdfPCell(new Paragraph(new Phrase(saleDeatils.getSaleBean().get(i).getPackType(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedRightcell21.setPaddingTop(5);
				nestedRightcell21.setPaddingBottom(5);
				nestedRightcell21.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedRightcell21.setVerticalAlignment(Element.ALIGN_CENTER);
				
				PdfPCell nestedRightcell31 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedRightcell31.setPaddingTop(5);
				nestedRightcell31.setPaddingBottom(5);
				nestedRightcell31.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedRightcell31.setVerticalAlignment(Element.ALIGN_CENTER);
				
					nestedRightcell41 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));	
					nestedRightcell51  = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				PdfPCell nestedRightcell61 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				PdfPCell nestedRightcell71 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				PdfPCell nestedRightcell81 = new PdfPCell(new Paragraph(new Phrase(Integer.toString((saleDeatils.getSaleBean().get(i).getUnitPrice()).intValue()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				nestedRightcell81.setPaddingTop(5);
				nestedRightcell81.setPaddingBottom(5);
				nestedRightcell81.setHorizontalAlignment(Element.ALIGN_CENTER);
				nestedRightcell81.setVerticalAlignment(Element.ALIGN_CENTER);
				PdfPCell nestedRightcell91 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 8))));
				
				nestedRightTable.addCell(nestedRightcell11);
				nestedRightTable.addCell(nestedRightcell21);
				nestedRightTable.addCell(nestedRightcell31);
				nestedRightTable.addCell(nestedRightcell41);
				nestedRightTable.addCell(nestedRightcell51);
				nestedRightTable.addCell(nestedRightcell61);
				nestedRightTable.addCell(nestedRightcell71);
				nestedRightTable.addCell(nestedRightcell81);
				nestedRightTable.addCell(nestedRightcell91);
				
			}
 		PdfPCell emptyNestedcell1 = new PdfPCell(new Paragraph(new Phrase(".", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
		emptyNestedcell1.setPaddingTop(5);
		emptyNestedcell1.setPaddingBottom(5);
		PdfPCell emptyNestedcell2 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
		emptyNestedcell2.setPaddingTop(5);
		emptyNestedcell2.setPaddingBottom(5);
		PdfPCell emptyNestedcell3 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
		emptyNestedcell3.setPaddingTop(5);
		emptyNestedcell3.setPaddingBottom(5);
		PdfPCell emptyNestedcell4 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
		emptyNestedcell4.setPaddingTop(5);
		emptyNestedcell4.setPaddingBottom(5);
		PdfPCell emptyNestedcell5 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
		emptyNestedcell5.setPaddingTop(5);
		emptyNestedcell5.setPaddingBottom(5);
		PdfPCell emptyNestedcell6 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
		emptyNestedcell6.setPaddingTop(5);
		emptyNestedcell6.setPaddingBottom(5);
		PdfPCell emptyNestedcell7 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
		emptyNestedcell7.setPaddingTop(5);
		emptyNestedcell7.setPaddingBottom(5);
		PdfPCell emptyNestedcell8 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
		emptyNestedcell8.setPaddingTop(5);
		emptyNestedcell8.setPaddingBottom(5);
		PdfPCell emptyNestedcell9 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
		emptyNestedcell9.setPaddingTop(5);
		emptyNestedcell9.setPaddingBottom(5);
		
		
		nestedRightTable.addCell(emptyNestedcell1);
		nestedRightTable.addCell(emptyNestedcell2);
		nestedRightTable.addCell(emptyNestedcell3);
		nestedRightTable.addCell(emptyNestedcell4);
		nestedRightTable.addCell(emptyNestedcell5);
		nestedRightTable.addCell(emptyNestedcell6);
		nestedRightTable.addCell(emptyNestedcell7);
		nestedRightTable.addCell(emptyNestedcell8);
		nestedRightTable.addCell(emptyNestedcell9);
		
		nestedRightTable.addCell(emptyNestedcell1);
		nestedRightTable.addCell(emptyNestedcell2);
		nestedRightTable.addCell(emptyNestedcell3);
		nestedRightTable.addCell(emptyNestedcell4);
		nestedRightTable.addCell(emptyNestedcell5);
		nestedRightTable.addCell(emptyNestedcell6);
		nestedRightTable.addCell(emptyNestedcell7);
		nestedRightTable.addCell(emptyNestedcell8);
		nestedRightTable.addCell(emptyNestedcell9);
		
		nestedRightTable.addCell(emptyNestedcell1);
		nestedRightTable.addCell(emptyNestedcell2);
		nestedRightTable.addCell(emptyNestedcell3);
		nestedRightTable.addCell(emptyNestedcell4);
		nestedRightTable.addCell(emptyNestedcell5);
		nestedRightTable.addCell(emptyNestedcell6);
		nestedRightTable.addCell(emptyNestedcell7);
		nestedRightTable.addCell(emptyNestedcell8);
		nestedRightTable.addCell(emptyNestedcell9);
		
		nestedRightTable.addCell(emptyNestedcell1);
		nestedRightTable.addCell(emptyNestedcell2);
		nestedRightTable.addCell(emptyNestedcell3);
		nestedRightTable.addCell(emptyNestedcell4);
		nestedRightTable.addCell(emptyNestedcell5);
		nestedRightTable.addCell(emptyNestedcell6);
		nestedRightTable.addCell(emptyNestedcell7);
		nestedRightTable.addCell(emptyNestedcell8);
		nestedRightTable.addCell(emptyNestedcell9);
		
		nestedRightTable.addCell(emptyNestedcell1);
		nestedRightTable.addCell(emptyNestedcell2);
		nestedRightTable.addCell(emptyNestedcell3);
		nestedRightTable.addCell(emptyNestedcell4);
		nestedRightTable.addCell(emptyNestedcell5);
		nestedRightTable.addCell(emptyNestedcell6);
		nestedRightTable.addCell(emptyNestedcell7);
		nestedRightTable.addCell(emptyNestedcell8);
		nestedRightTable.addCell(emptyNestedcell9);
		
		nestedRightTable.addCell(emptyNestedcell1);
		nestedRightTable.addCell(emptyNestedcell2);
		nestedRightTable.addCell(emptyNestedcell3);
		nestedRightTable.addCell(emptyNestedcell4);
		nestedRightTable.addCell(emptyNestedcell5);
		nestedRightTable.addCell(emptyNestedcell6);
		nestedRightTable.addCell(emptyNestedcell7);
		nestedRightTable.addCell(emptyNestedcell8);
		nestedRightTable.addCell(emptyNestedcell9);
		
		nestedRightTable.addCell(emptyNestedcell1);
		nestedRightTable.addCell(emptyNestedcell2);
		nestedRightTable.addCell(emptyNestedcell3);
		nestedRightTable.addCell(emptyNestedcell4);
		nestedRightTable.addCell(emptyNestedcell5);
		nestedRightTable.addCell(emptyNestedcell6);
		nestedRightTable.addCell(emptyNestedcell7);
		nestedRightTable.addCell(emptyNestedcell8);
		nestedRightTable.addCell(emptyNestedcell9);
		
		nestedRightTable.addCell(emptyNestedcell1);
		nestedRightTable.addCell(emptyNestedcell2);
		nestedRightTable.addCell(emptyNestedcell3);
		nestedRightTable.addCell(emptyNestedcell4);
		nestedRightTable.addCell(emptyNestedcell5);
		nestedRightTable.addCell(emptyNestedcell6);
		nestedRightTable.addCell(emptyNestedcell7);
		nestedRightTable.addCell(emptyNestedcell8);
		nestedRightTable.addCell(emptyNestedcell9);
		
		nestedRightTable.addCell(emptyNestedcell1);
		nestedRightTable.addCell(emptyNestedcell2);
		nestedRightTable.addCell(emptyNestedcell3);
		nestedRightTable.addCell(emptyNestedcell4);
		nestedRightTable.addCell(emptyNestedcell5);
		nestedRightTable.addCell(emptyNestedcell6);
		nestedRightTable.addCell(emptyNestedcell7);
		nestedRightTable.addCell(emptyNestedcell8);
		nestedRightTable.addCell(emptyNestedcell9);
		
		nestedRightTable.addCell(emptyNestedcell1);
		nestedRightTable.addCell(emptyNestedcell2);
		nestedRightTable.addCell(emptyNestedcell3);
		nestedRightTable.addCell(emptyNestedcell4);
		nestedRightTable.addCell(emptyNestedcell5);
		nestedRightTable.addCell(emptyNestedcell6);
		nestedRightTable.addCell(emptyNestedcell7);
		nestedRightTable.addCell(emptyNestedcell8);
		nestedRightTable.addCell(emptyNestedcell9);
			cell2.addElement(nestedRightTable);
			cell2.setBorder(0);
			table.addCell(cell1);
			table.addCell(cell2);
			document.add(table);
			// For expenses
			PdfPTable expensetable = new PdfPTable(2); // 3 columns.
			expensetable.setWidthPercentage(108); //Width 100%
			expensetable.setSpacingBefore(20f); //Space before table
			expensetable.setSpacingAfter(10f);
			
			float[] expensecolumnWidths = {1f, 1f};
			expensetable.setWidths(expensecolumnWidths);
			PdfPCell expensecell1 = new PdfPCell(new Paragraph(new Phrase("Expenses")));
			
			PdfPTable nestedExpenseTable  = new PdfPTable(2);
			nestedExpenseTable.setWidthPercentage(100);
			float[] nestedExpensecolumnWidths = {2f, 1f};
			nestedExpenseTable.setWidths(nestedExpensecolumnWidths);
			PdfPCell expenseNestedcell1 = new PdfPCell(new Paragraph(new Phrase("Expenses", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			expenseNestedcell1.setPaddingTop(5);
			expenseNestedcell1.setPaddingBottom(5);
			PdfPCell expenseNestedcell2 = new PdfPCell(new Paragraph(new Phrase("Amount", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			expenseNestedcell2.setPaddingTop(5);
			expenseNestedcell2.setPaddingBottom(5);
			
			PdfPCell expenseNestedcell3 = new PdfPCell(new Paragraph(new Phrase("1)")));
			expenseNestedcell3.setPaddingTop(5);
			expenseNestedcell3.setPaddingBottom(5);
			PdfPCell expenseNestedcell4 = new PdfPCell(new Paragraph(new Phrase("")));
			expenseNestedcell4.setPaddingTop(5);
			expenseNestedcell4.setPaddingBottom(5);
			PdfPCell expenseNestedcell5 = new PdfPCell(new Paragraph(new Phrase("2)")));
			expenseNestedcell5.setPaddingTop(5);
			expenseNestedcell5.setPaddingBottom(5);
			PdfPCell expenseNestedcell6 = new PdfPCell(new Paragraph(new Phrase("")));
			expenseNestedcell6.setPaddingTop(5);
			expenseNestedcell6.setPaddingBottom(5);
			PdfPCell expenseNestedcell7 = new PdfPCell(new Paragraph(new Phrase("3)")));
			expenseNestedcell7.setPaddingTop(5);
			expenseNestedcell7.setPaddingBottom(5);
			PdfPCell expenseNestedcell8 = new PdfPCell(new Paragraph(new Phrase("")));
			expenseNestedcell8.setPaddingTop(5);
			expenseNestedcell8.setPaddingBottom(5);
			PdfPCell expenseNestedcell9 = new PdfPCell(new Paragraph(new Phrase("4)")));
			expenseNestedcell9.setPaddingTop(5);
			expenseNestedcell9.setPaddingBottom(5);
			PdfPCell expenseNestedcell10 = new PdfPCell(new Paragraph(new Phrase("")));
			expenseNestedcell10.setPaddingTop(5);
			expenseNestedcell10.setPaddingBottom(5);
			PdfPCell expenseNestedcell11 = new PdfPCell(new Paragraph(new Phrase("5)")));
			expenseNestedcell11.setPaddingTop(5);
			expenseNestedcell11.setPaddingBottom(5);
			PdfPCell expenseNestedcell12 = new PdfPCell(new Paragraph(new Phrase("")));
			expenseNestedcell12.setPaddingTop(5);
			expenseNestedcell12.setPaddingBottom(5);
			PdfPCell expenseNestedcell13 = new PdfPCell(new Paragraph(new Phrase("Total")));
			expenseNestedcell13.setPaddingTop(5);
			expenseNestedcell13.setPaddingBottom(5);
			PdfPCell expenseNestedcell14 = new PdfPCell(new Paragraph(new Phrase("")));
			expenseNestedcell14.setPaddingTop(5);
			expenseNestedcell14.setPaddingBottom(5);
			nestedExpenseTable.addCell(expenseNestedcell1);
			nestedExpenseTable.addCell(expenseNestedcell2);
			nestedExpenseTable.addCell(expenseNestedcell3);
			nestedExpenseTable.addCell(expenseNestedcell4);
			nestedExpenseTable.addCell(expenseNestedcell5);
			nestedExpenseTable.addCell(expenseNestedcell6);
			nestedExpenseTable.addCell(expenseNestedcell7);
			nestedExpenseTable.addCell(expenseNestedcell8);
			nestedExpenseTable.addCell(expenseNestedcell9);
			nestedExpenseTable.addCell(expenseNestedcell10);
			nestedExpenseTable.addCell(expenseNestedcell11);
			nestedExpenseTable.addCell(expenseNestedcell12);
			nestedExpenseTable.addCell(expenseNestedcell13);
			nestedExpenseTable.addCell(expenseNestedcell14);
			
			expensecell1.addElement(nestedExpenseTable);
			expensecell1.setBorder(0);
			expensetable.addCell(expensecell1);
			PdfPCell expensecell2 = new PdfPCell(new Paragraph(new Phrase("", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
			expensecell2.setBorder(0);
			expensetable.addCell(expensecell2);
			document.add(expensetable);
			
			// For Last Table
						PdfPTable lastTable  = new PdfPTable(3);
						lastTable.setWidthPercentage(100);
						float[] lasttablecolumnWidths = {1f, 1f, 1f};
						lastTable.setWidths(lasttablecolumnWidths);
						PdfPCell lasttablecell1 = new PdfPCell(new Paragraph(new Phrase("Sale Sheet", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
						lasttablecell1.setPaddingTop(5);
						lasttablecell1.setPaddingBottom(5);
						PdfPCell lasttablecell2 = new PdfPCell(new Paragraph(new Phrase("Shop Cash", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
						lasttablecell2.setPaddingTop(5);
						lasttablecell2.setPaddingBottom(5);
						PdfPCell lasttablecell3 = new PdfPCell(new Paragraph(new Phrase("Difference", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD))));
						lasttablecell3.setPaddingTop(5);
						lasttablecell3.setPaddingBottom(5);
						
						PdfPCell lasttablecell4 = new PdfPCell(new Paragraph(new Phrase(".")));
						lasttablecell4.setPaddingTop(5);
						lasttablecell4.setPaddingBottom(5);
						PdfPCell lasttablecell5 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell5.setPaddingTop(5);
						lasttablecell5.setPaddingBottom(5);
						PdfPCell lasttablecell6 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell6.setPaddingTop(5);
						lasttablecell6.setPaddingBottom(5);
						
						PdfPCell lasttablecell7 = new PdfPCell(new Paragraph(new Phrase(".")));
						lasttablecell7.setPaddingTop(5);
						lasttablecell7.setPaddingBottom(5);
						PdfPCell lasttablecell8 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell8.setPaddingTop(5);
						lasttablecell8.setPaddingBottom(5);
						PdfPCell lasttablecell9 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell9.setPaddingTop(5);
						lasttablecell9.setPaddingBottom(5);
						
						PdfPCell lasttablecell10 = new PdfPCell(new Paragraph(new Phrase(".")));
						lasttablecell10.setPaddingTop(5);
						lasttablecell10.setPaddingBottom(5);
						PdfPCell lasttablecell11 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell11.setPaddingTop(5);
						lasttablecell11.setPaddingBottom(5);
						PdfPCell lasttablecell12 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell12.setPaddingTop(5);
						lasttablecell12.setPaddingBottom(5);
						
						PdfPCell lasttablecell13 = new PdfPCell(new Paragraph(new Phrase(".")));
						lasttablecell13.setPaddingTop(5);
						lasttablecell13.setPaddingBottom(5);
						PdfPCell lasttablecell14 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell14.setPaddingTop(5);
						lasttablecell14.setPaddingBottom(5);
						PdfPCell lasttablecell15 = new PdfPCell(new Paragraph(new Phrase("")));
						
						PdfPCell lasttablecell16 = new PdfPCell(new Paragraph(new Phrase(".")));
						lasttablecell16.setPaddingTop(5);
						lasttablecell16.setPaddingBottom(5);
						PdfPCell lasttablecell17 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell17.setPaddingTop(5);
						lasttablecell17.setPaddingBottom(5);
						PdfPCell lasttablecell18 = new PdfPCell(new Paragraph(new Phrase("")));
						lasttablecell18.setPaddingTop(5);
						lasttablecell18.setPaddingBottom(5);
						lastTable.addCell(lasttablecell1);
						lastTable.addCell(lasttablecell2);
						lastTable.addCell(lasttablecell3);
						lastTable.addCell(lasttablecell4);
						lastTable.addCell(lasttablecell5);
						lastTable.addCell(lasttablecell6);
						lastTable.addCell(lasttablecell7);
						lastTable.addCell(lasttablecell8);
						lastTable.addCell(lasttablecell9);
						lastTable.addCell(lasttablecell10);
						lastTable.addCell(lasttablecell11);
						lastTable.addCell(lasttablecell12);
						lastTable.addCell(lasttablecell13);
						lastTable.addCell(lasttablecell14);
						lastTable.addCell(lasttablecell15);
						lastTable.addCell(lasttablecell16);
						lastTable.addCell(lasttablecell17);
						lastTable.addCell(lasttablecell18);
						document.add(lastTable);
			
			document.close();
			writer.close();
		} catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
 		System.out.println("Close");
 		return true;
    }
    @RequestMapping(value = "/discountReportForSelectedMonth", method = RequestMethod.GET)
    public @ResponseBody List<MobileViewStockLiftingDiscount> discountReportForSelectedMonth(HttpServletRequest request, HttpServletResponse response,
    		@RequestParam("startMonth") String startMonth, @RequestParam("endMonth") String endMonth) throws ParseException {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
		List<MobileViewStockLiftingDiscount> expenseList = null;
		expenseList = poizonThirdService.discountReportForSelectedMonth(startMonth,endMonth);
		return expenseList;
    }
    @RequestMapping(value = "/getExpenseDataRangeWise", method = RequestMethod.GET)
    public @ResponseBody List<ExpenseCategoryAndDate> getExpenseDataRangeWise(HttpServletRequest request,
    		HttpServletResponse response,@RequestParam("startDate") String startDate,@RequestParam("endDate") String endDate) throws ParseException {
    	System.out.println("getExpenseDataRangeWise method");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<ExpenseCategoryAndDate> list = null;
 		list = poizonThirdService.getExpenseDataRangeWise(CommonUtil.chageDateFormat(startDate),CommonUtil.chageDateFormat(endDate));
		return list;
    }
    @RequestMapping(value = "/getHomePageDiscountDetails", method = RequestMethod.GET)
    public @ResponseBody List<DiscountAsPerCompanyBeen> getHomePageDiscountDetails(HttpServletRequest request,
    		HttpServletResponse response,@RequestParam("companyId") int companyId) throws ParseException {
    	System.out.println("getHomePageDiscountDetails method");
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<DiscountAsPerCompanyBeen> list = null;
 		list = poizonThirdService.getHomePageDiscountDetails(companyId);
		return list;
    }
    @RequestMapping(value = "/getExistingSingleCompanyArrears", method = RequestMethod.GET)
	public @ResponseBody VendorAdjustmentGetArrearsBean getExistingSingleCompanyArrears(HttpServletRequest request, 
			HttpServletResponse response, @RequestParam("discountMonth") String date,@RequestParam("company") String company) throws ParseException {
		log.info("PoizonThirdController.getDiscountEsitmateData");
		response.setContentType("application/json");
			response.setHeader("Access-Control-Allow-Origin","*");
			response.setHeader("value", "valid");
			DateFormat df = new SimpleDateFormat("dd/MMMM/yyyy"); 
	 		Date startDate;
	 		 startDate = df.parse(date);
	 		df = new SimpleDateFormat("yyyy-MM-dd");  
	 	   String discountMonth = df.format(startDate); 
			VendorAdjustmentGetArrearsBean bean = null;
			bean = poizonThirdService.getExistingSingleCompanyArrears(discountMonth,company);
		return bean;
	}
    /**
     * Rental GET and POST start
     * @throws ParseException 
     * */
    @RequestMapping(value = "getExistingRentalForAMonth", method=RequestMethod.GET)
    public @ResponseBody RentalAndBreakageBean getExistingRentalForAMonth(HttpServletRequest request, HttpServletResponse response, @RequestParam("rentalMonth") String rentalMonth,
    		@RequestParam("company") String company) throws ParseException{
    	log.info("PoizonThirdController :: getExistingRentalForAMonth()");
    	DateFormat df = new SimpleDateFormat("dd/MMMM/yyyy"); 
 		Date startDate;
 		 startDate = df.parse(rentalMonth);
 		df = new SimpleDateFormat("yyyy-MM-dd");  
 	   String Month = df.format(startDate); 
    	RentalAndBreakageBean rentalAndBreakageBean = null;
    	if(rentalMonth != null && rentalMonth !="" && company !=null){
    		rentalAndBreakageBean = poizonThirdService.getExistingRentalForAMonth(company, Month);
    	}
    	return rentalAndBreakageBean;
    }
    
    @RequestMapping(value = "saveOrUpdateRental", method = RequestMethod.POST)
    public @ResponseBody String saveOrUpdateRentalForAMonth(HttpServletRequest request, HttpServletResponse response, @RequestParam("rentalMonth") String rentalMonth,
    		@RequestParam("rentalcompanyId") String companyId,@RequestParam("rentalAmount") double rentalAmount,RedirectAttributes redirectAttributes) throws ParseException{
    	log.info("PoizonThirdController :: saveOrUpdateRentalForAMonth()");
    	String result =null;
    	DateFormat df = new SimpleDateFormat("dd/MMMM/yyyy"); 
	    Date startDate;
		startDate = df.parse(rentalMonth);
	    df = new SimpleDateFormat("yyyy-MM-dd");  
	 	String month = df.format(startDate); 
    	if(rentalMonth != null && rentalMonth !="" && companyId != null){
    		RentalAndBreakageBean bean = new RentalAndBreakageBean();
    		bean.setAmount(rentalAmount);
    		//bean.setCompany(companyId);
    		bean.setMonth(month);
    		result = poizonThirdService.saveOrUpdateRentalForAMonth(bean,companyId);
    	}
		return result;
    }
    /**
     * Rental GET and POST End
     * */
    
    /**
     * Breakage GET and POST start
     * @throws ParseException 
     * */
    @RequestMapping(value = "getExistingBreakageForAMonth", method=RequestMethod.GET)
    public @ResponseBody RentalAndBreakageBean getExistingBreakageForAMonth(HttpServletRequest request, HttpServletResponse response, 
    		@RequestParam("breakageMonth") String breakageMonth) throws ParseException{
    	log.info("PoizonThirdController :: getExistingRentalForAMonth()");
    	DateFormat df = new SimpleDateFormat("dd/MMMM/yyyy"); 
	    Date startDate;
		startDate = df.parse(breakageMonth);
	    df = new SimpleDateFormat("yyyy-MM-dd");  
	 	String month = df.format(startDate); 
    	RentalAndBreakageBean rentalAndBreakageBean = null;
    	if(breakageMonth != null && breakageMonth !=""){
    		rentalAndBreakageBean = poizonThirdService.getExistingBreakageForAMonth(month);
    	}
    	return rentalAndBreakageBean;
    }
    /**
     * This method is used to get save or update breakage amount 
     * */
    @RequestMapping(value = "saveOrUpdateBreakage", method = RequestMethod.POST)
    public @ResponseBody String saveOrUpdateBreakageForAMonth(HttpServletRequest request, HttpServletResponse response, @RequestParam("breakageMonth") String breakageMonth,
    		@RequestParam("breakagecomment") String breakagecomment,@RequestParam("breakageAmount") double breakageAmount,RedirectAttributes redirectAttributes) throws ParseException{
    	log.info("PoizonThirdController :: saveOrUpdateBreakageForAMonth()");
    	DateFormat df = new SimpleDateFormat("dd/MMMM/yyyy"); 
	    Date startDate;
		startDate = df.parse(breakageMonth);
	    df = new SimpleDateFormat("yyyy-MM-dd");  
	 	String month = df.format(startDate); 
    	String result =null;
    	if(breakageMonth != null && breakageMonth !="" && breakageAmount > 0){
    		RentalAndBreakageBean bean = new RentalAndBreakageBean();
    		bean.setAmount(breakageAmount);
    		bean.setMonth(month);
    		bean.setComment(breakagecomment);
    		result = poizonThirdService.saveOrUpdateBreakageForAMonth(bean);
    	}
		return result;
    }
   
    /**
     * Breakage GET and POST End
     * */
    /***
     * This method is used to get uploaded check but still they have not received or credited in bank
     * */
    @RequestMapping(value = "getAllPendingChecks", method = RequestMethod.GET)
    public @ResponseBody List<DiscountTransactionBean> getAllPendingChecks(HttpServletRequest request, HttpServletResponse response){
    	log.info("PoiznThirdController :: getAllPendingChecks()");
    	response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
 		List<DiscountTransactionBean> pendingchecks = poizonThirdService.getAllPendingChecks();
    	return pendingchecks;
    }
    /***
     * This method is used to get update transaction column and chnage received check once Check got deposited in bank
     * */
    @RequestMapping(value = "updateReceivedCheckStatus", method = RequestMethod.POST)
    public @ResponseBody String updateReceivedCheckStatus(HttpServletRequest request, HttpServletResponse response, @RequestParam("trid") int trid,
    		@RequestParam("companyid") int companyid, @RequestParam("month") String month,@RequestParam("transactionDate") String transactionDate,
    		@RequestParam("clearedBank") String clearedBank){
    	log.info("PoiznThirdController :: updateReceivedCheckStatus(-)");
    	String result=null;
    	if(transactionDate != null && month != null && transactionDate != "" && month != "" && companyid > 0){
    		result = poizonThirdService.updateReceivedCheckStatus(trid,companyid,month,transactionDate,clearedBank);
    	}else{
    		result = "Something went wrong.";
    	}
    	
    	return result;
    }
    /**
     * This method is used to download indent estimate
     * */
    @RequestMapping(value = "/downloadendentEstimateDiscount", method = RequestMethod.GET,produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody void downloadendentEstimateDiscount(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	boolean value = false;
    	DateAndIndentBean dateAndIndentBean =  poizonThirdService.downloadendentEstimateDiscountAsPdf(false);
    	System.out.println(new ObjectMapper().writeValueAsString(dateAndIndentBean));
    	if(dateAndIndentBean !=null)
		 value = downloadendentEstimateDiscountAsPdf(request,response,dateAndIndentBean,"Indent");
		if(value)
		CommonUtil.downloadZipFile(request,response,poizinPdfFile+"/Indent.pdf");
    }
	private boolean downloadendentEstimateDiscountAsPdf(HttpServletRequest request, HttpServletResponse response,DateAndIndentBean dateAndIndentBean, String indent) {
		response.setContentType("application/json");
 		response.setHeader("Access-Control-Allow-Origin","*");
 		response.setHeader("value", "valid");
        Document document = new Document(PageSize.A4_LANDSCAPE);
        String pdfResult = poizinPdfFile+"/"+indent+".pdf";
        try
		{
        	PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfResult));
			document.open();
			Paragraph header = new Paragraph();
			Font f = new Font(FontFamily.UNDEFINED, 20.0f, Font.NORMAL, new BaseColor(36,58,81));
			header.setAlignment(Element.ALIGN_RIGHT);
			Chunk chunk = new Chunk("POIZIN",f);
			header.add(chunk);
			document.add(header);
			
			PdfPTable tableParent = new PdfPTable(9);
			tableParent.setWidthPercentage(100);
			tableParent.setSpacingBefore(10f);
			tableParent.setSpacingAfter(10f); 
			
			document.add(new Paragraph(indent+" ("+dateAndIndentBean.getDate()+" )", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD)));
			
			document.add(new Paragraph("LIQUOR", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD)));
			
			float[] columnWidthsParent = {2f, 1f, 1f, 1f,1f, 1f,1f, 1f,1f};
			tableParent.setWidths(columnWidthsParent);
			
			PdfPCell cellParent1 = new PdfPCell(new Paragraph(new Phrase("Name", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent1.setBorderColor(BaseColor.BLACK);
			cellParent1.setPaddingTop(5);
			cellParent1.setPaddingBottom(5);
			cellParent1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cellParent1.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell cellParent9 = new PdfPCell(new Paragraph(new Phrase("Brand No.", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent9.setBorderColor(BaseColor.BLACK);
			cellParent9.setPaddingTop(5);
			cellParent9.setPaddingBottom(5);
			cellParent9.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent9.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cellParent9.setBackgroundColor(BaseColor.ORANGE);
        	
			PdfPCell cellParent8 = new PdfPCell(new Paragraph(new Phrase(indent+" Case", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent8.setBorderColor(BaseColor.BLACK);
			cellParent8.setPaddingTop(5);
			cellParent8.setPaddingBottom(5);
			cellParent8.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent8.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cellParent8.setBackgroundColor(BaseColor.ORANGE);

			
			PdfPCell cellParent2 = new PdfPCell(new Paragraph(new Phrase("2L", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent2.setBorderColor(BaseColor.BLACK);
			cellParent2.setPaddingTop(5);
			cellParent2.setPaddingBottom(5);
			cellParent2.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cellParent2.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell cellParent3 = new PdfPCell(new Paragraph(new Phrase("1L", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent3.setBorderColor(BaseColor.BLACK);
			cellParent3.setPaddingTop(5);
			cellParent3.setPaddingBottom(5);
			cellParent3.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent3.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cellParent3.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell cellParent4 = new PdfPCell(new Paragraph(new Phrase("Q", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent4.setBorderColor(BaseColor.BLACK);
			cellParent4.setPaddingTop(5);
			cellParent4.setPaddingBottom(5);
			cellParent4.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent4.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cellParent4.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell cellParent5 = new PdfPCell(new Paragraph(new Phrase("P", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent5.setBorderColor(BaseColor.BLACK);
			cellParent5.setPaddingTop(5);
			cellParent5.setPaddingBottom(5);
			cellParent5.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent5.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cellParent5.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell cellParent6 = new PdfPCell(new Paragraph(new Phrase("N", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent6.setBorderColor(BaseColor.BLACK);
			cellParent6.setPaddingTop(5);
			cellParent6.setPaddingBottom(5);
			cellParent6.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent6.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cellParent6.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell cellParent7 = new PdfPCell(new Paragraph(new Phrase("D", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellParent7.setBorderColor(BaseColor.BLACK);
			cellParent7.setPaddingTop(5);
			cellParent7.setPaddingBottom(5);
			cellParent7.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellParent7.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cellParent7.setBackgroundColor(BaseColor.ORANGE);
			
			tableParent.addCell(cellParent1);
			tableParent.addCell(cellParent9);
			tableParent.addCell(cellParent8);
			tableParent.addCell(cellParent2);
			tableParent.addCell(cellParent3);
			tableParent.addCell(cellParent4);
			tableParent.addCell(cellParent5);
			tableParent.addCell(cellParent6);
			tableParent.addCell(cellParent7);
			
			
			for(IndentEstimatePDF bean : dateAndIndentBean.getIndentEstimatePDF()){
				PdfPCell cell11 = new PdfPCell(new Paragraph(new Phrase(bean.getName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell11.setBorderColor(BaseColor.BLACK);
				cell11.setPaddingTop(5);
				cell11.setPaddingBottom(5);
				cell11.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell91 = new PdfPCell(new Paragraph(new Phrase(Long.toString(bean.getBrandNo()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell91.setBorderColor(BaseColor.BLACK);
				cell91.setPaddingTop(5);
				cell91.setPaddingBottom(5);
				cell91.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell91.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell28 = new PdfPCell(new Paragraph(new Phrase(bean.getIndentCase(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell28.setBorderColor(BaseColor.BLACK);
				cell28.setPaddingTop(5);
				cell28.setPaddingBottom(5);
				cell28.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell28.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell21 = new PdfPCell(new Paragraph(new Phrase(bean.getL2Case(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell21.setBorderColor(BaseColor.BLACK);
				cell21.setPaddingTop(5);
				cell21.setPaddingBottom(5);
				cell21.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell21.setVerticalAlignment(Element.ALIGN_MIDDLE);

				PdfPCell cell31 = new PdfPCell(new Paragraph(new Phrase(bean.getL1Case(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell31.setBorderColor(BaseColor.BLACK);
				cell31.setPaddingTop(5);
				cell31.setPaddingBottom(5);
				cell31.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell31.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell41 = new PdfPCell(new Paragraph(new Phrase(new Phrase(bean.getqCase(), FontFactory.getFont(FontFactory.HELVETICA, 9)))));
				cell41.setBorderColor(BaseColor.BLACK);
				cell41.setPaddingTop(5);
				cell41.setPaddingBottom(5);
				cell41.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell41.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell51 = new PdfPCell(new Paragraph(new Phrase(bean.getpCase(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell51.setBorderColor(BaseColor.BLACK);
				cell51.setPaddingTop(5);
				cell51.setPaddingBottom(5);
				cell51.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell51.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				
				PdfPCell cell61 = new PdfPCell(new Paragraph(new Phrase(new Phrase(bean.getnCase(), FontFactory.getFont(FontFactory.HELVETICA, 9)))));
				cell61.setBorderColor(BaseColor.BLACK);
				cell61.setPaddingTop(5);
				cell61.setPaddingBottom(5);
				cell61.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell61.setVerticalAlignment(Element.ALIGN_MIDDLE);

				PdfPCell cell71 = new PdfPCell(new Paragraph(new Phrase(new Phrase(bean.getdCase(), FontFactory.getFont(FontFactory.HELVETICA, 9)))));
				cell71.setBorderColor(BaseColor.BLACK);
				cell71.setPaddingTop(5);
				cell71.setPaddingBottom(5);
				cell71.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell71.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				tableParent.addCell(cell11);
				tableParent.addCell(cell91);
				tableParent.addCell(cell28);
				tableParent.addCell(cell21);
				tableParent.addCell(cell31);
				tableParent.addCell(cell41);
				tableParent.addCell(cell51);
				tableParent.addCell(cell61);
				tableParent.addCell(cell71);
			}
			document.add(tableParent);
			
			document.add(new Paragraph("BEER", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD)));
			
			PdfPTable tableBeer = new PdfPTable(6);
			tableBeer.setWidthPercentage(100);
			tableBeer.setSpacingBefore(10f);
			tableBeer.setSpacingAfter(10f); 
			
			float[] beercolumnWidths = {2f, 1f, 1f, 1f,1f,1f};
			tableBeer.setWidths(beercolumnWidths);
			
			PdfPCell cellbeer1 = new PdfPCell(new Paragraph(new Phrase("Name", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellbeer1.setBorderColor(BaseColor.BLACK);
			cellbeer1.setPaddingTop(5);
			cellbeer1.setPaddingBottom(5);
			cellbeer1.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellbeer1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cellbeer1.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell cellbeer5 = new PdfPCell(new Paragraph(new Phrase("Brand No.", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellbeer5.setBorderColor(BaseColor.BLACK);
			cellbeer5.setPaddingTop(5);
			cellbeer5.setPaddingBottom(5);
			cellbeer5.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellbeer5.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cellbeer5.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell cellbeer11 = new PdfPCell(new Paragraph(new Phrase(indent+" Case", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellbeer11.setBorderColor(BaseColor.BLACK);
			cellbeer11.setPaddingTop(5);
			cellbeer11.setPaddingBottom(5);
			cellbeer11.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellbeer11.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cellbeer11.setBackgroundColor(BaseColor.ORANGE);
        	
			PdfPCell cellbeer2 = new PdfPCell(new Paragraph(new Phrase("LB", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellbeer2.setBorderColor(BaseColor.BLACK);
			cellbeer2.setPaddingTop(5);
			cellbeer2.setPaddingBottom(5);
			cellbeer2.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellbeer2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cellbeer2.setBackgroundColor(BaseColor.ORANGE);

			PdfPCell cellbeer3 = new PdfPCell(new Paragraph(new Phrase("SB", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellbeer3.setBorderColor(BaseColor.BLACK);
			cellbeer3.setPaddingTop(5);
			cellbeer3.setPaddingBottom(5);
			cellbeer3.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellbeer3.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cellbeer3.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell cellbeer4 = new PdfPCell(new Paragraph(new Phrase("TIN", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cellbeer4.setBorderColor(BaseColor.BLACK);
			cellbeer4.setPaddingTop(5);
			cellbeer4.setPaddingBottom(5);
			cellbeer4.setHorizontalAlignment(Element.ALIGN_CENTER);
			cellbeer4.setVerticalAlignment(Element.ALIGN_MIDDLE);
			cellbeer4.setBackgroundColor(BaseColor.ORANGE);
			
			tableBeer.addCell(cellbeer1);
			tableBeer.addCell(cellbeer5);
			tableBeer.addCell(cellbeer11);
			tableBeer.addCell(cellbeer2);
			tableBeer.addCell(cellbeer3);
			tableBeer.addCell(cellbeer4);
			
			for(IndentEstimatePDF bean : dateAndIndentBean.getIndentEstimateBeerPDF()){
				PdfPCell cell11 = new PdfPCell(new Paragraph(new Phrase(bean.getName(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell11.setBorderColor(BaseColor.BLACK);
				cell11.setPaddingTop(5);
				cell11.setPaddingBottom(5);
				cell11.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell15 = new PdfPCell(new Paragraph(new Phrase(Long.toString(bean.getBrandNo()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell15.setBorderColor(BaseColor.BLACK);
				cell15.setPaddingTop(5);
				cell15.setPaddingBottom(5);
				cell15.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell15.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell28 = new PdfPCell(new Paragraph(new Phrase(bean.getIndentCase(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
				cell28.setBorderColor(BaseColor.BLACK);
				cell28.setPaddingTop(5);
				cell28.setPaddingBottom(5);
				cell28.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell28.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell81 = new PdfPCell(new Paragraph(new Phrase(new Phrase(bean.getLbCase(), FontFactory.getFont(FontFactory.HELVETICA, 9)))));
				cell81.setBorderColor(BaseColor.BLACK);
				cell81.setPaddingTop(5);
				cell81.setPaddingBottom(5);
				cell81.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell81.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell91 = new PdfPCell(new Paragraph(new Phrase(new Phrase(bean.getSbCase(), FontFactory.getFont(FontFactory.HELVETICA, 9)))));
				cell91.setBorderColor(BaseColor.BLACK);
				cell91.setPaddingTop(5);
				cell91.setPaddingBottom(5);
				cell91.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell91.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				PdfPCell cell101 = new PdfPCell(new Paragraph(new Phrase(new Phrase(bean.getTinCase(), FontFactory.getFont(FontFactory.HELVETICA, 9)))));
				cell101.setBorderColor(BaseColor.BLACK);
				cell101.setPaddingTop(5);
				cell101.setPaddingBottom(5);
				cell101.setHorizontalAlignment(Element.ALIGN_CENTER);
				cell101.setVerticalAlignment(Element.ALIGN_MIDDLE);
				
				tableBeer.addCell(cell11);
				tableBeer.addCell(cell15);
				tableBeer.addCell(cell28);
				tableBeer.addCell(cell81);
				tableBeer.addCell(cell91);
				tableBeer.addCell(cell101);
				
			}
			document.add(tableBeer);
			
			PdfPTable footertable = new PdfPTable(7);
			footertable.setWidthPercentage(100);
			footertable.setSpacingBefore(10f);
			footertable.setSpacingAfter(10f); 
			float[] footertableParent = {1f, 1f, 1f, 1f, 1f, 1f, 1f};
			footertable.setWidths(footertableParent);
			
			PdfPCell footercell1 = new PdfPCell(new Paragraph(new Phrase("BEER", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			footercell1.setBorderColor(BaseColor.BLACK);
			footercell1.setPaddingTop(5);
			footercell1.setPaddingBottom(5);
			footercell1.setHorizontalAlignment(Element.ALIGN_CENTER);
			footercell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
			footercell1.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell footercell2 = new PdfPCell(new Paragraph(new Phrase("LIQUOR", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			footercell2.setBorderColor(BaseColor.BLACK);
			footercell2.setPaddingTop(5);
			footercell2.setPaddingBottom(5);
			footercell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			footercell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			footercell2.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell footercell3 = new PdfPCell(new Paragraph(new Phrase("Total Case", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			footercell3.setBorderColor(BaseColor.BLACK);
			footercell3.setPaddingTop(5);
			footercell3.setPaddingBottom(5);
			footercell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			footercell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
			footercell3.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell footercell4 = new PdfPCell(new Paragraph(new Phrase(indent+" Value", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			footercell4.setBorderColor(BaseColor.BLACK);
			footercell4.setPaddingTop(5);
			footercell4.setPaddingBottom(5);
			footercell4.setHorizontalAlignment(Element.ALIGN_CENTER);
			footercell4.setVerticalAlignment(Element.ALIGN_MIDDLE);
			footercell4.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell footercell5 = new PdfPCell(new Paragraph(new Phrase("Mrp Rounding Off", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			footercell5.setBorderColor(BaseColor.BLACK);
			footercell5.setPaddingTop(5);
			footercell5.setPaddingBottom(5);
			footercell5.setHorizontalAlignment(Element.ALIGN_CENTER);
			footercell5.setVerticalAlignment(Element.ALIGN_MIDDLE);
			footercell5.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell footercell6 = new PdfPCell(new Paragraph(new Phrase("TCS", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			footercell6.setBorderColor(BaseColor.BLACK);
			footercell6.setPaddingTop(5);
			footercell6.setPaddingBottom(5);
			footercell6.setHorizontalAlignment(Element.ALIGN_CENTER);
			footercell6.setVerticalAlignment(Element.ALIGN_MIDDLE);
			footercell6.setBackgroundColor(BaseColor.ORANGE);
			
			PdfPCell footercell7 = new PdfPCell(new Paragraph(new Phrase("Total Investment", FontFactory.getFont(FontFactory.HELVETICA, 9))));
			footercell7.setBorderColor(BaseColor.BLACK);
			footercell7.setPaddingTop(5);
			footercell7.setPaddingBottom(5);
			footercell7.setHorizontalAlignment(Element.ALIGN_CENTER);
			footercell7.setVerticalAlignment(Element.ALIGN_MIDDLE);
			footercell7.setBackgroundColor(BaseColor.ORANGE);
			
			footertable.addCell(footercell1);
			footertable.addCell(footercell2);
			footertable.addCell(footercell3);
			footertable.addCell(footercell4);
			footertable.addCell(footercell5);
			footertable.addCell(footercell6);
			footertable.addCell(footercell7);
			
			PdfPCell footercell11 = new PdfPCell(new Paragraph(new Phrase(Long.toString(dateAndIndentBean.getBeerCases()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			footercell11.setBorderColor(BaseColor.BLACK);
			footercell11.setPaddingTop(5);
			footercell11.setPaddingBottom(5);
			footercell11.setHorizontalAlignment(Element.ALIGN_CENTER);
			footercell11.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell footercell12 = new PdfPCell(new Paragraph(new Phrase(Long.toString(dateAndIndentBean.getLiquorCases()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			footercell12.setBorderColor(BaseColor.BLACK);
			footercell12.setPaddingTop(5);
			footercell12.setPaddingBottom(5);
			footercell12.setHorizontalAlignment(Element.ALIGN_CENTER);
			footercell12.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell footercell13 = new PdfPCell(new Paragraph(new Phrase(Long.toString(dateAndIndentBean.getTotalCase()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			footercell13.setBorderColor(BaseColor.BLACK);
			footercell13.setPaddingTop(5);
			footercell13.setPaddingBottom(5);
			footercell13.setHorizontalAlignment(Element.ALIGN_CENTER);
			footercell13.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell footercell14 = new PdfPCell(new Paragraph(new Phrase(Long.toString(dateAndIndentBean.getIndentAmt()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			footercell14.setBorderColor(BaseColor.BLACK);
			footercell14.setPaddingTop(5);
			footercell14.setPaddingBottom(5);
			footercell14.setHorizontalAlignment(Element.ALIGN_CENTER);
			footercell14.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell footercell15 = new PdfPCell(new Paragraph(new Phrase(Long.toString(dateAndIndentBean.getSpecialMrp()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			footercell15.setBorderColor(BaseColor.BLACK);
			footercell15.setPaddingTop(5);
			footercell15.setPaddingBottom(5);
			footercell15.setHorizontalAlignment(Element.ALIGN_CENTER);
			footercell15.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell footercell16 = new PdfPCell(new Paragraph(new Phrase(Long.toString(dateAndIndentBean.getTcs()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			footercell16.setBorderColor(BaseColor.BLACK);
			footercell16.setPaddingTop(5);
			footercell16.setPaddingBottom(5);
			footercell16.setHorizontalAlignment(Element.ALIGN_CENTER);
			footercell16.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell footercell17 = new PdfPCell(new Paragraph(new Phrase(Long.toString(dateAndIndentBean.getTotalInvestment()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			footercell17.setBorderColor(BaseColor.BLACK);
			footercell17.setPaddingTop(5);
			footercell17.setPaddingBottom(5);
			footercell17.setHorizontalAlignment(Element.ALIGN_CENTER);
			footercell17.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			footertable.addCell(footercell11);
			footertable.addCell(footercell12);
			footertable.addCell(footercell13);
			footertable.addCell(footercell14);
			footertable.addCell(footercell15);
			footertable.addCell(footercell16);
			footertable.addCell(footercell17);
			document.add(footertable);
			
			document.close();
			writer.close();
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	/**
     * This method is used to download(brand, target case, disc/case) discount for single company
     * */
    @RequestMapping(value = "/downloadtargetanddiscpercase", method = RequestMethod.GET,produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody void downloadtargetanddiscpercase(HttpServletRequest request, HttpServletResponse response,@RequestParam("date") String date, @RequestParam("company") String company) throws Exception {
    	boolean value = false;
    	TargetWithBudget targetWithBudget = poizonThirdService.downloadtargetanddiscpercase(company,date);
    	if(targetWithBudget !=null)
    		value = downloadTargetCases(request,response,targetWithBudget);
		if(value)
		CommonUtil.downloadZipFile(request,response,poizinPdfFile+"/target-case.pdf");
    }
	private boolean downloadTargetCases(HttpServletRequest request, HttpServletResponse response, TargetWithBudget targetWithBudget) throws FileNotFoundException, DocumentException {
		Document document = new Document(PageSize.A4_LANDSCAPE);
        String pdfResult = poizinPdfFile+"/target-case.pdf";
        
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(pdfResult));
		document.open();
		Paragraph header = new Paragraph();
		Font f = new Font(FontFamily.UNDEFINED, 20.0f, Font.NORMAL, new BaseColor(36,58,81));
		header.setAlignment(Element.ALIGN_RIGHT);
		Chunk chunk = new Chunk("POIZIN",f);
		header.add(chunk);
		document.add(header);
		
		document.add(new Paragraph(targetWithBudget.getCompany()+" ("+targetWithBudget.getMonth()+")", FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD)));
		
		PdfPTable table = new PdfPTable(3);
		table.setWidthPercentage(100);
		table.setSpacingBefore(10f);
		table.setSpacingAfter(10f); 
		float[] columnWidthsParent = {2f, 1f, 1f};
		table.setWidths(columnWidthsParent);
		
		PdfPCell cellParent1 = new PdfPCell(new Paragraph(new Phrase("Brand", FontFactory.getFont(FontFactory.HELVETICA, 9))));
		cellParent1.setBorderColor(BaseColor.BLACK);
		cellParent1.setPaddingTop(5);
		cellParent1.setPaddingBottom(5);
		cellParent1.setHorizontalAlignment(Element.ALIGN_CENTER);
		cellParent1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cellParent1.setBackgroundColor(BaseColor.ORANGE);
		
		PdfPCell cellParent2 = new PdfPCell(new Paragraph(new Phrase("Target Case", FontFactory.getFont(FontFactory.HELVETICA, 9))));
		cellParent2.setBorderColor(BaseColor.BLACK);
		cellParent2.setPaddingTop(5);
		cellParent2.setPaddingBottom(5);
		cellParent2.setHorizontalAlignment(Element.ALIGN_CENTER);
		cellParent2.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cellParent2.setBackgroundColor(BaseColor.ORANGE);
		
		PdfPCell cellParent3 = new PdfPCell(new Paragraph(new Phrase("Disc/Case", FontFactory.getFont(FontFactory.HELVETICA, 9))));
		cellParent3.setBorderColor(BaseColor.BLACK);
		cellParent3.setPaddingTop(5);
		cellParent3.setPaddingBottom(5);
		cellParent3.setHorizontalAlignment(Element.ALIGN_CENTER);
		cellParent3.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cellParent3.setBackgroundColor(BaseColor.ORANGE);
		
		table.addCell(cellParent1);
		table.addCell(cellParent2);
		table.addCell(cellParent3);
		
		for(DownloadTargetCase bean : targetWithBudget.getTargetCaseList()){
			PdfPCell cell1 = new PdfPCell(new Paragraph(new Phrase(bean.getBrand(), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cell1.setBorderColor(BaseColor.BLACK);
			cell1.setPaddingLeft(5);
			cell1.setPaddingTop(5);
			cell1.setPaddingBottom(5);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_LEFT);
			
			PdfPCell cell2 = new PdfPCell(new Paragraph(new Phrase(String.valueOf(bean.getTargetCase()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cell2.setBorderColor(BaseColor.BLACK);
			cell2.setPaddingTop(5);
			cell2.setPaddingBottom(5);
			cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			PdfPCell cell3 = new PdfPCell(new Paragraph(new Phrase(String.valueOf(bean.getDiscPerCase()), FontFactory.getFont(FontFactory.HELVETICA, 9))));
			cell3.setBorderColor(BaseColor.BLACK);
			cell3.setPaddingTop(5);
			cell3.setPaddingBottom(5);
			cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			table.addCell(cell1);
			table.addCell(cell2);
			table.addCell(cell3);
		}
		
		document.add(table);
		document.add(new Paragraph("Total budget : "+String.valueOf(targetWithBudget.getBudget()), FontFactory.getFont(FontFactory.HELVETICA, 9,Font.BOLD)));
		document.close();
		writer.close();
		return true;
	}
	 /**
     * This method is used to download indent estimate
     * */
    @RequestMapping(value = "/downloadSchemeEstimateDiscount", method = RequestMethod.GET,produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody void downloadSchemeEstimateDiscount(HttpServletRequest request, HttpServletResponse response) throws Exception {
    	boolean value = false;
    	DateAndIndentBean dateAndIndentBean =  poizonThirdService.downloadendentEstimateDiscountAsPdf(true);
    	System.out.println(new ObjectMapper().writeValueAsString(dateAndIndentBean));
    	if(dateAndIndentBean !=null)
		 value = downloadendentEstimateDiscountAsPdf(request,response,dateAndIndentBean,"Scheme");
		if(value)
		CommonUtil.downloadZipFile(request,response,poizinPdfFile+"/Scheme.pdf");
    }
  
}
