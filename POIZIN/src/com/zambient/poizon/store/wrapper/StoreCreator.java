package com.zambient.poizon.store.wrapper;

import org.springframework.stereotype.Component;

import com.zambient.poizon.security.entity.LoginCredentials;
import com.zambient.poizon.security.entity.LoginProfile;
import com.zambient.poizon.security.entity.StoreAddress;
import com.zambient.poizon.security.entity.StoreProfile;

@Component
 public class StoreCreator {
	
	
	private StoreProfile storeProfile;
	private StoreAddress storeAddress;
	public StoreProfile getStoreProfile() {
		return storeProfile;
	}
	public void setStoreProfile(StoreProfile storeProfile) {
		this.storeProfile = storeProfile;
	}
	public StoreAddress getStoreAddress() {
		return storeAddress;
	}
	public void setStoreAddress(StoreAddress storeAddress) {
		this.storeAddress = storeAddress;
	}


}
