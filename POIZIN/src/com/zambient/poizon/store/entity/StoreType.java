package com.zambient.poizon.store.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@NamedQueries({
	
	@NamedQuery(
		name="getProfileType",
				query="SELECT store_type FROM store_type"
	)
	
})

@Entity
@Table(name="store_type")
public class StoreType {
	
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name="store_type_id")
private int id;

@Column(name="store_type")
private String profileType;

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getProfileType() {
	return profileType;
}

public void setProfileType(String profileType) {
	this.profileType = profileType;
}
}
