package com.zambient.poizon.store.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.zambient.poizon.store.wrapper.StoreCreator;

/**
 * <h1>StoreService</h1>
 * <p>This interface is used to declare Store curd api service methods</p>
 * @author Durga Rao Maruboina
 * @version 1.0
 * @since 2020
 * 
 * */
public interface StoreService {

	public List<String> getProfileType();
	public long createNewStore(StoreCreator storeCreator);
	public String fetchStore(String userName,HttpServletRequest request);
}
