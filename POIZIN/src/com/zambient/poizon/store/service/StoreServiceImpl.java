package com.zambient.poizon.store.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.zambient.poizon.controller.SendEmail;
import com.zambient.poizon.security.entity.LoginCredentials;
import com.zambient.poizon.security.entity.LoginProfile;
import com.zambient.poizon.security.entity.StoreAddress;
import com.zambient.poizon.security.entity.StoreProfile;
import com.zambient.poizon.store.dao.StoreDao;
import com.zambient.poizon.store.dao.StoreDaoImpl;
import com.zambient.poizon.store.util.PasswordGenerator;
import com.zambient.poizon.store.wrapper.StoreCreator;
import com.zambient.poizon.utill.PoizinMail;

@Service
public class StoreServiceImpl implements StoreService{
	
	@Autowired
	private StoreDaoImpl storeDao;
	
	@Autowired
	private LoginProfile loginProfile;
	
	@Autowired
	private LoginCredentials loginCredentials;
	
	@Autowired
	private PasswordGenerator passwordGenerator;
	
	@Autowired
	private PoizinMail poizinMail;
	@Value("${mail.from.username}")
	private String mailFromUsername;
	@Value("${mail.from.bccusername}")
	private String mailFromBccUsername;

	@Override
	public List<String> getProfileType() {
		
		return storeDao.getProfileType();
	}

	@Override
	public long createNewStore(StoreCreator storeCreator) {
		
	 //prepare entity classes to create new store
	StoreProfile storeProfile=storeCreator.getStoreProfile();
	storeProfile.setCurrentAcademicYear(new Date());
	StoreAddress storeAddress=storeCreator.getStoreAddress();
    
	
	//login Profile data
	loginProfile.setUserName(storeProfile.getEmailPrimary());
	loginProfile.setPassChangeInd('N');
	loginProfile.setRole("ADMIN");
	loginProfile.setSecurityRoleIds("1");
	loginProfile.setIsLocked('N');
	loginProfile.setFailedLoginAttemps(0);
	loginProfile.setIsActive('Y');
	
	
	//login credentials
	loginCredentials.setUserName(storeProfile.getEmailPrimary());
	loginCredentials.setPassword(passwordGenerator.generatePassword(10));
	loginCredentials.setEnabled(true);
	
    
    long result=storeDao.createNewStore(storeProfile,loginProfile,storeAddress,loginCredentials);
   // System.out.println(result);
    
    //Mail Sending logic
    String mailData = "<html><body>"
			   + "<p>  "+"Hi, "+storeProfile.getStoreName()+" "+"</p><br>"
			   		+ "<p style="+"coloer:blue"+">Your Store details :</p>"
			   		+ "<p style=\"+\"coloer:blue\"+\">Login Url : http://apps.zambientsystems.com:8080/POIZIN-STORE/admin</p>"
			   		+ "<p style=\"+\"coloer:blue\"+\">User Name : "+storeProfile.getEmailPrimary()+"</p>"
			   		+ "<p style=\"+\"coloer:blue\"+\">Password : "+loginCredentials.getPassword()+"</p>"
			   				+ "<br>"
			   				+ "<br>"
			   				+ "<br><br>"
			   				+ "<p>"+"Thanks & Regards,"+"</p>"
			   						+ "<p>Zambient Team</p>";
			     
    if(result>0) {
    new SendEmail(mailFromUsername, "", storeProfile.getEmailPrimary(), storeProfile.getStoreName()+" Store Registration Successful.",mailData, poizinMail).start();
    }
		return result;
	}

	@Override
	public String fetchStore(String userName,HttpServletRequest request) {
		List result=storeDao.fetchStore(userName);
		 String URI = request.getRequestURI();
		System.out.println("request url is :"+URI);
		
		 String mailData = "<html><body>"
				        +"<p>Update your password </p>"
				   		+ "<p style=\"+\"coloer:blue\"+\">Forgot Password Url :"+"http://apps."+URI+"</p>"
				   		+ "<p style=\"+\"coloer:blue\"+\">For the User : "+userName+"</p>"
				   				+ "<br>"
				   				+ "<br>"
				   				+ "<br><br>"
				   				+ "<p>"+"Thanks & Regards,"+"</p>"
				   						+ "<p>Zambient Team</p>";
		
		if(result.size()>0) {
			String storeName="- - -";
			 for(Object listData:result){
	        	   Map resultMap=(Map)listData;
	        	   storeName= (String) resultMap.get("store_name");
	           }
			
			
			
			
		    new SendEmail(mailFromUsername, "", userName.trim(), storeName+" Store Forgot Password.",mailData, poizinMail).start();

		}
		
		return null;
	}

}
