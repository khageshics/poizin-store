
package com.zambient.poizon.store.profile;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zambient.poizon.security.entity.StoreAddress;
import com.zambient.poizon.security.entity.StoreProfile;
import com.zambient.poizon.store.service.StoreServiceImpl;
import com.zambient.poizon.store.wrapper.StoreCreator;



/**
 * <h1>StoreProfileController</h1>
 * <p> controller is used to map all Store Admin URI's to respected APIs</p>
 * 
 * @author Durga Rao Maruboina
 * @version 1.0
 * @since 2020
 * 
 * */


@Controller
@RequestMapping(value="/store")
public class StoreProfileController {

	private static Logger logger=Logger.getLogger(StoreProfileController.class);
	
	
	@Autowired
	private StoreServiceImpl storeService;
	
	
	/**
	 * This method is used to redict the request to 'StorePortal' jsp view page.
	 * @return String it will redirect to StoreAdmin screen
	 * */
	@RequestMapping(value="/portal")
	public String storePortal() {
		return "storePortal";
	}
	
	/**
	 * This method is used to create new Store in the Database based on <code>storeCreator</code>
	 * 
	 * @param storeCreator
	 * @return String response with status code.
	 * */
	@RequestMapping(value="/createStore",method=RequestMethod.POST,consumes="application/json")
	public ResponseEntity<String> createStore(@RequestBody StoreCreator storeCreator) {
		System.out.println("Entered into the create Store");
		
		
		
		System.out.println(storeCreator);
		long result=storeService.createNewStore(storeCreator);
		System.out.println("controller result is "+result);
		
		if(result>0) {
		return new ResponseEntity<String>("Store Created Successfully",HttpStatus.ACCEPTED);
		}else if(result==-2) {
			return new ResponseEntity<String>("Store Already Existed",HttpStatus.ACCEPTED);
		}else {
			return new ResponseEntity<String>("Something went wrong",HttpStatus.BAD_REQUEST);
		}
	}
	
	
	@RequestMapping(value="/getProfileType",method=RequestMethod.GET)
	public ResponseEntity<List<String>> getProfileType(){
		System.out.println("getProfileType..............");
		
		List<String> result=storeService.getProfileType();
		if(result.size()>0) {
			return new ResponseEntity<List<String>>(result,HttpStatus.OK);
		}else {
			return new ResponseEntity<List<String>>(result,HttpStatus.NO_CONTENT);	
		}
		
		
	}
	
	
	@RequestMapping(value="/forgotPassword")
	public String forgotPassword() {
		System.out.println("forgotPassword Request processed");
		return "forgotPassword";
	}
	
	
	@RequestMapping(value="/forgotPProcess/{userName:.+}")
	public String processForgotPassword(@PathVariable("userName") String userName,HttpServletRequest request) {
		
		//storeService.fetchStore(userName,request);
		
     System.out.println(userName);
		return "";
	}
	

	
}




	
	


