package com.zambient.poizon.store.dao;

import java.util.List;

import com.zambient.poizon.security.entity.LoginCredentials;
import com.zambient.poizon.security.entity.LoginProfile;
import com.zambient.poizon.security.entity.StoreAddress;
import com.zambient.poizon.security.entity.StoreProfile;
import com.zambient.poizon.store.wrapper.StoreCreator;

/**
 * <h1>StoreDao</h1>
 * <p>This interface used to declare all store related curd api methods</p>
 * @author Durga Rao Maruboina
 * @version 1.0
 * @since 2020
 * 
 * */

public interface StoreDao {
	
	public List<String> getProfileType();
	public long createNewStore(StoreProfile storeProfile,LoginProfile loginProfile,StoreAddress storeAddress,LoginCredentials loginCredentials);
	
	public List fetchStore(String userName);

}
