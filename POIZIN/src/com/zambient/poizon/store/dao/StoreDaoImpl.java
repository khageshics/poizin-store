package com.zambient.poizon.store.dao;

import java.util.List;
import java.util.Map;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.TypedQuery;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.zambient.poizon.security.entity.LoginCredentials;
import com.zambient.poizon.security.entity.LoginProfile;
import com.zambient.poizon.security.entity.StoreAddress;
import com.zambient.poizon.security.entity.StoreProfile;
import com.zambient.poizon.store.entity.StoreType;
import com.zambient.poizon.store.wrapper.StoreCreator;


/**
 * 
 * <h1>StoreDaoImpl</h1>
 * <p>This class is used to implement curd operations which are related to store creation</p>
 * @author Durga Rao Maruboina
 * @version 1.0
 * @since 2020
 * 
 * */




@Repository
public class StoreDaoImpl implements StoreDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	/**
	 * This method is used to fetch all profileType details
	 * */
	@Override
	public List<String> getProfileType() {
	    
		Session session=sessionFactory.openSession();
		
		/*
		 * Criteria criteria=session.createCriteria(StoreType.class);
		 * criteria.setProjection(Projections.projectionList()
		 * .add(Projections.property("profileType"),"profileType"))
		 * .setResultTransformer(Transformers.aliasToBean(StoreType.class));
		 */
		List<String> result=session.createSQLQuery("SELECT category FROM store_type").list();
		session.close();
		
	return result;
		
	}


	@Override
	public long createNewStore(StoreProfile storeProfile, LoginProfile loginProfile, StoreAddress storeAddress,
			LoginCredentials loginCredentials) {
		
		
		Session parentSession=sessionFactory.openSession();
		Query query=parentSession.createQuery("FROM com.zambient.poizon.security.entity.StoreProfile credentials WHERE credentials.emailPrimary = :userName");
		query.setParameter("userName",storeProfile.getEmailPrimary().trim());
		List resultList = query.list();
		parentSession.close();
if (resultList.size() > 0) {
System.out.println("User Already Existed");
  return -2;
}else {
		
		
		
		  Session session= sessionFactory.openSession();
		  session.beginTransaction();
		  
		  session.save(storeProfile);
		  
		  storeAddress.setStoreId(storeProfile);
		  loginProfile.setStoreProfile(storeProfile);
		
		  session.save(storeAddress);
		  session.save(loginProfile);
		  
		  loginCredentials.setLoginProfile(loginProfile);
		  int id= (int) session.save(loginCredentials);
		 
		  session.getTransaction().commit();
		  session.close();
	
		return id;
}
	}


	@Override
	public List fetchStore(String userName) {
		Session session=sessionFactory.openSession();
		Query query=session.createSQLQuery("SELECT * FROM store_profile WHERE email_addr1 = :email");
		query.setString("email", userName.trim());
		query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		List result=query.list();
		
		
		
		if(result.size()>0) {
			System.out.println("User Found");
			
		}
	
		
		session.close();
		
		
		return result;
	}


	


	

}
