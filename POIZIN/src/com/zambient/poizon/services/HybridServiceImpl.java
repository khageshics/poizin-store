package com.zambient.poizon.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zambient.poizon.bean.BalanceSheetBean;
import com.zambient.poizon.bean.CompareSaleBean;
import com.zambient.poizon.bean.DiscountCalculationBean;
import com.zambient.poizon.bean.DiscountEstimationBean;
import com.zambient.poizon.bean.SaleListBean;
import com.zambient.poizon.dao.HybridDao;

@Service ("hybridService")
public class HybridServiceImpl implements HybridService{

	@Autowired
	HybridDao hybridDao;
    
	@Override
	public String appUserLogin() {
		// TODO Auto-generated method stub
		return hybridDao.appUserLogin();
	}

	@Override
	public BalanceSheetBean getLatestDayDiff() {
		// TODO Auto-generated method stub
		return hybridDao.getLatestDayDiff();
	}

	@Override
	public String getProductsForIndent() {
		// TODO Auto-generated method stub
		return hybridDao.getProductsForIndent();
	}

	@Override
	public SaleListBean generateSaleDataPdf(String saledate) {
		// TODO Auto-generated method stub
		return hybridDao.generateSaleDataPdf(saledate);
	}

	@Override
	public List<DiscountCalculationBean> getDiscountCalculationList(String month) {
		// TODO Auto-generated method stub
		return hybridDao.getDiscountCalculationList(month);
	}

	@Override
	public List<CompareSaleBean> getWeeklyOrMonthlySaleComparisionData(String startDate, String endDate, int id) {
		// TODO Auto-generated method stub
		return hybridDao.getWeeklyOrMonthlySaleComparisionData(startDate, endDate, id);
	}
}
