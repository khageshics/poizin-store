package com.zambient.poizon.services;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zambient.poizon.bean.AddNewBrandBean;
import com.zambient.poizon.bean.BalanceSheetWithBreckage;
import com.zambient.poizon.bean.CallDiscountBean;
import com.zambient.poizon.bean.CardCashSaleBeen;
import com.zambient.poizon.bean.ChartDataBean;
import com.zambient.poizon.bean.CreditMasterBean;
import com.zambient.poizon.bean.CreditPaymentBean;
import com.zambient.poizon.bean.DiscountEstimationBean;
import com.zambient.poizon.bean.DiscountEstimationWithDate;
import com.zambient.poizon.bean.DropDownsBean;
import com.zambient.poizon.bean.ExpenseBean;
import com.zambient.poizon.bean.ExpenseCategoryAndDate;
import com.zambient.poizon.bean.InvoiceDateBean;
import com.zambient.poizon.bean.InvoiceReportWithMrpRoundOffBean;
import com.zambient.poizon.bean.MessageBean;
import com.zambient.poizon.bean.NewSaleBean;
import com.zambient.poizon.bean.NoDiscountProductsBean;
import com.zambient.poizon.bean.PendingLiftedCaseDiscountBean;
import com.zambient.poizon.bean.SaleAndAmmountBean;
import com.zambient.poizon.bean.SaleBean;
import com.zambient.poizon.bean.SaleBeanAndCashCardExpensesBean;
import com.zambient.poizon.bean.SchemeEstimateBean;
import com.zambient.poizon.bean.TotalPriceDateWiseBean;
import com.zambient.poizon.bean.UpdateExpensesBean;
import com.zambient.poizon.dao.PoizonDao;

@Service ("poizonService")
public class PoizonServiceImpl implements PoizonService {
	
	@Autowired
	PoizonDao poizonDao;

	@Override
	public String addNewBrandName(AddNewBrandBean addNewBrandBean) {
		// TODO Auto-generated method stub
		return poizonDao.addNewBrandName(addNewBrandBean);
	}

	@Override
	public List<AddNewBrandBean> getBrandDetails() {
		// TODO Auto-generated method stub
		return poizonDao.getBrandDetails();
	}

	@Override
	public List<AddNewBrandBean> getSingleBrandDetail(String brandNoPackQty) {
		// TODO Auto-generated method stub
		return poizonDao.getSingleBrandDetail(brandNoPackQty);
	}

	@Override
	public String authenticateAdmin() {
		// TODO Auto-generated method stub
		return poizonDao.authenticateAdmin();
	}
	@Override
	public SaleAndAmmountBean getSingleSaleBrandDetailForClosing() {
		// TODO Auto-generated method stub
		return poizonDao.getSingleSaleBrandDetailForClosing();
	}
	@Override
	public String saveSingleSaleBrandDetail(JSONArray saleDetailsJson) {
		// TODO Auto-generated method stub
		return poizonDao.saveSingleSaleBrandDetail(saleDetailsJson);
	}

	@Override
	public List<InvoiceDateBean> getTotalInvoiceDate() {
		// TODO Auto-generated method stub
		return poizonDao.getTotalInvoiceDate();
	}

	@Override
	public InvoiceReportWithMrpRoundOffBean getSingleInvoiceDetailDateWise(String idate) {
		// TODO Auto-generated method stub
		return poizonDao.getSingleInvoiceDetailDateWise(idate);
	}

	@Override
	public List<SaleBean> getProfitPercentage() {
		// TODO Auto-generated method stub
		return poizonDao.getProfitPercentage();
	}

	@Override
	public List<SaleBean> getStockLifting(String newSdate,String newEdate) {
		// TODO Auto-generated method stub
		return poizonDao.getStockLifting(newSdate, newEdate);
	}

	@Override
	public String saveInvoiceJsonData(JSONArray invoiceJson) {
		// TODO Auto-generated method stub
		return poizonDao.saveInvoiceJsonData(invoiceJson);
	}

	@Override
	public List<SaleBean> getTopSaleDetails(String startDate,String endDate,int type) {
		// TODO Auto-generated method stub
		return poizonDao.getTopSaleDetails(startDate,endDate,type);
	}

	@Override
	public SaleAndAmmountBean getSaleReports(String newSdate, String newEdate) {
		// TODO Auto-generated method stub
		return poizonDao.getSaleReports(newSdate, newEdate);
	}

	@Override
	public List<InvoiceDateBean> getTotalSaleDate() {
		// TODO Auto-generated method stub
		return poizonDao.getTotalSaleDate();
	}

	@Override
	public ExpenseCategoryAndDate getExpenseCategory() {
		// TODO Auto-generated method stub
		return poizonDao.getExpenseCategory();
	}

	@Override
	public String saveExpenses(JSONArray expenseJson) {
		// TODO Auto-generated method stub
		return poizonDao.saveExpenses(expenseJson);
	}

	@Override
	public List<SaleBean> getUpdateSale() {
		// TODO Auto-generated method stub
		return poizonDao.getUpdateSale();
	}

	@Override
	public String updateSaleDetails(JSONArray saleDetailsJson) {
		// TODO Auto-generated method stub
		return poizonDao.updateSaleDetails(saleDetailsJson);
	}

	@Override
	public CardCashSaleBeen getCashCardSale() {
		// TODO Auto-generated method stub
		return poizonDao.getCashCardSale();
	}

	@Override
	public String saveCardCashSale(CardCashSaleBeen cardCashSaleBeen) {
		// TODO Auto-generated method stub
		return poizonDao.saveCardCashSale(cardCashSaleBeen);
	}


	@Override
	public SaleBeanAndCashCardExpensesBean getSaleDayWiseReports(String startDate) {
		// TODO Auto-generated method stub
		return poizonDao.getSaleDayWiseReports(startDate);
	}

	@Override
	public String getCashCardSaleDate() {
		// TODO Auto-generated method stub
		return poizonDao.getCashCardSaleDate();
	}


	@Override
	public UpdateExpensesBean getExpensesData() {
		// TODO Auto-generated method stub
		return poizonDao.getExpensesData();
	}

	@Override
	public String updateUpdateExpenses(JSONArray expenseJson) {
		// TODO Auto-generated method stub
		return poizonDao.updateUpdateExpenses(expenseJson);
	}

	@Override
	public List<SaleBean> getSingleSaleDetailForUpdate(String brandNoAndPackQty, String date) {
		// TODO Auto-generated method stub
		return poizonDao.getSingleSaleDetailForUpdate(brandNoAndPackQty, date);
	}

	@Override
	public List<SaleBean> getInHouseStockValue(String startDate) {
		// TODO Auto-generated method stub
		return poizonDao.getInHouseStockValue(startDate);
	}

	@Override
	public BalanceSheetWithBreckage getBalanceSheet(String sDate, String eDate) {
		// TODO Auto-generated method stub
		return poizonDao.getBalanceSheet(sDate,eDate);
	}

	@Override
	public List<SaleBean> getDetailsforEditMrp() {
		// TODO Auto-generated method stub
		return poizonDao.getDetailsforEditMrp();
	}

	@Override
	public List<SaleBean> getProductComparision( String brandNoPackQtys, String startdate,
			String enddate) {
		// TODO Auto-generated method stub
		return poizonDao.getProductComparision(brandNoPackQtys, startdate, enddate);
	}

	@Override
	public List<NewSaleBean> getNewSaleReports(String newSdate, String newEdate) {
		// TODO Auto-generated method stub
		return poizonDao.getNewSaleReports(newSdate, newEdate);
	}

	@Override
	public List<SaleBean> getStockLiftGraphData(String startDate, String endDate) {
		// TODO Auto-generated method stub
		return poizonDao.getStockLiftGraphData(startDate, endDate);
	}

	/*@Override
	public List<SaleBean> getSameRangeProducts(int unitPrice,String category,String qty) {
		// TODO Auto-generated method stub
		return poizonDao.getSameRangeProducts(unitPrice,category,qty);
	}*/
	@Override
	public List<SaleBean> getSameRangeProducts(int matchval,String qty) {
		// TODO Auto-generated method stub
		return poizonDao.getSameRangeProducts(matchval,qty);
	}

	@Override
	public List<ExpenseBean> getExpenseDetails(String expenseMasterID) {
		// TODO Auto-generated method stub
		return poizonDao.getExpenseDetails(expenseMasterID);
	}

	@Override
	public DiscountEstimationWithDate getDiscountEsitmateData() {
		// TODO Auto-generated method stub
		return poizonDao.getDiscountEsitmateData();
	}

	@Override
	public List<ChartDataBean> getBeerCasesGraphData(String sdate, String edate) {
		// TODO Auto-generated method stub
		return poizonDao.getBeerCasesGraphData(sdate, edate);
	}

	@Override
	public List<ChartDataBean> getLiquorCasesGraphData(String sdate, String edate) {
		// TODO Auto-generated method stub
		return poizonDao.getLiquorCasesGraphData(sdate, edate);
	}

	@Override
	public String saveDiscountEsitmateData(JSONArray discountDetails) {
		// TODO Auto-generated method stub
		return poizonDao.saveDiscountEsitmateData(discountDetails);
	}

	@Override
	public List<InvoiceDateBean> getTotalInvoiceDateAsPerSheet() {
		// TODO Auto-generated method stub
		return poizonDao.getTotalInvoiceDateAsPerSheet();
	}

	@Override
	public List<TotalPriceDateWiseBean> getSaleAmountMonthWise() {
		// TODO Auto-generated method stub
		return poizonDao.getSaleAmountMonthWise();
	}

	@Override
	public DiscountEstimationWithDate getDiscountEsitmateDataWithDays(int days,String date) {
		// TODO Auto-generated method stub
		return poizonDao.getDiscountEsitmateDataWithDays(days,date);
	}

	@Override
	public DiscountEstimationBean getDiscountEsitmateDataWithBrandNo(int brandNoVal, String inputdate, int noOfDays) {
		// TODO Auto-generated method stub
		return poizonDao.getDiscountEsitmateDataWithBrandNo(brandNoVal,inputdate,noOfDays);
	}

	@Override
	public String saveTempIndent(JSONArray indentDetailsJson) {
		// TODO Auto-generated method stub
		return poizonDao.saveTempIndent(indentDetailsJson);
	}

	@Override
	public MessageBean getDateAndDays() {
		// TODO Auto-generated method stub
		return poizonDao.getDateAndDays();
	}

	@Override
	public DropDownsBean getDropDownListFromProduct() {
		// TODO Auto-generated method stub
		return poizonDao.getDropDownListFromProduct();
	}

	@Override
	public DiscountEstimationWithDate downloadIndentProduct(int days, String date) {
		// TODO Auto-generated method stub
		return poizonDao.downloadIndentProduct(days, date);
	}

	@Override
	public List<InvoiceDateBean> getSaleReportSaleDate() {
		// TODO Auto-generated method stub
		return poizonDao.getSaleReportSaleDate();
	}

	@Override
	public boolean saveSaleWithZeroOrNot(String date) {
		// TODO Auto-generated method stub
		return poizonDao.saveSaleWithZeroOrNot(date);
	}

	@Override
	public String updateLatestMrpForAllProducts(JSONArray invoiceJson) {
		// TODO Auto-generated method stub
		return poizonDao.updateLatestMrpForAllProducts(invoiceJson);
	}

	@Override
	public List<SchemeEstimateBean> getSchemeData(String date) {
		// TODO Auto-generated method stub
		return poizonDao.getSchemeData(date);
	}

	@Override
	public String saveTabExpenses(JSONObject expenseJson) {
		// TODO Auto-generated method stub
		return poizonDao.saveTabExpenses(expenseJson);
	}

	@Override
	public double getSingleProductDetails(int brandNoPackQty, String date) {
		// TODO Auto-generated method stub
		return poizonDao.getSingleProductDetails(brandNoPackQty, date);
	}

	@Override
	public String postCreditData(JSONObject creditJson) {
		// TODO Auto-generated method stub
		return poizonDao.postCreditData(creditJson);
	}

	@Override
	public List<CreditMasterBean> getExistingCredit() {
		// TODO Auto-generated method stub
		return poizonDao.getExistingCredit();
	}

	@Override
	public String updateExistingCredit(CreditPaymentBean creditPaymentBean) {
		// TODO Auto-generated method stub
		return poizonDao.updateExistingCredit(creditPaymentBean);
	}

	@Override
	public List<CreditMasterBean> getcompanyWiseExistingCredit(int company, String date) {
		// TODO Auto-generated method stub
		return poizonDao.getcompanyWiseExistingCredit(company, date);
	}

	@Override
	public List<NoDiscountProductsBean> remindLiftedCaseToUpdateDiscount(String date) {
		// TODO Auto-generated method stub
		return poizonDao.remindLiftedCaseToUpdateDiscount(date);
	}

	@Override
	public List<InvoiceDateBean> getUnionSaleAndInvoiceDate() {
		// TODO Auto-generated method stub
		return poizonDao.getUnionSaleAndInvoiceDate();
	}

}
