package com.zambient.poizon.services;

import java.util.List;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zambient.poizon.bean.AddNewBrandBean;
import com.zambient.poizon.bean.BankCreditAmountBean;
import com.zambient.poizon.bean.CallDiscountBean;
import com.zambient.poizon.bean.CommentForSaleBean;
import com.zambient.poizon.bean.DiscountAndMonthBean;
import com.zambient.poizon.bean.DiscountAsPerCompanyBeen;
import com.zambient.poizon.bean.DiscountEstimationBean;
import com.zambient.poizon.bean.DiscountEstimationDetails;
import com.zambient.poizon.bean.DiscountTransactionBean;
import com.zambient.poizon.bean.EditProductBean;
import com.zambient.poizon.bean.ExpenseCategoryAndDate;
import com.zambient.poizon.bean.ExpenseCategoryBean;
import com.zambient.poizon.bean.InvestmentBean;
import com.zambient.poizon.bean.InvoiceDateBean;
import com.zambient.poizon.bean.MonthlyExpensesBean;
import com.zambient.poizon.bean.MrpRoundOffBean;
import com.zambient.poizon.bean.PieChartBean;
import com.zambient.poizon.bean.ProductListBean;
import com.zambient.poizon.bean.ProductOrderBy;
import com.zambient.poizon.bean.ProductPerformanceBean;
import com.zambient.poizon.bean.ProductPerformanceWithComaparisonBean;
import com.zambient.poizon.bean.ProductWithStatement;
import com.zambient.poizon.bean.ProfitAndLossBean;
import com.zambient.poizon.bean.SaleWithDiscountBean;
import com.zambient.poizon.bean.StockLiftCompanyWithChecksBean;
import com.zambient.poizon.bean.StockLiftWithDiscountEstimate;
import com.zambient.poizon.bean.TillDateBalanceSheetChart;
import com.zambient.poizon.bean.TillMonthStockLiftWithDiscount;
import com.zambient.poizon.bean.UploadAndViewWithYearBean;
import com.zambient.poizon.bean.VendorAdjustmentBean;
import com.zambient.poizon.bean.addNewIndentBean;
import com.zambient.poizon.bean.category;
import com.zambient.poizon.bean.companyBean;
import com.zambient.poizon.dao.PoizonSecondDao;

@Service ("poizonSecondService")
public class PoizonSecondServiceImpl implements PoizonSecondService{
	
	@Autowired
	PoizonSecondDao poizonSecondDao;

	@Override
	public List<InvoiceDateBean> getTotalDiscountDate() {
		// TODO Auto-generated method stub
		return poizonSecondDao.getTotalDiscountDate();
	}

	@Override
	public List<DiscountEstimationBean> getbuildDiscountIntentData(String sdate) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getbuildDiscountIntentData(sdate);
	}

	@Override
	public List<StockLiftWithDiscountEstimate> getStockLiftingWithDiscount(String date) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getStockLiftingWithDiscount(date);
	}

	@Override
	public String saveStockLiftWithDiscountData(JSONArray discountDetails,String strDate) {
		// TODO Auto-generated method stub
		return poizonSecondDao.saveStockLiftWithDiscountData(discountDetails, strDate);
	}

	@Override
	public String saveBankCreditAmount(BankCreditAmountBean bankCreditAmountBean) {
		// TODO Auto-generated method stub
		return poizonSecondDao.saveBankCreditAmount(bankCreditAmountBean);
	}
	@Override
	public List<DiscountAsPerCompanyBeen> getCompanyWiseDiscountAmount(String strDate) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getCompanyWiseDiscountAmount(strDate);
	}

	@Override
	public String addOrderForAProduct(ProductOrderBy productOrderBy) {
		// TODO Auto-generated method stub
		return poizonSecondDao.addOrderForAProduct(productOrderBy);
	}

	@Override
	public String addCommentForSale(CommentForSaleBean commentForSaleBean) {
		// TODO Auto-generated method stub
		return poizonSecondDao.addCommentForSale(commentForSaleBean);
	}

	@Override
	public CommentForSaleBean getSaleCommentData(String date) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getSaleCommentData(date);
	}

	@Override
	public String saveMrpRoundOff(MrpRoundOffBean mrpRoundOffBean) {
		// TODO Auto-generated method stub
		return poizonSecondDao.saveMrpRoundOff(mrpRoundOffBean);
	}

	@Override
	public List<AddNewBrandBean> getBrandDetailsForProductOrder() {
		// TODO Auto-generated method stub
		return poizonSecondDao.getBrandDetailsForProductOrder();
	}

	@Override
	public List<companyBean> getCompanyList() {
		// TODO Auto-generated method stub
		return poizonSecondDao.getCompanyList();
	}

	@Override
	public List<companyBean> getCategoryList() {
		// TODO Auto-generated method stub
		return poizonSecondDao.getCategoryList();
	}

	@Override
	public String saveMonthlyExpensesData(JSONArray expensesJson) {
		// TODO Auto-generated method stub
		return poizonSecondDao.saveMonthlyExpensesData(expensesJson);
	}

	@Override
	public List<MonthlyExpensesBean> getMonthlyExpenses(String expensesdate) {
		return poizonSecondDao.getMonthlyExpenses(expensesdate);
	}

	@Override
	public String deleteMonthlyExpenses(int expenseMonthlyId) {
		// TODO Auto-generated method stub
		return poizonSecondDao.deleteMonthlyExpenses(expenseMonthlyId);
	}

	@Override
	public List<ExpenseCategoryBean> getMonthlyExpenseCategory() {
		// TODO Auto-generated method stub
		return poizonSecondDao.getMonthlyExpenseCategory();
	}
	@Override
	public List<ProductPerformanceBean> getProductPerformance(int brandNo) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getProductPerformance(brandNo);
	}

	@Override
	public List<AddNewBrandBean> getDistinctBrandNameAndNo() {
		return poizonSecondDao.getDistinctBrandNameAndNo();
	}

	@Override
	public List<TillMonthStockLiftWithDiscount> getTillMonthStockLiftWithDiscount(String date, int company) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getTillMonthStockLiftWithDiscount(date,company);
	}

	@Override
	public double getLastDateCredit(String date) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getLastDateCredit(date);
	}


	@Override
	public String addNewTransaction(DiscountTransactionBean discountTransactionBean) {
		// TODO Auto-generated method stub
		return poizonSecondDao.addNewTransaction(discountTransactionBean);
	}

	@Override
	public List<DiscountTransactionBean> getDiscountTransactionDetails(String date, int company) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getDiscountTransactionDetails(date, company);
	}

	@Override
	public String deleteDiscountTransactionDetails(int id) {
		// TODO Auto-generated method stub
		return poizonSecondDao.deleteDiscountTransactionDetails(id);
	}

	@Override
	public List<SaleWithDiscountBean> getSaleWithDiscount(String startdate,String enddate) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getSaleWithDiscount(startdate,enddate);
	}

	@Override
	public List<StockLiftCompanyWithChecksBean> getStockLiftWithDiscountTransactionDetails(String date) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getStockLiftWithDiscountTransactionDetails(date);
	}

	@Override
	public List<DiscountAndMonthBean> getDiscountCompanyWiseForAllMonth(String date, int company) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getDiscountCompanyWiseForAllMonth(date, company);
	}

	@Override
	public List<DiscountAsPerCompanyBeen> getDiscountCompanyWiseForAllMonthWithReceived(int company) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getDiscountCompanyWiseForAllMonthWithReceived(company);
	}

	@Override
	public double getUpToMonthRental(String date, int company) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getUpToMonthRental(date, company);
	}

	@Override
	public String addNewInvestment(InvestmentBean investmentBean) {
		// TODO Auto-generated method stub
		return poizonSecondDao.addNewInvestment(investmentBean);
	}

	@Override
	public List<InvestmentBean> getInvestmentData(String date) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getInvestmentData(date);
	}

	@Override
	public ProductWithStatement getProductWithInvestment(long brandNo) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getProductWithInvestment(brandNo);
	}

	@Override
	public List<DiscountEstimationDetails> getDiscountEsitmateDetails(String date) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getDiscountEsitmateDetails(date);
	}

	@Override
	public String saveDiscountEsitmateDetails(JSONArray discountDetails) {
		// TODO Auto-generated method stub
		return poizonSecondDao.saveDiscountEsitmateDetails(discountDetails);
	}

	@Override
	public ProfitAndLossBean getProfitAndLossData(String firstDate, String lastDate) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getProfitAndLossData(firstDate, lastDate);
	}

	@Override
	public List<ProductPerformanceWithComaparisonBean> getProductComparisionWithPerformance(String startDate,
			String endDate, int category) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getProductComparisionWithPerformance(startDate, endDate, category);
	}

	@Override
	public category getCompanyWithEnterPrice(int company) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getCompanyWithEnterPrice(company);
	}

	@Override
	public List<ExpenseCategoryAndDate> getExpenseDataByCategory(String startDate, String endDate, int[] expenseId) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getExpenseDataByCategory(startDate, endDate, expenseId);
	}

	@Override
	public List<ProductListBean> getProductList() {
		// TODO Auto-generated method stub
		return poizonSecondDao.getProductList();
	}

	@Override
	public String setActiveOrInActive(double brandNoPackQty, int activaVal) {
		// TODO Auto-generated method stub
		return poizonSecondDao.setActiveOrInActive(brandNoPackQty, activaVal);
	}

	@Override
	public List<DiscountEstimationDetails> getNewDiscountEsitmateDetails(String date) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getNewDiscountEsitmateDetails(date);
	}

	@Override
	public List<EditProductBean> editProductDetails(int brandNo) {
		// TODO Auto-generated method stub
		return poizonSecondDao.editProductDetails(brandNo);
	}

	@Override
	public String updateSingleProductDetails(JSONArray jsonData) {
		// TODO Auto-generated method stub
		return poizonSecondDao.updateSingleProductDetails(jsonData);
	}

	@Override
	public List<PieChartBean> getSaleByBrandNoPackQty(double brandNoPackQty, String sdate, String edate) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getSaleByBrandNoPackQty(brandNoPackQty, sdate, edate);
	}

	@Override
	public List<AddNewBrandBean> getProductListWithShortName(double brandNopackQty, int categoryId) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getProductListWithShortName(brandNopackQty,categoryId);
	}

	@Override
	public TillDateBalanceSheetChart getMultiChartBeanSaleData(String sdate, String edate, int[] brandNoPackQty) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getMultiChartBeanSaleData(sdate, edate, brandNoPackQty);
	}

	@Override
	public String addNewIndentFile(addNewIndentBean indentBean) {
		// TODO Auto-generated method stub
		return poizonSecondDao.addNewIndentFile(indentBean);
	}

	@Override
	public List<UploadAndViewWithYearBean> getIndentListBasedOnMonth() {
		// TODO Auto-generated method stub
		return poizonSecondDao.getIndentListBasedOnMonth();
	}

	@Override
	public List<DiscountAsPerCompanyBeen> getDiscountForAllCompanies(String month) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getDiscountForAllCompanies(month);
	}

	@Override
	public List<DiscountAndMonthBean> getDiscountForSelectedMonth(String startMonth, String endMonth, int company) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getDiscountForSelectedMonth(startMonth, endMonth, company);
	}

	@Override
	public double getRentalForSelected(String startMonth, String endMonth, int company) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getRentalForSelected(startMonth, endMonth, company);
	}

	@Override
	public List<DiscountAsPerCompanyBeen> getDiscountChecksForSelectedMonths(String startMonth, String endMonth,
			int company) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getDiscountChecksForSelectedMonths(startMonth, endMonth, company);
	}

	@Override
	public PieChartBean getArrearsForPreviousDays(String startMonth, int company) {
		// TODO Auto-generated method stub
		return poizonSecondDao.getArrearsForPreviousDays(startMonth, company);
	}

	@Override
	public String saveVendorAdjustment(VendorAdjustmentBean bean, String company) {
		// TODO Auto-generated method stub
		return poizonSecondDao.saveVendorAdjustment(bean,company);
	}
	@Override
	public List<CallDiscountBean> callForDiscountData(String month) {
		// TODO Auto-generated method stub
		return poizonSecondDao.callForDiscountData(month);
	}

	@Override
	public String saveGroupingDiscountEstimateData(JSONArray discountDetails, String date) {
		// TODO Auto-generated method stub
		return poizonSecondDao.saveGroupingDiscountEstimateData(discountDetails, date);
	}

	@Override
	public String saveSchemeListData(JSONArray schemeJson, String date) {
		// TODO Auto-generated method stub
		return poizonSecondDao.saveSchemeListData(schemeJson, date);
	}

	@Override
	public List<companyBean> getGroupList() {
		// TODO Auto-generated method stub
		return poizonSecondDao.getGroupList();
	}

}
