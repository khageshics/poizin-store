package com.zambient.poizon.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zambient.poizon.bean.CardCashSaleBeen;
import com.zambient.poizon.bean.DateAndIndentBean;
import com.zambient.poizon.bean.DiscountAndMonthBean;
import com.zambient.poizon.bean.DiscountAsPerCompanyBeen;
import com.zambient.poizon.bean.DiscountTransactionBean;
import com.zambient.poizon.bean.ExpenseCategoryAndDate;
import com.zambient.poizon.bean.HomeBean;
import com.zambient.poizon.bean.MobileViewStockLiftingDiscount;
import com.zambient.poizon.bean.RentalAndBreakageBean;
import com.zambient.poizon.bean.TargetWithBudget;
import com.zambient.poizon.bean.TotalPriceDateWiseBean;
import com.zambient.poizon.bean.VendorAdjustmentGetArrearsBean;
import com.zambient.poizon.dao.PoizonThirdDao;

@Service ("poizonThirdService")
public class PoizonThirdServiceImpl implements PoizonThirdService{

	@Autowired
	PoizonThirdDao poizonThirdDao;

	@Override
	public HomeBean getHomePageDetails() {
		// TODO Auto-generated method stub
		return poizonThirdDao.getHomePageDetails();
	}

	@Override
	public List<DiscountAsPerCompanyBeen> downLoadCompanyDiscount(int company, String date) {
		// TODO Auto-generated method stub
		return poizonThirdDao.downLoadCompanyDiscount(company,date);
	}

	@Override
	public List<DiscountAndMonthBean> downloadDiscountCompanyWiseForAllMonth(String date, int company) {
		// TODO Auto-generated method stub
		return poizonThirdDao.downloadDiscountCompanyWiseForAllMonth(date, company);
	}

	@Override
	public List<MobileViewStockLiftingDiscount> discountReportForSelectedMonth(String startMonth, String endMonth) {
		// TODO Auto-generated method stub
		return poizonThirdDao.discountReportForSelectedMonth(startMonth, endMonth);
	}

	@Override
	public List<ExpenseCategoryAndDate> getExpenseDataRangeWise(String startDate, String endDate) {
		// TODO Auto-generated method stub
		return poizonThirdDao.getExpenseDataRangeWise(startDate, endDate);
	}

	@Override
	public List<DiscountAsPerCompanyBeen> getHomePageDiscountDetails(int comapnyId) {
		// TODO Auto-generated method stub
		return poizonThirdDao.getHomePageDiscountDetails(comapnyId);
	}

	@Override
	public List<DiscountAsPerCompanyBeen> generateCompanyDiscount(int company, String startMonth, String endMonth) {
		// TODO Auto-generated method stub
		return poizonThirdDao.generateCompanyDiscount(company, startMonth, endMonth);
	}

	@Override
	public CardCashSaleBeen getTotalDiscountAndRental(int company, String startMonth, String endMonth) {
		// TODO Auto-generated method stub
		return poizonThirdDao.getTotalDiscountAndRental(company, startMonth, endMonth);
	}

	@Override
	public List<TotalPriceDateWiseBean> getSaleCanvasPieChart(String startDate, String endDate) {
		// TODO Auto-generated method stub
		return poizonThirdDao.getSaleCanvasPieChart(startDate, endDate);
	}

	@Override
	public VendorAdjustmentGetArrearsBean getExistingSingleCompanyArrears(String discountMonth, String company) {
		// TODO Auto-generated method stub
		return poizonThirdDao.getExistingSingleCompanyArrears(discountMonth, company);
	}

	@Override
	public RentalAndBreakageBean getExistingRentalForAMonth(String company, String month) {
		// TODO Auto-generated method stub
		return poizonThirdDao.getExistingRentalForAMonth(company, month);
	}

	@Override
	public String saveOrUpdateRentalForAMonth(RentalAndBreakageBean bean, String companyId) {
		// TODO Auto-generated method stub
		return poizonThirdDao.saveOrUpdateRentalForAMonth(bean,companyId);
	}

	@Override
	public RentalAndBreakageBean getExistingBreakageForAMonth(String month) {
		// TODO Auto-generated method stub
		return poizonThirdDao.getExistingBreakageForAMonth(month);
	}

	@Override
	public String saveOrUpdateBreakageForAMonth(RentalAndBreakageBean bean) {
		// TODO Auto-generated method stub
		return poizonThirdDao.saveOrUpdateBreakageForAMonth(bean);
	}

	@Override
	public List<DiscountTransactionBean> getAllPendingChecks() {
		// TODO Auto-generated method stub
		return poizonThirdDao.getAllPendingChecks();
	}

	@Override
	public String updateReceivedCheckStatus(int trid, int companyid, String month, String transactionDate, String clearedBank) {
		// TODO Auto-generated method stub
		return poizonThirdDao.updateReceivedCheckStatus(trid, companyid, month, transactionDate, clearedBank);
	}

	@Override
	public DateAndIndentBean downloadendentEstimateDiscountAsPdf(boolean flag) {
		// TODO Auto-generated method stub
		return poizonThirdDao.downloadendentEstimateDiscountAsPdf(flag);
	}

	@Override
	public TargetWithBudget downloadtargetanddiscpercase(String company, String date) {
		// TODO Auto-generated method stub
		return poizonThirdDao.downloadtargetanddiscpercase(company,date);
	}
	
	
	
}
