package com.zambient.poizon.services;

import java.util.List;

import com.zambient.poizon.bean.BalanceSheetBean;
import com.zambient.poizon.bean.CompareSaleBean;
import com.zambient.poizon.bean.DiscountCalculationBean;
import com.zambient.poizon.bean.DiscountEstimationBean;
import com.zambient.poizon.bean.SaleListBean;

public interface HybridService {
	public String appUserLogin();
	public BalanceSheetBean getLatestDayDiff();
	public String getProductsForIndent();
	public SaleListBean generateSaleDataPdf(String saledate);
	public List<DiscountCalculationBean> getDiscountCalculationList(String month);
	public List<CompareSaleBean> getWeeklyOrMonthlySaleComparisionData(String startDate, String endDate, int id);

}
