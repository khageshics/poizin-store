package com.zambient.poizon.services;

import java.util.List;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.zambient.poizon.bean.BalanceSheetWithBreckage;
import com.zambient.poizon.bean.CreditChildBean;
import com.zambient.poizon.bean.CreditMasterBean;
import com.zambient.poizon.bean.MobileViewInHouseStock;
import com.zambient.poizon.bean.MobileViewStockLiftingDiscount;
import com.zambient.poizon.bean.SaleAndAmmountBean;
import com.zambient.poizon.bean.SaleBean;
import com.zambient.poizon.bean.SaleListBean;
import com.zambient.poizon.bean.VendorAdjustmentGetArrearsBean;
import com.zambient.poizon.dao.PoizonMobileUIDao;

@Service ("poizonMobileUIService")
public class PoizonMobileUIServiceImpl implements PoizonMobileUIService{
	
	@Autowired
	PoizonMobileUIDao poizonMobileUIDao;

	@Override
	public List<MobileViewInHouseStock> mobileViewInHouseStock() {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.mobileViewInHouseStock();
	}

	@Override
	public double InHouseStockAmount() {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.InHouseStockAmount();
	}

	@Override
	public List<SaleBean> InHouseStockAmountWithSale() {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.InHouseStockAmountWithSale();
	}

	@Override
	public BalanceSheetWithBreckage dayWiseBalanceSheet(String sdate, String edate) {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.dayWiseBalanceSheet(sdate, edate);
	}

	@Override
	public SaleAndAmmountBean getSaleReports(String newSdate, String newEdate) {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.getSaleReports(newSdate, newEdate);
	}

	@Override
	public List<MobileViewStockLiftingDiscount> stockLiftingWithDiscountReport() {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.stockLiftingWithDiscountReport();
	}

	@Override
	public List<SaleBean> getStockLiftingReports(String date) {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.getStockLiftingReports(date);
	}

	@Override
	public List<SaleBean> downloadIndent() {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.downloadIndent();
	}

	@Override
	public SaleListBean getSaleDetailsForClosing() {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.getSaleDetailsForClosing();
	}

	@Override
	public List<SaleBean> getTillMonthDiscountDetails(String date, int companyId) {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.getTillMonthDiscountDetails(date, companyId);
	}

	@Override
	public List<VendorAdjustmentGetArrearsBean> getVendorDiscount(String date, int companyId) {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.getVendorDiscount(date, companyId);
	}

	@Override
	public String saveSaleDetails(JSONArray saleDetailsJson) {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.saveSaleDetails(saleDetailsJson);
	}

	@Override
	public SaleListBean getSaleForClosingByNewLogic() {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.getSaleForClosingByNewLogic();
	}

	@Override
	public List<CreditChildBean> getVendorCreditListWithRangeWise(String startMonth, String endMonth, int company) {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.getVendorCreditListWithRangeWise(startMonth, endMonth, company);
	}

	@Override
	public CreditMasterBean getTotalCreditAmountAndReceived(String startMonth, String endMonth, int company) {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.getTotalCreditAmountAndReceived(startMonth, endMonth, company);
	}

	@Override
	public List<CreditChildBean> getVendorCreditListToTillMonth(String date, int company) {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.getVendorCreditListToTillMonth(date, company);
	}

	@Override
	public CreditMasterBean getTotalCreditAmountAndReceivedTillMonth(String date, int company) {
		// TODO Auto-generated method stub
		return poizonMobileUIDao.getTotalCreditAmountAndReceivedTillMonth(date, company);
	}

}
