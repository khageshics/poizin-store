package com.zambient.poizon.ocr.exception;

import java.util.List;

public class NewBrandNoException extends RuntimeException{
	private String message;
	private int status;
	private List<Long> newBrands;
	public List<Long> getNewBrands() {
		return newBrands;
	}
	public void setNewBrands(List<Long> newBrands) {
		this.newBrands = newBrands;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	} 

}
