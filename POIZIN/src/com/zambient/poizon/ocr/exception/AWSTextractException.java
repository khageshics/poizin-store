package com.zambient.poizon.ocr.exception;

import org.springframework.stereotype.Component;

@Component
public class AWSTextractException extends RuntimeException{
	
	private static final long serialVersionUID=1L;
	private String message;
	private int status;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
