package com.zambient.poizon.ocr.exception;

import java.util.List;

public class ProductNotFoundExc extends RuntimeException{
	private String message;
	private int status;
	
	public ProductNotFoundExc(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	

}