package com.zambient.poizon.ocr.exception;



public class SaveJsonInvoiceData extends RuntimeException{
	
	private String message;
	private String status;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}


}
