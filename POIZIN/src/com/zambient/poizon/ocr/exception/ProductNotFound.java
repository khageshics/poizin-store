package com.zambient.poizon.ocr.exception;

public class ProductNotFound extends RuntimeException{
	private String message;
	private int status;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ProductNotFound(String message) {
		super();
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	

}
