package com.zambient.poizon.ocr.exception;



public class DatabaseException extends RuntimeException{
	
	private String status;
	
	public DatabaseException(){
		super();
	}

	public DatabaseException(String status){
		this.status=status;
	}
}
