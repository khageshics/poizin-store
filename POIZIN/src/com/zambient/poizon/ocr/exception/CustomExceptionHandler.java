package com.zambient.poizon.ocr.exception;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;


/**
 *This class used to handle application Exceptions Using <code>@ControllerAdvice and @ExceptionHandler</code>
 *  classes
 * 
 */

@ControllerAdvice
public class CustomExceptionHandler{
	
	private static final Logger logger=Logger.getLogger(CustomExceptionHandler.class);
	
	
	
	/**
	 * This method is used to Handle the Exceptions of awsTextract Api 
	 * 
	 * @param AWSAccessDeniedException
	 * @param ResponseEntity
	 * @param ErrorResponse
	 * @return ResponseEntity<ErrorResponse>
	 */
	@ExceptionHandler(AWSAccessDeniedException.class)
	public ResponseEntity<ErrorResponse> awsTextractExceptionHandler(Exception ade){
		logger.info("ACCESS denied exception handled in controller advice"+ade);
		ErrorResponse errorResponse=new ErrorResponse();
		errorResponse.setMessage("Network connection lost? , Please check your network connection");
		errorResponse.setStatus("400");
		return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.NETWORK_AUTHENTICATION_REQUIRED);
	}
	
/*	@ExceptionHandler(Exception.class)
	public ModelAndView handleSQLException(Exception ex){
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("Error");
		modelAndView.addObject("message", ex.getMessage());
		//ex.printStackTrace();
		return modelAndView;
	}*/

	@ExceptionHandler(DatabaseException.class)
	public ResponseEntity<ErrorResponse> databaseException(DatabaseException exc){
		//System.out.println("db exception :"+exc);
		ErrorResponse errorResponse=new ErrorResponse();
		errorResponse.setMessage("Something went Wrong...");
		errorResponse.setStatus("400");
		return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.EXPECTATION_FAILED);
	}
	
	
	@ExceptionHandler(AWSTextractException.class)
	public ResponseEntity<ErrorResponse> amazonTextractException(AWSTextractException aTE){
		ErrorResponse errorResponse=new ErrorResponse();
		errorResponse.setMessage("The security token included in the request is invalid.");
		errorResponse.setStatus("400");
		
		return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.BAD_REQUEST);
		
		
	}
	
	
	@ExceptionHandler(OCRException.class)
	public ResponseEntity<ErrorResponse> ocrException(OCRException ocr){
		ErrorResponse errorResponse=new ErrorResponse();
		errorResponse.setMessage("OCR Internal Error...");
		errorResponse.setStatus("400");
		return new ResponseEntity<ErrorResponse>(errorResponse,HttpStatus.BAD_REQUEST);
	}
	
	
	
	@ExceptionHandler(ProductNotFound.class)
	public ResponseEntity productNotFound(ProductNotFound productNotFound){
		String statusMsg=productNotFound.getMessage().toString();
		return new ResponseEntity(statusMsg,HttpStatus.NO_CONTENT);
		
	}
	
	/*@ExceptionHandler(SQLException.class)
	public ResponseEntity sqlException(){
		System.out.println("sql exception");
		return new ResponseEntity("Something went wrong, please check the data...",HttpStatus.FORBIDDEN);
		
	}*/
	
	@ExceptionHandler(SaveJsonInvoiceData.class)
	public ResponseEntity<List> saveJsonInvoiceData(SaveJsonInvoiceData exc){
		List list=new ArrayList();
		list.add(417);
		return new ResponseEntity<List>(list,HttpStatus.EXPECTATION_FAILED);
	}
	
}
