package com.zambient.poizon.ocr.exception;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Component;


public class ErrorResponse implements Serializable{
	 private static final long serialVersionUID = 1L;
	
	private String message;
	private String status;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}


}
