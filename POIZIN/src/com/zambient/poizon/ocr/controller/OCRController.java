package com.zambient.poizon.ocr.controller;


import java.io.BufferedOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.amazonaws.SdkClientException;
import com.amazonaws.services.textract.model.AmazonTextractException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.zambient.poizon.bean.AddNewBrandBean;
import com.zambient.poizon.bean.MessageBean;
import com.zambient.poizon.bean.MrpRoundOffBean;
import com.zambient.poizon.ocr.dao.OCRDao;
import com.zambient.poizon.ocr.exception.AWSAccessDeniedException;
import com.zambient.poizon.ocr.exception.AWSTextractException;
import com.zambient.poizon.ocr.exception.DatabaseException;
import com.zambient.poizon.ocr.exception.OCRException;
import com.zambient.poizon.ocr.exception.ProductNotFound;
import com.zambient.poizon.ocr.exception.SaveJsonInvoiceData;
import com.zambient.poizon.ocr.model.Invoice;
import com.zambient.poizon.ocr.service.OCR;
import com.zambient.poizon.ocr.service.OCRImpl;
import com.zambient.poizon.ocr.util.InvoiceErrorMessages;
import com.zambient.poizon.services.PoizonService;




/**
 * <h3>OCRController</h3>
 * <p>This Controller class is will organize the rest calls related to the OCR APIs
 * 
 * @author Durga Rao Maruboina
 * @version 1.0
 * @since 2019
 */

@Controller
public class OCRController {
	
	@Autowired
	private OCR ocr;
	
	@Autowired
	private OCRDao ocrDao;
	@Value("${ocr.images}")
	private String ocrImagesPath;
	@Autowired
	PoizonService poizonService;
	
	@Autowired
	private OCRImpl ocrService;
	
	
	private static final Logger logger=Logger.getLogger(OCRController.class);
	/**
	 * This method is used to call the ocrTextract service to Analyze the text from images
	 * 
	 * @param file
	 * @param ocrImagesPath
	 * @return 
	 */
	
	@RequestMapping(value="/ocrInvoice")
	public String invoiceOCR(){
		return "ocrUpload";
	}
	
	
	@RequestMapping(value="/ocrInvoiceUpload" , method=RequestMethod.POST)
	public ResponseEntity<Map> invoiceUpload(@RequestParam("bulkUploadFile") MultipartFile file,ModelMap model) throws AWSAccessDeniedException{
		logger.info("------controller intered into invoiceUpload() API------- "+file);
		  Map invoiceMap=null;
		try{
			if(file!=null){
				
				
				BufferedOutputStream bos =null;
	            try {
	                byte[] fileBytes = file.getBytes();
	                // location to save the file
	                String fileName = file.getOriginalFilename();
	                try {
	                    Path path = Paths.get(ocrImagesPath+fileName);
	                    Files.write(path, fileBytes);
	                } catch (IOException e) {
	                    e.printStackTrace();
	                    logger.error(e);
	                }
	               
	              
	                invoiceMap=(Map)ocr.awsTextract(ocrImagesPath+fileName);
	                
	                if(invoiceMap.keySet().size() <=0){
	                	
	                	
	                return new ResponseEntity(InvoiceErrorMessages.ERROR_INVALID_QUALITY,HttpStatus.BAD_REQUEST);
	                
	                }
	                
	             
	               
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	            
	            	logger.error(e);
	                
	            }finally {
	                if(bos != null) {
	                    try {
	                        bos.close();
	                    } catch (IOException e) {
	                        // TODO Auto-generated catch block
	                        e.printStackTrace();
	                    }
	                }
	            }
				
				
			}else{
		        return new ResponseEntity(InvoiceErrorMessages.ERROR_FILE_NOT_FOUND,HttpStatus.BAD_REQUEST);
			}
			
			
		}
		catch(AmazonTextractException ate){
			logger.error(ate);
	        throw new AWSTextractException();
		}
		catch(SdkClientException ae){
			logger.error(ae);
			throw new AWSAccessDeniedException("AWSAccessDeniedException",ae.getCause());
		}catch(AWSAccessDeniedException exc){
			throw new AWSAccessDeniedException("AWSAccessDeniedException",exc.getCause());
		}
		catch(Exception exc){
			logger.error(exc);
			throw new OCRException();

		}
		
		
		return new ResponseEntity<Map>(invoiceMap,HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/ocrSuggestion",method=RequestMethod.GET)
	public ResponseEntity<List> ocrSuggestion(@RequestParam("brand_No") String brandNo,@RequestParam("FlagData") String flagData){
		List list = null;
		try{
		list=ocrDao.ocrSuggesion(brandNo, flagData);
	//	System.out.println("result :"+list.size());
		
		}catch(Exception exc){
			logger.error(exc);
			throw new DatabaseException();
		}
		return new ResponseEntity<List>(list,HttpStatus.OK);
	}
	
	@RequestMapping(value="/btlMrpAndSplMargin",method=RequestMethod.POST)
    public ResponseEntity<Map<Integer,List>> btlMrpAndSpecialMargin( @RequestBody List<Invoice> invoiceDataList){
   	 System.out.println(invoiceDataList);
   	// System.out.println("btlMrpAndSplMargin entered.........................................");
   	Map resultMap=null;
   	 try{
    resultMap= ocrDao.ocrMarginAndBtlMrp(invoiceDataList);
   //	System.out.println(resultMap);
   	}catch(Exception exc){
   		logger.error(exc);
			 throw new ProductNotFound(exc.getMessage());

  	 }
		return new ResponseEntity<Map<Integer,List>>(resultMap,HttpStatus.OK);
   	 
    }
	
	@RequestMapping(value="/suggBtlMrpAndSplMargin",method=RequestMethod.GET)
    public ResponseEntity<List> suggBtlMrpAndSpecialMargin( @RequestParam("brand_No") long brandNo,@RequestParam("quantity") int quantity){
   	 System.out.println(brandNo+" "+quantity);
   	// System.out.println("btlMrpAndSplMargin entered.........................................");
   	List resultList=null;
   	 try{
   		resultList= ocrDao.ocrSuggSingleMrpAndMargin(brandNo,quantity);
   //	System.out.println(resultMap);
   	}catch(Exception exc){
   		logger.error(exc);
			 throw new ProductNotFound(exc.getMessage());

  	 }
		return new ResponseEntity<List>(resultList,HttpStatus.OK);
   	 
    }
	
	@RequestMapping(value="/ocrSuggestionPackQty",method=RequestMethod.GET)
	public ResponseEntity<List> ocrSuggestionPackQty(@RequestParam("brand_No") String brandNo,@RequestParam("FlagData") String flagData){
		List resultList=null;
		try{
		resultList=ocrDao.ocrSuggesionPackQty(brandNo, flagData);
		//System.out.println("result :"+resultList.size());
		}catch(Exception exc){
			logger.error(exc);
			throw new DatabaseException();
		}
		return new ResponseEntity<List>(resultList,HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/ocrSuggestionPackSize",method=RequestMethod.GET)
	public ResponseEntity<List> ocrSuggestionPackSize(@RequestParam("brand_No") String brandNo,@RequestParam("FlagData") String flagData){
		List list=null;
		try{
		list=ocrDao.ocrSuggesionPackSize(brandNo, flagData);
		//System.out.println("result :"+list.size());
		}catch(Exception exc){
			logger.error(exc);
			throw new DatabaseException();
		}
		return new ResponseEntity<List>(list,HttpStatus.OK);
		
	}
	
	/** 
	 * This method is used to add new product to product tab including all details of that product.
	 * And this will return String message like Success or failure and redirecting to AddNewBrand jsp page.
	 * */
	@RequestMapping(value="/ocrAddNewBrandName",method=RequestMethod.POST,consumes="application/json")
	public ResponseEntity<String> ocrAddNewProduct(@RequestBody AddNewBrandBean addNewBrandBean){
		//System.out.println("ocrAddNewBrandName entered.....");
		String result=null;
		if(addNewBrandBean != null){
	    result= poizonService.addNewBrandName(addNewBrandBean);
		return new ResponseEntity<String>(result,HttpStatus.OK);
		}else{
			return new ResponseEntity<String>("failed",HttpStatus.EXPECTATION_FAILED);
		}
	}
	
	
	@RequestMapping(value = "/ocrSaveInvoiceJsonData/{flag}", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<List> saveInvoiceJsonData(@RequestBody String values,HttpServletRequest request, HttpServletResponse response,@PathVariable("flag") boolean flag){
		logger.info("Client Ip: "+request.getHeader("x-my-custom-header"));
		logger.info("PoizonController.saveInvoiceJsonData>> "+values);
		ModelAndView mav = new ModelAndView();
		response.setContentType("application/json");
		response.setHeader("Access-Control-Allow-Origin","*");
		response.setHeader("value", "valid");
		MessageBean messageBean = new MessageBean();
		ObjectMapper mapper = new ObjectMapper();
		List result=null;
		try {
			JSONObject jsonObject = new JSONObject(values);
			JSONArray invoiceJson = jsonObject.getJSONArray("invoiceDetails");
			System.out.println(invoiceJson);
			if(invoiceJson.length() !=0)
			result=ocrService.ocrSaveInvoiceJsonData(invoiceJson,flag);
			
			return new ResponseEntity<List>(result,HttpStatus.OK);
		} catch (Exception ex) {
			/*logger.info(ex.getMessage(), ex);
			messageBean.setMessage(result);
			mav.addObject("json",messageBean);
			return result;*/
			throw new SaveJsonInvoiceData();
		}
    }
	
	/**
	 * this method is used to save invoice mrp rounding up
	 * */
	@RequestMapping(value="/ocrSaveMrpRoundOff",method=RequestMethod.POST)
	public String saveMrpRoundOff(HttpServletRequest request,HttpServletResponse response,RedirectAttributes redirectAttributes){
		logger.info("PoizonSecondController.saveMrpRoundOff");
		MrpRoundOffBean mrpRoundOffBean = new MrpRoundOffBean();
		String date=request.getParameter("date");
		String dateAsPerCopy=request.getParameter("dateAsPerCopy");
		String mrproundoff=request.getParameter("mrproundoff");
		String turnoverTax=request.getParameter("turnoverTax");
		String tcsVal=request.getParameter("tcsVal");
		String retailerCreditVal=request.getParameter("retailerCreditVal");
		mrpRoundOffBean.setDate(date);
		mrpRoundOffBean.setDateAsPerCopy(dateAsPerCopy);
		mrpRoundOffBean.setMrpRoundOff(Double.parseDouble(mrproundoff));
		mrpRoundOffBean.setTurnOverTax(Double.parseDouble(turnoverTax));
		mrpRoundOffBean.setTcsVal(Double.parseDouble(tcsVal));
		mrpRoundOffBean.setRetailerCreditVal(Double.parseDouble(retailerCreditVal));
		String result=null;
		if(mrpRoundOffBean != null)
	    result= ocrService.saveMrpRoundOff(mrpRoundOffBean);
		redirectAttributes.addFlashAttribute("message", result);
		return "redirect:/sale";
	}
	
	
	@RequestMapping(value="/ocrSuggestionPackType",method=RequestMethod.GET)
	public ResponseEntity<List> ocrSuggestionPackType(@RequestParam("brand_No") String brandNo,@RequestParam("FlagData") String flagData){
		List result=null;
		try{
			result=ocrService.ocrSuggestions(brandNo, flagData);
			//System.out.println(result);
		}catch(Exception exc){
			logger.error(exc);
			throw new DatabaseException();
		}
		return new ResponseEntity<List>(result,HttpStatus.OK);
		
	}
}
