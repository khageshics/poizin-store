package com.zambient.poizon.ocr.model;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Scope("prototype")
public class Invoice {
	
	private long brandNumber;
	private String brandName;
	private String productType;
	private String packType;
	private int packQty;
	private int packSize;
	private int caseDeliveredQty;
	private int bottelsDeliveredQty;
	private String unitRate;
	private String btlRate;
	private double btlMrp;
	private double margin; 
	private String total;
	
	   private boolean Brand_No_Flag;
	   private boolean brand_Name_Flag;
	   private boolean Prod_Type_Flag;
	   private boolean Pack_Type_Flag;
	   private boolean Pack_Qty_Flag;
	   private boolean Pack_Size_Flag;
	   private boolean caseDelv_Flag;
	   private boolean btlDelv_Flag;
	   private boolean unitRate_Flag;
	   private boolean btlRate_Flag;
	   private boolean btlMrp_Flag;
	   private boolean margin_Flag;
	   public double getBtlMrp() {
		return btlMrp;
	}
	public void setBtlMrp(double btlMrp) {
		this.btlMrp = btlMrp;
	}
	public double getMargin() {
		return margin;
	}
	public void setMargin(double margin) {
		this.margin = margin;
	}
	public boolean isBtlMrp_Flag() {
		return btlMrp_Flag;
	}
	public void setBtlMrp_Flag(boolean btlMrp_Flag) {
		this.btlMrp_Flag = btlMrp_Flag;
	}
	public boolean isMargin_Flag() {
		return margin_Flag;
	}
	public void setMargin_Flag(boolean margin_Flag) {
		this.margin_Flag = margin_Flag;
	}
	private boolean total_Flag;
	
	public long getBrandNumber() {
		return brandNumber;
	}
	public void setBrandNumber(long brandNumber) {
		this.brandNumber = brandNumber;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getPackType() {
		return packType;
	}
	public void setPackType(String packType) {
		this.packType = packType;
	}

	public int getBottelsDeliveredQty() {
		return bottelsDeliveredQty;
	}
	public void setBottelsDeliveredQty(int bottelsDeliveredQty) {
		this.bottelsDeliveredQty = bottelsDeliveredQty;
	}
	public String getUnitRate() {
		return unitRate;
	}
	public void setUnitRate(String unitRate) {
		this.unitRate = unitRate;
	}
	public String getBtlRate() {
		return btlRate;
	}
	public void setBtlRate(String btlRate) {
		this.btlRate = btlRate;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public boolean isBrand_No_Flag() {
		return Brand_No_Flag;
	}
	public void setBrand_No_Flag(boolean brand_No_Flag) {
		Brand_No_Flag = brand_No_Flag;
	}
	public boolean isBrand_Name_Flag() {
		return brand_Name_Flag;
	}
	public void setBrand_Name_Flag(boolean brand_Name_Flag) {
		this.brand_Name_Flag = brand_Name_Flag;
	}
	public boolean isProd_Type_Flag() {
		return Prod_Type_Flag;
	}
	public void setProd_Type_Flag(boolean prod_Type_Flag) {
		Prod_Type_Flag = prod_Type_Flag;
	}
	public boolean isPack_Type_Flag() {
		return Pack_Type_Flag;
	}
	public void setPack_Type_Flag(boolean pack_Type_Flag) {
		Pack_Type_Flag = pack_Type_Flag;
	}
	public boolean isPack_Qty_Flag() {
		return Pack_Qty_Flag;
	}
	public void setPack_Qty_Flag(boolean pack_Qty_Flag) {
		Pack_Qty_Flag = pack_Qty_Flag;
	}
	public boolean isPack_Size_Flag() {
		return Pack_Size_Flag;
	}
	public void setPack_Size_Flag(boolean pack_Size_Flag) {
		Pack_Size_Flag = pack_Size_Flag;
	}
	public boolean isCaseDelv_Flag() {
		return caseDelv_Flag;
	}
	public void setCaseDelv_Flag(boolean caseDelv_Flag) {
		this.caseDelv_Flag = caseDelv_Flag;
	}
	public boolean isBtlDelv_Flag() {
		return btlDelv_Flag;
	}
	public void setBtlDelv_Flag(boolean btlDelv_Flag) {
		this.btlDelv_Flag = btlDelv_Flag;
	}
	public boolean isUnitRate_Flag() {
		return unitRate_Flag;
	}
	public void setUnitRate_Flag(boolean unitRate_Flag) {
		this.unitRate_Flag = unitRate_Flag;
	}
	public boolean isBtlRate_Flag() {
		return btlRate_Flag;
	}
	public void setBtlRate_Flag(boolean btlRate_Flag) {
		this.btlRate_Flag = btlRate_Flag;
	}
	public boolean isTotal_Flag() {
		return total_Flag;
	}
	public void setTotal_Flag(boolean total_Flag) {
		this.total_Flag = total_Flag;
	}
	public int getPackQty() {
		return packQty;
	}
	public void setPackQty(int packQty) {
		this.packQty = packQty;
	}
	public int getPackSize() {
		return packSize;
	}
	public void setPackSize(int packSize) {
		this.packSize = packSize;
	}
	public int getCaseDeliveredQty() {
		return caseDeliveredQty;
	}
	public void setCaseDeliveredQty(int caseDeliveredQty) {
		this.caseDeliveredQty = caseDeliveredQty;
	}

	
	

}
