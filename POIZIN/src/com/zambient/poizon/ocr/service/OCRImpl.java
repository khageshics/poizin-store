package com.zambient.poizon.ocr.service;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.jboss.logging.Logger;
import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.textract.AmazonTextract;
import com.amazonaws.services.textract.AmazonTextractClientBuilder;
import com.amazonaws.services.textract.model.AmazonTextractException;
import com.amazonaws.services.textract.model.Block;
import com.amazonaws.services.textract.model.DetectDocumentTextRequest;
import com.amazonaws.services.textract.model.DetectDocumentTextResult;
import com.amazonaws.services.textract.model.Document;
import com.zambient.poizon.bean.MrpRoundOffBean;
import com.zambient.poizon.ocr.dao.OCRDao;
import com.zambient.poizon.ocr.dao.OCRDaoImpl;
import com.zambient.poizon.ocr.exception.AWSAccessDeniedException;
import com.zambient.poizon.ocr.exception.AWSTextractException;
import com.zambient.poizon.ocr.exception.ProductNotFound;
import com.zambient.poizon.ocr.model.Invoice;
import com.zambient.poizon.ocr.model.InvoiceData;
import com.zambient.poizon.ocr.util.PdfToImage;


/**
 * <h3>OCRImpl</h3>
 * <p>OCRImpl class is used to implement ocr operation by using aws textract service</p>
 * 
 * @author Durga Rao Maruboina
 * @version 1.0
 * @since 2019
 */

@Configuration
@Repository
public class OCRImpl implements OCR{
	
	@Value("${aws.bucket}")
	private String awsS3Bucket;
	@Autowired
	private PdfToImage pdfToImage;
	
	@Value("${aws.service_endpoint}")
	private String awsServiceEndpoint;
	
	@Value("${aws.signing_region}")
	private String awsSigningRegion;
	
	@Value("${aws_access_key_id}")
	private String aws_access_key_id;
	
	@Value("${aws_secret_access_key}")
	private String aws_secret_access_key;
	
	@Autowired
	private InvoiceData invoiceData;
	
	@Autowired
	private OCRDaoImpl ocrDao;
	/*@Bean
	@Scope("prototype")
	public Invoice invoice(){
		return new Invoice();
	}*/
	
	@Autowired
	OCRDao userDao;
	
private static final Logger logger=Logger.getLogger(OCRImpl.class);
private DecimalFormat decimalFormat = new DecimalFormat("#.##");
	
/*	@Autowired
	private Invoice invoice;*/
	
	
	

/**
 *This method converts given input string <code>columnData</code> to double value
 *
 * @param columnData
 * @return tempString as output
 * 
 */
	public static String stringToDouble(String columnData){
		String tempString=null;
		try{
		 // comma removing from string for convertion to double
		  columnData=columnData.replaceAll(",", "");
		  columnData=columnData.replace("/", "");
		  
		  //identify & remove if String contains more than one decimal(.) point
		  List<Integer> dotPositionsList=new ArrayList<Integer>();
		  int dotCounter=0;
		  for(int i=columnData.length()-1; i>=0; i-- ){
			   switch(columnData.charAt(i)){
			   case '.':
				   dotCounter=dotCounter+1;
				   if(dotCounter>1){
					   dotPositionsList.add(i);
				   }
				   
				   break;
			   }
		  }
		  
		  //remove positions
		  StringBuilder stringBuilder=new StringBuilder(columnData);
		  tempString=columnData;
		  for(int i=0;i<dotPositionsList.size();i++){
			  stringBuilder.deleteCharAt(dotPositionsList.get(i));
			  tempString= stringBuilder.toString();
		  }
		}catch(Exception exc){
		logger.error("Error at OCRImpl class : "+exc);
		}
		// DecimalFormat decimalFormatter = new DecimalFormat("#.##");	
		//return Double.parseDouble(tempString);
		return tempString;
	}

	
	/**
	 * This method is used to convert inpud pdf to plan text by using AWSTextract service
	 * @param uploadPath
	 * @param awsServiceEndpoint : fetching it from application.properties file
	 * @param awsSigningRegion   : fetching it from application.properties file
	 * @return 
	 * @throws AWSAccessDeniedException 
	 */
	public Map awsTextract(String uploadPath) throws AWSAccessDeniedException {
		
		
		logger.info("--OCR controll inters into awsTextract API---");
		List<Invoice> invoiceObjList=null;
	
		 Map invoiceMap=new HashMap();
		try{
			

	        
			/*Images Generation of input pdf file */
	       List imagesList=pdfToImage.pdfToImageGenerator(uploadPath);
	        
	     
	         if(imagesList.get(imagesList.size()-1).equals("success")){
	        	 
	        	// if(true){
	        	 BufferedWriter bufferedWriter = null;
				try {
					bufferedWriter = new BufferedWriter(new FileWriter(new File(Paths.get(uploadPath).getParent().toString()+"/invoice.txt")));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.error(e);
				}
				
	        	 List<String> invoiceList=new ArrayList<String>();
	        	
	        
	        	 
	        	  // Looping each gaenerated image to aws service to analyze the data
	        	 
	        	  
	        	 for(int image=1;image <=(int)imagesList.get(0);image++){
	        	 
	        String document1=imagesList.get(image).toString();
	        ByteBuffer imageBytes = null;
	        
	        
	        
	        try (InputStream inputStream = new FileInputStream(new File(document1))) {
	         imageBytes = ByteBuffer.wrap(IOUtils.toByteArray(inputStream));
	        } catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
			logger.error(e);
			} catch (IOException e) {
				// TODO Auto-generated catch block
		    logger.error(e);
			}
	        
	        
	     
	        
	       // Connectivity with AWSTEXTRACT service
	        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(this.aws_access_key_id, this.aws_secret_access_key);
	        // AmazonTextractClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(awsCredentials)).build();
	        EndpointConfiguration endpoint = new EndpointConfiguration(awsServiceEndpoint, awsSigningRegion);
	        AmazonTextract client = AmazonTextractClientBuilder.standard().withEndpointConfiguration(endpoint).withCredentials(new AWSStaticCredentialsProvider(awsCredentials)).build();
	        
	        
	      
	     
	       // Text Detection operation with the AWS Textract service
	        DetectDocumentTextRequest request = new DetectDocumentTextRequest().withDocument(new Document().withBytes(imageBytes));
	        DetectDocumentTextResult result = client.detectDocumentText(request);
	        
	        

	        
	  
	        

	      //  Detected data saving into <code>invoiceList</code> and writing in to the file <code>invoice.txt</code> for forther operation
	       List<Block> blocks = result.getBlocks();
	        for (Block block : blocks) {
	          //  DisplayBlockInfo(block);
	            switch(block.getBlockType()) {
	            
	            case "LINE":
	            	
	            	String data=block.getText();
	            	invoiceList.add(data);
	            	//System.out.println(data);
	            	try {
						bufferedWriter.write(data+"\n");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	            	
	            	break;
	            
	     
	            }
	        }
	        bufferedWriter.flush();
	        
	        
	//   	Deleting the generated images from input pdf file
	   	 try{
	   		 
	   		 Path pathToBeDeleted = Paths.get(document1);

	   	        try {
	   	            Files.delete(pathToBeDeleted);
	   	        } catch (IOException e) {
	   	            logger.error(e);
	   	        }
	   	 }catch(Exception exc){
	   		 logger.error(exc);
	   	 }
	      
	         }
	        	 
	        
	        	 
	        
				/*	try {
					BufferedReader	bufferedReader= new BufferedReader(new FileReader(new File(Paths.get(uploadPath).getParent().toString()+"/invoice.txt")));
					String line=null;
					while((line=bufferedReader.readLine())!=null){
						
						invoiceList.add(line);
					}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						logger.error(e);
					}*/
					
					
	        	 /*	 try{
	    
	    	   		 Path pathToBeDeleted = Paths.get(uploadPath);
	    	   	        try {
	    	   	            Files.delete(pathToBeDeleted);
	    	   	        } catch (IOException e) {
	    	   	            logger.error(e);
	    	   	        }
	    	   	 }catch(Exception exc){
	    	   		 logger.error(exc);
	    	   	 }*/
	        	 
	        	
	        	 /*Each record will be stored in to the <code>invoiceObjList</code>*/
	        	invoiceObjList=new ArrayList<Invoice>();
	        	 
	        	/*Fetching <code>invoiceList</code> data and detecting the rows data */
	        	 for(int globalIndex=0;globalIndex<invoiceList.size();globalIndex++){
	        		 
	        		 try{
	        		
	        			 
	        			 if (invoiceList.get(globalIndex).contains("Invoice Date:")){

	        				 globalIndex=globalIndex+1;
	        				 invoiceData.setInvoiceDate(invoiceList.get(globalIndex));
	        				 logger.info("Entering into the row creator logic......");
	        		       }else if(invoiceList.get(globalIndex).contains("Btl Rate") || invoiceList.get(globalIndex).contains("Btt Rate") || invoiceList.get(globalIndex).contains("Bti Rate") || invoiceList.get(globalIndex).contains("BtI Rate") || invoiceList.get(globalIndex).contains("Bt1 Rate") || invoiceList.get(globalIndex).contains("BtL Rate")){
	        		    	   
	        		    	   
	        		    	   globalIndex=globalIndex+1;
	        		    	   int columnTemp=0;
	        		    	   
	        		    	   for(int table=globalIndex; table<invoiceList.size(); table++){
	        		    		   
	        		    		 
	        		    		   
	        		    		   if(!invoiceList.get(table).contains("TIN")){
	        		    			   
	        		    			   try{
	        		    		   
	        		    				   
	        		    			   Invoice invoice=new Invoice();  //Invoice object creation
	        		    				 
	        		    			   String brandNameAppender="";
	        		    		   
	        		    		   String rowData=invoiceList.get(table);
	        		    		   rowData=rowData.replace(",", "");
	        		    		   rowData=rowData.replace("/", "");
	        		    		   rowData=rowData.trim();
	        		    		   boolean rowGenerator=true;
	     		    		            
	        		    		  
	     		    		   
	     		    		              if(NumberUtils.isParsable(rowData)){ //This is UnitRate value
	     		    		            	  
	     		    		            	  
	        		    		   
	        		    		   for(int column=table; column<table+11; column++){
	        		    		   
	        		    			   try{
	        		    			   String columnData=invoiceList.get(column);
	        		    		    
	        		    		            if(!columnData.equals("TIN")){
	        		    		            	  
	        		    		            	 
	        		    		            	  if(columnData.contains(".")){  // UnitRate value
	        		    		            		  
	        		    		            		  // comma removing from string for convertion to double
	        		    		            		  //identify & remove if String contains more than one decimal(.) point and /
	        		    		            		 String unitRateValue= stringToDouble(columnData); //only one decimal value as result
	        		    		            		  
	        		    		            		  
	        		    		            		  invoice.setUnitRate(unitRateValue);
	        		    		            		  
	        		    		            	  }else if((columnData.length() <5 && columnData.contains("Beer") )|| columnData.length() <5 && columnData.contains("IML") || (columnData.length() <10 && columnData.contains("Duty") )){
	        		    		            		  
	        		    		            		  invoice.setProductType(columnData);
	        		    		            		  
	        		    		            	  }else if((columnData.contains("/") && columnData.contains("ml") ) || (columnData.contains("(") && columnData.contains("ml") ) || (columnData.contains("/") && columnData.contains("m") ) || columnData.contains(" ml")){
	        		    		            		  
	        		    		            		
	        		    		           
	        		    		            		  
	        		    		            		  String qty=null;
	        		    		            		  String packSize=null;
	        		    		            		  String[] packSplitter=columnData.split("\\s+");
	        		    		            		  for(String data:packSplitter){
	        		    		            		  if(data.contains("/")){
	        		    		            		  
		        		    		            		/*  StringBuffer num = new StringBuffer();
		        		    		            		   for (int i=0; i<packSplitter[1].length(); i++) { 
		        		    		            	            if (Character.isDigit(packSplitter[1].charAt(i))) 
		        		    		            	                num.append(packSplitter[1].charAt(i)); 
		        		    		            	        } */
		        		    		            			  
		        		    		            			  String[] splitter=data.split("/");
		        		    		            			  if(splitter[0]!=null || !(splitter[0].trim()).equals("")){
		        		    		            				  if(splitter[0].contains(" ")){
		        		    		            					  String[] val=splitter[0].split("\\s+");
		        		    		            				  qty =val[0].trim();
		        		    		            				  }else if(qty==null || qty.equals("")){
		        		    		            					  qty =splitter[0].trim();
		        		    		            				  }
		        		    		            			  }
		        		    		            		   packSize=splitter[1].replaceAll("([a-z])", "").trim();
		        		    		            		   break;
	        		    		            		  }else{
	        		    		            			  qty =packSplitter[0].trim();
	        		    		            		   
	        		    		            		  }
	        		    		            		  
	        		    		            		  }
	        		    		            		  
	        		    		            		  try{
	        		    		            		  invoice.setPackQty(Integer.parseInt(qty));
	        		    		            		  }catch(Exception exc){
	        		    		            			  
	        		    		            		  }
	        		    		            		  
	        		    		            		  try{
	        		    		            		  invoice.setPackSize(Integer.parseInt(packSize));
	        		    		            		  }catch(Exception exc){
	        		    		            			  
	        		    		            		  }
	        		    		            		  
	        		    		            		  //gather cases delivered and bottles delivered details
	        		    		            		  column=column+1;
	        		    		            		  
	        		    		            		  List cDelList=new ArrayList<String>();
	        		    		            		  List<String> tList=new ArrayList<String>();
	        		    		            		  int maxData=column;
	        		    		            		  for(int i=column;i<maxData+4;i++){
	        		    		            			  if(!invoiceList.get(i).equals("TIN")){
	        		    		            			 // System.out.println("value :"+i);
	        		    		            			  if(!invoiceList.get(i).contains(".") && cDelList.size()<2){
	        		    		            				  cDelList.add(stringToDouble(invoiceList.get(i)));
		        		    		            		  }else if(tList.size()<2){
		        		    		            			  
		        		    		            			  tList.add(stringToDouble(invoiceList.get(i)));
		        		    		            		  }else{
		        		    		            			  break;
		        		    		            		  }
	        		    		            			  
	        		    		            			  
	        		    		            			  column=i;
	        		    		            		  }else{
	        		    		            			  break;
	        		    		            		  }
	        		    		            		  }
	        		    		            		  
	        		    		            		  if(cDelList.size()!=0){
	        		    		            			  
	        		    		            			  invoice.setCaseDeliveredQty(Integer.parseInt((String) cDelList.get(0)));
	        		    		            			  invoice.setBottelsDeliveredQty(Integer.parseInt((String) cDelList.get(1)));
	        		    		            		  }
	        		    		            		  
	        		    		            		  int max=0;
	        		    		            		  int min=0;
	        		    		            		  int temp=0;
	        		    		            		  if(tList.size()!=0){
	        		    		            			
	        		    		            			   double largestString = Double.parseDouble(tList.get(0));
	        		    		            		        int index = 0;

	        		    		            		        for(int i = 0; i < tList.size(); i++)
	        		    		            		        {
	        		    		            		            if(Double.parseDouble(tList.get(i)) > largestString)
	        		    		            		            {
	        		    		            		               max=i;
	        		    		            		            }else{
	        		    		            		            	min=i;
	        		    		            		            }
	        		    		            		        }
	        		    		            		        for(int j=0;j<tList.size();j++){
	        		    		            		        	if(j==max){
	        		    		            		        		  invoice.setTotal( stringToDouble(tList.get(j)));
	        		    		            		        	}else if(j==min){
	        		    		            		        		invoice.setBtlRate( stringToDouble(tList.get(j)));
	        		    		            		        	}
	        		    		            		        }
	        		    		            		        
	        		    		            		  }
	        		    		            		  
	        		    		            		  
	        		    		            		//  System.out.println(cDelList.size()+" "+tList.size());

	        		    		            		/*  if(invoiceList.get(column).contains(".") && invoiceList.get(column).contains("/")){
		        		    		            			  String unitRateValue= stringToDouble(columnData); //only one decimal value as result
		            		    		            		  invoice.setUnitRate(unitRateValue);
		            		    		            		  column=column+1;
		        		    		            		  }*/
	        		    		            		  
	        		    		            		/*  if(invoiceList.get(column).equalsIgnoreCase("o")){
		        		    		            			  int value=0;
		        		    		            		  invoice.setCaseDeliveredQty(value);
		        		    		            		  }else{
		        		    		            			  try{
		        		    		            			  invoice.setCaseDeliveredQty(Integer.parseInt(invoiceList.get(column).trim()));
		        		    		            			  }catch(Exception exc){
		        		    		            				  
		        		    		            			  }
		        		    		            			  }
		        		    		            		  column=column+1;
		        		    		            		  */
		        		    		            	/*	  if(invoiceList.get(column).contains(".")){
		        		    		            			  String total= stringToDouble(columnData); //only one decimal value as result
		            		    		            		  invoice.setTotal(total);
		            		    		            		  column=column+1;
		            		    		            		  invoice.setBillRate( stringToDouble(invoiceList.get(column)));
		        		    		            		  }
		        		    		            		  
		        		    		            		  else if(invoiceList.get(column).equalsIgnoreCase("o")){
	        		    		            			  int value=0;
	        		    		            		  invoice.setBottelsDeliveredQty(value);
	        		    		            		  column=column+1;
	        		    		            		  invoice.setTotal( stringToDouble(invoiceList.get(column)));
	        		    		            		  column=column+1;
	        		    		            		  invoice.setBillRate( stringToDouble(invoiceList.get(column)));
	        		    		            		  }else{
	        		    		            			  try{
	        		    		            			  invoice.setBottelsDeliveredQty(Integer.parseInt(invoiceList.get(column).trim()));
	        		    		            			  column=column+1;
		        		    		            		  invoice.setTotal( stringToDouble(invoiceList.get(column)));
		        		    		            		  column=column+1;
		        		    		            		  invoice.setBillRate( stringToDouble(invoiceList.get(column)));
	        		    		            			  }catch(Exception exc){
	        		    		            				  
	        		    		            			  }
	        		    		            			  }*/
	        		    		            		  
	        		    		            		 // column=column+1;
	        		    		            		 // invoice.setTotal( stringToDouble(invoiceList.get(column)));
	        		    		            		 // column=column+1;
	        		    		            		 // invoice.setBillRate( stringToDouble(invoiceList.get(column)));
	        		    		            		  
	        		    		            	  }else{
	        		    		            		  
	        		    		            		  if(NumberUtils.isParsable(columnData)){ 
	        		    		            			//  System.out.println("SI No. :"+columnData);
	        		    		            			  
	        		    		            			   if(columnData.length()>=4){ //Brand Number
	                		    		            		  long brandNumber=Long.parseLong(columnData);
	                		    		            	
	                		    		            		  invoice.setBrandNumber(brandNumber);
	                		    		            
	                		    		            	  }
	        		    		            			  
	        		    		            		  }else {
	        		    		            			  
	        		    		            			  if(columnData.length()==1){     //Pack Type
	        		    		            				  invoice.setPackType(columnData);
	        		    		            				  
	        		    		            		         }else{   //Brand Name
	        		    		            		        	 brandNameAppender=brandNameAppender+""+columnData;
	        		    		            		        	 
	        		    		            		        	 
	        		    		            		        	 invoice.setBrandName(brandNameAppender);
	        		    		            		         }
	        		    		            			  
	        		    		            		  }
	        		    		            		  
	        		    		            	  }
	        		    		            	  
	        		    			   }else{
	        		    				   break;
	        		    			   }
	 
	        		    		   }catch(Exception exc){
	        		    		   }
	        		    			   columnTemp=column;
	        		    	         } //(.)Loop closing**********************************************
	        		    	  
	     		    		              }else{
	     		    		            	 
	     		    		            	  if(!rowData.contains(":") ){
	     		    		            		  if(!rowData.contains(" of 2")){
	     		    		            		
	     		    		            	 brandNameAppender=brandNameAppender+""+rowData;
	     		    		            	  
	     		    		            	  
	     		    		            	  // Two Brand Name lines , if it reads first then Util rate value
	     		    		            	
	  		        		    		    
	     		    		            	 table=table+1;
	    		    		            	  if(invoiceList.get(table).contains(".")){ 
	     		    		            	  
	     		    		            	  
	     		        		    		   for(int column=table; column<table+11; column++){
	     		        		    			   try{
	     		        		    		   
	     		        		    			   String columnData=invoiceList.get(column);
	     		        		    			   
	     		        		    			   if(!columnData.equals("TIN")){
	     		        		    		    
	     		        		    		            	  if(columnData.contains(".")){  // UnitRate value
	     		        		    		            		  
	     		        		    		            		  // comma removing from string for convertion to double
	     		        		    		            		  //identify & remove if String contains more than one decimal(.) point and /
	     		        		    		            		 String unitRateValue= stringToDouble(columnData); //only one decimal value as result
	     		        		    		            		  
	     		        		    		            		  
	     		        		    		            		  invoice.setUnitRate(unitRateValue);
	     		        		    		            		  
	     		        		    		            	  }else if((columnData.length() <5 && columnData.contains("Beer") )|| columnData.length() <5 && columnData.contains("IML") || (columnData.length() <10 && columnData.contains("Duty") )){
	     		        		    		            		  
	     		        		    		            		  invoice.setProductType(columnData);
	     		        		    		            		  
	     		        		    		            	  }else if((columnData.contains("/") && columnData.contains("ml") ) || (columnData.contains("(") && columnData.contains("ml") ) || (columnData.contains("/") && columnData.contains("m") )){
	     		        		    		            		  
	     		        		    		            		  
	     		        		    		            		  String qty=null;
	     		        		    		            		  String packSize=null;
	     		        		    		            		  String[] packSplitter=columnData.split("\\s+");
	     		        		    		            		  for(String data:packSplitter){
	     		        		    		            		  if(data.contains("/")){
	     		        		    		            		  
	     			        		    		              
	     			        		    		            			  String[] splitter=data.split("/");
	     			        		    		            			  if(splitter[0]!=null || !(splitter[0].trim()).equals("")){
	     			        		    		            				  qty =splitter[0].trim();
	     			        		    		            			  }
	     			        		    		            			   packSize=splitter[1].replaceAll("([a-z])", "").trim();
	     			        		    		            		   break;
	     		        		    		            		  }else{
	     		        		    		            			  qty =packSplitter[0].trim();
	     		        		    		            		   
	     		        		    		            		  }
	     		        		    		            		  
	     		        		    		            		  }
	     		        		    		            		  try{
	     		        		    		            		 invoice.setPackQty(Integer.parseInt(qty));
	     		        		    		            		  }catch(Exception exc){
	     		        		    		            			  
	     		        		    		            		  }
	     		        		    		            		  try{
	     		        		    		            		 invoice.setPackSize(Integer.parseInt(packSize));
	     		        		    		            		  }catch(Exception exc){
	     		        		    		            			  
	     		        		    		            		  }
	     		        		    		            		  
	     		        		    		            		  //gather cases delivered and bottles delivered details
	     		        		    		            		  column=column+1;
	     		        		    		            		  
	     		        		    		            		  List cDelList=new ArrayList<String>();
	     		        		    		            		  List<String> tList=new ArrayList<String>();
	     		        		    		            		  int maxData=column;
	     		        		    		            		  for(int i=column;i<maxData+4;i++){
	     		        		    		            			  try{
	     		        		    		            			  if(!invoiceList.get(i).equals("TIN")){
	     		        		    		            			 // System.out.println("value :"+i);
	     		        		    		            			  if(!invoiceList.get(i).contains(".") && cDelList.size()<2 && !invoiceList.get(i).contains(" ")){
	     		        		    		            				  cDelList.add(stringToDouble(invoiceList.get(i)));
	     			        		    		            		  }else if(tList.size()<3 && stringToDouble(invoiceList.get(i)).matches("-?\\d+(\\.\\d+)?")){
	     			        		    		            			  
	     			        		    		            			  tList.add(stringToDouble(invoiceList.get(i)));
	     			        		    		            		  }else if(!invoiceList.get(i).matches("-?\\d+(\\.\\d+)?")){
	     			        		    		            			 brandNameAppender=brandNameAppender+" "+invoiceList.get(i);
     		        		    		            		        	 
     		        		    		            		      
     		        		    		            		        	 invoice.setBrandName(brandNameAppender);
     		        		    		            		        	i=i+1;
     		        		    		            		        	 tList.add(stringToDouble(invoiceList.get(i)));
	     			        		    		            		  }else{
	     			        		    		            			  break;
	     			        		    		            		  }
	     		        		    		            			  
	     		        		    		            			  
	     		        		    		            			  column=i;
	     		        		    		            		  }else{
	     		        		    		            			  break;
	     		        		    		            		  }
	     		        		    		            			  
	     		        		    		            			  }catch(Exception exc){
	     		        		    		            				  exc.printStackTrace();
	     		        		    		            			  }
	     		        		    		            		  }
	     		        		    		            		  
	     		        		    		            		  if(cDelList.size()!=0){
	     		        		    		            			  try{
	     		        		    		            				  
	     		        		    		            				 if(invoiceList.get(column).trim().equalsIgnoreCase("o")){
	           		        		    		            			  int value=0;
	           		        		    		            		  invoice.setCaseDeliveredQty(value);
	           		        		    		            		  }else{
	           		        		    		            			 invoice.setCaseDeliveredQty(Integer.parseInt((String) cDelList.get(0)));
	           		        		    		            		  }
	     		        		    		            				  
	     		        		    		            			 
	     		        		    		            			  }catch(Exception exc){
	     		        		    		            				  
	     		        		    		            			  }
	     		        		    		            			  try{
	     		        		    		            			  invoice.setBottelsDeliveredQty(Integer.parseInt((String) cDelList.get(1)));
	     		        		    		            			  }catch(Exception exc){
	     		        		    		            				  
	     		        		    		            			  }
	     		        		    		            		  }
	     		        		    		            		  
	     		        		    		            		  int max=0;
	     		        		    		            		  int min=0;
	     		        		    		            		  int temp=0;
	     		        		    		            		  if(tList.size()!=0){
	     		        		    		            			
	     		        		    		            			   double largestString = Double.parseDouble(tList.get(0));
	     		        		    		            		        int index = 0;

	     		        		    		            		        for(int i = 0; i < tList.size(); i++)
	     		        		    		            		        {
	     		        		    		            		        	try{
	     		        		    		            		            if(Double.parseDouble(tList.get(i)) > largestString)
	     		        		    		            		            {
	     		        		    		            		               max=i;
	     		        		    		            		            }else{
	     		        		    		            		            	min=i;
	     		        		    		            		            }
	     		        		    		            		        	}catch(Exception exc){
	     		        		    		            		        		
	     		        		    		            		        	}
	     		        		    		            		        }
	     		        		    		            		        for(int j=0;j<tList.size();j++){
	     		        		    		            		        	if(j==max){
	     		        		    		            		        		  invoice.setTotal( stringToDouble(tList.get(j)));
	     		        		    		            		        	}else if(j==min){
	     		        		    		            		        		invoice.setBtlRate( stringToDouble(tList.get(j)));
	     		        		    		            		        	}
	     		        		    		            		        }
	     		        		    		            		        
	     		        		    		            		  }
	     		        		    		            		  
	     		        		    		            		  
	     		        		    		            		 // System.out.println(cDelList.size()+" "+tList.size());
	     		        		    		            		  
	     		        		    		            		 /* invoice.setPackQty(columnData);
	     		        		    		            		  
	     		        		    		            		  //gather cases delivered and bottles delivered details
	     		        		    		            		  column=column+1;
	     		        		    		            		  
	     		        		    		            		  if(invoiceList.get(column).contains(".") && invoiceList.get(column).contains("/")){
	     		        		    		            			  String unitRateValue= stringToDouble(columnData); //only one decimal value as result
	     		            		    		            		  invoice.setUnitRate(unitRateValue);
	     		            		    		            		  column=column+1;
	     		        		    		            		  }
	     		        		    		            		  
	     		        		    		            		 if(invoiceList.get(column).equalsIgnoreCase("o")){
        		        		    		            			  int value=0;
        		        		    		            		  invoice.setCaseDeliveredQty(value);
        		        		    		            		  }else{
        		        		    		            			  invoice.setCaseDeliveredQty(Integer.parseInt(invoiceList.get(column).trim()));
        		        		    		            		  }
        		        		    		            		  column=column+1;
        		        		    		            		  
        		        		    		            		 if(invoiceList.get(column).equalsIgnoreCase("o")){
       		        		    		            			  int value=0;
       		        		    		            		  invoice.setBottelsDeliveredQty(value);
       		        		    		            		  }else{
       		        		    		            			  invoice.setBottelsDeliveredQty(Integer.parseInt(invoiceList.get(column).trim()));
       		        		    		            		  }
        		        		    		            		  
        		        		    		            		  
        		        		    		            		  column=column+1;
        		        		    		            		  invoice.setTotal( stringToDouble(invoiceList.get(column)));
        		        		    		            		  
        		        		    		            		  column=column+1;
        		        		    		            		  brandNameAppender=brandNameAppender+" "+invoiceList.get(column);
   	        		    		            		          invoice.setBrandName(brandNameAppender);
        		        		    		            		  column=column+1;
        		        		    		            		  invoice.setBillRate( stringToDouble(invoiceList.get(column)));*/
		        		    		            				  
	     		        		    		            		  
	     		        		    		            	  }else{
	     		        		    		            		  
	     		        		    		            		  if(NumberUtils.isParsable(columnData)){ 
	     		        		    		            			//  System.out.println("SI No. :"+columnData);
	     		        		    		            			  
	     		        		    		            			   if(columnData.length()>=4){ //Brand Number
	     		                		    		            		  long brandNumber=Long.parseLong(columnData);
	     		                		    		            	
	     		                		    		            		  invoice.setBrandNumber(brandNumber);
	     		                		    		            
	     		                		    		            	  }
	     		        		    		            			  
	     		        		    		            		  }else {
	     		        		    		            			  
	     		        		    		            			  if(columnData.length()==1){     //Pack Type
	     		        		    		            				  invoice.setPackType(columnData);
	     		        		    		            				  
	     		        		    		            		         }else{   //Brand Name
	     		        		    		            		        	 brandNameAppender=brandNameAppender+" "+columnData;
	     		        		    		            		        	 
	     		        		    		            		        	 
	     		        		    		            		        	 invoice.setBrandName(brandNameAppender);
	     		        		    		            		         }
	     		        		    		            			  
	     		        		    		            		  }
	     		        		    		            		  
	     		        		    		            	  }
	     		        		    		            	  
	     		        		    		            	  
	     		        		    			   }else{
	     		        		    				   break;
	     		        		    			   }
	     		        		    		             
	     		        		    		              
	     		        		    		   }catch(Exception exc){
	     		        		    		   }
	     		        		    			  columnTemp=column;
	     		        		    	         }
	    		    		            	  }else if(((invoiceList.get(table).contains("/") && invoiceList.get(table).contains("ml")) || (invoiceList.get(table).contains("/") && invoiceList.get(table).contains("m")) || (invoiceList.get(table).contains("(") && invoiceList.get(table).contains("ml")) ) || (NumberUtils.isParsable(invoiceList.get(table)) && invoiceList.get(table).length()>=4) ||(invoiceList.get(table).equals("Beer"))){
	    		    		            		  
	    		    		            		//If first data is BrandName then Brand Number  
	    		    		            		  
	    		    		            		   for(int column=table; column<table+6; column++){
	    		    		            			   try{
	    	     		        		    		   
	         		        		    			   String columnData=invoiceList.get(column);
	         		        		    			   
	         		        		    			   if(!columnData.equals("TIN")){
	         		        		    		    
	         		        		    		            	  if(columnData.contains(".")){  // UnitRate value
	         		        		    		            		  
	         		        		    		            		  // comma removing from string for convertion to double
	         		        		    		            		  //identify & remove if String contains more than one decimal(.) point and /
	         		        		    		            		 String unitRateValue= stringToDouble(columnData); //only one decimal value as result
	         		        		    		            		  
	         		        		    		            		  
	         		        		    		            		  invoice.setUnitRate(unitRateValue);
	         		        		    		            		  
	         		        		    		            	  }else if((columnData.length() <5 && columnData.contains("Beer") )|| columnData.length() <5 && columnData.contains("IML") || (columnData.length() <10 && columnData.contains("Duty") )){
	         		        		    		            		  
	         		        		    		            		  invoice.setProductType(columnData);
	         		        		    		            		  
	         		        		    		            	  }else if((columnData.contains("/") && columnData.contains("ml") ) || (columnData.contains("(") && columnData.contains("ml") ) || (columnData.contains("/") && columnData.contains("m") ) ){
	         		        		    		            		  
	         		        		    		            		 String qty=null;
		     		        		    		            		  String packSize=null;
		     		        		    		            		  String[] packSplitter=columnData.split("\\s+");
		     		        		    		            		  for(String data:packSplitter){
		     		        		    		            		  if(data.contains("/")){
		     		        		    		            		  
		     			        		    		              
		     			        		    		            			  String[] splitter=data.split("/");
		     			        		    		            			  if(splitter[0]!=null || !splitter[0].equals("")){
		     			        		    		            				  qty =splitter[0].trim();
		     			        		    		            			  }
		     			        		    		            			   packSize=splitter[1].replaceAll("([a-z])", "").trim();
		     			        		    		            		   break;
		     		        		    		            		  }else{
		     		        		    		            			  qty =packSplitter[0].trim();
		     		        		    		            		   
		     		        		    		            		  }
		     		        		    		            		  
		     		        		    		            		  }
		     		        		    		            		  
		     		        		    		            		  try{
		     		        		    		            		 invoice.setPackQty(Integer.parseInt(qty));
		     		        		    		            		  }catch(Exception exc){
		     		        		    		            			  
		     		        		    		            		  }
		     		        		    		            		  
		     		        		    		            		  try{
		     		        		    		            		 invoice.setPackSize(Integer.parseInt(packSize));
		     		        		    		            		  }catch(Exception exc){
		     		        		    		            			  
		     		        		    		            		  }
		     		        		    		            		  
	         		        		    		            		  
	         		        		    		            		  
	         		        		    		            		 
	         		        		    		            		  
	         		        		    		            	  }else{
	         		        		    		            		  
	         		        		    		            		  if(NumberUtils.isParsable(columnData)){ 
	         		        		    		            			//  System.out.println("SI No. :"+columnData);
	         		        		    		            			  
	         		        		    		            			   if(columnData.length()>=4){ //Brand Number
	         		                		    		            		  long brandNumber=Long.parseLong(columnData);
	         		                		    		            	
	         		                		    		            		  invoice.setBrandNumber(brandNumber);
	         		                		    		            
	         		                		    		            	  }
	         		        		    		            			  
	         		        		    		            		  }else {
	         		        		    		            			  
	         		        		    		            			  if(columnData.length()==1){     //Pack Type
	         		        		    		            				  invoice.setPackType(columnData);
	         		        		    		            				  
	         		        		    		            				 //gather cases delivered and bottles delivered details
	                 		        		    		            		  column=column+1;
	                 		        		    		            		  
	                 		        		    		            		
	                 		        		    		            		 if(invoiceList.get(column).contains(".") && invoiceList.get(column).contains("/")){
	                		        		    		            			  String unitRateValue= stringToDouble(columnData); //only one decimal value as result
	                		            		    		            		  invoice.setUnitRate(unitRateValue);
	                		            		    		            		  column=column+1;
	                		        		    		            		  }
	                 		        		    		            		  
	                 		        		    		            		  if(invoiceList.get(column).equalsIgnoreCase("o")){
	                 		        		    		            			  int value=0;
	                 		        		    		            		  invoice.setCaseDeliveredQty(value);
	                 		        		    		            		  }else{
	                 		        		    		            			  invoice.setCaseDeliveredQty(Integer.parseInt(invoiceList.get(column).trim()));
	                 		        		    		            		  }
	                 		        		    		            		  column=column+1;
	                 		        		    		            		  
	                 		        		    		            		 if(invoiceList.get(column).equalsIgnoreCase("o")){
	                		        		    		            			  int value=0;
	                		        		    		            		  invoice.setBottelsDeliveredQty(value);
	                		        		    		            		  }else{
	                		        		    		            			  invoice.setBottelsDeliveredQty(Integer.parseInt(invoiceList.get(column).trim()));
	                		        		    		            		  }
	                 		        		    		            		  
	                 		        		    		            		  
	                 		        		    		            		  column=column+1;
	                 		        		    		            		  invoice.setTotal( stringToDouble(invoiceList.get(column)));
	                 		        		    		            		  
	                 		        		    		            		  column=column+1;
	                 		        		    		            		  brandNameAppender=brandNameAppender+" "+invoiceList.get(column);
	            	        		    		            		          invoice.setBrandName(brandNameAppender);
	                 		        		    		            		  column=column+1;
	                 		        		    		            		  invoice.setBtlRate( stringToDouble(invoiceList.get(column)));
	         		        		    		            				  
	         		        		    		            		         }else{   //Brand Name
	         		        		    		            		        	 brandNameAppender=brandNameAppender+" "+columnData;
	         		        		    		            		        	 
	         		        		    		            		        	 
	         		        		    		            		        	 invoice.setBrandName(brandNameAppender);
	         		        		    		            		         }
	         		        		    		            			  
	         		        		    		            		  }
	         		        		    		            		  
	         		        		    		            	  }
	         		        		    		            	  
	         		        		    		            	  
	         		        		    		              
	    		    		            			   }else{
	    		    		            				   break;
	    		    		            			   }
	         		        		    		              
	    		    		            		   }catch(Exception exc){
	    		    		            		   }
	    		    		            			   columnTemp=column;
	         		        		    		              
	         		        		    	         }
	    		    		            		  
	    		    		            		  
	    		    		            	  }else if(NumberUtils.isParsable(invoiceList.get(table)) && invoiceList.get(table).length()<4){
	    		    		            		  
	    		    		            		  for(int column=table; column<table+6; column++){
	    		   	        		    		   
	    			        		    			   try{
	    			        		    			   String columnData=invoiceList.get(column);
	    			        		    		    
	    			        		    		            if(!columnData.equals("TIN")){
	    			        		    		            	  
	    			        		    		            	 
	    			        		    		            	  if(columnData.contains(".") && !columnData.contains(" ")){  // UnitRate value
	    			        		    		            		  
	    			        		    		            		  // comma removing from string for convertion to double
	    			        		    		            		  //identify & remove if String contains more than one decimal(.) point and /
	    			        		    		            		 String unitRateValue= stringToDouble(columnData); //only one decimal value as result
	    			        		    		            		  
	    			        		    		            		  
	    			        		    		            		  invoice.setUnitRate(unitRateValue);
	    			        		    		            		  
	    			        		    		            	  }else if((columnData.length() <5 && columnData.contains("Beer") )|| columnData.length() <5 && columnData.contains("IML") || (columnData.length() <10 && columnData.contains("Duty") )){
	    			        		    		            		  
	    			        		    		            		  invoice.setProductType(columnData);
	    			        		    		            		  
	    			        		    		            	  }else if((columnData.contains("/") && columnData.contains("ml") ) || (columnData.contains("(") && columnData.contains("ml") ) || (columnData.contains("/") && columnData.contains("m") )){
	    			        		    		            		  
	    			        		    		            		
	    			        		    		           
	    			        		    		            		  
	    			        		    		            		  String qty=null;
	    			        		    		            		  String packSize=null;
	    			        		    		            		  String[] packSplitter=columnData.split("\\s+");
	    			        		    		            		  for(String data:packSplitter){
	    			        		    		            		  if(data.contains("/")){
	    			        		    		            		  
	    				        		    		            		/*  StringBuffer num = new StringBuffer();
	    				        		    		            		   for (int i=0; i<packSplitter[1].length(); i++) { 
	    				        		    		            	            if (Character.isDigit(packSplitter[1].charAt(i))) 
	    				        		    		            	                num.append(packSplitter[1].charAt(i)); 
	    				        		    		            	        } */
	    				        		    		            			  
	    				        		    		            			  String[] splitter=data.split("/");
	    				        		    		            			  if(splitter[0]!=null || !splitter[0].equals("")){
	    				        		    		            				  qty =splitter[0].trim();
	    				        		    		            			  }
	    				        		    		            			   packSize=splitter[1].replaceAll("([a-z])", "").trim();
	    				        		    		            		   break;
	    			        		    		            		  }else{
	    			        		    		            			  qty =packSplitter[0].trim();
	    			        		    		            		   
	    			        		    		            		  }
	    			        		    		            		  
	    			        		    		            		  }
	    			        		    		            		  
	    			        		    		            		  try{
	    			        		    		            		  invoice.setPackQty(Integer.parseInt(qty));
	    			        		    		            		  }catch(Exception exc){
	    			        		    		            			  
	    			        		    		            		  }
	    			        		    		            		  
	    			        		    		            		  try{
	    			        		    		            		  invoice.setPackSize(Integer.parseInt(packSize));
	    			        		    		            		  }catch(Exception exc){
	    			        		    		            			  
	    			        		    		            		  }
	    			        		    		            		  
	    			        		    		            		  
	    			        		    		            		  //gather cases delivered and bottles delivered details
	    			        		    		            		  column=column+1;
	    			        		    		            		  
	    			        		    		            		  List cDelList=new ArrayList<String>();
	    			        		    		            		  List<String> tList=new ArrayList<String>();
	    			        		    		            		  int maxData=column;
	    			        		    		            		  for(int i=column;i<maxData+5;i++){
	    			        		    		            			  if(!invoiceList.get(i).equals("TIN")){
	    			        		    		            			//  System.out.println("value :"+i);
	    			        		    		            			  if(!invoiceList.get(i).contains(".") && cDelList.size()<2){
	    			        		    		            				  cDelList.add(stringToDouble(invoiceList.get(i)));
	    				        		    		            		  }else if(tList.size()<3){
	    				        		    		            			  
	    				        		    		            			  tList.add(stringToDouble(invoiceList.get(i)));
	    				        		    		            		  }else{
	    				        		    		            			  break;
	    				        		    		            		  }
	    			        		    		            			  
	    			        		    		            			  
	    			        		    		            			  column=i;
	    			        		    		            		  }else{
	    			        		    		            			  break;
	    			        		    		            		  }
	    			        		    		            		  }
	    			        		    		            		  
	    			        		    		            		  if(cDelList.size()!=0){
	    			        		    		            			  
	    			        		    		            			  invoice.setCaseDeliveredQty(Integer.parseInt((String) cDelList.get(0)));
	    			        		    		            			  invoice.setBottelsDeliveredQty(Integer.parseInt((String) cDelList.get(1)));
	    			        		    		            		  }
	    			        		    		            		  
	    			        		    		            		  int max=0;
	    			        		    		            		  int min=0;
	    			        		    		            		  int temp=0;
	    			        		    		            		  if(tList.size()!=0){
	    			        		    		            			
	    			        		    		            			   double largestString = Double.parseDouble(tList.get(0));
	    			        		    		            		        int index = 0;

	    			        		    		            		        for(int i = 0; i < tList.size(); i++)
	    			        		    		            		        {
	    			        		    		            		            if(Double.parseDouble(tList.get(i)) > largestString)
	    			        		    		            		            {
	    			        		    		            		               max=i;
	    			        		    		            		            }else{
	    			        		    		            		            	min=i;
	    			        		    		            		            }
	    			        		    		            		        }
	    			        		    		            		        for(int j=0;j<tList.size();j++){
	    			        		    		            		        	if(j==max){
	    			        		    		            		        		  invoice.setTotal( stringToDouble(tList.get(j)));
	    			        		    		            		        	}else if(j==min){
	    			        		    		            		        		invoice.setBtlRate( stringToDouble(tList.get(j)));
	    			        		    		            		        	}else{
	    			        		    		            		        		 invoice.setUnitRate(tList.get(j)); 
	    			        		    		            		        	}
	    			        		    		            		        }
	    			        		    		            		        
	    			        		    		            		  }
	    			        		    		            		  
	    			        		    		            		  
	    			        		    		            		//  System.out.println(cDelList.size()+" "+tList.size());

	    			        		    		            		/*  if(invoiceList.get(column).contains(".") && invoiceList.get(column).contains("/")){
	    				        		    		            			  String unitRateValue= stringToDouble(columnData); //only one decimal value as result
	    				            		    		            		  invoice.setUnitRate(unitRateValue);
	    				            		    		            		  column=column+1;
	    				        		    		            		  }*/
	    			        		    		            		  
	    			        		    		            		/*  if(invoiceList.get(column).equalsIgnoreCase("o")){
	    				        		    		            			  int value=0;
	    				        		    		            		  invoice.setCaseDeliveredQty(value);
	    				        		    		            		  }else{
	    				        		    		            			  try{
	    				        		    		            			  invoice.setCaseDeliveredQty(Integer.parseInt(invoiceList.get(column).trim()));
	    				        		    		            			  }catch(Exception exc){
	    				        		    		            				  
	    				        		    		            			  }
	    				        		    		            			  }
	    				        		    		            		  column=column+1;
	    				        		    		            		  */
	    				        		    		            	/*	  if(invoiceList.get(column).contains(".")){
	    				        		    		            			  String total= stringToDouble(columnData); //only one decimal value as result
	    				            		    		            		  invoice.setTotal(total);
	    				            		    		            		  column=column+1;
	    				            		    		            		  invoice.setBillRate( stringToDouble(invoiceList.get(column)));
	    				        		    		            		  }
	    				        		    		            		  
	    				        		    		            		  else if(invoiceList.get(column).equalsIgnoreCase("o")){
	    			        		    		            			  int value=0;
	    			        		    		            		  invoice.setBottelsDeliveredQty(value);
	    			        		    		            		  column=column+1;
	    			        		    		            		  invoice.setTotal( stringToDouble(invoiceList.get(column)));
	    			        		    		            		  column=column+1;
	    			        		    		            		  invoice.setBillRate( stringToDouble(invoiceList.get(column)));
	    			        		    		            		  }else{
	    			        		    		            			  try{
	    			        		    		            			  invoice.setBottelsDeliveredQty(Integer.parseInt(invoiceList.get(column).trim()));
	    			        		    		            			  column=column+1;
	    				        		    		            		  invoice.setTotal( stringToDouble(invoiceList.get(column)));
	    				        		    		            		  column=column+1;
	    				        		    		            		  invoice.setBillRate( stringToDouble(invoiceList.get(column)));
	    			        		    		            			  }catch(Exception exc){
	    			        		    		            				  
	    			        		    		            			  }
	    			        		    		            			  }*/
	    			        		    		            		  
	    			        		    		            		 // column=column+1;
	    			        		    		            		 // invoice.setTotal( stringToDouble(invoiceList.get(column)));
	    			        		    		            		 // column=column+1;
	    			        		    		            		 // invoice.setBillRate( stringToDouble(invoiceList.get(column)));
	    			        		    		            		  
	    			        		    		            	  }else{
	    			        		    		            		  
	    			        		    		            		  if(NumberUtils.isParsable(columnData)){ 
	    			        		    		            			//  System.out.println("SI No. :"+columnData);
	    			        		    		            			  
	    			        		    		            			   if(columnData.length()>=4){ //Brand Number
	    			                		    		            		  long brandNumber=Long.parseLong(columnData);
	    			                		    		            	
	    			                		    		            		  invoice.setBrandNumber(brandNumber);
	    			                		    		            
	    			                		    		            	  }
	    			        		    		            			  
	    			        		    		            		  }else {
	    			        		    		            			  
	    			        		    		            			  if(columnData.length()==1){     //Pack Type
	    			        		    		            				  invoice.setPackType(columnData);
	    			        		    		            				  
	    			        		    		            		         }else{   //Brand Name
	    			        		    		            		        	 brandNameAppender=brandNameAppender+" "+columnData;
	    			        		    		            		        	 
	    			        		    		            		        	 
	    			        		    		            		        	 invoice.setBrandName(brandNameAppender);
	    			        		    		            		         }
	    			        		    		            			  
	    			        		    		            		  }
	    			        		    		            		  
	    			        		    		            	  }
	    			        		    		            	  
	    			        		    			   }else{
	    			        		    				   break;
	    			        		    			   }
	    			 
	    			        		    		   }catch(Exception exc){
	    			        		    			  // System.out.println(exc);
	    			        		    		   }
	    			        		    			   columnTemp=column;
	    			        		    	         }
	    		    		            		  
	    		    		            	  }else{
	    		    		            		  columnTemp=columnTemp+1;
	    		    		            		  rowGenerator=false;
	    		    		            		 
	    		    		            	  }
	     		    		            	 
	     		    		            	  }else{
	     		    		            		 columnTemp=columnTemp+1;
	    		    		            		  rowGenerator=false;
	     		    		            	  }
	     		    		              }else{
	     		    		            	 columnTemp=columnTemp+1;
   		    		            		  rowGenerator=false;
	     		    		              }
	     		    		              }
	        		    	   
	     		    		              if(rowGenerator==true){
	     		    		             invoiceObjList.add(invoice);
	     		    		           //  System.out.println("Row count :"+invoiceObjList.size());
	     		    		              }
	        		    			   }catch(Exception exc){
	        		    				   exc.printStackTrace();
	        		    			   }
	        		    			   invoiceMap.put(0, invoiceObjList);
	        		    		   }else{
	        		    			   
	        		    			   try{
	        		    			   
	        		    			   
	        		    			   //After TIN values
	        		    			  // System.out.println("After TIN");
	        		    			   
	        		    			  int salesIndex= invoiceList.indexOf("Sales Value:");
	        		    			  
	        		    			  if(salesIndex!=0 && salesIndex!=-1){
	        		    				  try{
	        		    				  
	        		    			  invoiceData.setSalesValue(stringToDouble(invoiceList.get((salesIndex+1))));
	        		    				  }catch(Exception exc){
	        		    					  invoiceData.setSalesValue("0");  
	        		    				  }
	        		    			  }else{
	        		    				  invoiceData.setSalesValue("0");
	        		    			  }
	        		    			
	        		    			  
	        		    			  int mrpIndex= invoiceList.indexOf("MRP");
	        		    			  if(mrpIndex!=0){
	        		    				  String value=invoiceList.get((mrpIndex+1));
	        		    				  if(value.contains(".")){
	        		    			  invoiceData.setMrpRoundingOff(stringToDouble(value));
	        		    				  }else{
	        		    					  int rounding= invoiceList.indexOf("Rounding");
	        		    					  if(rounding!=0){
	        		    						  invoiceData.setMrpRoundingOff(stringToDouble(invoiceList.get((rounding+1))));
	        		    					  }else{
	        		    						  invoiceData.setMrpRoundingOff("0");
	        		    					  }
	        		    				  }
	        		    			  }else{
	        		    				  invoiceData.setMrpRoundingOff("0");	  
	        		    			  }
	        		    			  
	        		    			  
	        		    			  int retailIndex= invoiceList.indexOf("Retail Shop Excise Turnover Tax:");
	        		    			  if(retailIndex!=0 && retailIndex!=-1){
	        		    				  try{
	        		    			  invoiceData.setRetailShopExciseTurnoverTax(stringToDouble(invoiceList.get((retailIndex+1))));
	        		    				  }catch(Exception exc){
	        		    					  invoiceData.setRetailShopExciseTurnoverTax("0");
	        		    				  }
	        		    			  }else{
	        		    				  invoiceData.setRetailShopExciseTurnoverTax("0");
	        		    			  }
	        		    			 
	        		    			  
	        		    			  int tcsIndex= invoiceList.indexOf("TCS:");
	        		    			  invoiceData.setTcs(stringToDouble(invoiceList.get(tcsIndex+1)));
	        		    			
	        		    			   
	        		    			  int retailerIndex= invoiceList.indexOf("Retailer Credit Balance:");
	        		    			  if(retailerIndex!=0 && retailerIndex!=-1){
	        		    				  
	        		    				  if(invoiceList.get((retailerIndex)).contains("Rs.")){
	        		    					  String[] split=invoiceList.get((retailerIndex)).split(":");
	        		    					  invoiceData.setRetailerCreditBalance(stringToDouble(split[1].replaceAll("Rs", ""))); 
	        		    					  
	        		    				  }else{
	        		    				  
	        		    			  invoiceData.setRetailerCreditBalance(stringToDouble(invoiceList.get((retailerIndex+1) ).replaceAll("Rs","")));
	        		    				  }
	        		    			  }else{
	        		    				  invoiceData.setRetailerCreditBalance("0"); 
	        		    			  }
	        		    			  
	        		    		   }catch(Exception exc){
	        		    			   
	        		    		   }
	        		    			  
	        		    			  invoiceMap.put(1, invoiceData);
	        		    			
	        		    			  break;
	        		    		   }
	        		    		   table=columnTemp;
	        		    	   }
	        		    	  
	        		    	   
	        		    	   
	        		    	   
	        		    	   
	        		       }
	        				 
	        			 
	        		 
	        		 }catch(Exception exc){
	        			 exc.printStackTrace();
	        		 }
	        	 }
	        	 
	        	 
	        	 
	        	 //print output
	        	/* System.out.println("***********************************************");
	        	 invoiceObjList.stream().forEach(System.out::println);
	        	 
	        	 System.out.println("*************************************************");
	               System.out.println("Sales Value : "+invoiceData.getSalesValue());
	               System.out.println("MRP Rounding Off : "+invoiceData.getMrpRoundingOff());
	               System.out.println("Retail Shop Excise Turnover Tax : "+invoiceData.getRetailShopExciseTurnoverTax());
	               System.out.println("TCS : "+invoiceData.getTcs());
	               System.out.println("Retailer Credit Balance : "+invoiceData.getRetailerCreditBalance());
	               System.out.println("*********************************************************************");*/
	               
	               /*Validation logic and call to db*/
	         
	        	 	List<Invoice> localInvoiceList=new ArrayList<Invoice>();
	               for(int i=0;i<invoiceObjList.size();i++)
	               {
	            	   Invoice invoiceData=invoiceObjList.get(i);
	            	   try{
	            	// System.out.println("row is :"+i);
	            	  
	            	   try{
	            		   List resultList=null;
	            		   
	            		 
	            		    resultList=userDao.ocrValidator(invoiceData);
	            		    StringBuilder flagBuilder=new StringBuilder();
	            		    //here 1 means not exist 0 means exist the property
	            		    
	            		   //Flags
	            		    boolean brandNameFlag=false;
	            		    boolean productTypeFlag=false;
	            		    boolean packTypeFlag=false;
	            		    boolean packQtyFlag=false;
	            		    boolean packSizeFlag=false;
	            		    
	            		   
	            		   
	            		   if(resultList.size()<=0){
	            			   invoiceData.setBrand_No_Flag(false);
	            		   }else{
	            			   invoiceData.setBrand_No_Flag(true);
	            			   
	            		   for(Object object : resultList) {
	            			   
	            			  
	            			
	            	            Map row = (Map)object;
	            	           /* System.out.print(" brandNo: " + row.get("brandNo")); 
	            	            System.out.println(", brandName: " + row.get("brandName")); 
	            	            System.out.println(", productType: " + row.get("productType")); 
	            	            System.out.println(", quantity: " + row.get("quantity")); 
	            	            System.out.println(", packType: " + row.get("packType")); 
	            	            System.out.println(", packQty: " + row.get("packQty")); */
	            	            
	            	            
	            	            //BrandName validation
	            	            try{
	            	            if(brandNameFlag==false){
	            	            	 if(row.get("brandName").toString()==null || row.get("brandName").toString().isEmpty() || row.get("brandName").toString().equals("")){
	 	            	            	invoiceData.setBrand_Name_Flag(true);
	 	            	            	 brandNameFlag=true;
	 	            	            }else{
	 	            	            	invoiceData.setBrandName(row.get("brandName").toString());
	 	            	            	invoiceData.setBrand_Name_Flag(true);
	 	            	            }
	            	            }
	            	            }catch(Exception exc){
	            	            	exc.printStackTrace();
	            	            }
	            	            
	            	            
	            	            //Product Type
	            	            try{
	            	            if(productTypeFlag==false){
	            	            if((invoiceData.getProductType().trim()).equalsIgnoreCase(row.get("productType").toString()) || (invoiceData.getProductType().trim()).equals("IML") || (invoiceData.getProductType().trim()).equals("Duty Paid")){
	            	            	invoiceData.setProd_Type_Flag(true);
	            	            	 productTypeFlag=true;
	            	            }else{
	            	            	invoiceData.setProductType(row.get("productType").toString());
	            	            	//invoiceData.setProd_Type_Flag(false);
	            	            }
	            	            }
	            	            }catch(Exception exc){
	            	            	invoiceData.setProductType(row.get("productType").toString());
	            	            	//exc.printStackTrace();
	            	            }
	            	            
	            	            
	            	            //PackType
	            	            try{
	            	            if(packTypeFlag==false){
                                  if((invoiceData.getPackType().trim()).equalsIgnoreCase((String) row.get("packType")) || (invoiceData.getPackType().trim()).equals("C") || (invoiceData.getPackType().trim()).equals("G") || (invoiceData.getPackType().trim()).equals("P")){
	            	            	
                                      invoiceData.setPack_Type_Flag(true);
                                	  packTypeFlag=true;
	            	               }else{
	            	            	   invoiceData.setPackType((String) row.get("packType"));
	            	            	  // invoiceData.setPack_Type_Flag(false);
	            	               }
	            	            }
	            	            }catch(Exception exc){
	            	            	invoiceData.setPackType((String) row.get("packType"));
	            	            	//exc.printStackTrace();
	            	            }
                                  
                                  
                                  //PackQty
	            	            try{
	            	            if(packQtyFlag==false){
                                  if(invoiceData.getPackQty()==Integer.parseInt(row.get("packQty").toString().trim())){
                                      invoiceData.setPack_Qty_Flag(true);
                                	  packQtyFlag=true;
                                	  
                                	  //db call to get btlRate and packQty
                                	  try{
                                	  String brandNoPackQty=invoiceData.getBrandNumber()+""+invoiceData.getPackQty();
                                	  List list=userDao.ocrValidatorBtlAndCase(brandNoPackQty);
                                	  
                                	  Map rowMap = (Map)list.get(0);
                                	  
                                	  String singleBtlRate=Double.toString((double) rowMap.get("SingleBottelRate"));
                                	  String unitRate=Double.toString((double) rowMap.get("packQtyRate"));
                                	  invoiceData.setBtlRate(singleBtlRate);
                                	  invoiceData.setUnitRate(unitRate);
                                	  }catch(Exception exc){
                                		 // exc.printStackTrace();
                                	  }
                                	  
                                  }else{
                                	  invoiceData.setPack_Qty_Flag(false);
                                  }
	            	            }
	            	            }catch(Exception exc){
	            	            	//exc.printStackTrace();
	            	            }
	            	          
	            	            
	            	            try{
                                  if(packSizeFlag==false){
                                  if(invoiceData.getPackSize()==Integer.parseInt((row.get("quantity")).toString().replaceAll("ml","").trim())){
                                	 invoiceData.setPack_Size_Flag(true);
                                	  packSizeFlag=true;
                                	  
                                	
                                	  
                                	  
                                  }else{
                                	  invoiceData.setPack_Size_Flag(false); 
                                  }
                                  }
	            	            }catch(Exception exc){
	            	            	//exc.printStackTrace();
	            	            }
                                  
                                  
                                  
	            	         }
	            		   
	            		   
	            		 
	            		   
	            		   
	            		   //Validation of Case,Bottels delivered ,Unit,btl rate and total
	            		 /*  try{
	            			   
	            			   
	            			   if(invoiceData.getPackQty()>0 && invoiceData.getPackSize() >0 ){
	            				   
	            				   if(invoiceData.getBtlRate()!=null){
	            					   invoiceData.setBtlDelv_Flag(true);
	            				 
	            			   int packQty=invoiceData.getPackQty();
	            			   int packSize=invoiceData.getPackSize();
	            			   int btlDelv=invoiceData.getBottelsDeliveredQty();
	            			   int caseDelv=invoiceData.getCaseDeliveredQty();
	            			   
	            			   double btlRate=Double.parseDouble(invoiceData.getBtlRate());
	            			   
	            			   double unitRate=Math.round(Double.parseDouble(decimalFormat.format(packQty*btlRate)));
	            			   double total=Math.round((unitRate*caseDelv)+(btlDelv*btlRate ));
	            			  System.out.println( "caseDelv:   "+caseDelv+"   BtlDelv:   "+btlDelv+"  BtlRate:   "   +btlRate+"   UnitRate :  "+unitRate+"    "+"  total : "+total);
	            			   
	            			   invoiceData.setUnitRate(String.valueOf(unitRate));
	            			   invoiceData.setTotal(String.valueOf(total));
	            				   }else{
	            					   invoiceData.setBtlDelv_Flag(false);
	            				   }
	            			   
	            			   }
	            		   }catch(Exception exc){
	            			   exc.printStackTrace();
	            		   }*/
	            		   
	            		  
	            	   }
	            		
	            		
	            		   
	            		   
	            	   }catch(Exception exc){
	            		   exc.printStackTrace();
	            	   }
	            	   }catch(Exception exc){
	            		   exc.printStackTrace();
	            	   }
	            	   localInvoiceList.add(invoiceData);
	            	   
	               }
	               
	               
	               
	               //btlMrp and margin fetching..
	               List<Invoice> invoiceFinalList=null;
        		   try{
        			 	// System.out.println("btlMrpAndSplMargin entered initial.........................................");
        			   	
        			   	
        			    invoiceFinalList= ocrDao.suggOcrMarginAndBtlMrp(localInvoiceList);
        			   System.out.println(invoiceFinalList);
        		
       
        		   }catch(Exception exc){
        			   exc.printStackTrace();
        		   }
	        	if(invoiceFinalList!=null){
	               invoiceMap.put(0, invoiceFinalList);
	        	}else{
	        		invoiceMap.put(0, localInvoiceList);
	        	}
	        	// System.out.println("invoiceMap updated ");
	        	 
	        	 
	        	 
	        	 
	        	 
	        	 
	        	 
	    }
	      
		}catch(AmazonTextractException ate){
			logger.error(ate);
	        throw new AWSTextractException();
		}
		catch(SdkClientException ae){
			logger.error(ae);
			throw new AWSAccessDeniedException("AWSAccessDeniedException",ae.getCause());
		}
		
		catch(Exception exc){
			logger.error(exc);
			
			
			
			
/*
			if(exc.getClass().getCanonicalName().contains("SdkClientException")){
				try {
					throw new AWSAccessDeniedException("error",exc.getCause());
				} catch (AWSAccessDeniedException e) {
					// TODO Auto-generated catch block
					logger.error(e);
				}
			}*/
			 
			
		  
			
		}
		
		
		
		return invoiceMap;
	}


	@Override
	public List ocrSuggestions(String brandNo, String flagData) {
		
		return ocrDao.ocrPTypePQtySugg(brandNo, flagData);
	}
	
	@Override
	public List ocrSaveInvoiceJsonData(JSONArray invoiceJson,boolean flag) {
		// TODO Auto-generated method stub
		return ocrDao.ocrSaveInvoiceJsonData(invoiceJson ,flag);
	}
	
	@Override
	public String saveMrpRoundOff(MrpRoundOffBean mrpRoundOffBean) {
		// TODO Auto-generated method stub
		return ocrDao.saveMrpRoundOff(mrpRoundOffBean);
	}

}
