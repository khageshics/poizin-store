package com.zambient.poizon.ocr.service;

import java.util.List;
import java.util.Map;

import org.json.JSONArray;

import com.zambient.poizon.bean.MrpRoundOffBean;
import com.zambient.poizon.ocr.exception.AWSAccessDeniedException;

public interface OCR {
	
	public Map awsTextract(String uploadPath) throws AWSAccessDeniedException;
	public List ocrSuggestions(String brandNo,String flagData);
	public List ocrSaveInvoiceJsonData(JSONArray invoiceJson, boolean flag);
	public String saveMrpRoundOff(MrpRoundOffBean mrpRoundOffBean);

}
