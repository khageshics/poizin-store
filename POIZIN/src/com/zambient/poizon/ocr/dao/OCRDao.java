package com.zambient.poizon.ocr.dao;



import java.util.List;
import java.util.Map;

import org.json.JSONArray;

import com.zambient.poizon.bean.MrpRoundOffBean;
import com.zambient.poizon.ocr.model.Invoice;

public interface OCRDao {

	public List ocrValidator(Invoice invoice);
	public List ocrSuggesion(String brandNo,String flagData);
	public Map<Integer,List> ocrMarginAndBtlMrp(List<Invoice> invoiceDataList);
	public List<Invoice> suggOcrMarginAndBtlMrp(List<Invoice> invoiceDataList);
	public List ocrSuggesionPackQty(String brandNo,String flagData);
	public List ocrSuggesionPackSize(String brandNo,String flagData);
	public List ocrValidatorBtlAndCase(String brandNoPackQty);
	public List ocrPTypePQtySugg(String brandNo,String flagData);
	public List ocrSaveInvoiceJsonData(JSONArray invoiceJs,boolean flag);
	public List ocrSuggSingleMrpAndMargin(long brandNo,int quantity);
	public String saveMrpRoundOff(MrpRoundOffBean mrpRoundOffBean);
}
