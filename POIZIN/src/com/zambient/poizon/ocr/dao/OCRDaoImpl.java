package com.zambient.poizon.ocr.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.jboss.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.zambient.poizon.bean.MrpRoundOffBean;
import com.zambient.poizon.ocr.controller.OCRController;
import com.zambient.poizon.ocr.exception.DatabaseException;
import com.zambient.poizon.ocr.exception.ProductNotFound;
import com.zambient.poizon.ocr.exception.SaveJsonInvoiceData;
import com.zambient.poizon.ocr.model.Invoice;
import com.zambient.poizon.utill.UserSession;

@Repository
public class OCRDaoImpl implements OCRDao{
	
	@Autowired
	private UserSession userSession;
	
	private static final Logger logger=Logger.getLogger(OCRDaoImpl.class);
	@Autowired
	private SessionFactory sessionFactory;
	
	  @Autowired
		private JdbcTemplate jdbcTemplate;
	  
	  @Autowired
	  private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	@Override
	public List ocrValidator(Invoice invoice) {
		//String qry="select prod.brandNo,prod.brandName,prod.productType,prod.quantity,prod.packQty,prod.packType from Product prod where prod.brandNo=:brandNumber";
		
				//Query query=sessionFactory.openSession().createQuery(qry);
				
				String qry="select brandNo,brandName,productType,quantity,packQty,packType from product where brandNo=:brandNumber";
				
				Session session=sessionFactory.openSession();
				Query query=session.createSQLQuery(qry);
				query.setLong("brandNumber", invoice.getBrandNumber());
				 query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
				 List list=query.list();
				 session.close();
				return list;
	}

	@Override
	public List ocrSuggesion(String brandNo, String flagData) {
	String qry=null;
		
		if(flagData.equals("brandName")){
			
	       qry="select DISTINCT brandName from product where realBrandNo=:brandNumber";
		}else if(flagData.equals("packType")){
			qry="select packType from product where realBrandNo=:brandNumber";
		}else if(flagData.equals("packQty")){
			qry="select packQty from product where realBrandNo=:brandNumber";
		}else if(flagData.equals("productType")){
			qry="select DISTINCT productType from product where realBrandNo=:brandNumber";
		}else if(flagData.equals("packSize")){
			qry="select quantity from product where realBrandNo=:brandNumber";
			flagData="quantity";
		}
		Session session=sessionFactory.openSession();
		Query query=session.createSQLQuery(qry);
		query.setLong("brandNumber", Long.parseLong(brandNo));
		 query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		 List<String> data=query.list();
		 List resultList=new ArrayList<String>();
		 for(Object result:data){
			 
			  Map row = (Map)result;
			 resultList.add(row.get(flagData));
		 }
		 
		 session.close();
		 
		return resultList;
	}

	@Override
	public Map<Integer, List> ocrMarginAndBtlMrp(List<Invoice> invoiceDataList) throws ProductNotFound{
		int storeId=userSession.getStoreId();
		
		 String qry="SELECT DISTINCT  bottleSaleMrp ,specialMargin  FROM invoice WHERE store_id=" +storeId+ " AND brandNoPackQty=:brandNoPackQty ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1";

         Map map=new HashMap<String,List>();
         List<Long> newBrandNoList=new ArrayList();
           for(int i=0;i<invoiceDataList.size();i++){
        	  
        	   
        	   //Prepare brandNoPackQty 
        	  String subQry= "SELECT * FROM product WHERE realBrandNo=:realBrandNo AND quantity=:quantity";
        	   
        	   
        	  Session globalSession=sessionFactory.openSession();
        	  Query globalQry=globalSession.createSQLQuery(subQry);
        	  globalQry.setLong("realBrandNo",(invoiceDataList.get(i).getBrandNumber()));
        	  globalQry.setString("quantity", String.valueOf(invoiceDataList.get(i).getPackSize())+" ml");
        	  globalQry.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
   			 List brandNoPackQtyList=globalQry.list();
   			 globalSession.close();
   			 double brandNoPackQtyId=0;
   			 if(brandNoPackQtyList.size()>0){
   				 for(Object listData:brandNoPackQtyList){
                	   Map resultMap=(Map)listData;
                	   brandNoPackQtyId= (double) resultMap.get("brandNoPackQty");
                	   
                   }
   			
   			
   			
        	   
        	   
          	 Session session=sessionFactory.openSession();
          	 session.beginTransaction();
          	 
          	 try{
          	   Query query=session.createSQLQuery(qry);   
      			query.setDouble("brandNoPackQty", brandNoPackQtyId);
      			 query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
      			 List resultData=query.list();
                 List resultList=new ArrayList();
                 resultList.add(brandNoPackQtyId);
                 
                 if(resultData.size()>1){
                 for(Object listData:resultData){
              	   Map resultMap=(Map)listData;
          
              	 //  System.out.println(resultMap.get("bottleSaleMrp"));
              	  // System.out.println(resultMap.get("specialMargin"));
              	   resultList.add(resultMap.get("bottleSaleMrp"));
              	   resultList.add(resultMap.get("specialMargin"));
              	   
              	   map.put(i, resultList);
                 }
                 }else{
                	 map.put(i, resultList); 
                 }
                 
                 
                 session.getTransaction().commit();
                 session.close();
          	 }catch(Exception exc){
          		 exc.printStackTrace();
          	 }
          	 
   			 }else{
   				newBrandNoList.add(invoiceDataList.get(i).getBrandNumber());
   				 //prepare error response as product not found
   			 }
                 
                
           }
           
           if(newBrandNoList.size()>0){
 				 throw new ProductNotFound(newBrandNoList+" : Not Found, please add product first...");

           }
         
      
		return map;
	}
	
	
	@Override
	public List<Invoice> suggOcrMarginAndBtlMrp(List<Invoice> invoiceDataList) throws ProductNotFound{
		int storeId=userSession.getStoreId();
		 String qry="SELECT DISTINCT  bottleSaleMrp ,specialMargin  FROM invoice WHERE store_id="+storeId+" AND brandNoPackQty=:brandNoPackQty ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1";

         Map map=new HashMap<String,List>();
         
           for(int i=0;i<invoiceDataList.size();i++){
        	   
        	   //Prepare brandNoPackQty 
        	  String subQry= "SELECT * FROM product WHERE realBrandNo=:realBrandNo AND quantity=:quantity";
        	   
        	   
        	  Session globalSession=sessionFactory.openSession();
        	  Query globalQry=globalSession.createSQLQuery(subQry);
        	  long brandNumber=invoiceDataList.get(i).getBrandNumber();
        	  globalQry.setLong("realBrandNo",(brandNumber));
        	  int packSize=invoiceDataList.get(i).getPackSize();
        	  globalQry.setString("quantity", String.valueOf(packSize)+" ml");
        	  globalQry.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
   			 List brandNoPackQtyList=globalQry.list();
   			 globalSession.close();
   			 double brandNoPackQtyId=0;
   			 if(brandNoPackQtyList.size()>0){
   				 for(Object listData:brandNoPackQtyList){
                	   Map resultMap=(Map)listData;
                	   brandNoPackQtyId= (double) resultMap.get("brandNoPackQty");
                	   
                   }
   			
   	
        	   
          	 Session session=sessionFactory.openSession();
          	 session.beginTransaction();
          	 
          	 try{
          	   Query query=session.createSQLQuery(qry);   
      			query.setDouble("brandNoPackQty", brandNoPackQtyId);
      			 query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
      			 List resultData=query.list();
                
                 
                 for(Object listData:resultData){
              	   Map resultMap=(Map)listData;
          
              	 //  System.out.println(resultMap.get("bottleSaleMrp"));
              	  // System.out.println(resultMap.get("specialMargin"));
              	   invoiceDataList.get(i).setBtlMrp((double)resultMap.get("bottleSaleMrp")); 
              	   invoiceDataList.get(i).setBtlMrp_Flag(true);
              	   invoiceDataList.get(i).setMargin((double)resultMap.get("specialMargin"));
              	   invoiceDataList.get(i).setMargin_Flag(true);
              	   
              	 
                 }
                 session.getTransaction().commit();
                 session.close();
          	 }catch(Exception exc){
          		 exc.printStackTrace();
          	 }
          	 
   			 }else{
   				 
   				 //prepare error response as product not found
   				// throw new ProductNotFound(invoiceDataList.get(i).getBrandNumber()+" : Not Found, please add product first...");
   			 }
                 
                
           }
         
      
		return invoiceDataList;
	}

	@Override
	public List ocrSuggesionPackQty(String brandNo, String flagData) {
		int storeId=userSession.getStoreId();
		List resultlistData=new ArrayList();
		
		String qry1="SELECT packQty,quantity FROM product WHERE realBrandNo=:brandNumber";
		String qry2="SELECT DISTINCT p.brandNo,p.packQty,p.quantity ,i.SingleBottelRate,i.packQtyRate FROM product p INNER JOIN invoice i ON  p.`brandNoPackQty`=i.`brandNoPackQty` WHERE i.store_id="+storeId+" AND p.brandNoPackQty=:brandNoPackQty ORDER BY invoiceDateAsPerSheet DESC LIMIT 1";
		try{
			Session session=sessionFactory.openSession();
			session.beginTransaction();
			Query query=session.createSQLQuery(qry1);
			query.setLong("brandNumber", Long.parseLong(brandNo));
			 query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			 List data=query.list();
			 session.close();
			 
			 String temp_brandNoPackQty="";
			 
			 for(Object listData:data){
            	   Map resultMap=(Map)listData;
        
            	
            	  String packQty= resultMap.get("packQty").toString();
            	   String packSize=resultMap.get("quantity").toString();
            	   
            	   Session session1=sessionFactory.openSession();
           		session1.beginTransaction();
            	   Query query2=session1.createSQLQuery(qry2);
            		String brandNoPackQty=brandNo+""+packQty;
            		if(!brandNoPackQty.equals(temp_brandNoPackQty)){
            			temp_brandNoPackQty=brandNoPackQty;
            		}else{
            			packSize=packSize.replaceAll("ml", "").trim();
            			brandNoPackQty=brandNo+""+packSize+""+packQty;
            		}
            		
   				query2.setLong("brandNoPackQty", Long.parseLong(brandNoPackQty));
   				 query2.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
   				 List totalList=query2.list();
   				 
   				
   				 session1.close();
   				 
   				 if(totalList.size()>0){
   			    for(Object btlData:totalList){
               	   Map btlMap=(Map)btlData;
 
               	resultlistData.add( btlMap);
                  }
   				 }else{
   				  for(Object btlData:data){
                  	   Map btlMap=(Map)btlData;
    
                  	resultlistData.add( btlMap);
                     }
   				 }
            	   
            	 
               }
			 
			
			 
			 
				
			
		}catch(Exception exc){
			exc.printStackTrace();
		}
		return resultlistData;
	}

	@Override
	public List ocrSuggesionPackSize(String brandNo, String flagData) {
		Session session=sessionFactory.openSession();
		session.beginTransaction();
		List resultData=null;
		String brandNoPackQty=brandNo+""+flagData;
		String qry="SELECT packQty,quantity FROM product WHERE realBrandNo=:brandNumber";
		try{
			
			Query query=session.createSQLQuery(qry);
			query.setLong("brandNoPackQty", Long.parseLong(brandNo));
			 query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			 resultData=query.list();
			 
			 
			 session.close();
			
		}catch(Exception exc){
			
		}
		return resultData;
	
	}

	@Override
	public List ocrValidatorBtlAndCase(String brandNoPackQty) {
		int storeId=userSession.getStoreId();
		Session session=sessionFactory.openSession();
		session.beginTransaction();
		List resultData=null;
		String qry="SELECT DISTINCT SingleBottelRate,packQtyRate FROM invoice WHERE store_id="+storeId+" AND  brandNoPackQty=:brandNoPackQty ORDER BY invoiceDateAsPerSheet DESC LIMIT 1";
		try{
			
			Query query=session.createSQLQuery(qry);
			query.setLong("brandNoPackQty", Long.parseLong(brandNoPackQty));
			 query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			 resultData=query.list();
			 
			 
			 session.close();
			
		}catch(Exception exc){
			exc.printStackTrace();
		}
		return resultData;
	}

	@Override
	public List ocrPTypePQtySugg(String brandNo, String flagData) {
		List result=null;
		try{
			
			Session session=sessionFactory.openSession();
			session.beginTransaction();
			
			String qryStr="SELECT packType,quantity FROM product WHERE brandNo=:brandNumber";
			Query query=session.createSQLQuery(qryStr);
			query.setLong("brandNumber", Long.parseLong(brandNo));
			 query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			 result=query.list();
		
			 session.close();
			 
			 
		}catch(Exception exc){
			throw new DatabaseException();
		}
		return result;
	}
	
	
	/**
	 * This method is used to save invoice data to 'invoice' table.
	 * As input getting data in json format.
	 * */
	@Override
	public List ocrSaveInvoiceJsonData(JSONArray invoiceJson,boolean flag) {
		int storeId=userSession.getStoreId();
		List resultList=new ArrayList();
		logger.info("OCR : PoizonDaoImpl.saveInvoiceJsonData invoiceJson= "+invoiceJson);
		String QUERY="insert into invoice (brandNoPackQty,caseQty,packQty,QtyBottels,SingleBottelRate,packQtyRate,bottleSaleMrp,totalPrice,receivedBottles,invoiceDate,invoiceDateAsPerSheet,specialMargin,store_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		String result=null;
		
		synchronized(this){
		try {
			
			
			if(flag==false){
			
			
			 JSONObject jsObj= invoiceJson.getJSONObject(0);
			/*check already data saved on the same day*/
				String qry="SELECT p.brandNo,p.brandName,p.productType,p.packType,p.packQty,p.quantity ,i.caseQty,i.QtyBottels,i.packQtyRate,i.SingleBottelRate,i.bottleSaleMrp,i.specialMargin,i.totalPrice FROM product p INNER JOIN invoice i ON  p.`brandNoPackQty`=i.`brandNoPackQty` WHERE i.store_id="+storeId+" AND i.invoiceDate='"+jsObj.get("date")+"' AND i.invoiceDateAsPerSheet='"+jsObj.get("dateAsCopy")+"'";

			List<Invoice> resultInvData=jdbcTemplate.query(qry,new RowMapper<Invoice>(){  
			    @Override  
			    public Invoice mapRow(ResultSet rs, int rownumber) throws SQLException {  
			    	Invoice e=new Invoice();  
			        e.setBrandNumber(rs.getLong(1));  
			        e.setBrandName(rs.getString(2));  
			        e.setProductType(rs.getString(3));
			        e.setPackType(rs.getString(4));
			        e.setPackQty(rs.getInt(5));
			        e.setPackSize(rs.getInt(6));
			        e.setCaseDeliveredQty(rs.getInt(7));
			        e.setBottelsDeliveredQty(rs.getInt(8));
			        e.setUnitRate(String.valueOf(rs.getDouble(9)));
			        e.setBtlRate(String.valueOf(rs.getDouble(10)));
			        e.setMargin(rs.getDouble(11));
			        e.setBtlMrp(rs.getDouble(12));
			        e.setTotal(String.valueOf(rs.getDouble(13)));
			        return e;  
			    }  
			    });  
			
			System.out.print(resultInvData);
			
			if(resultInvData.size()>0){
				
				resultList= resultInvData;
				
			}else{
			
			
			for (int i = 0; i < invoiceJson.length(); ++i) {
			 JSONObject rec;
			    rec = invoiceJson.getJSONObject(i);
			    
			    //validation whether this recored already existed in db or not
				List recordList = jdbcTemplate.queryForList("SELECT * FROM invoice WHERE store_id="+storeId+" AND brandNoPackQty='"+rec.getDouble("brandNoPackQtyId")+"' AND invoiceDate='"+rec.get("date")+"' AND invoiceDateAsPerSheet='"+rec.get("dateAsCopy")+"'");
			    
			    if(recordList.size()<=0){
			    double singleBtlPrice=rec.getDouble("SingleBottelRate");
			    int ttlBtls=rec.getInt("caseQty")*rec.getInt("packQtyId")+rec.getInt("QtyBottels");
				//Double total=(double) ((rec.getInt("caseQty")*rec.getInt("packQtyId")+rec.getInt("QtyBottels"))*singleBtlPrice);
			   /* Double total=ttlBtls*singleBtlPrice;
				Double totalPrice = (double) Math.round(total);*/
			    try{
					int val=jdbcTemplate.update(QUERY,new Object[]{rec.getDouble("brandNoPackQtyId"),rec.getInt("caseQty"),rec.getInt("packQtyId"),rec.getInt("QtyBottels"),rec.getDouble("SingleBottelRate"),rec.getDouble("packQtyRate"),rec.getDouble("EachBottleMrp"),rec.getDouble("totalValue"),ttlBtls,rec.get("date"),rec.get("dateAsCopy"),rec.getDouble("marginVal"),storeId});
						if(val>0)
							result="Invoice Added Successfully";
						    resultList.add("201");
				}catch(Exception e){
					result="something went wrong";
					resultList.add("201");
					logger.error( "saveInvoiceJsonData in PoizonDaoImpl exception", e);
					e.printStackTrace();
					SaveJsonInvoiceData saveJsonInvoiceData=new SaveJsonInvoiceData();
					saveJsonInvoiceData.setMessage(result);
					saveJsonInvoiceData.setStatus("417");
					throw saveJsonInvoiceData;
				
					}
			    
			}else{
				result="Records Already Submitted";
				resultList.add("200");
			}
			    
			}
			
			
		}
			
			
			
			}else{
				
				
				System.out.println("falg is  "+flag);
				for (int i = 0; i < invoiceJson.length(); ++i) {
					 JSONObject rec;
					    rec = invoiceJson.getJSONObject(i);
					    
					    //validation whether this recored already existed in db or not
					    List<Map<String, Object>> recordList = jdbcTemplate.queryForList("SELECT * FROM invoice WHERE store_id="+storeId+" AND brandNoPackQty='"+rec.getDouble("brandNoPackQtyId")+"' AND invoiceDate='"+rec.get("date")+"' AND invoiceDateAsPerSheet='"+rec.get("dateAsCopy")+"'");
					    
						
					    if(recordList.size()<=0){
					    double singleBtlPrice=rec.getDouble("SingleBottelRate");
					    int ttlBtls=rec.getInt("caseQty")*rec.getInt("packQtyId")+rec.getInt("QtyBottels");
						//Double total=(double) ((rec.getInt("caseQty")*rec.getInt("packQtyId")+rec.getInt("QtyBottels"))*singleBtlPrice);
					   /* Double total=ttlBtls*singleBtlPrice;
						Double totalPrice = (double) Math.round(total);*/
					    try{
							int val=jdbcTemplate.update(QUERY,new Object[]{rec.getDouble("brandNoPackQtyId"),rec.getInt("caseQty"),rec.getInt("packQtyId"),rec.getInt("QtyBottels"),rec.getDouble("SingleBottelRate"),rec.getDouble("packQtyRate"),rec.getDouble("EachBottleMrp"),rec.getDouble("totalValue"),ttlBtls,rec.get("date"),rec.get("dateAsCopy"),rec.getDouble("marginVal"),storeId});
								if(val>0)
									result="Invoice Added Successfully";
								    resultList.add("201");
						}catch(Exception e){
							result="something went wrong";
							resultList.add("201");
							logger.error( "saveInvoiceJsonData in PoizonDaoImpl exception", e);
							e.printStackTrace();
							SaveJsonInvoiceData saveJsonInvoiceData=new SaveJsonInvoiceData();
							saveJsonInvoiceData.setMessage(result);
							saveJsonInvoiceData.setStatus("417");
							throw saveJsonInvoiceData;
						
							}
					    
					}else{
						try{
							System.out.println("");
							 double singleBtlPrice=rec.getDouble("SingleBottelRate");
							 int ttlBtls=rec.getInt("caseQty")*rec.getInt("packQtyId")+rec.getInt("QtyBottels");
							    
							    
						/*	rec.getDouble("brandNoPackQtyId");
							
							rec.getInt("packQtyId");
							rec.getInt("QtyBottels");
							rec.getDouble("SingleBottelRate");
							rec.getDouble("packQtyRate");
							rec.getDouble("EachBottleMrp");
							rec.getDouble("totalValue");
						    ttlBtls;
						    rec.get("date");
							rec.get("dateAsCopy");
							rec.getDouble("marginVal");*/
							 
							 
							int caseQtySumm= (int) recordList.get(0).get("caseQty")+rec.getInt("caseQty");
							System.out.println("");
									
								
							int QtyBottelsSumm=((int) recordList.get(0).get("QtyBottels"))+rec.getInt("QtyBottels");
							double totalSumm=(double)recordList.get(0).get("totalPrice")+rec.getDouble("totalValue");
						int totalbtlsRecSumm=(int)recordList.get(0).get("receivedBottles")+ttlBtls;
							int val1 = jdbcTemplate.update("update `invoice` set caseQty='"+caseQtySumm+"',QtyBottels='"+QtyBottelsSumm+"',receivedBottles='"+totalbtlsRecSumm+"',totalPrice='"+totalSumm+"'     WHERE store_id="+storeId+" AND brandNoPackQty='"+rec.getDouble("brandNoPackQtyId")+"' AND invoiceDate='"+rec.get("date")+"' AND invoiceDateAsPerSheet='"+rec.get("dateAsCopy")+"'");
							resultList.add("201");
							if(val1>0){
								System.out.println("updated" +val1);
							}else{
								System.out.println("Not updated");
							}
							
						}catch(Exception exc){
							SaveJsonInvoiceData saveJsonInvoiceData=new SaveJsonInvoiceData();
							saveJsonInvoiceData.setMessage(result);
							saveJsonInvoiceData.setStatus("417");
							throw saveJsonInvoiceData;
						}
					}
					    
					}
				
				
				
			}
			
			
			
			
			if(invoiceJson.length() > 0)
				updateStockLiftingDiscount(invoiceJson.getJSONObject(0).getString("dateAsCopy"));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			SaveJsonInvoiceData saveJsonInvoiceData=new SaveJsonInvoiceData();
			result="Something Went Wrong.";
			logger.error("saveInvoiceJsonData in PoizonDaoImpl exception", e);
			e.printStackTrace();
			saveJsonInvoiceData.setMessage(result);
			saveJsonInvoiceData.setStatus("417");
			throw saveJsonInvoiceData;
			
		}
		}
		return resultList;
		
	}
	
	private void updateStockLiftingDiscount(String date) {
	    logger.info("PoizonDaoImpl :: updateStockLiftingDiscount()");
	    String QUERY = "SELECT COALESCE(ROUND(CEIL(SUM(s.receivedBottles/p.packQty))),0) AS liftedCase, p.`shortBrandName`,p.`realBrandNo`,"
	    		+ "(SELECT COALESCE(SUM(d.discountAmount),0) FROM stock_lift_with_discount d WHERE d.brandNo=p.realBrandNo AND store_id="+userSession.getStoreId()+""
	    		+ " AND d.stockDiscountDate=(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+date+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))) AS discountAmount,"
	    		+ "(SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+date+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01')) AS stockDate"
	    		+ " FROM product p INNER JOIN invoice s ON p.`brandNoPackQty` = s.`brandNoPackQty` AND s.store_id="+userSession.getStoreId()+" "
	    		+ "AND s.`invoiceDateAsPerSheet`>= (SELECT CONCAT(DATE_FORMAT(LAST_DAY('"+date+"' - INTERVAL 0 MONTH),'%Y-%m-'),'01'))"
	    		+ " AND  s.invoiceDateAsPerSheet <= (SELECT LAST_DAY('"+date+"')) GROUP BY p.`realBrandNo`";
	    try{
	    	List<Map<String, Object>> rows = jdbcTemplate.queryForList(QUERY);
	    	if(rows.size() > 0 && rows !=null){
	        for(Map row : rows){
	        	double caseQty = row.get("liftedCase")!=null?Double.parseDouble(row.get("liftedCase").toString()):0;
	        	int brandNo = row.get("realBrandNo")!=null?Integer.parseInt(row.get("realBrandNo").toString()):0;
	        	double discountAmt = row.get("discountAmount")!=null?Double.parseDouble(row.get("discountAmount").toString()):0;
	        	double totatDiscount = caseQty * discountAmt;
	        	String discDate = row.get("stockDate")!=null?row.get("stockDate").toString():"";
	        	jdbcTemplate.update("UPDATE stock_lift_with_discount SET  totalDiscountAmount=? WHERE brandNo = ? AND stockDiscountDate= ? AND store_id= ?", new Object[]{totatDiscount, brandNo, discDate, userSession.getStoreId()});
	        }
	    		
	    	}
	    }catch(Exception e){
	    	e.printStackTrace();
	    }
			
		}

	@Override
	public List ocrSuggSingleMrpAndMargin(long brandNo, int quantity) {
		 String qry="SELECT DISTINCT  bottleSaleMrp ,specialMargin  FROM invoice WHERE brandNoPackQty=:brandNoPackQty ORDER BY `invoiceDateAsPerSheet` DESC LIMIT 1";

         List resultList=new ArrayList();
         
        	   
        	   //Prepare brandNoPackQty 
        	  String subQry= "SELECT * FROM product WHERE realBrandNo=:realBrandNo AND quantity=:quantity";
        	   
        	   
        	  Session globalSession=sessionFactory.openSession();
        	  Query globalQry=globalSession.createSQLQuery(subQry);
        	  globalQry.setLong("realBrandNo",brandNo);
        	  globalQry.setString("quantity", String.valueOf(quantity)+" ml");
        	  globalQry.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
   			 List brandNoPackQtyList=globalQry.list();
   			 globalSession.close();
   			 double brandNoPackQtyId=0;
   			 if(brandNoPackQtyList.size()>0){
   				 for(Object listData:brandNoPackQtyList){
                	   Map resultMap=(Map)listData;
                	   brandNoPackQtyId= (double) resultMap.get("brandNoPackQty");
                	   
                   }
   			
 
          	 Session session=sessionFactory.openSession();
          	 session.beginTransaction();
          	 
          	 try{
          	   Query query=session.createSQLQuery(qry);   
      			query.setDouble("brandNoPackQty", brandNoPackQtyId);
      			 query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
      			 List resultData=query.list();
             
                 
                 for(Object listData:resultData){
              	   Map resultMap=(Map)listData;
          
              	 //  System.out.println(resultMap.get("bottleSaleMrp"));
              	  // System.out.println(resultMap.get("specialMargin"));
              	   resultList.add(resultMap.get("bottleSaleMrp"));
              	   resultList.add(resultMap.get("specialMargin"));
              	   resultList.add(true);
              	   resultList.add(true);
              	   
                 }
                 session.getTransaction().commit();
                 session.close();
          	 }catch(Exception exc){
          		 exc.printStackTrace();
          	 }
          	 
   			 }else{
   				 //prepare error response as product not found
   				resultList.add(0.0);
   				resultList.add(0.0);
   				resultList.add(false);
   				resultList.add(false);
   			 }
                 

      
		return resultList;
	}

	@Override
	public String saveMrpRoundOff(MrpRoundOffBean mrpRoundOffBean) {
		
		int storeId=userSession.getStoreId();
		
		logger.info("PoizonSecondDaoImpl.saveMrpRoundOff");
		String result = null;
		try{
			
			/*validate the duplicate data*/

		      MapSqlParameterSource parameters = new MapSqlParameterSource();
		      parameters.addValue("storeId", storeId);
		      parameters.addValue("mrpRoundOffDate", mrpRoundOffBean.getDate());
		      
		  
			String query="select * from invoice_mrp_round_off where store_id=:storeId AND mrpRoundOffDate= :mrpRoundOffDate";
			MrpRoundOffBean resultData=null;
			try{
			resultData =namedParameterJdbcTemplate.queryForObject(query,
					 parameters, new RowMapper() {
					 public Object mapRow(ResultSet resultSet, int rowNum)
					  {
						 MrpRoundOffBean resultBean=new MrpRoundOffBean();
						 try{
						 resultBean.setMrpRoundOff(resultSet.getDouble(2));
						 resultBean.setTurnOverTax(resultSet.getDouble(3));
						 resultBean.setTcsVal(resultSet.getDouble(4));
						 resultBean.setDate(resultSet.getString(5));
						 resultBean.setDateAsPerCopy(resultSet.getString(6));
						 resultBean.setRetailerCreditVal(resultSet.getDouble(7));
						 }catch(SQLException exc){
							 
						 }
					       return resultBean;
					 }
					 });
			}catch(Exception exc){
				resultData=null;
			}

			
			System.out.println(resultData);
			if(resultData!=null){
			
				double mrpRoundOff=(resultData.getMrpRoundOff()) + (mrpRoundOffBean.getMrpRoundOff());
				double turnoverTax=resultData.getTurnOverTax()+mrpRoundOffBean.getTurnOverTax();
				double tcsValue=resultData.getTcsVal()+mrpRoundOffBean.getTcsVal();
				//double retailCB=resultData.getRetailerCreditVal()+mrpRoundOffBean.getRetailerCreditVal();
				double retailCB=mrpRoundOffBean.getRetailerCreditVal();

				int val = jdbcTemplate.update("update `invoice_mrp_round_off` set mrpRoundOff='"+mrpRoundOff+"',turnoverTax='"+turnoverTax+"',tcsVal='"+tcsValue+"',retailer_credit_balance='"+retailCB+"' where mrpRoundOffDate='"+mrpRoundOffBean.getDate()+"'  AND store_id="+storeId+"" );
				result="MrpRoundOff Updated Successfully";
			}else{
			
				jdbcTemplate.update("INSERT INTO `invoice_mrp_round_off` (mrpRoundOff,turnoverTax,tcsVal,mrpRoundOffDate,mrpRoundOffDateAsCopy,retailer_credit_balance,store_id) VALUES(?,?,?,?,?,?,?) ",mrpRoundOffBean.getMrpRoundOff(),mrpRoundOffBean.getTurnOverTax(),mrpRoundOffBean.getTcsVal(),mrpRoundOffBean.getDate(),mrpRoundOffBean.getDateAsPerCopy(),mrpRoundOffBean.getRetailerCreditVal(),storeId);
				result="MrpRoundOff Saved Successfully";
			}
			
			
		}catch(Exception e){
			result="Something Went Wrong.";
			//logger.(Level.WARN, "saveMrpRoundOff in PoizonSecondDaoImpl exception", e);
			e.printStackTrace();
			throw new DatabaseException();
		}
		return result;
	}

}
