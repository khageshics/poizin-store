package com.zambient.poizon.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.FlashMapManager;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.zambient.poizon.utill.UserSession;

public class PoizonInterceptor implements HandlerInterceptor {
	final static Logger log = Logger.getLogger(PoizonInterceptor.class);
	@Autowired 
	private UserSession userSession;
	
	@Override
	public boolean preHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler) throws Exception {
		
		 String uri = request.getRequestURI();
		
		System.out.println("uri> "+uri);
		//log.info("uri> "+uri);
		//log.info("Usersession getUserId>> "+userSession.getUserId());
		System.out.println("Usersession getUserId>> "+userSession.getUserId());
		if(userSession !=null && userSession.getUserId() != null && !(userSession.getUserId().trim().equals(""))){
			return true;
		}else{
			 /*if(uri.startsWith("/POIZIN/mobileView")){
				 return true;
			}else{*/
			// create a flashmap
			FlashMap flashMap = new FlashMap();
			// store the message
			flashMap.put("message", "Session Expired, Please Login.");
			// create a flashmapMapManger with `request`
			FlashMapManager flashMapManager = RequestContextUtils.getFlashMapManager(request);
			// save th e flash map data in session with falshMapManager
			flashMapManager.saveOutputFlashMap(flashMap, request, response);
			if(uri.startsWith("/POIZIN/mobileView")){
				response.sendRedirect("loginInMobile");
			}else{
			response.sendRedirect("admin");
			}
			return false;
		//}
		//return false;
	   }
	}
	@Override
	public void afterCompletion(HttpServletRequest request,
            HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void postHandle(HttpServletRequest request,
            HttpServletResponse response, Object handler,
            ModelAndView modelAndView)
			throws Exception {
		// TODO Auto-generated method stub
		
	}

}
