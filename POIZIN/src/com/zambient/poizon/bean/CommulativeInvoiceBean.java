package com.zambient.poizon.bean;

public class CommulativeInvoiceBean {
	
	private String invoiceDate;
	private Long totalInvoiceBtls;
	private double discoutnPerBtl;
	private String invoiceRealDate;
	private Double singleBtlRate;
	private Double btlSaleMrp;
	private Double specialMargin;
	private Double turnOverTax;
	
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public Long getTotalInvoiceBtls() {
		return totalInvoiceBtls;
	}
	public void setTotalInvoiceBtls(Long totalInvoiceBtls) {
		this.totalInvoiceBtls = totalInvoiceBtls;
	}
	public double getDiscoutnPerBtl() {
		return discoutnPerBtl;
	}
	public void setDiscoutnPerBtl(double discoutnPerBtl) {
		this.discoutnPerBtl = discoutnPerBtl;
	}
	public String getInvoiceRealDate() {
		return invoiceRealDate;
	}
	public void setInvoiceRealDate(String invoiceRealDate) {
		this.invoiceRealDate = invoiceRealDate;
	}
	public Double getSingleBtlRate() {
		return singleBtlRate;
	}
	public void setSingleBtlRate(Double singleBtlRate) {
		this.singleBtlRate = singleBtlRate;
	}
	public Double getBtlSaleMrp() {
		return btlSaleMrp;
	}
	public void setBtlSaleMrp(Double btlSaleMrp) {
		this.btlSaleMrp = btlSaleMrp;
	}
	
	public Double getSpecialMargin() {
		return specialMargin;
	}
	public void setSpecialMargin(Double specialMargin) {
		this.specialMargin = specialMargin;
	}
	public Double getTurnOverTax() {
		return turnOverTax;
	}
	public void setTurnOverTax(Double turnOverTax) {
		this.turnOverTax = turnOverTax;
	}
	@Override
	public String toString() {
		return "CommulativeInvoiceBean [invoiceDate=" + invoiceDate + ", totalInvoiceBtls=" + totalInvoiceBtls
				+ ", discoutnPerBtl=" + discoutnPerBtl + ", invoiceRealDate=" + invoiceRealDate + ", singleBtlRate="
				+ singleBtlRate + ", btlSaleMrp=" + btlSaleMrp + ", specialMargin=" + specialMargin + ", turnOverTax="
				+ turnOverTax + "]";
	}

	
	

}
