package com.zambient.poizon.bean;

import java.io.Serializable;

public class ProductWithStatement implements Serializable {
	
	private Long brandNo;
	private Double invoicePrice;
	private Long sale;
	
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public Double getInvoicePrice() {
		return invoicePrice;
	}
	public void setInvoicePrice(Double invoicePrice) {
		this.invoicePrice = invoicePrice;
	}
	public Long getSale() {
		return sale;
	}
	public void setSale(Long sale) {
		this.sale = sale;
	}

}
