package com.zambient.poizon.bean;

import java.io.Serializable;

public class DiscountEstimationBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long brandNo;
	private String brandName;
	private Double caseRate;
	private String category;
	private String company;
	private Long noOfCases;
	private Double cashBackOnPerPrice;
	private Double priceToPurches;
	private Double cashBackOnOrder;
	private String profitPercentageDiscount;
	private Double L2;
	private Double L1;
	private Double SB;
	private Double D;
	private Double LB;
	private Double TIN;
	private Double N;
	private Double P;
	private Double Q;
	private Double X;
	private Long estimateDays;
	private String color;
	private Long orderBy;
	private Long target;
	private Double lastMonthSold;
	private Long companyOrder;
	private String companyColor; 
	private Double inHouseStock;
	private Double L2Stock;
	private Double L1Stock;
	private Double SBStock;
	private Double DStock;
	private Double LBStock;
	private Double TINStock;
	private Double NStock;
	private Double PStock;
	private Double QStock;
	private Double XStock;
	private Double pending;
	private Double LiftedCase;
	private Long commitment;
	private Double needCase;
	private String productType;
	private Double L2SpecialMargin;
	private Double L1SpecialMargin;
	private Double SBSpecialMargin;
	private Double DSpecialMargin;
	private Double LBSpecialMargin;
	private Double TINSpecialMargin;
	private Double NSpecialMargin;
	private Double PSpecialMargin;
	private Double QSpecialMargin;
	private Double XSpecialMargin;
	private Long otherPending;
	private Long l2NeedCase;
	private Long l1NeedCase;
	private Long qNeedCase;
	private Long pNeedCase;
	private Long nNeedCase;
	private Long dNeedCase;
	private Long lbNeedCase;
	private Long sbNeedCase;
	private Long tinNeedCase;
	private Long xNeedCase;
	private Double L2Sale;
	private Double L1Sale;
	private Double SBSale;
	private Double DSale;
	private Double LBSale;
	private Double TINSale;
	private Double NSale;
	private Double PSale;
	private Double QSale;
	private Double XSale;
	private Long otherInvestment;
	private Long l2Val;
	private Long l1Val;
	private Long qVal;
	private Long pVal;
	private Long nVal;
	private Long dVal;
	private Long lbVal;
	private Long sbVal;
	private Long tinVal;
	private Long xVal;
	private Double totalSpecialMrp;
	private Double L2perday;
	private Double L1perday;
	private Double SBperday;
	private Double Dperday;
	private Double LBperday;
	private Double TINperday;
	private Double Nperday;
	private Double Pperday;
	private Double Qperday;
	private Double Xperday;
	
	
	
	public Long getOtherPending() {
		return otherPending;
	}
	public void setOtherPending(Long otherPending) {
		this.otherPending = otherPending;
	}
	public Long getOtherInvestment() {
		return otherInvestment;
	}
	public void setOtherInvestment(Long otherInvestment) {
		this.otherInvestment = otherInvestment;
	}
	public Long getL2Val() {
		return l2Val;
	}
	public void setL2Val(Long l2Val) {
		this.l2Val = l2Val;
	}
	public Long getL1Val() {
		return l1Val;
	}
	public void setL1Val(Long l1Val) {
		this.l1Val = l1Val;
	}
	public Long getqVal() {
		return qVal;
	}
	public void setqVal(Long qVal) {
		this.qVal = qVal;
	}
	public Long getpVal() {
		return pVal;
	}
	public void setpVal(Long pVal) {
		this.pVal = pVal;
	}
	public Long getnVal() {
		return nVal;
	}
	public void setnVal(Long nVal) {
		this.nVal = nVal;
	}
	public Long getdVal() {
		return dVal;
	}
	public void setdVal(Long dVal) {
		this.dVal = dVal;
	}
	public Long getLbVal() {
		return lbVal;
	}
	public void setLbVal(Long lbVal) {
		this.lbVal = lbVal;
	}
	public Long getSbVal() {
		return sbVal;
	}
	public void setSbVal(Long sbVal) {
		this.sbVal = sbVal;
	}
	public Long getTinVal() {
		return tinVal;
	}
	public void setTinVal(Long tinVal) {
		this.tinVal = tinVal;
	}
	public Long getxVal() {
		return xVal;
	}
	public void setxVal(Long xVal) {
		this.xVal = xVal;
	}
	public Double getTotalSpecialMrp() {
		return totalSpecialMrp;
	}
	public void setTotalSpecialMrp(Double totalSpecialMrp) {
		this.totalSpecialMrp = totalSpecialMrp;
	}
	public Double getL2Sale() {
		return L2Sale;
	}
	public void setL2Sale(Double l2Sale) {
		L2Sale = l2Sale;
	}
	public Double getL1Sale() {
		return L1Sale;
	}
	public void setL1Sale(Double l1Sale) {
		L1Sale = l1Sale;
	}
	public Double getSBSale() {
		return SBSale;
	}
	public void setSBSale(Double sBSale) {
		SBSale = sBSale;
	}
	public Double getDSale() {
		return DSale;
	}
	public void setDSale(Double dSale) {
		DSale = dSale;
	}
	public Double getLBSale() {
		return LBSale;
	}
	public void setLBSale(Double lBSale) {
		LBSale = lBSale;
	}
	public Double getTINSale() {
		return TINSale;
	}
	public void setTINSale(Double tINSale) {
		TINSale = tINSale;
	}
	public Double getNSale() {
		return NSale;
	}
	public void setNSale(Double nSale) {
		NSale = nSale;
	}
	public Double getPSale() {
		return PSale;
	}
	public void setPSale(Double pSale) {
		PSale = pSale;
	}
	public Double getQSale() {
		return QSale;
	}
	public void setQSale(Double qSale) {
		QSale = qSale;
	}
	public Double getXSale() {
		return XSale;
	}
	public void setXSale(Double xSale) {
		XSale = xSale;
	}
	
	public Double getNeedCase() {
		return needCase;
	}
	public void setNeedCase(Double needCase) {
		this.needCase = needCase;
	}
	public Long getCommitment() {
		return commitment;
	}
	public void setCommitment(Long commitment) {
		this.commitment = commitment;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public Double getCaseRate() {
		return caseRate;
	}
	public void setCaseRate(Double caseRate) {
		this.caseRate = caseRate;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public Long getNoOfCases() {
		return noOfCases;
	}
	public void setNoOfCases(Long noOfCases) {
		this.noOfCases = noOfCases;
	}
	public Double getCashBackOnPerPrice() {
		return cashBackOnPerPrice;
	}
	public void setCashBackOnPerPrice(Double cashBackOnPerPrice) {
		this.cashBackOnPerPrice = cashBackOnPerPrice;
	}
	public Double getPriceToPurches() {
		return priceToPurches;
	}
	public void setPriceToPurches(Double priceToPurches) {
		this.priceToPurches = priceToPurches;
	}
	public Double getCashBackOnOrder() {
		return cashBackOnOrder;
	}
	public void setCashBackOnOrder(Double cashBackOnOrder) {
		this.cashBackOnOrder = cashBackOnOrder;
	}
	public String getProfitPercentageDiscount() {
		return profitPercentageDiscount;
	}
	public void setProfitPercentageDiscount(String profitPercentageDiscount) {
		this.profitPercentageDiscount = profitPercentageDiscount;
	}
	public Double getL2() {
		return L2;
	}
	public void setL2(Double l2) {
		L2 = l2;
	}
	public Double getL1() {
		return L1;
	}
	public void setL1(Double l1) {
		L1 = l1;
	}
	public Double getSB() {
		return SB;
	}
	public void setSB(Double sB) {
		SB = sB;
	}
	public Double getD() {
		return D;
	}
	public void setD(Double d) {
		D = d;
	}
	public Double getLB() {
		return LB;
	}
	public void setLB(Double lB) {
		LB = lB;
	}
	public Double getTIN() {
		return TIN;
	}
	public void setTIN(Double tIN) {
		TIN = tIN;
	}
	public Double getN() {
		return N;
	}
	public void setN(Double n) {
		N = n;
	}
	public Double getP() {
		return P;
	}
	public void setP(Double p) {
		P = p;
	}
	public Double getQ() {
		return Q;
	}
	public void setQ(Double q) {
		Q = q;
	}
	public Double getX() {
		return X;
	}
	public void setX(Double x) {
		X = x;
	}
	public Long getEstimateDays() {
		return estimateDays;
	}
	public void setEstimateDays(Long estimateDays) {
		this.estimateDays = estimateDays;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Long getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(Long orderBy) {
		this.orderBy = orderBy;
	}
	public Long getTarget() {
		return target;
	}
	public void setTarget(Long target) {
		this.target = target;
	}
	public Double getLastMonthSold() {
		return lastMonthSold;
	}
	public void setLastMonthSold(Double lastMonthSold) {
		this.lastMonthSold = lastMonthSold;
	}
	public Long getCompanyOrder() {
		return companyOrder;
	}
	public void setCompanyOrder(Long companyOrder) {
		this.companyOrder = companyOrder;
	}
	public String getCompanyColor() {
		return companyColor;
	}
	public void setCompanyColor(String companyColor) {
		this.companyColor = companyColor;
	}
	public Double getInHouseStock() {
		return inHouseStock;
	}
	public void setInHouseStock(Double inHouseStock) {
		this.inHouseStock = inHouseStock;
	}
	public Double getL2Stock() {
		return L2Stock;
	}
	public void setL2Stock(Double l2Stock) {
		L2Stock = l2Stock;
	}
	public Double getL1Stock() {
		return L1Stock;
	}
	public void setL1Stock(Double l1Stock) {
		L1Stock = l1Stock;
	}
	public Double getSBStock() {
		return SBStock;
	}
	public void setSBStock(Double sBStock) {
		SBStock = sBStock;
	}
	public Double getDStock() {
		return DStock;
	}
	public void setDStock(Double dStock) {
		DStock = dStock;
	}
	public Double getLBStock() {
		return LBStock;
	}
	public void setLBStock(Double lBStock) {
		LBStock = lBStock;
	}
	public Double getTINStock() {
		return TINStock;
	}
	public void setTINStock(Double tINStock) {
		TINStock = tINStock;
	}
	public Double getNStock() {
		return NStock;
	}
	public void setNStock(Double nStock) {
		NStock = nStock;
	}
	public Double getPStock() {
		return PStock;
	}
	public void setPStock(Double pStock) {
		PStock = pStock;
	}
	public Double getQStock() {
		return QStock;
	}
	public void setQStock(Double qStock) {
		QStock = qStock;
	}
	public Double getXStock() {
		return XStock;
	}
	public void setXStock(Double xStock) {
		XStock = xStock;
	}
	public Double getPending() {
		return pending;
	}
	public void setPending(Double pending) {
		this.pending = pending;
	}
	public Double getLiftedCase() {
		return LiftedCase;
	}
	public void setLiftedCase(Double liftedCase) {
		LiftedCase = liftedCase;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public Double getL2SpecialMargin() {
		return L2SpecialMargin;
	}
	public void setL2SpecialMargin(Double l2SpecialMargin) {
		L2SpecialMargin = l2SpecialMargin;
	}
	public Double getL1SpecialMargin() {
		return L1SpecialMargin;
	}
	public void setL1SpecialMargin(Double l1SpecialMargin) {
		L1SpecialMargin = l1SpecialMargin;
	}
	public Double getSBSpecialMargin() {
		return SBSpecialMargin;
	}
	public void setSBSpecialMargin(Double sBSpecialMargin) {
		SBSpecialMargin = sBSpecialMargin;
	}
	public Double getDSpecialMargin() {
		return DSpecialMargin;
	}
	public void setDSpecialMargin(Double dSpecialMargin) {
		DSpecialMargin = dSpecialMargin;
	}
	public Double getLBSpecialMargin() {
		return LBSpecialMargin;
	}
	public void setLBSpecialMargin(Double lBSpecialMargin) {
		LBSpecialMargin = lBSpecialMargin;
	}
	public Double getTINSpecialMargin() {
		return TINSpecialMargin;
	}
	public void setTINSpecialMargin(Double tINSpecialMargin) {
		TINSpecialMargin = tINSpecialMargin;
	}
	public Double getNSpecialMargin() {
		return NSpecialMargin;
	}
	public void setNSpecialMargin(Double nSpecialMargin) {
		NSpecialMargin = nSpecialMargin;
	}
	public Double getPSpecialMargin() {
		return PSpecialMargin;
	}
	public void setPSpecialMargin(Double pSpecialMargin) {
		PSpecialMargin = pSpecialMargin;
	}
	public Double getQSpecialMargin() {
		return QSpecialMargin;
	}
	public void setQSpecialMargin(Double qSpecialMargin) {
		QSpecialMargin = qSpecialMargin;
	}
	public Double getXSpecialMargin() {
		return XSpecialMargin;
	}
	public void setXSpecialMargin(Double xSpecialMargin) {
		XSpecialMargin = xSpecialMargin;
	}
	public Long getL2NeedCase() {
		return l2NeedCase;
	}
	public void setL2NeedCase(Long l2NeedCase) {
		this.l2NeedCase = l2NeedCase;
	}
	public Long getL1NeedCase() {
		return l1NeedCase;
	}
	public void setL1NeedCase(Long l1NeedCase) {
		this.l1NeedCase = l1NeedCase;
	}
	public Long getqNeedCase() {
		return qNeedCase;
	}
	public void setqNeedCase(Long qNeedCase) {
		this.qNeedCase = qNeedCase;
	}
	public Long getpNeedCase() {
		return pNeedCase;
	}
	public void setpNeedCase(Long pNeedCase) {
		this.pNeedCase = pNeedCase;
	}
	public Long getnNeedCase() {
		return nNeedCase;
	}
	public void setnNeedCase(Long nNeedCase) {
		this.nNeedCase = nNeedCase;
	}
	public Long getdNeedCase() {
		return dNeedCase;
	}
	public void setdNeedCase(Long dNeedCase) {
		this.dNeedCase = dNeedCase;
	}
	public Long getLbNeedCase() {
		return lbNeedCase;
	}
	public void setLbNeedCase(Long lbNeedCase) {
		this.lbNeedCase = lbNeedCase;
	}
	public Long getSbNeedCase() {
		return sbNeedCase;
	}
	public void setSbNeedCase(Long sbNeedCase) {
		this.sbNeedCase = sbNeedCase;
	}
	public Long getTinNeedCase() {
		return tinNeedCase;
	}
	public void setTinNeedCase(Long tinNeedCase) {
		this.tinNeedCase = tinNeedCase;
	}
	public Long getxNeedCase() {
		return xNeedCase;
	}
	public void setxNeedCase(Long xNeedCase) {
		this.xNeedCase = xNeedCase;
	}
	public Double getL2perday() {
		return L2perday;
	}
	public void setL2perday(Double l2perday) {
		L2perday = l2perday;
	}
	public Double getL1perday() {
		return L1perday;
	}
	public void setL1perday(Double l1perday) {
		L1perday = l1perday;
	}
	public Double getSBperday() {
		return SBperday;
	}
	public void setSBperday(Double sBperday) {
		SBperday = sBperday;
	}
	public Double getDperday() {
		return Dperday;
	}
	public void setDperday(Double dperday) {
		Dperday = dperday;
	}
	public Double getLBperday() {
		return LBperday;
	}
	public void setLBperday(Double lBperday) {
		LBperday = lBperday;
	}
	public Double getTINperday() {
		return TINperday;
	}
	public void setTINperday(Double tINperday) {
		TINperday = tINperday;
	}
	public Double getNperday() {
		return Nperday;
	}
	public void setNperday(Double nperday) {
		Nperday = nperday;
	}
	public Double getPperday() {
		return Pperday;
	}
	public void setPperday(Double pperday) {
		Pperday = pperday;
	}
	public Double getQperday() {
		return Qperday;
	}
	public void setQperday(Double qperday) {
		Qperday = qperday;
	}
	public Double getXperday() {
		return Xperday;
	}
	public void setXperday(Double xperday) {
		Xperday = xperday;
	}
	@Override
	public String toString() {
		return "DiscountEstimationBean [brandNo=" + brandNo + ", brandName=" + brandName + ", caseRate=" + caseRate
				+ ", category=" + category + ", company=" + company + ", noOfCases=" + noOfCases
				+ ", cashBackOnPerPrice=" + cashBackOnPerPrice + ", priceToPurches=" + priceToPurches
				+ ", cashBackOnOrder=" + cashBackOnOrder + ", profitPercentageDiscount=" + profitPercentageDiscount
				+ ", L2=" + L2 + ", L1=" + L1 + ", SB=" + SB + ", D=" + D + ", LB=" + LB + ", TIN=" + TIN + ", N=" + N
				+ ", P=" + P + ", Q=" + Q + ", X=" + X + ", estimateDays=" + estimateDays + ", color=" + color
				+ ", orderBy=" + orderBy + ", target=" + target + ", lastMonthSold=" + lastMonthSold + ", companyOrder="
				+ companyOrder + ", companyColor=" + companyColor + ", inHouseStock=" + inHouseStock + ", L2Stock="
				+ L2Stock + ", L1Stock=" + L1Stock + ", SBStock=" + SBStock + ", DStock=" + DStock + ", LBStock="
				+ LBStock + ", TINStock=" + TINStock + ", NStock=" + NStock + ", PStock=" + PStock + ", QStock="
				+ QStock + ", XStock=" + XStock + ", pending=" + pending + ", LiftedCase=" + LiftedCase
				+ ", commitment=" + commitment + ", needCase=" + needCase + ", productType=" + productType
				+ ", L2SpecialMargin=" + L2SpecialMargin + ", L1SpecialMargin=" + L1SpecialMargin + ", SBSpecialMargin="
				+ SBSpecialMargin + ", DSpecialMargin=" + DSpecialMargin + ", LBSpecialMargin=" + LBSpecialMargin
				+ ", TINSpecialMargin=" + TINSpecialMargin + ", NSpecialMargin=" + NSpecialMargin + ", PSpecialMargin="
				+ PSpecialMargin + ", QSpecialMargin=" + QSpecialMargin + ", XSpecialMargin=" + XSpecialMargin
				+ ", otherPending=" + otherPending + ", l2NeedCase=" + l2NeedCase + ", l1NeedCase=" + l1NeedCase
				+ ", qNeedCase=" + qNeedCase + ", pNeedCase=" + pNeedCase + ", nNeedCase=" + nNeedCase + ", dNeedCase="
				+ dNeedCase + ", lbNeedCase=" + lbNeedCase + ", sbNeedCase=" + sbNeedCase + ", tinNeedCase="
				+ tinNeedCase + ", xNeedCase=" + xNeedCase + ", L2Sale=" + L2Sale + ", L1Sale=" + L1Sale + ", SBSale="
				+ SBSale + ", DSale=" + DSale + ", LBSale=" + LBSale + ", TINSale=" + TINSale + ", NSale=" + NSale
				+ ", PSale=" + PSale + ", QSale=" + QSale + ", XSale=" + XSale + ", otherInvestment=" + otherInvestment
				+ ", l2Val=" + l2Val + ", l1Val=" + l1Val + ", qVal=" + qVal + ", pVal=" + pVal + ", nVal=" + nVal
				+ ", dVal=" + dVal + ", lbVal=" + lbVal + ", sbVal=" + sbVal + ", tinVal=" + tinVal + ", xVal=" + xVal
				+ ", totalSpecialMrp=" + totalSpecialMrp + ", L2perday=" + L2perday + ", L1perday=" + L1perday
				+ ", SBperday=" + SBperday + ", Dperday=" + Dperday + ", LBperday=" + LBperday + ", TINperday="
				+ TINperday + ", Nperday=" + Nperday + ", Pperday=" + Pperday + ", Qperday=" + Qperday + ", Xperday="
				+ Xperday + "]";
	}
	
}
