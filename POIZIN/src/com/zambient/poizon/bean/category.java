package com.zambient.poizon.bean;

import java.io.Serializable;

public class category implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String label;
	private String color;
	private String hovertext;
	private Long value;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getHovertext() {
		return hovertext;
	}
	public void setHovertext(String hovertext) {
		this.hovertext = hovertext;
	}
	public Long getValue() {
		return value;
	}
	public void setValue(Long value) {
		this.value = value;
	}
	@Override
	public String toString() {
		return "category [label=" + label + ", color=" + color + ", hovertext=" + hovertext + ", value=" + value + "]";
	}
	

}
