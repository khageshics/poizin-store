package com.zambient.poizon.bean;

import java.io.Serializable;

public class InvestmentBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	 
	private String bankdate;
	private String bank;
	private String transactionType;
	private Double amount;
	public String getBankdate() {
		return bankdate;
	}
	public void setBankdate(String bankdate) {
		this.bankdate = bankdate;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "InvestmentBean [bankdate=" + bankdate + ", bank=" + bank + ", transactionType=" + transactionType
				+ ", amount=" + amount + "]";
	}
	
	
}
