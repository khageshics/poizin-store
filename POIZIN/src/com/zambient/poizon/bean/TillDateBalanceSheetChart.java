package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class TillDateBalanceSheetChart implements Serializable{
	private static final long serialVersionUID = 1L;
	
	List<categoryLineChart> categoryLineChart;
	List<dataset> dataset;
	public List<categoryLineChart> getCategoryLineChart() {
		return categoryLineChart;
	}
	public void setCategoryLineChart(List<categoryLineChart> categoryLineChart) {
		this.categoryLineChart = categoryLineChart;
	}
	public List<dataset> getDataset() {
		return dataset;
	}
	public void setDataset(List<dataset> dataset) {
		this.dataset = dataset;
	}
	
	

}
