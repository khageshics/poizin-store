package com.zambient.poizon.bean;

import java.io.Serializable;

public class ProfitDiscountBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Double brandNoPackQty;
	private Long brandNo;
	private String brandName;
	private String productType;
	private String quantity; 
	private Long packQty;
	private String packType;
	private Long saleId;
	private String salePrimaryKey;
	private Long closing;
	private Double unitPrice;
	private Double totalPrice;
	private Long opening;
	private Double singleBottelPrice;
	private Long totalSale;
	private String saleDate;
	private Long caseQty;
	private String invoiceDate;
	private Long QtyBottels;
	private Long received;
	private String category;
	private String company;
	private String color;
	private Long categoryOrder;
	private Long companyOrder;
	private Double profitPercentage;
	private Double stockLiftProfit;
	
	public Double getBrandNoPackQty() {
		return brandNoPackQty;
	}
	public void setBrandNoPackQty(Double brandNoPackQty) {
		this.brandNoPackQty = brandNoPackQty;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public Long getPackQty() {
		return packQty;
	}
	public void setPackQty(Long packQty) {
		this.packQty = packQty;
	}
	public String getPackType() {
		return packType;
	}
	public void setPackType(String packType) {
		this.packType = packType;
	}
	public Long getSaleId() {
		return saleId;
	}
	public void setSaleId(Long saleId) {
		this.saleId = saleId;
	}
	public String getSalePrimaryKey() {
		return salePrimaryKey;
	}
	public void setSalePrimaryKey(String salePrimaryKey) {
		this.salePrimaryKey = salePrimaryKey;
	}
	public Long getClosing() {
		return closing;
	}
	public void setClosing(Long closing) {
		this.closing = closing;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Long getOpening() {
		return opening;
	}
	public void setOpening(Long opening) {
		this.opening = opening;
	}
	public Double getSingleBottelPrice() {
		return singleBottelPrice;
	}
	public void setSingleBottelPrice(Double singleBottelPrice) {
		this.singleBottelPrice = singleBottelPrice;
	}
	public Long getTotalSale() {
		return totalSale;
	}
	public void setTotalSale(Long totalSale) {
		this.totalSale = totalSale;
	}
	public String getSaleDate() {
		return saleDate;
	}
	public void setSaleDate(String saleDate) {
		this.saleDate = saleDate;
	}
	public Long getCaseQty() {
		return caseQty;
	}
	public void setCaseQty(Long caseQty) {
		this.caseQty = caseQty;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public Long getQtyBottels() {
		return QtyBottels;
	}
	public void setQtyBottels(Long qtyBottels) {
		QtyBottels = qtyBottels;
	}
	public Long getReceived() {
		return received;
	}
	public void setReceived(Long received) {
		this.received = received;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Long getCategoryOrder() {
		return categoryOrder;
	}
	public void setCategoryOrder(Long categoryOrder) {
		this.categoryOrder = categoryOrder;
	}
	public Long getCompanyOrder() {
		return companyOrder;
	}
	public void setCompanyOrder(Long companyOrder) {
		this.companyOrder = companyOrder;
	}
	public Double getProfitPercentage() {
		return profitPercentage;
	}
	public void setProfitPercentage(Double profitPercentage) {
		this.profitPercentage = profitPercentage;
	}
	public Double getStockLiftProfit() {
		return stockLiftProfit;
	}
	public void setStockLiftProfit(Double stockLiftProfit) {
		this.stockLiftProfit = stockLiftProfit;
	}
	

}
