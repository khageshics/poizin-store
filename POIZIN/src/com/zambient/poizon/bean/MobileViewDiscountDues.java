package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class MobileViewDiscountDues implements Serializable{
	
  private String name;
  private Double amount;
  private String color;
  private Long order;
  MobileViewDiscountDuesInner mobileViewDiscountDuesInner;
  
  public Long getOrder() {
	return order;
}
public void setOrder(Long order) {
	this.order = order;
}
public MobileViewDiscountDuesInner getMobileViewDiscountDuesInner() {
	return mobileViewDiscountDuesInner;
}
public void setMobileViewDiscountDuesInner(MobileViewDiscountDuesInner mobileViewDiscountDuesInner) {
	this.mobileViewDiscountDuesInner = mobileViewDiscountDuesInner;
}

public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public Double getAmount() {
	return amount;
}
public void setAmount(Double amount) {
	this.amount = amount;
}
public String getColor() {
	return color;
}
public void setColor(String color) {
	this.color = color;
}


}
