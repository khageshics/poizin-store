package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class UploadAndViewWithYearBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String year;
	private Double totalInvoice;
	private List<UploadAndViewInvoiceBean> uploadAndViewInvoiceBean;
	
	public String getYear() {
		return year;
	}
	public Double getTotalInvoice() {
		return totalInvoice;
	}
	public List<UploadAndViewInvoiceBean> getUploadAndViewInvoiceBean() {
		return uploadAndViewInvoiceBean;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public void setTotalInvoice(Double totalInvoice) {
		this.totalInvoice = totalInvoice;
	}
	public void setUploadAndViewInvoiceBean(List<UploadAndViewInvoiceBean> uploadAndViewInvoiceBean) {
		this.uploadAndViewInvoiceBean = uploadAndViewInvoiceBean;
	}

}
