package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class StockLiftWithDiscountEstimate implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Long brandNo;
	private String brandName;
	private String color;
	private Long companyOrder;
	private String category;
	private String company;
	private Long comapnyId;
	private Long adjustment;
	private Double discount;
	private Double investment;
	private Double rateOfInterestWithInhouse;
	private Double rateOfInterestWithoutInhouse;
	private Double caseRate;
	private Double totalDiscount;
	private Double liftedCase;
	private  Double lastMonthSold;
	private Double inHouseStock;
	private Long targetCase;
	private Double estimateInHouseStock;
	private Double liftedAndAdjCase;
	private Long received;
	private Long pending;
	private String categoryColor;
	List<PieChartBean> caseRateList;
	List<MaxMinDiscountBean> maxMinDiscountBean;
	
	public void setPending(Long pending) {
		this.pending = pending;
	}
	public Long getPending() {
		return pending;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Long getCompanyOrder() {
		return companyOrder;
	}
	public void setCompanyOrder(Long companyOrder) {
		this.companyOrder = companyOrder;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public Long getComapnyId() {
		return comapnyId;
	}
	public void setComapnyId(Long comapnyId) {
		this.comapnyId = comapnyId;
	}
	public Long getAdjustment() {
		return adjustment;
	}
	public void setAdjustment(Long adjustment) {
		this.adjustment = adjustment;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Double getInvestment() {
		return investment;
	}
	public void setInvestment(Double investment) {
		this.investment = investment;
	}
	public Double getRateOfInterestWithInhouse() {
		return rateOfInterestWithInhouse;
	}
	public void setRateOfInterestWithInhouse(Double rateOfInterestWithInhouse) {
		this.rateOfInterestWithInhouse = rateOfInterestWithInhouse;
	}
	public Double getRateOfInterestWithoutInhouse() {
		return rateOfInterestWithoutInhouse;
	}
	public void setRateOfInterestWithoutInhouse(Double rateOfInterestWithoutInhouse) {
		this.rateOfInterestWithoutInhouse = rateOfInterestWithoutInhouse;
	}
	public Double getCaseRate() {
		return caseRate;
	}
	public void setCaseRate(Double caseRate) {
		this.caseRate = caseRate;
	}
	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public Double getLiftedCase() {
		return liftedCase;
	}
	public void setLiftedCase(Double liftedCase) {
		this.liftedCase = liftedCase;
	}
	public Double getLastMonthSold() {
		return lastMonthSold;
	}
	public void setLastMonthSold(Double lastMonthSold) {
		this.lastMonthSold = lastMonthSold;
	}
	public Double getInHouseStock() {
		return inHouseStock;
	}
	public void setInHouseStock(Double inHouseStock) {
		this.inHouseStock = inHouseStock;
	}
	public Long getTargetCase() {
		return targetCase;
	}
	public void setTargetCase(Long targetCase) {
		this.targetCase = targetCase;
	}
	public Double getEstimateInHouseStock() {
		return estimateInHouseStock;
	}
	public void setEstimateInHouseStock(Double estimateInHouseStock) {
		this.estimateInHouseStock = estimateInHouseStock;
	}
	public Double getLiftedAndAdjCase() {
		return liftedAndAdjCase;
	}
	public void setLiftedAndAdjCase(Double liftedAndAdjCase) {
		this.liftedAndAdjCase = liftedAndAdjCase;
	}
	public Long getReceived() {
		return received;
	}
	public void setReceived(Long received) {
		this.received = received;
	}
	public String getCategoryColor() {
		return categoryColor;
	}
	public void setCategoryColor(String categoryColor) {
		this.categoryColor = categoryColor;
	}
	public List<PieChartBean> getCaseRateList() {
		return caseRateList;
	}
	public void setCaseRateList(List<PieChartBean> caseRateList) {
		this.caseRateList = caseRateList;
	}
	public List<MaxMinDiscountBean> getMaxMinDiscountBean() {
		return maxMinDiscountBean;
	}
	public void setMaxMinDiscountBean(List<MaxMinDiscountBean> maxMinDiscountBean) {
		this.maxMinDiscountBean = maxMinDiscountBean;
	}
	
}
