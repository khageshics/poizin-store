package com.zambient.poizon.bean;

import java.io.Serializable;

public class IndentEstimatePDF implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String name;
	private String indentCase;
	private String l2Case;
	private String l1Case;
	private String qCase;
	private String pCase;
	private String nCase;
	private String dCase;
	private String lbCase;
	private String sbCase;
	private String tinCase;
	private Long brandNo;
	private Double indentAmt;
	private Double specialMrp;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getL2Case() {
		return l2Case;
	}
	public void setL2Case(String l2Case) {
		this.l2Case = l2Case;
	}
	public String getL1Case() {
		return l1Case;
	}
	public void setL1Case(String l1Case) {
		this.l1Case = l1Case;
	}
	public String getqCase() {
		return qCase;
	}
	public void setqCase(String qCase) {
		this.qCase = qCase;
	}
	public String getpCase() {
		return pCase;
	}
	public void setpCase(String pCase) {
		this.pCase = pCase;
	}
	public String getnCase() {
		return nCase;
	}
	public void setnCase(String nCase) {
		this.nCase = nCase;
	}
	public String getdCase() {
		return dCase;
	}
	public void setdCase(String dCase) {
		this.dCase = dCase;
	}
	public String getLbCase() {
		return lbCase;
	}
	public void setLbCase(String lbCase) {
		this.lbCase = lbCase;
	}
	public String getSbCase() {
		return sbCase;
	}
	public void setSbCase(String sbCase) {
		this.sbCase = sbCase;
	}
	public String getTinCase() {
		return tinCase;
	}
	public void setTinCase(String tinCase) {
		this.tinCase = tinCase;
	}
	public String getIndentCase() {
		return indentCase;
	}
	public void setIndentCase(String indentCase) {
		this.indentCase = indentCase;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public Double getIndentAmt() {
		return indentAmt;
	}
	public void setIndentAmt(Double indentAmt) {
		this.indentAmt = indentAmt;
	}
	public Double getSpecialMrp() {
		return specialMrp;
	}
	public void setSpecialMrp(Double specialMrp) {
		this.specialMrp = specialMrp;
	}
	@Override
	public String toString() {
		return "IndentEstimatePDF [name=" + name + ", indentCase=" + indentCase + ", l2Case=" + l2Case + ", l1Case="
				+ l1Case + ", qCase=" + qCase + ", pCase=" + pCase + ", nCase=" + nCase + ", dCase=" + dCase
				+ ", lbCase=" + lbCase + ", sbCase=" + sbCase + ", tinCase=" + tinCase + ", brandNo=" + brandNo
				+ ", indentAmt=" + indentAmt + ", specialMrp=" + specialMrp + "]";
	}
	
	
}
