package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class TillMonthStockLiftWithDiscount implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String date;
	private Double discounts;
	private Double rentals;
	private Double total;
	private Double received;
	private Double dues;
	private Double id;
	private String color;
	private Double adjustmentCheque;
	DiscountAsPerCompanyBeen discountAsPerCompanyBeen;
	List<SaleBean> saleBean;
	List<DiscountTransactionBean> discountAsPerCompanyBeenList;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Double getDiscounts() {
		return discounts;
	}
	public void setDiscounts(Double discounts) {
		this.discounts = discounts;
	}
	public Double getRentals() {
		return rentals;
	}
	public void setRentals(Double rentals) {
		this.rentals = rentals;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Double getReceived() {
		return received;
	}
	public void setReceived(Double received) {
		this.received = received;
	}
	public Double getDues() {
		return dues;
	}
	public void setDues(Double dues) {
		this.dues = dues;
	}
	public Double getId() {
		return id;
	}
	public void setId(Double id) {
		this.id = id;
	}
	public DiscountAsPerCompanyBeen getDiscountAsPerCompanyBeen() {
		return discountAsPerCompanyBeen;
	}
	public void setDiscountAsPerCompanyBeen(DiscountAsPerCompanyBeen discountAsPerCompanyBeen) {
		this.discountAsPerCompanyBeen = discountAsPerCompanyBeen;
	}
	public List<SaleBean> getSaleBean() {
		return saleBean;
	}
	public void setSaleBean(List<SaleBean> saleBean) {
		this.saleBean = saleBean;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Double getAdjustmentCheque() {
		return adjustmentCheque;
	}
	public void setAdjustmentCheque(Double adjustmentCheque) {
		this.adjustmentCheque = adjustmentCheque;
	}
	public List<DiscountTransactionBean> getDiscountAsPerCompanyBeenList() {
		return discountAsPerCompanyBeenList;
	}
	public void setDiscountAsPerCompanyBeenList(List<DiscountTransactionBean> discountAsPerCompanyBeenList) {
		this.discountAsPerCompanyBeenList = discountAsPerCompanyBeenList;
	}
	
	
	
}
