package com.zambient.poizon.bean;

import java.io.Serializable;

public class ProductOrderBy implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long brandNo;
	private Long orderValue;
	private Long packQty;
	
	public Long getPackQty() {
		return packQty;
	}
	public void setPackQty(Long packQty) {
		this.packQty = packQty;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public Long getOrderValue() {
		return orderValue;
	}
	public void setOrderValue(Long orderValue) {
		this.orderValue = orderValue;
	}
	@Override
	public String toString() {
		return "ProductOrderBy [brandNo=" + brandNo + ", orderValue=" + orderValue + ", packQty=" + packQty + "]";
	}
	
	

}
