package com.zambient.poizon.bean;

public class DiscountWithDate {
	
	private String date;
	private Double value;
	private Double govtProfit;
	private Double costPrice;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public Double getGovtProfit() {
		return govtProfit;
	}
	public void setGovtProfit(Double govtProfit) {
		this.govtProfit = govtProfit;
	}
	public Double getCostPrice() {
		return costPrice;
	}
	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}

}
