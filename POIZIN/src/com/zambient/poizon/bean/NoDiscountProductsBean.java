package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class NoDiscountProductsBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String companyName;
	private List<PendingLiftedCaseDiscountBean> listData;
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public List<PendingLiftedCaseDiscountBean> getListData() {
		return listData;
	}
	public void setListData(List<PendingLiftedCaseDiscountBean> listData) {
		this.listData = listData;
	}
	@Override
	public String toString() {
		return "NoDiscountProductsBean [companyName=" + companyName + ", listData=" + listData + "]";
	}
	
}
