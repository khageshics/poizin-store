package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class ExpenseCategoryAndDate implements Serializable{
	private static final long serialVersionUID = 1L;
	
	List<ExpenseCategoryBean> expenseCategoryBean;
	private String edate;
	private Double total;
	public List<ExpenseCategoryBean> getExpenseCategoryBean() {
		return expenseCategoryBean;
	}
	public void setExpenseCategoryBean(List<ExpenseCategoryBean> expenseCategoryBean) {
		this.expenseCategoryBean = expenseCategoryBean;
	}
	public String getEdate() {
		return edate;
	}
	public void setEdate(String edate) {
		this.edate = edate;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	
	
}
