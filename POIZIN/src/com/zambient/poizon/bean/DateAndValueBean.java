package com.zambient.poizon.bean;

public class DateAndValueBean {
	private static final long serialVersionUID = 1L;
	
	private String date;
	private Double value;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
}
