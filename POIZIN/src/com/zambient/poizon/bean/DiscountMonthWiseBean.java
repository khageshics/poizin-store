package com.zambient.poizon.bean;

import java.io.Serializable;

public class DiscountMonthWiseBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String brand;
	private Long Case;
	private Double discount;
	private Double total;
	private String date;
	private Long match;
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public Long getCase() {
		return Case;
	}
	public void setCase(Long case1) {
		Case = case1;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Long getMatch() {
		return match;
	}
	public void setMatch(Long match) {
		this.match = match;
	}
	@Override
	public String toString() {
		return "DiscountMonthWiseBean [brand=" + brand + ", Case=" + Case + ", discount=" + discount + ", total="
				+ total + ", date=" + date + "]";
	}

}
