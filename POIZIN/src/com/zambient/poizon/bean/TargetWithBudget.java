package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class TargetWithBudget implements Serializable{
	private static final long serialVersionUID = 1L;
	private String month;
	private String company;
	private Double budget;
	private List<DownloadTargetCase> targetCaseList;
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public Double getBudget() {
		return budget;
	}
	public void setBudget(Double budget) {
		this.budget = budget;
	}
	public List<DownloadTargetCase> getTargetCaseList() {
		return targetCaseList;
	}
	public void setTargetCaseList(List<DownloadTargetCase> targetCaseList) {
		this.targetCaseList = targetCaseList;
	}
	
	
}
