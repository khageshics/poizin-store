package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class DiscountAndMonthBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String month;
	private Double totalDiscount;
	private Double rentals;
	List<DiscountMonthWiseBean> discountMonthWiseBean;
	
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public List<DiscountMonthWiseBean> getDiscountMonthWiseBean() {
		return discountMonthWiseBean;
	}
	public void setDiscountMonthWiseBean(List<DiscountMonthWiseBean> discountMonthWiseBean) {
		this.discountMonthWiseBean = discountMonthWiseBean;
	}
	public Double getRentals() {
		return rentals;
	}
	public void setRentals(Double rentals) {
		this.rentals = rentals;
	}
	@Override
	public String toString() {
		return "DiscountAndMonthBean [month=" + month + ", totalDiscount=" + totalDiscount + ", rentals=" + rentals
				+ ", discountMonthWiseBean=" + discountMonthWiseBean + "]";
	}

}
