package com.zambient.poizon.bean;

import java.util.ArrayList;
import java.util.List;

public class BalanceSheetWithBreckage {
	private data data;
	List<BalanceSheetBean> balanceSheetBean;
	
	public data getData() {
		return data;
	}
	public List<BalanceSheetBean> getBalanceSheetBean() {
		return balanceSheetBean;
	}
	public void setData(data data) {
		this.data = data;
	}
	public void setBalanceSheetBean(List<BalanceSheetBean> balanceSheetBean) {
		this.balanceSheetBean = balanceSheetBean;
	}
	
	

}
