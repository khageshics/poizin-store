package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class SaleBeanAndCashCardExpensesBean implements Serializable {
	private static final long serialVersionUID = 1L;
	List<SaleBean> saleBean;
	private Double expenseAmount;
	private Double cardAmount;
	private Double cashAmount;
	public List<SaleBean> getSaleBean() {
		return saleBean;
	}
	public void setSaleBean(List<SaleBean> saleBean) {
		this.saleBean = saleBean;
	}
	public Double getExpenseAmount() {
		return expenseAmount;
	}
	public void setExpenseAmount(Double expenseAmount) {
		this.expenseAmount = expenseAmount;
	}
	public Double getCardAmount() {
		return cardAmount;
	}
	public void setCardAmount(Double cardAmount) {
		this.cardAmount = cardAmount;
	}
	public Double getCashAmount() {
		return cashAmount;
	}
	public void setCashAmount(Double cashAmount) {
		this.cashAmount = cashAmount;
	}
	@Override
	public String toString() {
		return "SaleBeanAndCashCardExpensesBean [saleBean=" + saleBean + ", expenseAmount=" + expenseAmount
				+ ", cardAmount=" + cardAmount + ", cashAmount=" + cashAmount + "]";
	}

}
