package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class DropDownsBean  implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<String> productType;
	private List<String> quantity;
	private List<String> caseType;
	private List<Long> packQty;
	private List<String> category;
	private List<String> company;
	public List<String> getProductType() {
		return productType;
	}
	public List<String> getQuantity() {
		return quantity;
	}
	public List<String> getCaseType() {
		return caseType;
	}
	public List<Long> getPackQty() {
		return packQty;
	}
	public List<String> getCategory() {
		return category;
	}
	public List<String> getCompany() {
		return company;
	}
	public void setProductType(List<String> productType) {
		this.productType = productType;
	}
	public void setQuantity(List<String> quantity) {
		this.quantity = quantity;
	}
	public void setCaseType(List<String> caseType) {
		this.caseType = caseType;
	}
	public void setPackQty(List<Long> packQty) {
		this.packQty = packQty;
	}
	public void setCategory(List<String> category) {
		this.category = category;
	}
	public void setCompany(List<String> company) {
		this.company = company;
	}
	
}

