package com.zambient.poizon.bean;

public class PieChartBean {
	private String label;
	private Double value;
	public String getLabel() {
		return label;
	}
	public Double getValue() {
		return value;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	

}
