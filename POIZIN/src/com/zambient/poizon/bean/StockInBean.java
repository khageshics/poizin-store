package com.zambient.poizon.bean;

import java.io.Serializable;

public class StockInBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Double stockInId;
	private Long inventoryId;
	private Double quantity;
	private Double qtyBtl;
	private Double totalPrice;
	private String date;
	private Double casePrice;
	public Double getStockInId() {
		return stockInId;
	}
	public void setStockInId(Double stockInId) {
		this.stockInId = stockInId;
	}
	public Long getInventoryId() {
		return inventoryId;
	}
	public void setInventoryId(Long inventoryId) {
		this.inventoryId = inventoryId;
	}
	public Double getQuantity() {
		return quantity;
	}
	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}
	public Double getQtyBtl() {
		return qtyBtl;
	}
	public void setQtyBtl(Double qtyBtl) {
		this.qtyBtl = qtyBtl;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Double getCasePrice() {
		return casePrice;
	}
	public void setCasePrice(Double casePrice) {
		this.casePrice = casePrice;
	}
	@Override
	public String toString() {
		return "StockInBean [stockInId=" + stockInId + ", inventoryId=" + inventoryId + ", quantity=" + quantity
				+ ", qtyBtl=" + qtyBtl + ", totalPrice=" + totalPrice + ", date=" + date + "]";
	}
	
}
