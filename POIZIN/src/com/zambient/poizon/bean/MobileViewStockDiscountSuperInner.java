package com.zambient.poizon.bean;

import java.io.Serializable;

public class MobileViewStockDiscountSuperInner implements Serializable{

	private String brandName;
	private Long brandNo;
	private Long caseQty;
	private Double discountAmount;
	private Long pending;
	private Double totalDiscountAmt;
	private Double closingCase;
	private Long stockInCase;
	private Long stockInBtl;
	private Long noOfDays;
	private Double casePrice;
	private Double arears;
	private Long dateCompair;
	private Long adjustment;
	
	public Double getCasePrice() {
		return casePrice;
	}
	public void setCasePrice(Double casePrice) {
		this.casePrice = casePrice;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public Long getCaseQty() {
		return caseQty;
	}
	public void setCaseQty(Long caseQty) {
		this.caseQty = caseQty;
	}
	public Double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public Long getPending() {
		return pending;
	}
	public void setPending(Long pending) {
		this.pending = pending;
	}
	public Double getTotalDiscountAmt() {
		return totalDiscountAmt;
	}
	public void setTotalDiscountAmt(Double totalDiscountAmt) {
		this.totalDiscountAmt = totalDiscountAmt;
	}
	public Double getClosingCase() {
		return closingCase;
	}
	public Long getStockInCase() {
		return stockInCase;
	}
	public Long getStockInBtl() {
		return stockInBtl;
	}
	public void setClosingCase(Double closingCase) {
		this.closingCase = closingCase;
	}
	public void setStockInCase(Long stockInCase) {
		this.stockInCase = stockInCase;
	}
	public void setStockInBtl(Long stockInBtl) {
		this.stockInBtl = stockInBtl;
	}
	public Long getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(Long noOfDays) {
		this.noOfDays = noOfDays;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public Double getArears() {
		return arears;
	}
	public void setArears(Double arears) {
		this.arears = arears;
	}
	public Long getDateCompair() {
		return dateCompair;
	}
	public void setDateCompair(Long dateCompair) {
		this.dateCompair = dateCompair;
	}
	public Long getAdjustment() {
		return adjustment;
	}
	public void setAdjustment(Long adjustment) {
		this.adjustment = adjustment;
	}
	
}
