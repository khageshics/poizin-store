package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class dataset implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String seriesname;
	List<data> data;
	public String getSeriesname() {
		return seriesname;
	}
	public void setSeriesname(String seriesname) {
		this.seriesname = seriesname;
	}
	public List<data> getData() {
		return data;
	}
	public void setData(List<data> data) {
		this.data = data;
	}
	

}
