package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class NewSaleBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private Long brandNo;
	private String BrandName;
	private Long twoThousandML;
	private Long oneThousandML;
	private Long sevenFiftyML;
	private Long sevenhundredML;
	private Long sixFiftyML;
	private Long fiveHundredML;
	private Long threeSeventyFiveML;
	private Long threeThirtyFiveML;
	private Long threeThirtyML;
	private Long oneEightyML;
	private Long nightyML;
	private Long sixtyML;
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public String getBrandName() {
		return BrandName;
	}
	public void setBrandName(String brandName) {
		BrandName = brandName;
	}
	public Long getTwoThousandML() {
		return twoThousandML;
	}
	public void setTwoThousandML(Long twoThousandML) {
		this.twoThousandML = twoThousandML;
	}
	public Long getOneThousandML() {
		return oneThousandML;
	}
	public void setOneThousandML(Long oneThousandML) {
		this.oneThousandML = oneThousandML;
	}
	public Long getSevenFiftyML() {
		return sevenFiftyML;
	}
	public void setSevenFiftyML(Long sevenFiftyML) {
		this.sevenFiftyML = sevenFiftyML;
	}
	public Long getSevenhundredML() {
		return sevenhundredML;
	}
	public void setSevenhundredML(Long sevenhundredML) {
		this.sevenhundredML = sevenhundredML;
	}
	public Long getSixFiftyML() {
		return sixFiftyML;
	}
	public void setSixFiftyML(Long sixFiftyML) {
		this.sixFiftyML = sixFiftyML;
	}
	public Long getFiveHundredML() {
		return fiveHundredML;
	}
	public void setFiveHundredML(Long fiveHundredML) {
		this.fiveHundredML = fiveHundredML;
	}
	public Long getThreeSeventyFiveML() {
		return threeSeventyFiveML;
	}
	public void setThreeSeventyFiveML(Long threeSeventyFiveML) {
		this.threeSeventyFiveML = threeSeventyFiveML;
	}
	public Long getThreeThirtyFiveML() {
		return threeThirtyFiveML;
	}
	public void setThreeThirtyFiveML(Long threeThirtyFiveML) {
		this.threeThirtyFiveML = threeThirtyFiveML;
	}
	public Long getThreeThirtyML() {
		return threeThirtyML;
	}
	public void setThreeThirtyML(Long threeThirtyML) {
		this.threeThirtyML = threeThirtyML;
	}
	public Long getOneEightyML() {
		return oneEightyML;
	}
	public void setOneEightyML(Long oneEightyML) {
		this.oneEightyML = oneEightyML;
	}
	public Long getNightyML() {
		return nightyML;
	}
	public void setNightyML(Long nightyML) {
		this.nightyML = nightyML;
	}
	public Long getSixtyML() {
		return sixtyML;
	}
	public void setSixtyML(Long sixtyML) {
		this.sixtyML = sixtyML;
	}
		
	
	
}
