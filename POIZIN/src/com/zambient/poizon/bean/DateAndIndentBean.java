package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class DateAndIndentBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String date;
	private List<IndentEstimatePDF> indentEstimatePDF;
	private List<IndentEstimatePDF> indentEstimateBeerPDF;
	private Long indentAmt;
	private Long tcs;
	private Long beerCases;
	private Long liquorCases;
	private Long totalCase;
	private Long specialMrp;
	private Long totalInvestment;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public List<IndentEstimatePDF> getIndentEstimatePDF() {
		return indentEstimatePDF;
	}
	public void setIndentEstimatePDF(List<IndentEstimatePDF> indentEstimatePDF) {
		this.indentEstimatePDF = indentEstimatePDF;
	}
	public List<IndentEstimatePDF> getIndentEstimateBeerPDF() {
		return indentEstimateBeerPDF;
	}
	public void setIndentEstimateBeerPDF(List<IndentEstimatePDF> indentEstimateBeerPDF) {
		this.indentEstimateBeerPDF = indentEstimateBeerPDF;
	}
	public Long getIndentAmt() {
		return indentAmt;
	}
	public void setIndentAmt(Long indentAmt) {
		this.indentAmt = indentAmt;
	}
	public Long getTcs() {
		return tcs;
	}
	public void setTcs(Long tcs) {
		this.tcs = tcs;
	}
	public Long getBeerCases() {
		return beerCases;
	}
	public void setBeerCases(Long beerCases) {
		this.beerCases = beerCases;
	}
	public Long getLiquorCases() {
		return liquorCases;
	}
	public void setLiquorCases(Long liquorCases) {
		this.liquorCases = liquorCases;
	}
	public Long getTotalCase() {
		return totalCase;
	}
	public void setTotalCase(Long totalCase) {
		this.totalCase = totalCase;
	}
	public Long getSpecialMrp() {
		return specialMrp;
	}
	public void setSpecialMrp(Long specialMrp) {
		this.specialMrp = specialMrp;
	}
	public Long getTotalInvestment() {
		return totalInvestment;
	}
	public void setTotalInvestment(Long totalInvestment) {
		this.totalInvestment = totalInvestment;
	}
	

}
