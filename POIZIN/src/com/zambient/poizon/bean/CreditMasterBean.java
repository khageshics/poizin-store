package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class CreditMasterBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String name;
	private String companyName;
	private Long companyId;
	private String date;
	private Double totalAmount;
	private boolean paid;
	private String paidVal;
	private Long creditId;
	List<CreditChildBean> creditChildBeanList;
	private Double received;
	private Long cases;
	private String receivedDate;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public boolean isPaid() {
		return paid;
	}
	public void setPaid(boolean paid) {
		this.paid = paid;
	}
	public List<CreditChildBean> getCreditChildBeanList() {
		return creditChildBeanList;
	}
	public void setCreditChildBeanList(List<CreditChildBean> creditChildBeanList) {
		this.creditChildBeanList = creditChildBeanList;
	}
	public String getPaidVal() {
		return paidVal;
	}
	public void setPaidVal(String paidVal) {
		this.paidVal = paidVal;
	}
	public Long getCreditId() {
		return creditId;
	}
	public void setCreditId(Long creditId) {
		this.creditId = creditId;
	}
	public Double getReceived() {
		return received;
	}
	public void setReceived(Double received) {
		this.received = received;
	}
	public Long getCases() {
		return cases;
	}
	public void setCases(Long cases) {
		this.cases = cases;
	}
	public String getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(String receivedDate) {
		this.receivedDate = receivedDate;
	}

	
	
}
