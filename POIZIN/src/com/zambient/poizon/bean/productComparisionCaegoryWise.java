package com.zambient.poizon.bean;

import java.util.ArrayList;
import java.util.List;

public class productComparisionCaegoryWise {
	
	List<PieChartBean> PieChartBean;
	List<PieChartColor> PieChartColor;
	ArrayList<String> arrlist ;
	public List<PieChartBean> getPieChartBean() {
		return PieChartBean;
	}
	public List<PieChartColor> getPieChartColor() {
		return PieChartColor;
	}
	public void setPieChartBean(List<PieChartBean> pieChartBean) {
		PieChartBean = pieChartBean;
	}
	public void setPieChartColor(List<PieChartColor> pieChartColor) {
		PieChartColor = pieChartColor;
	}
	public ArrayList<String> getArrlist() {
		return arrlist;
	}
	public void setArrlist(ArrayList<String> arrlist) {
		this.arrlist = arrlist;
	}

}
