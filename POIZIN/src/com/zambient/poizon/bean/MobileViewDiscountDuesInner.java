package com.zambient.poizon.bean;

import java.io.Serializable;

public class MobileViewDiscountDuesInner implements Serializable{
	
	private Double discounts;
	private Double rentals;
	private Double total;
	private Double received;
	private Double dues;
	private Double adjustment;
	public Double getDiscounts() {
		return discounts;
	}
	public void setDiscounts(Double discounts) {
		this.discounts = discounts;
	}
	public Double getRentals() {
		return rentals;
	}
	public void setRentals(Double rentals) {
		this.rentals = rentals;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public Double getReceived() {
		return received;
	}
	public void setReceived(Double received) {
		this.received = received;
	}
	public Double getDues() {
		return dues;
	}
	public void setDues(Double dues) {
		this.dues = dues;
	}
	public Double getAdjustment() {
		return adjustment;
	}
	public void setAdjustment(Double adjustment) {
		this.adjustment = adjustment;
	}

}
