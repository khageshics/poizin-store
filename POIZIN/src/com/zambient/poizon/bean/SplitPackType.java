package com.zambient.poizon.bean;

import java.io.Serializable;

public class SplitPackType implements Serializable{
	
	private Double L2Stock;
	private Double L1Stock;
	private Double SBStock;
	private Double DStock;
	private Double LBStock;
	private Double TINStock;
	private Double NStock;
	private Double PStock;
	private Double QStock;
	private Double XStock;
	public Double getL2Stock() {
		return L2Stock;
	}
	public void setL2Stock(Double l2Stock) {
		L2Stock = l2Stock;
	}
	public Double getL1Stock() {
		return L1Stock;
	}
	public void setL1Stock(Double l1Stock) {
		L1Stock = l1Stock;
	}
	public Double getSBStock() {
		return SBStock;
	}
	public void setSBStock(Double sBStock) {
		SBStock = sBStock;
	}
	public Double getDStock() {
		return DStock;
	}
	public void setDStock(Double dStock) {
		DStock = dStock;
	}
	public Double getLBStock() {
		return LBStock;
	}
	public void setLBStock(Double lBStock) {
		LBStock = lBStock;
	}
	public Double getTINStock() {
		return TINStock;
	}
	public void setTINStock(Double tINStock) {
		TINStock = tINStock;
	}
	public Double getNStock() {
		return NStock;
	}
	public void setNStock(Double nStock) {
		NStock = nStock;
	}
	public Double getPStock() {
		return PStock;
	}
	public void setPStock(Double pStock) {
		PStock = pStock;
	}
	public Double getQStock() {
		return QStock;
	}
	public void setQStock(Double qStock) {
		QStock = qStock;
	}
	public Double getXStock() {
		return XStock;
	}
	public void setXStock(Double xStock) {
		XStock = xStock;
	}

}
