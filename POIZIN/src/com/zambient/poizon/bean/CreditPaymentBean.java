package com.zambient.poizon.bean;

import java.io.Serializable;

public class CreditPaymentBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long paymentCreditId;
	private String paymentDate;
	private Double paymentAmount;
	private String paymentmode;
	private String paymentcomment;
	public Long getPaymentCreditId() {
		return paymentCreditId;
	}
	public void setPaymentCreditId(Long paymentCreditId) {
		this.paymentCreditId = paymentCreditId;
	}
	public String getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}
	public Double getPaymentAmount() {
		return paymentAmount;
	}
	public void setPaymentAmount(Double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}
	public String getPaymentmode() {
		return paymentmode;
	}
	public void setPaymentmode(String paymentmode) {
		this.paymentmode = paymentmode;
	}
	public String getPaymentcomment() {
		return paymentcomment;
	}
	public void setPaymentcomment(String paymentcomment) {
		this.paymentcomment = paymentcomment;
	}
	@Override
	public String toString() {
		return "CreditPaymentBean [paymentCreditId=" + paymentCreditId + ", paymentDate=" + paymentDate
				+ ", paymentAmount=" + paymentAmount + ", paymentmode=" + paymentmode + ", paymentcomment="
				+ paymentcomment + "]";
	}
	
	
}
