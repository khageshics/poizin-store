package com.zambient.poizon.bean;

public class CommulativeDiscountBean {
	
	private Long btls;
	private Double discAmount;
	private String Date;
	private Double govtProfit;
	private Double btlRate;
	private Double btlSaleMrp;
	private Double costPrice;
	
	public Long getBtls() {
		return btls;
	}
	public void setBtls(Long btls) {
		this.btls = btls;
	}
	public Double getDiscAmount() {
		return discAmount;
	}
	public void setDiscAmount(Double discAmount) {
		this.discAmount = discAmount;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public Double getGovtProfit() {
		return govtProfit;
	}
	public void setGovtProfit(Double govtProfit) {
		this.govtProfit = govtProfit;
	}
	public Double getBtlRate() {
		return btlRate;
	}
	public void setBtlRate(Double btlRate) {
		this.btlRate = btlRate;
	}
	public Double getBtlSaleMrp() {
		return btlSaleMrp;
	}
	public void setBtlSaleMrp(Double btlSaleMrp) {
		this.btlSaleMrp = btlSaleMrp;
	}
	public Double getCostPrice() {
		return costPrice;
	}
	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}
	
}
