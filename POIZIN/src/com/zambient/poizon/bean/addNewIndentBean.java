package com.zambient.poizon.bean;

import java.io.Serializable;

public class addNewIndentBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String indentMonth;
	private String indentDate;
	private String ngalleryImage;
	private String imageName;
	public Long getId() {
		return id;
	}
	public String getIndentMonth() {
		return indentMonth;
	}
	public String getIndentDate() {
		return indentDate;
	}
	public String getNgalleryImage() {
		return ngalleryImage;
	}
	public String getImageName() {
		return imageName;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public void setIndentMonth(String indentMonth) {
		this.indentMonth = indentMonth;
	}
	public void setIndentDate(String indentDate) {
		this.indentDate = indentDate;
	}
	public void setNgalleryImage(String ngalleryImage) {
		this.ngalleryImage = ngalleryImage;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	@Override
	public String toString() {
		return "addNewIndentBean [id=" + id + ", indentMonth=" + indentMonth + ", indentDate=" + indentDate
				+ ", ngalleryImage=" + ngalleryImage + ", imageName=" + imageName + "]";
	}
	
}
