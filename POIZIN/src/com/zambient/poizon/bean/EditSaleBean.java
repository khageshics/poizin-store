package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class EditSaleBean implements Serializable{
private static final long serialVersionUID = 1L;
	
	private Long productId;
	private Double brandNoPackQty;
	private Long brandNo;
	private String brandName;
	private String productType;
	private String quantity; 
	private Long packQty;
	private String packType;
	private Double SingleBottelRate;
	private Double packQtyRate;
	private Double unitPrice;
	private Long saleId;
	List<SaleBean> saleBean;
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Double getBrandNoPackQty() {
		return brandNoPackQty;
	}
	public void setBrandNoPackQty(Double brandNoPackQty) {
		this.brandNoPackQty = brandNoPackQty;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public Long getPackQty() {
		return packQty;
	}
	public void setPackQty(Long packQty) {
		this.packQty = packQty;
	}
	public String getPackType() {
		return packType;
	}
	public void setPackType(String packType) {
		this.packType = packType;
	}
	public Double getSingleBottelRate() {
		return SingleBottelRate;
	}
	public void setSingleBottelRate(Double singleBottelRate) {
		SingleBottelRate = singleBottelRate;
	}
	public Double getPackQtyRate() {
		return packQtyRate;
	}
	public void setPackQtyRate(Double packQtyRate) {
		this.packQtyRate = packQtyRate;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Long getSaleId() {
		return saleId;
	}
	public void setSaleId(Long saleId) {
		this.saleId = saleId;
	}
	public List<SaleBean> getSaleBean() {
		return saleBean;
	}
	public void setSaleBean(List<SaleBean> saleBean) {
		this.saleBean = saleBean;
	}
	

}
