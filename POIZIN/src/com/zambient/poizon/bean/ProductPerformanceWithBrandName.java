package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class ProductPerformanceWithBrandName implements Serializable{
	
	private String brandName;
	List<ProductPerformanceBean> productPerformanceBean;
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public List<ProductPerformanceBean> getProductPerformanceBean() {
		return productPerformanceBean;
	}
	public void setProductPerformanceBean(List<ProductPerformanceBean> productPerformanceBean) {
		this.productPerformanceBean = productPerformanceBean;
	}

}
