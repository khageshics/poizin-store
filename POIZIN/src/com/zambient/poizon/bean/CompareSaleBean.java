package com.zambient.poizon.bean;

import java.io.Serializable;

public class CompareSaleBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String brandName;
	private Long brandNo;
	private Double brandNoPackQty;
	private Long categoryId;
	private Long companyId;
	private String company;
	private String category;
	private String quantity;
	private String packType;
	private Long packQty;
	private Long currentSale;
	private Long previousSale;
	private String companyColor;
	private String categoryColor;
	private Long companyOrder;
	private Long categoryOrder;
	private String currentStartDate;
	private String currentEndDate;
	private String previousStartDate;
	private String previousEndDate;
	private Double currentSalePrice;
	private Double previousSalePrice;
	private String showCurrentSelectedDate;
	private String showPreviousSelectedDate;
	
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public Double getBrandNoPackQty() {
		return brandNoPackQty;
	}
	public void setBrandNoPackQty(Double brandNoPackQty) {
		this.brandNoPackQty = brandNoPackQty;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getPackType() {
		return packType;
	}
	public void setPackType(String packType) {
		this.packType = packType;
	}
	public Long getPackQty() {
		return packQty;
	}
	public void setPackQty(Long packQty) {
		this.packQty = packQty;
	}
	public Long getCurrentSale() {
		return currentSale;
	}
	public void setCurrentSale(Long currentSale) {
		this.currentSale = currentSale;
	}
	public Long getPreviousSale() {
		return previousSale;
	}
	public void setPreviousSale(Long previousSale) {
		this.previousSale = previousSale;
	}
	public String getCompanyColor() {
		return companyColor;
	}
	public void setCompanyColor(String companyColor) {
		this.companyColor = companyColor;
	}
	public String getCategoryColor() {
		return categoryColor;
	}
	public void setCategoryColor(String categoryColor) {
		this.categoryColor = categoryColor;
	}
	public Long getCompanyOrder() {
		return companyOrder;
	}
	public void setCompanyOrder(Long companyOrder) {
		this.companyOrder = companyOrder;
	}
	public Long getCategoryOrder() {
		return categoryOrder;
	}
	public void setCategoryOrder(Long categoryOrder) {
		this.categoryOrder = categoryOrder;
	}
	public String getCurrentStartDate() {
		return currentStartDate;
	}
	public void setCurrentStartDate(String currentStartDate) {
		this.currentStartDate = currentStartDate;
	}
	public String getCurrentEndDate() {
		return currentEndDate;
	}
	public void setCurrentEndDate(String currentEndDate) {
		this.currentEndDate = currentEndDate;
	}
	public String getPreviousStartDate() {
		return previousStartDate;
	}
	public void setPreviousStartDate(String previousStartDate) {
		this.previousStartDate = previousStartDate;
	}
	public String getPreviousEndDate() {
		return previousEndDate;
	}
	public void setPreviousEndDate(String previousEndDate) {
		this.previousEndDate = previousEndDate;
	}
	public Double getCurrentSalePrice() {
		return currentSalePrice;
	}
	public void setCurrentSalePrice(Double currentSalePrice) {
		this.currentSalePrice = currentSalePrice;
	}
	public Double getPreviousSalePrice() {
		return previousSalePrice;
	}
	public void setPreviousSalePrice(Double previousSalePrice) {
		this.previousSalePrice = previousSalePrice;
	}
	public String getShowCurrentSelectedDate() {
		return showCurrentSelectedDate;
	}
	public String getShowPreviousSelectedDate() {
		return showPreviousSelectedDate;
	}
	public void setShowCurrentSelectedDate(String showCurrentSelectedDate) {
		this.showCurrentSelectedDate = showCurrentSelectedDate;
	}
	public void setShowPreviousSelectedDate(String showPreviousSelectedDate) {
		this.showPreviousSelectedDate = showPreviousSelectedDate;
	}
	
}
