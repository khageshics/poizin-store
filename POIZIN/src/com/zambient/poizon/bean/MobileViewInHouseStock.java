package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class MobileViewInHouseStock implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String name;
	private Long stock;
	private Long stockInBottles;
	private String color;
	private Long order;
	List<MobileViewInhouseStockCategoryWise> mobileViewInhouseStockCategoryWise;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getStock() {
		return stock;
	}
	public void setStock(Long stock) {
		this.stock = stock;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Long getOrder() {
		return order;
	}
	public void setOrder(Long order) {
		this.order = order;
	}
	public List<MobileViewInhouseStockCategoryWise> getMobileViewInhouseStockCategoryWise() {
		return mobileViewInhouseStockCategoryWise;
	}
	public void setMobileViewInhouseStockCategoryWise(
			List<MobileViewInhouseStockCategoryWise> mobileViewInhouseStockCategoryWise) {
		this.mobileViewInhouseStockCategoryWise = mobileViewInhouseStockCategoryWise;
	}
	public Long getStockInBottles() {
		return stockInBottles;
	}
	public void setStockInBottles(Long stockInBottles) {
		this.stockInBottles = stockInBottles;
	}

}
