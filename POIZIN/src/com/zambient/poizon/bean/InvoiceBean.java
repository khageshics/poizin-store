package com.zambient.poizon.bean;

import java.io.Serializable;

public class InvoiceBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long invoiceId;
	private Long brandNoPackQty;
	private Long caseQty;
	private Long packQty;
	private Long QtyBottels;
	private Double SingleBottelRate;
	private Double packQtyRate;
	private Double totalPrice;
	private String invoiceDate;
	private Double EachBottleMrp;
	private Long saleId;
	public Double getEachBottleMrp() {
		return EachBottleMrp;
	}
	public void setEachBottleMrp(Double eachBottleMrp) {
		EachBottleMrp = eachBottleMrp;
	}
	public Long getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}
	public Long getBrandNoPackQty() {
		return brandNoPackQty;
	}
	public void setBrandNoPackQty(Long brandNoPackQty) {
		this.brandNoPackQty = brandNoPackQty;
	}
	public Long getCaseQty() {
		return caseQty;
	}
	public void setCaseQty(Long caseQty) {
		this.caseQty = caseQty;
	}
	public Long getPackQty() {
		return packQty;
	}
	public void setPackQty(Long packQty) {
		this.packQty = packQty;
	}
	public Long getQtyBottels() {
		return QtyBottels;
	}
	public void setQtyBottels(Long qtyBottels) {
		QtyBottels = qtyBottels;
	}
	public Double getSingleBottelRate() {
		return SingleBottelRate;
	}
	public void setSingleBottelRate(Double singleBottelRate) {
		SingleBottelRate = singleBottelRate;
	}
	public Double getPackQtyRate() {
		return packQtyRate;
	}
	public void setPackQtyRate(Double packQtyRate) {
		this.packQtyRate = packQtyRate;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public Long getSaleId() {
		return saleId;
	}
	public void setSaleId(Long saleId) {
		this.saleId = saleId;
	}
	@Override
	public String toString() {
		return "InvoiceBean [invoiceId=" + invoiceId + ", brandNoPackQty=" + brandNoPackQty + ", caseQty=" + caseQty
				+ ", packQty=" + packQty + ", QtyBottels=" + QtyBottels + ", SingleBottelRate=" + SingleBottelRate
				+ ", packQtyRate=" + packQtyRate + ", totalPrice=" + totalPrice + ", invoiceDate=" + invoiceDate + "]";
	}

}
