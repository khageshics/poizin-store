package com.zambient.poizon.bean;

import java.io.Serializable;

public class NotClearCheckBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String month;
	private String checkNo;
	private Double amount;
	private String realDate;
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getCheckNo() {
		return checkNo;
	}
	public void setCheckNo(String checkNo) {
		this.checkNo = checkNo;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getRealDate() {
		return realDate;
	}
	public void setRealDate(String realDate) {
		this.realDate = realDate;
	}
	

}
