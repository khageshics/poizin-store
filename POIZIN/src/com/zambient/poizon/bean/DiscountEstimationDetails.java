package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class DiscountEstimationDetails implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long brandNo;
	private String brandName;
	private String date;
	private Long companyOrder;
	private String companyColor; 
	private String category;
	private String company;
	private Double inHouseStock;
	private Long estimateinhouseStock;
	private Long targetCase;
	private  Double lastMonthSold;
	private Double dicsPerCase;
	private Double investment;
	private Double clearnsPeriod;
	private Double rateOfInterestWithInhouse;
	private Double rateOfInterestWithoutInhouse;
	private Double caseRate;
	private Double caseRateEstimation;
	private Double totalDiscount;
	private Double clearnsPeriodWithoutInhouseStock;
	private Double liftedCase;
	private Long companyId;
	private Long pending;
	private String categoryColor;
	private Long saleSheetOrder;
	private SplitPackType splitPackType;
	private Long categoryId;
	List<MaxMinDiscountBean> maxMinDiscountBean;
	private String productType;
	
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public Double getInHouseStock() {
		return inHouseStock;
	}
	public void setInHouseStock(Double inHouseStock) {
		this.inHouseStock = inHouseStock;
	}
	public Long getEstimateinhouseStock() {
		return estimateinhouseStock;
	}
	public void setEstimateinhouseStock(Long estimateinhouseStock) {
		this.estimateinhouseStock = estimateinhouseStock;
	}
	public Long getTargetCase() {
		return targetCase;
	}
	public void setTargetCase(Long targetCase) {
		this.targetCase = targetCase;
	}
	public Double getLastMonthSold() {
		return lastMonthSold;
	}
	public void setLastMonthSold(Double lastMonthSold) {
		this.lastMonthSold = lastMonthSold;
	}
	public Double getDicsPerCase() {
		return dicsPerCase;
	}
	public void setDicsPerCase(Double dicsPerCase) {
		this.dicsPerCase = dicsPerCase;
	}
	public Double getInvestment() {
		return investment;
	}
	public void setInvestment(Double investment) {
		this.investment = investment;
	}
	public Double getClearnsPeriod() {
		return clearnsPeriod;
	}
	public void setClearnsPeriod(Double clearnsPeriod) {
		this.clearnsPeriod = clearnsPeriod;
	}
	public Double getRateOfInterestWithInhouse() {
		return rateOfInterestWithInhouse;
	}
	public void setRateOfInterestWithInhouse(Double rateOfInterestWithInhouse) {
		this.rateOfInterestWithInhouse = rateOfInterestWithInhouse;
	}
	public Double getRateOfInterestWithoutInhouse() {
		return rateOfInterestWithoutInhouse;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setRateOfInterestWithoutInhouse(Double rateOfInterestWithoutInhouse) {
		this.rateOfInterestWithoutInhouse = rateOfInterestWithoutInhouse;
	}
	public Long getCompanyOrder() {
		return companyOrder;
	}
	public void setCompanyOrder(Long companyOrder) {
		this.companyOrder = companyOrder;
	}
	public String getCompanyColor() {
		return companyColor;
	}
	public void setCompanyColor(String companyColor) {
		this.companyColor = companyColor;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public Double getCaseRate() {
		return caseRate;
	}
	public void setCaseRate(Double caseRate) {
		this.caseRate = caseRate;
	}
	public Double getCaseRateEstimation() {
		return caseRateEstimation;
	}
	public void setCaseRateEstimation(Double caseRateEstimation) {
		this.caseRateEstimation = caseRateEstimation;
	}
	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public Double getClearnsPeriodWithoutInhouseStock() {
		return clearnsPeriodWithoutInhouseStock;
	}
	public void setClearnsPeriodWithoutInhouseStock(Double clearnsPeriodWithoutInhouseStock) {
		this.clearnsPeriodWithoutInhouseStock = clearnsPeriodWithoutInhouseStock;
	}
	public Double getLiftedCase() {
		return liftedCase;
	}
	public void setLiftedCase(Double liftedCase) {
		this.liftedCase = liftedCase;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public Long getPending() {
		return pending;
	}
	public void setPending(Long pending) {
		this.pending = pending;
	}
	public String getCategoryColor() {
		return categoryColor;
	}
	public void setCategoryColor(String categoryColor) {
		this.categoryColor = categoryColor;
	}
	public Long getSaleSheetOrder() {
		return saleSheetOrder;
	}
	public void setSaleSheetOrder(Long saleSheetOrder) {
		this.saleSheetOrder = saleSheetOrder;
	}
	public SplitPackType getSplitPackType() {
		return splitPackType;
	}
	public void setSplitPackType(SplitPackType splitPackType) {
		this.splitPackType = splitPackType;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public List<MaxMinDiscountBean> getMaxMinDiscountBean() {
		return maxMinDiscountBean;
	}
	public void setMaxMinDiscountBean(List<MaxMinDiscountBean> maxMinDiscountBean) {
		this.maxMinDiscountBean = maxMinDiscountBean;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}

}
