package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class CallDiscountBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String name;
	private Long brandNo;
	private Double caseRate;
	private Double inhouseStock;
	private Double lastMonthSold;
	private Double liftedCase;
	private Long targetCase;
	private Double investment;
	private Long productGrouping;
	private Long companyOrder;
	private String companyColor;
	private Long adjustment;
	private Double discountAmount;
	private Long companyId;
	private String companyName;
	private String productType;
	private String groupName;
	private SplitPackType splitPackType;
	List<MaxMinDiscountBean> maxMinDiscountBean;
	private String stockDates;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public Double getCaseRate() {
		return caseRate;
	}
	public void setCaseRate(Double caseRate) {
		this.caseRate = caseRate;
	}
	public Double getInhouseStock() {
		return inhouseStock;
	}
	public void setInhouseStock(Double double1) {
		this.inhouseStock = double1;
	}
	public Double getLastMonthSold() {
		return lastMonthSold;
	}
	public void setLastMonthSold(Double double1) {
		this.lastMonthSold = double1;
	}
	public Double getLiftedCase() {
		return liftedCase;
	}
	public void setLiftedCase(Double double1) {
		this.liftedCase = double1;
	}
	public Long getTargetCase() {
		return targetCase;
	}
	public void setTargetCase(Long targetCase) {
		this.targetCase = targetCase;
	}
	public Double getInvestment() {
		return investment;
	}
	public void setInvestment(Double investment) {
		this.investment = investment;
	}
	public Long getProductGrouping() {
		return productGrouping;
	}
	public void setProductGrouping(Long productGrouping) {
		this.productGrouping = productGrouping;
	}
	public Long getCompanyOrder() {
		return companyOrder;
	}
	public void setCompanyOrder(Long companyOrder) {
		this.companyOrder = companyOrder;
	}
	public String getCompanyColor() {
		return companyColor;
	}
	public void setCompanyColor(String companyColor) {
		this.companyColor = companyColor;
	}
	public Long getAdjustment() {
		return adjustment;
	}
	public void setAdjustment(Long adjustment) {
		this.adjustment = adjustment;
	}
	public Double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public SplitPackType getSplitPackType() {
		return splitPackType;
	}
	public void setSplitPackType(SplitPackType splitPackType) {
		this.splitPackType = splitPackType;
	}
	public List<MaxMinDiscountBean> getMaxMinDiscountBean() {
		return maxMinDiscountBean;
	}
	public void setMaxMinDiscountBean(List<MaxMinDiscountBean> maxMinDiscountBean) {
		this.maxMinDiscountBean = maxMinDiscountBean;
	}
	public String getStockDates() {
		return stockDates;
	}
	public void setStockDates(String stockDates) {
		this.stockDates = stockDates;
	}
	@Override
	public String toString() {
		return "CallDiscountBean [name=" + name + ", brandNo=" + brandNo + ", caseRate=" + caseRate + ", inhouseStock="
				+ inhouseStock + ", lastMonthSold=" + lastMonthSold + ", liftedCase=" + liftedCase + ", targetCase="
				+ targetCase + ", investment=" + investment + ", productGrouping=" + productGrouping + ", companyOrder="
				+ companyOrder + ", companyColor=" + companyColor + ", adjustment=" + adjustment + ", discountAmount="
				+ discountAmount + ", companyId=" + companyId + ", companyName=" + companyName + ", productType="
				+ productType + ", groupName=" + groupName + ", splitPackType=" + splitPackType
				+ ", maxMinDiscountBean=" + maxMinDiscountBean + ", stockDates=" + stockDates + "]";
	}
	
}
