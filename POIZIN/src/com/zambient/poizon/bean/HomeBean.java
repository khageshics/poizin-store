package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class HomeBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Double inHouseStockValue;
	private Double saleValue;
	private Double pendingDiscount;
	private Double profitOrLoss;
	private List<DayAndSaleBean> monthlySale;
	private List<TotalPriceDateWiseBean> dailySale;
	private Double comulativeValue;
	private Double tcsVal;
	private List<DayAndSaleBean> maxSaleProduct;
	private List<TotalPriceDateWiseBean> doughnutChart;
	private List<SaleBean> lowStockProduct;
	private List<DiscountAsPerCompanyBeen> companyDiscountDues;
	private String currentDate;
	private DateAndValueBean retailerCreditBalance;
	
	
	public DateAndValueBean getRetailerCreditBalance() {
		return retailerCreditBalance;
	}
	public void setRetailerCreditBalance(DateAndValueBean retailerCreditBalance) {
		this.retailerCreditBalance = retailerCreditBalance;
	}
	public List<TotalPriceDateWiseBean> getDoughnutChart() {
		return doughnutChart;
	}
	public void setDoughnutChart(List<TotalPriceDateWiseBean> doughnutChart) {
		this.doughnutChart = doughnutChart;
	}
	public Double getTcsVal() {
		return tcsVal;
	}
	public void setTcsVal(Double tcsVal) {
		this.tcsVal = tcsVal;
	}
	public Double getInHouseStockValue() {
		return inHouseStockValue;
	}
	public void setInHouseStockValue(Double inHouseStockValue) {
		this.inHouseStockValue = inHouseStockValue;
	}
	public Double getSaleValue() {
		return saleValue;
	}
	public void setSaleValue(Double saleValue) {
		this.saleValue = saleValue;
	}
	public Double getPendingDiscount() {
		return pendingDiscount;
	}
	public void setPendingDiscount(Double pendingDiscount) {
		this.pendingDiscount = pendingDiscount;
	}
	public Double getProfitOrLoss() {
		return profitOrLoss;
	}
	public void setProfitOrLoss(Double profitOrLoss) {
		this.profitOrLoss = profitOrLoss;
	}
	public List<DayAndSaleBean> getMonthlySale() {
		return monthlySale;
	}
	public void setMonthlySale(List<DayAndSaleBean> monthlySale) {
		this.monthlySale = monthlySale;
	}
	public List<TotalPriceDateWiseBean> getDailySale() {
		return dailySale;
	}
	public void setDailySale(List<TotalPriceDateWiseBean> dailySale) {
		this.dailySale = dailySale;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Double getComulativeValue() {
		return comulativeValue;
	}
	public void setComulativeValue(Double comulativeValue) {
		this.comulativeValue = comulativeValue;
	}
	public List<DayAndSaleBean> getMaxSaleProduct() {
		return maxSaleProduct;
	}
	public void setMaxSaleProduct(List<DayAndSaleBean> maxSaleProduct) {
		this.maxSaleProduct = maxSaleProduct;
	}
	public List<SaleBean> getLowStockProduct() {
		return lowStockProduct;
	}
	public void setLowStockProduct(List<SaleBean> lowStockProduct) {
		this.lowStockProduct = lowStockProduct;
	}
	public List<DiscountAsPerCompanyBeen> getCompanyDiscountDues() {
		return companyDiscountDues;
	}
	public void setCompanyDiscountDues(List<DiscountAsPerCompanyBeen> companyDiscountDues) {
		this.companyDiscountDues = companyDiscountDues;
	}
	public String getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

}
