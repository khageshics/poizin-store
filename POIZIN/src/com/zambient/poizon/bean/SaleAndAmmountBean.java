package com.zambient.poizon.bean;

import java.util.List;

public class SaleAndAmmountBean {
	private static final long serialVersionUID = 1L;
	private Long expenseId;
	private Double amount;
	private String comment;
	private String expenseDate;
	private Double diff;
	private Double total;
	List<SaleBean> saleBean;
	public Long getExpenseId() {
		return expenseId;
	}
	public void setExpenseId(Long expenseId) {
		this.expenseId = expenseId;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getExpenseDate() {
		return expenseDate;
	}
	public void setExpenseDate(String expenseDate) {
		this.expenseDate = expenseDate;
	}
	public List<SaleBean> getSaleBean() {
		return saleBean;
	}
	public void setSaleBean(List<SaleBean> saleBean) {
		this.saleBean = saleBean;
	}
	@Override
	public String toString() {
		return "SaleAndAmmountBean [expenseId=" + expenseId + ", amount=" + amount + ", comment=" + comment
				+ ", expenseDate=" + expenseDate + ", saleBean=" + saleBean + "]";
	}
	public Double getDiff() {
		return diff;
	}
	public void setDiff(Double diff) {
		this.diff = diff;
	}
	public Double getTotal() {
		return total;
	}
	public void setTotal(Double total) {
		this.total = total;
	}

}
