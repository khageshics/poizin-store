package com.zambient.poizon.bean;

import java.io.Serializable;

public class QuantityBean implements Serializable{
	
	private Double twoThousand;
	private Double oneThousandFiveHundred;
	private Double oneThousand;
	private Double sevenFifty;
	private Double threeSeventyFive;
	private Double oneEighty;
	private Double ninety;
	private Double sevenHundred;
	private Double twoFifty;
	private Double sixFifty;
	private Double threeThirty;
	private Double fiveHundred;
	private Double threeFiftyFive;
	private Double sixty;
	private Double fifty;
	private Long twoThousandDays;
	private Long oneThousandFiveHundredDays;
	private Long oneThousandDays;
	private Long sevenFiftyDays;
	private Long threeSeventyDays;
	private Long oneEightyDays;
	private Long ninetyDays;
	private Long sevenHundredDays;
	private Long twoFiftyDays;
	private Long sixFiftyDays;
	private Long threeThirtyDays;
	private Long fiveHundredDays;
	private Long threeFiftyFiveDays;
	private Long sixtyDays;
	private Long fiftyDays;
	private String color;
	private String name;
	private Long order;
	private String ProductType;
	private Double casePrice;
	
	public String getProductType() {
		return ProductType;
	}
	public void setProductType(String productType) {
		ProductType = productType;
	}
	public Double getTwoThousand() {
		return twoThousand;
	}
	public void setTwoThousand(Double twoThousand) {
		this.twoThousand = twoThousand;
	}
	public Double getOneThousandFiveHundred() {
		return oneThousandFiveHundred;
	}
	public void setOneThousandFiveHundred(Double oneThousandFiveHundred) {
		this.oneThousandFiveHundred = oneThousandFiveHundred;
	}
	public Double getOneThousand() {
		return oneThousand;
	}
	public void setOneThousand(Double oneThousand) {
		this.oneThousand = oneThousand;
	}
	public Double getSevenFifty() {
		return sevenFifty;
	}
	public void setSevenFifty(Double sevenFifty) {
		this.sevenFifty = sevenFifty;
	}
	public Double getThreeSeventyFive() {
		return threeSeventyFive;
	}
	public void setThreeSeventyFive(Double threeSeventyFive) {
		this.threeSeventyFive = threeSeventyFive;
	}
	public Double getOneEighty() {
		return oneEighty;
	}
	public void setOneEighty(Double oneEighty) {
		this.oneEighty = oneEighty;
	}
	public Double getNinety() {
		return ninety;
	}
	public void setNinety(Double ninety) {
		this.ninety = ninety;
	}
	public Double getSevenHundred() {
		return sevenHundred;
	}
	public void setSevenHundred(Double sevenHundred) {
		this.sevenHundred = sevenHundred;
	}
	public Double getTwoFifty() {
		return twoFifty;
	}
	public void setTwoFifty(Double twoFifty) {
		this.twoFifty = twoFifty;
	}
	public Double getSixFifty() {
		return sixFifty;
	}
	public void setSixFifty(Double sixFifty) {
		this.sixFifty = sixFifty;
	}
	public Double getThreeThirty() {
		return threeThirty;
	}
	public void setThreeThirty(Double threeThirty) {
		this.threeThirty = threeThirty;
	}
	public Double getFiveHundred() {
		return fiveHundred;
	}
	public void setFiveHundred(Double fiveHundred) {
		this.fiveHundred = fiveHundred;
	}
	public Double getThreeFiftyFive() {
		return threeFiftyFive;
	}
	public void setThreeFiftyFive(Double threeFiftyFive) {
		this.threeFiftyFive = threeFiftyFive;
	}
	public Double getSixty() {
		return sixty;
	}
	public void setSixty(Double sixty) {
		this.sixty = sixty;
	}
	public Double getFifty() {
		return fifty;
	}
	public void setFifty(Double fifty) {
		this.fifty = fifty;
	}
	public Long getTwoThousandDays() {
		return twoThousandDays;
	}
	public void setTwoThousandDays(Long twoThousandDays) {
		this.twoThousandDays = twoThousandDays;
	}
	public Long getOneThousandFiveHundredDays() {
		return oneThousandFiveHundredDays;
	}
	public void setOneThousandFiveHundredDays(Long oneThousandFiveHundredDays) {
		this.oneThousandFiveHundredDays = oneThousandFiveHundredDays;
	}
	public Long getOneThousandDays() {
		return oneThousandDays;
	}
	public void setOneThousandDays(Long oneThousandDays) {
		this.oneThousandDays = oneThousandDays;
	}
	public Long getSevenFiftyDays() {
		return sevenFiftyDays;
	}
	public void setSevenFiftyDays(Long sevenFiftyDays) {
		this.sevenFiftyDays = sevenFiftyDays;
	}
	public Long getThreeSeventyDays() {
		return threeSeventyDays;
	}
	public void setThreeSeventyDays(Long threeSeventyDays) {
		this.threeSeventyDays = threeSeventyDays;
	}
	public Long getOneEightyDays() {
		return oneEightyDays;
	}
	public void setOneEightyDays(Long oneEightyDays) {
		this.oneEightyDays = oneEightyDays;
	}
	public Long getNinetyDays() {
		return ninetyDays;
	}
	public void setNinetyDays(Long ninetyDays) {
		this.ninetyDays = ninetyDays;
	}
	public Long getSevenHundredDays() {
		return sevenHundredDays;
	}
	public void setSevenHundredDays(Long sevenHundredDays) {
		this.sevenHundredDays = sevenHundredDays;
	}
	public Long getTwoFiftyDays() {
		return twoFiftyDays;
	}
	public void setTwoFiftyDays(Long twoFiftyDays) {
		this.twoFiftyDays = twoFiftyDays;
	}
	public Long getSixFiftyDays() {
		return sixFiftyDays;
	}
	public void setSixFiftyDays(Long sixFiftyDays) {
		this.sixFiftyDays = sixFiftyDays;
	}
	public Long getThreeThirtyDays() {
		return threeThirtyDays;
	}
	public void setThreeThirtyDays(Long threeThirtyDays) {
		this.threeThirtyDays = threeThirtyDays;
	}
	public Long getFiveHundredDays() {
		return fiveHundredDays;
	}
	public void setFiveHundredDays(Long fiveHundredDays) {
		this.fiveHundredDays = fiveHundredDays;
	}
	public Long getThreeFiftyFiveDays() {
		return threeFiftyFiveDays;
	}
	public void setThreeFiftyFiveDays(Long threeFiftyFiveDays) {
		this.threeFiftyFiveDays = threeFiftyFiveDays;
	}
	public Long getSixtyDays() {
		return sixtyDays;
	}
	public void setSixtyDays(Long sixtyDays) {
		this.sixtyDays = sixtyDays;
	}
	public Long getFiftyDays() {
		return fiftyDays;
	}
	public void setFiftyDays(Long fiftyDays) {
		this.fiftyDays = fiftyDays;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getOrder() {
		return order;
	}
	public void setOrder(Long order) {
		this.order = order;
	}
	public Double getCasePrice() {
		return casePrice;
	}
	public void setCasePrice(Double casePrice) {
		this.casePrice = casePrice;
	}

}
