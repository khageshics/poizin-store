package com.zambient.poizon.bean;

import java.io.Serializable;

public class AddNewBrandBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long productId;
	private Double brandNoPackQty;
	private Long brandNo;
	private String brandName;
	private String productType;
	private String quantity; 
	private Long packQty;
	private String packType;
	private Double SingleBottelRate;
	private Double packQtyRate;
	private Double unitPrice;
	private Long saleId;
	private Long category;
	private Long company;
	private String group;
	private Long match;
	private String shortBrandName;
	private Double specialMargin;
	private String matchName;
	private Long realBrandNo;
	
	public String getMatchName() {
		return matchName;
	}
	public void setMatchName(String matchName) {
		this.matchName = matchName;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Double getBrandNoPackQty() {
		return brandNoPackQty;
	}
	public void setBrandNoPackQty(Double brandNoPackQty) {
		this.brandNoPackQty = brandNoPackQty;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public Long getPackQty() {
		return packQty;
	}
	public void setPackQty(Long packQty) {
		this.packQty = packQty;
	}
	public String getPackType() {
		return packType;
	}
	public void setPackType(String packType) {
		this.packType = packType;
	}
	public Double getSingleBottelRate() {
		return SingleBottelRate;
	}
	public void setSingleBottelRate(Double singleBottelRate) {
		SingleBottelRate = singleBottelRate;
	}
	public Double getPackQtyRate() {
		return packQtyRate;
	}
	public void setPackQtyRate(Double packQtyRate) {
		this.packQtyRate = packQtyRate;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Long getSaleId() {
		return saleId;
	}
	public void setSaleId(Long saleId) {
		this.saleId = saleId;
	}
	public Long getMatch() {
		return match;
	}
	public void setMatch(Long match) {
		this.match = match;
	}
	public Long getCategory() {
		return category;
	}
	public void setCategory(Long category) {
		this.category = category;
	}
	public Long getCompany() {
		return company;
	}
	public void setCompany(Long company) {
		this.company = company;
	}
	public String getShortBrandName() {
		return shortBrandName;
	}
	public void setShortBrandName(String shortBrandName) {
		this.shortBrandName = shortBrandName;
	}
	public Double getSpecialMargin() {
		return specialMargin;
	}
	public void setSpecialMargin(Double specialMargin) {
		this.specialMargin = specialMargin;
	}
	public Long getRealBrandNo() {
		return realBrandNo;
	}
	public void setRealBrandNo(Long realBrandNo) {
		this.realBrandNo = realBrandNo;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	@Override
	public String toString() {
		return "AddNewBrandBean [productId=" + productId + ", brandNoPackQty=" + brandNoPackQty + ", brandNo=" + brandNo
				+ ", brandName=" + brandName + ", productType=" + productType + ", quantity=" + quantity + ", packQty="
				+ packQty + ", packType=" + packType + ", SingleBottelRate=" + SingleBottelRate + ", packQtyRate="
				+ packQtyRate + ", unitPrice=" + unitPrice + ", saleId=" + saleId + ", category=" + category
				+ ", company=" + company + ", group=" + group + ", match=" + match + ", shortBrandName="
				+ shortBrandName + ", specialMargin=" + specialMargin + ", matchName=" + matchName + ", realBrandNo="
				+ realBrandNo + "]";
	}
}
