package com.zambient.poizon.bean;

import java.io.Serializable;

public class TillDateBalanceShhetBeen implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Double saleAmount;
	private Double debits;
	private Double credits;
	private Double discountCheque;
	private Double invoiceAmount;
	private Double expenseDayWise;
	private Double expenseMonthWise;
	private Double totalAssets;
	private Double totalLiabilities;
	private Double profit;
	private Double loss;
	private Double inhouseStock;
	
	public Double getInhouseStock() {
		return inhouseStock;
	}
	public void setInhouseStock(Double inhouseStock) {
		this.inhouseStock = inhouseStock;
	}
	public Double getSaleAmount() {
		return saleAmount;
	}
	public void setSaleAmount(Double saleAmount) {
		this.saleAmount = saleAmount;
	}
	public Double getDebits() {
		return debits;
	}
	public void setDebits(Double debits) {
		this.debits = debits;
	}
	public Double getCredits() {
		return credits;
	}
	public void setCredits(Double credits) {
		this.credits = credits;
	}
	public Double getDiscountCheque() {
		return discountCheque;
	}
	public void setDiscountCheque(Double discountCheque) {
		this.discountCheque = discountCheque;
	}
	public Double getInvoiceAmount() {
		return invoiceAmount;
	}
	public void setInvoiceAmount(Double invoiceAmount) {
		this.invoiceAmount = invoiceAmount;
	}
	public Double getExpenseDayWise() {
		return expenseDayWise;
	}
	public void setExpenseDayWise(Double expenseDayWise) {
		this.expenseDayWise = expenseDayWise;
	}
	public Double getExpenseMonthWise() {
		return expenseMonthWise;
	}
	public void setExpenseMonthWise(Double expenseMonthWise) {
		this.expenseMonthWise = expenseMonthWise;
	}
	public Double getTotalAssets() {
		return totalAssets;
	}
	public void setTotalAssets(Double totalAssets) {
		this.totalAssets = totalAssets;
	}
	public Double getTotalLiabilities() {
		return totalLiabilities;
	}
	public void setTotalLiabilities(Double totalLiabilities) {
		this.totalLiabilities = totalLiabilities;
	}
	public Double getProfit() {
		return profit;
	}
	public void setProfit(Double profit) {
		this.profit = profit;
	}
	public Double getLoss() {
		return loss;
	}
	public void setLoss(Double loss) {
		this.loss = loss;
	}

}
