package com.zambient.poizon.bean;

import java.io.Serializable;

public class VendorAdjustmentBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String month;
	private int company;
	private Double amount;
	private String comment;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public int getCompany() {
		return company;
	}
	public void setCompany(int company) {
		this.company = company;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Override
	public String toString() {
		return "VendorAdjustmentBean [id=" + id + ", month=" + month + ", company=" + company + ", amount=" + amount
				+ ", comment=" + comment + "]";
	}
	
	
	
}
