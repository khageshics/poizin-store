package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class DayAndSaleBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private String date;
	private Double value;
	List<PieChartBean> linechartData;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public List<PieChartBean> getLinechartData() {
		return linechartData;
	}
	public void setLinechartData(List<PieChartBean> linechartData) {
		this.linechartData = linechartData;
	}
}
