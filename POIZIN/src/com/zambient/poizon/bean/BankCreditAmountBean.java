package com.zambient.poizon.bean;

import java.io.Serializable;

public class BankCreditAmountBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private String bankdate;
	private Double bankCredit;
	private Double retention;
	public String getBankdate() {
		return bankdate;
	}
	public void setBankdate(String bankdate) {
		this.bankdate = bankdate;
	}
	public Double getBankCredit() {
		return bankCredit;
	}
	public void setBankCredit(Double bankCredit) {
		this.bankCredit = bankCredit;
	}
	public Double getRetention() {
		return retention;
	}
	public void setRetention(Double retention) {
		this.retention = retention;
	}

}
