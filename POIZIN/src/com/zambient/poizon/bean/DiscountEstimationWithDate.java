package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class DiscountEstimationWithDate implements Serializable{
	
	private String date;
	private boolean flag;
	List<DiscountEstimationBean> discountEstimationBean;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public List<DiscountEstimationBean> getDiscountEstimationBean() {
		return discountEstimationBean;
	}
	public void setDiscountEstimationBean(List<DiscountEstimationBean> discountEstimationBean) {
		this.discountEstimationBean = discountEstimationBean;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}

}
