package com.zambient.poizon.bean;

import java.io.Serializable;

public class data implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Double value;

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

}
