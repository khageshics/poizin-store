package com.zambient.poizon.bean;

import java.io.Serializable;

public class DiscountCalculationBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String categoryName;
	private Long categoryId;
	private Long sale;
	private Double discount;
	private Double btlMrp;
	private Double totalDiscount;
	private Double discInPer;
	private String productName;
	private String packType;
	private Long packQty;
	private Long brandNo;
	private Double caseRate;
	private Double specialMargin;
	private Double brandNoPackQty;
	
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public Long getSale() {
		return sale;
	}
	public void setSale(Long sale) {
		this.sale = sale;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Double getBtlMrp() {
		return btlMrp;
	}
	public void setBtlMrp(Double btlMrp) {
		this.btlMrp = btlMrp;
	}
	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public Double getDiscInPer() {
		return discInPer;
	}
	public void setDiscInPer(Double discInPer) {
		this.discInPer = discInPer;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getPackType() {
		return packType;
	}
	public void setPackType(String packType) {
		this.packType = packType;
	}
	public Long getPackQty() {
		return packQty;
	}
	public void setPackQty(Long packQty) {
		this.packQty = packQty;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public Double getCaseRate() {
		return caseRate;
	}
	public void setCaseRate(Double caseRate) {
		this.caseRate = caseRate;
	}
	public Double getSpecialMargin() {
		return specialMargin;
	}
	public void setSpecialMargin(Double specialMargin) {
		this.specialMargin = specialMargin;
	}
	public Double getBrandNoPackQty() {
		return brandNoPackQty;
	}
	public void setBrandNoPackQty(Double brandNoPackQty) {
		this.brandNoPackQty = brandNoPackQty;
	}
	

}
