package com.zambient.poizon.bean;

import java.io.Serializable;

public class TurnovertaxBean implements Serializable{
	
	private Double brandNoPackQty;
	private Long stock;
	private String date;
	private Double invoiceRate;
	public Double getBrandNoPackQty() {
		return brandNoPackQty;
	}
	public void setBrandNoPackQty(Double brandNoPackQty) {
		this.brandNoPackQty = brandNoPackQty;
	}
	public Long getStock() {
		return stock;
	}
	public void setStock(Long stock) {
		this.stock = stock;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Double getInvoiceRate() {
		return invoiceRate;
	}
	public void setInvoiceRate(Double invoiceRate) {
		this.invoiceRate = invoiceRate;
	}
	@Override
	public String toString() {
		return "TurnovertaxBean [brandNoPackQty=" + brandNoPackQty + ", stock=" + stock + ", date=" + date
				+ ", invoiceRate=" + invoiceRate + "]";
	}

}
