package com.zambient.poizon.bean;

import java.io.Serializable;

public class DownloadTargetCase implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String month;
	private String brand;
	private Long targetCase;
	private Double discPerCase;
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public Long getTargetCase() {
		return targetCase;
	}
	public void setTargetCase(Long targetCase) {
		this.targetCase = targetCase;
	}
	public Double getDiscPerCase() {
		return discPerCase;
	}
	public void setDiscPerCase(Double discPerCase) {
		this.discPerCase = discPerCase;
	}
}
