package com.zambient.poizon.bean;

import java.io.Serializable;

public class VendorAdjustmentGetArrearsBean implements Serializable{
	private static final long serialVersionUID = 1L;
	private String companyName;
	private Double  vendorAmt;
	private Double arrears;
	private String date;
	private String comment;
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Double getVendorAmt() {
		return vendorAmt;
	}
	public void setVendorAmt(Double vendorAmt) {
		this.vendorAmt = vendorAmt;
	}
	public Double getArrears() {
		return arrears;
	}
	public void setArrears(Double arrears) {
		this.arrears = arrears;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Override
	public String toString() {
		return "VendorAdjustmentGetArrearsBean [companyName=" + companyName + ", vendorAmt=" + vendorAmt + ", arrears="
				+ arrears + ", date=" + date + ", comment=" + comment + "]";
	}
	

}
