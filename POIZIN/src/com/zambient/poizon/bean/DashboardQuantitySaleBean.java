package com.zambient.poizon.bean;

import java.io.Serializable;

public class DashboardQuantitySaleBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String quantity;
	private Long currentSale;
	private Long previousSale;
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public Long getCurrentSale() {
		return currentSale;
	}
	public void setCurrentSale(Long currentSale) {
		this.currentSale = currentSale;
	}
	public Long getPreviousSale() {
		return previousSale;
	}
	public void setPreviousSale(Long previousSale) {
		this.previousSale = previousSale;
	}

}
