package com.zambient.poizon.bean;

import java.io.Serializable;

public class InvoiceDateBean implements Serializable{
	@Override
	public String toString() {
		return "InvoiceDateBean [date=" + date + "]";
	}

	private static final long serialVersionUID = 1L;
	
	public String date;

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
