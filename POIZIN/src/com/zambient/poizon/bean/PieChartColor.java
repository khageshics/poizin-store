package com.zambient.poizon.bean;

public class PieChartColor {

	private String color;

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
}
