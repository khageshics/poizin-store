package com.zambient.poizon.bean;

import java.util.List;

public class SaleListBean {
	private String date;
	List<SaleBean> saleBean;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public List<SaleBean> getSaleBean() {
		return saleBean;
	}
	public void setSaleBean(List<SaleBean> saleBean) {
		this.saleBean = saleBean;
	}

}
