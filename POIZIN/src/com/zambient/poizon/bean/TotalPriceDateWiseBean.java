package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class TotalPriceDateWiseBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String label;
	private Double value;
	private Double previousSale;
	private Double saleInCase;
	private Double caseInPercentage;
	List <DashboardSaleBean> DashboardSaleBeanList;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Double getValue() {
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public Double getPreviousSale() {
		return previousSale;
	}
	public void setPreviousSale(Double previousSale) {
		this.previousSale = previousSale;
	}
	public List<DashboardSaleBean> getDashboardSaleBeanList() {
		return DashboardSaleBeanList;
	}
	public void setDashboardSaleBeanList(List<DashboardSaleBean> dashboardSaleBeanList) {
		DashboardSaleBeanList = dashboardSaleBeanList;
	}
	public Double getSaleInCase() {
		return saleInCase;
	}
	public void setSaleInCase(Double saleInCase) {
		this.saleInCase = saleInCase;
	}
	public Double getCaseInPercentage() {
		return caseInPercentage;
	}
	public void setCaseInPercentage(Double caseInPercentage) {
		this.caseInPercentage = caseInPercentage;
	}

}
