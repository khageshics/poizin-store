package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class UpdateExpensesBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	List<UpdateExpenseAndMasterExpense> updateExpenseAndMasterExpense;
	private Long expenseMasterId;
	private Double totalAmount;
	private String expenseMasterDate;
	public List<UpdateExpenseAndMasterExpense> getUpdateExpenseAndMasterExpense() {
		return updateExpenseAndMasterExpense;
	}
	public void setUpdateExpenseAndMasterExpense(List<UpdateExpenseAndMasterExpense> updateExpenseAndMasterExpense) {
		this.updateExpenseAndMasterExpense = updateExpenseAndMasterExpense;
	}
	public Long getExpenseMasterId() {
		return expenseMasterId;
	}
	public void setExpenseMasterId(Long expenseMasterId) {
		this.expenseMasterId = expenseMasterId;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getExpenseMasterDate() {
		return expenseMasterDate;
	}
	public void setExpenseMasterDate(String expenseMasterDate) {
		this.expenseMasterDate = expenseMasterDate;
	}
	
	

}
