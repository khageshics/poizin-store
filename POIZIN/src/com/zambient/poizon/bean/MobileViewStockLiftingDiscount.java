package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class MobileViewStockLiftingDiscount implements Serializable{
	
	private String company;
	private String color;
	private Long companyOrder;
	private Double duesAmt;
	private Double totalDiscount;
	private Double totalRentals;
	private Double totalVendorAmount;
	private Double notReceived;
	List<MobileViewStockDiscountInner> mobileViewStockDiscountInner;
	List<NotClearCheckBean> notClearCheckBean;
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public List<MobileViewStockDiscountInner> getMobileViewStockDiscountInner() {
		return mobileViewStockDiscountInner;
	}
	public void setMobileViewStockDiscountInner(List<MobileViewStockDiscountInner> mobileViewStockDiscountInner) {
		this.mobileViewStockDiscountInner = mobileViewStockDiscountInner;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Long getCompanyOrder() {
		return companyOrder;
	}
	public void setCompanyOrder(Long companyOrder) {
		this.companyOrder = companyOrder;
	}
	public Double getDuesAmt() {
		return duesAmt;
	}
	public void setDuesAmt(Double duesAmt) {
		this.duesAmt = duesAmt;
	}
	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public Double getTotalRentals() {
		return totalRentals;
	}
	public void setTotalRentals(Double totalRentals) {
		this.totalRentals = totalRentals;
	}
	public Double getTotalVendorAmount() {
		return totalVendorAmount;
	}
	public void setTotalVendorAmount(Double totalVendorAmount) {
		this.totalVendorAmount = totalVendorAmount;
	}
	public Double getNotReceived() {
		return notReceived;
	}
	public void setNotReceived(Double notReceived) {
		this.notReceived = notReceived;
	}
	public List<NotClearCheckBean> getNotClearCheckBean() {
		return notClearCheckBean;
	}
	public void setNotClearCheckBean(List<NotClearCheckBean> notClearCheckBean) {
		this.notClearCheckBean = notClearCheckBean;
	}
	
	
	

}
