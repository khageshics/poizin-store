package com.zambient.poizon.bean;

import java.io.Serializable;

public class CardCashSaleBeen implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Long id;
	private Double cardSale;
	private Double cashSale;
	private Double chequeSale;
	private String date;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Double getCardSale() {
		return cardSale;
	}
	public void setCardSale(Double cardSale) {
		this.cardSale = cardSale;
	}
	public Double getCashSale() {
		return cashSale;
	}
	public void setCashSale(Double cashSale) {
		this.cashSale = cashSale;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Double getChequeSale() {
		return chequeSale;
	}
	public void setChequeSale(Double chequeSale) {
		this.chequeSale = chequeSale;
	}
	@Override
	public String toString() {
		return "CardCashSaleBeen [id=" + id + ", cardSale=" + cardSale + ", cashSale=" + cashSale + ", chequeSale="
				+ chequeSale + ", date=" + date + "]";
	}

}
