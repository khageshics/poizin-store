package com.zambient.poizon.bean;

import java.io.Serializable;

public class InvoiceBo implements Serializable {
	
private static final long serialVersionUID = 1L;
	
	private String invoiceId;
	private String brandNoPackQty;
	private String caseQty;
	private String packQty;
	private String QtyBottels;
	private String SingleBottelRate;
	private String packQtyRate;
	private String totalPrice;
	private String invoiceDate;
	private String EachBottleMrp;
	private String saleId;
	public String getEachBottleMrp() {
		return EachBottleMrp;
	}
	public void setEachBottleMrp(String eachBottleMrp) {
		EachBottleMrp = eachBottleMrp;
	}
	public String getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(String invoiceId) {
		this.invoiceId = invoiceId;
	}
	public String getBrandNoPackQty() {
		return brandNoPackQty;
	}
	public void setBrandNoPackQty(String brandNoPackQty) {
		this.brandNoPackQty = brandNoPackQty;
	}
	public String getCaseQty() {
		return caseQty;
	}
	public void setCaseQty(String caseQty) {
		this.caseQty = caseQty;
	}
	public String getPackQty() {
		return packQty;
	}
	public void setPackQty(String packQty) {
		this.packQty = packQty;
	}
	public String getQtyBottels() {
		return QtyBottels;
	}
	public void setQtyBottels(String qtyBottels) {
		QtyBottels = qtyBottels;
	}
	public String getSingleBottelRate() {
		return SingleBottelRate;
	}
	public void setSingleBottelRate(String singleBottelRate) {
		SingleBottelRate = singleBottelRate;
	}
	public String getPackQtyRate() {
		return packQtyRate;
	}
	public void setPackQtyRate(String packQtyRate) {
		this.packQtyRate = packQtyRate;
	}
	public String getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(String totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getSaleId() {
		return saleId;
	}
	public void setSaleId(String saleId) {
		this.saleId = saleId;
	}
	@Override
	public String toString() {
		return "InvoiceBo [invoiceId=" + invoiceId + ", brandNoPackQty=" + brandNoPackQty + ", caseQty=" + caseQty
				+ ", packQty=" + packQty + ", QtyBottels=" + QtyBottels + ", SingleBottelRate=" + SingleBottelRate
				+ ", packQtyRate=" + packQtyRate + ", totalPrice=" + totalPrice + ", invoiceDate=" + invoiceDate + "]";
	}

}
