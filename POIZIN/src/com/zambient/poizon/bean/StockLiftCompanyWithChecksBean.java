package com.zambient.poizon.bean;

import java.util.List;

public class StockLiftCompanyWithChecksBean {
	private static final long serialVersionUID = 1L;
	
	private Long companyId;
	private String company;
	private String months;
	private Double totalDiscount;
	private Long companyOrder;
	private Double arrears;
	private Double rental;
	private String color;
	private String enterprise;
	private Double checkAmt;
	private Double adjAmt;
	List<DiscountTransactionBean> discountTransactionBeanList;
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getMonths() {
		return months;
	}
	public void setMonths(String months) {
		this.months = months;
	}
	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public Long getCompanyOrder() {
		return companyOrder;
	}
	public void setCompanyOrder(Long companyOrder) {
		this.companyOrder = companyOrder;
	}
	public Double getArrears() {
		return arrears;
	}
	public void setArrears(Double arrears) {
		this.arrears = arrears;
	}
	public Double getRental() {
		return rental;
	}
	public void setRental(Double rental) {
		this.rental = rental;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public List<DiscountTransactionBean> getDiscountTransactionBeanList() {
		return discountTransactionBeanList;
	}
	public void setDiscountTransactionBeanList(List<DiscountTransactionBean> discountTransactionBeanList) {
		this.discountTransactionBeanList = discountTransactionBeanList;
	}
	public String getEnterprise() {
		return enterprise;
	}
	public void setEnterprise(String enterprise) {
		this.enterprise = enterprise;
	}
	public Double getCheckAmt() {
		return checkAmt;
	}
	public void setCheckAmt(Double checkAmt) {
		this.checkAmt = checkAmt;
	}
	public Double getAdjAmt() {
		return adjAmt;
	}
	public void setAdjAmt(Double adjAmt) {
		this.adjAmt = adjAmt;
	}
	

}
