package com.zambient.poizon.bean;

import java.io.Serializable;

public class InventorBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long inventoryId;
	private Double brandNo;
	private String brandName;
	private String productType;
	private String packType;
	private Long packQty;
	private String qty;
	private Double unitRate;
	private Double btlRate;
	public Long getInventoryId() {
		return inventoryId;
	}
	public void setInventoryId(Long inventoryId) {
		this.inventoryId = inventoryId;
	}
	public Double getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Double brandNo) {
		this.brandNo = brandNo;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getPackType() {
		return packType;
	}
	public void setPackType(String packType) {
		this.packType = packType;
	}
	public Long getPackQty() {
		return packQty;
	}
	public void setPackQty(Long packQty) {
		this.packQty = packQty;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}
	public Double getUnitRate() {
		return unitRate;
	}
	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}
	public Double getBtlRate() {
		return btlRate;
	}
	public void setBtlRate(Double btlRate) {
		this.btlRate = btlRate;
	}
	@Override
	public String toString() {
		return "InventorBean [inventoryId=" + inventoryId + ", brandNo=" + brandNo + ", brandName=" + brandName
				+ ", productType=" + productType + ", packType=" + packType + ", packQty=" + packQty + ", qty=" + qty
				+ ", unitRate=" + unitRate + ", btlRate=" + btlRate + "]";
	}

}
