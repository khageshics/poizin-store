package com.zambient.poizon.bean;

import java.io.Serializable;

public class NewMrpRateBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private Long productId;
	private Double brandNoPackQty;
	private Long brandNo;
	private String brandName;
	private String productType;
	private String quantity; 
	private Long packQty;
	private String packType;
	private Double unitPrice;
	private Double singleBottelPrice;
	private Long QtyBottels;
	private Double bottleSaleMrp;
	private String category;
	private String company;
	private Double newBottleSaleMrp;
	private Double newSingleBottelPrice;
	private Double newUnitPrice;
	private String totalOldSaleMrp;
	private String color;
	private Long match;
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Long getCompanyOrder() {
		return companyOrder;
	}
	public void setCompanyOrder(Long companyOrder) {
		this.companyOrder = companyOrder;
	}
	private Long companyOrder;
	
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Double getBrandNoPackQty() {
		return brandNoPackQty;
	}
	public void setBrandNoPackQty(Double brandNoPackQty) {
		this.brandNoPackQty = brandNoPackQty;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public Long getPackQty() {
		return packQty;
	}
	public void setPackQty(Long packQty) {
		this.packQty = packQty;
	}
	public String getPackType() {
		return packType;
	}
	public void setPackType(String packType) {
		this.packType = packType;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Double getSingleBottelPrice() {
		return singleBottelPrice;
	}
	public void setSingleBottelPrice(Double singleBottelPrice) {
		this.singleBottelPrice = singleBottelPrice;
	}
	public Long getQtyBottels() {
		return QtyBottels;
	}
	public void setQtyBottels(Long qtyBottels) {
		QtyBottels = qtyBottels;
	}
	public Double getBottleSaleMrp() {
		return bottleSaleMrp;
	}
	public void setBottleSaleMrp(Double bottleSaleMrp) {
		this.bottleSaleMrp = bottleSaleMrp;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public Double getNewBottleSaleMrp() {
		return newBottleSaleMrp;
	}
	public String getTotalOldSaleMrp() {
		return totalOldSaleMrp;
	}
	public void setTotalOldSaleMrp(String totalOldSaleMrp) {
		this.totalOldSaleMrp = totalOldSaleMrp;
	}
	public void setNewBottleSaleMrp(Double newBottleSaleMrp) {
		this.newBottleSaleMrp = newBottleSaleMrp;
	}
	public Double getNewSingleBottelPrice() {
		return newSingleBottelPrice;
	}
	public void setNewSingleBottelPrice(Double newSingleBottelPrice) {
		this.newSingleBottelPrice = newSingleBottelPrice;
	}
	public Double getNewUnitPrice() {
		return newUnitPrice;
	}
	public void setNewUnitPrice(Double newUnitPrice) {
		this.newUnitPrice = newUnitPrice;
	}
	public Long getMatch() {
		return match;
	}
	public void setMatch(Long match) {
		this.match = match;
	}
	
	
}
