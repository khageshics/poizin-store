package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class ProductPerformanceWithMultipleBrands implements Serializable{
	
	List<ProductPerformanceWithBrandName> productPerformanceWithBrandName;

	public List<ProductPerformanceWithBrandName> getProductPerformanceWithBrandName() {
		return productPerformanceWithBrandName;
	}

	public void setProductPerformanceWithBrandName(List<ProductPerformanceWithBrandName> productPerformanceWithBrandName) {
		this.productPerformanceWithBrandName = productPerformanceWithBrandName;
	}

}
