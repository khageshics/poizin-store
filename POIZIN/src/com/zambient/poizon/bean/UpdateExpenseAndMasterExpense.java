package com.zambient.poizon.bean;

import java.io.Serializable;

public class UpdateExpenseAndMasterExpense implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Long expenseChildId;
	private Long categoryId;
	private Long expenseMasterId;
	private Double expenseChildAmount;
	private String comment;
	private String expenseName;
	public Long getExpenseChildId() {
		return expenseChildId;
	}
	public void setExpenseChildId(Long expenseChildId) {
		this.expenseChildId = expenseChildId;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public Long getExpenseMasterId() {
		return expenseMasterId;
	}
	public void setExpenseMasterId(Long expenseMasterId) {
		this.expenseMasterId = expenseMasterId;
	}
	public Double getExpenseChildAmount() {
		return expenseChildAmount;
	}
	public void setExpenseChildAmount(Double expenseChildAmount) {
		this.expenseChildAmount = expenseChildAmount;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getExpenseName() {
		return expenseName;
	}
	public void setExpenseName(String expenseName) {
		this.expenseName = expenseName;
	}

}
