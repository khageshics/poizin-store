package com.zambient.poizon.bean;

import java.io.Serializable;

public class CommentForSaleBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String commentDate;
	private String saleComment;
	
	public String getCommentDate() {
		return commentDate;
	}
	public void setCommentDate(String commentDate) {
		this.commentDate = commentDate;
	}
	public String getSaleComment() {
		return saleComment;
	}
	public void setSaleComment(String saleComment) {
		this.saleComment = saleComment;
	}
	@Override
	public String toString() {
		return "CommentForSaleBean [commentDate=" + commentDate + ", saleComment=" + saleComment + "]";
	}
	
	
}
