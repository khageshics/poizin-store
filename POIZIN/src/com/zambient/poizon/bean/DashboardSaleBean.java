package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class DashboardSaleBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String shortName;
	private Long brandNo;
	private Long currentSale;
	private Long previousSale;
	private List<DashboardQuantitySaleBean> dashboardQuantitySaleBean; 
	private Double amount;
	private Double saleInCase;
	private Double caseInPercentage;
	
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public Long getCurrentSale() {
		return currentSale;
	}
	public void setCurrentSale(Long currentSale) {
		this.currentSale = currentSale;
	}
	public Long getPreviousSale() {
		return previousSale;
	}
	public void setPreviousSale(Long previousSale) {
		this.previousSale = previousSale;
	}
	public List<DashboardQuantitySaleBean> getDashboardQuantitySaleBean() {
		return dashboardQuantitySaleBean;
	}
	public void setDashboardQuantitySaleBean(List<DashboardQuantitySaleBean> dashboardQuantitySaleBean) {
		this.dashboardQuantitySaleBean = dashboardQuantitySaleBean;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Double getSaleInCase() {
		return saleInCase;
	}
	public void setSaleInCase(Double saleInCase) {
		this.saleInCase = saleInCase;
	}
	public Double getCaseInPercentage() {
		return caseInPercentage;
	}
	public void setCaseInPercentage(Double caseInPercentage) {
		this.caseInPercentage = caseInPercentage;
	}
	

}
