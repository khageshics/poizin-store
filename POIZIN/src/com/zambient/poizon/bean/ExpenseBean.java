package com.zambient.poizon.bean;

import java.io.Serializable;

public class ExpenseBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Long categoryId;
	private Double expenseChildAmount;
	private String comment;
	private String date;
	private String name;
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public Double getExpenseChildAmount() {
		return expenseChildAmount;
	}
	public void setExpenseChildAmount(Double expenseChildAmount) {
		this.expenseChildAmount = expenseChildAmount;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
