package com.zambient.poizon.bean;

import java.io.Serializable;

public class BalanceSheetBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String date;
	private Double totalPrice;
	private Double totalAmount;
	private Double cradSale;
	private Double cashSale;
	private Double chequeSale;
	private Long expenseMasterId;
	private Double creditedAmount;
	private Double retention;
	private String day;
	private Double investment;
	
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public Long getExpenseMasterId() {
		return expenseMasterId;
	}
	public void setExpenseMasterId(Long expenseMasterId) {
		this.expenseMasterId = expenseMasterId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	public Double getCradSale() {
		return cradSale;
	}
	public void setCradSale(Double cradSale) {
		this.cradSale = cradSale;
	}
	public Double getCashSale() {
		return cashSale;
	}
	public void setCashSale(Double cashSale) {
		this.cashSale = cashSale;
	}
	public Double getChequeSale() {
		return chequeSale;
	}
	public void setChequeSale(Double chequeSale) {
		this.chequeSale = chequeSale;
	}
	public Double getCreditedAmount() {
		return creditedAmount;
	}
	public void setCreditedAmount(Double creditedAmount) {
		this.creditedAmount = creditedAmount;
	}
	public Double getRetention() {
		return retention;
	}
	public void setRetention(Double retention) {
		this.retention = retention;
	}
	public Double getInvestment() {
		return investment;
	}
	public void setInvestment(Double investment) {
		this.investment = investment;
	}

}
