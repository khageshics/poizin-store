package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class EditProductBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private Long brandNo,matchValue,companyId,categoryId,packQty,groupId;
	private String brandName,productType,quantity,packType,categoryName,companyName,shortBrandName,matchName,groupName;
	private Double margin,brandNoPackQty;
	private List<String> producttype,packtype,quantities;
	private List<Long> packqty;
	private List<companyBean> companyList;
	private List<companyBean> categoryList;
	private List<companyBean> groupingList;
	
	public Double getBrandNoPackQty() {
		return brandNoPackQty;
	}
	public void setBrandNoPackQty(Double brandNoPackQty) {
		this.brandNoPackQty = brandNoPackQty;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public Long getMatchValue() {
		return matchValue;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public Long getPackQty() {
		return packQty;
	}
	public String getBrandName() {
		return brandName;
	}
	public String getProductType() {
		return productType;
	}
	public String getQuantity() {
		return quantity;
	}
	public String getPackType() {
		return packType;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public String getShortBrandName() {
		return shortBrandName;
	}
	public String getMatchName() {
		return matchName;
	}
	public Double getMargin() {
		return margin;
	}
	public List<String> getProducttype() {
		return producttype;
	}
	public List<String> getPacktype() {
		return packtype;
	}
	public List<String> getQuantities() {
		return quantities;
	}
	public List<Long> getPackqty() {
		return packqty;
	}
	public List<companyBean> getCompanyList() {
		return companyList;
	}
	public List<companyBean> getCategoryList() {
		return categoryList;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public void setMatchValue(Long matchValue) {
		this.matchValue = matchValue;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public void setPackQty(Long packQty) {
		this.packQty = packQty;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public void setPackType(String packType) {
		this.packType = packType;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public void setShortBrandName(String shortBrandName) {
		this.shortBrandName = shortBrandName;
	}
	public void setMatchName(String matchName) {
		this.matchName = matchName;
	}
	public void setMargin(Double margin) {
		this.margin = margin;
	}
	public void setProducttype(List<String> producttype) {
		this.producttype = producttype;
	}
	public void setPacktype(List<String> packtype) {
		this.packtype = packtype;
	}
	public void setQuantities(List<String> quantities) {
		this.quantities = quantities;
	}
	public void setPackqty(List<Long> packqty) {
		this.packqty = packqty;
	}
	public void setCompanyList(List<companyBean> companyList) {
		this.companyList = companyList;
	}
	public void setCategoryList(List<companyBean> categoryList) {
		this.categoryList = categoryList;
	}
	public List<companyBean> getGroupingList() {
		return groupingList;
	}
	public void setGroupingList(List<companyBean> groupingList) {
		this.groupingList = groupingList;
	}
	public Long getGroupId() {
		return groupId;
	}
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	

}
