package com.zambient.poizon.bean;

import java.io.Serializable;

public class SaleBean implements Serializable{
private static final long serialVersionUID = 1L;
	
	private Long productId;
	private Double brandNoPackQty;
	private Long brandNo;
	private String brandName;
	private String productType;
	private String quantity; 
	private Long packQty;
	private String packType;
	private Long saleId;
	private String salePrimaryKey;
	private Long closing;
	private Double unitPrice;
	private Double totalPrice;
	private Long opening;
	private Double singleBottelPrice;
	private Long totalSale;
	private String saleDate;
	private Long invoiceId;
	private Long caseQty;
	private String invoiceDate;
	private Long QtyBottels;
	private Long received;
	private Double bottleSaleMrp;
	private Double cummulativePrice;
	private String category;
	private String company;
	private String color;
	private Long categoryOrder;
	private Long companyOrder;
	private Long noOfReturnsBtl;
	private String invoiceDateAsCopy;
	private Double target;
	private Double lastMonthSold;
	private Double currentMonthSold;
	private boolean conditonVal;
	
	public String getInvoiceDateAsCopy() {
		return invoiceDateAsCopy;
	}
	public void setInvoiceDateAsCopy(String invoiceDateAsCopy) {
		this.invoiceDateAsCopy = invoiceDateAsCopy;
	}
	public Double getBottleSaleMrp() {
		return bottleSaleMrp;
	}
	public void setBottleSaleMrp(Double bottleSaleMrp) {
		this.bottleSaleMrp = bottleSaleMrp;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Double getBrandNoPackQty() {
		return brandNoPackQty;
	}
	public void setBrandNoPackQty(Double brandNoPackQty) {
		this.brandNoPackQty = brandNoPackQty;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public Long getPackQty() {
		return packQty;
	}
	public void setPackQty(Long packQty) {
		this.packQty = packQty;
	}
	public String getPackType() {
		return packType;
	}
	public void setPackType(String packType) {
		this.packType = packType;
	}
	public Long getSaleId() {
		return saleId;
	}
	public void setSaleId(Long saleId) {
		this.saleId = saleId;
	}
	public String getSalePrimaryKey() {
		return salePrimaryKey;
	}
	public void setSalePrimaryKey(String salePrimaryKey) {
		this.salePrimaryKey = salePrimaryKey;
	}
	public Long getClosing() {
		return closing;
	}
	public void setClosing(Long closing) {
		this.closing = closing;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public Double getLastMonthSold() {
		return lastMonthSold;
	}
	public void setLastMonthSold(Double lastMonthSold) {
		this.lastMonthSold = lastMonthSold;
	}
	public Double getTarget() {
		return target;
	}
	public void setTarget(Double target) {
		this.target = target;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Long getOpening() {
		return opening;
	}
	public Long getNoOfReturnsBtl() {
		return noOfReturnsBtl;
	}
	public void setNoOfReturnsBtl(Long noOfReturnsBtl) {
		this.noOfReturnsBtl = noOfReturnsBtl;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Long getCaseQty() {
		return caseQty;
	}
	public void setCaseQty(Long caseQty) {
		this.caseQty = caseQty;
	}
	public Long getReceived() {
		return received;
	}
	public void setReceived(Long received) {
		this.received = received;
	}
	public String getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public Long getQtyBottels() {
		return QtyBottels;
	}
	public void setQtyBottels(Long qtyBottels) {
		QtyBottels = qtyBottels;
	}
	public void setOpening(Long opening) {
		this.opening = opening;
	}
	public Double getSingleBottelPrice() {
		return singleBottelPrice;
	}
	public Long getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}
	public void setSingleBottelPrice(Double singleBottelPrice) {
		this.singleBottelPrice = singleBottelPrice;
	}
	public Long getTotalSale() {
		return totalSale;
	}
	public void setTotalSale(Long totalSale) {
		this.totalSale = totalSale;
	}
	public String getSaleDate() {
		return saleDate;
	}
	public void setSaleDate(String saleDate) {
		this.saleDate = saleDate;
	}
	public Double getCummulativePrice() {
		return cummulativePrice;
	}
	public void setCummulativePrice(Double cummulativePrice) {
		this.cummulativePrice = cummulativePrice;
	}
	public Long getCategoryOrder() {
		return categoryOrder;
	}
	public void setCategoryOrder(Long categoryOrder) {
		this.categoryOrder = categoryOrder;
	}
	public Long getCompanyOrder() {
		return companyOrder;
	}
	public void setCompanyOrder(Long companyOrder) {
		this.companyOrder = companyOrder;
	}
	
	public Double getCurrentMonthSold() {
		return currentMonthSold;
	}
	public void setCurrentMonthSold(Double currentMonthSold) {
		this.currentMonthSold = currentMonthSold;
	}
	public boolean isConditonVal() {
		return conditonVal;
	}
	public void setConditonVal(boolean conditonVal) {
		this.conditonVal = conditonVal;
	}
	
}
