package com.zambient.poizon.bean;

import java.io.Serializable;

public class ProductPerformanceWithComaparisonBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String brandName;
    private Double caseSold;
    private Double caseRate;
    private Double costPrice;
    private Double govtMaegin;
    private Double companyDiscount;
    private Double totalDiscount;
    private Long brandNo;
    private Double brandNopackQty;
    private Double totalPrice;
    private Long packQty;
    private Long totalSale;
	
    public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public Double getCaseSold() {
		return caseSold;
	}
	public void setCaseSold(Double caseSold) {
		this.caseSold = caseSold;
	}
	public Double getCaseRate() {
		return caseRate;
	}
	public void setCaseRate(Double caseRate) {
		this.caseRate = caseRate;
	}
	public Double getCostPrice() {
		return costPrice;
	}
	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}
	public Double getGovtMaegin() {
		return govtMaegin;
	}
	public void setGovtMaegin(Double govtMaegin) {
		this.govtMaegin = govtMaegin;
	}
	public Double getCompanyDiscount() {
		return companyDiscount;
	}
	public void setCompanyDiscount(Double companyDiscount) {
		this.companyDiscount = companyDiscount;
	}
	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public Double getBrandNopackQty() {
		return brandNopackQty;
	}
	public void setBrandNopackQty(Double brandNopackQty) {
		this.brandNopackQty = brandNopackQty;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Long getPackQty() {
		return packQty;
	}
	public void setPackQty(Long packQty) {
		this.packQty = packQty;
	}
	public Long getTotalSale() {
		return totalSale;
	}
	public void setTotalSale(Long totalSale) {
		this.totalSale = totalSale;
	}

}
