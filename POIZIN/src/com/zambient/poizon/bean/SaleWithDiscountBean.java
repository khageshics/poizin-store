package com.zambient.poizon.bean;

public class SaleWithDiscountBean {
	
	private Long BrandNo;
	private String BrandName;
	private Double BrandNoPackQty;
	private Double discount;
	private Long companyOrder;
	private String company;
	private String category;
	private String color;
	private Long totalSale;
	private String Quantity;
	private Double totalPrice;
	private Long packQty;
	private Double govtDiscount;
	private String discountMonth;
	private Long companyId;
	private Long categoryId;
	private String packType;
	private String categoryColor;
	private Double costPrice;
	private Long categoryOrder;
	
	public Double getCostPrice() {
		return costPrice;
	}
	public void setCostPrice(Double costPrice) {
		this.costPrice = costPrice;
	}
	public Long getBrandNo() {
		return BrandNo;
	}
	public void setBrandNo(Long brandNo) {
		BrandNo = brandNo;
	}
	public String getBrandName() {
		return BrandName;
	}
	public void setBrandName(String brandName) {
		BrandName = brandName;
	}
	public Double getBrandNoPackQty() {
		return BrandNoPackQty;
	}
	public void setBrandNoPackQty(Double brandNoPackQty) {
		BrandNoPackQty = brandNoPackQty;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Long getCompanyOrder() {
		return companyOrder;
	}
	public void setCompanyOrder(Long companyOrder) {
		this.companyOrder = companyOrder;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Long getTotalSale() {
		return totalSale;
	}
	public void setTotalSale(Long totalSale) {
		this.totalSale = totalSale;
	}
	public String getQuantity() {
		return Quantity;
	}
	public void setQuantity(String quantity) {
		Quantity = quantity;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Long getPackQty() {
		return packQty;
	}
	public void setPackQty(Long packQty) {
		this.packQty = packQty;
	}
	public Double getGovtDiscount() {
		return govtDiscount;
	}
	public void setGovtDiscount(Double govtDiscount) {
		this.govtDiscount = govtDiscount;
	}
	public String getDiscountMonth() {
		return discountMonth;
	}
	public void setDiscountMonth(String discountMonth) {
		this.discountMonth = discountMonth;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getPackType() {
		return packType;
	}
	public void setPackType(String packType) {
		this.packType = packType;
	}
	public String getCategoryColor() {
		return categoryColor;
	}
	public void setCategoryColor(String categoryColor) {
		this.categoryColor = categoryColor;
	}
	public Long getCategoryOrder() {
		return categoryOrder;
	}
	public void setCategoryOrder(Long categoryOrder) {
		this.categoryOrder = categoryOrder;
	}

}
