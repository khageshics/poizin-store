package com.zambient.poizon.bean;

public class DiscountTransactionBean {
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Long companyId;
	private String company;
	private String months;
	private String realMonth;
	private String TransactionType;
	private Double transactionAmt;
	private String transactionDate;
	private Double rental;
	private Double adjCheck;
	private String ngalleryImage;
	private String imageName;
	private String enterprise;
	private Double totalDiscount;
	private Long companyOrder;
	private Double arrears;
	private String bank;
	private String checkNo;
	private String fromBank;
	private String comment;
	private boolean received;
	private int checkBox;
	
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getMonths() {
		return months;
	}
	public void setMonths(String months) {
		this.months = months;
	}
	public String getTransactionType() {
		return TransactionType;
	}
	public void setTransactionType(String transactionType) {
		TransactionType = transactionType;
	}
	public Double getTransactionAmt() {
		return transactionAmt;
	}
	public void setTransactionAmt(Double transactionAmt) {
		this.transactionAmt = transactionAmt;
	}
	public String getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}
	public Double getRental() {
		return rental;
	}
	public void setRental(Double rental) {
		this.rental = rental;
	}
	public Double getAdjCheck() {
		return adjCheck;
	}
	public void setAdjCheck(Double adjCheck) {
		this.adjCheck = adjCheck;
	}
	public String getNgalleryImage() {
		return ngalleryImage;
	}
	public void setNgalleryImage(String ngalleryImage) {
		this.ngalleryImage = ngalleryImage;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public String getRealMonth() {
		return realMonth;
	}
	public void setRealMonth(String realMonth) {
		this.realMonth = realMonth;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public String getEnterprise() {
		return enterprise;
	}
	public void setEnterprise(String enterprise) {
		this.enterprise = enterprise;
	}
	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public Long getCompanyOrder() {
		return companyOrder;
	}
	public void setCompanyOrder(Long companyOrder) {
		this.companyOrder = companyOrder;
	}
	public Double getArrears() {
		return arrears;
	}
	public void setArrears(Double arrears) {
		this.arrears = arrears;
	}
	public String getBank() {
		return bank;
	}
	public void setBank(String bank) {
		this.bank = bank;
	}
	public String getCheckNo() {
		return checkNo;
	}
	public void setCheckNo(String checkNo) {
		this.checkNo = checkNo;
	}
	public String getFromBank() {
		return fromBank;
	}
	public void setFromBank(String fromBank) {
		this.fromBank = fromBank;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public boolean isReceived() {
		return received;
	}
	public void setReceived(boolean received) {
		this.received = received;
	}
	public int getCheckBox() {
		return checkBox;
	}
	public void setCheckBox(int checkBox) {
		this.checkBox = checkBox;
	}
	@Override
	public String toString() {
		return "DiscountTransactionBean [id=" + id + ", companyId=" + companyId + ", company=" + company + ", months="
				+ months + ", realMonth=" + realMonth + ", TransactionType=" + TransactionType + ", transactionAmt="
				+ transactionAmt + ", transactionDate=" + transactionDate + ", rental=" + rental + ", adjCheck="
				+ adjCheck + ", ngalleryImage=" + ngalleryImage + ", imageName=" + imageName + ", enterprise="
				+ enterprise + ", totalDiscount=" + totalDiscount + ", companyOrder=" + companyOrder + ", arrears="
				+ arrears + ", bank=" + bank + ", checkNo=" + checkNo + ", fromBank=" + fromBank + ", comment="
				+ comment + ", received=" + received + ", checkBox=" + checkBox + "]";
	}

}
