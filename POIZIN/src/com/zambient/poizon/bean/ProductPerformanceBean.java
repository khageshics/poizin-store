package com.zambient.poizon.bean;

import java.io.Serializable;

public class ProductPerformanceBean implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String date;
	private Double casesSold;
	private Double caseRate;
	private Double totalPrice;
	private Double govtProfit;
	private Double discountAmount;
	private Double stockLiftDiscount;
	private Double totalProductDiscount;
	private Long sale;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Double getCasesSold() {
		return casesSold;
	}
	public void setCasesSold(Double casesSold) {
		this.casesSold = casesSold;
	}
	public Double getCaseRate() {
		return caseRate;
	}
	public void setCaseRate(Double caseRate) {
		this.caseRate = caseRate;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Double getGovtProfit() {
		return govtProfit;
	}
	public void setGovtProfit(Double govtProfit) {
		this.govtProfit = govtProfit;
	}
	public Double getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(Double discountAmount) {
		this.discountAmount = discountAmount;
	}
	public Double getStockLiftDiscount() {
		return stockLiftDiscount;
	}
	public void setStockLiftDiscount(Double stockLiftDiscount) {
		this.stockLiftDiscount = stockLiftDiscount;
	}
	public Double getTotalProductDiscount() {
		return totalProductDiscount;
	}
	public void setTotalProductDiscount(Double totalProductDiscount) {
		this.totalProductDiscount = totalProductDiscount;
	}
	public Long getSale() {
		return sale;
	}
	public void setSale(Long sale) {
		this.sale = sale;
	}
	

}
