package com.zambient.poizon.bean;

import java.io.Serializable;

public class MaxMinDiscountBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Double maxDisc;
	private Double minDisc;
	private Double prevoiusDisc;
	private Long maxDiscCase;
	private Long minDiscCase;
	private Long previousDiscCase;
	private String minDiscDate;
	private String maxDiscDate;
	private String previousDiscDate;
	
	public Double getMaxDisc() {
		return maxDisc;
	}
	public void setMaxDisc(Double maxDisc) {
		this.maxDisc = maxDisc;
	}
	public Double getMinDisc() {
		return minDisc;
	}
	public void setMinDisc(Double minDisc) {
		this.minDisc = minDisc;
	}
	public Double getPrevoiusDisc() {
		return prevoiusDisc;
	}
	public void setPrevoiusDisc(Double prevoiusDisc) {
		this.prevoiusDisc = prevoiusDisc;
	}
	public Long getMaxDiscCase() {
		return maxDiscCase;
	}
	public void setMaxDiscCase(Long maxDiscCase) {
		this.maxDiscCase = maxDiscCase;
	}
	public Long getMinDiscCase() {
		return minDiscCase;
	}
	public void setMinDiscCase(Long minDiscCase) {
		this.minDiscCase = minDiscCase;
	}
	public Long getPreviousDiscCase() {
		return previousDiscCase;
	}
	public void setPreviousDiscCase(Long previousDiscCase) {
		this.previousDiscCase = previousDiscCase;
	}
	public String getMinDiscDate() {
		return minDiscDate;
	}
	public void setMinDiscDate(String minDiscDate) {
		this.minDiscDate = minDiscDate;
	}
	public String getMaxDiscDate() {
		return maxDiscDate;
	}
	public void setMaxDiscDate(String maxDiscDate) {
		this.maxDiscDate = maxDiscDate;
	}
	public String getPreviousDiscDate() {
		return previousDiscDate;
	}
	public void setPreviousDiscDate(String previousDiscDate) {
		this.previousDiscDate = previousDiscDate;
	}
	@Override
	public String toString() {
		return "MaxMinDiscountBean [maxDisc=" + maxDisc + ", minDisc=" + minDisc + ", prevoiusDisc=" + prevoiusDisc
				+ ", maxDiscCase=" + maxDiscCase + ", minDiscCase=" + minDiscCase + ", previousDiscCase="
				+ previousDiscCase + ", minDiscDate=" + minDiscDate + ", maxDiscDate=" + maxDiscDate
				+ ", previousDiscDate=" + previousDiscDate + "]";
	}
	

}
