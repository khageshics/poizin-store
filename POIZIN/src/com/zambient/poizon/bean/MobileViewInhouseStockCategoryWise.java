package com.zambient.poizon.bean;

import java.io.Serializable;

public class MobileViewInhouseStockCategoryWise implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String Quantity;
	private Long caseQty;
	private Long bottle;
	private Long closing;
	public String getQuantity() {
		return Quantity;
	}
	public void setQuantity(String quantity) {
		Quantity = quantity;
	}
	public Long getCaseQty() {
		return caseQty;
	}
	public void setCaseQty(Long caseQty) {
		this.caseQty = caseQty;
	}
	public Long getBottle() {
		return bottle;
	}
	public void setBottle(Long bottle) {
		this.bottle = bottle;
	}
	public Long getClosing() {
		return closing;
	}
	public void setClosing(Long closing) {
		this.closing = closing;
	}
	

}
