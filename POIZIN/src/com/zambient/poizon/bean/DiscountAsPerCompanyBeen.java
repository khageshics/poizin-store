package com.zambient.poizon.bean;

import java.io.Serializable;

public class DiscountAsPerCompanyBeen 	implements Serializable{
		
		private static final long serialVersionUID = 1L;
		
		private Long company;
		private Long BrandNo;
		private Double arrears;
		private String companyName;
		private Double discountAmount;
		private String color;
		private Double chequeAmount;
		private Double rentals;
		private String comment;
		private String enterprise;
		private Double adjustmentCheque;
		private Double creditAmt;
		
		public Double getRentals() {
			return rentals;
		}
		public void setRentals(Double rentals) {
			this.rentals = rentals;
		}
		public Long getCompany() {
			return company;
		}
		public void setCompany(Long company) {
			this.company = company;
		}
		public Long getBrandNo() {
			return BrandNo;
		}
		public void setBrandNo(Long brandNo) {
			BrandNo = brandNo;
		}
		public Double getArrears() {
			return arrears;
		}
		public void setArrears(Double arrears) {
			this.arrears = arrears;
		}
		public String getCompanyName() {
			return companyName;
		}
		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}
		public Double getDiscountAmount() {
			return discountAmount;
		}
		public void setDiscountAmount(Double discountAmount) {
			this.discountAmount = discountAmount;
		}
		public String getColor() {
			return color;
		}
		public void setColor(String color) {
			this.color = color;
		}
		public Double getChequeAmount() {
			return chequeAmount;
		}
		public void setChequeAmount(Double chequeAmount) {
			this.chequeAmount = chequeAmount;
		}
		public String getComment() {
			return comment;
		}
		public void setComment(String comment) {
			this.comment = comment;
		}
		public String getEnterprise() {
			return enterprise;
		}
		public void setEnterprise(String enterprise) {
			this.enterprise = enterprise;
		}
		public Double getAdjustmentCheque() {
			return adjustmentCheque;
		}
		public void setAdjustmentCheque(Double adjustmentCheque) {
			this.adjustmentCheque = adjustmentCheque;
		}
		public Double getCreditAmt() {
			return creditAmt;
		}
		public void setCreditAmt(Double creditAmt) {
			this.creditAmt = creditAmt;
		}
}
