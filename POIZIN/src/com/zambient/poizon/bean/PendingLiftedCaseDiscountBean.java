package com.zambient.poizon.bean;

import java.io.Serializable;

public class PendingLiftedCaseDiscountBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String brandName;
	private Long cases;
	private Long brandNo;
	private String companyName;
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public Long getCases() {
		return cases;
	}
	public void setCases(Long cases) {
		this.cases = cases;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	@Override
	public String toString() {
		return "PendingLiftedCaseDiscountBean [brandName=" + brandName + ", cases=" + cases + ", brandNo=" + brandNo
				+ ", companyName=" + companyName + "]";
	}


}
