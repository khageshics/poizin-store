package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class ProfitAndLossBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Double openingStock;
	private Double closingStock;
	private Double sales;
	private Double mrpRoundOff;
	private Double purchase;
	private List<AdministrationExpensesBean> administrationExpensesBean;
	private Double receivedAmt;
	private Double turnOverTax;
	private Double breackage;
	private Double licenseFee;
	private Double sittingAmt;
	private Double tcsAmt;
	private Double monthlyExpenses;
	
	public Double getOpeningStock() {
		return openingStock;
	}
	public void setOpeningStock(Double openingStock) {
		this.openingStock = openingStock;
	}
	public Double getClosingStock() {
		return closingStock;
	}
	public void setClosingStock(Double closingStock) {
		this.closingStock = closingStock;
	}
	public Double getSales() {
		return sales;
	}
	public void setSales(Double sales) {
		this.sales = sales;
	}
	public Double getMrpRoundOff() {
		return mrpRoundOff;
	}
	public void setMrpRoundOff(Double mrpRoundOff) {
		this.mrpRoundOff = mrpRoundOff;
	}
	public Double getPurchase() {
		return purchase;
	}
	public void setPurchase(Double purchase) {
		this.purchase = purchase;
	}
	public List<AdministrationExpensesBean> getAdministrationExpensesBean() {
		return administrationExpensesBean;
	}
	public void setAdministrationExpensesBean(List<AdministrationExpensesBean> administrationExpensesBean) {
		this.administrationExpensesBean = administrationExpensesBean;
	}
	public Double getReceivedAmt() {
		return receivedAmt;
	}
	public void setReceivedAmt(Double receivedAmt) {
		this.receivedAmt = receivedAmt;
	}
	public Double getTurnOverTax() {
		return turnOverTax;
	}
	public void setTurnOverTax(Double turnOverTax) {
		this.turnOverTax = turnOverTax;
	}
	public Double getBreackage() {
		return breackage;
	}
	public void setBreackage(Double breackage) {
		this.breackage = breackage;
	}
	public Double getLicenseFee() {
		return licenseFee;
	}
	public void setLicenseFee(Double licenseFee) {
		this.licenseFee = licenseFee;
	}
	public Double getSittingAmt() {
		return sittingAmt;
	}
	public void setSittingAmt(Double sittingAmt) {
		this.sittingAmt = sittingAmt;
	}
	public Double getTcsAmt() {
		return tcsAmt;
	}
	public void setTcsAmt(Double tcsAmt) {
		this.tcsAmt = tcsAmt;
	}
	public Double getMonthlyExpenses() {
		return monthlyExpenses;
	}
	public void setMonthlyExpenses(Double monthlyExpenses) {
		this.monthlyExpenses = monthlyExpenses;
	}
	
	

}
