package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class MobileViewStockDiscountInner implements Serializable{
	
	private String date;
	private Double discount;
	private Double recieved;
	private Integer companyId;
	private String realDate;
	private Double arrears;
	List<MobileViewStockDiscountSuperInner> mobileViewStockDiscountSuperInner;
	DiscountAsPerCompanyBeen discountAsPerCompanyBeen;
	List<DiscountTransactionBean> discountAsPerCompanyBeenList;
	private Double rentals;
	private Double vendorAmount;
	private String comment;
	private Double creditAmt;
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Double getRecieved() {
		return recieved;
	}
	public void setRecieved(Double recieved) {
		this.recieved = recieved;
	}
	public List<MobileViewStockDiscountSuperInner> getMobileViewStockDiscountSuperInner() {
		return mobileViewStockDiscountSuperInner;
	}
	public void setMobileViewStockDiscountSuperInner(
			List<MobileViewStockDiscountSuperInner> mobileViewStockDiscountSuperInner) {
		this.mobileViewStockDiscountSuperInner = mobileViewStockDiscountSuperInner;
	}
	public DiscountAsPerCompanyBeen getDiscountAsPerCompanyBeen() {
		return discountAsPerCompanyBeen;
	}
	public void setDiscountAsPerCompanyBeen(DiscountAsPerCompanyBeen discountAsPerCompanyBeen) {
		this.discountAsPerCompanyBeen = discountAsPerCompanyBeen;
	}
	public Integer getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}
	public String getRealDate() {
		return realDate;
	}
	public void setRealDate(String realDate) {
		this.realDate = realDate;
	}
	public Double getArrears() {
		return arrears;
	}
	public void setArrears(Double arrears) {
		this.arrears = arrears;
	}
	public List<DiscountTransactionBean> getDiscountAsPerCompanyBeenList() {
		return discountAsPerCompanyBeenList;
	}
	public void setDiscountAsPerCompanyBeenList(List<DiscountTransactionBean> discountAsPerCompanyBeenList) {
		this.discountAsPerCompanyBeenList = discountAsPerCompanyBeenList;
	}
	public Double getRentals() {
		return rentals;
	}
	public void setRentals(Double rentals) {
		this.rentals = rentals;
	}
	public Double getVendorAmount() {
		return vendorAmount;
	}
	public void setVendorAmount(Double vendorAmount) {
		this.vendorAmount = vendorAmount;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public Double getCreditAmt() {
		return creditAmt;
	}
	public void setCreditAmt(Double creditAmt) {
		this.creditAmt = creditAmt;
	}
	

}
