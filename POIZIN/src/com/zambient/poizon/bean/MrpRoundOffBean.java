package com.zambient.poizon.bean;

import java.io.Serializable;

public class MrpRoundOffBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String date;
	private Double mrpRoundOff;
	private String dateAsPerCopy;
	private Double saveMrpRoundOff;
	private Double turnOverTax;
	private Double tcsVal;
	private Double retailerCreditVal;
	
	public Double getSaveMrpRoundOff() {
		return saveMrpRoundOff;
	}
	public void setSaveMrpRoundOff(Double saveMrpRoundOff) {
		this.saveMrpRoundOff = saveMrpRoundOff;
	}
	public String getDateAsPerCopy() {
		return dateAsPerCopy;
	}
	public void setDateAsPerCopy(String dateAsPerCopy) {
		this.dateAsPerCopy = dateAsPerCopy;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Double getMrpRoundOff() {
		return mrpRoundOff;
	}
	public void setMrpRoundOff(Double mrpRoundOff) {
		this.mrpRoundOff = mrpRoundOff;
	}
	public Double getTurnOverTax() {
		return turnOverTax;
	}
	public void setTurnOverTax(Double turnOverTax) {
		this.turnOverTax = turnOverTax;
	}
	public Double getTcsVal() {
		return tcsVal;
	}
	public void setTcsVal(Double tcsVal) {
		this.tcsVal = tcsVal;
	}
	public Double getRetailerCreditVal() {
		return retailerCreditVal;
	}
	public void setRetailerCreditVal(Double retailerCreditVal) {
		this.retailerCreditVal = retailerCreditVal;
	}
	
}
