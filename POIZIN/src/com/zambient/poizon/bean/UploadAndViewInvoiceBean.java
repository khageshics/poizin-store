package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class UploadAndViewInvoiceBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String date;
	private Double invoiceAmt;
	private List<addNewIndentBean> addNewIndentBeanList;
	
	public Double getInvoiceAmt() {
		return invoiceAmt;
	}
	public void setInvoiceAmt(Double invoiceAmt) {
		this.invoiceAmt = invoiceAmt;
	}
	public String getDate() {
		return date;
	}
	public List<addNewIndentBean> getAddNewIndentBeanList() {
		return addNewIndentBeanList;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setAddNewIndentBeanList(List<addNewIndentBean> addNewIndentBeanList) {
		this.addNewIndentBeanList = addNewIndentBeanList;
	}
}
