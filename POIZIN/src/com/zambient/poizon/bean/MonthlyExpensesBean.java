package com.zambient.poizon.bean;

import java.io.Serializable;

public class MonthlyExpensesBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long monthlyExpenseId;
	private String debitCredit;
	private Double amount;
	private String subject;
	private String comment;
	private String date;
	public Long getMonthlyExpenseId() {
		return monthlyExpenseId;
	}
	public void setMonthlyExpenseId(Long monthlyExpenseId) {
		this.monthlyExpenseId = monthlyExpenseId;
	}
	public String getDebitCredit() {
		return debitCredit;
	}
	public void setDebitCredit(String debitCredit) {
		this.debitCredit = debitCredit;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
}
