package com.zambient.poizon.bean;

import java.util.List;

public class InvoiceReportWithMrpRoundOffBean {
	private static final long serialVersionUID = 1L;
	
	List<SaleBean> saleBean;
	private MrpRoundOffBean mrpRoundOffBean;
	public List<SaleBean> getSaleBean() {
		return saleBean;
	}
	public void setSaleBean(List<SaleBean> saleBean) {
		this.saleBean = saleBean;
	}
	public MrpRoundOffBean getMrpRoundOffBean() {
		return mrpRoundOffBean;
	}
	public void setMrpRoundOffBean(MrpRoundOffBean mrpRoundOffBean) {
		this.mrpRoundOffBean = mrpRoundOffBean;
	}

}
