package com.zambient.poizon.bean;

import java.io.Serializable;

public class CreditChildBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Long creditId;
	private Long bottles;
	private Double brandNoPackQty;
	private Double price;
	private Double discount;
	private Double totalPrice;
	private Double afterIncludingDiscount;
	private String date;
	private Double totalDiscount;
	private String productName;
	private Long cases;
	private String name;
	public Double getTotalDiscount() {
		return totalDiscount;
	}
	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}
	public Long getCreditId() {
		return creditId;
	}
	public void setCreditId(Long creditId) {
		this.creditId = creditId;
	}
	public Long getBottles() {
		return bottles;
	}
	public void setBottles(Long bottles) {
		this.bottles = bottles;
	}
	public Double getBrandNoPackQty() {
		return brandNoPackQty;
	}
	public void setBrandNoPackQty(Double brandNoPackQty) {
		this.brandNoPackQty = brandNoPackQty;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public Double getAfterIncludingDiscount() {
		return afterIncludingDiscount;
	}
	public void setAfterIncludingDiscount(Double afterIncludingDiscount) {
		this.afterIncludingDiscount = afterIncludingDiscount;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public Long getCases() {
		return cases;
	}
	public void setCases(Long cases) {
		this.cases = cases;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

}
