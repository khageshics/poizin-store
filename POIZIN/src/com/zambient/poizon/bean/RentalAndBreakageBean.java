package com.zambient.poizon.bean;

import java.io.Serializable;

public class RentalAndBreakageBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private String month;
	private Integer company;
	private Double amount;
	private String comment;
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public Integer getCompany() {
		return company;
	}
	public void setCompany(Integer company) {
		this.company = company;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	@Override
	public String toString() {
		return "RentalAndBreakageBean [month=" + month + ", company=" + company + ", amount=" + amount + ", comment="
				+ comment + "]";
	}

}
