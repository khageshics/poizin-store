package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class ChartDataBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String label;
	private String color;
	private Long value;
	private String tooltext;
	List<category> category;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Long getValue() {
		return value;
	}
	public void setValue(Long value) {
		this.value = value;
	}
	public String getTooltext() {
		return tooltext;
	}
	public void setTooltext(String tooltext) {
		this.tooltext = tooltext;
	}
	public List<category> getCategory() {
		return category;
	}
	public void setCategory(List<category> category) {
		this.category = category;
	}
	@Override
	public String toString() {
		return "ChartDataBean [label=" + label + ", color=" + color + ", value=" + value + ", tooltext=" + tooltext
				+ ", category=" + category + "]";
	}

}
