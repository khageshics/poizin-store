package com.zambient.poizon.bean;

import java.io.Serializable;

public class StockPrediction implements Serializable {
private static final long serialVersionUID = 1L;
	
	private Long productId;
	private Double brandNoPackQty;
	private Long brandNo;
	private String brandName;
	private String productType;
	private String quantity; 
	private Long packQty;
	private String packType;
	private Long closing;
	private Double perDayStock;
	private Long totalSale;
	private Long noOfDays;
	private String company;
	private String category;
	private String color;
	private Long companyOrder;
	
	public Long getTotalSale() {
		return totalSale;
	}
	public void setTotalSale(Long totalSale) {
		this.totalSale = totalSale;
	}
	public Long getProductId() {
		return productId;
	}
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public Double getBrandNoPackQty() {
		return brandNoPackQty;
	}
	public void setBrandNoPackQty(Double brandNoPackQty) {
		this.brandNoPackQty = brandNoPackQty;
	}
	public Long getBrandNo() {
		return brandNo;
	}
	public void setBrandNo(Long brandNo) {
		this.brandNo = brandNo;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public Long getPackQty() {
		return packQty;
	}
	public void setPackQty(Long packQty) {
		this.packQty = packQty;
	}
	public String getPackType() {
		return packType;
	}
	public void setPackType(String packType) {
		this.packType = packType;
	}
	public Long getClosing() {
		return closing;
	}
	public void setClosing(Long closing) {
		this.closing = closing;
	}
	public Double getPerDayStock() {
		return perDayStock;
	}
	public void setPerDayStock(Double perDayStock) {
		this.perDayStock = perDayStock;
	}
	public Long getNoOfDays() {
		return noOfDays;
	}
	public void setNoOfDays(Long noOfDays) {
		this.noOfDays = noOfDays;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public Long getCompanyOrder() {
		return companyOrder;
	}
	public void setCompanyOrder(Long companyOrder) {
		this.companyOrder = companyOrder;
	}
}
