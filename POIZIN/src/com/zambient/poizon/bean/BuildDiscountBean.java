package com.zambient.poizon.bean;

import java.io.Serializable;
import java.util.List;

public class BuildDiscountBean implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private List<DiscountAndMonthBean>  transactiontransactionData;
	private List<DiscountAsPerCompanyBeen> receivedData;
	private Double rental;
	private PieChartBean previousArrears;
	private category categoryBean;
	private List<VendorAdjustmentGetArrearsBean> arrearsdetails;
	private Double totalChequeAmount;
	private Double adjCheque;
	private Double totalVendorDiscount;
	private Double totalDiscountAmt;
	private List<CreditMasterBean> creditMasterBeanList;
	private List<CreditChildBean> creditChildBeanList;
	private Double totalVendorCredit;
	private Double totalVendorCreditReceived;
	private CreditMasterBean creditMasterBean;
	
	public List<DiscountAndMonthBean> getTransactiontransactionData() {
		return transactiontransactionData;
	}
	public void setTransactiontransactionData(List<DiscountAndMonthBean> transactiontransactionData) {
		this.transactiontransactionData = transactiontransactionData;
	}
	public List<DiscountAsPerCompanyBeen> getReceivedData() {
		return receivedData;
	}
	public void setReceivedData(List<DiscountAsPerCompanyBeen> receivedData) {
		this.receivedData = receivedData;
	}
	public Double getRental() {
		return rental;
	}
	public void setRental(Double rental) {
		this.rental = rental;
	}
	public PieChartBean getPreviousArrears() {
		return previousArrears;
	}
	public void setPreviousArrears(PieChartBean previousArrears) {
		this.previousArrears = previousArrears;
	}
	public category getCategoryBean() {
		return categoryBean;
	}
	public void setCategoryBean(category categoryBean) {
		this.categoryBean = categoryBean;
	}
	public List<VendorAdjustmentGetArrearsBean> getArrearsdetails() {
		return arrearsdetails;
	}
	public void setArrearsdetails(List<VendorAdjustmentGetArrearsBean> arrearsdetails) {
		this.arrearsdetails = arrearsdetails;
	}
	public Double getTotalChequeAmount() {
		return totalChequeAmount;
	}
	public void setTotalChequeAmount(Double totalChequeAmount) {
		this.totalChequeAmount = totalChequeAmount;
	}
	public Double getAdjCheque() {
		return adjCheque;
	}
	public void setAdjCheque(Double adjCheque) {
		this.adjCheque = adjCheque;
	}
	public Double getTotalVendorDiscount() {
		return totalVendorDiscount;
	}
	public void setTotalVendorDiscount(Double totalVendorDiscount) {
		this.totalVendorDiscount = totalVendorDiscount;
	}
	public Double getTotalDiscountAmt() {
		return totalDiscountAmt;
	}
	public void setTotalDiscountAmt(Double totalDiscountAmt) {
		this.totalDiscountAmt = totalDiscountAmt;
	}
	public List<CreditMasterBean> getCreditMasterBeanList() {
		return creditMasterBeanList;
	}
	public void setCreditMasterBeanList(List<CreditMasterBean> creditMasterBeanList) {
		this.creditMasterBeanList = creditMasterBeanList;
	}
	public Double getTotalVendorCredit() {
		return totalVendorCredit;
	}
	public void setTotalVendorCredit(Double totalVendorCredit) {
		this.totalVendorCredit = totalVendorCredit;
	}
	public Double getTotalVendorCreditReceived() {
		return totalVendorCreditReceived;
	}
	public void setTotalVendorCreditReceived(Double totalVendorCreditReceived) {
		this.totalVendorCreditReceived = totalVendorCreditReceived;
	}
	public List<CreditChildBean> getCreditChildBeanList() {
		return creditChildBeanList;
	}
	public void setCreditChildBeanList(List<CreditChildBean> creditChildBeanList) {
		this.creditChildBeanList = creditChildBeanList;
	}
	public CreditMasterBean getCreditMasterBean() {
		return creditMasterBean;
	}
	public void setCreditMasterBean(CreditMasterBean creditMasterBean) {
		this.creditMasterBean = creditMasterBean;
	}
	

}
