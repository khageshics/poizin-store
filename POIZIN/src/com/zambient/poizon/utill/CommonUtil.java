package com.zambient.poizon.utill;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zambient.poizon.bean.DiscountAndMonthBean;
import com.zambient.poizon.bean.DiscountMonthWiseBean;

public class CommonUtil {
	
	
	public static void deleteImage(String month,String image,String companyId,String imageRootPath){
		try
        {
			 Files.deleteIfExists(Paths.get(imageRootPath+image));
        }
        catch(NoSuchFileException e)
        {
            System.out.println("No such file/directory exists");
        }
        catch(DirectoryNotEmptyException e)
        {
            System.out.println("Directory is not empty.");
        }
        catch(IOException e)
        {
            System.out.println("Invalid permissions.");
        }
	}
	public static void downloadZipFile(HttpServletRequest request, HttpServletResponse response,String filePath) throws IOException {
		//String fileName="PoizinChecks.zip";
	      //String filePath="D:\\khagesh\\zipFile\\";
	      ServletContext context = request.getServletContext();
	      File downloadFile = new File(filePath);
	        FileInputStream inputStream = new FileInputStream(downloadFile);
	        
	      String mimeType = context.getMimeType(filePath);
	        if (mimeType == null) {
	            // set to binary type if MIME mapping not found
	            mimeType = "application/octet-stream";
	        }
	        response.setContentType(mimeType);
	        response.setContentLength((int) downloadFile.length());
	        // set headers for the response
	        String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"",    downloadFile.getName());
	        response.setHeader(headerKey, headerValue);
	        
	     // get output stream of the response
	        OutputStream outStream = response.getOutputStream();
	 
	        byte[] buffer = new byte[4096];
	        int bytesRead = -1;
	 
	        // write bytes read from the input stream into the output stream
	        while ((bytesRead = inputStream.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	 
	        inputStream.close();
	        outStream.close();
		
	}
	// for create zip file
	static public void zipFolder(String srcFolder, String destZipFile) throws Exception {
        ZipOutputStream zip = null;
        FileOutputStream fileWriter = null;
        fileWriter = new FileOutputStream(destZipFile);
        zip = new ZipOutputStream(fileWriter);
        addFolderToZip("", srcFolder, zip);
        zip.flush();
        zip.close();
      }
      static private void addFileToZip(String path, String srcFile, ZipOutputStream zip)
          throws Exception {
        File folder = new File(srcFile);
        if (folder.isDirectory()) {
          addFolderToZip(path, srcFile, zip);
        } else {
          byte[] buf = new byte[1024];
          int len;
          FileInputStream in = new FileInputStream(srcFile);
          zip.putNextEntry(new ZipEntry(path + "/" + folder.getName()));
          while ((len = in.read(buf)) > 0) {
            zip.write(buf, 0, len);
          }
        }
      }

      static private void addFolderToZip(String path, String srcFolder, ZipOutputStream zip)
          throws Exception {
        File folder = new File(srcFolder);

        for (String fileName : folder.list()) {
          if (path.equals("")) {
            addFileToZip(folder.getName(), srcFolder + "/" + fileName, zip);
          } else {
            addFileToZip(path + "/" + folder.getName(), srcFolder + "/" +   fileName, zip);
          }
        }
      }
      
	public static List<DiscountAndMonthBean> getListBasedOnMatch(List<DiscountAndMonthBean> discountTransactionBeanList) {
		 List<DiscountAndMonthBean> secBeanList = new ArrayList<DiscountAndMonthBean>();
	   		for(DiscountAndMonthBean bean : discountTransactionBeanList){
	   			DiscountAndMonthBean beandata = new DiscountAndMonthBean();
	   			beandata.setMonth(bean.getMonth());
	   			double totalDiscount=0;
	  			List<DiscountMonthWiseBean> innerBeanList = new ArrayList<DiscountMonthWiseBean>();
	  			Set<Long> set = new HashSet<Long>();
	  			for(DiscountMonthWiseBean beanList : bean.getDiscountMonthWiseBean()){
	   				set.add(beanList.getMatch()); 
	   			}
	  			for (Long l : set) {
	  				double ttlDiscAmt = 0,discAmt=0;
	  				long totalCase =0; String name=null;
	  				DiscountMonthWiseBean innerBean = new DiscountMonthWiseBean();
	  				for (DiscountMonthWiseBean bealList : bean.getDiscountMonthWiseBean()) {
	  					if (bealList.getMatch().longValue() == l.longValue()) {
	  						ttlDiscAmt += bealList.getTotal();
	  						discAmt = bealList.getDiscount();
	  						totalCase += bealList.getCase();
	  						name = bealList.getBrand();
	  					}
	  				}
	  				totalDiscount += ttlDiscAmt;
	  				innerBean.setBrand(name);
	  				innerBean.setCase(totalCase);
	  				innerBean.setDiscount(discAmt);
	  				innerBean.setTotal(ttlDiscAmt);
	  				innerBeanList.add(innerBean);
	  			}
	  			beandata.setTotalDiscount(totalDiscount);
	  			beandata.setDiscountMonthWiseBean(innerBeanList);
	  			secBeanList.add(beandata);
	   		}
			return secBeanList;
	}
	public static double splitScheme(double qty, double totalSale, double pendingCase){
		 double data = (((qty * 100)/totalSale) * pendingCase) / 100;
		if (data >= 0)
			return data;
		else
			return 0;
	} 
/**
 * This method is used to convert date from yyyy-mm-dd to dd-mm-yyy
 * */
	public static String chageDateFormat(String date) {
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy");

		try {
			Date formatdate = formatter2.parse(date);
			String changedDate = formatter1.format(formatdate);
			return changedDate;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return date;
		}
	}
	public static String chageDateFormatSec(String date) {
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatter2 = new SimpleDateFormat("dd-MM-yyyy");

		try {
			Date formatdate = formatter1.parse(date);
			String changedDate = formatter2.format(formatdate);
			return changedDate;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return date;
		}
	}
	
}
