package com.zambient.poizon.utill;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.zambient.poizon.bean.DateAndValueBean;

public class SaleDateUtil {
	
	
	public static DateAndValueBean getMatchSaleDate(List<String> dateList,String indentDate,int days) throws ParseException{
		List<String> updatedList = new ArrayList<String>();
		String value = "";
		DateAndValueBean bean = new DateAndValueBean();
		for(int i=dateList.size();i>0;i--){
			Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dateList.get(i-1));
			DateFormat simpleDate=new SimpleDateFormat("E");
			String day=simpleDate.format(date);
			if(day.equalsIgnoreCase(indentDate)){
				System.out.println("day is :"+day+"  date :"+dateList.get(i-1));
				switch(days){
				case 1:
					if(i-1>0){
						updatedList.add(dateList.get(i-1));
						/*String firstNext=dateList.get(i-1);
						System.out.println(" day 1"+firstNext);*/
						}
					break;
				case 2:
					if(i-1>0){
						updatedList.add(dateList.get(i-1));
						}
						if(i-2>0){
						updatedList.add(dateList.get(i-2));
						}
					break;
				case 3:
					if(i-1>0){
						updatedList.add(dateList.get(i-1));
						}
						if(i-2>0){
						updatedList.add(dateList.get(i-2));
						}
						if(i-3>0){
						updatedList.add(dateList.get(i-3));
						}
					break;
				case 4:
					if(i-1>0){
						updatedList.add(dateList.get(i-1));
						}
						if(i-2>0){
						updatedList.add(dateList.get(i-2));
						}
						if(i-3>0){
						updatedList.add(dateList.get(i-3));
						}
						if(i-4>0){
						updatedList.add(dateList.get(i-4));
						}
					break;
				case 5:
					if(i-1>0){
						updatedList.add(dateList.get(i-1));
						}
						if(i-2>0){
						updatedList.add(dateList.get(i-2));
						}
						if(i-3>0){
						updatedList.add(dateList.get(i-3));
						}
						if(i-4>0){
						updatedList.add(dateList.get(i-4));
						}
						if(i-5>0){
						updatedList.add(dateList.get(i-5));
						}
					break;
				case 6:
					if(i-1>0){
						updatedList.add(dateList.get(i-1));
						}
						if(i-2>0){
						updatedList.add(dateList.get(i-2));
						}
						if(i-3>0){
						updatedList.add(dateList.get(i-3));
						}
						if(i-4>0){
						updatedList.add(dateList.get(i-4));
						}
						if(i-5>0){
						updatedList.add(dateList.get(i-5));
						}
						if(i-6>0){
						updatedList.add(dateList.get(i-6));
						}
					break;
				case 7:
					if(i-1>0){
					updatedList.add(dateList.get(i-1));
					}
					if(i-2>0){
					updatedList.add(dateList.get(i-2));
					}
					if(i-3>0){
					updatedList.add(dateList.get(i-3));
					}
					if(i-4>0){
					updatedList.add(dateList.get(i-4));
					}
					if(i-5>0){
					updatedList.add(dateList.get(i-5));
					}
					if(i-6>0){
					updatedList.add(dateList.get(i-6));
					}
					if(i-7>0){
					updatedList.add(dateList.get(i-7));
					}
					break;
				}
			}
			
		}
		
		for(int k=0; k < updatedList.size(); k++)
		{
			value +="'"+updatedList.get(k)+"'";
			if(!(k == (updatedList.size()-1))){
				value += ",";
			}
		}
		bean.setDate(value);
		bean.setValue((double) updatedList.size());
		System.out.println("value In String format :: "+value);
		return bean;
	} 

}
